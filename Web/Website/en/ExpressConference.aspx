<%--ZD 100147 start--%>
<%--/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/--%><%--ZD 100147 End--%>
<%@ Page Language="C#" Inherits="en_ExpressConference.ExpressConference" %><%--ZD 100170--%>

<%@ Register Assembly="MetaBuilders.WebControls.ComboBox" Namespace="MetaBuilders.WebControls"
    TagPrefix="mbcbb" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajax" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<!-- FB 2719 Starts -->
<% if (Session["isExpressUser"].ToString() == "1"){%>
<!-- #INCLUDE FILE="inc/maintopNETExp.aspx" -->
<%}else{%>
<!-- #INCLUDE FILE="inc/maintopNET.aspx" -->
<%}%>
<!-- FB 2719 Ends -->
<!--Window Dressing--> 

<script type="text/javascript">
    // ZD 100335 start
    function setCookie(c_name, value, exdays) {
        var exdate = new Date();
        exdate.setDate(exdate.getDate() + exdays);
        var c_value = escape(value) + ((exdays == null) ? "" : "; expires=" + exdate.toUTCString());
        document.cookie = c_name + "=" + c_value;
    }
    setCookie("hdnscreenres", screen.width, 365);
    //ZD 100335 End
    
    var servertoday = new Date();
    var hdnCfSt;
    var dFormat;
    dFormat = "<%=format %>";

    var servertoday = new Date(parseInt("<%=DateTime.Today.Year%>", 10), parseInt("<%=DateTime.Today.Month%>", 10) - 1, parseInt("<%=DateTime.Today.Day%>", 10),
        parseInt("<%=DateTime.Today.Hour%>", 10), parseInt("<%=DateTime.Today.Minute%>", 10), parseInt("<%=DateTime.Today.Second%>", 10));

    var maxDuration = 24;
    if ('<%=Application["MaxConferenceDurationInHours"] %>' != "")
        maxDuration = parseInt('<%=Application["MaxConferenceDurationInHours"] %>', 10);

    function fnShowHideAVLink() {
        var args = fnShowHideAVLink.arguments;
        var obj = eval(document.getElementById("LnkAVExpand"));

        if (obj) {
            obj.style.display = 'none';
            if (args[0] == '1') {
                obj.style.display = '';
            }
        }
    }


    //FB 2659 - Starts
    function fnShowSeats() {
        document.getElementById("modalDivPopup").style.display = 'block';
        document.getElementById("modalDivContent").style.display = 'block';
    }

    function fnPopupSeatsClose() {
        document.getElementById("modalDivPopup").style.display = 'none';
        document.getElementById("modalDivContent").style.display = 'none';
        return false;
    }

    function SelectOneDefault(obj) {
        var elements = document.getElementById(obj).innerHTML;
        var a = elements.toUpperCase().split("<BR>"); //Split the dateTime and assign. Start Date/Time: 04/30/2013 08:00<br>End Date/Time: 04/30/2013 09:00<br>Available Seats: 4
        var c = a[0];
        var b = c.split(": ");
        var strStartDate = b[1];

        c = a[1];
        b = c.split(": ");
        var strEndDate = b[1];

        document.getElementById("confStartDate").value = strStartDate.split(" ")[0];
        if (strStartDate.split(" ")[2] != null)
            document.getElementById("confStartTime_Text").value = strStartDate.split(" ")[1] + " " + strStartDate.split(" ")[2];
        else
            document.getElementById("confStartTime_Text").value = strStartDate.split(" ")[1];

        document.getElementById("confEndDate").value = strEndDate.split(" ")[0];
        if (strEndDate.split(" ")[2] != null)
            document.getElementById("confEndTime_Text").value = strEndDate.split(" ")[1] + " " + strEndDate.split(" ")[2];
        else
            document.getElementById("confEndTime_Text").value = strEndDate.split(" ")[1];

        fnPopupSeatsClose();
    }  
    //FB 2659 - End

    //FB 3055-Filter in Upload Files Starts

    function checkTipoFileInput(fileTypes) {

        var fileUpload1 = document.getElementById("FileUpload1");
        var fileUpload2 = document.getElementById("FileUpload2");
        var fileUpload3 = document.getElementById("FileUpload3");
        var retError = false;

        if ((fileUpload1 != null && (fileUpload1.value == "" || fileUpload1.value.length < 3)) && (fileUpload2 != null && (fileUpload2.value == "" || fileUpload2.value.length < 3)) && (fileUpload3 != null && (fileUpload3.value == "" || fileUpload3.value.length < 3)))
            return false;

        if (fileUpload1 != null && fileUpload1.value != "") {
            dots = fileUpload1.value.split(".");
            fileType = "." + dots[dots.length - 1];

            if (fileTypes.join(".").indexOf(fileType.toLowerCase()) == -1)
                retError = true;
        }
        if (fileUpload2 != null && fileUpload2.value != "") {
            dots = fileUpload2.value.split(".");
            fileType = "." + dots[dots.length - 1];
            if (fileTypes.join(".").indexOf(fileType.toLowerCase()) == -1)
                retError = true;
        }
        if (fileUpload3 != null && fileUpload3.value != "") {
            dots = fileUpload3.value.split(".");
            fileType = "." + dots[dots.length - 1];
            if (fileTypes.join(".").indexOf(fileType.toLowerCase()) == -1)
                retError = true;
        }
        if (retError) {
            alert("File type is invalid. Please select a new file and try again.");
            return false;
        }
    }
    //FB 3055-Filter in Upload Files - End

    function SelectAudioParty() {
        if (document.getElementById("txtAudioDialNo") != null)
            document.getElementById("txtAudioDialNo").value = "";

        if (document.getElementById("txtConfCode") != null)
            document.getElementById("txtConfCode").value = "";

        if (document.getElementById("txtLeaderPin") != null)
            document.getElementById("txtLeaderPin").value = "";

        document.getElementById("lblError2").innerText = "";
        document.getElementById("lblError3").innerText = "";
        document.getElementById("lblError4").innerText = "";

        RegtxtAudioDialNo.style.display = "none";
        RegtxtConfCode.style.display = "none";
        RegtxtLeaderPin.style.display = "none";
        var audins = "";//FB 2341
        var count; 
        var audinsitems = document.getElementsByName("lstAudioParty"); 
        for (var i=0; i < audinsitems.length; i++)
        {
            if (audinsitems[i].checked)
            {
                audins = audinsitems[i].value;
                //alert(audins);
            }
        }
        
        if (audins == -1) {
            document.getElementById("hdnAudioInsID").value = "";
            document.getElementById("txtAudioDialNo").disabled = true;
            document.getElementById("txtConfCode").disabled = true;
            document.getElementById("txtLeaderPin").disabled = true;
        }
        else {

            party = audins.split("|");
            if (party.length > 0) {
                document.getElementById("hdnAudioInsID").value = party[0];
                document.getElementById("txtAudioDialNo").disabled = false;
                document.getElementById("txtConfCode").disabled = false;
                document.getElementById("txtLeaderPin").disabled = false;
                document.getElementById("txtAudioDialNo").value = party[2];
                document.getElementById("txtConfCode").value = party[3];
                document.getElementById("txtLeaderPin").value = party[4];
            }
        }
    }

    function fnShowAVParams() {
        var obj = eval(document.getElementById("trAVCommonSettings"));
        var obj2 = eval(document.getElementById("trSelRoomsTitle"));
        var obj3 = eval(document.getElementById("trSelRoomsDetails"));
        var linkState = eval(document.getElementById("hdnAVParamState"));
        var expandlink = eval(document.getElementById("LnkAVExpand"));

        if (linkState) {
            if (obj) {
                obj.style.display = 'none';
                obj2.style.display = 'none';
                obj3.style.display = 'none';
                if (linkState.value == '') {
                    obj.style.display = '';
                    obj2.style.display = '';
                    obj3.style.display = '';
                    linkState.value = '1';

                    if (expandlink) {
                        expandlink.innerText = 'Collapse';
                    }
                }
                else {
                    obj.style.display = 'none';
                    obj2.style.display = 'none';
                    obj3.style.display = 'none';

                    linkState.value = '';
                    if (expandlink) {
                        expandlink.innerText = 'Expand';
                    }
                }
            }
        }
        return false;
    }

    function fnShowHideMeetLink() {
        var args = fnShowHideMeetLink.arguments;
        var obj = eval(document.getElementById("LnkMeetExpand"));

        if (obj) {
            obj.style.display = 'none';
            if (args[0] == '1') {
                obj.style.display = '';
            }
        }
    }

    //FB 2426 Start

    function ValidateflyEndpoints() {
        var ret = true;
        ValidatorEnable(document.getElementById("reqRoomName"), true);
        ValidatorEnable(document.getElementById("regRoomName"), true);
        ValidatorEnable(document.getElementById("reqcontactName"), true);
        ValidatorEnable(document.getElementById("cmpIPValPwd1"), true);
        ValidatorEnable(document.getElementById("cmpIPValPwd2"), true);
        ValidatorEnable(document.getElementById("cmpSIPValPwd1"), true);
        ValidatorEnable(document.getElementById("cmpSIPValPwd2"), true);
        ValidatorEnable(document.getElementById("cmpISDNValPwd1"), true);
        ValidatorEnable(document.getElementById("cmpISDNValPwd2"), true);

        var txtsiteName = document.getElementById("txtsiteName").value;//FB 2592
        var txtApprover5 = document.getElementById("txtApprover5").value; //FB 2592
        var txtIPAdd = document.getElementById("txtIPAddress").value;
        var txtIPPwd = document.getElementById("txtIPPassword").value;
        var txtIPconfirmPwd = document.getElementById("txtIPconfirmPassword").value;
        var dropA1 = document.getElementById("lstIPlinerate");
        var lstIPlinerate = dropA1.options[dropA1.selectedIndex].value;
        var dropA2 = document.getElementById("lstIPConnectionType");
        var lstIPConType = dropA2.options[dropA2.selectedIndex].value;
        var isDefault1 = document.getElementById("radioIsDefault").checked;

//        if (isDefault1 == false && txtIPAdd == "" && txtIPPwd == "" && txtIPconfirmPwd == "" && lstIPlinerate == "-1" && lstIPConType == "-1") {
//            fnDisableValidator1(false);
//            ret = true;
//        }
//        else {
//            fnDisableValidator1(true);
//        }
        //FB 2592 - Starts
        var cont = true;
        if (txtsiteName == "") {
            ValidatorEnable(document.getElementById("reqRoomName"), true);
            return false;
        }
        else if (txtsiteName.search(/^(a-z|A-Z|0-9)*[^\\<>^+?|!`\[\]{}\=@$%&~]*$/) == -1) {
            ValidatorEnable(document.getElementById("regRoomName"), true);
            return false;
        }

        if (txtApprover5 == "") {
            ValidatorEnable(document.getElementById("reqcontactName"), true);
            return false;
        }

        if (isDefault1 == true && (txtIPAdd == "" || lstIPlinerate == "-1" || lstIPConType == "-1"))
            cont = false;
        else if ((txtIPPwd == "" && txtIPconfirmPwd != "") || (txtIPPwd != "" && txtIPconfirmPwd == ""))
            cont = false;
        else if (isDefault1 == false) {
            var i = 0;
            if (txtIPAdd != "") i = i + 1;
            if (lstIPlinerate != "-1") i = i + 1;
            if (lstIPConType != "-1") i = i + 1;

            if (i > 0 && i < 3) cont = false;
        }
        if (cont == false) {
            fnDisableValidator1(true);
            return false;
        }
        //FB 2592 - End

        var txtSIPAdd = document.getElementById("txtSIPAddress").value;
        var txtSIPPwd = document.getElementById("txtSIPPassword").value;
        var txtSIPconfirmPwd = document.getElementById("txtSIPconfirmPassword").value;
        var dropB1 = document.getElementById("lstSIPlinerate");
        var lstSIPlinerate = dropB1.options[dropB1.selectedIndex].value;
        var dropB2 = document.getElementById("lstSIPConnectionType");
        var lstSIPConType = dropB2.options[dropB2.selectedIndex].value;
        var isDefault2 = document.getElementById("radioIsDefault2").checked;

//        if (isDefault2 == false && txtSIPAdd == "" && txtSIPPwd == "" && txtSIPconfirmPwd == "" && lstSIPlinerate == "-1" && lstSIPConType == "-1") {
//            fnDisableValidator2(false);
//            ret = true;
//        }
//        else {
//            fnDisableValidator2(true);
//        }

        //FB 2592 - Starts
        cont = true;
        if (isDefault2 == true && (txtSIPAdd == "" || lstSIPlinerate == "-1" || lstSIPConType == "-1"))
            cont = false;
        else if ((txtSIPPwd == "" && txtSIPconfirmPwd != "") || (txtSIPPwd != "" && txtSIPconfirmPwd == ""))
            cont = false;
        else if (isDefault2 == false) {
            var i = 0;
            if (txtSIPAdd != "") i = i + 1;
            if (lstSIPlinerate != "-1") i = i + 1;
            if (lstSIPConType != "-1") i = i + 1;

            if (i > 0 && i < 3) cont = false;
        }

        if (cont == false) {
            fnDisableValidator2(true);
            return false;
        }
        //FB 2592 - End

        var txtISDNAdd = document.getElementById("txtISDNAddress").value;
        var txtISDNPwd = document.getElementById("txtISDNPassword").value;
        var txtISDNconfirmPwd = document.getElementById("txtISDNconfirmPassword").value;
        var dropC1 = document.getElementById("lstISDNlinerate");
        var lstISDNlinerate = dropC1.options[dropC1.selectedIndex].value;
        var dropC2 = document.getElementById("lstISDNConnectionType");
        var lstISDNConType = dropC2.options[dropC2.selectedIndex].value;
        var isDefault3 = document.getElementById("radioIsDefault3").checked;

//        if (isDefault3 == false && txtISDNAdd == "" && txtISDNPwd == "" && txtISDNconfirmPwd == "" && lstISDNlinerate == "-1" && lstISDNConType == "-1") {
//            fnDisableValidator3(false);
//            ret = true;
//        }
//        else {
//            fnDisableValidator3(true);
//        }

        //FB 2592 - Starts

        cont = true;
        if (isDefault3 == true && (txtISDNAdd == "" || lstISDNlinerate == "-1" || lstISDNConType == "-1"))
            cont = false;
        else if ((txtISDNPwd == "" && txtISDNconfirmPwd != "") || (txtISDNPwd != "" && txtISDNconfirmPwd == ""))
            cont = false;
        else if (isDefault3 == false) {
            var i = 0;
            if (txtISDNAdd != "") i = i + 1;
            if (lstISDNlinerate != "-1") i = i + 1;
            if (lstISDNConType != "-1") i = i + 1;

            if (i > 0 && i < 3) cont = false;
        }

        if (cont == false) {
            fnDisableValidator3(true);
            return false;
        }

        //if (!Page_ClientValidate())
        //   return Page_IsValid;

        //FB 2592 - End

        return ret;
    }

    function deleteAssistant() {
        document.getElementById("txtApprover5").value = "";
        document.getElementById("txtEmailId").value = "";
    }

    function fnClearGuestGrid() {
        document.getElementById("txtsiteName").value = "";
        document.getElementById("txtApprover5").value = "";
        document.getElementById("txtEmailId").value = "";
        document.getElementById("txtPhone").value = "";

        document.getElementById("txtAddress").value = "";
        document.getElementById("txtState").value = "";
        document.getElementById("txtCity").value = "";
        document.getElementById("txtZipcode").value = "";
        document.getElementById("lstCountries").selectedIndex = 1;

        document.getElementById("txtIPAddress").value = "";
        document.getElementById("txtIPPassword").value = "";
        document.getElementById("txtIPconfirmPassword").value = "";
        document.getElementById("lstIPlinerate").selectedIndex = 0;
        document.getElementById("lstIPConnectionType").selectedIndex = 0;

        document.getElementById("txtSIPAddress").value = "";
        document.getElementById("txtSIPPassword").value = "";
        document.getElementById("txtSIPconfirmPassword").value = "";
        document.getElementById("lstSIPlinerate").selectedIndex = 0;
        document.getElementById("lstSIPConnectionType").selectedIndex = 0;

        document.getElementById("txtISDNAddress").value = "";
        document.getElementById("txtISDNPassword").value = "";
        document.getElementById("txtISDNconfirmPassword").value = "";
        document.getElementById("lstISDNlinerate").selectedIndex = 0;
        document.getElementById("lstISDNConnectionType").selectedIndex = 0;

        document.getElementById("radioIsDefault").checked = true;
        document.getElementById("radioIsDefault2").checked = false;
        document.getElementById("radioIsDefault3").checked = false;

        document.getElementById("btnGuestLocationSubmit").value = "Submit";
        document.getElementById("hdnGuestRoom").value = "-1";
    }

    function fnValidator() {
        fnClearGuestGrid();
        document.getElementById("reqRoomName").enabled = "true";
        document.getElementById("regRoomName").enabled = "true";
        document.getElementById("reqcontactName").enabled = "true";

        document.getElementById("reqIPAddress").enabled = "true";
        document.getElementById("regIPAddress").enabled = "true";
        document.getElementById("reqIPlinerate").enabled = "true";
        document.getElementById("reqIPConnectionType").enabled = "true";
        document.getElementById("cmpIPValPwd1").enabled = "true";
        document.getElementById("cmpIPValPwd2").enabled = "true";

        document.getElementById("reqSIPAddress").enabled = "true";
        document.getElementById("regSIPAddress").enabled = "true";
        document.getElementById("reqSIPlinerate").enabled = "true";
        document.getElementById("reqSIPConnectionType").enabled = "true";
        document.getElementById("cmpSIPValPwd1").enabled = "true";
        document.getElementById("cmpSIPValPwd2").enabled = "true";

        document.getElementById("reqISDNAddress").enabled = "true";
        document.getElementById("regISDNAddress").enabled = "true";
        document.getElementById("reqISDNlinerate").enabled = "true";
        document.getElementById("reqISDNConnectionType").enabled = "true";
        document.getElementById("cmpISDNValPwd1").enabled = "true";
        document.getElementById("cmpISDNValPwd2").enabled = "true";

        document.getElementById("btnGuestLocation2").click(); // FB 2525
        return false; // FB 2525
    }

    function fnDisableValidator() {

        ValidatorEnable(document.getElementById("reqRoomName"), false);
        ValidatorEnable(document.getElementById("regRoomName"), false);
        ValidatorEnable(document.getElementById("reqcontactName"), false);
        ValidatorEnable(document.getElementById("cmpIPValPwd1"), false);
        ValidatorEnable(document.getElementById("cmpIPValPwd2"), false);
        ValidatorEnable(document.getElementById("cmpSIPValPwd1"), false);
        ValidatorEnable(document.getElementById("cmpSIPValPwd2"), false);
        ValidatorEnable(document.getElementById("cmpISDNValPwd1"), false);
        ValidatorEnable(document.getElementById("cmpISDNValPwd2"), false);
        fnDisableValidator1(false);
        fnDisableValidator2(false);
        fnDisableValidator3(false);
    }

    function fnDisableValidator1(par1) {
        ValidatorEnable(document.getElementById("reqIPAddress"), par1);
        ValidatorEnable(document.getElementById("regIPAddress"), par1);
        ValidatorEnable(document.getElementById("reqIPlinerate"), par1);
        ValidatorEnable(document.getElementById("reqIPConnectionType"), par1);
    }

    function fnDisableValidator2(par2) {
        ValidatorEnable(document.getElementById("reqSIPAddress"), par2);
        ValidatorEnable(document.getElementById("regSIPAddress"), par2);
        ValidatorEnable(document.getElementById("reqSIPlinerate"), par2);
        ValidatorEnable(document.getElementById("reqSIPConnectionType"), par2);
    }

    function fnDisableValidator3(par3) {
        ValidatorEnable(document.getElementById("reqISDNAddress"), par3);
        ValidatorEnable(document.getElementById("regISDNAddress"), par3);
        ValidatorEnable(document.getElementById("reqISDNlinerate"), par3);
        ValidatorEnable(document.getElementById("reqISDNConnectionType"), par3);
    }

    //FB 2426 End
    
    

    function fnShowMeetPlanner() {
        var obj = eval(document.getElementById("tdMeetingPlanner"));
        var linkState = eval(document.getElementById("hdnMeetLinkSt"));
        var expandlink = eval(document.getElementById("LnkMeetExpand"));

        if (linkState) {
            if (obj) {
                obj.style.display = 'none';
                if (linkState.value == '') {
                    obj.style.display = '';
                    linkState.value = '1';

                    if (expandlink) {
                        expandlink.innerText = 'Collapse';
                    }
                }
                else {
                    obj.style.display = 'none';
                    linkState.value = '';
                    if (expandlink) {
                        expandlink.innerText = 'Expand';
                    }
                }
            }
        }
        return false;
    }

    function CheckUploadedFiles() {
        if (document.getElementById("FileUpload1") != null && document.getElementById("FileUpload2") != null && document.getElementById("FileUpload3") != null) {
            if (document.getElementById("FileUpload1").value == "" && document.getElementById("FileUpload2").value == "" && document.getElementById("FileUpload3").value == "") {
                document.getElementById("aFileUp").innerHTML = "Add Document";
            }
        }
    }

    function fnOpenGuest() {
        window.open('ManageInterSPGuest.aspx?frm=frmSettings2', 'spGuest', 'titlebar=yes,width=550,height=350,resizable=yes,scrollbars=yes,status=yes');
    }

    function fnOpenRemote() {
        var traccess = document.getElementById("trAccessnumber");
        var chkacess = document.getElementById("chkRemoteSP");
        var trintersp = document.getElementById("trInterSpGuest");

        if (chkacess) {
            if (chkacess.checked) {
                traccess.style.display = ""; //block
                trintersp.style.display = "none";
                document.getElementById("NONRecurringConferenceDiv8").style.display = "none";

            }
            else {
                traccess.style.display = "none";
                document.getElementById("NONRecurringConferenceDiv8").style.display = ""; //block
                trintersp.style.display = ""; //block

            }
        }

    }

    //FB 2501 Starts
    function deleteApprover(id) {
        eval("document.getElementById('hdnApprover" + (id) + "')").value = "";
        eval("document.getElementById('txtApprover" + (id) + "')").value = "";
    }
    //FB 2501 Ends
	
</script>

<script type="text/javascript" language="JavaScript" src="script/errorList.js"></script>

<script type="text/javascript" language="javascript1.1" src="extract.js"></script>

<script language="VBScript" src="script/lotus.vbs"></script>

<script type="text/javascript" src="inc/functions.js"></script>

<script type="text/javascript" src="script/mytreeNET.js"></script>

<script type="text/javascript" src="script/settings2.js"></script>

<script type="text/vbscript" src="script/settings2.vbs"></script>

<script type="text/javascript" src="script/mousepos.js"></script>

<script type="text/javascript" src="script/cal-flat.js"></script>

<script type="text/javascript" src="script/calview.js"></script>

<script type="text/javascript" src="lang/calendar-en.js"></script>

<script type="text/javascript" src="script/calendar-setup.js"></script>

<script language="VBScript" src="script/outlook.vbs" type="text/vbscript"></script>

<script type="text/javascript" src="script/saveingroup.js"></script>

<script type="text/javascript" src="script/calendar-flat-setup.js"></script>

<script type="text/javascript" src="script/RoomSearch.js"></script> <%--FB 2367--%>
<%--FB 2693--%>
<script type="text/javascript" src="script/CallMonitorJquery/jquery.1.4.2.js" ></script>
<script type="text/javascript" src="script/CallMonitorJquery/jquery.bpopup-0.7.0.min.js"></script>
<script type="text/javascript" src="script/pcconference.js"></script>
<script type="text/javascript" src="script/CallMonitorJquery/json2.js" ></script>

<script language="javascript" type="text/javascript">

    function callalert(val) {
        alert(val);
        return false;
    }

    function OpenSetRoom() {

        var lb = document.getElementById("RoomList"); //FB 2367
        if (lb != null) {

            for (var i = lb.options.length - 1; i >= 0; i--)
                lb.options[i] = null;

            lb.selectedIndex = -1;

            lb.disabled = true;
        }
        if (document.getElementById("btnRoomSelect"))
            document.getElementById("btnRoomSelect").click();
    }


    function ProccedClick() {

        /*var lb = document.getElementById("seltdRooms");
        if(lb != null)
        {
        
        for (var i=lb.options.length-1; i>=0; i--)    
        lb.options[i] = null;
        
        lb.selectedIndex = -1;
             
        lb.disabled=true;
        }
        if(document.getElementById("btnRoomSelect"))
        document.getElementById("btnRoomSelect").click();*/

        if (document.getElementById("ClosePUpbutton"))
            document.getElementById("ClosePUpbutton").click();

        return false;


    }
    
</script>

<script language="javascript" type="text/javascript">
    function fnEnableBuffer()
     {
        var chkenablebuffer = document.getElementById("chkEnableBuffer");
        
        //FB 1911
        if(document.getElementById("RecurSpec").value  != "")
            return;
            
         //FB 2341
         if(document.getElementById("chkStartNow").checked)
            return;
    
        var chkrecurrence = document.getElementById("chkRecurrence");
     
        if(chkrecurrence && chkrecurrence.checked == true) //Recurrence enabled and buffer zone checked
        {
            var confstdate = '';
            confstdate = GetDefaultDate(document.getElementById("confStartDate").value,'<%=format%>');
        
            var endTime = new Date(confstdate + " " + document.getElementById("confStartTime_Text").value);        
            //var apBfr = endTime.toLocaleTimeString().toString().split(" ")[1]; 
            var apBfr = endTime.format("tt"); //FB 2108
            var hrs = document.getElementById("RecurDurationhr").value;
            var mins = document.getElementById("RecurDurationmi").value;
            
            if(hrs == "")
                hrs = "0";
            
            if(mins == "")
                mins = "0"; 
            
            var timeMins = parseInt(hrs) * 60 ;
            timeMins = timeMins + parseInt(mins);
            endTime.setMinutes(endTime.getMinutes()  + parseInt(timeMins));
             //FB 2614
            var hh = endTime.getHours(); // parseInt(endTime.toLocaleTimeString().toString().split(" ")[0].toString().split(":")[0],10);            
            if('<%=Session["timeFormat"]%>' == "1") //FB 2108
            {
                if(hh >= 12)
                    hh = hh - 12;
                    
                if(hh == 0)
                    hh = 12
            }
            if (hh < 10)
                hh = "0" + hh;
            //FB 2614
            var mm = endTime.getMinutes();// parseInt(endTime.toLocaleTimeString().toString().split(" ")[0].toString().split(":")[1],10);
            if (mm < 10)
                mm = "0" + mm;
            //var ap = endTime.toLocaleTimeString().toString().split(" ")[1];
            var ap = endTime.format("tt"); //FB 2108
            var evntime = parseInt(hh,10);
                
            if(evntime < 12)
                evntime = evntime + 12
                           
            if('<%=Session["timeFormat"]%>' == "0")
            {
                if(ap == "AM")
                {
                    if(hh == "12")
                        document.getElementById("confEndTime_Text").value = "00:" + mm ;
                    else
                        document.getElementById("confEndTime_Text").value = hh + ":" + mm ;
                    
                }
                else
                {
                    if(evntime == "24")
                        document.getElementById("confEndTime_Text").value = "12:" + mm ;
                    else
                        document.getElementById("confEndTime_Text").value = evntime + ":" + mm ;
                }
            }
            else
            {
                document.getElementById("confEndTime_Text").value = hh + ":" + mm + " " + ap;
            }
              //FB 2634  
            //document.getElementById("TearDownDate").value = document.getElementById("confEndDate").value;
            //document.getElementById("TeardownTime_Text").value = document.getElementById("confEndTime_Text").value;
        }
     
//        if(chkenablebuffer && chkenablebuffer.checked) //Enable buffer zone check box checked state
//        {
//            //FB 2634
//            //document.getElementById("SDateText").innerHTML = "<span class='reqfldstarText'>*&nbsp;</span>Conference Setup ";
//            //document.getElementById("EDateText").innerHTML  = "Post Conference End <span class='reqfldstarText'>*</span>";
//            document.getElementById("SetupRow").style.display = "";//CTX hiding
//            document.getElementById("ConfStartRow").style.display = "";//CTX hiding
//            document.getElementById("ConfEndRow").style.display = "";//Block
//            //document.getElementById("SetupTime_Text").style.width = "75px"; 
//            //document.getElementById("TeardownTime_Text").style.width = "75px";
//        }
//        else
//        {   
//            //document.getElementById("SetupDate").value = document.getElementById("confStartDate").value;
//            //document.getElementById("SetupTime_Text").value = document.getElementById("confStartTime_Text").value;
//            
//            if(document.getElementById("Recur").value == "" && document.getElementById("hdnRecurValue").value == "")
//            { 
//                document.getElementById("TearDownDate").value = document.getElementById("confEndDate").value;
//                document.getElementById("TeardownTime_Text").value = document.getElementById("confEndTime_Text").value;
//            } 
//              
//        }
     }

     function SetRecurBuffer()
     {
        //FB 2588
        ChangeTimeFormat("D");
		//FB 2634
        //var setupdate = Date.parse(document.getElementById("SetupDate").value + " " + document.getElementById("SetupTime_Text").value);
        var sDate = Date.parse(document.getElementById("confStartDate").value + " " + document.getElementById("hdnStartTime").value);
        var chkenablebuffer = document.getElementById("chkEnableBuffer");
        var chkrecurrence = document.getElementById("chkRecurrence");
        
            if(chkrecurrence && chkrecurrence.checked == true)
            {
                var confstdate = '';
                confstdate = GetDefaultDate(document.getElementById("confStartDate").value,'<%=format%>');
        
                var endTime = new Date(confstdate + " " + document.getElementById("hdnStartTime").value);        
                //var apBfr = endTime.toLocaleTimeString().toString().split(" ")[1]; 
                var apBfr = endTime.format("tt"); //FB 2108
                var hrs = document.getElementById("RecurDurationhr").value;
                var mins = document.getElementById("RecurDurationmi").value;
            
                if(hrs == "")
                    hrs = "0";
                
                if(mins == "")
                    mins = "0"; 
            
                var timeMins = parseInt(hrs) * 60 ;
            
                timeMins = timeMins + parseInt(mins);
                endTime.setMinutes(endTime.getMinutes()  + parseInt(timeMins));
                
                var endTime1 = endTime;
                var month,date;
                month = endTime1.getMonth() + 1;
                date = endTime1.getDate();
                
                if(eval(endTime1.getMonth() + 1) <10)
                    month = "0" + (endTime1.getMonth() + 1);
                
                if(eval(date) <10)
                    date = "0" + endTime1.getDate();
                   
                if('<%=format%>' == 'MM/dd/yyyy')
                {
                    document.getElementById("confEndDate").value = month + "/" + date + "/" + endTime1.getFullYear();
                    //document.getElementById("TearDownDate").value = month + "/" + date + "/" + endTime1.getFullYear();//FB 2634
                }
                else
                {
                    document.getElementById("confEndDate").value =  date + "/" + month + "/" + endTime1.getFullYear();
                    //document.getElementById("TearDownDate").value =  date + "/" + month + "/" + endTime1.getFullYear();//FB 2634
                }
                //FB 2614
                var hh = endTime.getHours(); // parseInt(endTime.toLocaleTimeString().toString().split(" ")[0].toString().split(":")[0],10);            
                if("<%=Session["timeFormat"]%>" == "1") //FB 2108
                {
                    if(hh >= 12)
                        hh = hh - 12;
                        
                    if(hh == 0)
                        hh = 12
                }
                if (hh < 10)
                    hh = "0" + hh;
                
                //FB 2614
                var mm = endTime.getMinutes(); // parseInt(endTime.toLocaleTimeString().toString().split(" ")[0].toString().split(":")[1],10);
                if (mm < 10)
                    mm = "0" + mm;
                //var ap = endTime.toLocaleTimeString().toString().split(" ")[1];
                var ap = endTime.format("tt"); //FB 2108
                var evntime = parseInt(hh,10);
                
                if(evntime < 12)
                    evntime = evntime + 12
                       
                if('<%=Session["timeFormat"]%>' == '0' || '<%=Session["timeFormat"]%>' == '2')
                {
                    if(ap == "AM")
                    {
                        if(hh == "12")
                            document.getElementById("confEndTime_Text").value = "00:" + mm ;
                        else
                            document.getElementById("confEndTime_Text").value = hh + ":" + mm ;
                        
                    }
                    else
                    {
                        if(evntime == "24")
                            document.getElementById("confEndTime_Text").value = "12:" + mm ;
                        else
                            document.getElementById("confEndTime_Text").value = evntime + ":" + mm ;
                    }
                }
                else
                {
                    document.getElementById("confEndTime_Text").value = hh + ":" + mm + " " + ap;
                }
                //FB 2588
                ChangeTimeFormat("O");
                document.getElementById("confEndTime_Text").value = document.getElementById("hdnEndTime").value;
			//FB 2634
//                document.getElementById("TearDownDate").value = document.getElementById("confEndDate").value;
//                document.getElementById("TeardownTime_Text").value = document.getElementById("confEndTime_Text").value;
//                document.getElementById("SetupDate").value = document.getElementById("confStartDate").value;
//            
//                var setupdate = Date.parse(document.getElementById("SetupDate").value + " " + document.getElementById("SetupTime_Text").value);
//                var tDate = Date.parse(document.getElementById("TearDownDate").value + " " + document.getElementById("TeardownTime_Text").value);
//                
//                if(setupdate > tDate)
//                    document.getElementById("SetupTime_Text").value = document.getElementById("SetupTime_Text").value;
//                    
//               if(sDate > setupdate)
//               {
//                   document.getElementById("SetupDate").value = document.getElementById("confStartDate").value;
//                   document.getElementById("SetupTime_Text").value = document.getElementById("confStartTime_Text").value;
//               }
            
            }
     }
         
    function randam() { 
       var totalDigits = 4;
       var n = Math.random()*10000;
       n = Math.round(n);
       n = n.toString(); 
          var pd = ''; 
          if (totalDigits > n.length) { 
             for (i=0; i < (totalDigits-n.length); i++) { 
                pd += '0'; 
             } 
          } 
       return pd + n.toString();
    }
    //FB 2501 - Starts
    /*function num_gen() 
    {
       var num = randam()
       if (num.indexOf(0) == 0) 
          num = num.replace(0,1)

       var pass = document.getElementById("ConferencePassword")
       var pass1 = document.getElementById("ConferencePassword2")
       if(pass != "")
         pass.value = num;
       if(pass1 != "")
         pass1.value = num;
         
    } */
    function num_gen() 
    {
       var num = randam()
      //alert(num);
       if (num.indexOf(0) == 0) 
          num = num.replace(0,1)
      
        //FB 2244 - Starts
        if(document.getElementById("cmpValPassword") != null)
            document.getElementById("cmpValPassword").style.display = "none";
        if(document.getElementById("cmpValPassword1") != null)
            document.getElementById("cmpValPassword1").style.display = "none";
        if(document.getElementById("numPassword1") != null)
            document.getElementById("numPassword1").style.display = "none";

        var pass = document.getElementById("ConferencePassword")
        var pass1 = document.getElementById("ConferencePassword2")
        //if(pass != "")
        if(pass != null)
            pass.value = num;
        //if(pass1 != "")
        if(pass1 != null)
            pass1.value = num;
        //FB 2244 - End
         
       SavePassword(); 
    } //fb 676 end 
    
    function SavePassword()
    {
        //alert(document.getElementById("<%=ConferencePassword.ClientID %>").value);
        document.getElementById("<%=confPassword.ClientID %>").value = document.getElementById("<%=ConferencePassword.ClientID %>").value;
    }
 	//FB 2501 - End

    function UpdateCheckbox(obj)
    {
        if (obj.value == "")
        {
           document.getElementById(obj.id.replace("txtQuantity", "chkSelectedMenu")).checked = false;
           UpdatePrice(obj);
        }
        else
        {
            if (!isNaN(obj.value) && obj.value.indexOf(".") < 0)
            {
                document.getElementById(obj.id.replace("txtQuantity", "chkSelectedMenu")).checked = true;
                UpdatePrice(obj);
            }
            else
            {
                alert("Invalid Value.");
                obj.value = "";
            }
        }
    }
    
    function UpdatePrice(obj)
    {
        var lblPrice = document.getElementById(obj.id.substring(0, obj.id.indexOf("_dgCateringMenus")) + "_lblPrice");
        var price = parseFloat("0.00");
        var i = 2;
        
        while(document.getElementById(obj.id.substring(0, (obj.id.indexOf("Menus_ctl0") + 10)) + i + "_txtQuantity"))
        {
            if (i<10)
            {
                price += document.getElementById(obj.id.substring(0, (obj.id.indexOf("Menus_ctl0") + 10)) + i + "_txtPrice").value * document.getElementById(obj.id.substring(0, (obj.id.indexOf("Menus_ctl0") + 10)) + i + "_txtQuantity").value;
            }
            else
            {
                price += document.getElementById(obj.id.substring(0, (obj.id.indexOf("Menus_ctl") + 9)) + i + "_txtPrice").value * document.getElementById(obj.id.substring(0, (obj.id.indexOf("Menus_ctl") + 9)) + i + "_txtQuantity").value;
            }
            i++;
        }
        lblPrice.innerHTML = (price * 100.00) / 100.00;
    }

    function ShowItems(obj)
    {
        getMouseXY();
        var str = document.getElementById(obj.id.substring(0, obj.id.lastIndexOf("_")) + "_dgMenuItems").innerHTML;
        str = str.replace("<TBODY>", "<TABLE border='0' cellspacing='0' cellpadding='3' class='tableBody' width='200'><TBODY>");
        str = str.replace("</TBODY>", "</TBODY></TABLE>");
        document.getElementById("tblMenuItems").style.position = 'absolute';
        document.getElementById("tblMenuItems").style.left = mousedownX - 200;
        document.getElementById("tblMenuItems").style.top = mousedownY;
        document.getElementById("tblMenuItems").style.borderWidth = 1;
        document.getElementById("tblMenuItems").style.display="";
        document.getElementById("tblMenuItems").innerHTML = str;
    }
    
    function HideItems()
    {
       document.getElementById("tblMenuItems").style.display="none";
    }

    function ShowImage(obj)
    {
        document.getElementById("myPic").src = obj.src;
        getMouseXY();
        document.getElementById("divPic").style.position = 'absolute';
        document.getElementById("divPic").style.left = mousedownX + 20;
        document.getElementById("divPic").style.top = mousedownY;
        document.getElementById("divPic").style.display="";
    }

    function HideImage()
    {
        document.getElementById("divPic").style.display="none";
    }

    function CheckParty()
    {
        if (document.getElementById("ifrmPartylist"))
        {
            if (!ifrmPartylist.bfrRefresh())
            {
                DataLoading(0);
                return false;
            }
        }
        return true;
    }

function viewendpoint(val)
{
	
	url = "dispatcher/admindispatcher.asp?eid=" + val + "&cmd=GetEndpoint&ed=1&wintype=pop";

	if (!window.winrtc) {	// has not yet been defined
		winrtc = window.open(url, "", "width=400,height=310,top=0,left=0,resizable=yes,scrollbars=yes,status=no");
		winrtc.focus();
	} else { // has been defined
	    if (!winrtc.closed) {     // still open
	    	winrtc.close();
	        winrtc = window.open(url, "", "width=400,height=310,top=0,left=0,resizable=yes,scrollbars=yes,status=no");
			winrtc.focus();
		} else {
	        winrtc = window.open(url, "", "width=400,height=310,top=0,left=0,resizable=yes,scrollbars=yes,status=no");
	        winrtc.focus();
    	}
	}
}

function viewconflict(id, confTime, confDate, confDuration, confTZ, ConferenceName, confID)
{
    var rms = document.getElementById("selectedloc").value;
	url = "preconferenceroomchart.asp?wintype=ifr&cid=" + confID + "&cno=" + id +
		"&date=" + confDate + "&time=" + confTime + "&timeZone=" + confTZ +
		"&duration=" + confDuration + "&r=" + rms + "&n=" + ConferenceName;
	wincrm = window.open(url,'confroomchart','status=no,width=1,height=1,top=30,left=0,scrollbars=yes,resizable=yes')
	if (wincrm)
		wincrm.focus();
	else
		alert(EN_132);
}
		
function formatTime(timeText, regText)//FB 1715
{

   if("<%=Session["timeFormat"]%>" == "1")
   {    
		var tText = document.getElementById(timeText);

	    if(tText.value.length < 8)
        {
            tText.value = tText.value.replace('a','A').replace('p','P').replace('m','M').replace('AM',' AM').replace('PM',' PM');
        }
	
        if (document.getElementById(timeText).value.search(/^[0|1][0-9]:[0-5][0-9] ?[a|A|p|P][M|m]/) != -1)
        {
            var a_p = "";
            //FB 2614
            //var t = new Date();
            //var d = new Date(t.getDay() + "/" + t.getMonth() + "/" + t.getYear() + " " + document.getElementById(timeText).value);
            
            var d = new Date(GetDefaultDate(document.getElementById("confStartDate").value,'<%=format%>') + " " + tText.value);
           
            var curr_hour = d.getHours();
            if (curr_hour < 12) a_p = "AM"; else a_p = "PM";

            if (curr_hour == 0) curr_hour = 12;
            if (curr_hour > 12) curr_hour = curr_hour - 12;
            
            curr_hour = curr_hour + "";             
            if (curr_hour.length == 1)
               curr_hour = "0" + curr_hour;
                     
            var curr_min = d.getMinutes();
            curr_min = curr_min + "";
            if (curr_min.length == 1)
               curr_min = "0" + curr_min;

            document.getElementById(timeText).value = curr_hour + ":" + curr_min + " " + a_p;            
            document.getElementById(regText).style.display = "None"; 
            return true;            
       }
       else
       {    
            document.getElementById(regText).style.display = ""; 
            //document.getElementById(timeText).focus();
            return false;
       }
   }
    return true;
}
//FB 1716

function ChangeDuration()
{ 
    if ('<%=isEditMode%>' == "1" )    
    {        
        var duration = document.getElementById("hdnDuration").value;
        var chgVar =  document.getElementById("hdnChange").value;
              
        if(duration != '')
        {        
            var durArr = duration.split("&");
            var changeTime;
            
            var setupTime = new Date(GetDefaultDate(document.getElementById("confStartDate").value,'<%=format%>') + " "
            + document.getElementById("confStartTime_Text").value);
            
            var startTime = new Date(GetDefaultDate(document.getElementById("SetupDate").value,'<%=format%>') + " "
            + document.getElementById("SetupTime_Text").value);
            
            var endTime = new Date(GetDefaultDate(document.getElementById("TearDownDate").value,'<%=format%>') + " "
            + document.getElementById("TeardownTime_Text").value);
           
            switch(chgVar)
            {
                case "ST":  
                    setupTime = setCDuration(setupTime,durArr[1]);                    
                    document.getElementById("SetupDate").value = getCDate(setupTime);                    
                    document.getElementById("SetupTime_Text").value = getCTime(setupTime);
                    var startTime = new Date(GetDefaultDate(document.getElementById("SetupDate").value,'<%=format%>') + " "
                    + document.getElementById("SetupTime_Text").value);
                    
                    startTime = setCDuration(startTime,durArr[0]);                    
                    document.getElementById("TearDownDate").value = getCDate(startTime);                    
                    document.getElementById("TeardownTime_Text").value = getCTime(startTime);
                    var endTime = new Date(GetDefaultDate(document.getElementById("TearDownDate").value,'<%=format%>') + " "
                    + document.getElementById("TeardownTime_Text").value);                
                    
                    endTime = setCDuration(endTime,durArr[2]);
                    document.getElementById("confEndDate").value = getCDate(endTime);                   
                    document.getElementById("confEndTime_Text").value = getCTime(endTime);
                break;
                case "SU":
                
                    startTime = setCDuration(startTime,durArr[0]);                    
                    document.getElementById("TearDownDate").value = getCDate(startTime);                    
                    document.getElementById("TeardownTime_Text").value = getCTime(startTime);
                    var endTime = new Date(GetDefaultDate(document.getElementById("TearDownDate").value,'<%=format%>') + " "
                    + document.getElementById("TeardownTime_Text").value);                
                    
                    endTime = setCDuration(endTime,durArr[2]);
                    document.getElementById("confEndDate").value = getCDate(endTime);                   
                    document.getElementById("confEndTime_Text").value = getCTime(endTime);
                break;
                case "TD":
                   endTime = setCDuration(endTime,durArr[2]);
                   document.getElementById("confEndDate").value = getCDate(endTime);                   
                   document.getElementById("confEndTime_Text").value = getCTime(endTime);
                break;               
            }
        }
    }  
}

//FB 1716
function setCDuration(setupTime, dura)
{
    var chTime = new Date(setupTime);
    var d = 0;
    var min = 0;
    var hh = 0, dur = 0;
    if(dura > 60)
    {   
        hh = dura / 60;
        min = dura % 60;                        
        if(min > 0)
            hh = Math.floor(hh) + 1;                        
     
        for(d = 1; d <= hh ; d++)
        {
            if(min > 0 && d == hh)
                dur = dura % 60;
            else
                dur = 60;
                
             chTime.setMinutes(chTime.getMinutes() + dur);   
        }
    } 
    else //if(dura > 0) //FB 2501 - Start     FB 2634
    {
        chTime.setTime(chTime.getTime() + (dura * 60 * 1000));
        //if("<%=Session["DefaultConfDuration"]%>" != null)
        //{
//            var ConfDuration = "<%=Session["DefaultConfDuration"]%>";
//            chTime.setTime(chTime.getTime() + (dura * ConfDuration * 1000));        
//        }
    } 
    //FB 2501 - End 
    //else
      //  chTime;
        
   return chTime
}
//FB 1716

function getCDate(changeTime)
{
    var strDate;
    var month,date;
    month = changeTime.getMonth() + 1;
    date = changeTime.getDate();
    
    if(eval(changeTime.getMonth() + 1) <10)
        month = "0" + (changeTime.getMonth() + 1);
    
    if(eval(date) <10)
        date = "0" + changeTime.getDate();
       
    if('<%=format%>' == 'MM/dd/yyyy')    
        strDate = month + "/" + date + "/" + changeTime.getFullYear();
    else
        strDate =  date + "/" + month + "/" + changeTime.getFullYear();
    
    return strDate;
}
//FB 1716

function getCTime(changeTime)
{
    //FB 2614
    var hh = changeTime.getHours(); // parseInt(changeTime.toLocaleTimeString().toString().split(" ")[0].toString().split(":")[0],10);            
    if("<%=Session["timeFormat"]%>" == "1") //FB 2108
    {
        if(hh >= 12)
            hh = hh - 12;
            
        if(hh == 0)
            hh = 12
    }
    if (hh < 10)
        hh = "0" + hh;
    //FB 2614
    var mm = changeTime.getMinutes(); // parseInt(changeTime.toLocaleTimeString().toString().split(" ")[0].toString().split(":")[1],10);
    if (mm < 10)
        mm = "0" + mm;
     //var ap = changeTime.toLocaleTimeString().toString().split(" ")[1];
    var ap = changeTime.format("tt"); //FB 2108
   
    var evntime = parseInt(hh,10);
    
    if(evntime < 12 && ap == "PM")
       evntime = evntime + 12;
       
    var tiFormat =  "<%=Session["timeFormat"]%>" ;

    if(tiFormat == '0')
    {
        if(ap == "AM")                        
            strTime = (hh == "12") ? "00:" + mm : hh + ":" + mm ;
        else
        {
            if(evntime == "24")
                strTime = (hh == "12") ? "00:" + mm : evntime + ":" + mm;
            else
                strTime =  evntime + ":" + mm;
        }   
    }
    else
        strTime = hh + ":" + mm + " " + ap;
        
   return strTime;
}
//FB 2634
function EndDateValidation()
{
    ChangeTimeFormat("D"); //FB 2588
    var sDate = Date.parse(GetDefaultDate(document.getElementById("confStartDate").value,'<%=format%>') + " " + document.getElementById("hdnStartTime").value);
    var eDate = Date.parse(GetDefaultDate(document.getElementById("confEndDate").value,'<%=format%>') + " " + document.getElementById("hdnEndTime").value);
    
    if ( (sDate >= eDate) && (document.getElementById("hdnStartTime").value.search(/^[0|1][0-9]:[0-5][0-9] ?[a|A|p|P][M|m]/) != -1)
    && (document.getElementById("hdnEndTime").value.search(/^[0|1][0-9]:[0-5][0-9] ?[a|A|p|P][M|m]/) != -1))
    {
        if (sDate == eDate)
        {
             if (sDate > eDate)
                alert("End Time will be changed because it should be greater than Start Time.");
            else if (sDate == eDate)
                alert("End Time will be changed because it should be greater than Start Time.");            
        }
        else
            alert("End Date will be changed because it should be equal/greater than Start Date.");
            
         ChangeEndDate();
      }
}

//FB 2588
function ChangeTimeFormat() {

    var args = ChangeTimeFormat.arguments;
    var stime = document.getElementById("confStartTime_Text").value;
    var etime = document.getElementById("confEndTime_Text").value;
    var hdnsTime = document.getElementById("hdnStartTime");
    var hdneTime = document.getElementById("hdnEndTime");

    if ('<%=Session["timeFormat"]%>' == "2") {
        if (args[0] == "D") {
            stime = stime.replace('Z', '')
            stime = stime.substring(0, 2) + ":" + stime.substring(2, 4);
            etime = etime.replace('Z', '')
            etime = etime.substring(0, 2) + ":" + etime.substring(2, 4);
        }
        else {
            if (stime.indexOf("Z") < 0)
                stime = stime.replace(':', '') + "Z";
            if (etime.indexOf("Z") < 0)
                etime = etime.replace(':', '') + "Z";
        }
    }

    hdnsTime.value = stime
    hdneTime.value = etime
}

function ChangeEndDate()
{
    // FB 2692 Starts
    if ('<%=isEditMode%>' == "1" && document.getElementById("chkRecurrence").checked == false)
    {
        var sDate = Date.parse(GetDefaultDate(document.getElementById("confStartDate").value,'<%=format%>') + " " + document.getElementById("confStartTime_Text").value);
        var eDate = Date.parse(GetDefaultDate(document.getElementById("confEndDate").value,'<%=format%>') + " " + document.getElementById("confEndTime_Text").value);
        if(sDate >= eDate)
        {
            document.getElementById("errLabel").innerHTML = "Start time should be less than end time";
            window.scrollTo(0,0);
            return false;
        }
        else
        {
            var nDate = eDate - sDate;
            if(nDate < 900000)
            {
            document.getElementById("errLabel").innerHTML = "Minimum duration should be 15 minutes";
            window.scrollTo(0,0);
            return false;
            }
        }
        document.getElementById("errLabel").innerHTML = "";
        return false;
    }
    else if('<%=isEditMode%>' == "1")
        return false;
    // FB 2692 Ends
    ChangeTimeFormat("D"); //FB 2588
    var args = ChangeEndDate.arguments;
    var startDateTime = new Date(GetDefaultDate(document.getElementById("confStartDate").value,'<%=format%>') + " " + document.getElementById("hdnStartTime").value);
    var endDateTime = Date.parse(GetDefaultDate(document.getElementById("confEndDate").value,'<%=format%>') + " " + document.getElementById("hdnEndTime").value);
    var skipcheck = 0;
    
    if(args[0] == "ET")
    {
        if(startDateTime >= endDateTime)
            skipcheck = 0;
        else
            skipcheck = 1;
    }
    
    if('<%=Session["DefaultConfDuration"]%>' != null && skipcheck == 0)
    {
        var ConfDuration = '<%=Session["DefaultConfDuration"]%>';
        
        if ('<%=isEditMode%>' == "1" )
        {
            var duration = document.getElementById("hdnDuration").value;
            if(duration != "")
                ConfDuration = duration            
        }
        
        var ConfHours = parseInt(ConfDuration) / 60; 
        var ConfMinutes = parseInt(ConfDuration) % 60;    
        ConfMinutes = ConfMinutes + startDateTime.getMinutes();
        if(ConfMinutes > 59)
        {
           ConfMinutes = ConfMinutes - 60;
           ConfHours = ConfHours + 1;
        }
        startDateTime.setHours(startDateTime.getHours() + ConfHours);
        startDateTime.setMinutes(ConfMinutes);
    }
    else
        startDateTime = new Date(endDateTime);

    var month,date;
    month = startDateTime.getMonth() + 1;
    date = startDateTime.getDate();
    
    if(eval(month) < 10)
        month = "0" + month;
    
    if(eval(date) < 10)
        date = "0" + startDateTime.getDate();
    
    if('<%=format%>' == 'MM/dd/yyyy')
        document.getElementById("confEndDate").value = month + "/" + date + "/" + startDateTime.getFullYear();
    else
        document.getElementById("confEndDate").value =  date + "/" + month + "/" + startDateTime.getFullYear();
        
    var hh = startDateTime.getHours();
        
    if('<%=Session["timeFormat"]%>' == "1")
    {
        if(hh >= 12)
            hh = hh - 12;
            
        if(hh == 0)
            hh = 12
    }
            
    if (hh < 10)
        hh = "0" + hh;
    var mm = startDateTime.getMinutes()
    if (mm < 10)
        mm = "0" + mm;
    var ap = startDateTime.format("tt");
    var evntime = parseInt(hh,10);
    
     if(evntime < 12 && ap == "PM")
        evntime = evntime + 12;
               
    if('<%=Session["timeFormat"]%>' == "0" || '<%=Session["timeFormat"]%>' == "2") //FB 2588
    {
        if(ap == "AM")
        {
            if(hh == "12")
                document.getElementById("confEndTime_Text").value = "00:" + mm ;
            else
                document.getElementById("confEndTime_Text").value = hh + ":" + mm ;
        }
        else
        {
            if(evntime == "24")
                document.getElementById("confEndTime_Text").value = "12:" + mm ;
            else
                document.getElementById("confEndTime_Text").value = evntime + ":" + mm ;
        }
    }
    else
        document.getElementById("confEndTime_Text").value = hh + ":" + mm + " " + ap;
        
    //FB 2588
    ChangeTimeFormat("O");
    document.getElementById("confEndTime_Text").value = document.getElementById("hdnEndTime").value;
    
}


function DuraionCalculation()
{
    var confenddate = '';
    var confstdate = '';
    var durationMin = '';
    var totalMinutes;
    var totalDays;
    var totalHours;
    var lblconfduration = document.getElementById('<%=lblConfDuration.ClientID%>');
    
    lblconfduration.innerText = '';
    confenddate = GetDefaultDate(document.getElementById("confEndDate").value + " " + document.getElementById("confEndTime_Text").value,'<%=format%>');
    confstdate = GetDefaultDate(document.getElementById("confStartDate").value + " " + document.getElementById("confStartTime_Text").value,'<%=format%>');
    
    var durationMin = parseInt(Date.parse(confenddate) - Date.parse(confstdate)) /  1000  ;
    if(durationMin != '')
    {
        totalDays = Math.floor(durationMin / (24 * 60 * 60));
        totalHours = Math.floor((durationMin - (totalDays * 24 * 60 * 60)) / (60 * 60));
        totalMinutes = Math.floor((durationMin - (totalDays * 24 * 60 * 60) -  (totalHours * 60 * 60)) / 60);
    }
    
    if (Math.floor(durationMin) < 0)
        lblconfduration.innerText = "Invalid duration";
    if (totalDays > 0)
        lblconfduration.innerText = totalDays + " day(s) ";
    if (totalHours > 0)
        lblconfduration.innerText += totalHours + " hr(s) ";
    if (totalMinutes > 0)
        lblconfduration.innerText += totalMinutes + " min(s)";
}
//FB 2634
function EndDateValidation1(frm,confstdate,confenddate,msgchk)
{
    var setupdate = Date.parse(document.getElementById("SetupDate").value + " " + document.getElementById("SetupTime_Text").value);
    var sDate = Date.parse(document.getElementById("confStartDate").value + " " + document.getElementById("confStartTime_Text").value);
    var eDate = Date.parse(document.getElementById("confEndDate").value + " " + document.getElementById("confEndTime_Text").value);
    var tDate = Date.parse(document.getElementById("TearDownDate").value + " " + document.getElementById("TeardownTime_Text").value);
    //FB 1715
    if ( (sDate >= eDate)&& (document.getElementById("confStartTime_Text").value.search(/^[0|1][0-9]:[0-5][0-9] ?[a|A|p|P][M|m]/) != -1)        
    && (document.getElementById("confEndTime_Text").value.search(/^[0|1][0-9]:[0-5][0-9] ?[a|A|p|P][M|m]/) != -1))
    {
         if(msgchk == 'S')
        {
            if (Date.parse(confstdate) == Date.parse(confenddate))        
            {
                if (Date.parse(confstdate + " " + document.getElementById("confStartTime_Text").value) > Date.parse(confenddate + " " + document.getElementById("confEndTime_Text").value) )        
                    alert("End Time will be changed because it should be greater than Start Time.");  
               else if (Date.parse(confstdate + " " + document.getElementById("confStartTime_Text").value) == Date.parse(confenddate + " " + document.getElementById("confEndTime_Text").value) )        
                    alert("End Time will be changed because it should be greater than Start Time.");              
            }    
            else
                alert("End Date will be changed because it should be equal/greater than Start Date.");
        }
            
        ChangeEndDateTime(confstdate,confenddate); 
     }
     if(!document.getElementById("chkStartNow").checked) //FB 2341
      {
        var chkbuffer = document.getElementById("chkEnableBuffer");
        if (('<%=enableBufferZone%>' == "1") && (chkbuffer.checked))
        { 
              if ((setupdate < sDate) || (setupdate >= tDate) ||(setupdate > eDate) || (tDate > eDate) || (tDate < setupdate))
              {
                    //Modification for FB 1716 - Start
                    if( (setupdate < sDate) || (setupdate >= tDate) || (setupdate > eDate)|| (tDate < setupdate))
                    {
                        //if(!(setupdate >= tDate))
                        //{
                            document.getElementById("SetupDate").value = document.getElementById("confStartDate").value;
                            document.getElementById("SetupTime_Text").value = document.getElementById("confStartTime_Text").value;
                            document.getElementById("TearDownDate").value = document.getElementById("confEndDate").value;
                            document.getElementById("TeardownTime_Text").value = document.getElementById("confEndTime_Text").value;
                        //}
                    }
                    //Modification for FB 1716 - End 
              }
              if(tDate < setupdate)
               {
                     document.getElementById("TearDownDate").value = document.getElementById("SetupDate").value;                   
                    document.getElementById("TeardownTime_Text").value = document.getElementById("confEndTime_Text").value;
               } 
              
                  if(hdnCfSt == '1')
                  if(setupdate < sDate)
                  {
                     alert("Start Date/time will be changed because it should not be lesser than Setup Date/time.");
                     document.getElementById("confStartDate").value = document.getElementById("SetupDate").value;
                     document.getElementById("confStartTime_Text").value = document.getElementById("SetupTime_Text").value;
                     hdnCfSt = '0';
                  }
             if(setupdate >= tDate)
              {
                    var hhT;
                    var mmT;
                    var apT;
                    var zerto;
                                        
                    if(setupdate < tDate)
                       confstdate = GetDefaultDate(document.getElementById("TearDownDate").value,'<%=format%>');
                    else
                       confstdate = GetDefaultDate(document.getElementById("SetupDate").value,'<%=format%>');
                        
                    
            
            
                    var endTimeT = new Date(confstdate + " " + document.getElementById("SetupTime_Text").value); 
                    
                    
                               
                    if((setupdate > sDate))// && (document.getElementById("SetupTime_Text").value <= document.getElementById("confEndTime_Text").value))
                    {
                        endTimeT.setHours(endTimeT.getHours() + 1);
                        
                        
                        var monthT,dateT;
                        monthT = endTimeT.getMonth() + 1;
                        dateT = endTimeT.getDate();
                        
                        if(eval(endTimeT.getMonth() + 1) <10)
                            monthT = "0" + (endTimeT.getMonth() + 1);
                        
                        if(eval(dateT) <10)
                            dateT = "0" + endTimeT.getDate();
                           
                        if('<%=format%>' == 'MM/dd/yyyy')
                        {
                            document.getElementById("confEndDate").value = monthT + "/" + dateT + "/" + endTimeT.getFullYear();
                            document.getElementById("TearDownDate").value = monthT + "/" + dateT + "/" + endTimeT.getFullYear();
                        }
                        else
                        {
                            document.getElementById("confEndDate").value =  dateT + "/" + monthT + "/" + endTimeT.getFullYear();
                            document.getElementById("TearDownDate").value =  dateT + "/" + monthT + "/" + endTimeT.getFullYear();
                        }
                        //FB 2614
                        hhT = endTimeT.getHours(); //parseInt(endTimeT.toLocaleTimeString().toString().split(" ")[0].toString().split(":")[0],10);            
                        if (hhT < 10)
                            hhT = "0" + hhT;
                        
                        //FB 2614
                        mmT = endTimeT.getMinutes(); // parseInt(endTimeT.toLocaleTimeString().toString().split(" ")[0].toString().split(":")[1],10);
                        if (mmT < 10)
                            mmT = "0" + mmT;
                        //FB 2614
                        apT = endTimeT.format("tt"); //.toLocaleTimeString().toString().split(" ")[1];
                        
                        zerto = hhT + ":" + mmT + " " + apT;                
                        var evntime = parseInt(hhT,10);
                
                        if(evntime < 12 && apT == "PM")
                            evntime = evntime + 12;
                            if('<%=Session["timeFormat"]%>' == "0")
                            {
                                if(apT == "AM")
                                {
                                    if(hhT == "12")
                                        document.getElementById("confEndTime_Text").value = "00:" + mmT ;
                                    else
                                        document.getElementById("confEndTime_Text").value = hhT + ":" + mmT ;
                                    
                                }
                                else
                                {
                                    if(evntime == "24")
                                        document.getElementById("confEndTime_Text").value = "12:" + mmT ;
                                    else
                                        document.getElementById("confEndTime_Text").value = evntime + ":" + mmT ;
                                }
                                    
                            }
                            else
                            {
                                document.getElementById("confEndTime_Text").value = zerto;
                                
                            }
                        }
                   document.getElementById("TearDownDate").value = document.getElementById("confEndDate").value;
                   document.getElementById("TeardownTime_Text").value = document.getElementById("confEndTime_Text").value;
              }
              var chkrecurrence = document.getElementById("chkRecurrence");    
              if(chkrecurrence != null && chkrecurrence.checked == true)
              {
                    document.getElementById("confEndDate").value = document.getElementById("TearDownDate").value;
                    document.getElementById("confEndTime_Text").value = document.getElementById("TeardownTime_Text").value;
              }
        }
        else
        {
            document.getElementById("SetupDate").value = document.getElementById("confStartDate").value;
            document.getElementById("SetupTime_Text").value = document.getElementById("confStartTime_Text").value;
                        
            document.getElementById("TearDownDate").value = document.getElementById("confEndDate").value;
            document.getElementById("TeardownTime_Text").value = document.getElementById("confEndTime_Text").value;
        }   
    }
      DuraionCalculation();  
}

function ChangeEndDateTime(confstdate,confenddate)
{
      document.getElementById("confEndDate").value = document.getElementById("confStartDate").value;
         
        var endTime = new Date(confstdate + " " + document.getElementById("confStartTime_Text").value);
        
        //var apBfr = endTime.toLocaleTimeString().toString().split(" ")[1];          
        var apBfr = endTime.format("tt");      //FB 2108
        
        endTime.setHours(endTime.getHours() + 1);
            //if('<%=client.ToString().ToUpper()%>' =="MOJ") FB 2501
            //endTime.setMinutes(endTime.getMinutes() - 45);
        
        var month,date;
        month = endTime.getMonth() + 1;
        date = endTime.getDate();
        
        if(eval(endTime.getMonth() + 1) <10)
            month = "0" + (endTime.getMonth() + 1);
        
        if(eval(date) <10)
            date = "0" + endTime.getDate();
        document.getElementById("SetupDate").value = document.getElementById("confStartDate").value;//FB 1716   
        if('<%=format%>' == 'MM/dd/yyyy')
        {
            document.getElementById("confEndDate").value = month + "/" + date + "/" + endTime.getFullYear();
            document.getElementById("TearDownDate").value = month + "/" + date + "/" + endTime.getFullYear();
        }
        else
        {
            document.getElementById("confEndDate").value =  date + "/" + month + "/" + endTime.getFullYear();
            document.getElementById("TearDownDate").value =  date + "/" + month + "/" + endTime.getFullYear();
        }
        
        //FB 2614
        var hh = endTime.getHours(); // parseInt(endTime.toLocaleTimeString().toString().split(" ")[0].toString().split(":")[0],10);            
        if("<%=Session["timeFormat"]%>" == "1") //FB 2108
        {
            if(hh >= 12)
                hh = hh - 12;
                
            if(hh == 0)
                hh = 12
        }
        if (hh < 10)
            hh = "0" + hh;
        //FB 2614
        var mm = endTime.getMinutes() ;// parseInt(endTime.toLocaleTimeString().toString().split(" ")[0].toString().split(":")[1],10);
        if (mm < 10)
            mm = "0" + mm;
        //var ap = endTime.toLocaleTimeString().toString().split(" ")[1];
        var ap = endTime.format("tt");    //FB 2108  
        var evntime = parseInt(hh,10);
        
         if(evntime < 12 && ap == "PM")
            evntime = evntime + 12;
            
        if('<%=Session["timeFormat"]%>' == "0")
        {
            if(ap == "AM")
            {
                if(hh == "12")
                    document.getElementById("confEndTime_Text").value = "00:" + mm ;
                else
                    document.getElementById("confEndTime_Text").value = hh + ":" + mm ;
                
            }
            else
            {
                if(evntime == "24")
                    document.getElementById("confEndTime_Text").value = "12:" + mm ;
                else
                    document.getElementById("confEndTime_Text").value = evntime + ":" + mm ;
            }
                
        }
        else
        {
            document.getElementById("confEndTime_Text").value = hh + ":" + mm + " " + ap;
        }      
}

function ChangeStartDate(frm)
{
    if(document.getElementById("Recur").value == "" && document.getElementById("hdnRecurValue").value == "")
    {
        var confenddate = '';
        confenddate = GetDefaultDate(document.getElementById("confEndDate").value,'<%=format%>');
        var confstdate = '';
        confstdate = GetDefaultDate(document.getElementById("confStartDate").value,'<%=format%>');
       /*
         if (Date.parse("<%=Session["systemDate"]%>" + " " + "<%=Session["systemTime"]%>") > Date.parse(confenddate + " " + document.getElementById("confEndTime_Text").value))
        {
            document.getElementById("confEndDate").value = document.getElementById("confStartDate").value;
            if (frm == "0") 
            {
                alert("Invalid End Date or Time. It should be greater than time current time in user preferred timezone.");
                DataLoading(0);
                return false;
            }
        }
        */
        EndDateValidation(frm,confstdate,confenddate,'S');

    }
    ChangeDuration();//FB 1716
    return true;
}

function CheckDuration()
{
    if (document.getElementById("Recur").value == "")
    if (Date.parse(document.getElementById("confStartDate").value + " " + document.getElementById("confStartTime_Text").value) > Date.parse(document.getElementById("confEndDate").value + " " + document.getElementById("confEndTime_Text").value))
    {
        alert("Invalid Duration");
        return false;
    }
}
//FB 2226
function ChangePublic()
{    	
    if(document.getElementById("chkPublic") != null)
    {
        if ((document.getElementById("chkPublic").checked) && ("<%=Session["dynInvite"] %>" == "1") )    
        {
            document.getElementById("openRegister").style.display = "";
        }
        else
        {
            document.getElementById("openRegister").style.display ="none";
        }
    }
}
//FB 2870 Start
function ChangeNumeric()
{
    if(document.getElementById("ChkEnableNumericID") != null)
    {
        if ((document.getElementById("ChkEnableNumericID").checked))  
        {
            document.getElementById("DivNumeridID").style.display = "";
            ValidatorEnable(document.getElementById("ReqNumeridID"), true);
        }
        else
        {
            document.getElementById("DivNumeridID").style.display ="none";
            ValidatorEnable(document.getElementById("ReqNumeridID"), false);
        }
    }
}
//FB 2870 End
//VMR
function changeVMR()
{
    var iVMR;//FB 2448
    iVMR = "1";

    //var vmrParty = document.getElementById("isVMR")
    if(document.getElementById("lstVMR") != null) //FB 2620
    {
        if ((document.getElementById("lstVMR").selectedIndex) >0) // FB 2620
        {
            //if (iVMR  == "1" || iVMR  == "2" || iVMR  == "3")//FB 2448 //FB 2717
	        {
    		   if(document.getElementById("lstVMR").selectedIndex  == 1)
    		    document.getElementById("divbridge").style.display = "block";
    		   else
                document.getElementById("divbridge").style.display ="none";
    		    
    		   document.getElementById("OnFlyRowGuestRoom").style.visibility = "hidden";
    		   document.getElementById("btnGuestLocation").style.visibility = "hidden";
    		    document.getElementById("Image6").style.visibility = "hidden";
    		    
    		    document.getElementById("idHr").style.visibility = "hidden";
    		    document.getElementById("divIdRoomList").style.height = "1px";
    		   
                document.getElementById("trFECC").style.display ="none";//FB 2583 //FB 2595
            }
            //vmrParty.value = "1";
            
            // FB 2501 Starts
            if('<%=isEditMode%>' == "0")
            {
                if(document.getElementById('ConferencePassword') != null )
                {
                  if(document.getElementById('ConferencePassword').value == "")
                    num_gen();
                }         
            }   
            if(document.getElementById('trStartMode1') != null && document.getElementById('trStartMode2') != null)
            {
               document.getElementById('trStartMode1').style.display = 'none';
               document.getElementById('trStartMode2').style.display = 'none';
            }            
        }
        else
        {   
            if ('<%=Session["EnableFECC"]%>' == "1") //FB 2583
                document.getElementById("trFECC").style.display = ""; //FB 2595//FB 2592
            else
                 document.getElementById("trFECC").style.display = "none"; //FB 2595
               
            if(document.getElementById('trStartMode1') != null && document.getElementById('trStartMode2') != null)
            {     
                if('<%=EnableStartMode%>' == 1)
                {
                    document.getElementById('trStartMode1').style.display = '';
                    document.getElementById('trStartMode2').style.display = '';
                }
                else
                {
                    document.getElementById('trStartMode1').style.display = 'none';
                    document.getElementById('trStartMode2').style.display = 'none';
                }
            }
            // FB 2501 Ends     
            document.getElementById("divbridge").style.display ="none";
            document.getElementById("OnFlyRowGuestRoom").style.visibility = "visible";
    		document.getElementById("btnGuestLocation").style.visibility = "visible";
		    document.getElementById("Image6").style.visibility = "visible";
		    document.getElementById("idHr").style.visibility = "visible";
		    document.getElementById("divIdRoomList").style.height = "100px";
    		    
            //vmrParty.value = "0";
            
            // FB 2501 Starts
//            if('<%=isEditMode%>' == "0")
//            {
//                if(document.getElementById('ConferencePassword') != null )
//                {
//                    document.getElementById('ConferencePassword').value = "";
//                    document.getElementById('ConferencePassword2').value = "";
//                }
//            }
            // FB 2501 Ends
         }
    }
  
   //FB 2819 Starts
   if(document.getElementById("chkPCConf") != null) 
    var isPCCOnf = document.getElementById("chkPCConf").checked;
   
   var VMRselected =0;
   if(document.getElementById("lstVMR") != null) 
     VMRselected = document.getElementById("lstVMR").selectedIndex;
    
    if(isPCCOnf)
    {
    
        document.getElementById("OnFlyRowGuestRoom").style.visibility = "hidden";
	    document.getElementById("btnGuestLocation").style.visibility = "hidden";
	    document.getElementById("Image6").style.visibility = "hidden";
	    
        if(document.getElementById("trVMR") != null)
            document.getElementById("trVMR").style.display ="none";
    
        if(document.getElementById("trStartMode") != null)
            document.getElementById("trStartMode").style.display = 'None';

        if(document.getElementById('trStartMode1') != null)
            document.getElementById('trStartMode1').style.display = 'None';
    }
    else if (VMRselected >0 || '<%=enableCloudInstallation%>' == 1) //ZD 100166
    {
     if(document.getElementById("trPCConf") != null)
         document.getElementById("trPCConf").style.display ="none";
    }
    else
    {
        if(document.getElementById("trPCConf") != null) 
         document.getElementById("trPCConf").style.display ="";
        
        //ZD 100166  
        if(document.getElementById("trVMR") != null)
        {
            if('<%=enableCloudInstallation%>' == 1)
               document.getElementById("trVMR").style.display = 'None';  
            else
               document.getElementById("trVMR").style.display = "";
        }
        if('<%=Session["EnableStartMode"]%>' ==1)
        {
         if(document.getElementById("trStartMode") != null)
            document.getElementById("trStartMode").style.display = '';

         if(document.getElementById('trStartMode1') != null)
            document.getElementById('trStartMode1').style.display = ''; 
        }
     }
    //FB 2819 Ends
    
    DisplayMCUConnectRow("<%=mcuSetupDisplay %>","<%=mcuTearDisplay %>",'<%=EnableBufferZone%>'); //FB 2998

  return true;
}
//FB 2501 - Starts
function fnVMR()
{
    var iVMR;//FB 2448
    iVMR = "1";

    //var vmrParty = document.getElementById("isVMR")
    if(document.getElementById("lstVMR") != null) //FB 2620
    {
        if ((document.getElementById("lstVMR").selectedIndex) >0) // FB 2620
        {
	        if (iVMR  == "1" || iVMR  == "2" || iVMR  == "3")//FB 2448 //FB 2717
	        {
    		   if(iVMR  == "1")
    		    document.getElementById("divbridge").style.display = "block";
    		   else
                document.getElementById("divbridge").style.display ="none";
    		    
    		   document.getElementById("OnFlyRowGuestRoom").style.visibility = "hidden";
    		   document.getElementById("btnGuestLocation").style.visibility = "hidden";
    		    document.getElementById("Image6").style.visibility = "hidden";
    		    
    		    document.getElementById("idHr").style.visibility = "hidden";
    		    document.getElementById("divIdRoomList").style.height = "1px";
    		    
            }
            //vmrParty.value = "1";
            // FB 2501
            if(document.getElementById('trStartMode1') != null && document.getElementById('trStartMode2') != null)
            {
               document.getElementById('trStartMode1').style.display = 'none';
               document.getElementById('trStartMode2').style.display = 'none';
            }
        }
        else
        {
            document.getElementById("divbridge").style.display ="none";
            document.getElementById("OnFlyRowGuestRoom").style.visibility = "visible";
            document.getElementById("btnGuestLocation").style.visibility = "visible";
		    document.getElementById("Image6").style.visibility = "visible";
		    document.getElementById("idHr").style.visibility = "visible";
		    document.getElementById("divIdRoomList").style.height = "100px";
    		    
            //vmrParty.value = "0";
            // FB 2501
            if(document.getElementById('trStartMode1') != null && document.getElementById('trStartMode2') != null)
            {
                document.getElementById('trStartMode1').style.display = 'block';
                document.getElementById('trStartMode2').style.display = 'block';
            }
         }
    }
  return true;
}
//FB 2501 - End
function ChangeImmediate()
{
    var t;
	var t1;
    document.getElementById("lstDuration_Text").style.width = "75px";
    document.getElementById("confStartTime_Text").style.width = "75px";
    document.getElementById("confEndTime_Text").style.width = "75px";
	//FB 2634
    //document.getElementById("SetupTime_Text").style.width = "75px"; 
    //document.getElementById("TeardownTime_Text").style.width = "75px"; 
    
	document.getElementById("NONRecurringConferenceDiv9").style.display = 'None'; //FB 2266 
    if (document.frmSettings2.Recur.value == "")
    {
        if( document.getElementById("hdnRecurValue").value == 'R')
        {
             t = "none";
             t1 = "";
            Page_ValidationActive=false;
            document.getElementById("divDuration").style.display = "none";
        }
        else if (document.getElementById("chkStartNow").checked)  //FB 2341
        {
            t = "none";
            t1 = "none";
            Page_ValidationActive=false;
            document.getElementById("divDuration").style.display = "";
            //document.getElementById("chkEnableBuffer").checked = false;
        }
        else
        {
            t = "";
            t1 = "";
            Page_ValidationActive=true;
            document.getElementById("divDuration").style.display ="none";
        }
        //FB 2634
        if(t1 == '' && '<%=EnableBufferZone%>' == "1" && '<%=enableCloudInstallation%>' <= 0) //ZD 100166        
	        document.getElementById("SetupRow").style.display = t1;
	    else
            document.getElementById("SetupRow").style.display = 'None';
	    
	    document.getElementById("NONRecurringConferenceDiv8").style.display = t1;
	    document.getElementById("NONRecurringConferenceDiv3").style.display = t1;
	    document.getElementById("ConfStartRow").style.display = t1;
	    document.getElementById("ConfEndRow").style.display = t1;
	    //document.getElementById("NONRecurringConferenceDiv9").style.display = 'None'; //FB 2266
	    if(document.getElementById("recurDIV"))
	        document.getElementById("recurDIV").style.display = 'None';
 		
 		//FB 1911
	    var args = ChangeImmediate.arguments;
	    if(args != null)
	    {
	        if(args[0] == "S")
	        {
	            document.getElementById("recurDIV").style.display = "None"; // FB 2050
	            document.frmSettings2.RecurSpec.value = '';
	            document.frmSettings2.RecurringText.value = '';	            
	        }
	    }
	    
        //FB 2266
        //if ('<%=Application["recurEnable"]%>' == '0')
		if ('<%=Session["recurEnable"]%>' == "0") 
        {
            document.getElementById("NONRecurringConferenceDiv8").style.display = "none";
        }
        else if ("<%=isInstanceEdit%>" == "Y" )
            document.getElementById("NONRecurringConferenceDiv8").style.display = "none";
        else
        {
            document.getElementById("NONRecurringConferenceDiv8").style.display = t1;
        }
        
        if ('<%=timeZone%>' == "0" ) 
            document.getElementById("NONRecurringConferenceDiv3").style.display = "none";    
          
        document.getElementById("EndDateArea").style.display = t;
	    //document.getElementById("TeardownArea").style.display = t;
	    
	    fnEnableBuffer();
    }
    
    DisplayMCUConnectRow("<%=mcuSetupDisplay %>","<%=mcuTearDisplay %>",'<%=EnableBufferZone%>') //FB 2998
    
}


function isRecur()
{
    var t = (document.frmSettings2.Recur.value == "") ? "" : "none";
    
    if(isRecur.arguments.length > 0 || document.getElementById("hdnRecurValue").value == 'R')
    {
        if(isRecur.arguments[0] == 'R' || document.getElementById("hdnRecurValue").value == 'R')
            t = "None";
    }
    
//FB 2634
   if ("<%=client.ToString().ToUpper() %>" == "MOJ")  
   {  
        document.getElementById("StartNowRow").style.display = "none";//FB 2341
	    document.getElementById("ConfEndRow").style.display = "none"; 
    }
    else if("<%=Session["EnableImmConf"]%>" == "0") //FB 2036
       document.getElementById("StartNowRow").style.display = "none";//FB 2341 
    else
       document.getElementById("StartNowRow").style.display = t;
 
	document.getElementById("lstDuration_Text").style.width = "75px";
    document.getElementById("confStartTime_Text").style.width = "75px";
    document.getElementById("confEndTime_Text").style.width = "75px";
//    document.getElementById("SetupTime_Text").style.width = "75px"; 
//    document.getElementById("TeardownTime_Text").style.width = "75px"; 
    if(t == "")
    {
        document.getElementById("RecurrenceRow").style.display = 'None';
        //document.getElementById("ConfEndRow").style.display = ''; 
        document.getElementById("DurationRow").style.display = 'None';
        
    }
    else
    {
        document.getElementById("RecurrenceRow").style.display = '';//Block
        document.getElementById("DurationRow").style.display = '';//Block
    }
        
	if (t != "")
	{
	    document.getElementById("<%=RecurFlag.ClientID %>").value="1";
	}
	
	if ("<%=timeZone%>" == "0" )
      document.getElementById("NONRecurringConferenceDiv3").style.display = "none"; 
      
	document.getElementById("EndDateArea").style.display = t;
    document.getElementById("StartDateArea").style.display = t;
    //document.getElementById("TeardownArea").style.display = t;//FB 2634
    document.getElementById("ConfEndRow").style.display = t;
      
      return false;
}


function getYourOwnEmailList (i)
{
	if(i == 999)
	    url = "emaillist2main.aspx?t=e&frm=party2NET&fn=Setup&n=" + i;	
	else
	    url = "emaillist2main.aspx?t=e&frm=approverNET&fn=Setup&n=" + i; 
	if (!window.winrtc) {	// has not yet been defined
		winrtc = window.open(url, "", "width=920,height=470,top=0,left=0,resizable=yes,scrollbars=yes,status=no"); //FB 2735
		winrtc.focus();
	} else // has been defined
	    if (!winrtc.closed) {     // still open
	    	winrtc.close();
	        winrtc = window.open(url, "", "width=920,height=470,top=0,left=0,resizable=yes,scrollbars=yes,status=no"); //FB 2735
			winrtc.focus();
		} else {
	        winrtc = window.open(url, "", "width=920,height=470,top=0,left=0,resizable=yes,scrollbars=yes,status=no"); //FB 2735
	        winrtc.focus();
		}
}
//FB 2670 Starts
function getVNOCEmailList()
{
    var Confvnoc = document.getElementById("hdnVNOCOperator");
    url = "VNOCparticipantlist.aspx?cvnoc="+Confvnoc.value;
    winrtc = window.open(url, "", "width=920,height=470,top=0,left=0,resizable=yes,scrollbars=yes,status=no");//FB 2596 //FB 2735
}

function deleteVNOC()
{
   document.getElementById('txtVNOCOperator').value = "";
   document.getElementById('hdnVNOCOperator').value = "";
}
//FB 2670 Ends
function goToCal()
{
	roomcalendarview();
}


function openAdvancesetting()
{
	var chkadvance = document.getElementById("chkAdvancesetting");
	
	if(chkadvance.checked)
	    document.getElementById("Advancesetting").style.display = '';//Block
	else
	    document.getElementById("Advancesetting").style.display = 'None';
}


function openRecur()
{
//FB 2634
	var chkrecurrence = document.getElementById("chkRecurrence");
    var args = openRecur.arguments;
    var recType = "";
    if(args.length > 0)
        if(args[0] == 'S')
            recType = "S";
            
    if((recType == "" && chkrecurrence.checked))
        recType = "N";
        
    document.getElementById("confStartTime_Text").style.width = "75px";
    document.getElementById("confEndTime_Text").style.width = "75px";
            
	if(recType == "N")
	{
	    document.getElementById("RecurrenceRow").style.display = '';
	    document.getElementById("<%=RecurFlag.ClientID %>").value="1";
	    
        document.getElementById("StartDateArea").style.display = 'None';
	    document.getElementById("EndDateArea").style.display = 'None';
        document.getElementById("StartNowRow").style.display = 'None';
        document.getElementById("divDuration").style.display = "None";
        document.getElementById("DurationRow").style.display = '';//FB 2634
        document.getElementById("ConfEndRow").style.display = 'None';
        document.getElementById("ConfStartRow").style.display = '';                
        
        if('<%=EnableBufferZone%>' == 0 || '<%=enableCloudInstallation%>' == 1) //ZD 100166
            document.getElementById("SetupRow").style.display = 'None';                
        else
            document.getElementById("SetupRow").style.display = '';                
        
        document.getElementById("RecurSpec").value = "";
        document.getElementById("hdnRecurValue").value = 'R';
        document.getElementById("recurDIV").style.display = 'None';       
        //FB 2634
        initial();
        fnShow();
        
	}
	else if (recType == "S")
	{
	    var st = document.getElementById("<%=lstConferenceTZ.ClientID%>");
        var sd = GetDefaultDate(document.getElementById("confStartDate").value,'<%=format%>')        
        var stime = document.getElementById("<%=confStartTime.ClientID%>");
        
        removerecur();
        
        chkrecurrence.checked = false;               
        document.getElementById("recurDIV").style.display = '';
        document.getElementById("SetupRow").style.display = 'None';                
        document.getElementById("ConfStartRow").style.display = 'None';                
        document.getElementById("ConfEndRow").style.display = 'None';
        document.getElementById("RecurrenceRow").style.display = 'None';
        document.getElementById("DurationRow").style.display = 'None';
        document.getElementById("StartNowRow").style.display = 'None';
        //FB 2588
        ChangeTimeFormat("D");
        //FB 2634
        top.recurWin = window.open('recurNET.aspx?pn=E&frm=frmSettings2&st='+ st.value + "&sd=" + sd + "&stime=" + stime 
        + "&su="+ document.getElementById("SetupDuration").value + "&tdn="+ document.getElementById("TearDownDuration").value 
        + "&wintype=pop", 'recur1', 'titlebar=yes,width=650,height=563,resizable=yes,scrollbars=yes,status=yes');//buffer zone
	}
	else
	{
        document.getElementById("DurationRow").style.display = 'None';
        document.getElementById("recurDIV").style.display = 'None';
        document.getElementById("hdnRecurValue").value = '';
	    document.getElementById("RecurrenceRow").style.display = 'None';
	    
        if('<%=EnableBufferZone%>' == 1 && '<%=enableCloudInstallation%>' <= 0) //ZD 100166
            document.getElementById("SetupRow").style.display = '';                
        else
            document.getElementById("SetupRow").style.display = 'None';
            
        document.getElementById("ConfStartRow").style.display = '';                
        document.getElementById("ConfEndRow").style.display = '';
        document.getElementById("StartDateArea").style.display = '';
	    document.getElementById("EndDateArea").style.display = '';
        if('<%=Session["EnableImmConf"] %>' == "1") //FB 2634
            document.getElementById("StartNowRow").style.display = '';
        else
            document.getElementById("StartNowRow").style.display = 'None';
            
        //removerecur();//FB 2605-Commented
	}
}


function openRecur1()
{
    //FB 1911
    var args = openRecur.arguments;
    var isarg = "0";
    if(args.length > 0)
        if(args[0] == 'S')
            isarg = "1";
            
	var chkrecurrence = document.getElementById("chkRecurrence");
	
	if(isarg == "1" && chkrecurrence.checked)
    {
        removerecur();        
        chkrecurrence.checked = false;
    }
    
    if(isarg =="0" && chkrecurrence.checked)
        document.getElementById("RecurSpec").value = "";
    
    if(chkrecurrence != null && chkrecurrence.checked == false && isarg == "0")
    {
        removerecur();
        ChangeEndDate();
    }
    else
    {
	    if (ChangeEndDate(1) && ChangeStartDate(1))
	    {
	        var st = document.getElementById("<%=lstConferenceTZ.ClientID%>");
	        var sd = GetDefaultDate(document.getElementById("confStartDate").value,'<%=format%>')        
	        var stime = document.getElementById("<%=confStartTime.ClientID%>");
			//FB 2634
	        //var setupTime = document.getElementById("SetupTime_Text");
	        //var teardownTime = document.getElementById("TeardownTime_Text");
	        var sTime = document.getElementById("hdnSetupTime");
	        var tTime = document.getElementById("hdnTeardownTime");
	       
//	         sTime.value  = setupTime.value;
//	         tTime.value = teardownTime.value;
	        
	       
	       document.getElementById("EndDateArea").style.display = 'None';
	       document.getElementById("StartDateArea").style.display = 'None';
	       document.getElementById("TeardownArea").style.display = 'None';
    	   
    	   //FB 1911
	       if(isarg == "1")
           {
                showSpecialRecur();
	            top.recurWin = window.open('recurNET.aspx?pn=E&frm=frmSettings2&st='+ st.value + "&sd=" + sd + "&stime=" + stime + "&wintype=pop", 'recur1', 'titlebar=yes,width=650,height=563,resizable=yes,scrollbars=yes,status=yes');//buffer zone
	       }
           else
           {        
				//FB 2634
               document.getElementById("hdnRecurValue").value = 'R';	
	           document.getElementById("SetupRow").style.display = 'None';
	           document.getElementById("ConfStartRow").style.display = '';                
	           document.getElementById("ConfEndRow").style.display = '';
	           document.getElementById("recurDIV").style.display = 'None';       
	           document.getElementById("RecurText").value = '';    
	           document.getElementById("hdnRecurValue").value = 'R';
	           isRecur();
	           initial();
	           fnShow();
	       }
	       
//            if (timeDur > (maxDuration * 60)) 
//                alert(EN_314);
		}
	}
	          fnHideStartNow();//FB 1825
}


//FB 1911
function visControls()
{
    if(document.getElementById("RecurSpec").value == "")
    {
	//FB 2634
	   document.getElementById("StartDateArea").style.display = '';
//	   document.getElementById("TeardownArea").style.display = '';
	   document.getElementById("EndDateArea").style.display = '';
	   //FB 2634
	   
	   if('<%=EnableBufferZone%>' == 0 || '<%=enableCloudInstallation%>' == 1) //ZD 100166
	    document.getElementById("SetupRow").style.display = 'None';
	   else
	    document.getElementById("SetupRow").style.display = '';
	   
	   document.getElementById("TearDownRow").style.display = 'None';
	   document.getElementById("ConfStartRow").style.display = '';
	   document.getElementById("ConfEndRow").style.display = '';
	   //FB 2959 START
	   if('<%=Session["EnableImmConf"]%>' == "1")//FB 2634
        document.getElementById("StartNowRow").style.display = '';
       else
        document.getElementById("StartNowRow").style.display = 'None';
       //FB 2959 END
	   
       document.getElementById("recurDIV").style.display = 'None';     
       document.getElementById("RecurText").value = ''; 
       
       document.getElementById("confStartTime_Text").style.width = "75px";
//       document.getElementById("SetupTime_Text").style.width = "75px"; 
//       document.getElementById("TeardownTime_Text").style.width = "75px";          
	}
}

function showSpecialRecur()
{
	//FB 2634
    document.getElementById("recurDIV").style.display = '';
    document.getElementById("SetupRow").style.display = 'None';                
    document.getElementById("ConfStartRow").style.display = 'None';                
    document.getElementById("ConfEndRow").style.display = 'None';
    document.getElementById("EndDateArea").style.display = 'None';
    document.getElementById("StartDateArea").style.display = 'None';
    //document.getElementById("TeardownArea").style.display = 'None';
    document.getElementById("TearDownRow").style.display = 'None';
    document.getElementById("RecurrenceRow").style.display = 'None';
}

function roomcalendarview()
{   
    var frm = document.getElementById("confStartDate").value;
    rn = document.getElementById("selectedloc").value; 
    rn = getListViewChecked(rn);	
    url = "dispatcher/admindispatcher.asp?cmd=ManageConfRoom&f=v&hf=1&m=" + rn + "&d=" + frm;
    window.open(url,"","left=50,top=50,width=860,height=550,resizable=yes,scrollbars=yes,status=no");
    return false;
}

function roomconflict(confDate) //FB 1154 & //FB 2027 SetConference
{   
    var frm = confDate;
    rn = document.getElementById("selectedloc").value
    
    //rn = getListViewChecked(rn);	
    
    //url = "dispatcher/admindispatcher.asp?cmd=ManageConfRoom&f=v&hf=1&m=" + rn  + "&d=" + frm;
    url = "roomcalendar.aspx?f=v&hf=1&m=" + rn + "&d=" + confDate;
    window.open(url,"","left=50,top=50,width=860,height=550,resizable=yes,scrollbars=yes,status=no");
    return false;
}

function CallMeetingPla()
{   

        var confTimeZone = "<%=lstConferenceTZ.SelectedValue%>";
        var sd = "<%=confStartDate.Text %>";
        var stime = "<%=confStartTime.Text %>";
        var confDateTime = sd + " " + stime;
        var roomId = document.getElementById("selectedloc").value;

        roomId = getListViewChecked(roomId); 
        rn = roomId.split(",").length - 1;
        rId = roomId.split(",").length - 1;

        if(rn==0 || (rn==1 && roomId == ', '))
        {
          alert("Please select at least one Room.")
          return false;
        }
        if(rn >5)
        {
          alert("Only Five Rooms are allowed to view the Meeting Planner")
          return false;
        }
       
        url = "MeetingPlanner.aspx?";
                url += "ConferenceDateTime=" + confDateTime + "&";
                url += "ConferenceTimeZone=" + confTimeZone + "&";
                url += "RoomID=" + roomId;
    
	  window.open(url,"","left=50,top=50,width=860,height=550,resizable=yes,scrollbars=yes,status=no");
      return false;
          
      }

function AudioAddon()
{
    var args = AudioAddon.arguments;
    
    var drptyp = document.getElementById("CreateBy");
    var controlName = 'dgUsers_ctl';
    var ctlid = '';
    
    if(drptyp)
    {
        if(drptyp.value == "2")
        {
            if(args)
            {
                var tbl1 = document.getElementById(args[0]);
                var tbl2 = document.getElementById(args[1]);
                var drp = document.getElementById(args[2]);
                var drp2 = document.getElementById(args[3]);
                 
                var usrArr=args[2].split("_");
                var len=usrArr.length;

                if(len > 2)
                {
                    ctlid = usrArr[1];
                    ctlid = ctlid.replace(/ctl/i,'');
                }
                
                var lstAddType = document.getElementById((controlName + ctlid +'_lstAddressType'));
                var lstConType = document.getElementById((controlName + ctlid +'_lstConnectionType'));
                var txtAdd = document.getElementById((controlName + ctlid +'_txtAddress'));

                var txtaddphone = document.getElementById((controlName + ctlid +'_txtaddphone'));
                var txtconfcode = document.getElementById((controlName + ctlid +'_txtConfCode'));
                var txtleader = document.getElementById((controlName + ctlid +'_txtleaderPin'));
                
                if(drp.value == "2" && '<%=Application["EnableAudioAddOn"]%>' == "1")
                {
                    tbl1.style.display = 'none';
                    tbl2.style.display = '';//block
                    
                    if(lstAddType)
                    {
                        lstAddType.value = '1';
                    }
                    if(lstConType)
                    {
                        lstConType.value = '2';
                    }
                    if(txtAdd)
                    {
                        if(Trim(txtAdd.value) == '')
                            txtAdd.value = '127.0.0.1';
                    }
                }
                else
                {
                    if(txtaddphone)
                    {
                        if(Trim(txtaddphone.value) == '')
                        {
                            txtaddphone.value = '127.0.0.1';
                        }
                    }
                    if(txtconfcode)
                    {
                        if(Trim(txtconfcode.value) == '')
                            txtconfcode.value = '0';
                    }
                    if(txtleader)
                    {
                        if(Trim(txtleader.value) == '')
                            txtleader.value = '0';
                    }
                    tbl1.style.display = '';//block
                    tbl2.style.display = 'none';
                }
                drp2.value = drp.value;
            }
                
        }
    }
}

</script>

<%--Merging Recurrence Script Start Here--%>

<script type="text/javascript">

var openerfrm = document.frmSettings2;


function CheckDate(obj)
{    
    var endDate;//FB 2246
    if ('<%=format%>'=='dd/MM/yyyy')
        endDate = GetDefaultDate(obj.value,'dd/MM/yyyy');
    else
        endDate = obj.value;
        
    if (Date.parse(endDate) < Date.parse(new Date()))
        alert("Invalid Date");
}

function fnHide()
{    
    document.getElementById("Daily").style.display = 'none';
    document.getElementById("Weekly").style.display = 'none';
    document.getElementById("Monthly").style.display = 'none';
    document.getElementById("Yearly").style.display = 'none';
    document.getElementById("Custom").style.display = 'none';
    document.getElementById("RangeRow").style.display = 'block';    
}

function fnShow()
{  
     var a = null;
    var f = document.forms[1];
    var e = f.elements["RecurType"];
    for (var i=0; i < e.length; i++)
    {
        if (e[i].checked)
        {
        a = e[i].value;
        document.frmSettings2.RecPattern.value = a;
        break;
        }
    }
    
    if(a != null)
    {
        fnHide();
        switch(a)
        {
            case "1":
	            initialdaily(rpstr);
                document.getElementById("Daily").style.display = 'block';
                
                break;
            case "2":
	            initialweekly(rpstr);
                document.getElementById("Weekly").style.display = 'block';                            
                break;
            case "3":
	            initialmonthly(rpstr);
                document.getElementById("Monthly").style.display = 'block';                            
                break;
            case "4":
	            initialyearly(rpstr);
                document.getElementById("Yearly").style.display = 'block';                            
                break;
            case "5":
            
                document.getElementById('flatCalendarDisplay').innerHTML = "";  // clear the div values
                showFlatCalendar(1, dFormat,document.getElementById("StartDate").value);  //for custom calendar display      
	            initialcustomly(rpstr);
                document.getElementById("Custom").style.display = 'block';                            
                document.getElementById("RangeRow").style.display = 'none';
                break;            
        }
    }      
}

function initial()
{

    var setuphr;
    var setupmin;
    var setupap;
    var teardownhr;
    var teardownmin;
    var teardownap;
    //FB 2634
//    document.getElementById("hdnSetupTime").value = document.getElementById("SetupTime_Text").value;
//    document.getElementById("hdnTeardownTime").value = document.getElementById("TeardownTime_Text").value;
    
	if (document.getElementById("Recur").value=="") {
    	
	//FB 2588
	    ChangeTimeFormat("D")
		timeMin = document.getElementById("hdnStartTime").value.split(":")[1].split(" ")[0] ; 
		timeDur = getConfDurNET(document.frmSettings2,dFormat,document.getElementById("hdnStartTime").value,document.getElementById("hdnEndTime").value);//FB 2588
		
		if('<%=Session["timeFormat"].ToString()%>' == '0' || '<%=Session["timeFormat"].ToString()%>' == '2')//FB 2588
		    tmpstr = document.getElementById("lstConferenceTZ").options[document.getElementById("lstConferenceTZ").selectedIndex].value + "&" + document.getElementById("hdnStartTime").value.split(":")[0] + "&" + timeMin + "&" + "-1" + "&" + timeDur + "#"
		else
		    tmpstr = document.getElementById("lstConferenceTZ").options[document.getElementById("lstConferenceTZ").selectedIndex].value + "&" + document.getElementById("confStartTime_Text").value.split(":")[0] + "&" + timeMin + "&" + document.getElementById("confStartTime_Text").value.split(" ")[1] + "&" + timeDur + "#"
		
		tmpstr += "1&1&-1&-1&-1&-1&-1&-1&-1&-1&-1&-1&-1&-1&-1&-1&-1#"
		tmpstr += document.getElementById("confStartDate").value + "&1&-1&-1";

 		document.frmSettings2.RecurValue.value = tmpstr;
		document.frmSettings2.RecurPattern.value = "1&1&-1&-1&-1&-1&-1&-1&-1&-1&-1&-1&-1&-1&-1&-1&-1";
		
	}
	else
	{
	    setuptime = document.getElementById("hdnBufferStr").value.split("&")[0];
	    teardowntime = document.getElementById("hdnBufferStr").value.split("&")[1];
	    //FB 2634
//	    if(document.getElementById("SetupTime_Text"))
//	        document.getElementById("SetupTime_Text").value = setuptime;
//	    
//	    if(document.getElementById("TeardownTime_Text"))
//	        document.getElementById("TeardownTime_Text").value = teardowntime;
	}
     
    //FB 2266
    if(document.frmSettings2.RecurValue.value == "")
	    document.frmSettings2.RecurValue.value = document.getElementById("Recur").value; 

	rpstr = AnalyseRecurStr(document.frmSettings2.RecurValue.value);
	//FB 1635 starts
	if('<%=Session["timeFormat"].ToString()%>' == '0' || '<%=Session["timeFormat"].ToString()%>' == '2')//FB 2588
	{
	    if(atint[1] < 12 && atint[3] == "PM")
             atint[1] = atint[1] + 12;
    }
	//FB 1635 Ends
	
	if(atint[1] < 10)
	    atint[1] = "0" + atint[1];
	    
	//FB 1715
	if(atint[2] < 10)
	    atint[2] = "0" + atint[2];
	
	//FB 1799
	if('<%=Session["timeFormat"].ToString()%>' == '0' || '<%=Session["timeFormat"].ToString()%>' == '2')//FB 2588
	    startstr = (atint[1] == "12" && atint[3] == "AM"? "00" : atint[1]) + ":" + (atint[2]=="0" ? "00" : atint[2]);
	else
	    startstr = atint[1] + ":" + (atint[2]=="0" ? "00" : atint[2]) + " " + atint[3];
	    	
    //FB 2634
//	if(document.getElementById("confStartTime_Text"))
//	    document.getElementById("confStartTime_Text").value = startstr;
	
	var actualDur = atint[4];
    var setup = document.getElementById("SetupDuration").value;
    var teardown = document.getElementById("TearDownDuration").value;
    
    if(document.getElementById("Recur").value != "")
        actualDur = parseInt(actualDur,10) - (parseInt(setup,10) + parseInt(teardown,10));
        
	if (document.frmSettings2.RecurDurationhr) {
		set_select_field (document.frmSettings2.RecurDurationhr, parseInt(actualDur/60, 10) , true);
		set_select_field (document.frmSettings2.RecurDurationmi, actualDur%60, true);
	}

	document.frmSettings2.RecurPattern.value = rpstr;	
	if(document.frmSettings2.RecurType)
	    document.frmSettings2.RecurType[rpint[0]-1].checked = true;

    document.frmSettings2.Occurrence.value ="";
    document.frmSettings2.EndDate.value ="";
   
	if (rpint[0] != 5) {
		document.frmSettings2.StartDate.value = rrint[0];

	
		switch (rrint[1]) 
		{
			case 1:
		        document.frmSettings2.EndType.checked = true;
				break;
			case 2:
		        document.frmSettings2.REndAfter.checked = true;
				document.frmSettings2.Occurrence.value = rrint[2];
				break;
			case 3:
		        document.frmSettings2.REndBy.checked = true;
				document.frmSettings2.EndDate.value = rrint[3];
				break;
			default:
				alert(EN_36);
				break;
		}
	}
	
}

function recurTimeChg()
{
   //FB 2588
    ChangeTimeFormat("D")   

    var aryStart = document.getElementById("hdnStartTime").value.split(" ");
    	
	rshr = aryStart[0].split(":")[0];
	rsmi = aryStart[0].split(":")[1];
	rsap = aryStart[1];

	if (document.frmSettings2.RecurDurationhr) {
		rdhr = parseInt(document.frmSettings2.RecurDurationhr.value, 10);
		rdmi = parseInt(document.frmSettings2.RecurDurationmi.value, 10);
		recurduration = rdhr * 60 + rdmi;
        if (recurduration > (maxDuration *60))
            alert(EN_314);
		document.frmSettings2.EndText.value = calEnd(calStart(rshr, rsmi, rsap), recurduration);
	}
	
	if (document.frmSettings2.RecurEndhr) {
		rehr = document.frmSettings2.RecurEndhr.options[document.frmSettings2.RecurEndhr.selectedIndex].value;
		remi = document.frmSettings2.RecurEndmi.options[document.frmSettings2.RecurEndmi.selectedIndex].value;
		reap = document.frmSettings2.RecurEndap.options[document.frmSettings2.RecurEndap.selectedIndex].value;
		
		document.frmSettings2.DurText.value = calDur(document.getElementById("confStartDate").value, document.getElementById("confEndDate").value, rshr, rsmi, rsap, rehr, remi, reap, 1);
	}
}

</script>

<%--daily Script--%>

<script type="text/javascript">
    function summarydaily() {
        dg = Trim(document.frmSettings2.DayGap.value);
        rp = "1&";
        rp += (document.frmSettings2.DEveryDay.checked) ? ("1&" + ((dg == "") ? "-1" : dg)) : "2&-1";
        rp += "&-1&-1&-1&-1&-1&-1&-1&-1&-1&-1&-1&-1&-1&-1";

        document.frmSettings2.RecurPattern.value = rp;
    }

    function initialdaily(rp) {
        var rpary = (rp).split("&");
        if (rpary[0] == 1) {
            if (!isNaN(rpary[2]))
                dg = parseInt(rpary[2], 10);
            if ((dg == -1) || isNaN(dg))
                document.frmSettings2.DayGap.value = "";
            else
                document.frmSettings2.DayGap.value = dg;
            if (!isNaN(rpary[1]))
                dt = parseInt(rpary[1], 10);
            if ((dt == -1) || isNaN(dt))
                rpary[1] = 1;

            if (rpary[1] == 1)
                document.frmSettings2.DEveryDay.checked = true;
            else if (rpary[1] == 2)
                document.frmSettings2.DWeekDay.checked = true;
        }
        else {
            document.frmSettings2.DayGap.value = "";
            document.frmSettings2.DEveryDay.checked = true;
        }
    }

</script>

<%--Weekly Script--%>

<script type="text/javascript">

    function summaryweekly() {
        wg = Trim(document.frmSettings2.WeekGap.value);
        rp = "2&-1&-1&";
        rp += ((wg == "") ? "" : wg) + "&";

        var elementRef = document.getElementById("WeekDay");
        var checkBoxArray = elementRef.getElementsByTagName('input');
        var checkedValues = '';
        for (var i = 0; i < checkBoxArray.length; i++)
            rp += checkBoxArray[i].checked ? ((i + 1) + ",") : ""

        if (rp.substr(rp.length - 1, 1) == ",")
            rp = rp.substr(0, rp.length - 1);

        rp += "&-1&-1&-1&-1&-1&-1&-1&-1&-1&-1&-1&-1";

        document.frmSettings2.RecurPattern.value = rp;
    }

    function initialweekly(rp) {
        var elementRef = document.getElementById("WeekDay");
        var checkBoxArray = elementRef.getElementsByTagName('input');
        rpary = (rp).split("&");
        if (rpary[0] == 2) {
            if (!(isNaN(rpary[3])))
                wg = parseInt(rpary[3], 10);
            if ((wg == -1) || isNaN(wg))
                document.frmSettings2.WeekGap.value = "1";
            else
                document.frmSettings2.WeekGap.value = wg;
            if (rpary[4] != -1) {
                wdary = (rpary[4]).split(",");

                for (i = 0; i < wdary.length; i++)
                    checkBoxArray[parseInt(wdary[i]) - 1].checked = true
            }
        }
        else {
            document.frmSettings2.WeekGap.value = "1";

            for (var i = 0; i < checkBoxArray.length; i++) {
                if (checkBoxArray[i].checked == true)
                    checkBoxArray[i].checked = false;
            }
        }
    }
</script>

<%--Monthly Script--%>

<script type="text/javascript">

    function summarymonthly() {
        mdn = Trim(document.frmSettings2.MonthDayNo.value);
        mg1 = Trim(document.frmSettings2.MonthGap1.value);
        mg2 = Trim(document.frmSettings2.MonthGap2.value);
        rp = "3&-1&-1&-1&-1&";
        rp += (document.frmSettings2.MEveryMthR1.checked) ? ("1&" + ((mdn == "") ? "-1" : mdn) + "&" + ((mg1 == "") ? "-1" : mg1) + "&-1&-1&-1")
		: ("2&-1&-1&" + (document.frmSettings2.MonthWeekDayNo.selectedIndex + 1) + "&" + (document.frmSettings2.MonthWeekDay.selectedIndex + 1) + "&" + ((mg2 == "") ? "-1" : mg2));
        rp += "&-1&-1&-1&-1&-1&-1";

        document.frmSettings2.RecurPattern.value = rp;
    }

    function initialmonthly(rp) {
        rpary = (rp).split("&");
        if (rpary[0] == 3) {
            for (i = 5; i < 11; i++) {
                if (!isNaN(rpary[i]))
                    rpary[i] = parseInt(rpary[i], 10);
                else
                    rpary[i] = -1;
            }

            switch (rpary[5]) {
                case 1:
                    document.frmSettings2.MEveryMthR1.checked = true;
                    document.frmSettings2.MonthDayNo.value = (rpary[6] == -1) ? "" : rpary[6];
                    document.frmSettings2.MonthGap1.value = (rpary[7] == -1) ? "" : rpary[7];
                    document.frmSettings2.MonthWeekDayNo[0].checked = true;
                    document.frmSettings2.MonthWeekDay[0].checked = true;
                    document.frmSettings2.MonthGap2.value = "";
                    break;
                case 2:
                    document.frmSettings2.MEveryMthR2.checked = true;
                    document.frmSettings2.MonthDayNo.value = "";
                    document.frmSettings2.MonthGap1.value = "";
                    if (rpary[8] == -1)
                        document.frmSettings2.MonthWeekDayNo[0].selected = true;
                    else
                        document.frmSettings2.MonthWeekDayNo[rpary[8] - 1].selected = true;
                    if (rpary[9] == -1)
                        document.frmSettings2.MonthWeekDay[0].selected = true;
                    else
                        document.frmSettings2.MonthWeekDay[rpary[9] - 1].selected = true;
                    document.frmSettings2.MonthGap2.value = (rpary[10] == -1) ? "" : rpary[10];
                    break;
            }
        }
        else {
            document.frmSettings2.MEveryMthR1.checked = true;
            document.frmSettings2.MonthDayNo.value = "";
            document.frmSettings2.MonthGap1.value = "";
            document.frmSettings2.MonthWeekDayNo[0].checked = true;
            document.frmSettings2.MonthWeekDay[0].checked = true;
            document.frmSettings2.MonthGap2.value = "";
        }
    }
</script>

<%--Yearly Script--%>

<script type="text/javascript">

    function summaryyearly() {
        ymd = Trim(document.frmSettings2.YearMonthDay.value);
        rp = "4&-1&-1&-1&-1&-1&-1&-1&-1&-1&-1&";
        rp += (document.frmSettings2.YEveryYr1.checked) ? ("1&" + (document.frmSettings2.YearMonth1.selectedIndex + 1) + "&" + ((ymd == "") ? "-1" : ymd) + "&-1&-1&-1")
		: ("2&-1&-1&" + (document.frmSettings2.YearMonthWeekDayNo.selectedIndex + 1) + "&" + (document.frmSettings2.YearMonthWeekDay.selectedIndex + 1) + "&" + (document.frmSettings2.YearMonth2.selectedIndex + 1));

        document.frmSettings2.RecurPattern.value = rp;
    }

    function initialyearly(rp) {
        rpary = (rp).split("&");
        if (rpary[0] == 4) {
            for (i = 11; i < 17; i++) {
                if (!isNaN(rpary[i]))
                    rpary[i] = parseInt(rpary[i], 10);
                else
                    rpary[i] = -1;
            }
            switch (rpary[11]) {
                case 1:
                    document.frmSettings2.YEveryYr1.checked = true;
                    if (rpary[12] == -1)
                        document.frmSettings2.YearMonth1[0].selected = true;
                    else
                        document.frmSettings2.YearMonth1[rpary[12] - 1].selected = true;
                    document.frmSettings2.YearMonthDay.value = (rpary[13] == -1) ? "" : rpary[13];
                    document.frmSettings2.YearMonthWeekDayNo[0].selected = true;
                    document.frmSettings2.YearMonthWeekDay[0].selected = true;
                    document.frmSettings2.YearMonth2[0].selected = true;
                    break;
                case 2:
                    document.frmSettings2.YEveryYr2.checked = true;
                    document.frmSettings2.YearMonth1[0].selected = true;
                    document.frmSettings2.YearMonthDay.value = "";
                    if (rpary[14] == -1)
                        document.frmSettings2.YearMonthWeekDayNo[0].selected = true;
                    else
                        document.frmSettings2.YearMonthWeekDayNo[rpary[14] - 1].selected = true;
                    if (rpary[15] == -1)
                        document.frmSettings2.YearMonthWeekDay[0].selected = true;
                    else
                        document.frmSettings2.YearMonthWeekDay[rpary[15] - 1].selected = true;
                    if (rpary[16] == -1)
                        document.frmSettings2.YearMonth2[0].selected = true;
                    else
                        document.frmSettings2.YearMonth2[rpary[16] - 1].selected = true;
                    break;
            }
        }
        else {
            document.frmSettings2.YEveryYr1.checked = true;
            document.frmSettings2.YearMonth1[0].selected = true;
            document.frmSettings2.YearMonthDay.value = "";
            document.frmSettings2.YearMonthWeekDayNo[0].selected = true;
            document.frmSettings2.YearMonthWeekDay[0].selected = true;
            document.frmSettings2.YearMonth2[0].selected = true;
        }
    }
</script>

<%--Custom Script--%>

<script type="text/javascript" language="JavaScript">

    function removedate(cb) {
        if (cb.selectedIndex != -1) {
            cb.options[cb.selectedIndex] = null;
        }
        cal.refresh();
    }

    function initialcustomly(seldates) {
        if (seldates != "") {
            seldatesary = seldates.split("&");

            cb = document.getElementById("CustomDate");
            if (seldatesary[0].length > 2) {
                for (var i = 0; i < seldatesary.length; i++)
                    chgOption(cb, seldatesary[i], seldatesary[i], false, false)

                if (cal)
                    cal.refresh();
            }
        }
    }

    function summarycustomly() {
        var selecteddate = "";
        dFormat = document.getElementById("HdnDateFormat").value;
        SortDates();
        cb = document.getElementById("CustomDate");
        for (i = 0; i < cb.length; i++)
            selecteddate += cb.options[i].value + "&";

        document.frmSettings2.CutomDates.value = (selecteddate == "") ? "" : selecteddate.substring(0, selecteddate.length - 1);
    }

    function isOverInstanceLimit(cb) {
        csl = parseInt("<%=CustomSelectedLimit%>");

        if (!isNaN(csl)) {
            if (cb.length >= csl) {
                alert(EN_211)
                return true;
            }
        }

        return false;
    }

    function SortDates() {
        var temp;

        datecb = document.frmSettings2.CustomDate;

        var dateary = new Array();

        for (var i = 0; i < datecb.length; i++) {
            dateary[i] = datecb.options[i].value;

            dateary[i] = ((parseInt(dateary[i].split("/")[0], 10) < 10) ? "0" + parseInt(dateary[i].split("/")[0], 10) : parseInt(dateary[i].split("/")[0], 10)) + "/" + ((parseInt(dateary[i].split("/")[1], 10) < 10) ? "0" + parseInt(dateary[i].split("/")[1], 10) : parseInt(dateary[i].split("/")[1], 10)) + "/" + (parseInt(dateary[i].split("/")[2], 10));
        }

        for (i = 0; i < dateary.length - 1; i++)
            for (j = i + 1; j < dateary.length; j++)
            if (mydatesort(dateary[i], dateary[j]) > 0) {
            temp = dateary[i];
            dateary[i] = dateary[j];
            dateary[j] = temp;
        }


        for (var i = 0; i < dateary.length; i++) {
            datecb.options[i].text = dateary[i];
            datecb.options[i].value = dateary[i];
        }

        return false;
    }

    //-->
</script>

<%--Submit Recurence --%>

<script type="text/javascript">

function SubmitRecurrence()
{
    var chkrecurrence = document.getElementById("chkRecurrence");
    
    
    if (typeof(Page_ClientValidate) == 'function') 
    if (!Page_ClientValidate())
    {
        DataLoading(0);
        return false;
    }
    
    if(!CheckParty())
        return false;
        
    if(chkrecurrence != null && chkrecurrence.checked == true)
    {
        if (validateConfDuration())  
        {
            if(summary())
                return true;
        }
        return false;        
    }
}

function validateDurationHr()
{
    var obj = document.getElementById("RecurDurationhr");
    
    if (obj.value == "") 
         obj.value = "0";
         
    if (isNaN(obj.value)) 
    {
        //alert(EN_314);
        obj.focus();    
        return false;
    }

    var maxdur = '<%= Application["MaxConferenceDurationInHours"] %>';
    
    if(maxdur == "")
        maxdur = "120";
    
    if (obj.value < 0 || eval(obj.value) > eval(maxdur))
    {
        alert(EN_314);
        obj.focus();    
        return false;
    }
    return true;
}

function validateDurationMi()
{
    var obj = document.getElementById("RecurDurationmi");
    if (obj.value == "") 
        obj.value = "0";
    if (isNaN(obj.value))
    {
        alert(EN_314);
        obj.focus();
        return false;
    }
    return true;
}

function validateConfDuration()
{
    var obj = document.getElementById("RecurDurationhr");
    var obj1 = document.getElementById("RecurDurationmi");
    if (obj.value == "") 
         obj.value = "0";
         
    if (obj1.value == "") 
        obj1.value = "0";
         
    if (isNaN(obj.value)) 
    {
        alert(EN_314);
        return false;
    }
    if (isNaN(obj1.value)) 
    {
        alert(EN_314);
        return false;
    }

    var maxdur = '<%= Application["MaxConferenceDurationInHours"] %>';
    if(maxdur == "")
        maxdur = "24";

    var totDur = parseInt(obj.value) * 60;
    totDur = totDur + parseInt(obj1.value);
    
    if (totDur < 0 || totDur > eval(maxdur*60))
    {
        alert(EN_314);
        return false;
    }
  
    return true;
}

function summary()
{
     SetRecurBuffer();
     switch(document.frmSettings2.RecPattern.value)
     {
        case "1":
		    summarydaily();
		    break;
		case "2":	
		    summaryweekly();
		    break;
        case "3":
		    summarymonthly();
		    break;
	    case "4":
		    summaryyearly();
		    break;
        case "5":
		    summarycustomly();
		    if(document.frmSettings2.CutomDates.value != "")
		    {
		        var cuDates = document.frmSettings2.CutomDates.value.split("&");
		        if(cuDates.length <=1)
		        {
		            alert("A recurring conference must contain at least (2) instances. Please modify recurrence pattern before submitting conference.");
		            return false;   
		        }
		    }
		    break;
     }
    
     if(document.frmSettings2.RecPattern.value != "5" && document.frmSettings2.REndAfter.checked)
     {
       if(document.frmSettings2.Occurrence.value <= 1)
       {
           alert("A recurring conference must contain at least (2) instances. Please modify recurrence pattern before submitting conference.");
           document.frmSettings2.Occurrence.focus();
           return false;   
       }
     }

	//FB 2634
	//var aryStart = document.getElementById("confStartTime_Text").value.split(" ");
	//FB 2588
    ChangeTimeFormat("D")
	var cstartdate = new Date(GetDefaultDate(document.getElementById("confStartDate").value,'<%=format%>') + " "
            + document.getElementById("hdnStartTime").value);
                
    var dura = parseInt(document.getElementById("SetupDuration").value,10)    
	var sttime = setCDuration(cstartdate, -dura)
	var sttime1 = getCTime(sttime);
	var aryStart = sttime1.split(" ");
	
	sh = aryStart[0].split(":")[0];
	sm = aryStart[0].split(":")[1];
	ss = aryStart[1];
	tz = document.getElementById("<%=lstConferenceTZ.ClientID%>").value;
	
	if('<%=Session["timeFormat"].ToString()%>' == '0' || '<%=Session["timeFormat"].ToString()%>' == '2') //FB 2588
	{
	    if(sh != "")
	    { 
	        if(eval(sh) >= 12)
	            ss = "PM";
	        else
	            ss = "AM";
	    }
	}
	//FB 2634
//	var aryStup = document.getElementById("confStartTime_Text").value.split(" ");
//	
//	setuphr = aryStup[0].split(":")[0];
//	setupmi = aryStup[0].split(":")[1];
//	setupap = aryStup[1];
//FB 2634
//	var aryTear = document.getElementById("confEndTime_Text").value.split(" ");
//	tearhr = aryTear[0].split(":")[0];
//	tearmi = aryTear[0].split(":")[1];
//	tearap = aryTear[1];
//	
//	if("<%=Session["timeFormat"].ToString()%>" == '0')
//	{ 
//	    startTime = sh + ":" + (sm =="0" ? "00" : sm);
//	    setupTime = setuphr + ":" + (setupmi =="0" ? "00" : setupmi);
//        teardownTime =  tearhr + ":" + (tearmi =="0" ? "00" : tearmi);
//    }
//	else
//	{
//	    startTime = sh + ":" + (sm =="0" ? "00" : sm) + " " + ss;
//	    setupTime = setuphr + ":" + (setupmi =="0" ? "00" : setupmi) + " " + setupap;
//        teardownTime =  tearhr + ":" + (tearmi =="0" ? "00" : tearmi) + " " + tearap;
//    }
//    
//    var insStDate = '';
//    if (document.frmSettings2.RecPattern.value == "5")
//    {
//        var cb = document.getElementById("CustomDate");

//	    if(cb.length > 0)
//	    {
//		    insStDate = cb.options[0].value;
//        }
//    }
//    else
//    {
//        insStDate = document.frmSettings2.StartDate.value;
//    }
//    startdate = insStDate + " " + startTime;
//   
//    var obj = document.getElementById("RecurDurationhr");
//    var obj1 = document.getElementById("RecurDurationmi");
//    
//    var totDur = parseInt(obj.value) * 60;
//    totDur = totDur + parseInt(obj1.value);
//    
//    if(totDur > 1440)
//    {
//        alert(EN_314);
//        return false;
//    }
//    
//    var confEndDt = '';
//    var sysdate = dateAddition(startdate,"m",totDur);
//    var dateP = sysdate.getDate();
//	
//	var monthN = sysdate.getMonth() + 1;
//	var yearN = sysdate.getFullYear();
//	var hourN = sysdate.getHours();
//	var minN = sysdate.getMinutes();
//	var secN = sysdate.getSeconds();
//	var timset = 'AM';
//	
//	if("<%=Session["timeFormat"].ToString()%>" == '0')
//	{
//	    timset = '';
//	}
//	else
//	{
//	    if(hourN == 12)
//	    {
//	        timset = 'PM';
//	    }
//	    else if( hourN > 12)
//	    {
//	         timset = 'PM';
//	         hourN = hourN - 12;
//	    }
//	    else if(hourN == 0)
//	    {
//	        timset = "AM";
//	        hourN = 12;
//	    }
//	 }
//	
//	var confDtAlone = monthN + "/" + dateP + "/" + yearN;
//	
//	confEndDt = monthN + "/" + dateP + "/" + yearN + " "+ hourN + ":" + minN + ":" + secN +" "+ timset;
//	if(document.getElementById("chkEnableBuffer").checked == true)	
//	{
//        setupDate = insStDate + " " + setupTime;
//        if(Date.parse(setupDate) < Date.parse(startdate))
//        {
//            setupDate = confDtAlone + " " + setupTime;
//        }
//        
//        teardownDate = confDtAlone + " " + teardownTime;
//        
//        if(Date.parse(confEndDt) < Date.parse(teardownDate))
//        {
//            teardownDate = insStDate + " " + teardownTime;
//        }
//      
//        if(Date.parse(setupDate) < Date.parse(startdate))
//        {
//            alert(EN_316);
//            return false;
//        }

//        if( (Date.parse(teardownDate) < Date.parse(setupDate)) || (Date.parse(teardownDate) < Date.parse(startdate)) || (Date.parse(teardownDate) > Date.parse(confEndDt)))
//        {
//             alert(EN_317);
//             return false;
//        }
//      
//        var setupDurMin = parseInt(Date.parse(setupDate) - Date.parse(startdate)) / (1000 * 60);
//        var tearDurMin = parseInt(Date.parse(confEndDt) - Date.parse(teardownDate)) / (1000 * 60);
//      }
//    else
//    {
//        setupDate = startdate;
//        teardownDate = confEndDt;
//    }  
//    
//    if(isNaN(setupDurMin))
//        setupDurMin = 0;
//    
//    if(isNaN(tearDurMin))
//        tearDurMin = 0;
//        
//    if(setupDurMin >0 || tearDurMin > 0)
//    {
//        if( (totDur -(setupDurMin + tearDurMin)) < 15)
//        {
//            alert(EN_314);
//            return false;
//        }
//    }
//    
//    document.frmSettings2.hdnBufferStr.value = setupTime + "&" + teardownTime;
//	document.frmSettings2.hdnSetupTime.value = setupDurMin;
//	document.frmSettings2.hdnTeardownTime.value = tearDurMin;
	
	if (document.frmSettings2.RecurDurationhr)
		dr = parseInt(document.frmSettings2.RecurDurationhr.value, 10) * 60 + parseInt(document.frmSettings2.RecurDurationmi.value, 10);
    
    //FB 2634
	if (dr < 15) 
    {
	    alert(EN_31);
	    return (false);		
	}
    dr = dr + parseInt(document.getElementById("SetupDuration").value,10) + parseInt(document.getElementById("TearDownDuration").value,10);
	
	if (document.frmSettings2.RecurEndhr) {
		rehr = document.frmSettings2.RecurEndhr.options[document.frmSettings2.RecurEndhr.selectedIndex].value;
		remi = document.frmSettings2.RecurEndmi.options[document.frmSettings2.RecurEndmi.selectedIndex].value;
		reap = document.frmSettings2.RecurEndap.options[document.frmSettings2.RecurEndap.selectedIndex].value;
		
		dr = calDur(sh, sm, ss, rehr, remi, reap, 0);
	}
	if ( dr < 15 ) {
		alert(EN_31);
		
		if (document.frmSettings2.RecurDurationhr)
			document.frmSettings2.RecurDurationhr.focus();
		if (document.frmSettings2.RecurEndhr)
			document.frmSettings2.RecurEndhr.focus();
			
		return (false);		
	}

	rv = tz + "&" + sh + "&" + sm + "&" + ss + "&" + dr + "#";
	recurrange = document.frmSettings2.StartDate.value + "&" + (document.frmSettings2.EndType.checked ? "1&-1&-1" : ( document.frmSettings2.REndAfter.checked ? ("2&" + document.frmSettings2.Occurrence.value + "&-1") : (document.frmSettings2.REndBy.checked ? ("3&-1&" + document.frmSettings2.EndDate.value) : "-1&-1&-1") ));
	rv += (document.frmSettings2.RecPattern.value == 5) ? ("5#" + document.frmSettings2.CutomDates.value) : (document.frmSettings2.RecurPattern.value + "#" + recurrange);
	if (frmRecur_Validator(document.frmSettings2.RecurPattern.value, recurrange, (document.frmSettings2.RecPattern.value == 5)?true:false)) {
		document.getElementById("Recur").value = rv;
	
		if (document.frmSettings2.EndText)
			endvalue = document.frmSettings2.EndText.value;
		if (document.frmSettings2.RecurEndhr)
			endvalue = document.frmSettings2.RecurEndhr.value + ":" + document.frmSettings2.RecurEndmi.value + " " + document.frmSettings2.RecurEndap.value;
		document.getElementById("RecurringText").value = recur_discription(rv, endvalue, document.frmSettings2.TimeZoneText.value, document.frmSettings2.StartDate.value,"<%=Session["timeFormat"].ToString()%>","<%=Session["timezoneDisplay"].ToString()%>");
		//FB 2588
		if('<%=Session["timeFormat"].ToString()%>' == '2')
		    document.getElementById("RecurringText").value = document.getElementById("RecurringText").value.replace(" AM","Z").replace(" PM","Z").replace(":","")
		isRecur();
		BtnCheckAvailDisplay ();
		return true;
	}
}

function frmRecur_Validator(rp, rr, isCustomly)
{
	if (parseInt(document.frmSettings2.Occurrence.value) > parseInt("<%=Application["confRecurrence"]%>"))
	{
		alert("Maximum limit of " + "<%=Application["confRecurrence"]%>" + " instances/occurrences in the recurring series.");
		return false;
	}
	
	if (isCustomly) {
		if (document.frmSettings2.CutomDates.value == "") {
			alert(EN_193)
			return false;
		} else
			return true;
	}

	if (chkPattern(rp))
		if (chkRange(rr))
			return true;
		else
			return false;
	else
		return false;
}

function chkPattern(rp)
{
	rpary = rp.split("&");
	
	switch (rpary[0]) {
		case "1":
			for (i=3; i<rpary.length; i++) {
				if  (rpary[i]!= "-1") {
					alert(EN_37);
					return false;
				}
			}

			switch (rpary[1]) {
				case "1":
					rst = isPositiveInt (rpary[2], "interval");
					if (rst == 1)
						return true;
					else
						return false;
					break;
				case "2":
					if (rpary[2] == "-1")
						return true;						
					else {
						alert(EN_37);
						return false;						
					}				
					break;
				default:
					alert(EN_38);
					return false;
					break;
			}
			break;

		case "2":
			for (i=1; i<3; i++) {
				if  (rpary[i]!= "-1") {
					alert(EN_37);
					return false;
				}
			}
			for (i=5; i<rpary.length; i++) {
				if  (rpary[i]!= "-1") {
					alert(EN_37);
					return false;
				}
			}
			rst = isPositiveInt (rpary[3], "weeks interval");
			if (rst == 1) {
				if (rpary[4]!="")
					return true;
				else {
					alert(EN_107);
					return false;
				}
			}
			break;
			
		case "3":
			for (i=1; i<5; i++) {
				if  (rpary[i]!= "-1") {
					alert(EN_37);
					return false;
				}
			}
			for (i=11; i<rpary.length; i++) {
				if  (rpary[i]!= "-1") {
					alert(EN_37);
					return false;
				}
			}
			
			switch (rpary[5]) {
				case "1":
					for (i=8; i<11; i++) {
						if  (rpary[i]!= "-1") {
							alert(EN_37);
							return false;
						}
					}
					if ( (isPositiveInt (rpary[6], "days interval") == 1) && (isPositiveInt (rpary[7], "months interval") == 1) )
						if ( isMonthDayNo(parseInt(rpary[6], 10)) )
							return true;
						else
							return false;
					else
						return false;
					break;
				case "2":
					for (i=6; i<8; i++) {
						if  (rpary[i]!= "-1") {
							alert(EN_37);
							return false;
						}
					}
					if (isPositiveInt(rpary[10], "months interval") == "1")
						return true;						
					else {
						return false;						
					}				
					break;
				default:
					alert(EN_38);
					return false;
					break;
			}
			break;
			
		case "4":
			for (i=1; i<11; i++) {
				if  (rpary[i]!= "-1") {
					alert(EN_37);
					return false;
				}
			}
			switch (rpary[11]) {
				case "1":
					for (i=14; i<rpary.length; i++) {
						if  (rpary[i]!= "-1") {
							alert(EN_37);
							return false;
						}
					}
					if (isPositiveInt (rpary[13], "date of the month") == 1)
						if ( isYearMonthDay(parseInt(rpary[12], 10), parseInt(rpary[13], 10)) )
							return true;
						else
							return false;
					else
						return false;						
					break;
				case "2":
					for (i=12; i<14; i++) {
						if  (rpary[i]!= "-1") {
							alert(EN_37);
							return false;
						}
					}
					return true;
					break;
				default:
					alert(EN_38);
					return false;
					break;
			}
			break;
		default:
			alert(EN_38);
			return false;
			break;
	}
}

var british = false;

function chkRange(rr)
{
	rrary = rr.split("&");
    
    if('<%=Session["FormatDateType"]%>' == 'dd/MM/yyyy')
        british = true;
    
    var dDate = GetDefaultDate(rrary[0],dFormat);
     
	if ( (!isValidDate(rrary[0])) || ( caldatecompare(dDate, servertoday) == -1 ) ) {		// check start time is reasonable future time
		alert(EN_74);
		document.frmSettings2.StartDate.focus();
		return (false);
	}

	switch (rrary[1]) {
		case "1":
			if ( (rrary[2]!= "-1") && (rrary[3]!= "-1") ) {
				alert(EN_38);
				return false;
			} else 
				return true;
			break;
		case "2":
			if (rrary[3]!= "-1") {
				alert(EN_38);
				return false;
			} else {
				if ( isPositiveInt(rrary[2], "times of occurrences")==1 )
					return true;
				else {
					document.frmSettings2.Occurrence.focus();
					return false;
				}
			}
			break;
		case "3":
			if (rrary[2]!= "-1") {
				alert(EN_38);
				return false;
			} else {
				if ( (!isValidDate(rrary[3])) ||  (caldatecompare(GetDefaultDate(rrary[3],dFormat), servertoday)== -1) ) {
					alert(EN_108);
					document.frmSettings2.EndDate.focus();
					return false;
				} else
				{
				    var fstdate = rrary[0];  //FB 2366
			        var snddate = rrary[3];
			        if(!british)
				    {
				        fstdate = GetDefaultDate(rrary[0],"dd/MM/yyyy");
				        snddate = GetDefaultDate(rrary[3],"dd/MM/yyyy");				         
				    }
					if ( !dateIsBefore(fstdate,snddate) ) {
						alert(EN_109);
						document.frmSettings2.EndDate.focus();
						return false;
					} else
						return true;
				}
			}
			return false;
			break;
		default:
			alert(EN_38);
			return false;
			break;
	}	
}

function BtnCheckAvailDisplay ()
{		
	if ( (e = document.getElementById("btnCheckAvailDIV")) != null ) {
		e.style.display = (document.getElementById("Recur").value=="") ? "" : "none";
	}
}
</script>

<script type="text/javascript">

    function removerecur() {
        document.getElementById("Recur").value = "";
        document.getElementById("RecurringText").value = "";
        document.getElementById("hdnRecurValue").value = "";
        document.getElementById('RecurValue').value = "";
        isRecur();
        initial();

        return false;
    }
    function fnRemoveFiles() {
        var lblflelist = document.getElementById("lblFileList");
        var lblCtrl = document.getElementById(fnRemoveFiles.arguments[0]);

        if (lblflelist != null && lblCtrl != null) {
            lblflelist.innerText = lblflelist.innerText.replace(lblCtrl.innerText + "; ", "");
        }

    }

    function clearError(controlID, clearId) {
        var ErrorLabel = document.getElementById(clearId);
        if (controlID.value != "") {
            ErrorLabel.innerText = "";
        }

    }

    function fnCheckSpecialCharacters(ID) {
        if (document.getElementById(ID) != null)//FB 2509
        {
            var controlID = document.getElementById(ID);
            var specialChars = "&<>+'dD";

            for (var i = 0; i < controlID.value.length; i++) {
                if (specialChars.indexOf(controlID.value.charAt(i)) != -1) {
                    controlID.focus();
                    return false;
                }
            }
        }
        return true;
    }

    function isNumber(ID) {
        var numbers = '0123456789';
        var controlID = document.getElementById(ID);

        for (i = 0; i < controlID.value.length; i++) {
            if (numbers.indexOf(controlID.value.charAt(i), 0) == -1) {
                controlID.focus();
                return false;
            }
        }
        return true;
    }

    function fnCheckRequiredField() {
        var isRequired = true;                
        if (document.getElementById("txtAudioDialNo") != null) {//FB 2509
            if (document.getElementById("txtAudioDialNo").value == "")//@@@@@
            {
                document.getElementById("lblError2").innerText = "Required";
                document.getElementById("txtAudioDialNo").focus();
                //isRequired=false;
                return false;
            }
        }
        else
            return true;

        if (document.getElementById("txtConfCode").value == "") {
            document.getElementById("lblError3").innerText = "Required";
            document.getElementById("txtConfCode").focus();
            isRequired = false;
        }

        if (document.getElementById("txtLeaderPin").value == "") {
            document.getElementById("lblError4").innerText = "Required";
            document.getElementById("txtLeaderPin").focus();
            isRequired = false;
        }
        if (isRequired == false) {
            return false;
        }
        return true;
    }

    //FB 2634
function fnValidation() 
{
    //FB 2588
    ChangeTimeFormat("D")
    //Remove Date Validation
    var sDate = Date.parse(document.getElementById("confStartDate").value + " " + document.getElementById("hdnStartTime").value); 
    var eDate = Date.parse(document.getElementById("confEndDate").value + " " + document.getElementById("hdnEndTime").value); 

    var actEndate = new Date(sDate);
    var timeMins = 15;
    actEndate.setMinutes(actEndate.getMinutes() + parseInt(timeMins));

    var errStr = "";
    if (eDate <= sDate && document.getElementById("chkRecurrence").checked == false) {// FB 2692
        errStr = "End Date should be greater than Start Date";
        document.getElementById("confEndTime_Text").focus();
    }
    else if (eDate < actEndate && document.getElementById("chkRecurrence").checked == false) {// FB 2692
        errStr = "Invalid Duration.Conference duration should be minimum of 15 mins";
        document.getElementById("confEndTime_Text").focus();
    }

    if (errStr != "") {
        alert(errStr);
        return false;
    }
	//FB 2997 starts
    /*if (document.getElementById("ConferenceName").value == "") {
        document.getElementById("lblConferenceNameError").innerText = "Required";
        document.getElementById("ConferenceName").focus();
        return false;
    } */ 
	//FB 2997 ends
    //FB 2592 - Start
    var hdnCusID = document.getElementById("hdnCusID");
    if (hdnCusID) {
        var Str1 = hdnCusID.value.split('?');
        var i
        for (i = 0; i < Str1.length; i++) {
            var r = document.getElementById("ReqValCA_" + Str1[i].split("_")[2]);
            if (r) {
                ValidatorValidate(r)
                if (!r.isvalid) {
                    var chkadvance = document.getElementById("chkAdvancesetting");
                    if (chkadvance) {
                        chkadvance.checked = true;
                        openAdvancesetting()
                    }
                }
            }
        }
    }
    //FB 2592 - End

    var list = "";
    var audinsitems = document.getElementsByName("lstAudioParty"); //FB 2341
    for (var i = 0; i < audinsitems.length; i++) {
        if (audinsitems[i].checked) {
            list = audinsitems[i].value;
        }
    }

    if (list != "-1") {
        if (fnCheckRequiredField() == false) {
            return false;
        }

        if (fnCheckSpecialCharacters('txtAudioDialNo') == false) {
            return false;
        }
        if (fnCheckSpecialCharacters('txtConfCode') == false) {
            return false;
        }
        if (fnCheckSpecialCharacters('txtLeaderPin') == false) {
            return false;
        }
    }
    else {
        document.getElementById("lblError2").innerText = "";
        document.getElementById("lblError3").innerText = "";
        document.getElementById("lblError4").innerText = "";
        return true;
    }
    return true;
}


    function fnValidation1() {

        //Remove Date Validation
        var setupdate = Date.parse(document.getElementById("confStartDate").value + " " + document.getElementById("confStartTime_Text").value); //Actual Setup
        var sDate = Date.parse(document.getElementById("SetupDate").value + " " + document.getElementById("SetupTime_Text").value); //Actual Start - Name InterChanged :(
        var tDate = Date.parse(document.getElementById("confEndDate").value + " " + document.getElementById("confEndTime_Text").value); //Actula Teardown time
        var eDate = Date.parse(document.getElementById("TearDownDate").value + " " + document.getElementById("TeardownTime_Text").value); //Actual End TIme

        var actEndate = new Date(sDate);
        var timeMins = 15;
        actEndate.setMinutes(actEndate.getMinutes() + parseInt(timeMins));

        var errStr = "";
        if (setupdate > sDate) {
            errStr = "Setup time should be less than Start Time";
            document.getElementById("confStartTime_Text").focus();
        }
        else if (eDate <= sDate) {
            errStr = "End time should be greater than Start Time";
            document.getElementById("TeardownTime_Text").focus();
        }
        else if (eDate < actEndate) {
            errStr = "Invalid Duration.Conference duration should be minimum of 15 mins";
            document.getElementById("TeardownTime_Text").focus();
        }

        if (errStr != "") {
            alert(errStr);
            return false;
        }
		//FB 2997 starts
        /*if (document.getElementById("ConferenceName").value == "") {
            document.getElementById("lblConferenceNameError").innerText = "Required";
            document.getElementById("ConferenceName").focus();
            return false;
        } */
		//FB 2997  ENDS
        //FB 2592 - Start
        var hdnCusID = document.getElementById("hdnCusID");
        if (hdnCusID) {
            var Str1 = hdnCusID.value.split('?');
            var i
            for (i = 0; i < Str1.length; i++) {
                var r = document.getElementById("ReqValCA_" + Str1[i].split("_")[2]);
                if (r) {
                    ValidatorValidate(r)
                    if (!r.isvalid) {
                        var chkadvance = document.getElementById("chkAdvancesetting");
                        if (chkadvance) {
                            chkadvance.checked = true;
                            openAdvancesetting()
                        }
                    }
                }
            }
        }
        //FB 2592 - Start
        
        
        
        var list = "";
        var audinsitems = document.getElementsByName("lstAudioParty"); //FB 2341
        for (var i=0; i < audinsitems.length; i++)
        {
            if (audinsitems[i].checked)
            {
                list = audinsitems[i].value;
            }
        }

        

        if (list != "-1") {
            if (fnCheckRequiredField() == false) {
                return false;
            }

            if (fnCheckSpecialCharacters('txtAudioDialNo') == false) {
                return false;
            }
            if (fnCheckSpecialCharacters('txtConfCode') == false) {
                return false;
            }
            if (fnCheckSpecialCharacters('txtLeaderPin') == false) {
                return false;
            }
        }
        else {
            document.getElementById("lblError2").innerText = "";
            document.getElementById("lblError3").innerText = "";
            document.getElementById("lblError4").innerText = "";
            return true;
        }
        return true;

    }

    //FB 1830 Email Edit - start
    function fnemailalert() {
        if ("<%=isEditMode%>" == "1") {
            initial(); //FB 2266
            fnShow();
            var hdnalert = document.getElementById("hdnemailalert");
            var alertreq = hdnalert.value.trim();
            hdnalert.value = "";

            if (alertreq == "2") //Alert user for notify on edit
            {
                var alertmessage = 'Do you want to notify all participant(s)? \n\n (Click OK to notify all or CANCEL to notify new participants if any)';
                if (confirm(alertmessage))
                    hdnalert.value = "1"; //send email to all existing participants
                else
                    hdnalert.value = "0"; //dont notify old participants
            }
        }

        var trRooms = document.getElementById("modalrooms");

        var trfiles = document.getElementById("fileuploadsection");

        if (trfiles) {
            trfiles.style.display = 'None';
        }
        if (trRooms) {
            trRooms.style.display = 'none';
        }

        if (document.getElementById('btnDummy')) {
            document.getElementById('btnDummy').click();
        }
    }
    //FB 1830 Email Edit - end

    function Final() {
        if (fnValidation() == false) {
            return false;
        }
        if (SubmitRecurrence() == false) {
            return false;
        }

        var trRooms = document.getElementById("modalrooms");
        var trfiles = document.getElementById("fileuploadsection");


        if (trRooms) {
            trRooms.style.display = 'None';
        }

        if (trfiles) {
            trfiles.style.display = 'None';
        }


        return true;
    }
    //FB 2634    
    function fnClear()
    {
        var args = fnClear.arguments;
        
        var txtCtrl = document.getElementById(args[0]);
        
        if(txtCtrl)
        {
            if(txtCtrl.value == "")
                txtCtrl.value = "0";
        }
    }
    
    // FB 2693
    function fnPCconf()
    {
        if (document.getElementById("chkPCConf").checked)
            document.getElementById("tblPcConf").style.display = "";
        else
            document.getElementById("tblPcConf").style.display = "none";
    
        DisplayMCUConnectRow("<%=mcuSetupDisplay %>","<%=mcuTearDisplay %>",'<%=EnableBufferZone%>'); //FB 2998
    }
</script>


<%--Merging Recurrence script End here--%>
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf8" />
    <title>Express Conference</title>
    <%--Merging  Recurrence start--%>
    <link rel="StyleSheet" href="css/divtable.css" type="text/css" />
    <link rel="stylesheet" title="Expedite base styles" type="text/css" href="css/border-table.css" />
    <link rel="stylesheet" title="Expedite base styles" type="text/css" href="css/main-table.css" />
    <link rel="stylesheet" title="Expedite base styles" type="text/css" href="App_Themes/CSS/main.css" />
    <%-- FB 1830 - Translation Menu--%>
    <link rel="stylesheet" title="Expedite base styles" type="text/css" href="App_Themes/CSS/buttons.css" />

    <link rel="stylesheet" type="text/css" media="all" href="css/calendar-blue.css" />
    <%--FB 1947--%>
    <style type="text/css">
        .dnTableheadingRad label
        {
            font-weight: bold;
            font-size: 10pt;
            color: #556AAF;
            font-family: Arial;
            text-decoration: none;
        }
        .dnTableheadingChk label
        {
            font-size: 11px;
            vertical-align: middle;
            font-weight: bold;
            color: #556AAF;
        }
    </style>
</head>
<body>
    <form id="frmSettings2" runat="server" method="post" enctype="multipart/form-data">
    <asp:ScriptManager ID="ConfScriptManager" runat="server" AsyncPostBackTimeout="600">
    </asp:ScriptManager>
    <input type="hidden" name="hdnextusrcnt" id="hdnextusrcnt" runat="server" />
    <input type="hidden" id="hdnSetStartNow" runat="server" /><%--FB 1825--%>
    <input type="hidden" name="hdnconftype" id="hdnconftype" runat="server" />
    <asp:TextBox ID="txtModifyType" Text="" Visible="false" runat="server"></asp:TextBox>
    <asp:TextBox ID="txtTimeCheck" Text="" Visible="false" runat="server"></asp:TextBox>
    <input type="hidden" runat="server" id="CreateBy" value="2" />
    <input type="hidden" name="ModifyType" value="0" />
    <input type="hidden" name="Recur" id="Recur" runat="server" />
    <input id="confPassword" runat="server" type="hidden" />
    <input runat="server" id="RecurFlag" type="hidden" />
    <input runat="server" id="selectedloc" type="hidden" />
    <input id="RecurringText" runat="server" type="hidden" />
    <input type="hidden" name="ConfID" value="" />
    <input type="hidden" name="NeedInitial" value="2" />
    <input type="hidden" name="outlookEmails" value="" />
    <input type="hidden" name="lotusEmails" value="" />
    <input type="hidden" name="hdnSetupTime" id="hdnSetupTime" runat="server" />
    <input type="hidden" name="hdnTeardownTime" id="hdnTeardownTime" runat="server" />
    <input type="hidden" name="hdnBufferStr" id="hdnBufferStr" runat="server" />
    <input type="hidden" runat="server" id="txtHasCalendar" />
    <input type="hidden" id="hdnAudioInsIDs" name="hdnAudioInsIDs" runat="server" />
    <input type="hidden" name="hdnHostIDs" id="hdnHostIDs" runat="server" />
    <input type="hidden" id="hdnAudioInsID" name="hdnAudioInsID" runat="server" />
    <%--Merging Recurrence - Start--%>
    <input type="hidden" id="hdnOldTimezone" name="hdnOldTimezone" runat="server" value=""/> <%--FB 2884--%>
    <input type="hidden" id="TimeZoneText" name="TimeZoneText" value="" />
    <input type="hidden" id="TimeZoneValues" name="TimeZoneValues" value="" />
    <input type="hidden" id="EndDateText" name="EndDateText" value="" />
    <input type="hidden" id="RecurValue" name="RecurValue" value="" />
    <input type="hidden" id="RecurPattern" name="RecurPattern" value="" />
    <input type="hidden" id="CustomDates" name="CutomDates" value="" />
    <input type="hidden" id="RecPattern" name="RecPattern" value="" />
    <input type="hidden" id="HdnDateFormat" name="HdnDateFormat" value="<%=format %>" />
    <input type="hidden" id="hdnValue" runat="server" />
    <input type="hidden" id="hdnRecurValue" runat="server" />
    <%--Merging Recurrence - End--%>
    <input runat="server" name="hdnemailalert" id="hdnemailalert" type="hidden" />
    <%--FB 1830 Email Edit--%>
    <%--FB 1716--%>
    <input runat="server" id="hdnChange" type="hidden" />
    <input runat="server" id="hdnDuration" type="hidden" />
    <%--FB 1911--%>
    <input runat="server" id="hdnSpecRec" type="hidden" />
    <input type="hidden" name="Recur" id="RecurSpec" runat="server" />
    <input type="hidden" runat="server" id="guardSet" value="" />
    <input name="locstrname" type="hidden" id="locstrname" runat="server" /><%--FB 2367--%>    
    <%--<input type="hidden" id="isVMR" runat="server" value=""  />--%> <%--FB 2376--%> <%--FB 2717--%>
	<input type="hidden" runat="server" id="hdnCusID" /><%--FB 2592--%>
	<input type="hidden" runat="server" id="hdnStartTime" name="hdnStartTime" /><%--FB 2588--%>
    <input type="hidden" runat="server" id="hdnEndTime" name="hdnEndTime"  /><%--FB 2588--%>
    <input type="hidden" runat="server" id="hdnMcuProfileSelected" /><%--FB 2839--%>
    <table width="100%" cellpadding="2" cellspacing="0">
        <tr align="center" style="display: none;">
            <td>
                <h3>
                    <asp:Label ID="lblConfHeader" runat="server" Text="Schedule"></asp:Label>&nbsp;a
                    Video Conference
                </h3>
            </td>
        </tr>
        <tr>
            <td align="center">
                <table width="80%" border="0">
                    <tr>
                        <td align="center">
                        <h3><asp:Label ID="lblExpHead" runat="server" Text=""></asp:Label></h3><br /><!-- FB 2570 -->
                            <asp:Label ID="errLabel" runat="server" CssClass="lblError" Visible="false"></asp:Label> 
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr style="display: none;">
            <td class="reqfldstarText" align="right">
                * Required Field
            </td>
        </tr>
        <tr style="display: none;">
            <td>
                <img src="image/DoubleLine.jpg" alt="" height="6px" width="100%" />
            </td>
        </tr>
        <tr style="height: 38" valign="middle" class="Bodybackgorund">
            <td>
                <span style="width: 30;" class="ExpressHeadingsNumbers">&nbsp;&nbsp;&nbsp;1&nbsp;&nbsp;&nbsp;</span><span
                    class="ExpressHeadings">&nbsp;&nbsp;&nbsp;Basic Information</span>
            </td>
        </tr>
        <tr>
            <td>
                <table width="100%" border="0">
                    <tr>
                        <td style="width: 50%;" valign="top">
                            <table width="100%" cellpadding="2" border="0" cellspacing="0">
                                <tr>
                                    <td class="blackblodtext" align="left" style="width: 40%">
                                        <asp:Label ID="lblConfID" runat="server" Visible="False" Width="20%"></asp:Label>
                                        <span id="Field2">Title</span><span class="reqfldstarText">*&nbsp;</span>
                                    </td>
                                    <td style="height: 20px" valign="top">
                                     	<%--FB 2997 Starts--%>
                                    	<%-- <asp:TextBox ID="ConferenceName" onblur="javascript:clearError(this,'lblConferenceNameError');javascript:this.className='altText'"
                                            runat="server" CssClass="altText" Width="60%"  onfocus="javascript:this.className='altText3'"></asp:TextBox> --%>

                                     	<asp:TextBox ID="ConferenceName" runat="server" CssClass="altText" Width="60%"></asp:TextBox>

                                         <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="ConferenceName" ErrorMessage="Required" Font-Bold="True" Display="Dynamic"  ></asp:RequiredFieldValidator>
                                        
                                        <%--FB 2997 End--%>
                                        
                                        <asp:RegularExpressionValidator ID="regConfName" ControlToValidate="ConferenceName"
                                            Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<br>& < and > are invalid characters."
                                            ValidationExpression="^[^<>&]*$" ValidationGroup="Submit"></asp:RegularExpressionValidator><%--@@@@@ --%><%--FB 2321--%>
                                        <asp:RegularExpressionValidator runat="server" ID="valInput" ControlToValidate="ConferenceName"  ValidationExpression="^[\s\S]{0,256}$"   ErrorMessage="<br>Maximum limit is 256 characters"
                                         Display="Dynamic"></asp:RegularExpressionValidator><%--FB 2508--%>
                                        <label id="lblConferenceNameError" runat="server" style="font-weight: normal; color: Red"> <%--FB 2592--%>
                                        </label>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2" style="color: Red">
                                        <asp:Label runat="server" ID="lblAudioNote" Visible="false"></asp:Label>
                                    </td>
                                </tr>
                                <%--FB 2634--%>
                                <tr id="SetupRow" runat="server">
                                    <td class="blackblodtext" align="left" id="SDateText" nowrap="nowrap">
                                         Set-up (Minutes)
                                    </td>
                                    <td valign="top" style="text-align: left; height: 27px; color: black; font-family: arial;
                                        width: 403px;" align="center" colspan="2"><%--FB 2592--%>
                                         <asp:TextBox ID="SetupDuration" runat="server" CssClass="altText" Width="70px" AutoPostBack="false" onblur="javascript:fnClear('SetupDuration')"></asp:TextBox>
                                         <asp:ImageButton ID="imgSetup" src="image/info.png" runat="server" OnClientClick="javascript:return false;"/><%--FB 2998--%>
                                        <asp:RegularExpressionValidator ID="regSetupDuration" runat="server" ControlToValidate="SetupDuration"
                                            Display="Dynamic" ErrorMessage="Invalid Duration" ValidationExpression="\d+"></asp:RegularExpressionValidator>
                                    </td>
                                </tr>
                                <tr id="ConfStartRow">
                                    <td class="blackblodtext" align="left">
                                        Conference Start<span class="reqfldstarText">*&nbsp;</span>
                                    </td>
                                    <td valign="top" style="text-align: left; height: 27px; color: black; font-family: arial;
                                        width: 403px;" align="center" colspan="2">
                                        <span id="StartDateArea"><%--FB 2592--%>
                                            <asp:TextBox ID="confStartDate" runat="server" CssClass="altText" Width="70px" onblur="javascript:ChangeEndDate()"
                                             AutoPostBack="false"></asp:TextBox>
                                            <img src="image/calendar.gif" border="0" width="20" height="20" id="cal_triggerd" style="cursor: pointer;vertical-align: middle;" title="Date selector" 
                                            onclick="return showCalendar('<%=confStartDate.ClientID %>', 'cal_triggerd', 1, '<%=format%>');" />
                                            <span class="blackblodtext">@</span> </span>
                                        <mbcbb:ComboBox ID="confStartTime" runat="server" CssClass="altText" Rows="10" CausesValidation="True"
                                            onblur="javascript:formatTime('confStartTime_Text');return ChangeEndDate();" Style="width: auto" AutoPostBack="false">
                                        </mbcbb:ComboBox>
                                        <asp:RequiredFieldValidator ID="reqStartTime" runat="server" ControlToValidate="confStartTime"
                                            Display="Dynamic" ErrorMessage="Time is Required"></asp:RequiredFieldValidator>
                                        <asp:RegularExpressionValidator ID="regStartTime" runat="server" ControlToValidate="confStartTime"
                                            Display="Dynamic" ErrorMessage="Invalid Time (HH:MM AM/PM)" ValidationExpression="[0|1][0-9]:[0-5][0-9] ?[a|A|p|P][M|m]"></asp:RegularExpressionValidator><%--FB 1715--%>
                                        <asp:RequiredFieldValidator ID="reqStartData" runat="server" ControlToValidate="confStartDate"
                                            Display="Dynamic" ErrorMessage="Date is Required"></asp:RequiredFieldValidator>
                                        <asp:RegularExpressionValidator ID="regStartDate" runat="server" ControlToValidate="confStartDate"
                                            Display="Dynamic" ErrorMessage="Invalid Date <%=format%>" ValidationExpression="(0[1-9]|1[012])[- /.](0[1-9]|[12][0-9]|3[01])[- /.](19|20)\d\d|(0[1-9]|[12][0-9]|3[01])[- /.](0[1-9]|1[012])[- /.](19|20)\d\d"></asp:RegularExpressionValidator>
                                    </td>
                                </tr>
                                <tr id="ConfEndRow">
                                    <td class="blackblodtext" align="left" id="EDateText">
                                        Conference End<span class="reqfldstarText">*</span>
                                    </td>
                                    <td valign="top" style="text-align: left; height: 27px; color: black; font-family: arial;
                                        width: 403px;" align="center" colspan="2">
                                        <span id="EndDateArea"><%--FB 2592--%>
                                        <asp:TextBox ID="confEndDate" runat="server" CssClass="altText" Width="70px" onblur="javascript:EndDateValidation()"  AutoPostBack="false"></asp:TextBox>
                                        <img src="image/calendar.gif" border="0" width="20" height="20" id="cal_trigger1"
                                            style="cursor: pointer; vertical-align: middle;" title="Date selector" onclick="return showCalendar('<%=confEndDate.ClientID %>', 'cal_trigger1', 1, '<%=format%>');" />
                                        <span class="blackblodtext">@ </span>
                                        </span>
                                        <mbcbb:ComboBox ID="confEndTime" runat="server" CssClass="altSelectFormat" Rows="10"
                                            onblur="javascript:formatTime('confEndTime_Text');" Style="width: auto" CausesValidation="True" AutoPostBack="false">
                                        </mbcbb:ComboBox>
                                        <asp:RequiredFieldValidator ID="reqEndTime" runat="server" ControlToValidate="confEndTime"
                                            Display="Dynamic" ErrorMessage="Time is Required"></asp:RequiredFieldValidator>
                                        <asp:RegularExpressionValidator ID="regEndTime" runat="server" ControlToValidate="confEndTime"
                                            Display="Dynamic" ErrorMessage="Invalid Time (HH:MM AM/PM)" ValidationExpression="[0-1][0-9]:[0-5][0-9] ?[A|a|P|p][M|m]"></asp:RegularExpressionValidator><%--FB 1715--%>
                                        <asp:RequiredFieldValidator ID="reqEndDate" runat="server" ControlToValidate="confEndDate"
                                            Display="Dynamic" ErrorMessage="Date is Required"></asp:RequiredFieldValidator>
                                        <asp:RegularExpressionValidator ID="regEndDate" runat="server" ControlToValidate="confEndDate"
                                            Display="Dynamic" ErrorMessage="Invalid Date <%=format%>" ValidationExpression="(0[1-9]|1[012])[- /.](0[1-9]|[12][0-9]|3[01])[- /.](19|20)\d\d|(0[1-9]|[12][0-9]|3[01])[- /.](0[1-9]|1[012])[- /.](19|20)\d\d"></asp:RegularExpressionValidator>
                                    </td>
                                </tr>
                                <tr id="TearDownRow" style="display: none;">
                                    <td>
                                        <span id="TearDownArea">
                                            <asp:TextBox ID="TearDownDuration" runat="server" CssClass="altText" Width="70px" AutoPostBack="false"></asp:TextBox>
                                        </span>
                                    </td>
                                </tr>
                                <%--FB 2659 Starts--%>
                                <tr id="trseatavailable" runat="server" >
                                    <td class="blackblodtext" align="left">Seats Available</td>
                                    <td style="height: 24px">   
                                        <input type="button" id="btnchkSeatAvailable" value="Seats Available" cssclass="altText" class="altMedium0BlueButtonFormat" runat="server" onserverclick="CheckSeatsAvailability" />
                                    </td>
                                </tr>
                                <tr >
                                <td></td>
                                <td align="center">
                                <div id="modalDivPopup" style="display:none;  position:fixed; z-index:1000; left:0px; top:0px; width:100%; height:1000px; background-color:Gray; opacity:0.5;"></div>
                                <div id="modalDivContent" style="display:none; left:5%; position:fixed; top:30%; padding-bottom:0.5%; padding-top:0.5%; z-index:10000; background-color:White; width:90%;">
                                <table border="0" width="98%" cellpadding="5">
                                <tr>
                                <td align="left">Conference Time Availability : Number of Seats</td>
                                <td bgcolor="#65FF65"></td>
                                <td align="left">Totally Free</td>
                                <td bgcolor="#F8F075"></td>
                                <td align="left">Partially Free</td>
                                </tr>
                                <tr>
                                <td colspan="5">
                                <table id="tblSeatsAvailability" cellpadding="4" cellspacing="0" runat="server" height="50%" width="100%" border="1" ></table>
                                </td>
                                </tr>
                                </table>
                                <input type="button" align="middle" id="btnClose" runat="server" onclick="javascript:fnPopupSeatsClose(); return false;" value="Close" class="altMedium0BlueButtonFormat" /> <%--FB 2997-- %>
                                <br />
                                </div>
                                </td>
                                </tr>
                                <%--FB 2659 End--%>
                                <tr id="NONRecurringConferenceDiv8">
                                    <td class="blackblodtext" align="left">
                                        <span class="reqfldstarText">&nbsp;</span>Recurring Meetings
                                    </td>
                                    <%--FB 1911 Start //FB 2052--%>
                                    <td>
                                        <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                            <tr>
                                                <td>
                                                    <asp:CheckBox onClick="openRecur()" runat="server" ID="chkRecurrence" />
                                                </td>
                                                <td class="blackblodtext" align="right" nowrap id="SPCell1">
                                                    <span class="subtitleblueblodtext">&nbsp;OR </span>&nbsp; Special Recurrence &nbsp;
                                                </td>
                                                <td id="SPCell2" width="45%">
                                                    <a onclick="openRecur('S')" style="cursor: hand;">
                                                        <img src="image/recurring.gif" width="25" height="25" border="0">
                                                    </a>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                 <tr id="recurDIV" style="display: none;">
                                    <td align="left" valign="top"> <%--FB 2052--%>
                                        <span class="blackblodtext">Special Recurrence Text</span>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="RecurText" Enabled="false" runat="server" CssClass="altText" Rows="4"
                                            TextMode="MultiLine" Width="70%">No recurrence</asp:TextBox>
                                    </td>
                                </tr>
                                <%--FB 1911 End--%>
                                <%--FB 2226 Start--%>
                                 <tr id="trPublic" runat="server">
                                   <td class="blackblodtext" align="left" width="182px">&nbsp;Public</td><%--FB 2684--%>
                                   <td style="height: 24px">
                                     <table cellpadding="0" cellspacing="0"><tr><td>
                                        <asp:CheckBox ID="chkPublic" runat="server" />
                                        </td><td>
                                        <div id="openRegister" style="display:none">
                                            <asp:CheckBox ID="chkOpenForRegistration" runat="server" TextAlign="Left" /><span  class="blackblodtext" align="right">Open for Registration</span>
                                        </div>
                                        </td></tr>
                                     </table>
                                   </td>
                                </tr>
                                <%--FB 2226 End--%>	
                                <%--FB 2595 Start--%><%--FB 2684 Start--%>
                                 <tr id="trNetworkState" runat="server">
                                   <td  valign="top" id="tdNetworkState" runat="server" align="left" class="blackblodtext">&nbsp;Network Classification</td>
                                   <td valign="bottom" >
                                      <asp:DropDownList ID="drpNtwkClsfxtn" runat="server" CssClass="alt2SelectFormat" style="margin-left:3px; width:140px">
                                                            <asp:ListItem Value="1">NATO Secret</asp:ListItem>
                                                            <asp:ListItem Value="0">NATO Unclassified</asp:ListItem>
                                                            </asp:DropDownList>
                                    </td>
                                 </tr>                       
                                <%--FB 2595 End--%><%--FB 2684 End--%>							
								<%--FB 2501 Starts--%>
                                <tr id="trVMR" runat="server"> 
                                      <td class="blackblodtext" align="left" valign="top" >&nbsp;VMR</td>
                                                        <td style="height: 24px">
                                                           <table border="0"><tr><td>
                                                           <%--FB 2620 Starts--%>
                                                <asp:DropDownList ID="lstVMR" runat="server" CssClass="alt2SelectFormat" OnChange="javascript:changeVMR();" Width="140px"> <%--FB 2993--%>
                                                    <%--FB 2448--%>
                                                    <asp:ListItem Value="0" Selected="True" Text="None"></asp:ListItem>
                                                    <asp:ListItem Value="1" Text="Personal"></asp:ListItem>
                                                    <asp:ListItem Value="2" Text="Room"></asp:ListItem>
                                                    <asp:ListItem Value="3" Text="External"></asp:ListItem>
                                                    <%--<asp:ListItem Value="4" Text="Random"></asp:ListItem>--%> <%--FB 2620--%>
                                                    <%--FB 2481--%>
                                                </asp:DropDownList>
                                                
                                                           </td>
                                                           <td>
                                                           </td>
                                                           </tr></table>
                                                           
                                                
                                                <%--FB 2620 Ends--%>
                                        <%--VMR Start--%>
                                    <div id="divbridge" style="display:none" >
		                            <table>
                                        <tr>
                                            <td class="blackblodtext" align="left">Internal Bridge</td>
                                            <td style="height: 24px">
                                               <asp:TextBox ID="txtintbridge" ReadOnly="true" runat="server" ></asp:TextBox>
				                            <input type="hidden" name="intbridge" id="hdnintbridge" runat="server" />
				                            </td>
                                            </tr>
                                            <tr>
                                                <td class="blackblodtext" align="left">External Bridge</td>
                                                <td style="height: 24px">
                                                   <asp:TextBox ID="txtextbridge" ReadOnly="true" runat="server" ></asp:TextBox>
				                            <input type="hidden" name="extbridge" id="hdnextbridge" runat="server" />
				                            </td>
                                        </tr>
                                    </table>
                                    </div>
                                    <%--VMR End--%>
                                    </td>
                                </tr>
                                <%--FB Vidyo Start--%>
                                <tr id="trCloudConferencing" runat="server"> 
                                    <td class="blackblodtext" align="left" valign="bottom">&nbsp;Vidyo</td><%--FB 2834--%>
                                    <td style="height: 24px">
                                       <asp:CheckBox ID="chkCloudConferencing" runat="server" onClick="fnChkCloudConference()" />
                                       </td>
                                  </tr>
                                  <%--FB Vidyo End--%>
                                								
                                <tr>
                                    <asp:Panel ID="pnlPassword1" runat="server" Visible="false">
                                    <td class="blackblodtext" align="left">&nbsp;Numeric Password
                                      </td>
                                    <td valign="top" nowrap="nowrap">
                                  
                                        <asp:TextBox ID="ConferencePassword" runat="server" TextMode="SingleLine" CssClass="altText"></asp:TextBox><%--FB 2244--%>
                                        <%--Code changes for FB : 1232--%>
                                        <asp:Button runat="server" ID="btnGeneratePassword" width="140px" Text="Generate Password" autopostback="false" CssClass="altLongblueButtonFormat" OnClientClick="javascript:num_gen(); return false;" /><%-- FB 676--%>
                                        <br />
                                        <asp:CompareValidator ID="cmpValPassword1" runat="server" ControlToCompare="ConferencePassword2"
                                            ControlToValidate="ConferencePassword" Display="Dynamic" ErrorMessage="Please confirm / enter similar password."></asp:CompareValidator>
                                        <asp:RegularExpressionValidator ID="numPassword1" runat="server" ErrorMessage="<br>Only 4 to 10 digits are allowed. First digit should be non-zero." SetFocusOnError="True" ToolTip="Only numeric values are allowed." ControlToValidate="ConferencePassword" ValidationExpression="^([1-9])([0-9]\d{2,8})" Display="Dynamic"></asp:RegularExpressionValidator> <%--Comments: Fogbugz case 107, 522 --%>
                                    </td>
                                    </asp:Panel>
                               </tr>
                                <tr>
                                    <asp:Panel ID="pnlPassword2" runat="server" Visible="false">
                                    <td class="blackblodtext" align="left">&nbsp;Confirm Password</td>
                                    <td style="height: 20px" valign="top">
                                        <asp:TextBox ID="ConferencePassword2" runat="server" CssClass="altText" TextMode="SingleLine" ></asp:TextBox><%--FB 2244--%>
                                        <asp:CompareValidator ID="cmpValPassword" runat="server" ControlToCompare="ConferencePassword"
                                            ControlToValidate="ConferencePassword2" Display="Dynamic" ErrorMessage="Your passwords do not match."></asp:CompareValidator>
                                    </td>
                                    </asp:Panel>
                                </tr>
								<%--FB 2501 End--%>
								
                                <%--FB 2693 Start--%>
                                <tr id="trPCConf" runat="server" > 
                                    <td class="blackblodtext" align="left" valign="top" ><br />&nbsp;PC Conferencing</td>
                                    <td style="height: 24px"><br />
                                        <input type="checkbox" runat="server"  id="chkPCConf" onclick="javascript:fnPCconf();changeVMR();" /> <%--FB 2819--%>
                                        <table width="100%" border="0" id="tblPcConf" runat="server" style="display:none; margin-left:30px; margin-top:-20px" >
                                            <tr id="trBJ"  >
                                                <td width="50%" align="left" id="tdBJ" runat="server" >
                                                    <input type="radio" style="vertical-align:top" id="rdBJ" name="PCSelection" value="1" runat="server" />
                                                    <img alt="" width="20px" src="../image/BlueJeans.jpg" title="Blue Jeans" />
                                                    <a href="" runat="server" class="PCSelected" id="btnBJ" style="cursor:pointer; text-decoration:none; vertical-align:top" ><b style="color:Blue; vertical-align:top" >View</b></a>
                                                </td>
                                                <td align="left" id="tdJB" runat="server">
                                                    <input type="radio" style="vertical-align:top" id="rdJB" name="PCSelection" value="2" runat="server" /> 
                                                    <img alt="" width="20px" src="../image/Jabber.jpg"  title="Jabber" />
                                                    <a href="" class="PCSelected" runat="server" id="btnJB" style="cursor:pointer; text-decoration:none; vertical-align:top;"> <b style="color:Blue; vertical-align:top" >View</b></a>
                                                </td>
                                            </tr>
                                            <tr id="trLync"  >
                                                <td align="left" id="tdLy" runat="server">
                                                    <input type="radio" style="vertical-align:top" id="rdLync" name="PCSelection" value="3" runat="server" /> 
                                                    <img alt="" width="20px" src="../image/Lync.jpg"  title="Lync" />
                                                    <a href="" class="PCSelected" id="btnLync"  runat="server"  style="cursor:pointer; text-decoration:none; vertical-align:top" ><b style="color:Blue; vertical-align:top" >View</b></a>
                                                </td>
                                                <td align="left" id="tdVid" runat="server">
                                                    <input type="radio" style="vertical-align:top" id="rdVidtel" name="PCSelection" value="4" runat="server" />                                                                         
                                                    <img alt="" width="20px" src="../image/Vidtel.jpg"  title="Vidtel" />
                                                    <a href="" runat="server" class="PCSelected" id="btnVidtel" style="cursor:pointer; text-decoration:none; vertical-align:top" ><b style="color:Blue; vertical-align:top" >View</b></a>
                                                </td>
                                            </tr>
                                            <tr id="trVidyo"  style="display:none" >
                                                <td align="left">
                                                    <input type="radio" id="rdVidyo" name="PCSelection" value="5" runat="server" />                                                                        
                                                    <img alt="" src="../image/Vidyo.jpg" />
                                                </td>
                                                <td>
                                                    <a href="" class="PCSelected" id="btnVidyo" >View</a>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <%--FB 2693 End--%>
								
                                <tr id="StartNowRow"><%--FB 2341--%>
                                    <td class="blackblodtext" align="left">
                                        
                                        <span class="reqfldstarText">&nbsp;</span>Start Now
                                    </td>
                                    <td>
                                        
                                        <asp:CheckBox runat="server" ID="chkStartNow" />
                                    </td>
                                </tr>
                                <%--FB 2870 Start--%>
                                 <tr id="TrCTNumericID" runat="server">
                                   <td class="blackblodtext" align="left" width="182px">&nbsp;CTS Numeric ID</td>
                                   <td style="height: 24px">
                                     <table cellpadding="0" cellspacing="0"><tr><td>
                                        <asp:CheckBox ID="ChkEnableNumericID" runat="server" onclick="javascript:ChangeNumeric();" />
                                        </td><td>
                                        <div id="DivNumeridID" runat="server" style="display:none">
                                            <asp:TextBox ID="txtNumeridID" runat="server" ></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="ReqNumeridID" Enabled="false" runat="server" ControlToValidate="txtNumeridID"
                                                                        Display="dynamic" SetFocusOnError="true" ErrorMessage="Required"></asp:RequiredFieldValidator>
                                             <asp:RegularExpressionValidator ID="RegNumeridID" ControlToValidate="txtNumeridID" Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<br> & < and > are invalid characters." ValidationExpression="^[^<>&]*$"></asp:RegularExpressionValidator>
                                        </div>
                                        </td></tr>
                                     </table>
                                   </td>
                                </tr>
                                <%--FB 2870 End--%>
                                <tr id="trLineRate" runat="server"><%--FB 2394--%> <%--FB 2641--%>
                                    <td class="blackblodtext" align="left">
                                        
                                        <span class="reqfldstarText">&nbsp;</span>Maximum Line Rate
                                    </td>
                                    <td>
                                        
                                        <asp:DropDownList ID="lstLineRate" CssClass="altSelectFormat" DataTextField="LineRateName" DataValueField="LineRateID" runat="server" Width="30%"></asp:DropDownList>
                                    </td>
                                </tr>
                                 <%--FB 2501 starts--%>
                                <tr id="trStartMode" runat="server"> <%--FB 2641--%>
                                   <td class="blackblodtext" align="left"><div id="trStartMode1" runat="server">&nbsp;Start Mode</div></td>
                                   <td ><div id="trStartMode2" runat="server">
                                        <asp:DropDownList ID="lstStartMode" runat="server" CssClass="alt2SelectFormat">
                                            <asp:ListItem Value="0" Selected="True" Text="Automatic"></asp:ListItem>
                                            <asp:ListItem Value="1" Text="Manual"></asp:ListItem>
                                        </asp:DropDownList></div> 
                                   </td>
                                </tr>
                                <%--FB2501 ends--%>
                                <tr id="trAdvancedSettings" runat="server">
                                    <td class="blackblodtext" align="left">
                                         <%--FB 2266--%>
                                        <span class="reqfldstarText">&nbsp;</span>Advanced Settings
                                    </td>
                                    <td>
                                        <asp:CheckBox onClick="openAdvancesetting()" runat="server" ID="chkAdvancesetting" />
                                    </td>
                                </tr>
                                <tr id="divDuration" style="display: none">
                                    <td class="blackblodtext" align="left">
                                        Duration<span class="reqfldstarText">*</span>
                                    </td>
                                    <td>
                                        <mbcbb:ComboBox ID="lstDuration" runat="server" CssClass="altSelectFormat" Rows="10"
                                            CausesValidation="True" width="80px">
                                            <asp:ListItem Value="01:00" Selected="True">01:00</asp:ListItem>
                                            <asp:ListItem Value="02:00">02:00</asp:ListItem>
                                            <asp:ListItem Value="03:00">03:00</asp:ListItem>
                                            <asp:ListItem Value="04:00">04:00</asp:ListItem>
                                            <asp:ListItem Value="05:00">05:00</asp:ListItem>
                                            <asp:ListItem Value="06:00">06:00</asp:ListItem>
                                            <asp:ListItem Value="07:00">07:00</asp:ListItem>
                                            <asp:ListItem Value="08:00">08:00</asp:ListItem>
                                            <asp:ListItem Value="09:00">09:00</asp:ListItem>
                                            <asp:ListItem Value="10:00">10:00</asp:ListItem>
                                            <asp:ListItem Value="11:00">11:00</asp:ListItem>
                                            <asp:ListItem Value="12:00">12:00</asp:ListItem>
                                        </mbcbb:ComboBox>
                                        hh:mm
                                    </td>
                                </tr>
                                <tr id="NONRecurringConferenceDiv5" style="display: none;">
                                    <td class="blackblodtext" align="right">
                                        Duration
                                    </td>
                                    <td align="left">
                                        <asp:Label ID="lblConfDuration" runat="server"></asp:Label>
                                        <asp:Button ID="btnRefresh" CssClass="dnButtonLong" Text="Refresh" OnClick="CalculateDuration"
                                            OnClientClick="javascript:DataLoading(1);" runat="server" ValidationGroup="Update" />
                                    </td>
                                </tr>
                                <tr id="DurationRow" style="display: none;">
                                    <td align="left" class="blackblodtext" valign="top"> <%--FB 2592--%>
                                        &nbsp;Duration
                                    </td>
                                    <td>
                                        <asp:TextBox ID="RecurDurationhr" runat="server" CssClass="altText" Width="15%" onchange="javascript: SetRecurBuffer();"
                                            onblur="Javascript:  return validateDurationHr();"></asp:TextBox><font class="blackblodtext">hrs</font>
                                        <asp:TextBox ID="RecurDurationmi" runat="server" CssClass="altText" Width="15%" onchange="javascript: SetRecurBuffer();recurTimeChg();"
                                            onblur="Javascript: return validateDurationMi();"></asp:TextBox><font class="blackblodtext">mins</font>
                                        <br />
                                        <span class="blackblodtext" style="font-size: 8pt">
                                            <%if (EnableBufferZone == 0)
                                              { %>
                                            (Maximum Limit is
                                            <%=Application["MaxConferenceDurationInHours"]%>
                                            hours)
                                            <%}
                                              else
                                              { %>
                                            (Maximum Limit is
                                            <%=Application["MaxConferenceDurationInHours"]%>
                                            hours including buffer period)
                                            <%} %>
                                        </span>
                                        <asp:TextBox ID="EndText" runat="server" Style="display: none"></asp:TextBox>
                                    </td>
                                </tr>
                            </table>
                        </td>
                        
                        <td valign="top" align="left" width="50%">
                            <table width="50%">
                                <tr id="Advancesetting">
                                    <td colspan="2" align="left">
                                        <table border="0" style="background-color: #2C398D; width: 450px;" cellspacing="1">
                                            <tr style="background-color: #FFFFFF">
                                                <td align="left" style="width: 31%;" valign="top">
                                                    <table style="width: 100%;" border="0" cellpadding="3">
                                                        <tr>
                                                            <td style="width:150px;"><%--FB 2592--%>
                                                            </td>
                                                            <td align="left">
                                                                &nbsp; <a href="#">
                                                                    <asp:Label ID="aFileUp" runat="server" Text="Add Document" onclick="javascript:return CheckFiles();"
                                                                        Style="font-size: 10pt; color: Blue" /></a>
                                                                <asp:Label ID="lblFileList" runat="server" class="blackblodtext" Style="font-size: 8pt;
                                                                    display: none;"></asp:Label>
                                                            </td>
                                                        </tr>
                                                        <tr >
                                                            <td class="blackblodtext" align="left" nowrap="nowrap">
                                                                Conference Description
                                                            </td>
                                                            <td>
                                                                <asp:TextBox ID="ConferenceDescription" runat="server" MaxLength="2000" class="altText" Height="40"
                                                                    Rows="2" TextMode="MultiLine" Width="310PX" Columns="20" Wrap="true"></asp:TextBox>
                                                                <asp:RegularExpressionValidator Enabled="false" ID="regConfDisc" ControlToValidate="ConferenceDescription" Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<br> & < and > are invalid characters." ValidationExpression="^[^<>&]*$"></asp:RegularExpressionValidator> <%-- ZD 100263 --%>
                                                                <asp:RegularExpressionValidator Enabled="false" ID="RegularExpressionValidator3"
                                                                    ControlToValidate="ConferenceDescription" Display="dynamic" runat="server" SetFocusOnError="true"
                                                                    ErrorMessage="<br>& < > + % \ ? | ^ = ! ` [ ] { } # $ @ and ~ are invalid characters."
                                                                    ValidationExpression="^(a-z|A-Z|0-9)*[^\\<>^+?|!`\[\]{}\=@#$%&~]*$"></asp:RegularExpressionValidator><%--@@@@@--%>
                                                                <%--FB 1888--%>
                                                                <asp:RegularExpressionValidator runat="server" ID="ValConfDesc" ControlToValidate="ConferenceDescription"  ValidationExpression="^[\s\S]{0,2000}$"   ErrorMessage="<br>Maximum limit is 2000 characters"
                                                                Display="Dynamic"></asp:RegularExpressionValidator><%--FB 2508--%>   
                                                            </td>
                                                        </tr>
                                                        <tr id="NONRecurringConferenceDiv3">
                                                            <td class="blackblodtext" align="left">
                                                                Time Zone<span class="reqfldstarText">*</span>
                                                            </td>
                                                            <td>
                                                                <asp:DropDownList ID="lstConferenceTZ" runat="server" Width="210px" DataTextField="timezoneName"
                                                                    DataValueField="timezoneID" CssClass="altText">
                                                                </asp:DropDownList>
                                                            </td>
                                                        </tr>
                                                        <tr id="NONRecurringConferenceDiv9" runat="server">
                                                            <td class="blackblodtext">
                                                                Enable Buffer Zone
                                                            </td>
                                                            <td>
                                                                <asp:CheckBox ID="chkEnableBuffer" runat="server" Checked="true" />
                                                            </td>
                                                        </tr>
                                                        <tr> <%--FB 2592--%>
                                                        <td nowrap="nowrap" class="blackblodtext" align="left" id="Field4" runat="server" valign="top">Conference Host</td>
                                                        <td>
                                                            <asp:TextBox ID="txtApprover4" runat="server" CssClass="altText"
                                                                Enabled="true"></asp:TextBox>
                                                            <img id="Img8" onclick="javascript:getYourOwnEmailList(3)" alt="" src="image/edit.gif" style="cursor:pointer;" title="myVRM Address Book" /> <%--FB 2798--%>
                                                            <asp:TextBox ID="hdnApprover4" runat="server" BackColor="Transparent" BorderColor="White"
                                                                BorderStyle="None" Width="0px" ForeColor="Black"></asp:TextBox>
                                                            <asp:TextBox ID="hdnApproverMail" runat="server" Visible="false"></asp:TextBox>
                                                        </td>
                                                        </tr>
                                                        <tr><%--FB 2359--%>
                                                        <td class="blackblodtext"  align="left">Requestor</td>
                                                        <%--FB 2501 starts--%>
                                                       <td align="left">
                                                            <asp:TextBox ID="txtApprover7" runat="server" CssClass="altText"></asp:TextBox>
                                                             <img id="Img3" onclick="javascript:getYourOwnEmailList(6)" src="image/edit.gif"  style="cursor:pointer;" title="myVRM Address Book"/> <%--FB 2798--%>
                                                             <asp:TextBox ID="hdnApprover7" runat="server" BackColor="Transparent" BorderColor="White"
                                                             BorderStyle="None" Width="0px" ForeColor="Black"></asp:TextBox>
                                                             <asp:TextBox ID="hdnRequestorMail" runat="server" Width="0px" style="display:none"></asp:TextBox>
                                                        </td>
                                                        <%--FB 2501 ends--%>
                                                        </tr>
                                                        
                                                        <%--FB 2583 START--%>
                                                        <tr id="trFECC" runat="server">
                                                            <td id="tdFECC" runat="server" align="left" class="blackblodtext">FECC</td>
                                                            <td><asp:CheckBox ID="chkFECC" runat="server" /></td>
                                                       </tr>
                                                        <%--FB 2583 END--%>
                                                        <%--FB 2592 Starts--%>
                                                        <tr>
                                                            <td colspan="2" class="subtitlexxsblueblodtext">Additional Options
                                                                <table border="0" cellspacing="0px" cellpadding="0px" width="100%"> <%--FB 2349--%>
                                                                    <tr>
                                                                        <td align="left">
                                                                            <table border="0" width="100%">
                                                                                <tr>
                                                                                    <td>
                                                                                        <asp:Table ID="tblHost" HorizontalAlign="Left" runat="server" width="100%" CellPadding="3" CellSpacing="2" Visible="true">
                                                                                        </asp:Table>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                    
                                                                </table>
                                                            </td>
                                                        </tr>
                                                        
                                                        <%--FB 2595 End--%>
                                                        <tr>
                                                            <td colspan="2">
                                                                <table id="tblSplinst" runat="server" width="100%" style="margin-top:-20px">
                                                                    <tr id="trsplinst" runat="server">
                                                                        <td class="subtitlexxsblueblodtext">
                                                                            Special Instructions <span class="blackblodtext" style="font-size: 8pt">(Additional
                                                                                Support Requirements)</span>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td align="left">
                                                                            <table width="100%" border="0">
                                                                                <tr>
                                                                                    <td align="left"><%--FB 2592--%>
                                                                                        <asp:Table ID="tblSpecial" width="100%" runat="server" CellPadding="3" CellSpacing="2" Visible="true">
                                                                                        </asp:Table>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                        <%--FB 2592 Starts--%>
                                                        <tr>
                                                            <td colspan="2">
                                                                <table width="100%" border="0" cellspacing="0px" cellpadding="0px" > <%--FB 2349--%>
                                                                    <tr>
                                                                        <td align="left">
                                                                            <asp:Table ID="tblCustomAttribute" runat="server" CellPadding="3" width="100%" CellSpacing="2" Visible="true">
                                                                            </asp:Table>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                        <%--FB 2592 End--%>
														<%--FB 2839 Start--%>
														 <tr id="trMCUProfile" runat="server"> <%--FB 3063--%>
                                                            <td colspan="2">
                                                                <div id="div3" runat="server">
                                                                    <table id="Table1" runat="server" width="100%" style="margin-top:-20px">
                                                                        <tr>
                                                                            <td class="subtitlexxsblueblodtext">MCU Profile Selection</td> <%--ZD 100298--%>
                                                                        </tr>
                                                                        <tr>
                                                                            <td align="left">
                                                                                <table width="100%" border="0">
																				    <tr>
                                                                                        <td colspan="2">
                                                                                            <table width="100%" border="0" cellspacing="0px" cellpadding="0px" > <%--FB 2349--%>
                                                                                                <tr>
                                                                                                    <td align="left">
                                                                                                        <asp:Table ID="tblMCUProfile" runat="server" CellPadding="3" width="100%" CellSpacing="2" Visible="true">
                                                                                                        </asp:Table>
                                                                                                    </td>
                                                                                                </tr>
                                                                                            </table>
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </div>
                                                            </td>
                                                        </tr>
														<%--FB 2839 End--%>
                                                        <tr id="trConcSupport"><%--FB 2341--%>
                                                            <td colspan="2">
                                                             <div id="divConcSupport" runat="server"><%--FB 2359--%>
                                                                <table id="tblConcSupport" runat="server" width="100%" style="margin-top:-20px">
                                                                    <tr>
                                                                        <td class="subtitlexxsblueblodtext">Conference Support</td><%--FB 3023--%>
                                                                    </tr>
                                                                    <tr>
                                                                        <td align="left">
                                                                            <table width="100%" border="0">
                                                                                <%--FB 2377 - End--%>
                                                                                <%--FB 2632 - Starts--%>
																					<tr>
                                                                                        <td width="4%">
                                                                                        </td>
                                                                                        <td id="tdOnSiteAVSupport" runat="server" align="left"><%--FB 2670--%>
                                                                                            <input id="chkOnSiteAVSupport" type="checkbox" runat="server" />
                                                                                            <strong style="align: left; color:Black">On-Site A/V Support</strong> <%-- FB 2827 --%>
                                                                                        </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                        <td width="4%">
                                                                                        </td>
                                                                                        <td id="tdConciergeMonitoring" runat="server" align="left"><%--FB 2670--%>
                                                                                            <input id="chkConciergeMonitoring" type="checkbox" runat="server" />
                                                                                            <strong style="align: left; color:Black">Call Monitoring</strong> <%-- FB 2827 --%> <%--FB 3023--%>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td width="4%">
                                                                                        </td>
                                                                                        <td id="tdMeetandGreet" runat="server" align="left"><%--FB 2670--%>
                                                                                            <input id="chkMeetandGreet" type="checkbox" runat="server" />
                                                                                            <strong style="align: left; color:Black">Meet and Greet</strong> <%-- FB 2827 --%>
                                                                                        </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                        <td width="4%">
                                                                                        </td>
                                                                                        <td align="left" id="tdDedicatedVNOC" runat="server">
                                                                                            <input id="chkDedicatedVNOCOperator" type="checkbox" runat="server" />
                                                                                            <strong style="align: left; color:Black">Dedicated VNOC Operator</strong> <%-- FB 2827 --%>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <%--FB 2632 - End--%>
                                                                                    
                                                                                    <tr>
                                                                                     <td width="4%">
                                                                                        </td>
                                                                                       <td align="left" >
																				            <asp:TextBox ID="txtVNOCOperator" TextMode="MultiLine" runat="server" CssClass="altText" Enabled="true"></asp:TextBox>
                                                                                            <img id="imgVNOC" onclick="javascript:getVNOCEmailList()" src="image/edit.gif" runat="server" style="cursor:pointer;" title="Select VNOC Operator" /><%-- FB 2783 --%>
                                                                                            <a href="javascript: deleteVNOC();" onmouseover="window.status='';return true;">
                                                                                            <img border="0" id="imgdeleteVNOC" src="image/btn_delete.gif" title="Delete" alt="delete" width="16" height="16" runat="server"></a> <%--FB 2798--%>
                                                                                            <asp:TextBox ID="hdnVNOCOperator" runat="server" BackColor="Transparent" BorderColor="White"
                                                                                                BorderStyle="None" Width="0px" ForeColor="Black" style="display:none"></asp:TextBox>                                                      
                                                                                         </td>
                                                                            </tr>
                                                                           
                                                                             </table>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                              </div>
                                                            </td>
                                                        </tr>
                                                        <tr id="MCUConnectDisplayRow"  runat="server"> <%--FB 2998--%>
                                                            <td colspan="2">
                                                                <table width="100%">                  
                                                                     <tr>
                                                                        <td class="subtitlexxsblueblodtext" colspan="2">
                                                                            <asp:Label ID="lblMCUConnect" runat="server" Text="MCU Connect / Disconnect"></asp:Label>
                                                                        </td>
                                                                    </tr>                                      
                                                                    <tr >
                                                                        <td  align="left" id="Td3" nowrap="nowrap" class="blackblodtext" width="5%">
                                                                        </td>
                                                                        <td valign="top">
                                                                            <table width="100%" border="0">
                                                                             <tr align="left">
                                                                                 <td id="ConnectCell" runat="server" width="45%" valign="top">
                                                                                    <span class="blackblodtext">Connect (Minutes)</span> &nbsp;<asp:TextBox ID="txtMCUConnect"  runat="server" CssClass="altText" Width="50px"
                                                                                    onblur="javascript:if(this.value.trim() =='') this.value='0';"></asp:TextBox>
                                                                                    <asp:ImageButton ID="imgMCUConnect" src="image/info.png" runat="server" OnClientClick="javascript:return false;"/>
                                                                                    <br />
                                                                                    <%--FB 2998 Validation Part Starts--%>
                                                                                    <asp:CompareValidator  id="cmpNumbers" Enabled="true"  ErrorMessage="MCU Connect time should be less than or equal to Setup Time." display="dynamic"
                                                                                        ControlToCompare="SetupDuration" ControlToValidate="txtMCUConnect" Operator="LessThanEqual" runat="server" Type="Integer"
                                                                                        ValidationGroup="Submit" SetFocusOnError="true"  EnableClientScript="true" ></asp:CompareValidator>
                                                                                   <%--FB 2998 Validation Part Ends--%>
                                                                                    <asp:RangeValidator id="RangeValidator1" setfocusonerror="true" type="Integer" minimumvalue="-15"
                                                                                        maximumvalue="15" display="Dynamic" controltovalidate="txtMCUConnect" runat="server"
                                                                                        errormessage="MCU connect time is not allowed more than 15 mins."></asp:RangeValidator>
                                                                                    <asp:RegularExpressionValidator id="RegularExpressionValidator17" validationgroup="Submit"
                                                                                        controltovalidate="txtMCUConnect" display="dynamic" runat="server" setfocusonerror="true"
                                                                                        errormessage="Numeric values only." validationexpression="^-{0,1}\d+$"></asp:RegularExpressionValidator>
                                                                                 </td>
                                                                                 <td id="DisconnectCell" runat="server" width="55%" valign="top">
                                                                                   <span class="blackblodtext">Disconnect (Minutes)</span> &nbsp;<asp:TextBox ID="txtMCUDisConnect" runat="server" CssClass="altText" Width="50px" 
                                                                                   onblur="javascript:if(this.value.trim() =='') this.value='0';"></asp:TextBox>
                                                                                   <asp:ImageButton ID="imgMCUDisconnect" src="image/info.png" runat="server" OnClientClick="javascript:return false;"/>
                                                                                    <br />
                                                                                    <asp:rangevalidator id="RangeValidator2" setfocusonerror="true" type="Integer" minimumvalue="-15"
                                                                                        maximumvalue="15" display="Dynamic" controltovalidate="txtMCUDisConnect" runat="server"
                                                                                        errormessage="MCU disconnect time is not allowed more than 15 mins."></asp:rangevalidator>
                                                                                    <asp:regularexpressionvalidator id="RegularExpressionValidator20" validationgroup="Submit"
                                                                                        controltovalidate="txtMCUDisConnect" display="dynamic" runat="server" setfocusonerror="true"
                                                                                        errormessage="Numeric values only." validationexpression="^-{0,1}\d+$"></asp:regularexpressionvalidator>
                                                                                    <%--FB 2998 Validation Part Starts--%>
                                                                                    <asp:CompareValidator  id="cmpTeardown" Enabled="true"  ErrorMessage="MCU Disconnect Time should be less than or equal to Tear-Down Time." display="dynamic"
                                                                                        ControlToCompare="TearDownDuration" ControlToValidate="txtMCUDisConnect" Operator="LessThanEqual" runat="server" Type="Integer"
                                                                                         ValidationGroup="Submit" SetFocusOnError="true"  EnableClientScript="true" ></asp:CompareValidator>
                                                                                    <%--FB 2998 Validation Part Ends--%>
                                        
                                                                                 </td>
                                                                             </tr>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>                                                                
                                                        <tr id="trAudioConf"><%--FB 2341--%>
                                                            <td colspan="2">
                                                              <div id="divAudioConf" runat="server"><%--FB 2359--%>                                                             
                                                                <table id="tblAudioser" runat="server" width="100%" style="margin-top:-20px">
                                                                    <tr>
                                                                        <td class="subtitlexxsblueblodtext">Audio Conferencing<span class="blackblodtext" style="font-size: 8pt"></span></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td align="left">
                                                                            <table width="100%" border="0">
                                                                                <tr>
                                                                                    <%--<td width="16%">
                                                                                    </td>--%>
                                                                                    <td align="left">
                                                                                        <table cellspacing="0" cellpadding="0" border="0">
                                                                                            <tr>
                                                                                                <td align="left" valign="middle" style="width: 6%;">
                                                                                                </td>
                                                                                                <td valign="middle">
                                                                                                    <table border="0" width="80%" >
                                                                                                        <tr>
                                                                                                            <td colspan="3" align="left" class="blackblodtext">
                                                                                                                <asp:RadioButtonList runat="server" ID="lstAudioParty" onClick="SelectAudioParty()">
                                                                                                                </asp:RadioButtonList>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td id="td1" class="blackblodtext" align="center" valign="middle">
                                                                                                                Address/Phone
                                                                                                            </td>
                                                                                                            <td id="tdlblConfcode" runat="server" class="blackblodtext"
                                                                                                                align="center" valign="middle">
                                                                                                                <asp:Label ID="lblConfCode" runat="server" Text="Conference Code"></asp:Label>
                                                                                                            </td>
                                                                                                            <td id="tdlblLeaderPin" runat="server" class="blackblodtext"
                                                                                                                align="center" valign="middle">
                                                                                                                <asp:Label ID="lblLeaderPin" runat="server" Text="Leader PIN"></asp:Label>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td>
                                                                                                                <asp:TextBox ID="txtAudioDialNo" onblur="javascript:clearError(this,'lblError2')"
                                                                                                                    CssClass="altText" runat="server" Enabled="true"></asp:TextBox>
                                                                                                            </td>
                                                                                                            <td id="tdConfCode" runat="server">
                                                                                                                <asp:TextBox ID="txtConfCode" class="altText" onblur="javascript:clearError(this,'lblError3')"
                                                                                                                    runat="server" Enabled="true"></asp:TextBox>
                                                                                                            </td>
                                                                                                            <td id="tdLeaderPin" runat="server">
                                                                                                                <asp:TextBox ID="txtLeaderPin" CssClass="altText" onblur="javascript:clearError(this,'lblError4')"
                                                                                                                    runat="server" Enabled="true"></asp:TextBox>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td>
                                                                                                                <asp:RegularExpressionValidator ID="RegtxtAudioDialNo" ControlToValidate="txtAudioDialNo"
                                                                                                                    Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="Invalid IP Address"
                                                                                                                    ValidationExpression="^[^&<>+'dD][0-9'.]*$" ValidationGroup="Submit"></asp:RegularExpressionValidator><%--@@@@@--%>
                                                                                                                <br />
                                                                                                                <label id="lblError2" runat="server" style="font-weight: normal; color: Red"><%--FB 2592--%>
                                                                                                                </label>
                                                                                                            </td>
                                                                                                            <td id="tdRegConfCode" runat="server">
                                                                                                                <asp:RegularExpressionValidator ID="RegtxtConfCode" ControlToValidate="txtConfCode"
                                                                                                                    Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="Invalid Conference Code"
                                                                                                                    ValidationExpression="^[^&<>+'dD][0-9'.]*$" ValidationGroup="Submit"></asp:RegularExpressionValidator><%--@@@@@--%>
                                                                                                                <br />
                                                                                                                <label id="lblError3" runat="server" style="font-weight: normal; color: Red"><%--FB 2592--%>
                                                                                                                </label>
                                                                                                            </td>
                                                                                                            <td id="tdRegLeaderPin" runat="server">
                                                                                                                <asp:RegularExpressionValidator ID="RegtxtLeaderPin" ControlToValidate="txtLeaderPin"
                                                                                                                    Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="Invalid Leader PIN"
                                                                                                                    ValidationExpression="^[^&<>+'dD][0-9'.]*$" ValidationGroup="Submit"></asp:RegularExpressionValidator><%--@@@@@--%>
                                                                                                                <br />
                                                                                                                <label id="lblError4" runat="server" style="font-weight: normal; color: Red"><%--FB 2592--%>
                                                                                                                </label>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                    </table>
                                                                                                </td>
                                                                                            </tr>
                                                                                        </table>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                    <tr style="display: none;">
                                                                        <td>
                                                                            <img src="image/DoubleLine.jpg" alt="" height="6px" width="100%" />
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                              </div>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr id="RecurrenceRow">
                                    <td colspan="2" align="left">
                                        <table border="0" style="background-color: #2C398D; width: 450px;" cellspacing="1">
                                            <tr style="background-color: #FFFFFF">
                                                <td align="left" style="width: 31%;" valign="top">
                                                    <table style="width: 100%;" border="0" cellpadding="3">
                                                        <tr>
                                                            <td colspan="2" valign="top">
                                                                <table style="width: 100%;" border="0" cellpadding="3">
                                                                    <tr>
                                                                        <td valign="top" align="left" colspan="2">
                                                                            <span class="blackblodtext">Recurring Pattern</span>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td valign="top" nowrap style="width: 5%;" align="left" class="blackblodtext">
                                                                            <span class="blackblodtext">
                                                                                <asp:RadioButtonList ID="RecurType" runat="server" CssClass="blackblodtext" RepeatDirection="Vertical"
                                                                                    OnClick="javascript:return fnShow();" CellPadding="3">
                                                                                </asp:RadioButtonList>
                                                                            </span>
                                                                        </td>
                                                                        <td align="left" valign="top">
                                                                            <%--Daily Recurring Pattern--%>
                                                                            &nbsp;
                                                                            <asp:Panel ID="Daily" runat="server" HorizontalAlign="left" CssClass="blackblodtext">
                                                                                <asp:RadioButton ID="DEveryDay" runat="server" GroupName="RDaily" CssClass="blackblodtext" /><font
                                                                                    class="blackblodtext">Every </font>
                                                                                <asp:TextBox ID="DayGap" CssClass="altText" runat="server" Width="8%" onClick="javaScript: DEveryDay.checked = true;"
                                                                                    onChange="javaScript: summarydaily();"></asp:TextBox>
                                                                                <font class="blackblodtext">day(s)</font>
                                                                                <br />
                                                                                <asp:RadioButton ID="DWeekDay" CssClass="blackblodtext" runat="server" GroupName="RDaily"
                                                                                    onClick="javaScript: DayGap.value=''; summarydaily();" /><font class="blackblodtext">Every
                                                                                        weekday</font>
                                                                            </asp:Panel>
                                                                            <%--Weekly Recurring Pattern--%>
                                                                            <asp:Panel ID="Weekly" runat="server" nowrap Width="360">
                                                                                <font class="blackblodtext">Recur Every </font>
                                                                                <asp:TextBox ID="WeekGap" runat="server" CssClass="altText" Width="8%"></asp:TextBox>
                                                                                <font class="blackblodtext">week(s) on:</font>
                                                                                <asp:CheckBoxList ID="WeekDay" runat="server" CssClass="blackblodtext" RepeatDirection="horizontal"
                                                                                    RepeatColumns="4" CellPadding="2" CellSpacing="3" onClick="javaScript: summaryweekly();">
                                                                                </asp:CheckBoxList>
                                                                            </asp:Panel>
                                                                            <%--Monthly Recurring Pattern--%>
                                                                            <asp:Panel ID="Monthly" runat="server" nowrap Width="360">
                                                                                <asp:RadioButton ID="MEveryMthR1" runat="server" GroupName="GMonthly" onClick="javaScript: MonthGap2.value = ''; summarymonthly();" />
                                                                                <font class="blackblodtext">Day</font>
                                                                                <asp:TextBox ID="MonthDayNo" runat="server" CssClass="altText" Width="8%" onClick="javaScript: MEveryMthR1.checked = true;"
                                                                                    onChange="javaScript: summarymonthly();" class="altText"></asp:TextBox>
                                                                                <font class="blackblodtext">of every</font>
                                                                                <asp:TextBox ID="MonthGap1" runat="server" CssClass="altText" Width="20px" onClick="javaScript: MEveryMthR1.checked = true;"
                                                                                    onChange="javaScript: summarymonthly();"></asp:TextBox>
                                                                                <font class="blackblodtext">month(s)</font>
                                                                                <br />
                                                                                <br />
                                                                                <asp:RadioButton ID="MEveryMthR2" runat="server" GroupName="GMonthly" onClick="javaScript: summarymonthly(); MonthDayNo.value = ''; MonthGap1.value = '';" />
                                                                                <font class="blackblodtext">The</font>
                                                                                <asp:DropDownList CssClass="altText" runat="server" ID="MonthWeekDayNo" onClick="javaScript: summarymonthly();">
                                                                                </asp:DropDownList>
                                                                                <asp:DropDownList CssClass="altText" runat="server" ID="MonthWeekDay" onClick="javaScript: summarymonthly();">
                                                                                </asp:DropDownList>
                                                                                <font class="blackblodtext">of every</font>
                                                                                <asp:TextBox ID="MonthGap2" runat="server" CssClass="altText" Width="20px" onClick="javaScript: MEveryMthR2.checked = true;"
                                                                                    onChange="javaScript: summarymonthly();"></asp:TextBox>
                                                                                <font class="blackblodtext">month(s)</font></asp:Panel>
                                                                            <%--Yearly Recurring Pattern--%>
                                                                            <asp:Panel ID="Yearly" runat="server" nowrap Width="360">
                                                                                <asp:RadioButton ID="YEveryYr1" runat="server" GroupName="GYearly" onClick="javaScript: summaryyearly();" />
                                                                                <font class="blackblodtext">Every</font>
                                                                                <asp:DropDownList CssClass="altText" runat="server" ID="YearMonth1" onClick="javaScript: summaryyearly();">
                                                                                </asp:DropDownList>
                                                                                <asp:TextBox ID="YearMonthDay" runat="server" CssClass="altText" Width="8%" onChange="javaScript: summaryyearly();"
                                                                                    onClick="YEveryYr1.checked = true;"></asp:TextBox>
                                                                                <br />
                                                                                <br />
                                                                                <asp:RadioButton ID="YEveryYr2" runat="server" GroupName="GYearly" onClick="javaScript: summaryyearly(); document.frmSettings2.YearMonthDay.value = '';" />
                                                                                <font class="blackblodtext">The </font>
                                                                                <asp:DropDownList CssClass="altText" runat="server" ID="YearMonthWeekDayNo" onClick="javaScript: summaryyearly();">
                                                                                </asp:DropDownList>
                                                                                <asp:DropDownList CssClass="altText" runat="server" ID="YearMonthWeekDay" onClick="javaScript: summaryyearly();">
                                                                                </asp:DropDownList>
                                                                                <font class="blackblodtext">of </font>
                                                                                <asp:DropDownList CssClass="altText" runat="server" ID="YearMonth2" onClick="javaScript: summaryyearly();">
                                                                                </asp:DropDownList>
                                                                            </asp:Panel>
                                                                            <asp:Panel ID="Custom" runat="server">
                                                                                <table border="0">
                                                                                    <tr>
                                                                                        <td style="width: 30%">
                                                                                            <div id="flatCalendarDisplay" style="float: left; clear: both;">
                                                                                            </div>
                                                                                            <br />
                                                                                            <div id="Div2" style="font-size: 80%; text-align: center; padding: 2px">
                                                                                            </div>
                                                                                        </td>
                                                                                        <td style="width: 25%" align="center">
                                                                                            <span class="blackblodtext">Selected Date</span><br />
                                                                                            <asp:ListBox runat="server" ID="CustomDate" Rows="8" CssClass="altSmall0SelectFormat"
                                                                                                onChange="JavaScript: removedate(this);"></asp:ListBox>
                                                                                            <br />
                                                                                            <%--<asp:ImageButton ID="btnsortDates" ImageUrl="~/en/image/sort.png" runat="server" Text="Sort"  OnClientClick="javascript:return SortDates();" />--%>
                                                                                            <asp:Button ID="btnsortDates" CssClass="altMedium0BlueButtonFormat" runat="server" Width="80pt"
                                                                                                Text="Sort" OnClientClick="javascript:return SortDates();" /> <%--FB 3030--%>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td>
                                                                                        </td>
                                                                                        <td>
                                                                                            <span><font class="blackblodtext">* click a date to remove it from the list.<font
                                                                                                class="blackblodtext"></span>
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                            </asp:Panel>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                        <tr id="RangeRow" runat="server">
                                                            <td colspan="2">
                                                                <table border="0" width="100%" cellpadding="5">
                                                                    <tr>
                                                                        <td colspan="2">
                                                                            <span class="blackblodtext">Range of Recurrence </span>
                                                                        </td>
                                                                    </tr>
                                                                    <tr valign="top">
                                                                        <td class="blackblodtext" nowrap style="width: 20%;" align="right">
                                                                            Start
                                                                            <asp:TextBox ID="StartDate" Width="70px" Font-Size="9" CssClass="altText" runat="server"></asp:TextBox>
                                                                            <img alt="" src="image/calendar.gif" border="0" id="cal_triggerd1" style="cursor: pointer;
                                                                                vertical-align: top;" title="Date selector" onclick="return showCalendar('<%=StartDate.ClientID%>', 'cal_triggerd1', 1, '<%=format %>');" />
                                                                        </td>
                                                                        <td>
                                                                            <table width="100%" border="0">
                                                                                <tr>
                                                                                    <td class="blackblodtext" colspan="2">
                                                                                        <asp:RadioButton ID="EndType" runat="server" GroupName="RangeGroup" onClick="javascript: document.frmSettings2.Occurrence.value=''; document.frmSettings2.EndDate.value='';" />
                                                                                        No end date
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td class="blackblodtext" nowrap style="width: 9%;">
                                                                                        <asp:RadioButton ID="REndAfter" runat="server" GroupName="RangeGroup" onClick="javascript: document.frmSettings2.EndDate.value='';" />
                                                                                        End after
                                                                                    </td>
                                                                                    <td class="blackblodtext">
                                                                                        <asp:TextBox ID="Occurrence" CssClass="altText" Width="70px" runat="server" onClick="javascript: document.frmSettings2.REndAfter.checked=true; document.frmSettings2.EndDate.value='';"></asp:TextBox>
                                                                                        occurrences
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td class="blackblodtext">
                                                                                        <asp:RadioButton ID="REndBy" runat="server" GroupName="RangeGroup" onClick="javascript: document.frmSettings2.Occurrence.value='';" />
                                                                                        End by
                                                                                    </td>
                                                                                    <td nowrap>
                                                                                        <asp:TextBox ID="EndDate" Width="70px" onblur="javascript:CheckDate(this)" onchange="javascript:CheckDate(this)"
                                                                                            CssClass="altText" runat="server" onClick="javascript: document.frmSettings2.REndBy.checked=true; document.frmSettings2.Occurrence.value='';"></asp:TextBox>
                                                                                        <img alt="" src="image/calendar.gif" border="0" id="cal_trigger2" style="cursor: pointer;
                                                                                            vertical-align: top;" title="Date selector" onclick="return showCalendar('<%=EndDate.ClientID%>', 'cal_trigger2', 1, '<%=format %>');" />
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td colspan="2" class="blackblodtext" style="font-size: 8pt;">
                                                                            Note: Maximum limit of
                                                                            <%=Application["confRecurrence"]%>
                                                                            instances/occurrences in the recurring series.
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
                <%--Below script is to hide Recurrence Div during page load--%>

                <script language="javascript" type="text/javascript">    
                	//FB 2634
                    document.getElementById("regStartTime").controltovalidate = "confStartTime_Text";
                    document.getElementById("reqStartTime").controltovalidate = "confStartTime_Text";
                    document.getElementById("regEndTime").controltovalidate = "confEndTime_Text";
                    document.getElementById("reqEndTime").controltovalidate = "confEndTime_Text";
//                    document.getElementById("regSetupStartTime").controltovalidate = "SetupTime_Text"; 
//                    document.getElementById("reqSetupStartTime").controltovalidate = "SetupTime_Text";
//                    document.getElementById("regTearDownStartDate").controltovalidate = "TeardownTime_Text";
                                
                    if (document.getElementById("<%=Recur.ClientID %>").value != "" ) 
                    {
                        isRecur();
                        AnalyseRecurStr(document.getElementById("<%=Recur.ClientID %>").value);
                        st = calStart(atint[1], atint[2], atint[3]);
                        et = calEnd(st, parseInt(atint[4], 10));
                        document.getElementById("RecurringText").value =  recur_discription(document.getElementById("<%=Recur.ClientID %>").value, et, "Eastern Standard Time", Date(),"<%=Session["timeFormat"].ToString()%>","<%=Session["timezoneDisplay"].ToString()%>");
                    }
                    isRecur();
                    ChangeImmediate();
                    ChangePublic();//FB 2226
                    openAdvancesetting();

                    if ("<%=isInstanceEdit%>" == "Y" )
                    {  
                        document.getElementById("NONRecurringConferenceDiv8").style.display = "none";
                    }
                    
                    if ("<%=timeZone%>" == "0" ) 
                        document.getElementById("NONRecurringConferenceDiv3").style.display = "none";
                             //FB 2634                  
                    if ("<%=client.ToString().ToUpper() %>" == "MOJ")
                    {
                        document.getElementById("StartNowRow").style.display = "none";//FB 2341
                        document.getElementById("trPublic").style.display = "none"; 
                        document.getElementById("trConfType").style.display = "none"; 
                        document.getElementById("ConfStartRow").style.display =  "none"; 
                        document.getElementById("ConfEndRow").style.display =  "none"; 
                        
                    }
                    // FB 2359
                   

                </script>

            </td>
        </tr>
        <tr>
            <td width="100%">
                <table width="100%" border="0">
                    <tr>
                        <td width="50%" valign="top">
                            <table width="100%">
                                <tr style="height: 38" valign="middle" class="Bodybackgorund">
                                    <td>
                                        <span style="width: 30;" class="ExpressHeadingsNumbers">&nbsp;&nbsp;&nbsp;2&nbsp;&nbsp;&nbsp;</span><span
                                            class="ExpressHeadings">&nbsp;&nbsp;&nbsp;Rooms </span>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left">
                                        <asp:UpdatePanel ID="UpdatePanelSelectedRooms" runat="server" RenderMode="Inline"
                                            UpdateMode="Conditional">
                                            <Triggers>
                                            </Triggers>
                                            <ContentTemplate>
                                                <asp:ListBox ID="RoomList" runat="server" Height="150" Width="100%"
                                                    CssClass="altText2" SelectionMode="multiple"></asp:ListBox><%--FB 2367--%><%--ZD 100238--%>
                                                <br />
                                                <asp:Button ID="btnRoomSelect" runat="server" Style="display: none" OnClick="btnRoomSelect_Click"
                                                    CssClass="dnButtonLong"></asp:Button>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </td>
                                </tr>
                            </table>
                            <%-- FB 2525 Starts --%>
                            <asp:UpdatePanel ID="updtguest" runat="server">
                            <ContentTemplate>
                            <input type="hidden" runat="server" id="hdnGuestRoom" /><%--FB 2426--%>
	                        <input type="hidden" runat="server" id="hdnGuestRoomID" /><%--FB 2426--%>
	                        <input type="hidden" runat="server" id="hdnGuestloc" /><%--FB 2426--%>
                            <table width="100%">
                                <%-- FB 2426 Starts --%>
                                <tr id="OnFlyRowGuestRoom" runat="server" >
                                    <td style="text-align: left" ><hr id="idHr" style="visibility:hidden" /> <%--FB 2448--%>
                                    <div id="divIdRoomList" style="height:100px; overflow-y:scroll; overflow-x:hidden" ><%--FB 2448--%>
                                        <asp:DataGrid ID="dgOnflyGuestRoomlist" ShowHeader="false"  runat="server" AutoGenerateColumns="False"
                                            CellPadding="4" GridLines="None" OnEditCommand="dgOnflyGuestRoomlist_Edit" OnDeleteCommand="dgOnflyGuestRoomlist_DeleteCommand"
                                            Width="100%" AllowSorting="True" Style="border-collapse: separate">
                                            <FooterStyle CssClass="tableBody" Font-Bold="True" />
                                            <Columns>
                                                <asp:BoundColumn DataField="RowUID" Visible="false"></asp:BoundColumn>
                                                <asp:BoundColumn DataField="RoomID" Visible="false"></asp:BoundColumn>
                                                <asp:BoundColumn DataField="RoomName" ItemStyle-CssClass="tableBody" ItemStyle-BackColor="White" ItemStyle-Width="300px" ></asp:BoundColumn><%--ZD 100238--%>
                                                <asp:BoundColumn DataField="ContactName" ItemStyle-CssClass="tableBody" HeaderText="Guest Room In-Charge"
                                                    Visible="false" HeaderStyle-CssClass="tableHeader"></asp:BoundColumn>
                                                <asp:BoundColumn DataField="ContactEmail" Visible="false"></asp:BoundColumn>
                                                <asp:BoundColumn DataField="ContactPhoneNo" Visible="false"></asp:BoundColumn>
                                                <asp:BoundColumn DataField="ContactAddress" Visible="false"></asp:BoundColumn>
                                                <asp:BoundColumn DataField="State" Visible="false"></asp:BoundColumn>
                                                <asp:BoundColumn DataField="City" Visible="false"></asp:BoundColumn>
                                                <asp:BoundColumn DataField="ZIP" Visible="false"></asp:BoundColumn>
                                                <asp:BoundColumn DataField="Country" Visible="false"></asp:BoundColumn>
                                                <asp:BoundColumn DataField="IPAddressType" Visible="false"></asp:BoundColumn>
                                                <asp:BoundColumn DataField="IPAddress" Visible="false"></asp:BoundColumn>
                                                <asp:BoundColumn DataField="IPPassword" Visible="false"></asp:BoundColumn>
                                                <asp:BoundColumn DataField="IPconfirmPassword" Visible="false"></asp:BoundColumn>
                                                <asp:BoundColumn DataField="IPMaxLineRate" Visible="false"></asp:BoundColumn>
                                                <asp:BoundColumn DataField="IPConnectionType" Visible="false"></asp:BoundColumn>
                                                <asp:BoundColumn DataField="IsIPDefault" Visible="false"></asp:BoundColumn>
                                                <asp:BoundColumn DataField="SIPAddressType" Visible="false"></asp:BoundColumn>
                                                <asp:BoundColumn DataField="SIPAddress" Visible="false"></asp:BoundColumn>
                                                <asp:BoundColumn DataField="SIPPassword" Visible="false"></asp:BoundColumn>
                                                <asp:BoundColumn DataField="SIPconfirmPassword" Visible="false"></asp:BoundColumn>
                                                <asp:BoundColumn DataField="SIPMaxLineRate" Visible="false"></asp:BoundColumn>
                                                <asp:BoundColumn DataField="SIPConnectionType" Visible="false"></asp:BoundColumn>
                                                <asp:BoundColumn DataField="IsSIPDefault" Visible="false"></asp:BoundColumn>
                                                <asp:BoundColumn DataField="ISDNAddressType" Visible="false"></asp:BoundColumn>
                                                <asp:BoundColumn DataField="ISDNAddress" Visible="false" ItemStyle-CssClass="tableBody"
                                                    HeaderText="Address" HeaderStyle-CssClass="tableHeader"></asp:BoundColumn>
                                                <asp:BoundColumn DataField="ISDNPassword" Visible="false"></asp:BoundColumn>
                                                <asp:BoundColumn DataField="ISDNconfirmPassword" Visible="false"></asp:BoundColumn>
                                                <asp:BoundColumn DataField="ISDNMaxLineRate" Visible="false"></asp:BoundColumn>
                                                <asp:BoundColumn DataField="ISDNConnectionType" Visible="false" ItemStyle-CssClass="tableBody"
                                                    HeaderText="Connection Type" HeaderStyle-CssClass="tableHeader"></asp:BoundColumn>
                                                <asp:BoundColumn DataField="IsISDNDefault" Visible="false"></asp:BoundColumn>
                                                <asp:BoundColumn DataField="DefaultAddressType" ItemStyle-CssClass="tableBody" HeaderText="Address Type"
                                                    Visible="false" HeaderStyle-CssClass="tableHeader"></asp:BoundColumn>
                                                <asp:BoundColumn DataField="DefaultAddress" ItemStyle-CssClass="tableBody" HeaderText="Address"
                                                   Visible="false" HeaderStyle-CssClass="tableHeader"></asp:BoundColumn>
                                                <asp:BoundColumn DataField="DefaultConnetionType" ItemStyle-CssClass="tableBody"
                                                   Visible="false" HeaderText="Connection Type" HeaderStyle-CssClass="tableHeader"></asp:BoundColumn>
                                                <asp:TemplateColumn ItemStyle-CssClass="tableBody"  ItemStyle-BackColor="White" >
                                                    <ItemTemplate><%-- FB 2592 --%>
                                                        <asp:Button ID="btnGuestRoomEdit" CausesValidation="false" BackColor="White" CommandName="Edit" runat="server" Text="Edit" />
                                                        <asp:Button ID="btnGuestRoomDelete" CausesValidation="false" BackColor="White" CommandName="Delete" runat="server" Text="Delete" />
                                                    </ItemTemplate>
                                                </asp:TemplateColumn>
                                            </Columns>
                                            <SelectedItemStyle CssClass="tableBody" Font-Bold="True" />
                                            <EditItemStyle CssClass="tableBody" />
                                            <AlternatingItemStyle CssClass="tableBody" />
                                            <ItemStyle CssClass="tableBody" />
                                            <HeaderStyle CssClass="tableBody" Font-Bold="True" />
                                            <PagerStyle CssClass="tableBody" HorizontalAlign="Center" />
                                        </asp:DataGrid>
                                    </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td >
                                        <ajax:ModalPopupExtender ID="guestLocationPopup" runat="server" TargetControlID="btnGuestLocation2"
                                            PopupControlID="PopupLdapPanel" DropShadow="false" Drag="true" BackgroundCssClass="modalBackground"
                                            CancelControlID="ClosePUp2" BehaviorID="btnGuestLocation2">
                                        </ajax:ModalPopupExtender>
                                        <asp:Panel ID="PopupLdapPanel" runat="server" Width="98%" Height="98%" HorizontalAlign="Center"
                                            CssClass="treeSelectedNode" ScrollBars="Vertical">
                                            <table align="center" cellpadding="3" cellspacing="0" width="98%" style="border-collapse: collapse;
                                                height: 100%;">
                                                <tr>
                                                    <td align="center">
                                                        <table width="100%" border="0" cellpadding="3" style="border-collapse: collapse;
                                                            height: 100%;">
                                                            <tr>
                                                                <td colspan="6">
                                                                    <h3>
                                                                        Video Guest Location</h3>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td style="width: 1%;">
                                                                </td>
                                                                <td align="left" nowrap="nowrap" class="blackblodtext" style="width: 10%;"><%--FB 2579 Start--%>
                                                                    Client Site Name <span class="reqfldstarText">*</span>
                                                                </td>
                                                                <td align="left" style="width: 20%">
                                                                    <asp:TextBox ID="txtsiteName" runat="server" CssClass="altText" MaxLength ="20" Width="187px" /><br />
                                                                    <asp:RequiredFieldValidator ID="reqRoomName" Enabled="false" runat="server" ControlToValidate="txtsiteName"
                                                                        Display="dynamic" SetFocusOnError="true" ErrorMessage="Required" ValidationGroup="Submit1"></asp:RequiredFieldValidator>
                                                                    <asp:RegularExpressionValidator ID="regRoomName" Enabled="false" ControlToValidate="txtsiteName"
                                                                        Display="dynamic" runat="server" ValidationGroup="Submit1" SetFocusOnError="true"
                                                                        ErrorMessage="<br> & < > + % \ ? | ^ = ! ` [ ] { } $ @  and ~ are invalid characters."
                                                                        ValidationExpression="^(a-z|A-Z|0-9)*[^\\<>^+?|!`\[\]{}\=@$%&~]*$"></asp:RegularExpressionValidator>
                                                                </td>
                                                                <td style="width: 5%">
                                                                </td>
                                                                <td align="left" class="blackblodtext" style="width: 10%">
                                                                    Address
                                                                </td>
                                                                <td align="left" style="vertical-align: top; width: 54%">
                                                                    <asp:TextBox ID="txtAddress" runat="server" CssClass="altText" TextMode="MultiLine"
                                                                        Width="187px" />
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                </td>
                                                                <td align="left" class="blackblodtext" nowrap="nowrap">
                                                                    Contact Name <span class="reqfldstarText">*</span>
                                                                </td>
                                                                <td align="left" nowrap="nowrap">
                                                                    <asp:TextBox ID="txtApprover5" onclick="javascript:getYourOwnEmailList(4)" runat="server" CssClass="altText" Width="187px" />
                                                                    <img id="Img10" onclick="javascript:getYourOwnEmailList(4)" alt="" src="image/edit.gif" style="cursor:pointer;" title="myVRM Address Book" />  <%--FB 2798--%>
                                                                    <asp:TextBox ID="hdnApprover5" runat="server" BackColor="Transparent" BorderColor="White"
                                                                        BorderStyle="None" Width="0px" ForeColor="Black" Style="display: none"></asp:TextBox>
                                                                    <a href="javascript: deleteAssistant();" onmouseover="window.status='';return true;">
                                                                        <img border="0" src="image/btn_delete.gif" alt="delete" width="16" height="16" /></a><br />
                                                                    <asp:RequiredFieldValidator ID="reqcontactName" Enabled="false" runat="server" ControlToValidate="txtApprover5"
                                                                        Display="dynamic" SetFocusOnError="true" ErrorMessage="Required" ValidationGroup="Submit1"></asp:RequiredFieldValidator>
                                                                </td>
                                                                <td>
                                                                </td>
                                                                <td align="left" class="blackblodtext" nowrap="nowrap">
                                                                    State / Region
                                                                </td>
                                                                <td align="left">
                                                                    <asp:TextBox ID="txtState" runat="server" CssClass="altText" Width="187px" />
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                </td>
                                                                <td align="left" class="blackblodtext" nowrap="nowrap">
                                                                    Contact Email 
                                                                </td>
                                                                <td align="left">
                                                                    <asp:TextBox ID="txtEmailId" runat="server" CssClass="altText" Width="187px" /><br />
                                                                </td>
                                                                <td>
                                                                </td>
                                                                <td align="left" class="blackblodtext">
                                                                    City
                                                                </td>
                                                                <td align="left">
                                                                    <asp:TextBox ID="txtCity" runat="server" CssClass="altText" Width="187px" />
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                </td>
                                                                <td align="left" class="blackblodtext" nowrap="nowrap">
                                                                    Room Phone #
                                                                </td>
                                                                <td align="left">
                                                                    <asp:TextBox ID="txtPhone" runat="server" CssClass="altText" Width="187px" />
                                                                </td>
                                                                <td>
                                                                </td>
                                                                <td align="left" class="blackblodtext" nowrap="nowrap">
                                                                    ZIP Code
                                                                </td>
                                                                <td align="left">
                                                                    <asp:TextBox ID="txtZipcode" runat="server" CssClass="altText" Width="187px" />
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td colspan="4">
                                                                </td>
                                                                <td align="left" class="blackblodtext" nowrap="nowrap">
                                                                    Site Country
                                                                </td>
                                                                <td align="left">
                                                                    <asp:DropDownList ID="lstCountries" CssClass="altText" runat="server" DataTextField="Name"
                                                                        DataValueField="ID" Width="195px">
                                                                    </asp:DropDownList>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td colspan="6" align="left">
                                                                    <table border="0" width="85%" cellpadding="1">
                                                                        <tr>
                                                                            <td colspan="8" class="blackblodtext" align="left" nowrap="nowrap">
                                                                                <br />
                                                                                Dialing Information to Connect to Video System.
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td colspan="8" align="left" nowrap="nowrap" style="color:Black" > <%-- FB 2827 --%>
                                                                                NOTE: Please provide the information on below in a format that can be used to connect
                                                                                from the outside of your network.<br />
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td colspan="8" align="left" nowrap="nowrap" class="subtitlexxsblueblodtext">
                                                                                IP Address
                                                                            </td>
                                                                        </tr>
                                                                        <tr align="left" nowrap="nowrap">
                                                                            <td class="blackblodtext">
                                                                                Address
                                                                            </td>
                                                                            <td>
                                                                                <asp:TextBox ID="txtIPAddress" runat="server" CssClass="altText" Width="120px" />
                                                                                <asp:RequiredFieldValidator ID="reqIPAddress" Enabled="false" ControlToValidate="txtIPAddress"
                                                                                    ErrorMessage="Required" runat="server" Display="Dynamic" ValidationGroup="Submit1"></asp:RequiredFieldValidator>
                                                                                <asp:RegularExpressionValidator ID="regIPAddress" Enabled="false" ControlToValidate="txtIPAddress"
                                                                                    Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="Invalid IP address"
                                                                                    ValidationExpression="^([1-9]|[1-9][0-9]|1[0-9][0-9]|2[0-4][0-9]|25[0-5])(\.([0-9]|[1-9][0-9]|1[0-9][0-9]|2[0-4][0-9]|25[0-5])){3}$"></asp:RegularExpressionValidator>
                                                                            </td>
                                                                            <td class="blackblodtext">
                                                                                Password
                                                                            </td>
                                                                            <td>
                                                                                <asp:TextBox ID="txtIPPassword" runat="server" CssClass="altText"
                                                                                    Width="120px" />
                                                                                <asp:CompareValidator ID="cmpIPValPwd1" runat="server" Enabled="false" ControlToCompare="txtIPconfirmPassword"
                                                                                    ControlToValidate="txtIPPassword" Display="Dynamic" ErrorMessage="Re-enter password."
                                                                                    ValidationGroup="Submit1"></asp:CompareValidator>
                                                                            </td>
                                                                            <td class="blackblodtext" nowrap="nowrap">
                                                                                Confirm Password
                                                                            </td>
                                                                            <td nowrap="nowrap">
                                                                                <asp:TextBox ID="txtIPconfirmPassword" runat="server" CssClass="altText"
                                                                                    Width="120px" />
                                                                                <asp:CompareValidator ID="cmpIPValPwd2" runat="server" Enabled="false" ControlToCompare="txtIPPassword"
                                                                                    ControlToValidate="txtIPconfirmPassword" Display="Dynamic" ErrorMessage="Passwords do not match."
                                                                                    ValidationGroup="Submit1"></asp:CompareValidator>
                                                                            </td>
                                                                        </tr>
                                                                        <tr align="left">
                                                                            <td class="blackblodtext" nowrap="nowrap">
                                                                                Line Rate
                                                                            </td>
                                                                            <td>
                                                                                <asp:DropDownList CssClass="altSelectFormat" Width="125px" ID="lstIPlinerate" runat="server"
                                                                                    DataTextField="LineRateName" DataValueField="LineRateID">
                                                                                </asp:DropDownList>
                                                                                <asp:RequiredFieldValidator ID="reqIPlinerate" Enabled="false" ControlToValidate="lstIPlinerate"
                                                                                    InitialValue="-1" ErrorMessage="Required" runat="server" Display="Dynamic" ValidationGroup="Submit1"></asp:RequiredFieldValidator>
                                                                            </td>
                                                                            <td class="blackblodtext" style="display: none">
                                                                                Equipment
                                                                            </td>
                                                                            <td style="display: none">
                                                                                <asp:DropDownList CssClass="altSelectFormat" ID="lstIPVideoEquipment" runat="server"
                                                                                    DataTextField="VideoEquipmentName" DataValueField="VideoEquipmentID">
                                                                                </asp:DropDownList>
                                                                            </td>
                                                                            <td class="blackblodtext" nowrap="nowrap">
                                                                                Connection Type
                                                                            </td>
                                                                            <td>
                                                                                <asp:DropDownList ID="lstIPConnectionType" Width="125px" CssClass="altSelectFormat"
                                                                                    runat="server" DataTextField="Name" DataValueField="ID">
                                                                                </asp:DropDownList>
                                                                                <asp:RequiredFieldValidator ID="reqIPConnectionType" Enabled="false" ControlToValidate="lstIPConnectionType"
                                                                                    ValidationGroup="Submit1" InitialValue="-1" ErrorMessage="Required" runat="server"
                                                                                    Display="Dynamic"></asp:RequiredFieldValidator>
                                                                            </td>
                                                                            <td class="blackblodtext">
                                                                                Use Default
                                                                            </td>
                                                                            <td>
                                                                                <asp:RadioButton ID="radioIsDefault" runat="server" GroupName="RDefault" Checked="true" /> <%--FB 2983--%>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td align="left" colspan="8" nowrap="nowrap" class="subtitlexxsblueblodtext">
                                                                                E164/SIP Address
                                                                            </td>
                                                                        </tr>
                                                                        <tr align="left" nowrap="nowrap">
                                                                            <td class="blackblodtext">
                                                                                Address
                                                                            </td>
                                                                            <td>
                                                                                <asp:TextBox ID="txtSIPAddress" runat="server" CssClass="altText" Width="120px" />
                                                                                <asp:RequiredFieldValidator ID="reqSIPAddress" Enabled="false" ControlToValidate="txtSIPAddress"
                                                                                    ErrorMessage="Required" runat="server" Display="Dynamic" ValidationGroup="Submit1"></asp:RequiredFieldValidator>
                                                                                <asp:RegularExpressionValidator ID="regSIPAddress" Enabled="false" ControlToValidate="txtSIPAddress"
                                                                                    Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="Invalid E164 / SIP address"
                                                                                    ValidationExpression="\d+"></asp:RegularExpressionValidator>
                                                                            </td>
                                                                            <td class="blackblodtext">
                                                                                Password
                                                                            </td>
                                                                            <td>
                                                                                <asp:TextBox ID="txtSIPPassword" runat="server" CssClass="altText"
                                                                                    Width="120px" />
                                                                                <asp:CompareValidator ID="cmpSIPValPwd1" Enabled="false" runat="server" ControlToCompare="txtSIPconfirmPassword"
                                                                                    ControlToValidate="txtSIPPassword" Display="Dynamic" ErrorMessage="Re-enter password."
                                                                                    ValidationGroup="Submit1"></asp:CompareValidator>
                                                                            </td>
                                                                            <td class="blackblodtext" nowrap="nowrap">
                                                                                Confirm Password
                                                                            </td>
                                                                            <td>
                                                                                <asp:TextBox ID="txtSIPconfirmPassword" runat="server" CssClass="altText"
                                                                                    Width="120px" />
                                                                                <asp:CompareValidator ID="cmpSIPValPwd2" Enabled="false" runat="server" ControlToCompare="txtSIPPassword"
                                                                                    ControlToValidate="txtSIPconfirmPassword" Display="Dynamic" ErrorMessage="Passwords do not match."
                                                                                    ValidationGroup="Submit1"></asp:CompareValidator>
                                                                            </td>
                                                                        </tr>
                                                                        <tr align="left">
                                                                            <td class="blackblodtext" nowrap="nowrap">
                                                                                Line Rate
                                                                            </td>
                                                                            <td>
                                                                                <asp:DropDownList CssClass="altSelectFormat" Width="125px" ID="lstSIPlinerate" runat="server"
                                                                                    DataTextField="LineRateName" DataValueField="LineRateID">
                                                                                </asp:DropDownList>
                                                                                <asp:RequiredFieldValidator ID="reqSIPlinerate" Enabled="false" ControlToValidate="lstSIPlinerate"
                                                                                    InitialValue="-1" ErrorMessage="Required" runat="server" Display="Dynamic" ValidationGroup="Submit1"></asp:RequiredFieldValidator>
                                                                            </td>
                                                                            <td class="blackblodtext" style="display: none">
                                                                                Equipment
                                                                            </td>
                                                                            <td style="display: none">
                                                                                <asp:DropDownList CssClass="altSelectFormat" ID="lstSIPVideoEquipment" runat="server"
                                                                                    DataTextField="VideoEquipmentName" DataValueField="VideoEquipmentID">
                                                                                </asp:DropDownList>
                                                                            </td>
                                                                            <td class="blackblodtext" nowrap="nowrap">
                                                                                Connection Type
                                                                            </td>
                                                                            <td>
                                                                                <asp:DropDownList ID="lstSIPConnectionType" Width="125px" CssClass="altSelectFormat"
                                                                                    runat="server" DataTextField="Name" DataValueField="ID">
                                                                                </asp:DropDownList>
                                                                                <asp:RequiredFieldValidator ID="reqSIPConnectionType" Enabled="false" ControlToValidate="lstSIPConnectionType"
                                                                                    ValidationGroup="Submit1" InitialValue="-1" ErrorMessage="Required" runat="server"
                                                                                    Display="Dynamic"></asp:RequiredFieldValidator>
                                                                            </td>
                                                                            <td class="blackblodtext">
                                                                                Use Default
                                                                            </td>
                                                                            <td>
                                                                                <asp:RadioButton ID="radioIsDefault2" runat="server" GroupName="RDefault" /><%--FB 2983--%>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td align="left" colspan="8" class="subtitlexxsblueblodtext">
                                                                                ISDN Address
                                                                            </td>
                                                                        </tr>
                                                                        <tr align="left" nowrap="nowrap">
                                                                            <td class="blackblodtext">
                                                                                Address
                                                                            </td>
                                                                            <td>
                                                                                <asp:TextBox ID="txtISDNAddress" runat="server" CssClass="altText" Width="120px" />
                                                                                <asp:RequiredFieldValidator ID="reqISDNAddress" Enabled="false" ControlToValidate="txtISDNAddress"
                                                                                    ErrorMessage="Required" runat="server" Display="Dynamic" ValidationGroup="Submit1"></asp:RequiredFieldValidator>
                                                                                <asp:RegularExpressionValidator ID="regISDNAddress" ControlToValidate="txtISDNAddress"
                                                                                    Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="Invalid ISDN address"
                                                                                    ValidationExpression="\d+"></asp:RegularExpressionValidator>
                                                                            </td>
                                                                            <td class="blackblodtext">
                                                                                Password
                                                                            </td>
                                                                            <td>
                                                                                <asp:TextBox ID="txtISDNPassword" runat="server" CssClass="altText"
                                                                                    Width="120px" />
                                                                                <asp:CompareValidator ID="cmpISDNValPwd1" runat="server" Enabled="false" ControlToCompare="txtISDNconfirmPassword"
                                                                                    ControlToValidate="txtISDNPassword" Display="Dynamic" ErrorMessage="Re-enter password."
                                                                                    ValidationGroup="Submit1"></asp:CompareValidator>
                                                                            </td>
                                                                            <td class="blackblodtext" nowrap="nowrap">
                                                                                Confirm Password
                                                                            </td>
                                                                            <td>
                                                                                <asp:TextBox ID="txtISDNconfirmPassword" runat="server" CssClass="altText"
                                                                                    Width="120px" />
                                                                                <asp:CompareValidator ID="cmpISDNValPwd2" runat="server" Enabled="false" ControlToCompare="txtISDNPassword"
                                                                                    ControlToValidate="txtISDNconfirmPassword" Display="Dynamic" ErrorMessage="Passwords do not match."
                                                                                    ValidationGroup="Submit1"></asp:CompareValidator>
                                                                            </td>
                                                                        </tr>
                                                                        <tr align="left">
                                                                            <td class="blackblodtext" nowrap="nowrap">
                                                                                Line Rate
                                                                            </td>
                                                                            <td>
                                                                                <asp:DropDownList CssClass="altSelectFormat" Width="125px" ID="lstISDNlinerate" runat="server"
                                                                                    DataTextField="LineRateName" DataValueField="LineRateID">
                                                                                </asp:DropDownList>
                                                                                <asp:RequiredFieldValidator ID="reqISDNlinerate" Enabled="false" ControlToValidate="lstISDNlinerate"
                                                                                    InitialValue="-1" ErrorMessage="Required" runat="server" Display="Dynamic" ValidationGroup="Submit1"></asp:RequiredFieldValidator>
                                                                            </td>
                                                                            <td class="blackblodtext" style="display: none">
                                                                                Equipment
                                                                            </td>
                                                                            <td style="display: none">
                                                                                <asp:DropDownList CssClass="altSelectFormat" ID="lstISDNVideoEquipment" runat="server"
                                                                                    DataTextField="VideoEquipmentName" DataValueField="VideoEquipmentID">
                                                                                </asp:DropDownList>
                                                                            </td>
                                                                            <td class="blackblodtext" nowrap="nowrap">
                                                                                Connection Type
                                                                            </td>
                                                                            <td>
                                                                                <asp:DropDownList ID="lstISDNConnectionType" Width="125px" CssClass="altSelectFormat"
                                                                                    runat="server" DataTextField="Name" DataValueField="ID">
                                                                                </asp:DropDownList>
                                                                                <asp:RequiredFieldValidator ID="reqISDNConnectionType" Enabled="false" ControlToValidate="lstISDNConnectionType"
                                                                                    ValidationGroup="Submit1" InitialValue="-1" ErrorMessage="Required" runat="server"
                                                                                    Display="Dynamic"></asp:RequiredFieldValidator>
                                                                            </td>
                                                                            <td class="blackblodtext">
                                                                                Use Default
                                                                            </td>
                                                                            <td>
                                                                                <asp:RadioButton ID="radioIsDefault3" runat="server" GroupName="RDefault" /> <%--FB 2983--%>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                            <tr align="center" style="height: 50%">
                                                                <td align="center" colspan="6">
                                                                    <br />
                                                                    <input align="middle" type="button" runat="server" validationgroup="Submit1" id="ClosePUp2"
                                                                        value=" Close " class="altMedium0BlueButtonFormat" style="width:75px" onserverclick="fnGuestLocationCancel"
                                                                        onclick="javascript:return fnDisableValidator();" /> <%-- FB 2827 --%>
                                                                    <asp:Button ID="btnGuestLocationSubmit" runat="server" Text="Submit" ValidationGroup="Submit1"
                                                                        OnClick="fnGuestLocationSubmit" class="altMedium0BlueButtonFormat" Width="75px" OnClientClick="javascript:return ValidateflyEndpoints();" /> <%-- FB 2827 --%>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </table>
                                        </asp:Panel>
                                    </td>
                                </tr>
                                <tr style="display:none">
                                    <td>
                                        <asp:Button ID="btnGuestLocation2" runat="server" />
                                    </td>
                                </tr>
                                <%-- FB 2426 Ends --%>
                            </table>
                            </ContentTemplate>
                            </asp:UpdatePanel>
                            <%-- FB 2525 Starts --%>
                        </td>
                        <td width="50%" valign="top">
                            <table width="100%" cellpadding="0" cellspacing="0">
                                <tr style="height: 38" valign="middle" class="Bodybackgorund">
                                    <td>
                                        <span style="width: 30;" class="ExpressHeadingsNumbers">&nbsp;&nbsp;&nbsp;3&nbsp;&nbsp;&nbsp;</span><span
                                            class="ExpressHeadings">&nbsp;&nbsp;&nbsp;Participants </span>
                                    </td>
                                </tr>
                                <tr>
                                    <td bordercolor="#0000ff" colspan="4" align="left" width="100%">
                                        <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                            <tr>
                                                <td width="40%" valign="top" align="left">
                                                    <iframe align="left" height="150" name="ifrmPartylist" runat="server" id="ifrmPartylist"
                                                        src="Settings2PartyNETExpress.aspx?wintype=ifr" valign="top" style="border: 0px"
                                                        width="100%">
                                                        <p>
                                                            go to <a href="settings2partyNET.aspx?wintype=ifr">Participants</a></p>
                                                    </iframe>
                                                </td>
                                            </tr>
                                        </table>
                                        <%--the following 2 controls have been added to confirm the validation on next and previous click under CheckFiles function--%>
                                        <asp:TextBox ID="txtTempUser" runat="server" Visible="false" Text="User"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="reqUser" ControlToValidate="txtTempUser" runat="server"></asp:RequiredFieldValidator>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="9">
                                        <table border="0" cellspacing="0" width="100%">
                                            <tr>
                                                <td width="100%" valign="top" align="left">
                                                    <asp:TextBox ID="txtUsersStr" runat="server" Width="0px" ForeColor="transparent"
                                                        BackColor="transparent" BorderStyle="None" BorderColor="Transparent"></asp:TextBox>
                                                    <asp:TextBox ID="txtPartysInfo" runat="server" Width="0px" ForeColor="Black" BackColor="transparent"
                                                        BorderStyle="None" BorderColor="Transparent"></asp:TextBox>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                  <%--FB 2426 Start--%>
                    <tr valign="top">
                    <td colspan="2">
                    <table border="0" style="width:100%;">
                    <tr>
                         <td style="width:50%;">
                         <%--<input type="image" id="RoomPoupup" value="RoomPoupup" onfocus="this.blur()" validationgroup="Submit" onclick="javascript:return OpenRoom();"  
                                        runat="server" src="~/en/image/PressToSelect.png" /> FB --%><%--FB 2367--%>
                            <input type="button" id="RoomPoupup" value="Choose" class="altMedium0BlueButtonFormat"
                                onfocus="this.blur()" onclick="javascript:OpenRoomSearchresponse();" runat="server" /><%-- FB 2827 --%>&nbsp;&nbsp;<asp:Image
                                    ID="Image2" src="image/info.png" runat="server" ToolTip="Add a TelePresence room or video endpoint to the meeting." />
                            <input name="addRooms" type="button" id="addRooms" onclick="javascript:AddRooms();"
                                style="display: none;" />
                            <asp:Button ID="btnGuestLocation" runat="server" Width="40%" Text="Add Video Guest Location"
                                class="altMedium0BlueButtonFormat" OnClientClick="javascript:return fnValidator();" /><%-- FB 2827 --%>
                            <asp:Image ID="Image6" src="image/info.png" runat="server" ToolTip="Create a Guest room to the meeting." />
                        </td>
                        <td align="right" class="blackblodtext" style="display: none">
                            <a onclick="javascript:goToCal(); return false;" href="#">Check Room Calendar</a>
                            <img src="image/calendar.gif" alt="" border="0" hspace="0" vspace="0" id="Img2" style="cursor: pointer;
                                vertical-align: bottom;" title="Date selector" onclick="javascript:goToCal(); return false;" />
                        </td>
                        <td align="left" valign="bottom" >
                            <input type="button" id="btnAddNewParty" value="New" class="altMedium0BlueButtonFormat"
                                onfocus="this.blur()" runat="server" onclick="javascript:addNewPartyNET(1);return false;" /><%-- FB 2827 --%>&nbsp;&nbsp;<asp:Image
                                    ID="Image4" src="image/info.png" runat="server" ToolTip="Manually add a participant who is not currently in your organization's address book." />
                            <input type="button" id="VRMLookup" value="Address Book" class="altMedium0BlueButtonFormat"
                                onfocus="this.blur()" runat="server" onclick="javascript:getYourOwnEmailListNET();return false;" /><%-- FB 2827 --%>&nbsp;&nbsp;<asp:Image
                                    ID="Image3" src="image/info.png" runat="server" ToolTip="Contains users who belong to the same organization as you." />
                        </td>
                        </tr>
                    </table>
                    </td>
                   </tr>
                   <%--FB 2426 End--%>
                </table>
            </td>
            <tr>
                <td>
                    <hr style="height: 3px; border-width: 0; background-color: #3685bb" />
                </td>
            </tr>
        </tr>
        <tr style="display: none;">
            <td>
                <img src="image/DoubleLine.jpg" alt="" height="6px" width="100%" />
            </td>
        </tr>
        <tr style="display: none">
            <td>
                <table id="webConf" runat="server" width="100%" border="0">
                    <tr>
                        <td class="subtitlexxsblueblodtext">
                            4) Web Conference Instructions
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table width="100%" border="0">
                                <tr>
                                    <td width="62px">
                                    </td>
                                    <td align="left">
                                        <asp:Table ID="tblWeb" runat="server" CellPadding="3" CellSpacing="2" Visible="true">
                                        </asp:Table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr style="display: none">
            <td>
                <img src="image/DoubleLine.jpg" alt="" height="6px" width="100%" />
            </td>
        </tr>
        <tr style="display: none;">
            <td>
                <img src="image/DoubleLine.jpg" alt="" height="6px" width="100%" />
            </td>
        </tr>
        <tr>
            <td align="center">
                <asp:Table runat="server" Width="100%" CellSpacing="5" CellPadding="2" ID="tblConflict"
                    Style="display: none;">
                    <asp:TableRow ID="TableRow11" runat="server">
                        <asp:TableCell ID="TableCell8" HorizontalAlign="Center" runat="server">
                           <h4>Resolve the following conflicts:</h4>
                        </asp:TableCell></asp:TableRow>
                    <asp:TableRow ID="TableRow12" runat="server">
                        <asp:TableCell ID="TableCell9" HorizontalAlign="Center" runat="server">
                            <asp:DataGrid Width="80%" ID="dgConflict" runat="server" AutoGenerateColumns="False"
                                OnItemDataBound="InitializeConflict">
                                <Columns>
                                    <asp:BoundColumn DataField="formatDate" HeaderText="Date">
                                        <HeaderStyle CssClass="tableHeader" />
                                    </asp:BoundColumn>
                                    <asp:BoundColumn DataField="conflict" HeaderText="Conflict" ItemStyle-Wrap="false">
                                        <HeaderStyle CssClass="tableHeader" />
                                    </asp:BoundColumn>
                                    <asp:TemplateColumn HeaderText="Setup Time">
                                        <HeaderStyle CssClass="tableHeader" />
                                        <ItemTemplate>
                                            <mbcbb:ComboBox CssClass="altText" Style="width: 70px" runat="server" ID="conflictStartTime"
                                                DataValueField='<%# DataBinder.Eval(Container, "DataItem.startHour") %>' DataTextField='<%# DataBinder.Eval(Container, "DataItem.startHour") %>'>
                                                <asp:ListItem Value="01:00 AM"></asp:ListItem>
                                                <asp:ListItem Value="02:00 AM"></asp:ListItem>
                                                <asp:ListItem Value="03:00 AM"></asp:ListItem>
                                                <asp:ListItem Value="04:00 AM"></asp:ListItem>
                                                <asp:ListItem Value="05:00 AM"></asp:ListItem>
                                                <asp:ListItem Value="06:00 AM"></asp:ListItem>
                                                <asp:ListItem Value="07:00 AM"></asp:ListItem>
                                                <asp:ListItem Value="08:00 AM"></asp:ListItem>
                                                <asp:ListItem Value="09:00 AM"></asp:ListItem>
                                                <asp:ListItem Value="10:00 AM"></asp:ListItem>
                                                <asp:ListItem Value="11:00 AM"></asp:ListItem>
                                                <asp:ListItem Value="12:00 PM"></asp:ListItem>
                                                <asp:ListItem Value="01:00 PM"></asp:ListItem>
                                                <asp:ListItem Value="02:00 PM"></asp:ListItem>
                                                <asp:ListItem Value="03:00 PM"></asp:ListItem>
                                                <asp:ListItem Value="04:00 PM"></asp:ListItem>
                                                <asp:ListItem Value="05:00 PM"></asp:ListItem>
                                                <asp:ListItem Value="06:00 PM"></asp:ListItem>
                                                <asp:ListItem Value="07:00 PM"></asp:ListItem>
                                                <asp:ListItem Value="08:00 PM"></asp:ListItem>
                                                <asp:ListItem Value="09:00 PM"></asp:ListItem>
                                                <asp:ListItem Value="10:00 PM"></asp:ListItem>
                                                <asp:ListItem Value="11:00 PM"></asp:ListItem>
                                                <asp:ListItem Value="12:00 AM"></asp:ListItem>
                                            </mbcbb:ComboBox>
                                            <asp:RequiredFieldValidator ID="ReqConflictTime" runat="server" ControlToValidate="conflictStartTime"
                                                Display="Dynamic" ErrorMessage="Required"></asp:RequiredFieldValidator>
                                            <asp:RegularExpressionValidator ID="RegConflictTime" runat="server" ControlToValidate="conflictStartTime"
                                                Display="Dynamic" ErrorMessage="Invalid Time (HH:MM AM/PM)" ValidationExpression="[0|1][0-9]:[0-5][0-9] ?[a|A|p|P][M|m]"></asp:RegularExpressionValidator><%--FB 1715--%>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn>
                                        <HeaderTemplate>
                                            Start Time
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <mbcbb:ComboBox CssClass="altText" Style="width: 70px" runat="server" ID="conflictSetupTime">
                                                <asp:ListItem Value="01:00 AM"></asp:ListItem>
                                                <asp:ListItem Value="02:00 AM"></asp:ListItem>
                                                <asp:ListItem Value="03:00 AM"></asp:ListItem>
                                                <asp:ListItem Value="04:00 AM"></asp:ListItem>
                                                <asp:ListItem Value="05:00 AM"></asp:ListItem>
                                                <asp:ListItem Value="06:00 AM"></asp:ListItem>
                                                <asp:ListItem Value="07:00 AM"></asp:ListItem>
                                                <asp:ListItem Value="08:00 AM"></asp:ListItem>
                                                <asp:ListItem Value="09:00 AM"></asp:ListItem>
                                                <asp:ListItem Value="10:00 AM"></asp:ListItem>
                                                <asp:ListItem Value="11:00 AM"></asp:ListItem>
                                                <asp:ListItem Value="12:00 PM"></asp:ListItem>
                                                <asp:ListItem Value="01:00 PM"></asp:ListItem>
                                                <asp:ListItem Value="02:00 PM"></asp:ListItem>
                                                <asp:ListItem Value="03:00 PM"></asp:ListItem>
                                                <asp:ListItem Value="04:00 PM"></asp:ListItem>
                                                <asp:ListItem Value="05:00 PM"></asp:ListItem>
                                                <asp:ListItem Value="06:00 PM"></asp:ListItem>
                                                <asp:ListItem Value="07:00 PM"></asp:ListItem>
                                                <asp:ListItem Value="08:00 PM"></asp:ListItem>
                                                <asp:ListItem Value="09:00 PM"></asp:ListItem>
                                                <asp:ListItem Value="10:00 PM"></asp:ListItem>
                                                <asp:ListItem Value="11:00 PM"></asp:ListItem>
                                                <asp:ListItem Value="12:00 AM"></asp:ListItem>
                                            </mbcbb:ComboBox>
                                            <asp:RequiredFieldValidator ID="ReqConflictSetupTime" runat="server" ControlToValidate="conflictSetupTime"
                                                Display="Dynamic" ErrorMessage="Required"></asp:RequiredFieldValidator>
                                            <asp:RegularExpressionValidator ID="RegConflictSetupTime" runat="server" ControlToValidate="conflictSetupTime"
                                                Display="Dynamic" ErrorMessage="Invalid Time (HH:MM AM/PM)" ValidationExpression="[0|1][0-9]:[0-5][0-9] ?[a|A|p|P][M|m]"></asp:RegularExpressionValidator><%--FB 1715--%>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn>
                                        <HeaderTemplate>
                                            Teardown time
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <mbcbb:ComboBox CssClass="altText" Style="width: 70px" runat="server" ID="conflictTeardownTime">
                                                <asp:ListItem Value="01:00 AM"></asp:ListItem>
                                                <asp:ListItem Value="02:00 AM"></asp:ListItem>
                                                <asp:ListItem Value="03:00 AM"></asp:ListItem>
                                                <asp:ListItem Value="04:00 AM"></asp:ListItem>
                                                <asp:ListItem Value="05:00 AM"></asp:ListItem>
                                                <asp:ListItem Value="06:00 AM"></asp:ListItem>
                                                <asp:ListItem Value="07:00 AM"></asp:ListItem>
                                                <asp:ListItem Value="08:00 AM"></asp:ListItem>
                                                <asp:ListItem Value="09:00 AM"></asp:ListItem>
                                                <asp:ListItem Value="10:00 AM"></asp:ListItem>
                                                <asp:ListItem Value="11:00 AM"></asp:ListItem>
                                                <asp:ListItem Value="12:00 PM"></asp:ListItem>
                                                <asp:ListItem Value="01:00 PM"></asp:ListItem>
                                                <asp:ListItem Value="02:00 PM"></asp:ListItem>
                                                <asp:ListItem Value="03:00 PM"></asp:ListItem>
                                                <asp:ListItem Value="04:00 PM"></asp:ListItem>
                                                <asp:ListItem Value="05:00 PM"></asp:ListItem>
                                                <asp:ListItem Value="06:00 PM"></asp:ListItem>
                                                <asp:ListItem Value="07:00 PM"></asp:ListItem>
                                                <asp:ListItem Value="08:00 PM"></asp:ListItem>
                                                <asp:ListItem Value="09:00 PM"></asp:ListItem>
                                                <asp:ListItem Value="10:00 PM"></asp:ListItem>
                                                <asp:ListItem Value="11:00 PM"></asp:ListItem>
                                                <asp:ListItem Value="12:00 AM"></asp:ListItem>
                                            </mbcbb:ComboBox>
                                            <asp:RequiredFieldValidator ID="ReqConflictTeardownTime" runat="server" ControlToValidate="conflictTeardownTime"
                                                Display="Dynamic" ErrorMessage="Required"></asp:RequiredFieldValidator>
                                            <asp:RegularExpressionValidator ID="RegConflictTeardownTime" runat="server" ControlToValidate="conflictTeardownTime"
                                                Display="Dynamic" ErrorMessage="Invalid Time (HH:MM AM/PM)" ValidationExpression="[0|1][0-9]:[0-5][0-9] ?[a|A|p|P][M|m]"></asp:RegularExpressionValidator><%--FB 1715--%>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="End time">
                                        <ItemTemplate>
                                            <mbcbb:ComboBox CssClass="altText" Style="width: 70px" runat="server" ID="conflictEndTime">
                                                <asp:ListItem Value="01:00 AM"></asp:ListItem>
                                                <asp:ListItem Value="02:00 AM"></asp:ListItem>
                                                <asp:ListItem Value="03:00 AM"></asp:ListItem>
                                                <asp:ListItem Value="04:00 AM"></asp:ListItem>
                                                <asp:ListItem Value="05:00 AM"></asp:ListItem>
                                                <asp:ListItem Value="06:00 AM"></asp:ListItem>
                                                <asp:ListItem Value="07:00 AM"></asp:ListItem>
                                                <asp:ListItem Value="08:00 AM"></asp:ListItem>
                                                <asp:ListItem Value="09:00 AM"></asp:ListItem>
                                                <asp:ListItem Value="10:00 AM"></asp:ListItem>
                                                <asp:ListItem Value="11:00 AM"></asp:ListItem>
                                                <asp:ListItem Value="12:00 PM"></asp:ListItem>
                                                <asp:ListItem Value="01:00 PM"></asp:ListItem>
                                                <asp:ListItem Value="02:00 PM"></asp:ListItem>
                                                <asp:ListItem Value="03:00 PM"></asp:ListItem>
                                                <asp:ListItem Value="04:00 PM"></asp:ListItem>
                                                <asp:ListItem Value="05:00 PM"></asp:ListItem>
                                                <asp:ListItem Value="06:00 PM"></asp:ListItem>
                                                <asp:ListItem Value="07:00 PM"></asp:ListItem>
                                                <asp:ListItem Value="08:00 PM"></asp:ListItem>
                                                <asp:ListItem Value="09:00 PM"></asp:ListItem>
                                                <asp:ListItem Value="10:00 PM"></asp:ListItem>
                                                <asp:ListItem Value="11:00 PM"></asp:ListItem>
                                                <asp:ListItem Value="12:00 AM"></asp:ListItem>
                                            </mbcbb:ComboBox>
                                            <asp:RequiredFieldValidator ID="ReqConflictEndTime" runat="server" ControlToValidate="conflictEndTime"
                                                Display="Dynamic" ErrorMessage="Required"></asp:RequiredFieldValidator>
                                            <asp:RegularExpressionValidator ID="RegConflictEndTime" runat="server" ControlToValidate="conflictEndTime"
                                                Display="Dynamic" ErrorMessage="Invalid Time (HH:MM AM/PM)" ValidationExpression="[0|1][0-9]:[0-5][0-9] ?[a|A|p|P][M|m]"></asp:RegularExpressionValidator><%--FB 1715--%>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="Action">
                                        <HeaderStyle CssClass="tableHeader" />
                                        <ItemTemplate>
                                            <asp:Button runat="server" ID="btnViewConflict" CssClass="altShortBlueButtonFormat"
                                                Text="View" />
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:BoundColumn DataField="startHour" Visible="False"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="startMin" Visible="False"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="startSet" Visible="False"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="durationMin" Visible="False"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="startDate" Visible="False"></asp:BoundColumn>
                                    <asp:TemplateColumn HeaderText="Delete">
                                        <ItemTemplate>
                                            <asp:CheckBox ID="chkConflictDelete" runat="server" />
                                        </ItemTemplate>
                                        <HeaderStyle CssClass="tableHeader" />
                                    </asp:TemplateColumn>
                                </Columns>
                                <AlternatingItemStyle CssClass="tablebody" />
                                <ItemStyle CssClass="tableBody" />
                                <HeaderStyle CssClass="tableHeader" />
                            </asp:DataGrid>
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow ID="TableRow1" runat="server">
                        <asp:TableCell ID="TableCell1" HorizontalAlign="Center" runat="server">
                            <table width="100%">
                                <tr align="center">
                                    <td align="center">
                                        <input type='submit' name='SoftEdgeTest1' style='width: 0px; display: none' />
                                        <%--<asp:ImageButton ImageUrl="~/en/image/Submit.png" ID="btnConflicts"  runat="server"  Text="Set Custom Instances" OnClick="SetConferenceCustom"/>--%>
                                        <asp:Button ID="btnConflicts" runat="server" CssClass="altMedium0BlueButtonFormat" Width="150pt"
                                            Text="Set Custom Instances" OnClick="SetConferenceCustom" /> <%--FB 2592--%> <%--FB 3030--%>
                                    </td>
                                </tr>
                            </table>
                        </asp:TableCell>
                    </asp:TableRow>
                </asp:Table>
                </asp:Panel>
            </td>
        </tr>
        <tr align="center">
            <td align="center">
                <table width="100%">
                    <tr align="center">
                        <td align="center">
                            <input type='submit' name='SoftEdgeTest1' style='width: 0px; display: none' />
                            <%--<asp:ImageButton ImageUrl="~/en/image/Submit.png" ID="btnConfSubmit" onfocus="this.blur()" runat="server" Text="Submit" OnClientClick="javascript:return Final();" OnClick="SetConference" />--%>
                            <asp:Button ID="btnConfSubmit" Width="195px" CssClass="altMedium0BlueButtonFormat" onfocus="this.blur()"
                                runat="server" Text="Schedule Meeting" OnClientClick="javascript:return Final();"
                                OnClick="EmailDecision" /><%-- FB 2827 --%>
                        </td>
                    </tr>
                    <tr align="right" style="display: none">
                        <%--FB 1830 Email Edit--%>
                        <td align="right" width="1px">
                            <asp:Button ID="btnDummy" BackColor="Transparent" runat="server" OnClick="SetConference" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr id="fileuploadsection">
            <td>
                <ajax:ModalPopupExtender ID="ModalPopupExtender1" runat="server" TargetControlID="aFileUp"
                    PopupControlID="PanloadFiles" DropShadow="false" Drag="true" BackgroundCssClass="modalBackground"
                    CancelControlID="CloseFilePop" BehaviorID="aFileUp">
                </ajax:ModalPopupExtender>
                <asp:Panel ID="PanloadFiles" Width="500px" Height="170px" runat="server" HorizontalAlign="Center"
                    CssClass="treeSelectedNode">
                    <table cellpadding="3" cellspacing="0" border="0" width="80%" class="treeSelectedNode">
                        <tr>
                            <td>
                                <asp:UpdatePanel ID="UpdatePanel1" runat="server" RenderMode="Inline" UpdateMode="Conditional">
                                    <Triggers>
                                    </Triggers>
                                    <ContentTemplate>
                                        <div align="center" id="Div4" style="width: 500Px; height: 170px; vertical-align: middle;
                                            border: none;" class="treeSelectedNode"> <br />
                                            <table cellpadding="3" cellspacing="0" border="0" width="80%">
                                                <tr id="trFile1" runat="server">
                                                    <td align="left" class="blackblodtext">
                                                        File1
                                                    </td>
                                                    <td align="left">
                                                        <table width="100%" cellpadding="0" cellspacing="0">
                                                            <tr>
                                                                <td width="70%"><%--FB 2909 Start--%>
                                                                <div>
                                                                   <input type="text" class="file_input_textbox" readonly="readonly" value='No file selected' style="width: 135px";/>
                                                                    <div class="file_input_div"><input type="button" value="Browse" class="file_input_button" onclick="document.getElementById('FileUpload1').click();return false;" /><%--FB 3055-Filter in Upload Files--%><%--ZD 100420--%>
                                                                      <input type="file"  class="file_input_hidden" id="FileUpload1" tabindex="-1"  contenteditable="false" enableviewstate="true" runat="server" onchange="getfilename(this)"/></div></div>
                                                                      <br />
                                                                      <br />
                                                                    <%--<input type="file" id="FileUpload1" contenteditable="false" enableviewstate="true"
                                                                        size="50" class="altText" runat="server" />--%><%--FB 2909 End--%>
                                                                    <asp:Label ID="lblUpload1" Text="" Visible="false" runat="server"></asp:Label>
                                                                </td>
                                                                <td width="30%" align="left">
                                                                    <%--<asp:ImageButton ID="btnRemove1" ImageUrl="~/en/image/remove.png"  Text="Remove" Visible="false" runat="server" OnCommand="RemoveFile" CommandArgument="1" />--%>
                                                                    <asp:Button ID="btnRemove1" Text="Remove" CssClass="altMedium0BlueButtonFormat" Width="75px" Visible="false"
                                                                        runat="server" OnCommand="RemoveFile" CommandArgument="1" /> <%-- FB 2827 --%>
                                                                    <asp:Label ID="hdnUpload1" Text="" Visible="false" runat="server"></asp:Label>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                                <tr id="trFile2" runat="server">
                                                    <td align="left" class="blackblodtext">
                                                        File2
                                                    </td>
                                                    <td align="left">
                                                        <table width="100%" cellpadding="0" cellspacing="0">
                                                            <tr>
                                                                <td width="70%"><%--FB 2909 Start--%>
                                                                <div>
                                                                   <input type="text" class="file_input_textbox" readonly="readonly" value='No file selected' style="width: 135px";/>
                                                                    <div class="file_input_div"><input type="button" value="Browse" class="file_input_button" /><%--FB 3055-Filter in Upload Files--%>
                                                                      <input type="file"  class="file_input_hidden" id="FileUpload2" contenteditable="false" enableviewstate="true" runat="server" onchange="getfilename(this)"/></div></div>
                                                                    <%--<input type="file" id="FileUpload2" contenteditable="false" enableviewstate="true"
                                                                        size="50" class="altText" runat="server" />--%><%--FB 2909 End--%>
                                                                    <br /><br /><asp:Label ID="lblUpload2" Text="" Visible="false" runat="server"></asp:Label>
                                                                </td>
                                                                <td width="30%">
                                                                    <%--<asp:ImageButton ID="btnRemove2" ImageUrl="~/en/image/remove.png"  Text="Remove" Visible="false" runat="server" OnCommand="RemoveFile" CommandArgument="2" />--%>
                                                                    <asp:Button ID="btnRemove2" Text="Remove" CssClass="altMedium0BlueButtonFormat" Width="75px" Visible="false"
                                                                        runat="server" OnCommand="RemoveFile" CommandArgument="2" /> <%-- FB 2827 --%>
                                                                    <asp:Label ID="hdnUpload2" Text="" Visible="false" runat="server"></asp:Label>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                                <tr id="trFile3" runat="server">
                                                    <td align="left" class="blackblodtext">
                                                        File3
                                                    </td>
                                                    <td align="left">
                                                        <table width="100%" cellpadding="0" cellspacing="0">
                                                            <tr>
                                                                <td width="70%"><%--FB 2909 Start--%>
                                                                <div>
                                                                   <input type="text" class="file_input_textbox" readonly="readonly" value='No file selected' style="width: 135px";/>
                                                                    <div class="file_input_div"><input type="button" value="Browse" class="file_input_button" /><%--FB 3055-Filter in Upload Files--%>
                                                                      <input type="file"  class="file_input_hidden" id="FileUpload3" contenteditable="false" enableviewstate="true" runat="server" onchange="getfilename(this)"/></div></div>
                                                                    <%--<input type="file" id="FileUpload3" contenteditable="false" enableviewstate="true"
                                                                        size="50" class="altText" runat="server" />--%><%--FB 2909 End--%>
                                                                    <br /><br /><asp:Label ID="lblUpload3" Text="" Visible="false" runat="server"></asp:Label>
                                                                </td>
                                                                <td width="30%">
                                                                    <%--<asp:ImageButton ID="btnRemove3" ImageUrl="~/en/image/remove.png"  Text="Remove" Visible="false" runat="server" OnCommand="RemoveFile" CommandArgument="3" />--%>
                                                                    <asp:Button ID="btnRemove3" Text="Remove" Visible="false" CssClass="altMedium0BlueButtonFormat" Width="75px"
                                                                        runat="server" OnCommand="RemoveFile" CommandArgument="3" /> <%-- FB 2827 --%>
                                                                    <asp:Label ID="hdnUpload3" Text="" Visible="false" runat="server"></asp:Label>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                                <br />
                                <%--<asp:ImageButton ID="btnUploadFiles" OnClick="UploadFiles" onfocus="this.blur()" ImageUrl="~/en/image/upload.png" runat="server" />--%>
                                <%--<asp:ImageButton ID="CloseFilePop" OnClientClick="CheckUploadedFiles();" onfocus="this.blur()"
                                                ImageUrl="~/en/image/close.png" runat="server" />--%>
                                <asp:Button ID="btnUploadFiles" Width="100" Text="Upload Files" CssClass="altMedium0BlueButtonFormat"
                                    OnClick="UploadFiles" onfocus="this.blur()" runat="server" /><%-- FB 2779 FB 2827 --%>
                                <asp:Button ID="CloseFilePop" Width="75" Text="Close" CssClass="altMedium0BlueButtonFormat" OnClientClick="CheckUploadedFiles();"
                                    onfocus="this.blur()" runat="server" /><%-- FB 2827 --%><br /><br />
                            </td>
                        </tr>
                    </table>
                </asp:Panel>

                <script type="text/javascript" language="javascript">

                    function CheckFiles() {
                        if (typeof (Page_ClientValidate) == 'function')
                            if (!Page_ClientValidate()) {
                            DataLoading(0);
                            return false;
                        }


                        if (document.getElementById("FileUpload1")) {
                            if (document.getElementById("FileUpload1").value) {
                                if (confirm("Do you want to upload the browsed file?")) {
                                    document.getElementById("__EVENTTARGET").value = "btnUploadFiles";
                                    WebForm_DoPostBackWithOptions(new WebForm_PostBackOptions("btnUploadFiles", "", true, "", "", false, false));
                                }
                                else {
                                    DataLoading(0);
                                    return false;
                                }
                            }
                        }
                        if (document.getElementById("FileUpload2")) {
                            if (document.getElementById("FileUpload2").value) {
                                if (confirm("Do you want to upload the browsed file?")) {
                                    document.getElementById("__EVENTTARGET").value = "btnUploadFiles";
                                    WebForm_DoPostBackWithOptions(new WebForm_PostBackOptions("btnUploadFiles", "", true, "", "", false, false));
                                }
                                else
                                    DataLoading(0);
                            }
                        }
                        if (document.getElementById("FileUpload3")) {
                            if (document.getElementById("FileUpload3").value) {
                                if (confirm("Do you want to upload the browsed file?")) {
                                    document.getElementById("__EVENTTARGET").value = "btnUploadFiles";
                                    WebForm_DoPostBackWithOptions(new WebForm_PostBackOptions("btnUploadFiles", "", true, "", "", false, false));
                                }
                                else
                                    DataLoading(0);
                            }
                        }

                        return true;
                    }                               
                      
                        changeVMR();//FB 2501
                </script>

            </td>
        </tr>
        <tr id="modalrooms">
            <asp:Button ID="CustomTrigger" runat="server" Style="display: none;" />
            <td>
                <ajax:ModalPopupExtender ID="RoomPopUp" runat="server" TargetControlID="CustomTrigger"
                    Enabled="true" PopupControlID="PopupRoomPanel" DropShadow="false" Drag="true"
                    BackgroundCssClass="modalBackground" CancelControlID="ClosePUpbutton" BehaviorID="RoomPopUp">
                </ajax:ModalPopupExtender>
                <%--FB 1858,1835 Start--%>
                <asp:Panel ID="PopupRoomPanel" Width="60%" Height="60%" runat="server" HorizontalAlign="Center"
                    CssClass="treeSelectedNode" ScrollBars="Auto">
                    <table cellpadding="3" cellspacing="0" border="0" width="100%">
                        <tr>
                            <td valign="top" align="right">
                                <asp:ImageButton ID="ClosePUp" ImageUrl="../image/Close.JPG" runat="server" OnClientClick="javascript:return ProccedClick();"
                                    ToolTip="Close"></asp:ImageButton>
                                <%--FB 1858,1835 End--%>
                                <asp:UpdatePanel ID="UpdatePanelRooms" runat="server" RenderMode="Inline" UpdateMode="Conditional">
                                    <Triggers>
                                    </Triggers>
                                    <ContentTemplate>
                                        <asp:Button ID="refreshrooms" runat="server" OnClick="RefreshRoom" CausesValidation="false"
                                            Style="display: none;" />
                                        <div align="center" id="conftypeDIV" style="width: 100%; height: 100%; border: none;"
                                            class="treeSelectedNode">
                                            <%--FB 1858,1835--%>
                                            <table border="0" cellpadding="3" cellspacing="0" width="100%">
                                                <tr>
                                                    <td align="right" valign="top" width="10%">
                                                    </td>
                                                    <td align="left" class="blackblodtext" valign="top" width="78%">
                                                        <table border="0" style="width: 100%">
                                                            <tr>
                                                                <td valign="top" align="left" width="80">
                                                                    <%-- <asp:ImageButton ID="btnCompare" onfocus="this.blur()"  OnClientClick="javascript:compareselected();" ImageUrl="~/en/image/compare.png" 
                                                        runat="server" />--%>
                                                                    <asp:Button ID="btnCompare" Text="Compare" CssClass="altShortBlueButtonFormat" onfocus="this.blur()"
                                                                        OnClientClick="javascript:compareselected();" runat="server" />
                                                                </td>
                                                                <td nowrap valign="top" align="left" class="blackblodtext">
                                                                    <asp:RadioButtonList ID="rdSelView" runat="server" CssClass="blackblodtext" OnSelectedIndexChanged="rdSelView_SelectedIndexChanged"
                                                                        RepeatDirection="Horizontal" AutoPostBack="True" RepeatLayout="Flow">
                                                                        <asp:ListItem Selected="True" Value="1"><span class="blackblodtext">Level View</span></asp:ListItem>
                                                                        <asp:ListItem Value="2"><span class="blackblodtext">List View</span></asp:ListItem>
                                                                    </asp:RadioButtonList>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td width="10%" align="right" valign="top" class="blackblodtext">
                                                        Rooms<br><%--FB 2579 End--%>
                                                        <font size="1">Click room name to show room resource details.</font>
                                                    </td>
                                                    <td width="78%" align="left" style="font-weight: bold; font-size: small; color: green;
                                                        font-family: arial" valign="top">
                                                        <asp:Panel ID="pnlLevelView" runat="server" Height="300px" Width="100%" ScrollBars="Auto"
                                                            BorderColor="Blue" BorderStyle="Solid" BorderWidth="1px" HorizontalAlign="Left">
                                                            <%--FB 1858,1835--%>
                                                            <asp:TreeView ID="treeRoomSelection" runat="server" BorderColor="White" Height="95%"
                                                                ShowCheckBoxes="All" onclick="javascript:getRooms(event)" ShowLines="True" Width="94%"
                                                                OnTreeNodeCheckChanged="treeRoomSelection_TreeNodeCheckChanged" OnSelectedNodeChanged="treeRoomSelection_SelectedNodeChanged">
                                                                <NodeStyle CssClass="treeNode" />
                                                                <RootNodeStyle CssClass="treeRootNode" />
                                                                <ParentNodeStyle CssClass="treeParentNode" />
                                                                <LeafNodeStyle CssClass="treeLeafNode" />
                                                            </asp:TreeView>
                                                        </asp:Panel>
                                                        <asp:Panel ID="pnlListView" runat="server" BorderColor="Blue" BorderStyle="Solid"
                                                            BorderWidth="1px" Height="300px" ScrollBars="Auto" Visible="False" Width="100%"
                                                            HorizontalAlign="Left" Direction="LeftToRight" Font-Bold="True" Font-Names="arial"
                                                            Font-Size="Small" ForeColor="Green">
                                                            <%--FB 1858,1835--%>
                                                            <input type="checkbox" id="selectAllCheckBox" runat="server" onclick="CheckBoxListSelect('lstRoomSelection',this);" /><font
                                                                size="2"> Select All</font>
                                                            <br />
                                                            <asp:CheckBoxList ID="lstRoomSelection" runat="server" Height="95%" Width="95%" Font-Size="Smaller"
                                                                ForeColor="ForestGreen" onclick="javascript:getValues(event)" Font-Names="arial"
                                                                RepeatLayout="Flow">
                                                            </asp:CheckBoxList>
                                                        </asp:Panel>
                                                        <asp:Panel ID="pnlNoData" runat="server" BorderColor="Blue" BorderStyle="Solid" BorderWidth="1px"
                                                            Height="300px" ScrollBars="Auto" Visible="False" Width="100%" HorizontalAlign="Left"
                                                            Direction="LeftToRight" Font-Size="Small">
                                                            <%--FB 1858,1835--%>
                                                            <table>
                                                                <tr align="center">
                                                                    <td>
                                                                        You have no Room(s) available for the selected Date/Time.
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </asp:Panel>
                                                        <asp:TextBox runat="server" ID="txtTemp" Text="test" Visible="false"></asp:TextBox>
                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" Enabled="false" ControlToValidate="txtTemp"
                                                            runat="server"></asp:RequiredFieldValidator>
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                                <br />
                                <%--<asp:ImageButton ID="ClosePUp1" OnClientClick="OpenSetRoom()" ImageUrl="~/en/image/close.png" runat="server" />--%><%--FB 1858,1835--%>
                            </td>
                        </tr>
                        <tr>
                            <td align="center">
                                <asp:Button ID="ClosePUpbutton" Text="Proceed" CssClass="altShortBlueButtonFormat"
                                    OnClientClick="javascript:OpenSetRoom();" runat="server" />
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </td>
        </tr>
        <tr id="modalConfirmation">
            <td>
                <ajax:ModalPopupExtender ID="SubModalPopupExtender" TargetControlID="adisNone" runat="server"
                    PopupControlID="panSubmit" DropShadow="false" BackgroundCssClass="modalBackground"
                    CancelControlID="showConfMsgImg">
                </ajax:ModalPopupExtender>
                <asp:Panel ID="panSubmit" Width="40%" Height="40%" runat="server" HorizontalAlign="Center"
                    CssClass="treeSelectedNode">
                    <%--FB 1858,1835--%>
                    <asp:UpdatePanel ID="UpdatePanel4" runat="server" RenderMode="Inline" UpdateMode="Conditional">
                        <Triggers>
                        </Triggers>
                        <ContentTemplate>
                            <div align="center" id="Div1" style="width: 100%; height: 100%;" class="treeSelectedNode">
                                <%--FB 1858,1835--%>
                                <table border="0" cellpadding="3" cellspacing="0" width="100%" style="vertical-align: middle;
                                    height: 40%;">
                                    <tr>
                                        <td align="center" style="vertical-align: middle">
                                            <asp:Label ID="showConfMsg" runat="server" CssClass="lblError" Text="Work In-Progress"></asp:Label>
                                            <br />
                                            <br />
                                            <br />
                                            <br />
                                            <br />
                                            <br />
                                            <%--<asp:ImageButton ID="showConfMsgImg" ImageUrl="~/en/image/close.png" OnClientClick="javascript:window.location.replace('ExpressConference.aspx?t=n');" runat="server"/>--%>
                                            <asp:Button ID="showConfMsgImg" Text="Close" CssClass="altMedium0BlueButtonFormat"
                                                OnClientClick="javascript:window.location.replace('ExpressConference.aspx?t=n'); return false;"
                                                runat="server" />
                                            <br />
                                            <br />
                                            <br />
                                            <br />
                                            <br />
                                            <br />
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </asp:Panel>
            </td>
        </tr>
        <tr>
            <td>
                <input type="button" id="adisNone" runat="server" style="display: none" />
            </td>
        </tr>
    </table>
    
    <%--FB 2693 Start--%>
    
    <div id="divPCdetails" class="rounded-corners" style="left: 425px; position: absolute;
        background-color: White; top: 420px; min-height:200px; z-index: 9999; overflow: hidden;
        border: 0px; width: 700px; display: none;">
        <table width="100%">
            <tr style='height: 25px;'>
                <td colspan="3" style='background-color: #3075AE;' align='center'>
                    <b style='font-size: small; margin-left: 10px; color: White; font-family: Verdana;
                        font-style: normal;'>INFORMATION</b>
                </td>
             </tr>                        
        </table>
        <div style="height:500px; overflow-y:scroll">
        <table  border="0" cellpadding="0"  cellspacing="0" width="100%" id="Table2">
             <tr>
                <td colspan="3" height="20px;"></td>
             </tr>
            <tr>
                <td colspan="3" >
                    <div id="PCHtmlContent"></div>
                </td>
            </tr>           
        </table>
        </div>
        <table width="100%">
            <tr style="height: 40px;">
                <td align="center" colspan="3">
                    <button id="btnPCCancel" class="altMedium0BlueButtonFormat" style="width:75px">
                        Close</button> <%-- FB 2827 --%>
                </td>
            </tr>
        </table>                
    </div>
    
    <%--FB 2693 End--%>
    </form>

    <script type="text/javascript">


        //FB 2620 Starts  FB Icon
        if (document.getElementById("lstVMR") != null) {
            var lstVMR = document.getElementById("lstVMR").selectedIndex;
            if (lstVMR == 1)
                document.getElementById("divbridge").style.display = "Block";
        }
        //FB 2620 Ends
        
        
	//FB 2634
        if (document.getElementById("confStartTime_Text")) 
    {
        var confstarttime_text = document.getElementById("confStartTime_Text");
        confstarttime_text.onblur = function() {
            if(formatTime('confStartTime_Text','regStartTime'))
                return ChangeEndDate()
        };
    }
    if (document.getElementById("confEndTime_Text")) 
    {
        var confendtime_text = document.getElementById("confEndTime_Text");
        confendtime_text.onblur = function() {
            if(formatTime('confEndTime_Text','regEndTime'))
                return ChangeEndDate("ET")
        };
    }
        if (document.getElementById("<%=rdSelView.ClientID%>")) {
            if (!document.getElementById("<%=rdSelView.ClientID%>").disabled)
                document.getElementById("<%=rdSelView.ClientID%>").onclick = function()
                { DataLoading(1); };
        }

        function openconflict() {
            if (document.getElementById("dgconflict")) {
                document.getElementById("dgconflict").focus();
            }
        }

        function ChangeDate(inVal) {
            var setupTime = new Date(GetDefaultDate(document.getElementById("confStartDate").value, '<%=format%>') + " "
            + document.getElementById("confStartTime_Text").value);

            var startTime = new Date(GetDefaultDate(document.getElementById("SetupDate").value, '<%=format%>') + " "
            + document.getElementById("SetupTime_Text").value);

            var endTime = new Date(GetDefaultDate(document.getElementById("TearDownDate").value, '<%=format%>') + " "
            + document.getElementById("TeardownTime_Text").value);


            if (inVal == "Set") {
                document.getElementById("SetupDate").value = getCDate(setupTime);
                document.getElementById("SetupTime_Text").value = getCTime(setupTime);
                var startTime = new Date(GetDefaultDate(document.getElementById("SetupDate").value, '<%=format%>') + " "
            + document.getElementById("SetupTime_Text").value);

                startTime = setCDuration(startTime, 60);
                document.getElementById("TearDownDate").value = getCDate(startTime);
                document.getElementById("TeardownTime_Text").value = getCTime(startTime);
                var endTime = new Date(GetDefaultDate(document.getElementById("TearDownDate").value, '<%=format%>') + " "
            + document.getElementById("TeardownTime_Text").value);

                endTime = setCDuration(endTime, 0);
                document.getElementById("confEndDate").value = getCDate(endTime);
                document.getElementById("confEndTime_Text").value = getCTime(endTime);
            }
            else if (inVal == "Start") {
                startTime = setCDuration(startTime, 60);
                document.getElementById("TearDownDate").value = getCDate(startTime);
                document.getElementById("TeardownTime_Text").value = getCTime(startTime);
                var endTime = new Date(GetDefaultDate(document.getElementById("TearDownDate").value, '<%=format%>') + " "
            + document.getElementById("TeardownTime_Text").value);

                endTime = setCDuration(endTime, 0);
                document.getElementById("confEndDate").value = getCDate(endTime);
                document.getElementById("confEndTime_Text").value = getCTime(endTime);
            }
            else if (inVal == "End") {

            }
        }
    </script>

    <script type="text/javascript">
      //SelectAudioParty();//@@@@@
	  fnEnableBuffer();
	  if(document.getElementById("hdnValue").value == "" || document.getElementById("hdnValue").value == "0")
	  {
	    if (document.frmSettings2.EndText)
		    document.frmSettings2.EndText.disabled = true;
	    if (document.frmSettings2.DurText)
		    document.frmSettings2.DurText.disabled = true;

	    document.frmSettings2.RecurValue.value = document.getElementById("Recur").value;
        
        var chkrecurrence = document.getElementById("chkRecurrence");
         
        if(document.getElementById("Recur").value != "" || (chkrecurrence && chkrecurrence.checked == true))
        {
            //var chkrecurrence = document.getElementById("chkRecurrence");
            if(chkrecurrence)
                chkrecurrence.checked = true;
            document.getElementById("hdnRecurValue").value = 'R'
            //FB 2634        
            openRecur();
        
            //initial(); FB 2771
           
            //fnShow();
        }
        
         fnEnableBuffer();
     } 
    
    //FB 2634
    if(document.getElementById("Recur").value == "")
    {
        initial();        
        fnShow();
    }
    
    function OpenRoom()
     {     
        
            
        if(document.getElementById("refreshrooms"))
            document.getElementById("refreshrooms").click();
                  
        SubmitRecurrence();
        
        var popup2 = $find('RoomPopUp');
        if(popup2)
            popup2.show();
            
        return false;
     }
     //if ("<%=Session["isExpressUserAdv"]%>" == "0") //FB 2394 -LineRate displays to all Express form.
       //document.getElementById("trLineRate").style.display = 'None';
     
    //FB 1911 - Start
    if ('<%=isEditMode%>' == "1" || "<%=Session["isSpecialRecur"]%>" == "0")
    {
        if(document.getElementById("SPCell1"))
            document.getElementById("SPCell1").style.display = 'None'
        
        if(document.getElementById("SPCell2"))
            document.getElementById("SPCell2").style.display = 'None'
    }    
    
	//if(document.getElementById("hdnValue").value == "" || document.getElementById("hdnValue").value == "0")
	//{
        if(document.getElementById("RecurSpec").value != "")
        {
           showSpecialRecur();
           document.getElementById("RecurText").value = document.getElementById("RecurringText").value;
        }
    //}
    //FB 1911 - End
    
    //FB 2266 
    function refreshIframe()
    {
        var iframeId = document.getElementById('ifrmPartylist');
        iframeId.src = iframeId.src;
    }
    
   
    if(document.getElementById("lstDuration_Container") != null)
        document.getElementById("lstDuration_Container").style.width = "auto";
    
  function OpenRoomSearchresponse() //FB 2367
  {
  
  ChangeTimeFormat("D"); //FB 2588
  
  //FB 2634
      var dStart = GetDefaultDate(document.getElementById("confStartDate").value,'<%=format%>');
    var dEnd = GetDefaultDate(document.getElementById("confEndDate").value,'<%=format%>');
    //var dSetup = GetDefaultDate(document.getElementById("SetupDate").value,'<%=format%>');
    var isROOMVMR = "0"; //FB 2448
    dStart = dStart + " " + document.getElementById("hdnStartTime").value; //FB 2588
    dEnd = dEnd + " " + document.getElementById("hdnEndTime").value;//FB 2588
    
    if('<%=EnableBufferZone%>' == 1)//FB 2634
    {
        var cstartdate = new Date(GetDefaultDate(document.getElementById("confStartDate").value,'<%=format%>') + " "
                + document.getElementById("hdnStartTime").value); //FB 2588
        var dura = parseInt(document.getElementById("SetupDuration").value,10);
        var sttime = setCDuration(cstartdate, -dura)
        var sttime1 = getCTime(sttime);
    	
        dStart = GetCurrentdatetime(sttime,0,0);
    } 
    
     //dSetup = dEnd + " " + document.getElementById("SetupTime_Text").value;   //FB 2534
     //dSetup = dSetup + " " + document.getElementById("SetupTime_Text").value;   //FB 2534
     
     //FB 2534 - Starts
     /*if(new Date(dStart).getTime() < new Date().getTime() || new Date(dEnd).getTime() < new Date().getTime() || new Date(dSetup).getTime() < new Date().getTime())
     {
	   alert("Invalid Date/Time");
	   return false;
	 }*/
	 //FB 2534 - End
	 var immediate = "0";//FB 2534
	 if(document.getElementById("chkStartNow").checked)
	 {
	    immediate = "1";
	    //FB 2534 - Starts
	    var currentDate = new Date();
	    dStart = GetCurrentdatetime(currentDate,0,0);
	    //FB 2534 - End
	    if(document.getElementById("lstDuration_Text"))
	    {
	        var dura = document.getElementById("lstDuration_Text").value.split(':');
	        //FB 2534 - Starts
	        //dStart = GetCurrentdatetime(dura[0],dura[1]);
	        dEnd = GetCurrentdatetime(currentDate,dura[0],dura[1]);//FB 2534
	        //FB 2534 - End
	    }
	 }
	 if (document.getElementById("lstVMR")) //FB 2448
     {
        if (document.getElementById("lstVMR").selectedIndex > 0) //FB 2620
            isROOMVMR = "1";
     }
     var cloudConf = 0;
     if(document.getElementById("chkCloudConferencing") != null && document.getElementById("lstVMR") != null)
        if ('<%=isCloudEnabled%>' == 1 && document.getElementById("lstVMR").selectedIndex > 0 && document.getElementById("chkCloudConferencing").checked) //FB 2717
           cloudConf = 1;
      
    var url   = "RoomSearch.aspx?rmsframe=" + document.getElementById("selectedloc").value + "&confID=" + '<%=lblConfID.Text%>'
            + "&stDate=" + dStart + "&enDate=" + dEnd + "&tzone=" + document.getElementById("<%=lstConferenceTZ.ClientID%>").value 
            + "&serType=-1&hf=1" + "&isVMR=" + isROOMVMR + "&CloudConf=" + cloudConf + "&immediate=" + immediate + "&ConfType=2"; //FB 2448 //FB 2534 //FB 2694//FB 2717
 
   window.open(url, "RoomSearch", "width="+ screen. availWidth +",height=650px,resizable=no,scrollbars=yes,status=no,top=0,left=0"); 
    return false;
 }
 
 
 function GetCurrentdatetime(strDateTime, addHr,addmin) //FB 2367 //FB 2634-Starts
 {
	 var day = strDateTime.getDate();
     var month = strDateTime.getMonth() + 1;
     var year = strDateTime.getFullYear();
	 var hours = parseInt(strDateTime.getHours(),10) + parseInt(addHr,10);
     var minutes = parseInt(strDateTime.getMinutes(),10) + parseInt(addmin,10); //FB 2634-End
	 var suffix = "AM";
	 
     if (hours >= 12)
      {
          suffix = "PM";
          hours = hours - 12;
      }
     
      if (hours == 0)
          hours = 12;
      
      if(minutes < 10)
        minutes = "0" + minutes;
        
      if(month < 10)
        month = "0" + month;
           
      if(day < 10)
         day = "0" + day;
       
       if(hours < 10)
         hours = "0" + hours;
       //FB 2534 - Starts   
       //return  day + "/" + month + "/" + year + " " + hours + ":" + minutes + " " + suffix;
       return  month + "/" + day + "/" + year + " " + hours + ":" + minutes + " " + suffix;
       //FB 2534 - End
 }
 
    </script>

</body>
</html>
<!-- FB 2719 Starts -->
<% if (Session["isExpressUser"].ToString() == "1"){%>
<!-- #INCLUDE FILE="inc/mainbottomNETExp.aspx" -->
<%}else{%>
<!-- #INCLUDE FILE="inc/mainbottomNET.aspx" -->
<%}%>
<!-- FB 2719 Ends -->
<!--Window Dressing-->

<%--FB 1822--%>
<%--<script type="text/javascript" src="inc/softedge.js"></script>--%>
<script type="text/javascript">
    // FB 2828 Starts
    function setMarqueeWidth() {
        var screenWidth = screen.width - 25;
        if (document.getElementById('martickerDiv') != null)
            document.getElementById('martickerDiv').style.width = screenWidth + 'px';

        if (document.getElementById('marticDiv') != null)
            document.getElementById('marticDiv').style.width = screenWidth + 'px';

        if (document.getElementById('marticker2Div') != null)
            document.getElementById('marticker2Div').style.width = (screenWidth - 15) + 'px';

        if (document.getElementById('martic2Div') != null)
            document.getElementById('martic2Div').style.width = (screenWidth - 15) + 'px';
    }
    setMarqueeWidth();
    // FB 2828 Ends
    
    //FB 2426 Start
    function fnchangetype() {
        fnHideStartNow();//FB 1825
        document.getElementById('txtIPPassword').type = "password";
        document.getElementById('txtIPconfirmPassword').type = "password";
        document.getElementById('txtISDNPassword').type = "password";
        document.getElementById('txtISDNconfirmPassword').type = "password";
        document.getElementById('txtSIPPassword').type = "password";
        document.getElementById('txtSIPconfirmPassword').type = "password";

    }
    window.onload = fnchangetype;
    //FB 2426 End

    //FB 2487 - Start
    if (document.getElementById("showConfMsg") != null)
        var obj = document.getElementById("showConfMsg");

    if (obj != null) {
    
        var strInput = obj.innerHTML.toUpperCase();

        if ((strInput.indexOf("SUCCESS") > -1) && !(strInput.indexOf("UNSUCCESS") > -1) && !(strInput.indexOf("ERROR") > -1)) {
            obj.setAttribute("class", "lblMessage");
            obj.setAttribute("className", "lblMessage");
        }
        else {
            obj.setAttribute("class", "lblError");
            obj.setAttribute("className", "lblError");
        }
    }
    //FB 2487 - End

        
    //FB 1825 Start
    function fnHideStartNow()
    {
    
        var startNow = document.getElementById('hdnSetStartNow').value;
        if (startNow == "hide" && chkrecurrence.checked == false) 
        {
            document.getElementById("StartNowRow").style.display = "none";//FB 2634
        }
    }
    //FB 1825 End

    //FB 2717 Starts
    function fnChkCloudConference() {

        var chkCloud = document.getElementById("chkCloudConferencing");
        if (chkCloud && chkCloud.checked) {
            document.getElementById("lstVMR").selectedIndex = 3;
            document.getElementById("lstVMR").disabled = true;
            document.getElementById("divbridge").style.display = "none";
        }
        else {
            document.getElementById("lstVMR").disabled = false;
            document.getElementById("lstVMR").selectedIndex = 0;//FB Vidyo_J
        }
        changeVMR();
    }
    //FB 2717 End
    //FB 2998
    if("<%=mcuSetupDisplay %>" == "1")
        document.getElementById("ConnectCell").style.display = "";
    else
        document.getElementById("ConnectCell").style.display = "None";
    if("<%=mcuTearDisplay %>" == "1")
        document.getElementById("DisconnectCell").style.display = "";
    else
        document.getElementById("DisconnectCell").style.display = "None";
</script>