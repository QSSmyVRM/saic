<%--ZD 100147 start--%>
<%--/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.	
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/--%><%--ZD 100147 End--%>
<%@ Page Language="C#" AutoEventWireup="true" Inherits="HDConferenceList" EnableEventValidation="false"
    Debug="true" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd"> <%--FB 2779--%>
<meta http-equiv="X-UA-Compatible" content="IE=8" /><%--FB 2779--%>
<!-- #INCLUDE FILE="inc/maintopNET.aspx" -->
<%--FB 2050--%>

<%@ Register Assembly="DevExpress.Web.ASPxGridView.v10.2, Version=10.2.3.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.ASPxEditors.v10.2, Version=10.2.3.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<%--Basic validation function--%>

<script type="text/javascript">

    var servertoday = new Date();

    function fnOpen() {
        var args = fnOpen.arguments;
        var hdnValue = document.getElementById("hdnValue");
        hdnValue.value = args[1];
        if (args[0] == "V") {
            url = "ManageConference.aspx?t=hf&confid=" + args[1];
            confdetail = window.open(url, "viewconference", "width=1,height=1,resizable=yes,scrollbars=yes,status=no");
            confdetail.focus();
        }
        else if (args[0] == "D") {
            if (confirm("Are you sure you want to delete this conference?"))
                return true;
            else {
                DataLoading('0');
                hdnValue.value = "";
                return false;
            }
        }

    }
     
</script>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>Report</title>

    <script type="text/javascript" src="inc/functions.js"></script>

    <style type="text/css">
        LABEL
        {
            font-size: 8pt;
            vertical-align: top;
            color: black;
            font-family: Arial, Helvetica;
            text-decoration: none;
        }
    </style>
</head>
<body>
    <form id="frmReport" runat="server">
    <input type="hidden" id="hdnValue" runat="server" />
    <input type="hidden" id="hdnRequestID" runat="server" />
    <table width="100%" border="0" cellpadding="0" cellspacing="0">
        <tr>
            <td colspan="2" align="center" height="10px">
                <h3>
                    <asp:Label ID="lblHeading" runat="server">Hotdesking Reservations</asp:Label>
                </h3>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <table width="100%">
                    <tr>
                        <td align="center">
                            <asp:Label ID="errLabel" runat="server" Text="" CssClass="lblError"></asp:Label><br />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td valign="top">
                <table width="100%" align="center" border="0">
                    <tr valign="top" runat="server" id="trDetails">
                        <td align="left">
                            <div id="MainDiv" style="overflow-y: auto; overflow-x: auto; word-break: break-all;
                                height: 440px; width: 100%;">
                                <dx:ASPxGridView ID="MainGrid" ClientInstanceName="MainGrid" runat="server" Width="100%"
                                    EnableCallBacks="false" AllowSort="true" OnHtmlRowCreated="MainGrid_HtmlRowCreated"
                                    KeyFieldName="ConferenceID" Styles-Header-Wrap="True" Styles-Cell-HorizontalAlign="Left">
                                    <Settings ShowFilterRow="True" ShowTitlePanel="True" ShowHorizontalScrollBar="true"
                                        ShowVerticalScrollBar="false" ShowHeaderFilterBlankItems="False" />
                                    <SettingsPager ShowDefaultImages="False" Mode="ShowPager" AlwaysShowPager="true"
                                        Position="Top">
                                        <AllButton Text="All">
                                        </AllButton>
                                        <NextPageButton Text="Next &gt;">
                                        </NextPageButton>
                                        <PrevPageButton Text="&lt; Prev">
                                        </PrevPageButton>
                                    </SettingsPager>
                                </dx:ASPxGridView>
                                <div style="float: right">
                                    <%--FB 2763--%>
				    <br/>
                                    <asp:Button ID="btnCancel" Visible="false" CssClass="altMedium0BlueButtonFormat"
                                        Text="Go Back" runat="server" OnClick="GoBack" />
                                </div>
                            </div>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    </form>
</body>
</html>
<%--code added for Soft Edge button--%>
<%--<script type="text/javascript">
    var mainDiv = document.getElementById("MainDiv");
    if(mainDiv)
    {  //Difference 180
    
        if (window.screen.width <= 1024)
            mainDiv.style.width = "983px";
        else
            mainDiv.style.width = "1184px";        
    }
    
         
</script>
--%>
<!-- #INCLUDE FILE="inc/mainbottomNET.aspx" -->
