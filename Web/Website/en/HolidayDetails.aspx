<%--ZD 100147 start--%>
<%--/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.	
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/--%><%--ZD 100147 End--%>
<%@ Page Language="C#" Inherits="ns_HolidayDetails.HolidayDetails" Buffer="true" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> <!-- FB 2050 -->
<meta http-equiv="X-UA-Compatible" content="IE=8" /> <!-- FB 2050 -->
<!-- #INCLUDE FILE="inc/maintopNET.aspx" --> 
<script language="javascript">

function ManageOrder ()
{
	change_mcu_order_prompt('image/pen.gif', 'Manage Day Order', document.getElementById('hdnValue').value, "Color days");
}

function frmsubmit()
{
    if(document.getElementById("btnManage")!= null)
        document.getElementById("btnManage").click();
}


function fnCancel() {
    DataLoading(1); //ZD 100176
	window.location.replace('organisationsettings.aspx');
}
//ZD 100176 start
function DataLoading(val) {
    if (val == "1")
        document.getElementById("dataLoadingDIV").innerHTML = "<img border='0' src='image/wait1.gif'>";
    else
        document.getElementById("dataLoadingDIV").innerHTML = "";
}
//ZD 100176 End
</script>
<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Manage Color Day Details</title>
    <script type="text/javascript" src="inc/functions.js"></script>
    <script type="text/javascript" src="script/mousepos.js"></script>
    <script type="text/javascript" src="script/managemcuorder.js"></script>

</head>
<body>
    <form id="frmManagebridge" runat="server" method="post">
        <input type="hidden" id="Bridges" runat="server" />
        <input type="hidden" id="hdnValue" runat="server" />
    <div>
        <table width="100%">
            <tr>
                <td align="center">
                    <h3>
                        <asp:Label ID="lblHeader" runat="server"></asp:Label>
                    </h3>
                </td>
            </tr>
            <tr>
                <td align="center">
                    <asp:Label ID="errLabel" runat="server" Text="" CssClass="lblError"></asp:Label>
                    <div id="dataLoadingDIV" align="center"></div><%--ZD 100176--%>
                </td>
            </tr>
            <tr>
                <td align="center">
                    <asp:DataGrid ID="dgHolidayDetails" runat="server" AutoGenerateColumns="False" CellPadding="4" GridLines="None"
                         BorderColor="blue" BorderStyle="solid" BorderWidth="1" ShowFooter="true" OnItemCreated="DeleteHolidayTypeMsg"
                        OnDeleteCommand="DeleteHolidayDetails" OnEditCommand="EditDetails" Width="60%" Visible="true" 
                        OnItemDataBound="dgHolidayDetails_ItemDataBound" style="border-collapse:separate"> 
                        <SelectedItemStyle  CssClass="tableBody"  Font-Bold="True"  />
                        <EditItemStyle CssClass="tableBody" />
                        <AlternatingItemStyle CssClass="tableBody" />
                        <ItemStyle CssClass="tableBody" />
                        <FooterStyle CssClass="tableBody" />
                        <HeaderStyle CssClass="tableHeader" />
                        <Columns>
                            <asp:BoundColumn DataField="id" Visible="false" ><HeaderStyle CssClass="tableHeader" /></asp:BoundColumn>
                            <asp:BoundColumn DataField="RowUID" HeaderText="S.No" ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left"><HeaderStyle CssClass="tableHeader" /></asp:BoundColumn>
                            <asp:TemplateColumn HeaderText="Name" ItemStyle-CssClass="tableBody" HeaderStyle-CssClass="tableHeader" ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left"> <%--FB 2918--%>
                                <ItemTemplate>
                                    <asp:Label ID="lblHoliday" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.HolidayDescription") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="Color" ItemStyle-CssClass="tableBody" HeaderStyle-CssClass="tableHeader" ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left" >
                                <ItemTemplate>
                                    <asp:TextBox ID="txtColor" runat="server" Width="60px" Text='<%# DataBinder.Eval(Container, "DataItem.Color") %>'  ReadOnly="true"></asp:TextBox>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="Actions" ItemStyle-Width="15%" ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left">
                                <HeaderStyle CssClass="tableHeader" />
                                <ItemTemplate>
                                    <asp:LinkButton runat="server" Text="Edit" ID="btnEdit" CommandName="Edit"></asp:LinkButton>
                                    &nbsp;<asp:LinkButton runat="server" Text="Delete" ID="btnDelete" CommandName="Delete"></asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                        </Columns>
                    </asp:DataGrid>
                    <asp:Table runat="server" ID="tblNoDetails" Visible="false" Width="90%">
                        <asp:TableRow CssClass="lblError">
                            <asp:TableCell CssClass="lblError">
                                No Details found.
                            </asp:TableCell>
                        </asp:TableRow>
                    </asp:Table>
                </td>
            </tr>
             <tr>
                <td align="center">
                    <table width="60%" >
                        <tr>
                            <td align="center">
                                <asp:Button ID="btnManageOrder" runat="server" OnClientClick="javascript:ManageOrder();return false;" Text="Manage Day Order" CssClass="altLongBlueButtonFormat" />
                                <asp:Button ID="btnManage" OnClick="ManageTypeOrder" runat="server" style="display:none;"  /> 
                            </td>
                            
                        </tr>
                    </table>
                </td>
            </tr>
            <tr style="display:none;"> <%--FB 2937--%>
                <td align="center">
                    <table cellspacing="5" width="90%">
                        <tr>
                            <td>&nbsp;</td>
                            <td>
                                <span class=subtitleblueblodtext><asp:Label ID="lblCreateEditDepartment" runat="server" Text="Create New"></asp:Label> Day Color  </SPAN><%--ZD 100176--%> 
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>    
            <tr>
               <td align="center" >
                    <table width="60%" border="0" > <%--FB 2937--%>
                        <tr>
                            <td align="right">                                
                                <input class="altLongBlueButtonFormat" onclick="fnCancel()" type="button" value="Cancel" name="btnCancel" style="width:12%" />
                                <asp:Button ID="btnCreate" onfocus="this.blur()" OnClick="CreateNewHolidayDetails" runat="server"  Text="Create New Day Color"  OnClientClick="javascript:DataLoading(1);"/> <%--FB 2796--%><%--FB 2937--%><%--ZD 100176--%> 
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>        
        </table>
    </div>

<img src="keepalive.asp" name="myPic" width="1px" height="1px">
    </form>

<%--code added for Soft Edge button--%>
<script type="text/javascript" src="inc/softedge.js"></script>
    <!-- #INCLUDE FILE="inc/mainbottomNET.aspx" -->
</body>
</html>

