<%--ZD 100147 start--%>
<%--/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.	
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/--%><%--ZD 100147 End--%>
<%@ Page Language="C#" Inherits="ns_InventoryManagement.InventoryManagement" EnableEventValidation="false" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> <!-- FB 2050 -->
<meta http-equiv="X-UA-Compatible" content="IE=8" /> <!-- FB 2050 -->
<!-- #INCLUDE FILE="inc/maintopNET.aspx" --> 
    
<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Inventory Management</title>
    <script type="text/javascript" src="script/mousepos.js"></script>
    <script type="text/javascript" src="inc/functions.js"></script> <%--FB 1830--%>

<script runat="server">

</script>
<script language="javascript">
    function ShowImage(obj)
    {
        //alert(obj.src);
        document.getElementById("myPic").src = obj.src;
        
        //getMouseXY(obj);
        //alert(document.body.scrollHeight);
        document.getElementById("divPic").style.position = 'absolute';
        document.getElementById("divPic").style.left = mousedownX + 20 + 'px'; // ZD 100175
        document.getElementById("divPic").style.top = mousedownY + 'px'; // ZD 100175
        document.getElementById("divPic").style.display="";
        //alert(obj.style.height + " : " + obj.style.width);
    }

    function HideImage()
    {
        document.getElementById("divPic").style.display="none";
    }

function hideItems()
{
    var temp = document.getElementById("detailsRow");
    var temp1 = document.getElementById("hideItem");
    //alert(temp);
    if (temp1.src.indexOf("plus") >0)
    {
        temp1.src = "image/loc/nolines_minus.gif";
        temp.style.display = "block";
    }
    else 
    {
        temp1.src = "image/loc/nolines_plus.gif";
        temp.style.display = "none";
    }
}
function convertControls(id)
{
    var t = document.getElementById("Item" + id);
    t.style.display="none";
    t = document.getElementById("hdnItemList" + id);
    t.style.display="";
}
</script>
</head>
<body>
    <form id="frmInventoryManagement" runat="server" method="post">
          <input type="hidden" id="helpPage" value="40">

    <div>
        <table width="100%">
            <tr>
                <td align="center" colspan="3">
                    <h3><asp:Label runat="server" Text="" ID="lblMainLabel"></asp:Label></h3>
                </td>
            </tr>
            <tr>
                <td align="center" colspan="3">
                    <asp:Label ID="errLabel" runat="server" Visible="false" CssClass="lblError"></asp:Label></td>
            </tr>
            <tr>
                <td colspan="3" align="center">
                    <asp:DataGrid ID="dgInventoryList" ShowFooter="true" AutoGenerateColumns="false" Width="90%" runat="server"
                      OnItemCreated="BindRowsDeleteMessage" OnEditCommand="EditInventory" OnDeleteCommand="DeleteInventory" OnCancelCommand="ViewDetails"
                      ItemStyle-BorderColor="Gray" ItemStyle-BorderStyle="Solid" ItemStyle-BorderWidth="1"
                      BorderStyle="Solid" BorderColor="Gray" BorderWidth="1" GridLines="None"> <%-- FB 2899 --%>
                        <%--Window Dressing start --%>
                        <AlternatingItemStyle CssClass="tableBody" />
                        <ItemStyle CssClass="tableBody" />
                        <FooterStyle CssClass="tableBody" />
                        <%--Window Dressing end --%>
                        <Columns>
<%--                            <asp:ButtonColumn ButtonType="LinkButton" Text=" + " CommandName="Cancel" />
--%>                            <asp:TemplateColumn>
                                <HeaderStyle CssClass="tableHeader" />
                                <HeaderTemplate>
                                    <table width="100%">
                                        <tr class="tableHeader" height="30" runat="server"> <%--FB Case 855--%>
                                            <td align="left" class="tableHeader" width="20%">Name</td>
                                            <td align="left" class="tableHeader" width="15%" nowrap>Admin-in-charge</td>
                                            <td align="left" class="tableHeader" width="5%">Notify</td>
                                            <td align="left" class="tableHeader" width="20%">Assigned to Rooms</td><%--FB 2898 Start--%>
                                            <td align="left" class="tableHeader" width="20%" runat="server" visible='<%# !txtType.Text.Equals("2") %>'>Comments</td>
                                            <td align="left" class="tableHeader" width="40%">Actions</td>
                                        </tr>
                                    </table>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <table width="100%" runat="server">
                                     <%--Window Dressing--%>
                                        <tr class="tableBody">
                                            <td width="20%" valign="top" align="left">
                                                <asp:Label ID="lblInventoryID" runat="server" Visible="false" Text='<%# DataBinder.Eval(Container, "DataItem.ID") %>' CssClass="tableBody"></asp:Label>
                                                <asp:Label ID="lblInventoryName" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Name") %>'  CssClass="tableBody"></asp:Label>
                                            </td>
                                            <td width="15%" valign="top" align="left">
                                                <asp:Label ID="lblAdminIncharge" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.AssignedToName") %>' CssClass="tableBody"></asp:Label>
                                            </td>
                                            <td width="5%" valign="top" align="left">
                                                <asp:Label ID="lblNotify" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Notify") %>' CssClass="tableBody"></asp:Label>
                                            </td>
                                            <td width="20%" valign="top" align="left">
                                                <asp:Label ID="lblAssignedToRooms" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.RoomNames") %>' CssClass="tableBody"></asp:Label>
                                            </td>
                                            <td width="20%" valign="top" align="left" runat="server" Visible='<%# !txtType.Text.Equals("2") %>'>
                                                <asp:Label ID="lblComments" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Comment") %>' CssClass="tableBody"></asp:Label>
                                            </td>
                                            <td width="40%" valign="top" align="left">
                                                <asp:LinkButton CommandName="Cancel" ID="btnViewDetails" runat="server" Text="View Items"></asp:LinkButton>&nbsp;&nbsp;
                                                <asp:LinkButton CommandName="Edit" ID="btnEditInventory" runat="server" Text="Edit"></asp:LinkButton>&nbsp;&nbsp;
                                                <asp:LinkButton CommandName="Delete" ID="btnDeleteInventory" runat="server" Text="Delete"></asp:LinkButton>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td  colspan="6">
                                            <asp:DataGrid runat="server" AutoGenerateColumns="False" Visible='<%# !txtType.Text.Equals("2") %>' ID="itemsGrid" BorderColor="Green" CellPadding="4" Font-Bold="False" Font-Names="Verdana" Font-Size="Small" ForeColor="#333333" GridLines="None" Width="99%" BorderStyle="Solid" BorderWidth="2px" AllowSorting="True" OnItemDataBound="BindImages">
                                                <AlternatingItemStyle CssClass="tableBody" />
                                                <ItemStyle CssClass="tableBody" />
                                                <FooterStyle CssClass="tableBody" />
                                                <HeaderStyle CssClass="tableHeader" />
                                                     <Columns>
                                                         <asp:BoundColumn DataField="ID" HeaderText="ID" Visible="False"></asp:BoundColumn>
                                                         <asp:TemplateColumn>
                                                            <HeaderTemplate>
                                                                <asp:Table ID="Table1" Width="100%" cellpadding="0" cellspacing="0" runat="server">
                                                                    <asp:TableRow>
                                                                        <asp:TableCell width="20%" ID="tdHName" HorizontalAlign="center" runat="server" CssClass="tableHeader"><asp:label ID="lblHName" runat="server" Text="Name"></asp:label></asp:TableCell>
                                                                        <asp:TableCell width="10%" HorizontalAlign="center" Visible='<%#txtType.Text.Equals("1") %>' cssclass="tableHeader" id="tdHQuantity" runat="server" Text="Quantity<br />in Hand"></asp:TableCell>
                                                                        <asp:TableCell width="20%" HorizontalAlign="center" cssclass="tableHeader" id="tdHPrice" runat="server" Text="Price (USD)"></asp:TableCell>
                                                                        <asp:TableCell width="10%" HorizontalAlign="center" Visible='<%# !txtType.Text.Equals("3") %>' cssclass="tableHeader" id="tdHSerialNumber" runat="server" Text="Serial Number"></asp:TableCell>
                                                                        <asp:TableCell width="10%" HorizontalAlign="center" Visible='<%#txtType.Text.Equals("1") %>' cssclass="tableHeader" id="tdHPortable" runat="server" Text="Portable"></asp:TableCell>
                                                                        <asp:TableCell width="10%" HorizontalAlign="center" cssclass="tableHeader" id="tdHImage" runat="server" Text="Image"></asp:TableCell>
                                                                        <asp:TableCell width="25%" HorizontalAlign="center" cssclass="tableHeader" id="tdHComments" runat="server" Text="Comments"></asp:TableCell>
                                                                        <asp:TableCell width="25%" HorizontalAlign="center" cssclass="tableHeader" id="tdHDescription" runat="server" Text="Description"></asp:TableCell><%--FB 2898 End--%>
                                                                    </asp:TableRow>
                                                                </asp:Table>
                                                            </HeaderTemplate>
                                                             <ItemTemplate>
                                                                <asp:Table runat="server" width="100%" cellpadding="0" cellspacing="0">
                                                                    <asp:TableRow>
                                                                        <asp:TableCell HorizontalAlign="center" width="20%" id="tdName" runat="server">
                                                                             <asp:Label ID="lblItemID" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.ID") %>' Visible="false" CssClass="tableBody"></asp:Label>
                                                                             <asp:Label ID="txtName" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Name") %>' CssClass="tableBody"></asp:Label>
                                                                        </asp:TableCell>
                                                                        <asp:TableCell HorizontalAlign="center" width="10%" id="tdQuantity" runat="server" Visible='<%#txtType.Text.Equals("1") %>' >
                                                                              <asp:Label ID="txtQuantity" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Quantity") %>' Width="30" CssClass="tableBody"></asp:Label>
                                                                        </asp:TableCell>
                                                                        <asp:TableCell HorizontalAlign="center" width="20%" id="tdPrice" runat="server"><%--FB 2903 Start--%>
                                                                             <asp:Label ID="txtPrice" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Price") %>' Width="50" CssClass="tableBody"></asp:Label>
                                                                        </asp:TableCell>
                                                                        <asp:TableCell HorizontalAlign="center" width="10%" id="tdSerialNumber" runat="server" Visible='<%# !txtType.Text.Equals("3") %>' >
                                                                             <asp:Label ID="txtSno" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.SerialNumber") %>' Width="50" CssClass="tableBody"></asp:Label>
                                                                        </asp:TableCell>
                                                                        <asp:TableCell HorizontalAlign="center" width="10%" id="tdPortable" runat="server" Visible='<%#txtType.Text.Equals("1") %>'  >
                                                                             <asp:CheckBox ID="chkPortable" runat="server" Enabled="false" Checked='<%# "1"==DataBinder.Eval(Container.DataItem, "Portable").ToString() %>'  />
                                                                        </asp:TableCell>
                                                                        <asp:TableCell HorizontalAlign="center" width="10%" id="tdImage" runat="server">
                                                                             <%--Image Project --%>
                                                                             <asp:Image runat="server" ID="imgItem" Width="30" Height="30" ImageUrl='<%# DataBinder.Eval(Container, "DataItem.Image") %>' onmouseover="javascript:ShowImage(this)" onmouseout="javascript:HideImage()" />
                                                                             <%--<cc1:ImageControl id="imgItem" Width="30" Height="30"   Runat="server"></cc1:ImageControl>--%>
                                                                        </asp:TableCell>
                                                                        <%--Modified For FB 1982 - Start--%>
                                                                        <asp:TableCell HorizontalAlign="center" width="25%" id="tdComments" runat="server">
                                                                             <asp:Label  width="80px" ID="txtComments" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Comments") %>' CssClass="tableBody"></asp:Label>
                                                                        </asp:TableCell>
                                                                        <asp:TableCell HorizontalAlign="center" width="25%" id="tdDescription" runat="server">
                                                                            <asp:Label ID="txtDescription" width="80px" Text='<%# DataBinder.Eval(Container, "DataItem.Description") %>' runat="server" CssClass="tableBody"/>
                                                                        </asp:TableCell><%--FB 2903 End--%>
                                                                        <%--Modified For FB 1982 - End--%>
                                                                    </asp:TableRow>
                                                                    <asp:TableRow>
                                                                        <asp:TableCell ColumnSpan="9" HorizontalAlign="center"><br />
                                                                            <asp:DataGrid ID="dgDeliveryType" Visible='<%# "1"==DataBinder.Eval(Container, "DataItem.Type").ToString() %>' runat="server" AllowSorting="false" AllowPaging="false" AutoGenerateColumns="false"
                                                                            DataSource='<%# getDeliveryTypesDataSource( (String)DataBinder.Eval(Container.DataItem, "Item_Id").ToString() ) %>' Width="90%">
                                                                                <%--Window Dressing--%>
                                                                                <AlternatingItemStyle CssClass="tableBody"  />
                                                                                <ItemStyle CssClass="tableBody" HorizontalAlign="Left"  /><%--FB 2896--%>
                                                                                <Columns>
                                                                                   <asp:BoundColumn DataField="DeliveryTypeID" Visible="false"></asp:BoundColumn>
                                                                                   <asp:TemplateColumn HeaderStyle-CssClass="tableHeader" HeaderText="Delivery Type" HeaderStyle-HorizontalAlign="Left">
                                                                                        <ItemTemplate>
                                                                                            <asp:Label ID="lblDeliveryTypeName" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.DeliveryName") %>' CssClass="tableBody"></asp:Label>
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateColumn>
                                                                                    <asp:TemplateColumn HeaderStyle-CssClass="tableHeader" HeaderText="Delivery Cost (USD)" HeaderStyle-HorizontalAlign="Left">
                                                                                        <ItemTemplate>
                                                                                            <asp:Label ID="txtDeliveryCost" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.DeliveryCost") %>' CssClass="tableBody"></asp:Label>
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateColumn>
                                                                                    <asp:TemplateColumn HeaderStyle-CssClass="tableHeader" HeaderText="Service Charge (USD)" HeaderStyle-HorizontalAlign="Left">
                                                                                        <ItemTemplate>
                                                                                            <asp:Label ID="txtServiceCharge" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.ServiceCharge") %>' CssClass="tableBody"></asp:Label>
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateColumn>
                                                                                </Columns>
                                                                            </asp:DataGrid>
                                                                        </asp:TableCell>
                                                                    </asp:TableRow>
                                                                </asp:Table>
                                                            </ItemTemplate>
                                                         </asp:TemplateColumn>
                                                     </Columns>
                                            </asp:DataGrid>
                                            <asp:DataGrid runat="server" AutoGenerateColumns="False" Visible='<%# txtType.Text.Equals("2") %>' ID="dgCateringMenus" BorderColor="Green" CellPadding="0" Font-Bold="False" Font-Names="Verdana" Font-Size="Small" ForeColor="#333333" GridLines="None" Width="99%" BorderStyle="Solid" BorderWidth="2px">
                                                <AlternatingItemStyle CssClass="tableBody" VerticalAlign="Top" />
                                                <ItemStyle CssClass="tableBody" VerticalAlign="Top" HorizontalAlign="left" />
                                                <HeaderStyle CssClass="tableHeader" VerticalAlign="Middle" HorizontalAlign="Left" />
                                                <Columns>
                                                    <asp:BoundColumn DataField="ID" Visible="false"></asp:BoundColumn>
                                                    <%--Window Dressing start --%>
                                                    <asp:BoundColumn DataField="Name" HeaderText="Name" HeaderStyle-CssClass="tableHeader" ItemStyle-CssClass="tableBody"></asp:BoundColumn>
                                                    <asp:TemplateColumn HeaderText="Service Type" HeaderStyle-CssClass="tableHeader">
                                                    <%--Window Dressing end --%>
                                                        <ItemTemplate>
                                                            <asp:DataGrid ID="dgServices" runat="server" ShowHeader="false" AutoGenerateColumns="false" CellPadding="2" CellSpacing="0" BorderStyle="none" BorderWidth="0px" GridLines="None"> <%-- FB 2050 --%>
                                                                <Columns>
                                                                    <asp:BoundColumn DataField="ID" Visible="false" ItemStyle-CssClass="tableBody"></asp:BoundColumn>
                                                                    <asp:BoundColumn DataField="Name" ItemStyle-CssClass="tableBody"></asp:BoundColumn>
                                                                </Columns>
                                                            </asp:DataGrid>
                                                        </ItemTemplate>
                                                    </asp:TemplateColumn>
                                                    <%--Window Dressing --%>
                                                    <asp:BoundColumn DataField="Price" HeaderText="Price(USD)" HeaderStyle-CssClass="tableHeader" ItemStyle-CssClass="tableBody"></asp:BoundColumn>
                                                    <asp:TemplateColumn HeaderText="Items" HeaderStyle-CssClass="tableHeader">
                                                        <ItemTemplate>
                                                            <asp:DataGrid ID="dgMenuItems" runat="server" ShowHeader="false" AutoGenerateColumns="false" CellPadding="2" CellSpacing="0" 
                                                            OnItemDataBound="BindImages" BorderStyle="none" BorderWidth="0px" GridLines="None"> <%-- FB 2050 --%> <%--ZD 100175--%>
                                                                <Columns>
                                                                    <asp:BoundColumn DataField="ID" Visible="false"></asp:BoundColumn>
                                                                    <asp:BoundColumn DataField="Name" ItemStyle-CssClass="tableBody"></asp:BoundColumn><%--ZD 100175--%>
                                                                    <asp:TemplateColumn HeaderText="Image"  HeaderStyle-CssClass="tableHeader" HeaderStyle-VerticalAlign="Top" HeaderStyle-Font-Bold="true" ItemStyle-Width="25%">
                                                                        <ItemTemplate>
                                                                            <asp:Image runat="server" ID="imgItem" Width="30" Height="30" onmouseover="javascript:ShowImage(this)" onmouseout="javascript:HideImage()" />
                                                                        </ItemTemplate>
                                                                    </asp:TemplateColumn>
                                                                </Columns>
                                                            </asp:DataGrid>
                                                        </ItemTemplate>
                                                    </asp:TemplateColumn>
                                                </Columns>
                                            </asp:DataGrid>                                            
                                            </td>
                                        </tr>
                                    </table>
                                </ItemTemplate>
                                <FooterStyle Font-Bold="true" /> <%-- FB 2895 FB 2902 --%>
                                <FooterTemplate>
                                    <table width="100%">
                                        <tr height="30">
                                            <td width="100%" valign="top" align="Right">
                                               <%--Window Dressing--%>
                                               <b class="blackblodtext"> Total Records: </b><asp:Label ForeColor="blue" ID="lblTotalRecords" runat="server"></asp:Label>
                                            </td>
                                        </tr>
                                    </table>
                                </FooterTemplate>
                            </asp:TemplateColumn>
                        </Columns>
                    </asp:DataGrid>
                    <asp:Label ID="lblNoRecords" Text="No Inventory found" Visible="false" CssClass="lblError" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td align="center" colspan="3" style="height: 47px">
                    <table border="0" cellpadding="0" cellspacing="0" width="90%">
                        <tr>
                            <td align="right" colspan="3" rowspan="3">
                                <asp:Label ID="lblCount" runat="server" Font-Bold="True" Font-Names="Verdana" Font-Size="Small"
                                    ForeColor="Blue"></asp:Label></td>
                        </tr>
                        <tr>
                        </tr>
                        <tr>
                        </tr>
                    </table>
                    <asp:DropDownList ID="lstDeliveryType" runat="server" DataTextField="Name" DataValueField="ID" Visible="false"></asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td align="center" colspan="3">
                    <table width="90%">
                        <tr>
                            <td align="right" colspan="3" rowspan="3">
                    <asp:Button ID="btnAddNew" Width="250px" runat="server" 
                        Text="Create New Inventory" OnClick="CreateNewInventory" /><%-- FB 2570 2796 --%></td>
                        </tr>
                        <tr>
                        </tr>
                        <tr>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </div>
<div id="divPic" style="display:none;  z-index:1;">
    <img src="" name="myPic" id="myPic" width="200" height="200">
</div>
<%--Window Dressing--%>
        <asp:TextBox ID="txtType" runat="server" Width="0px" BackColor="transparent" BorderStyle="none"></asp:TextBox>
        <asp:DropDownList ID="lstCateringServices" runat="server" DataTextField="Name" DataValueField="ID" Visible="false"></asp:DropDownList>
            
    <script language="javascript">
         if (document.getElementById("txtType").value == "1")
            document.getElementById("helpPage").value = "3";
         if (document.getElementById("txtType").value == "2")
            document.getElementById("helpPage").value = "5";
         if (document.getElementById("txtType").value == "3")
            document.getElementById("helpPage").value = "40";
//FB 1830 - Starts    
fnChangeCurrencyFormat();
function fnChangeCurrencyFormat(cntRows, gridID, type)
{           
    var incre = 1;
    var incstr = "";
    
    while(cntRows > 0)
    {
        incre = incre + 1;            
        incstr = incre;
        
        if(incre < 10 )
            incstr = "0" + incre;
      
        var tName = "dgInventoryList_ctl" + incstr + "_" + gridID;
        
        var incre1 = 0;
        var incstr1 = "";
        if(document.getElementById(tName))
        {
            if(type == 2)
            {                       
                changeCurrencyFormat(tName,'<%=currencyFormat %>');    
                cntRows = 0;                       
            }
            else
            {
                while(cntRows > 0)
                {
                    incre1 = incre1 + 1;            
                    incstr1 = incre1;
                
                    if(incre1 < 10 )
                        incstr1 = "0" + incre1;
                   var cName = tName +"_ctl" + incstr1 + "_Table1";
                
                    if(document.getElementById(cName))
                    {
                        changeCurrencyFormat(cName,'<%=currencyFormat %>');    
                    }        
                    
                    var num = 0;
                    var incre2 = 1;
                    var incstr2 = "";
                    
                    while(num == 0)
                    {   
                        incre2 = incre2 + 1;
                        incstr2 = incre2;    
                        
                        if(incre2 < 10 )
                          incstr2 = "0" + incre2;
                                                                             
                        var dName = tName +"_ctl" + incstr2 + "_dgDeliveryType";
                    
                        if (document.getElementById(dName))     
                        {
                            changeCurrencyFormat(dName,'<%=currencyFormat %>');
                        }  
                        else
                           num = 1;
                    }
                   cntRows = 0;
                 }
             }
         }
    }
}       
//FB 1830 - End                           
    </script>

            </form>
<%--code added for Soft Edge button--%>
<script type="text/javascript" src="inc/softedge.js"></script>
    <!-- #INCLUDE FILE="inc/mainbottomNET.aspx" -->
</body>
</html>
