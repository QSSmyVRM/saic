﻿//ZD 100147 Start
/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
//ZD 100147 End
using System;
using System.DirectoryServices;
using System.Collections;
using System.ComponentModel;
using System.Text;
using System.Xml;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Collections.Generic;
using DevExpress.Web.ASPxTreeList;
using DevExpress.Web.ASPxGridView;
using DevExpress.Web.ASPxEditors;
using DevExpress.Web;
using System.DirectoryServices.Protocols;//new LDAP Design

namespace test
{
    public partial class LDAPImport : Page
    {

        const string PageSizeSessionKey = "ed5e843d-cff7-47a7-815e-832923f7fb09";
        //new LDAP Design START
        private LdapConnection ldapConn;
        private ns_Logger.Logger log = null;
        private Int32 LmtExcd = 0;//PSU
        private string srcFilter = ""; //FB 2096
        private string loginKey = ""; //FB 2096
        private int AuthenticationType = 0;//FB 2993 LDAP
        //new LDAP Design End

        protected int GridPageSize
        {
            get
            {
                if (Session[PageSizeSessionKey] == null)
                    return grd1.SettingsPager.PageSize;
                return (int)Session[PageSizeSessionKey];
            }
            set { Session[PageSizeSessionKey] = value; }
        }

        myVRMNet.NETFunctions obj;

/*      protected ASPxTreeList treeList;
        protected TreeView treeList1;
        protected ASPxGridView grd1;
        protected Button BtnUpdateRHS;
        protected System.Web.UI.WebControls.Label lblServeraddr;
        protected System.Web.UI.WebControls.Label lblDomain;
        protected System.Web.UI.WebControls.Label lblLoginname;
        protected System.Web.UI.WebControls.Label lblPassword;
        protected System.Web.UI.WebControls.Button btnDfltSrch;
        protected System.Web.UI.WebControls.Button btnSrch1;
        protected System.Web.UI.HtmlControls.HtmlTable tblResults;
        protected System.Web.UI.WebControls.TextBox txtSearchUsers;
        protected System.Web.UI.WebControls.Button btnSubmitUsers;
        protected System.Web.UI.WebControls.DropDownList lstLDAPTemplates;
        protected System.Web.UI.WebControls.Label errLabel;
        protected HtmlInputHidden addroom;
        //Fixes START
        protected HtmlInputHidden clrSelgrd;
        protected HtmlInputHidden hdnSelVal;
        protected System.Web.UI.UpdatePanel UpdatePanel1;
        protected System.Web.UI.UpdatePanel UpdatePanel2;
        public System.Web.UI.WebControls.CheckBox chkSelectAll;
        //Fixes END
        protected HtmlInputHidden hdnSelNode;
        protected HtmlInputHidden selectedlocframe;
 * 
  */    
        private myVRMDirectoryEntryCollection immDEColl;
        private Boolean _isDefaultAll = false;
        private myVRMLDAP immLDAP;
        private Boolean _isSearch = false;
        private Boolean _isDefault = false;
        private DataTable dtSearchSelected = null;

        private DataTable dtDefault; //Fixes
        private DataTable dtusrSelected = null;//FB 2320


  //      protected System.Web.UI.WebControls.DataGrid SelectedGrid;
        
        
        public LDAPImport()
        {
            obj = new myVRMNet.NETFunctions();
            log = new ns_Logger.Logger();//new LDAP Design
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (obj == null)
                    obj = new myVRMNet.NETFunctions();
                obj.AccessandURLConformityCheck("LDAPImport.aspx", Request.Url.AbsoluteUri.ToLower()); // ZD 100263

                conftypeDIV.Attributes.Add("onscroll", "setonScroll(this,'" + LdapListTbl.ClientID + "');");  //FB 2320
                GridStyles();
                if (btnSubmitUsers.Enabled == true)//Fixes
                    btnSubmitUsers.Attributes.Add("class", "altLongBlueButtonFormat"); //FB 2664
                    if (errLabel.Text != "")
                    {
                        errLabel.Text = "";
                        errLabel.Visible = false;
                    }
                if (!IsPostBack)
                {
                    BindData();
                    BindTemplates();
                    if (lblServeraddr.Text == "") // LDAP Fix
                    {
                        Session["errMsg"] = "Active Directory or LDAP Directory not Configured in Site Settings";
                    }
                    else
                    {
                        BindTreeview(treeList1.Nodes[0]);
                    }
                    if (Session["dtSave"] != null)//Fixes
                        if (Session["dtSave"].ToString() != "")
                            Session["dtSave"] = null;
                    Session["dtDefault"] = null;
                }
                else
                {
                    if (Session["dtdE"] != null)
                    {
                        if (Session["dtdE"].ToString() != "")//Fixes
                        {
                            DataTable dtDE = Session["dtdE"] as DataTable;
                            BindGrid(dtDE);
                        }
                    }
                    if (Session["dtSave"] != null)
                    {
                        DataTable dtUsers = Session["dtSave"] as DataTable;
                        BindUserGrid(dtUsers);
                    }
                    if (clrSelgrd.Value.Equals("1"))//Fixes
                    {
                        SelectedGrid.DataSource = "";
                        SelectedGrid.DataBind();
                        clrSelgrd.Value = "";
                    }
                }
                if (Request.QueryString["m"] != null)
                    if (Request.QueryString["m"].ToString().Equals("1"))
                    {
                        errLabel.Text = "Operation Successful!";
                        errLabel.Visible = true;
                    }
                if (Session["errMsg"] != null)
                {
                    if (Session["errMsg"].ToString() != "")
                    {
                        errLabel.Visible = true;
                        errLabel.Text = Session["errMsg"].ToString();
                        Session["errMsg"] = null;

                    }
                }


            }
            catch (Exception ex)
            {
                errLabel.Text = ex.Message;
                errLabel.Visible = true;
            }

        }

        #region GridStyles
        protected void GridStyles()
        {
            try
            {
                grd1.SettingsPager.PageSize = GridPageSize;
                grd1.Templates.PagerBar = new CustomPagerBarTemplate();

                grd1.Settings.VerticalScrollBarStyle = GridViewVerticalScrollBarStyle.Standard;
                grd1.Settings.ShowVerticalScrollBar = true;
                grd1.Settings.VerticalScrollableHeight = 300;

                chkSelectAll.CheckedChanged += new EventHandler(chkSelectAll_CheckedChanged);

                if (grd1.VisibleRowCount > 0)
                    chkSelectAll.Enabled = true;
                else
                    chkSelectAll.Enabled = false;


                if (chkSelectAll.Checked)
                    Session.Add("combo", 1);
                else
                    Session.Add("combo", 0);
            }
            catch (Exception ex)
            {
                errLabel.Text = ex.Message;
                errLabel.Visible = true;
            }
        }
        #endregion

        #region chkSelectAll_CheckedChanged
        protected void chkSelectAll_CheckedChanged(object sender, EventArgs e)
        {
            String strVal = "";
            try
            {
                List<object> selectedUsers = grd1.GetSelectedFieldValues("Id");

                if (selectedUsers.Count > 0)
                {
                    foreach (String str in selectedUsers)
                    {
                        if (strVal == "")
                            strVal = str;
                        else
                            strVal += "§" + str;  //strVal += "!!" + str; (§-Alt789)-FB 2320
                    }
                    if (chkSelectAll.Checked)
                    {
                        chkSelectAll.Attributes.Add("onblur", "Javascript:Addusers('" + strVal + "')");
                        addroom.Value = "1";
                        selectedlocframe.Value = strVal;
                        AddUserstoList(null, null);
                    }
                    else
                    {
                        chkSelectAll.Attributes.Add("onblur", "Javascript:ClearAllSelection('" + strVal + "')");
                        DataTable dtselected = new DataTable();
                        BindUserGrid(dtselected);
                    }

                }
            }
            catch (Exception ex)
            {
                errLabel.Text = ex.Message;
                errLabel.Visible = true;
            }
        }
        #endregion

        #region AddUserstoList
        protected void AddUserstoList(object sender, EventArgs e)
        {
            try
            {
                if (addroom.Value == "1")
                {
                    addroom.Value = "0";
                    GetselectedRooms();
                }
                UpdatePanel1.Update();//Fixes
            }
            catch (Exception ex)
            {
                errLabel.Text =ex.Message;
                errLabel.Visible = true;
            }
        }
        #endregion

        #region btnDfltSrch_Click
        protected void btnDfltSrch_Click(object sender, EventArgs e)
        {
            try
            {
                _isSearch = true;
                _isDefault = true;
                BindTreeview(new TreeNode());
            }
            catch (Exception ex)
            {
                errLabel.Visible=true;
                errLabel.Text = ex.Message;
            }
        }
        #endregion

        #region btnSrch1_Click
        protected void btnSrch1_Click(object sender, EventArgs e)
        {
            try
            {
                txtSearchUsers.Text = "";
                _isSearch = true;
                _isDefaultAll = true;
                BindTreeview(new TreeNode());
            }
            catch (Exception ex)
            {
                errLabel.Visible = true;
                errLabel.Text = ex.Message;
            }
        }
        #endregion

        #region btnSubmitUsers Event Handlers Method

        protected void btnSubmitUsersClick(object sender, EventArgs e)
        {
            DataTable dtSelected = null;
            try
            {
                bool cbFlag = false, cbFlag1 = true; //FB 2320
                String inXML = "";
                if (Session["dtSave"] != null)
                {
                    dtSelected = (DataTable)Session["dtSave"];
                    
                    //FB 2320 - Starts
                    dtusrSelected = dtSelected.Clone();
                    if (dtSelected.Rows.Count > 0)
                    {
                        DataRow[] drArr = dtSelected.Select("mail is null");

                        for (int row = 0; row < drArr.Length; row++)
                        {
                            dtusrSelected.ImportRow(drArr[row]);
                        }
                    }
                    if (dtusrSelected.Rows.Count > 0)
                    {
                        foreach (DataRow da in dtusrSelected.Rows)
                        {
                            LdapPopUp.Show();
                            LdapListTbl.DataSource = dtusrSelected;
                            LdapListTbl.DataBind();
                            cbFlag1 = false;
                        }
                    }
                    //FB 2320 - End

                    if (cbFlag1)
                    {
                        if (dtSelected.Rows.Count > 0)
                        {
                            inXML += "<login>";
                            inXML += obj.OrgXMLElement();//Organization Module Fixes
                            inXML += "  <userID>" + Session["userID"].ToString() + "</userID>";
                            inXML += "  <userTemplateID> " + lstLDAPTemplates.SelectedValue.ToString() + "</userTemplateID>";
                            inXML += "  <users>";
                            foreach (DataRow dr in dtSelected.Rows)
                            {
                                inXML += "<user>";
                                inXML += "     <FirstName>" + dr["Givenname"].ToString() + "</FirstName>";
                                inXML += "     <LastName>" + dr["sn"].ToString() + "</LastName>";
                                inXML += "     <Login>" + dr["samaccountname"].ToString() + "</Login>";
                                if (dr["mail"].ToString().Contains("&") || dr["mail"].ToString().Contains("<") || dr["mail"].ToString().Contains(">"))//Fixes
                                {
                                    if (dr["mail"].ToString().Contains("&"))
                                        dr["mail"] = dr["mail"].ToString().Replace("&", "&amp;");

                                    if (dr["mail"].ToString().Contains("<"))
                                        dr["mail"] = dr["mail"].ToString().Replace("<", "&lt;");

                                    if (dr["mail"].ToString().Contains(">"))
                                        dr["mail"] = dr["mail"].ToString().Replace(">", "&gt;");

                                    inXML += "     <Email>" + dr["mail"].ToString() + "</Email>";
                                }
                                else
                                    inXML += "     <Email>" + dr["mail"].ToString() + "</Email>";
                                inXML += "     <Telephone/>";// +"Telephone Number " + "</Telephone>";
                                inXML += "     <whencreated>" + DateTime.UtcNow.ToLongDateString() + "</whencreated>";
                                inXML += "</user>";
                            }
                            inXML += "  </users>";
                            inXML += "</login>";
                            cbFlag = true;
                        }
                    }
                    //if (Session["dtSave"].ToString() != "") //Fixes
                    //{
                    //    Session["dtSave"] = "";//Fixes
                    //    clrSelgrd.Value = "1";//Fixes
                    //}
                }
                if (cbFlag1) //FB 2320
                {
                    if (cbFlag)
                    {
                        //Response.End();
                        String outXML = obj.CallMyVRMServer("SetMultipleUsers", inXML, Application["MyVRMServer_ConfigPath"].ToString());
                        //Response.Write(obj.Transfer(inXML));
                        if (outXML.IndexOf("<error>") >= 0)
                        {
                            //Session.Add("errMsg", obj.ShowErrorMessage(outXML));
                            errLabel.Text = obj.ShowErrorMessage(outXML); //Fixes
                            errLabel.Visible = true; //Fixes
                            //Response.Redirect("LDAPImport.aspx?" + Request.QueryString.ToString());
                        }
                        else
                        {
                            //Fixes START
                            if (Session["dtSave"].ToString() != "") //Fixes
                            {
                                Session["dtSave"] = null;//Fixes
                                clrSelgrd.Value = "1";//Fixes
                            }
                            errLabel.Text = "Operation Successful!";
                            errLabel.Visible = true;
                            SelectedGrid.DataSource = "";
                            SelectedGrid.DataBind();


                            selectedlocframe.Value = "";

                            //if (addroom.Value)
                            addroom.Value = "1";
                            AddUserstoList(null, null);
                            //Fixes END
                        }
                    }
                    else
                    {
                        errLabel.Text = "Please select at least one user to import";//Fixes
                        errLabel.Visible = true;
                    }
                }
            }
            catch (System.Threading.ThreadAbortException)
            {
            }
            catch (Exception ex)
            {
                errLabel.Text = ex.StackTrace;
                errLabel.Visible = true;
            }
        }
        #endregion

        //FB 2320 - Starts
        #region btn_Submit in Model Popup
        /// <summary>
        /// btn_Submit in Model Popup
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btn_Submit(object sender, EventArgs e)
        {
            try
            {
                DataTable dtselected = null;
                dtselected = (DataTable)Session["dtSave"];

                foreach (DataGridItem dgi in LdapListTbl.Items)
                {
                    foreach (DataRow drr1 in dtselected.Rows)
                    {
                        if (drr1["samaccountname"].ToString() == dgi.Cells[0].Text) //Compare Login 
                        {
                            drr1["Id"] = dgi.Cells[0].Text;
                            drr1["mail"] = ((TextBox)dgi.FindControl("txtUserEmail")).Text.ToString();
                        }
                    }
                }
                if (Session["dtSave"] != null)
                    Session["dtSave"] = dtselected;

                btnSubmitUsersClick(null, null);
            }
            catch (System.Threading.ThreadAbortException)
            {
            }
            catch (Exception ex)
            {
                errLabel.Text = ex.StackTrace;
                errLabel.Visible = true;
            }
        }
        #endregion
        //FB 2320 - End

        #region BindTemplates
        private void BindTemplates()
        {
            try
            {
                string sortBy = "2";
                string alphabet = "a";
                string currentPageNo = "1";
                string inXML = "";
                inXML += "<login>";
                inXML += obj.OrgXMLElement();//Organization Module Fixes
                inXML += "  <userID>" + Session["userID"].ToString() + "</userID>";
                inXML += "  <searchFor/>";
                inXML += "  <sortBy>" + sortBy + "</sortBy>";
                inXML += "  <alphabet>" + alphabet + "</alphabet>";
                inXML += "  <pageNo>" + currentPageNo + "</pageNo>";
                inXML += "</login>";
                string outXML = obj.CallMyVRMServer("GetMultipleUsers", inXML, Application["MyVRMServer_ConfigPath"].ToString());
                if (outXML.IndexOf("<error>") < 0)
                {
                    XmlDocument xmldoc = new XmlDocument();
                    xmldoc.LoadXml(outXML);
                    XmlNodeList nodes = xmldoc.SelectNodes("//getMultipleUsers/templates/template");
                    if (nodes.Count > 0)
                    {
                        btnSubmitUsers.Enabled = true;
                        btnSubmitUsers.Attributes.Add("class", "altLongBlueButtonFormat"); //FB 2664
                        LoadTemplates(nodes);
                    }
                    else
                    {
                        btnSubmitUsers.Enabled = false;
                        btnSubmitUsers.Attributes.Add("class", "btndisable"); //FB 2664
                        errLabel.Text += "<br>There are no user templates available. Please create at least one user template and proceed with LDAP Import";
                        errLabel.Visible = true;
                    }
                }
                else
                {
                    errLabel.Text = obj.ShowErrorMessage(outXML);
                    errLabel.Visible = true;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region LoadTemplates
        protected bool LoadTemplates(XmlNodeList nodes)
        {
            try
            {
                XmlTextReader xtr;
                DataSet ds = new DataSet();

                foreach (XmlNode node in nodes)
                {
                    xtr = new XmlTextReader(node.OuterXml, XmlNodeType.Element, new XmlParserContext(null, null, null, XmlSpace.None));
                    ds.ReadXml(xtr, XmlReadMode.InferSchema);
                }
                DataView dv;
                DataTable dt;

                if (ds.Tables.Count > 0)
                {
                    dv = new DataView(ds.Tables[0]);
                    dt = dv.Table;
                    lstLDAPTemplates.DataSource = dt;
                    lstLDAPTemplates.DataBind();
                }
                else
                    return false;

                return true;

            }
            catch (Exception ex)
            {
                errLabel.Text = ex.StackTrace;
                errLabel.Visible = true;
                return false;
            }
        }
        #endregion

        #region GetLdapConnection
        private LdapConnection GetLdapConnection(String sName, String domain, String uName, String pwd, Int32 Port)
        {
            LdapConnection lconnection = null;
            try
            {
                if (!IsPostBack) //FB 2096
                {
                    if (!(AuthenticateUser(sName, domain, uName, pwd, Port)))
                    {
                        btnDfltSrch.Enabled = false; //FB 2993 LDAP
                        btnSrch1.Enabled = false;//FB 2993 LDAP
                        return lconnection;
                    }
                    else
                    {
                        btnDfltSrch.Enabled = true;//FB 2993 LDAP
                        btnSrch1.Enabled = true;//FB 2993 LDAP
                    }
                }

                LdapDirectoryIdentifier ldapDir = new LdapDirectoryIdentifier(sName, Port); //new design

                lconnection = new LdapConnection(ldapDir);
                //You may need to try different types of Authentication depending on your setup
                if (uName != "")
                {
                    lconnection.AuthType = AuthType.Ntlm;
                    System.Net.NetworkCredential myCredentials = new System.Net.NetworkCredential(uName, pwd, domain);
                    lconnection.Bind(myCredentials);
                }
                else
                    lconnection.AuthType = AuthType.Anonymous;

                lconnection.AutoBind = true;
                lconnection.Timeout = new TimeSpan(0, 0, 20, 0, 0);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return lconnection;
        }
        #endregion

        //FB 2096 start
        #region AuthenticateUser
        private bool AuthenticateUser(String sAddress, String domain, String uName, String pwd, Int32 Port)
        {
            try
            {
                DirectoryEntry dirEntry = new DirectoryEntry();

                // specify the ldap server info
                string ldapPath = "LDAP://" + sAddress + ":" + Port;
                dirEntry.Path = ldapPath;

                //check to see if domain prefix exists
                if (domain != "")
                {
                    // prefix the domain before the username 
                    uName = domain + uName;
                }

                // specify ldap settings
                dirEntry.Username = uName.Trim();
                dirEntry.Password = pwd.Trim();
                // set auth type to "None"
                //FB 2993 LDAP Start
                switch (AuthenticationType)
                {
                    case 0:
                        dirEntry.AuthenticationType = AuthenticationTypes.None;
                        break;
                    case 1:
                        dirEntry.AuthenticationType = AuthenticationTypes.Signing;
                        break;
                    case 2:
                        dirEntry.AuthenticationType = AuthenticationTypes.Sealing;
                        break;
                    case 3:
                        dirEntry.AuthenticationType = AuthenticationTypes.Secure;
                        break;
                    case 4:
                        dirEntry.AuthenticationType = AuthenticationTypes.Anonymous;
                        break;
                    case 5:
                        dirEntry.AuthenticationType = AuthenticationTypes.Delegation;
                        break;
                    case 6:
                        dirEntry.AuthenticationType = AuthenticationTypes.Encryption;
                        break;
                    case 7:
                        dirEntry.AuthenticationType = AuthenticationTypes.FastBind;
                        break;
                    case 8:
                        dirEntry.AuthenticationType = AuthenticationTypes.ReadonlyServer;
                        break;
                    case 9:
                        dirEntry.AuthenticationType = AuthenticationTypes.SecureSocketsLayer;
                        break;
                    case 10:
                        dirEntry.AuthenticationType = AuthenticationTypes.ServerBind;
                        break;
                    default:
                        dirEntry.AuthenticationType = AuthenticationTypes.None;
                        break;
                }
                //FB 2993 LDAP End
                
                // specify the search string
                DirectorySearcher dirSearcher = new DirectorySearcher(dirEntry);

                //FB 2096 start
                //dirSearcher.Filter = ("(=" + uName.Trim() + ")"); //PSU 

                if (srcFilter != "")
                    dirSearcher.Filter = ("(&" + srcFilter + "(" + loginKey + "=" + uName.Trim() + "))");  //PSU
                else
                    dirSearcher.Filter = "";

                //FB 2096 end
                                							
                dirSearcher.PropertiesToLoad.Add("givenname");
                dirSearcher.PropertiesToLoad.Add("sn");
                dirSearcher.PropertiesToLoad.Add("mail");
                dirSearcher.PropertiesToLoad.Add("cn");
                dirSearcher.PropertiesToLoad.Add("phone");
                dirSearcher.PropertiesToLoad.Add(loginKey);

                // run the search. check the result count to see there is only one record.
                // if multiple records are returned then username entered should not be considered as valid.
                SearchResult searchResult = dirSearcher.FindOne();
                
                return true;
            }
            catch (Exception e)
            {
                this.errLabel.Text  = e.Message;
                log.Trace(e.Message);
                return false;
            }
        }
        #endregion
        //FB 2096 end

        #region GetRootDN
        private String GetRootDN(LdapConnection conn)
        {
            String rootDN = "";
            Boolean isRootDN = false;
            try
            {
                SearchRequest findme = new SearchRequest();
                findme.DistinguishedName = ""; //Find all People in this ou
                findme.Scope = System.DirectoryServices.Protocols.SearchScope.Base; //We want BASE

                SearchResponse results = (SearchResponse)ldapConn.SendRequest(findme); //Run the query and get results

                SearchResultEntryCollection entries = results.Entries;
                for (int i = 0; i < entries.Count; i++)//Iterate through the results
                {
                    SearchResultEntry entry = entries[i];
                    IDictionaryEnumerator attribEnum = entry.Attributes.GetEnumerator();
                    String parentDir = "";
                    while (attribEnum.MoveNext())//Iterate through the result attributes
                    {
                        DirectoryAttribute subAttrib = (DirectoryAttribute)attribEnum.Value;
                        for (int ic = 0; ic < subAttrib.Count; ic++)
                        {
                            //Attribute Name below
                            if (attribEnum.Key.ToString().Equals("defaultnamingcontext") || attribEnum.Key.ToString().Equals("rootdomainnamingcontext") || attribEnum.Key.ToString().Equals("namingcontexts"))//PSU Issue
                            //if (attribEnum.Key.ToString().Equals("defaultnamingcontext") || attribEnum.Key.ToString().Equals("rootdomainnamingcontext"))
                            {
                                rootDN = subAttrib[ic].ToString();
                                isRootDN = true;
                                if(!(Application["Client"].ToString().ToUpper() == "PSU"))
                                {
                                    break;
                                }
                            }

                            String output = attribEnum.Key.ToString() + " = " + subAttrib[ic].ToString();
                            log.Trace("GetRootDN" + output);
                        }
                        if (isRootDN)
                            break;
                    }
                    if (isRootDN)
                        break;
                }
                if (rootDN == "")
                    rootDN = "LDAP";
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return rootDN;
        }
        #endregion

        #region BindTreeview
        private void BindTreeview(TreeNode node)
        {
            System.DirectoryServices.Protocols.SearchScope scope = System.DirectoryServices.Protocols.SearchScope.OneLevel;
            TreeNode parentNode = new TreeNode();
            String dn = "";
            DataTable dt = new DataTable("Directory");
            DataRow dr = null;

            try
            {
                //New LDAP Design START
                GenerateColumns(ref dt);
                ldapConn = GetLdapConnection(lblServeraddr.Text, lblDomain.Text, lblLoginname.Text, lblPassword.Text, 389);

                if (ldapConn != null)
                {
                    if (node.Value != "" && node.Value.ToString() != "0")
                        dn = node.Value;
                    else
                    {
                        String rootDN = GetRootDN(ldapConn);
                        rootDN = GetRootDN(ldapConn);
                        
                        if (srcFilter.Trim() != "") //FB 2096
                            rootDN = srcFilter;
                        
                        dn = rootDN;
                        treeList1.Nodes.Clear();
                        parentNode.Text = rootDN;
                        parentNode.Value = rootDN;
                        parentNode.PopulateOnDemand = false;
                        parentNode.Expand();
                        treeList1.Nodes.Add(parentNode);
                        node = parentNode;
                    }
                    SearchRequest findme = new SearchRequest();
                    findme.DistinguishedName = dn; //Find all People in this ou
                    findme.Scope = scope; //We want all

                    if (txtSearchUsers.Text == "")
                    {
                        {
                            if (Application["Client"].ToString().ToUpper() == "PSU")
                            {
                                findme.Filter = "(|(uid=aa*)(sn=aa*)(mail=aa*))";
                                findme.Scope = System.DirectoryServices.Protocols.SearchScope.Subtree;
                            }
                        }
                    }
                    else
                    {
                        findme.Scope = System.DirectoryServices.Protocols.SearchScope.Subtree;
                        if (Application["Client"] != null)
                        {
                            if (Application["Client"].ToString().ToUpper() == "PSU")
                                findme.Filter = "(|(mail=" + txtSearchUsers.Text + "*)(sn=" + txtSearchUsers.Text + "*))";
                            else
                                findme.Filter = "(|(giveName=" + txtSearchUsers.Text + "*)(cn=" + txtSearchUsers.Text + "*)(mail=" + txtSearchUsers.Text + "*)(sn=" + txtSearchUsers.Text + "*))";
                        }
                        else
                        {
                            findme.Filter = "(|(giveName=" + txtSearchUsers.Text + "*)(cn=" + txtSearchUsers.Text + "*)(mail=" + txtSearchUsers.Text + "*)(sn=" + txtSearchUsers.Text + "*))";
                        }
                    }
                    if (Application["Client"].ToString().ToUpper() == "PSU" && LmtExcd.Equals(1) && txtSearchUsers.Text != "") //PSU
                    {
                        findme.Filter = "(|(uid=aa*)(sn=aa*)(mail=aa*))";
                        errLabel.Text = "There were too many matches in your search,please narrow your search";
                        errLabel.Visible = true;
                        LmtExcd = 0;
                    }
                    //PSU end
                    SearchResponse results = (SearchResponse)ldapConn.SendRequest(findme, TimeSpan.FromSeconds(2)); //Run the query and get results
                    SearchResultEntryCollection entries = results.Entries;
                    for (int i = 0; i < entries.Count; i++)//Iterate through the results
                    {
                        Guid id = new Guid();
                        SearchResultEntry entry = entries[i];
                        TreeNode newNode = new TreeNode();
                        newNode.PopulateOnDemand = false;
                        newNode.Text = entry.DistinguishedName.Remove(entry.DistinguishedName.IndexOf(","));//PSU
                        newNode.Value = entry.DistinguishedName;
                        node.ChildNodes.Add(newNode);
						//if (entry.Attributes.Contains("mail")) //FB 2320
                        if (entry.Attributes.Contains("samaccountname"))
                        {
                            dr = dt.NewRow();
                            dr["ParentId"] = dn;
                            dr["SchemaEntry"] = entry.DistinguishedName;
                            dr["distinguishedName"] = entry.DistinguishedName.ToString();
                            
                            IDictionaryEnumerator attribEnum = entry.Attributes.GetEnumerator();
                            while (attribEnum.MoveNext())//Iterate through the result attributes
                            {
                                //Attributes have one or more values so we iterate through all the values 
                                //for each attribute
                                DirectoryAttribute subAttrib = (DirectoryAttribute)attribEnum.Value;
                                for (int ic = 0; ic < subAttrib.Count; ic++)
                                {
                                    if (attribEnum.Key.ToString().Equals("givenname"))
                                        dr["Givenname"] = subAttrib[ic].ToString();
                                    if (attribEnum.Key.ToString().Equals("uid") || attribEnum.Key.ToString().Equals("samaccountname"))
                                        dr["samaccountname"] = subAttrib[ic].ToString();
                                    if (attribEnum.Key.ToString().Equals("sn"))
                                        dr["sn"] = subAttrib[ic].ToString();
                                    if (attribEnum.Key.ToString().Equals("cn"))
                                        dr["cn"] = subAttrib[ic].ToString();
                                    if (attribEnum.Key.ToString().Equals("mail"))
                                        dr["mail"] = subAttrib[ic].ToString();

                                    String output = attribEnum.Key.ToString() + " = " + subAttrib[ic].ToString();
                                    log.Trace(output);
                                }
                            } 
							//FB 2320	- Starts
                            if (dr["Givenname"].ToString() != "" || dr["sn"].ToString() != "" || dr["mail"].ToString() != "")
                            {
                                //dr["Id"] = dr["mail"].ToString(); 							
                                dr["Id"] = dr["samaccountname"].ToString();
                                dt.Rows.Add(dr);
                            }
                            //FB 2320	- End
                        }
                    }
                    BindGrid(dt);
                    if (grd1.VisibleRowCount > 0)
                        chkSelectAll.Enabled = true;
                    else
                        chkSelectAll.Enabled = false;
                }
            }
            catch (Exception ex)
            {
                if (ex.Message.ToUpper().Contains("SIZE") && LmtExcd.Equals(0))//PSU
                {
                    LmtExcd = 1;
                    BindTreeview(new TreeNode());
                }
                else
                {
                    throw ex;
                }
            }
        }
        #endregion

        //PSU START
        #region PerformPagedSearch
        private List<SearchResultEntry> PerformPagedSearch(LdapConnection connection,string baseDN,string filter, string[] attribs)
        {
            List<SearchResultEntry> results = new List<SearchResultEntry>();
            SearchRequest request = new SearchRequest(

                baseDN,
                filter,
                System.DirectoryServices.Protocols.SearchScope.Subtree,
                attribs
                );

            PageResultRequestControl prc = new PageResultRequestControl(1000);
            request.Controls.Add(prc);
            while (true)
            {
                SearchResponse response = (SearchResponse)ldapConn.SendRequest(request); //Run the query and get results
                SearchResultEntryCollection entries = response.Entries;
                //find the returned page response control
                foreach (DirectoryControl control in response.Controls)
                {
                    if (control is PageResultResponseControl)
                    {
                        //update the cookie for next set
                        prc.Cookie = ((PageResultResponseControl)control).Cookie;
                        break;
                    }
                }
                //add them to our collection
                foreach (SearchResultEntry sre in response.Entries)
                {
                    results.Add(sre);
                }
                //our exit condition is when our cookie is empty
                if (prc.Cookie.Length == 0)
                    break;
            }
            return results;
        }
        #endregion
        //PSU End

        #region CustomPagerBarTemplate
        public class CustomPagerBarTemplate : ITemplate
        {

            ASPxGridView grd1;
            Panel PanelRooms = new Panel();
            enum PageBarButtonType { First, Prev, Next, Last }

            protected ASPxGridView Grid { get { return grd1; } }

            LDAPImport ldp = null;


            public void InstantiateIn(Control container)
            {
                this.grd1 = (ASPxGridView)((GridViewPagerBarTemplateContainer)container).Grid;
                Table table = new Table();
                container.Controls.Add(table);
                TableRow row = new TableRow();
                table.Rows.Add(row);
                AddButtonCell(row.Cells, IsButtonEnabled(PageBarButtonType.First), "First", "pageBarFirstButton_Click");
                AddButtonCell(row.Cells, IsButtonEnabled(PageBarButtonType.Prev), "Prev", "pageBarPrevButton_Click");
                AddLiteralCell(row.Cells, "Page");
                AddTextBoxCell(row.Cells);
                AddLiteralCell(row.Cells, string.Format("of {0}", grd1.PageCount));
                AddButtonCell(row.Cells, IsButtonEnabled(PageBarButtonType.Next), "Next", "pageBarNextButton_Click");
                AddButtonCell(row.Cells, IsButtonEnabled(PageBarButtonType.Last), "Last", "pageBarLastButton_Click");
                AddSpacerCell(row.Cells);
                AddLiteralCell(row.Cells, "Records per page:");
                AddComboBoxCell(row.Cells);
            }
            void AddButtonCell(TableCellCollection cells, bool enabled, string text, string clickHandlerName)
            {
                TableCell cell = new TableCell();
                cells.Add(cell);
                ASPxButton button = new ASPxButton();
                cell.Controls.Add(button);
                button.Text = text;
                button.AutoPostBack = false;
                button.UseSubmitBehavior = false;
                button.Enabled = enabled;
                if (enabled)
                    button.ClientSideEvents.Click = clickHandlerName;
            }
            void AddTextBoxCell(TableCellCollection cells)
            {
                TableCell cell = new TableCell();
                cells.Add(cell);
                ASPxTextBox textBox = new ASPxTextBox();
                cell.Controls.Add(textBox);
                textBox.Width = 30;
                int pageNumber = grd1.PageIndex + 1;
                textBox.JSProperties["cpText"] = pageNumber;
                textBox.ClientSideEvents.Init = "pageBarTextBox_Init";
                textBox.ClientSideEvents.ValueChanged = "pageBarTextBox_ValueChanged";
                textBox.ClientSideEvents.KeyPress = "pageBarTextBox_KeyPress";
            }
            void AddComboBoxCell(TableCellCollection cells)
            {                
                TableCell cell = new TableCell();
                cells.Add(cell);
                DropDownList comboBox = new DropDownList();
                cell.Controls.Add(comboBox);
                comboBox.Width = 60;
                comboBox.BorderWidth = 1;   //Edited for FF
                comboBox.BorderColor = System.Drawing.Color.Gray;  //Edited for FF
                comboBox.Items.Add(new ListItem("5"));
                comboBox.Items.Add(new ListItem("10"));
                comboBox.Items.Add(new ListItem("20"));
                comboBox.Items.Add(new ListItem("30"));
                comboBox.Items.Add(new ListItem("40"));
                comboBox.Items.Add(new ListItem("50"));
                comboBox.Items.Add(new ListItem("60"));
                comboBox.SelectedValue = grd1.SettingsPager.PageSize.ToString();
                comboBox.Attributes.Add("onchange", "javascript:pagerBarComboBox_SelectedIndexChanged(this)");

                ldp = new LDAPImport();

                if (ldp.Session["combo"].ToString().Equals("1"))
                    comboBox.Enabled = false;
                
            }
            void AddLiteralCell(TableCellCollection cells, string text)
            {
                TableCell cell = new TableCell();
                cells.Add(cell);
                cell.Text = text;
                cell.Wrap = false;
            }
            void AddSpacerCell(TableCellCollection cells)
            {
                TableCell cell = new TableCell();
                cells.Add(cell);
                cell.Width = Unit.Percentage(100);
            }
            bool IsButtonEnabled(PageBarButtonType type)
            {
                if (grd1.PageIndex == 0)
                {
                    if (type == PageBarButtonType.First || type == PageBarButtonType.Prev)
                        return false;
                }
                if (grd1.PageIndex == grd1.PageCount - 1)
                {
                    if (type == PageBarButtonType.Next || type == PageBarButtonType.Last)
                        return false;
                }
                return true;
            }
        }
        #endregion

        #region treeList1_SelectedNodeChanged
        protected void treeList1_SelectedNodeChanged(object sender, EventArgs e)
        {
            if (treeList1.Nodes[0].Value != "0")
            {
                if (chkSelectAll.Checked)
                {
                    chkSelectAll.Checked = false;
                    
                }
                treeList1.SelectedNode.ChildNodes.Clear();//Fixes
                BindTreeview(treeList1.SelectedNode);
                //myVRMDirectoryEntry myVRMDE = immLDAP.newDEitem(immLDAP.myVRMDE);
                //DataTable dtDE = GetDETable(myVRMDE);
                //BindGrid(dtDE);
                //if (grd1.VisibleRowCount > 0)
                //    chkSelectAll.Enabled = true;
                //else
                //    chkSelectAll.Enabled = false;
                UpdatePanel1.Update();//Fixes
            }
        }
        #endregion

        #region BindData
        private void BindData()
        {
            try
            {
                String inXML = "<login>";
                inXML += "  <userID>" + Session["userID"].ToString() + "</userID>";
                inXML += "  <searchFor/>";
                inXML += "  </login>";
                string outxml = obj.CallMyVRMServer("GetLDAPUsers", inXML, Application["MyVRMServer_ConfigPath"].ToString());
                if (outxml.IndexOf("<error>") >= 0)
                {
                    Session.Add("errMsg", obj.ShowErrorMessage(outxml));
                }
                else
                {
                    XmlDocument xmldoc = new XmlDocument();
                    xmldoc.LoadXml(outxml);
                    lblServeraddr.Text = xmldoc.SelectSingleNode("//GetLDAPUsers/ServerDetails/ServerAddress").InnerText.Trim();
                    lblLoginname.Text = xmldoc.SelectSingleNode("//GetLDAPUsers/ServerDetails/Username").InnerText.Trim();
                    lblPassword.Text = xmldoc.SelectSingleNode("//GetLDAPUsers/ServerDetails/Password").InnerText.Trim();
                    lblDomain.Text = xmldoc.SelectSingleNode("//GetLDAPUsers/ServerDetails/Domain").InnerText.Trim();
                    srcFilter = xmldoc.SelectSingleNode("//GetLDAPUsers/ServerDetails/SearchFilter").InnerText.Trim(); //FB 2096
                    loginKey = xmldoc.SelectSingleNode("//GetLDAPUsers/ServerDetails/LoginKey").InnerText.Trim(); //FB 2096
                    int.TryParse(xmldoc.SelectSingleNode("//GetLDAPUsers/ServerDetails/AuthenticationType").InnerText.Trim(),out AuthenticationType); //FB 2993 LDAP
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region GenerateColumns
        private void GenerateColumns(ref DataTable pDT)
        {
            try
            {

                pDT.Columns.Add("Id");
                pDT.Columns.Add("ParentId");
                pDT.Columns.Add("SchemaEntry");
                pDT.Columns.Add("ObjectClass");
                pDT.Columns.Add("ObjectName");
                pDT.Columns.Add("Name");
                pDT.Columns.Add("path");
                pDT.Columns.Add("Givenname");
                pDT.Columns.Add("sn");
                pDT.Columns.Add("cn");
                pDT.Columns.Add("mail");
                pDT.Columns.Add("samaccountname");
                pDT.Columns.Add("countrycode");
                pDT.Columns.Add("whenchanged");
                pDT.Columns.Add("whencreated");
                pDT.Columns.Add("telephonenumber");
                pDT.Columns.Add("distinguishedName");
                pDT.Columns.Add("instanceType");
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        private void BindGrid(DataTable dtDE)
        {
            try
            {
                grd1.DataSource = null;
                grd1.DataSourceID = "";
                //grd1.PageIndex = 0;
                //grd1.ClearSort();
                grd1.DetailRows.CollapseAllRows();

                grd1.DataSource = dtDE;
                grd1.KeyFieldName = "Id";
                grd1.DataBind();

                Session.Add("dtdE", dtDE);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        private void BindUserGrid(DataTable dtSelected)
        {
            try
            {
                SelectedGrid.DataSource = null;

                SelectedGrid.DataSource = dtSelected;
                SelectedGrid.DataBind();

                if (Session["dtSave"] != null)
                    Session["dtSave"] = dtSelected;
                else
                    Session.Add("dtSave", dtSelected);

                if (clrSelgrd.Value.Equals("1"))//Fixes
                    Session["dtSave"] = "";//Fixes

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        protected void ASPxGridView1_HtmlRowCreated(object sender, DevExpress.Web.ASPxGridView.ASPxGridViewTableRowEventArgs e)
        {
            try
            {

                if (e.RowType == GridViewRowType.Data)
                {
                    HyperLink lblh = ((ASPxGridView)sender).FindRowTemplateControl(e.VisibleIndex, "Hyper1") as HyperLink;
                    lblh.Attributes.Add("onclick", "Javascript:Addusers('" + e.KeyValue.ToString() + "')");
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #region User Attribute

        protected void SetUserAttributes(Object sender, DataGridItemEventArgs e)
        {
            try
            {
                if ((e.Item.ItemType.Equals(ListItemType.Item)) || (e.Item.ItemType.Equals(ListItemType.AlternatingItem)))
                {
                    DataRowView row = e.Item.DataItem as DataRowView;

                    HtmlImage imageDel = (HtmlImage)e.Item.FindControl("ImageDel");

                    if (imageDel != null)
                        imageDel.Attributes.Add("onclick", "javascript:Delrooms('" + e.Item.Cells[0].Text.Trim() + "');");

                    HtmlTableCell uTd = (HtmlTableCell)e.Item.FindControl("usrName"); //FB 1623, 1624

                    string uName = "";
                    if (row["Givenname"] != null)
                        uName = row["Givenname"].ToString().Trim();

                    if (row["sn"] != null)
                        uName += " " + row["sn"].ToString().Trim();

                    if (uName.Trim() == "")
                    {
                        if (row["mail"] != null)
                            uName = row["mail"].ToString().Trim();
                    }
                    uTd.InnerText = uName;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion


        public void GetselectedRooms()
        {
            DataRow[] selrows = null;
            DataTable dtselected = null;
            DataTable SDTable = null;
            String selusers = "";
            string rows = "";
            DataTable selSession = null;//Fixes
            DataRow[] chkSelRows = null;//Fixes

            try
            {
                if (selectedlocframe.Value != "")
                {
                    foreach (String s in selectedlocframe.Value.Split('§')) //(§ - Alt789) - FB 2320
                    {
                        if (s != "")
                        {
                            if (selusers == "")
                                selusers = s.Trim();
                            else
                                selusers += "','" + s.Trim();
                        }
                    }
                    //Fixes START
                    SDTable = Session["dtdE"] as DataTable;

                    if (Session["dtDefault"] == null)
                    {
                        if (SDTable != null)
                            Session.Add("dtDefault", SDTable.Clone());
                    }

                    selSession = Session["dtDefault"] as DataTable;


                    selrows = SDTable.Select();

                    foreach (DataRow rw in selrows)
                    {
                        //if(!selSession.Rows.Contains(rw["Id"].ToString()))
                        chkSelRows = selSession.Select("Id in ('" + rw["Id"].ToString() + "')");
                        if (chkSelRows.Length == 0)
                            selSession.ImportRow(rw);
                    }

                    SDTable = selSession;
                    //Fixes END
                    selrows = SDTable.Select("Id in ('" + selusers + "')");

                    if (selrows != null)
                    {
                        if (selrows.Length > 0)
                        {

                            dtselected = SDTable.Clone();
                            foreach (DataRow rw in selrows)
                            {
                                dtselected.ImportRow(rw);
                            }
                        }
                    }

                }
                else
                    dtselected = new DataTable();

                BindUserGrid(dtselected);


            }
            catch (Exception ex)
            {
                throw ex;
            }

            if (selSession != null)
                Session["dtDefault"] = selSession;
        }

        protected void grd1_CustomCallback(object sender, ASPxGridViewCustomCallbackEventArgs e)
        {
            GridPageSize = int.Parse(e.Parameters);
            grd1.SettingsPager.PageSize = GridPageSize;
            grd1.DataBind();
        }

        protected void Grid_DataBound(object sender, EventArgs e)
        {
            grd1.JSProperties["cpPageCount"] = grd1.PageCount;
        }



        private SearchRequest getSearchRequest(String DN, String srchFilter, System.DirectoryServices.Protocols.SearchScope srchScope)
        {
            try
            {
                SearchRequest findme = new SearchRequest();
                //findme.DistinguishedName = "OU=OffShore,OU=Users,OU=MyBusiness,DC=myvrm,DC=local"; //Find all People in this ou
                findme.DistinguishedName = DN; //Find all People in this ou
                //findme.Filter = "(objectClass=*)"; //The type of entry we are looking for
                findme.Scope = srchScope; //We want all 


                return findme;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        //New LDAP design START
        private void RunAsyncSearch(IAsyncResult asyncResult)
        {

            //Console.WriteLine("Asynchronous search operation called.");
            log.Trace("Asynchronous search operation called.");

            if (!asyncResult.IsCompleted)
            {
                //Console.WriteLine("Getting a partial result");
                log.Trace("Getting a partial result");
                PartialResultsCollection result = null;

                try
                {
                    result = ldapConn.GetPartialResults(asyncResult);
                }
                catch (Exception e)
                {
                    errLabel.Text = e.Message;
                    errLabel.Visible = true;
                    //Console.WriteLine(e.Message);
                }
                if (result != null)
                {
                    for (int i = 0; i < result.Count; i++)
                    {
                        if (result[i] is SearchResultEntry)
                        {
                            SearchResultEntry entry = ((SearchResultEntry)result[i]);

                            //Console.WriteLine("DN = {0}", entry.DistinguishedName);
                            log.Trace("DN = {0}" + entry.DistinguishedName);
                            IDictionaryEnumerator attribEnum = entry.Attributes.GetEnumerator();
                            while (attribEnum.MoveNext())//Iterate through the result attributes
                            {
                                //Attributes have one or more values so we iterate through all the values 
                                //for each attribute
                                DirectoryAttribute subAttrib = (DirectoryAttribute)attribEnum.Value;
                                for (int ic = 0; ic < subAttrib.Count; ic++)
                                {
                                    //Attribute Name below
                                    String output = attribEnum.Key.ToString() + " = " + subAttrib[ic].ToString();
                                    //Console.WriteLine(output);
                                    log.Trace(output);

                                }
                            }
                        }
                    }
                }
                else
                    //Console.WriteLine("Search result is null");
                    log.Trace("Search result is null");
            }
            else
            {
                //Console.WriteLine("The search operation has been completed.");
                log.Trace("The search operation has been completed.");
                try
                {
                    // end the send request search operation
                    SearchResponse response = (SearchResponse)ldapConn.EndSendRequest(asyncResult);

                    foreach (SearchResultEntry entry in response.Entries)
                    {
                        //Console.WriteLine("{0}:{1}",
                        //    response.Entries.IndexOf(entry),
                        //    entry.DistinguishedName);

                        log.Trace("{0}:{1}" +
                            response.Entries.IndexOf(entry) +
                            entry.DistinguishedName);
                        IDictionaryEnumerator attribEnum = entry.Attributes.GetEnumerator();
                        while (attribEnum.MoveNext())//Iterate through the result attributes
                        {
                            //Attributes have one or more values so we iterate through all the values 
                            //for each attribute
                            DirectoryAttribute subAttrib = (DirectoryAttribute)attribEnum.Value;
                            for (int ic = 0; ic < subAttrib.Count; ic++)
                            {
                                //Attribute Name below
                                String output = attribEnum.Key.ToString() + " = " + subAttrib[ic].ToString();
                                //Console.WriteLine(output);
                                log.Trace(output);
                            }
                        }
                    }
                }
                // in case of some directory operation exception
                // return whatever data has been processed
                catch (DirectoryOperationException e)
                {
                    //Console.WriteLine(e.Message);
                    log.Trace(e.Message);
                    SearchResponse response = (SearchResponse)e.Response;

                    foreach (SearchResultEntry entry in response.Entries)
                    {
                        //Console.WriteLine("{0}:{1}",
                        //    response.Entries.IndexOf(entry),
                        //    entry.DistinguishedName);
                        log.Trace("{0}:{1}" +
                            response.Entries.IndexOf(entry) +
                            entry.DistinguishedName);
                    }
                }
                catch (LdapException e)
                {
                    //Console.WriteLine(e.Message);
                    log.Trace(e.Message);
                }
            }
        }
        //new LDAP Design END

    }
    public class myVRMLDAP
    {
        // Fields
        private string _defaultNamingContext = "";
        private string _hostname = "";
        private string _password = "";
        private string _username = "";
        private DirectoryEntry de;
        public DirectoryEntry myVRMDE
        {
            get { return de; }
            set { de = value; }
        }
        private myVRMDirectoryEntryCollection _entrycoll;
        public myVRMDirectoryEntryCollection Entrycoll
        {
            get { return _entrycoll; }
            set { _entrycoll = value; }
        }

        // Methods
        [Description("Returns a collection of Directory Entry Objects, that are the children of the passed object")]
        public virtual void AddChildrens(DirectoryEntry de)
        {
            try
            {
                if (_entrycoll == null)
                    _entrycoll = new myVRMDirectoryEntryCollection();

                myVRMDirectoryEntry tmpmyVRMDE = this.newDEitem(de);
                _entrycoll.Add(tmpmyVRMDE);
                foreach (DirectoryEntry entry in de.Children)
                {
                    try
                    {
                        myVRMDirectoryEntry myVRMDE = this.newDEitem(entry);
                        if (_entrycoll == null)
                            _entrycoll = new myVRMDirectoryEntryCollection();
                        _entrycoll.Add(myVRMDE);
                        //AddChildrens(myVRMDE.DE);//To Call Recursive
                        continue;
                    }
                    catch
                    {
                        throw new ApplicationException("AddChildrens: Unable to add directory item to collection: " + entry.Name);
                    }
                }
            }
            catch
            {
                throw new ApplicationException("AddChildrens: Too many items in this collection");
            }
            //Int32 cnt = _entrycoll.Count;
        }

        [Description("Returns the property of an Directory Object")]
        public virtual string getProperties(DirectoryEntry objADObject, string objectType)
        {
            string str = "";
            Int32 prptyCnt = 0;
            try
            {
                prptyCnt = objADObject.Properties[objectType].Count;
                if (prptyCnt > 0)
                {
                    for (int i = 0; i < prptyCnt; i++)
                    {
                        if (str == "")
                            str = objADObject.Properties[objectType][i].ToString();
                        else
                            str += "||" + objADObject.Properties[objectType][i].ToString();
                    }
                }
                //return str;
            }
            catch
            {
                str = "";
                //throw new ApplicationException("getProperties : Error getting property for : " + objectType);
            }
            return str;
        }

        [Description("Returns the root LDAP Object")]
        public virtual DirectoryEntry GetRootObject()
        {
            this.de = new DirectoryEntry();
            this.de.Username = this._username;
            this.de.Password = this._password;
            this.de.Path = "LDAP://" + this._hostname + this._defaultNamingContext;
            this.de.SchemaEntry.UsePropertyCache = true;
            return this.de;
        }

        [Description("Returns the LDAP DirectoryEntry Collection")]
        public virtual myVRMDirectoryEntryCollection GetDirectoryEntryCollection(String srchFilter)
        {
            myVRMDirectoryEntryCollection deColl = new myVRMDirectoryEntryCollection();
            this.de = new DirectoryEntry();
            this.de.Username = this._username;
            this.de.Password = this._password;
            this.de.Path = "LDAP://" + this._hostname + this._defaultNamingContext;
            this.de.SchemaEntry.UsePropertyCache = true;
            DirectorySearcher dsr = new DirectorySearcher(this.de);
            dsr.Filter = ("(|(name=" + srchFilter + "*)(cn=" + srchFilter + "*)(samaccountname=" + srchFilter + "*)(mail=" + srchFilter + "*)(sn=" + srchFilter + "*)(Givenname=" + srchFilter + "*))");
            SearchResultCollection objSrchResults = dsr.FindAll();
            foreach (SearchResult srchRslt in objSrchResults)
            {
                deColl.Add(this.newDEitem(srchRslt.GetDirectoryEntry()));
            }
            return deColl;
        }

        [Description("Returns the LDAP DirectoryEntry Collection")]
        public virtual void AddDirectoryEntryCollectionByObjName(String srchFilter)
        {
            myVRMDirectoryEntryCollection deColl = new myVRMDirectoryEntryCollection();
            this.de = new DirectoryEntry();
            this.de.Username = this._username;
            this.de.Password = this._password;
            this.de.Path = "LDAP://" + this._hostname + this._defaultNamingContext;
            this.de.SchemaEntry.UsePropertyCache = true;
            DirectorySearcher dsr = new DirectorySearcher(this.de);
            dsr.Filter = ("(&(NativeGuid=" + srchFilter + "))");
            SearchResultCollection objSrchResults = dsr.FindAll();
            _entrycoll = new myVRMDirectoryEntryCollection();
            foreach (SearchResult srchRslt in objSrchResults)
            {
                this.AddChildrens(srchRslt.GetDirectoryEntry());
            }
        }

        [Description("Returns the LDAP DirectoryEntry for the given Object Name")]
        public virtual DirectoryEntry GetDirectoryEntry(String path)
        {
            this.de = new DirectoryEntry();
            this.de.Username = this._username;
            this.de.Password = this._password;
            this.de.Path = "LDAP://" + this._hostname + "/<GUID=" + path + ">";
            this.de.SchemaEntry.UsePropertyCache = true;
            return de;
        }

        public virtual string n()
        {
            return "oLDAP";
        }

        public myVRMDirectoryEntry newDEitem(DirectoryEntry de)
        {
            string[] strArray = null;
            char[] separator = "=".ToCharArray();
            try
            {
                strArray = de.Name.Split(separator, 2);
            }
            catch
            {
                strArray[0] = "";
            }
            myVRMDirectoryEntry entry = new myVRMDirectoryEntry();
            entry.DE = de;
            entry.ObjectClass = strArray[0].ToLower();
            entry.ObjectName = de.Name;
            return entry;
        }

        [Description("Sets the default naming context")]
        public virtual void SetNamingContext()
        {
            this._defaultNamingContext = "";
        }

        // Properties
        [Description("Set the hostname")]
        public string HostName
        {
            set
            {
                this._hostname = value;
            }
        }

        [Description("Set the password to logon with Optional")]
        public string Password
        {
            set
            {
                this._password = value;
            }
        }

        [Description("Set the username to logon with Optional")]
        public string UserName
        {
            set
            {
                this._username = value;
            }
        }
    }

    public class myVRMDirectoryEntryCollection : ArrayList
    {
        // Methods
        public myVRMDirectoryEntryCollection()
        { }
    }

    public class myVRMDirectoryEntry
    {
        // Fields
        private DirectoryEntry _de;
        private string _objclass;
        private string _objname;

        // Properties
        public DirectoryEntry DE
        {
            get
            {
                return this._de;
            }
            set
            {
                this._de = value;
            }
        }

        public string ObjectClass
        {
            get
            {
                return this._objclass;
            }
            set
            {
                this._objclass = value;
            }
        }

        public string ObjectName
        {
            get
            {
                return this._objname;
            }
            set
            {
                this._objname = value;
            }
        }
    }

    public class NDS : myVRMLDAP
    {
        // Methods
        public override string n()
        {
            return "NDS";
        }
    }

    public class ActiveDirectory : myVRMLDAP
    {
        // Fields
        //private string _defaultNamingContext;

        // Methods
        public override string n()
        {
            return "AD";
        }

        public override void SetNamingContext()
        {
            base.SetNamingContext();
        }
    }

}