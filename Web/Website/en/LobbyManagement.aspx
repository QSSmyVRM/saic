<%--ZD 100147 start--%>
<%--/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.	
*
* You should have received a copy of the myVRM license with	
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/--%><%--ZD 100147 End--%>
<%@ Page Language="C#" AutoEventWireup="true" Inherits="MyVRMNet.LobbyManagement" %>
<%--FB 2779 Starts--%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<meta http-equiv="X-UA-Compatible" content="IE=8" />
<!-- #INCLUDE FILE="inc/maintopNET.aspx" -->
<%--FB 2779 Ends--%>

<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Lobby</title>
    <style type="text/css">
        img
        {
            border: none;
        }
        .big a
        {
            cursor: default;
        }
        .small a
        {
            cursor: default;
        }
        .big
        {
            position: relative; /*background-color:lightyellow;*/
            cursor: move;
            z-index: 10;
            width: 100%;
            height: 125px;
            margin-top:10px;
        }
        .small
        {
            position: relative; /*background-color:Green;*/
            /*cursor: move;*/
            z-index: 10;
            width: 100%;
            /*height: 80px;*/
            margin-top: 10px;
        }
        .big span
        {
            color: #555555; /* FB 2785 */
            font-weight: bold;
            font-size: 12px;
            cursor: default;
        }
        .small span
        {
            color: gray;
            font-weight: normal;
            font-size: 10px;
            cursor: default;
        }
        .big a img
        {
            border: none;
            width: 50px; /*50*/
            height: 50px; /*50*/
            filter: alpha(opacity=100);
            opacity: 1;
        }
        .big a img:hover
        {
            border: none;
            width: 50px;
            height: 50px;
            filter: alpha(opacity=100);
            opacity: 1;
        }
        /*style="opacity:0.4;filter:alpha(opacity=50)"

onmouseover="this.style.opacity=1;this.filters.alpha.opacity=100"

onmouseout="this.style.opacity=0.4;this.filters.alpha.opacity=50"*/
        .small a img
        {
            border: none;
            width: 40px;
            height: 40px;
            filter: alpha(opacity=50); /*-moz-opacity:0.5;*/
            opacity: 0.5;
        }
        /*
.small img:hover
{
	border:none;
	width:40px;
	height:40px;
	filter:alpha(opacity=100);
    -moz-opacity:1;
    opacity:1;
}
*/
        .tdx
        {
            border: solid 1px lightgray;
            width: 11%;
            vertical-align: middle;
        }
        .td2
        {
            border: solid 1px white;
        }
        .contBorder td
        {
            border: 'solid 1px lightgray';
        }
    </style>

    <script type="text/javascript">

function noError(){return true;}
window.onerror = noError;


var _startX = 0;            // mouse starting positions
var _startY = 0;
var _offsetX = 0;           // current element offset
var _offsetY = 0;
var _dragElement;           // needs to be passed from OnMouseDown to OnMouseMove
var _oldZIndex = 0;         // we temporarily increase the z-index during drag
//var _debug = $('debug');    // makes life easier
var draging = -1;
var mouseStatus = -1;

var reNameIcon = "";

var globChange = -1;

// mouseX and mouseY use grab mouse position of the clicked label
var mouseX = 0;
var mouseY = 0;

InitDragDrop();



function InitDragDrop()
{
    document.onmousedown = OnMouseDown;
    document.onmouseup = OnMouseUp;
    document.onkeypress = getKey;


}

function getKey(e)
{
    if (e == null) 
        var e = window.event;
        
	if(e.keyCode==27)
	    document.getElementById('renameDiv').style.display = 'none';
	    
	if(e.keyCode==13)
	    updtName();
}

function findPosX(obj)
  {
    var curleft = 0;
    if(obj.offsetParent)
        while(1) 
        {
          curleft += obj.offsetLeft;
          if(!obj.offsetParent)
            break;
          obj = obj.offsetParent;
        }
    else if(obj.x)
        curleft += obj.x;
    return curleft;
  }

  function findPosY(obj)
  {
    var curtop = 0;
    if(obj.offsetParent)
        while(1)
        {
          curtop += obj.offsetTop;
          if(!obj.offsetParent)
            break;
          obj = obj.offsetParent;
        }
    else if(obj.y)
        curtop += obj.y;
    return curtop;
  }

function OnMouseDown(e)
{
    // IE is retarded and doesn't pass the event object
    if (e == null) 
        e = window.event; 
    
    // IE uses srcElement, others use target
    var target = e.target != null ? e.target : e.srcElement;
    
    //_debug.innerHTML = target.className == 'drag' 
    //    ? 'draggable element clicked' 
    //    : 'NON-draggable element clicked';

    // This block takes mouse x and y position to display rename div
	if (e.pageX || e.pageY)
	{
		mouseX = e.pageX;
		mouseY = e.pageY;
	}
	else if (e.clientX || e.clientY)
	{
		mouseX = e.clientX + document.body.scrollLeft + document.documentElement.scrollLeft;
		mouseY = e.clientY + document.body.scrollTop + document.documentElement.scrollTop;
	}
	
    // for IE, left click == 1
    // for Firefox, left click == 0
    
    
    //This block prevent the exchange of empty contents by assigning dragging=0;
    var source=target.innerHTML.length;
    
    if (source!=null)
    {
    if(source < 50)
    draging = 0;
    else
    draging = 1;
    }
    
    mouseStatus = 1;
    
    if ((e.button == 1 && window.event != null || e.button == 0) && (target.className == 'big')) // || (target.className == 'small'))
    {
        // grab the mouse position
        
        _startX = e.clientX;
        _startY = e.clientY;
        
        //alert(_startX + ',' + _startY);
        
        // grab the clicked element's position

        _offsetX = ExtractNumber(target.style.left);
        _offsetY = ExtractNumber(target.style.top);
        
        //_startX = findPosX(target) - 5;
        //_startY = findPosY(target) - (document.body.scrollTop+5);
        
        //alert(findPosX(target));
        //alert(findPosY(target));
       
        // bring the clicked element to the front while it is being dragged
        _oldZIndex = target.style.zIndex;
        target.style.zIndex = 8;
        
        // we need to access the element in OnMouseMove
        _dragElement = target;

        // tell our code to start moving the element with the mouse
        document.onmousemove = OnMouseMove;
        
        // cancel out any text selections
        document.body.focus();

        // prevent text selection in IE
        document.onselectstart = function () { return false; };
        // prevent IE from trying to drag an image
        target.ondragstart = function() { return false; };
        
        // prevent text selection (except IE)
        return false;
    }
}


function OnMouseMove(e)
{
    if (e == null) 
        var e = window.event;
        
    /*
    if((document.body.scrollTop + document.documentElement.scrollTop + screen.height) < 1250) // FB 2050
    {
        if(e.clientY > (screen.height-250) && e.clientY < (screen.height-100))
        {
        document.body.scrollTop += 10;
        document.documentElement.scrollTop += 10; // FB 2050
        _startY -= 10;
        }
    }
    
    if(document.body.scrollTop + document.documentElement.scrollTop > 0) // FB 2050
    {
        if(e.clientY > 50 && e.clientY < 200)
        {
        document.body.scrollTop -= 10;
        document.documentElement.scrollTop -= 10; // FB 2050
        _startY += 10;
        }
    }
    */

    // this is the actual "drag code"
    _dragElement.style.left = (_offsetX + e.clientX - _startX) + 'px';
    _dragElement.style.top = (_offsetY + e.clientY - _startY) + 'px';
    
    //alert(e.clientY); screen.height
    //document.body.scrollTop += 10;

    
    //_debug.innerHTML = '(' + _dragElement.style.left + ', ' + _dragElement.style.top + ')';   
}

function OnMouseUp(e)
{
//document.getElementById('pane1').innerHTML="";

//debugger;
//alert(e.target.id);
    if (e == null) 
        e = window.event; 
    
    // IE uses srcElement, others use target
    var target = e.target != null ? e.target : e.srcElement;
    
    var swap=target.innerHTML;
    
    mouseStatus = 0;

    if (_dragElement != null)
    {
        _dragElement.style.zIndex = _oldZIndex;

        // we're done with these events until the next OnMouseDown
        document.onmousemove = null;
        document.onselectstart = null;
        _dragElement.ondragstart = null;

        // this is how we know we're not dragging      
        //_dragElement = null;
        
        _dragElement.style.left=_offsetX;
        _dragElement.style.top=_offsetY;
        
        var elementId = target.id.substring(0,4);
        //if((elementId=="pane" || elementId=="cont") && draging != "0")
        if(elementId=="pane" && draging != "0")
        {
        //alert(target.id.substring(0,4));
        //alert(_dragElement.id.substring(0,4));
        
        target.innerHTML=_dragElement.innerHTML;
        _dragElement.innerHTML=swap;
        
        //fnRefreshIconSet();

        globChange = 1;
        //document.getElementById('<%=saveLink.ClientID %>').innerHTML = 'Click here to save changes';
        
            /*
            if(document.getElementById(target.childNodes[1].childNodes[0].id)!=null)
            {
            //alert('inside targe');
                document.getElementById(target.childNodes[1].childNodes[0].id).removeAttribute("style");
                if(target.id.substring(0,4)=="cont")
                {
                reNameIcon=target.childNodes[3].id;
                changeDefault();
                }
            }
            
            if(document.getElementById(_dragElement.childNodes[1].childNodes[0].id)!=null)
            {
            //alert('inside dragelement');
                document.getElementById(_dragElement.childNodes[1].childNodes[0].id).removeAttribute("style");
                if(_dragElement.id.substring(0,4)=="cont")
                {
                reNameIcon=_dragElement.childNodes[3].id;
                changeDefault();
                }
            }
            */

//            if(target.id.substring(0,4)=="pane")
//            document.getElementById(target.childNodes[1].childNodes[0].id).setAttribute("style", "opacity:1;");
//            else
//            document.getElementById(target.childNodes[1].childNodes[0].id).setAttribute("style", "opacity:0.5;");
        }
        //alert(target.id); // destination
        //alert(_dragElement.id); //target
        
        //_debug.innerHTML = 'mouse up';
        _dragElement = null;
    }
}    

function fnTrim(str)     
{     
    return str.replace(/^\s+|\s+$/g,"");     
}

function fnMoveIcon(inf)
{
var chkBx = document.getElementById(inf);
var htmlCont = chkBx.parentNode.innerHTML;
    if(chkBx.checked == true)
    {
        // pane11-35
        
        var paneid='';
        var tagContent='';
        var updtStat = 0;
        for(var m=11; m<36; m++)
        {
            paneid = 'pane' + m;
            tagContent= fnTrim(document.getElementById(paneid).innerHTML);
            if(tagContent == "")
            {
            //alert(document.getElementById(paneid).innerHTML);
            //chkBx.checked = true;
            htmlCont= htmlCont.replace('type="checkbox"','type="checkbox" style="display:none"');
            htmlCont= htmlCont.replace("type=checkbox","type=checkbox style=display:none");
            htmlCont= htmlCont.replace("xcont","xpane");
            htmlCont= htmlCont.replace("fnRemoveIcon(0)","fnRemoveIcon(this.parentNode.parentNode.childNodes[0].id)");
            document.getElementById(paneid).innerHTML = htmlCont;
            //chkBx.parentNode.innerHTML = "";
            globChange = 1;
            updtStat = 1;
            //fnRefreshIconSet();
            break;
            }
        }
        if(updtStat == 0)
        {
        alert("Maximum of 25 icons can be customized for your lobby");
        htmlCont= htmlCont.replace('checked=""','');
        htmlCont= htmlCont.replace("CHECKED","");
        chkBx.parentNode.innerHTML = htmlCont;
        }
        
        //alert(chkBx.parentNode.innerHTML);
    }
    else
    {
        // cont11-62
        var contid='';
        var tagData='';
        //for(var n=11; n<63; n++) // CHANGE N RANGE
        for(var n=11; n<36; n++)
        {
            contid = 'pane' + n;
            tagData=fnTrim(document.getElementById(contid).innerHTML);
            if(tagData.indexOf(inf)>-1)
            {
            //alert(document.getElementById(contid).innerHTML);
            //chkBx.checked = false;
            document.getElementById(contid).innerHTML = "&nbsp;";
            document.getElementById(contid).innerHTML = "";
            globChange = 1;
            //alert(n);
            break;
            }
        }
        // FB 2863 Starts
        htmlCont = htmlCont.replace('checked=""', '');
        htmlCont = htmlCont.replace("CHECKED", "");
        chkBx.parentNode.innerHTML = htmlCont;
        // FB 2863 Ends

        //alert(chkBx.parentNode.innerHTML);
    }
}


function fnRemoveIcon(inf)
{
if(inf == 0)
return false;
/*
if(!confirm("Are you sure to remove this icon from preview section?"))
return false;
*/
var paneid='';
var tagContent='';
for(var m=11; m<36; m++)
{
    paneid = 'pane' + m;
    tagContent= fnTrim(document.getElementById(paneid).innerHTML);
    if(tagContent.indexOf(inf)>-1)
    {
    //alert(document.getElementById(contid).innerHTML);
    //chkBx.checked = false;
    document.getElementById(paneid).innerHTML = "&nbsp;";
    document.getElementById(paneid).innerHTML = "";
    break;
    }
}


var contid='';
var tagData='';
for(var n=11; n<63; n++)
{
    contid = 'cont' + n;
    tagData=fnTrim(document.getElementById(contid).innerHTML);
    if(tagData.indexOf(inf)>-1)
    {
    //alert(document.getElementById(contid).innerHTML);
    //chkBx.checked = false;
    tagData= tagData.replace('checked=""','');
    tagData= tagData.replace("CHECKED","");
    document.getElementById(contid).innerHTML = tagData;
    break;
    }
}

globChange = 1;
}

/*
function fnCheckCell()
{
var paneid='';
var tagContent='';
var updtStat = false;
    for(var m=11; m<36; m++)
    {
        paneid = 'pane' + m;
        tagContent= fnTrim(document.getElementById(paneid).innerHTML);
            if(tagContent != "")
            {
            updtStat = true;
            }
    }
    
return updtStat;
}
*/


function fnGroupChecked()
{
/*
if(fnCheckCell() == false)
{
alert("No icon to group");
return false;
}
*/
//alert("fnGroupChecked");
    var tagHtmlChecked = new Array(52);
    var tagHtmlUnchecked = new Array(52);
    var htmlEleId = '';
    var trimEle ='';
    
    for(var t=0; t<52; t++)
    {
        tagHtmlChecked[t]="";
        tagHtmlUnchecked[t]="";
    }
    
    var chkbx = '';
    var chkObj;
    var ind = 0;
    var inx = 0;
    //for(var t1=1; t1<53; t1++)
    for(var t1=11; t1<63; t1++)
    {
        //htmlEleId = 'cont' + t;
        chkbx = 'chk' + t1;
        chkObj = document.getElementById(chkbx);
        if(chkObj == null)
        break;
        trimEle=fnTrim(chkObj.parentNode.innerHTML);
        if(chkObj.checked == true)
        {
            if(trimEle.indexOf("checked")>-1 || trimEle.indexOf("CHECKED")>-1)
            {
            tagHtmlChecked[ind]=trimEle;
            //document.getElementById(htmlEleId).innerHTML = "";
            }
            else
            {
            trimEle = trimEle.replace('type="checkbox"','checked type="checkbox"');
            trimEle = trimEle.replace("type=checkbox","CHECKED type=checkbox");
            tagHtmlChecked[ind]=trimEle;
            }
            ind++;
        }
        else
        {
        tagHtmlUnchecked[inx]=trimEle;
        //document.getElementById(htmlEleId).innerHTML = "";
        inx++;
        }
    }
    
    for(var dx=11; dx<63; dx++)
    {
        htmlEleId = 'cont' + dx;
        document.getElementById(htmlEleId).innerHTML="";
    }
    
    var inx2 = 0;
    var d = 0;
    for(d=11; d<ind+11; d++)
    {
        htmlEleId = 'cont' + d;
        document.getElementById(htmlEleId).innerHTML=tagHtmlChecked[inx2];
        inx2++;
    }
    
    inx2=0;
    for(var d2=d; d2<63; d2++)
    {
        htmlEleId = 'cont' + d2;
        document.getElementById(htmlEleId).innerHTML=tagHtmlUnchecked[inx2];
        inx2++;
    }
}


function fnUncheckAll()
{
/*
if(fnCheckCell() == false)
{
alert("No icon to uncheck");
return false;
}
if(!confirm("Are you sure to uncheck all icons"))
return false;
*/
//alert("fnUncheckAll");
        var paneid='';
        for(var m=11; m<36; m++)
        {
            paneid = 'pane' + m;
            document.getElementById(paneid).innerHTML = "&nbsp;"; // FB 2767
            document.getElementById(paneid).innerHTML = "";
        }
        
        var contid=''
        var divCont ='';
        for(var n=11; n<63; n++)
        {
            contid = 'cont' + n;
            divCont = document.getElementById(contid).innerHTML;
            //divCont = fnTrim(document.getElementById(contid).innerHTML);
            divCont = divCont.replace('checked=""','');
            divCont = divCont.replace("CHECKED","");
            document.getElementById(contid).innerHTML = divCont;
        }


}

/*
function fnRefreshIconSet()
{
//alert('hi');
    var tagHtml = new Array(52);
    var htmlEleId = '';
    var trimEle ='';
    var ind = 0;
    
    for(var t=11; t<63; t++)
    {
        tagHtml[t]="";
    }
    for(var t=11; t<63; t++)
    {
        htmlEleId = 'cont' + t;
        trimEle=fnTrim(document.getElementById(htmlEleId).innerHTML);
        if(trimEle != "")
        {
        tagHtml[ind]=trimEle;
        document.getElementById(htmlEleId).innerHTML = "";
        ind++;
        }
    }
    
    for(var d=11; d<63; d++)
    {
        htmlEleId = 'cont' + d;
        document.getElementById(htmlEleId).innerHTML="";
    }
    
    for(var d=11; d<63; d++)
    {
        htmlEleId = 'cont' + d;
        document.getElementById(htmlEleId).innerHTML=tagHtml[d-11];
    }
    
}
*/

function ExtractNumber(value)
{
    var n = parseInt(value);
	
    return n == null || isNaN(n) ? 0 : n;
}

// this is simply a shortcut for the eyes and fingers
function $(id)
{
    return document.getElementById(id);
}


// This block restore the default name by using class name
function changeDefault()
{
var cont = document.getElementById(reNameIcon);
cont.innerHTML=cont.className;
document.getElementById('renameDiv').style.display = 'none';
globChange = 1;
//document.getElementById('<%=saveLink.ClientID %>').innerHTML = 'Click here to save changes';
//alert(cont.className);
}


// This block will display when double click the label
function changeName(x)
{

var obj = document.getElementById(x);

var pid=obj.parentNode.id.substring(0,4);
if(pid=="cont")
return false;


//document.getElementById('renameDiv').style.display = 'block';
var val = document.getElementById('renameDiv');
val.style.display = 'block';
val.style.zIndex = "10000";
val.style.position = 'absolute';
posX = mouseX - 100;
posY = mouseY + 25;
if(posX > (screen.width - 234))
posX = screen.width - 234;
val.style.left = posX+"px";
val.style.top = posY+"px";

            


var txtBox = document.getElementById('reIcon');
reNameIcon=x;
txtBox.value=obj.innerHTML;
txtBox.focus();
//var value = obj.innerHTML;
            
return false;
}

// This block used to update the icon name
function updtName()
{
//debugger;
// This block rename the label of the icon

var obj = document.getElementById(reNameIcon);

var pid=obj.parentNode.id.substring(0,4);
if(pid=="cont")
return false;


var value = obj.innerHTML;

var txt = document.getElementById('reIcon');

var reply = txt.value;



if(value==reply)
{
document.getElementById('renameDiv').style.display = 'none';
return false;
}


var ch,str="";
// This block check for empty
if(reply!=null & reply!="")
{
    for(var i=0; i<reply.length; i++)
    {
        ch=reply.substring(i,i+1);
        if(ch!=" ")
        {
        str+=reply.substring(i,i+1);
        }
        if(ch=="`" || ch=="~")
        {
        alert('Characters ` and ~ not allowed');
        return false;
        }
    }
}



// This block checks for duplicate - both current and defaults
var spanId, spanCont, dupCount=0;
var iconTotal = document.getElementById("iconCount").value;
for(var i=1; i<iconTotal; i++)
{
spanId = 'xpane'+i;
spanCont = document.getElementById(spanId);
if(spanCont != null)
{
    if(spanCont.innerHTML==reply)
    {
    alert("Icon name exist");
    return false;
    }
    if(spanCont.className==reply)
    {
    alert("System defaults not allowed");
    return false;
    }
}
}

if(str!="")
    obj.innerHTML=reply;
    
document.getElementById('renameDiv').style.display = 'none';
//document.getElementById('<%=saveLink.ClientID %>').innerHTML = 'Click here to save changes';
globChange = 1;



}

//This block retrive values from the pane to save
function prepareChanges()
{
/* FB 2687 Starts
if(globChange != "1")
{
 alert('No changes to save');
 return true;
}
FB 2687 Ends */
var lblID, lblContent="";
var curLabel="", gridPos="", totCont="";
var iconTotal = document.getElementById("iconCount").value;
for(var i=1; i<iconTotal; i++)
{
lblID = 'xpane'+i;
lblContent = document.getElementById(lblID);
if(lblContent)
{
divName=lblContent.parentNode.id.substring(0,4);
divID=lblContent.parentNode.id.substring(4,6);

    if(divName=="pane")
    {
    curLabel = curLabel + "," + lblContent.innerHTML;
    gridPos = gridPos + "," + divID;
    totCont=totCont+"~"+lblID+"`"+lblContent.innerHTML+"`"+divID;
    }
}
}
//alert(totCont);
// FB 2687 Starts
if(totCont == "")
{
    alert('Please select atleast one icon');
    return false;
}
// FB 2687 Ends
document.getElementById('<%=selectedValues.ClientID %>').value = totCont.substring(1,totCont.length);
globChange = 0;
return true;
}
   

//This block used for container image effects
function imgOver(o)
{

var x = document.getElementById(o); //FB 2767
if (x != null) { 
    var pid = x.parentNode.parentNode.id.substring(0, 4);
    if (pid == "pane")
        return false;

    x.style.opacity = 1;
    if (x.filters != null)
        x.filters.alpha.opacity = 100;
    }
}

//This block used for container image effects
function imgOut(o)
{

var x = document.getElementById(o);
if (x != null) { //FB 2767
    var pid = x.parentNode.parentNode.id.substring(0, 4);

    //var gp =x.parentNode.parentNode.parentNode.id;
    if (pid == "pane") {
        //alert(gp);

        //x.style.opacity=1;
        //x.filters.alpha.opacity=100;
        return false;
    }

    x.style.opacity = 0.5;
    if(x.filters != null)
        x.filters.alpha.opacity = 50;
}
}


function showBorder(option,id)
{
    if(mouseStatus == 1)
    {
        if(option == a)
        {
        var ele = document.getElementById(id);
        
        ele.style.border='solid 1px navy';
        }
        else if(option == b)
        {
        var ele = document.getElementById(id);
        
        ele.style.border='solid 1px navy';

        
        }
    
    
    }
}

function hideBorder(option,id)
{
        if(option == a)
        {
        var ele = document.getElementById(id);
        
        ele.style.border='solid 1px lightgray';
        }
        else if(option == b)
        {
        var ele = document.getElementById(id);
        
        ele.style.border='solid 1px white';

        
        }

}
    
    </script>

</head>
<body>
    <form id="form1" runat="server">
    <input type="hidden" id="iconCount" runat="server" />
    <table width="100%">
            <tr>
                <td align="left" width="6%" id="tdLobby" runat="server" colspan="3" >
                    <h3>
                                                <span id="Field1" runat="server">Manage My Lobby</span><!-- FB 2570 -->
                    </h3><br />
                    <asp:Label ID="errLabel" runat="server" CssClass="lblError"></asp:Label>
                </td>
            </tr>
            <!-- FB 2629 Instructions Removed -->
    <tr>
    <td width="25%" valign="top"><%--FB 2845--%>
    <table>
    <tr>
    <td nowrap="nowrap">
    <div><span class="subtitleblueblodtext">Icon List</span></div> <%--FB 2845--%>
    </td>
    <td nowrap="nowrap">
    <input type="button" style="width:135px" value="Sort Selected List" class="altMedium0BlueButtonFormat" onclick="fnGroupChecked()" />
    </td>
    <td nowrap="nowrap">
    <input type="button" style="width:135px" value="Uncheck All" class="altMedium0BlueButtonFormat" onclick="fnUncheckAll()" /> <%--FB 2845--%>
    </td>
    </tr>
    </table>
    <div onscroll="javascript:this.style.visibility = 'hidden';this.style.visibility = 'visible';" style="height:692px; overflow-y:scroll; overflow-x:hidden; border-style:solid; border-width:thin; border-color:Navy"><%-- FB 2767 --%>
    <table align="center">
            <tr>
                <td align="center" width="6%" id="td2a" runat="server" class="td2" onmouseover="showBorder(b, this.id);"
                    onmouseout="hideBorder(b, this.id);">
                    <div id="cont11" class="small" onmouseover='imgOver(this.childNodes[1].childNodes[0].id)'
                        onmouseout='imgOut(this.childNodes[1].childNodes[0].id)'>
                    </div>
                </td>
                <td align="center" width="6%" id="td3" runat="server" class="td2" onmouseover="showBorder(b, this.id);"
                    onmouseout="hideBorder(b, this.id);">
                    <div id="cont12" class="small" onmouseover='imgOver(this.childNodes[1].childNodes[0].id)'
                        onmouseout='imgOut(this.childNodes[1].childNodes[0].id)'>
                    </div>
                </td>
                <td align="center" width="6%" id="td4" runat="server" class="td2" onmouseover="showBorder(b, this.id);"
                    onmouseout="hideBorder(b, this.id);">
                    <div id="cont13" class="small" onmouseover='imgOver(this.childNodes[1].childNodes[0].id)'
                        onmouseout='imgOut(this.childNodes[1].childNodes[0].id)'>
                    </div>
                </td>
            </tr>
            <tr>
                <td align="center" width="6%" id="td5" runat="server" class="td2" onmouseover="showBorder(b, this.id);"
                    onmouseout="hideBorder(b, this.id);">
                    <div id="cont14" class="small" onmouseover='imgOver(this.childNodes[1].childNodes[0].id)'
                        onmouseout='imgOut(this.childNodes[1].childNodes[0].id)'>
                    </div>
                </td>
                <td align="center" width="6%" id="td6" runat="server" class="td2" onmouseover="showBorder(b, this.id);"
                    onmouseout="hideBorder(b, this.id);">
                    <div id="cont15" class="small" onmouseover='imgOver(this.childNodes[1].childNodes[0].id)'
                        onmouseout='imgOut(this.childNodes[1].childNodes[0].id)'>
                    </div>
                </td>
                <td align="center" width="6%" id="td7" runat="server" class="td2" onmouseover="showBorder(b, this.id);"
                    onmouseout="hideBorder(b, this.id);">
                    <div id="cont16" class="small" onmouseover='imgOver(this.childNodes[1].childNodes[0].id)'
                        onmouseout='imgOut(this.childNodes[1].childNodes[0].id)'>
                    </div>
                </td>
            </tr>
            <tr>
                <td align="center" width="6%" id="td8" runat="server" class="td2" onmouseover="showBorder(b, this.id);"
                    onmouseout="hideBorder(b, this.id);">
                    <div id="cont17" class="small" onmouseover='imgOver(this.childNodes[1].childNodes[0].id)'
                        onmouseout='imgOut(this.childNodes[1].childNodes[0].id)'>
                    </div>
                </td>
                <td align="center" width="6%" id="td9" runat="server" class="td2" onmouseover="showBorder(b, this.id);"
                    onmouseout="hideBorder(b, this.id);">
                    <div id="cont18" class="small" onmouseover='imgOver(this.childNodes[1].childNodes[0].id)'
                        onmouseout='imgOut(this.childNodes[1].childNodes[0].id)'>
                    </div>
                </td>
                <td align="center" width="6%" id="td10" runat="server" class="td2" onmouseover="showBorder(b, this.id);"
                    onmouseout="hideBorder(b, this.id);">
                    <div id="cont19" class="small" onmouseover='imgOver(this.childNodes[1].childNodes[0].id)'
                        onmouseout='imgOut(this.childNodes[1].childNodes[0].id)'>
                    </div>
                </td>
            </tr>
            <tr>
                <td align="center" width="6%" id="td11" runat="server" class="td2" onmouseover="showBorder(b, this.id);"
                    onmouseout="hideBorder(b, this.id);">
                    <div id="cont20" class="small" onmouseover='imgOver(this.childNodes[1].childNodes[0].id)'
                        onmouseout='imgOut(this.childNodes[1].childNodes[0].id)'>
                    </div>
                </td>
                <td align="center" width="6%" id="td12" runat="server" class="td2" onmouseover="showBorder(b, this.id);"
                    onmouseout="hideBorder(b, this.id);">
                    <div id="cont21" class="small" onmouseover='imgOver(this.childNodes[1].childNodes[0].id)'
                        onmouseout='imgOut(this.childNodes[1].childNodes[0].id)'>
                    </div>
                </td>
                <td align="center" width="6%" id="td13" runat="server" class="td2" onmouseover="showBorder(b, this.id);"
                    onmouseout="hideBorder(b, this.id);">
                    <div id="cont22" class="small" onmouseover='imgOver(this.childNodes[1].childNodes[0].id)'
                        onmouseout='imgOut(this.childNodes[1].childNodes[0].id)'>
                    </div>
                </td>
            </tr>
            <tr>
                <td align="center" width="6%" id="td14" runat="server" class="td2" onmouseover="showBorder(b, this.id);"
                    onmouseout="hideBorder(b, this.id);">
                    <div id="cont23" class="small" onmouseover='imgOver(this.childNodes[1].childNodes[0].id)'
                        onmouseout='imgOut(this.childNodes[1].childNodes[0].id)'>
                    </div>
                </td>
                <td align="center" width="6%" id="td15" runat="server" class="td2" onmouseover="showBorder(b, this.id);"
                    onmouseout="hideBorder(b, this.id);">
                    <div id="cont24" class="small" onmouseover='imgOver(this.childNodes[1].childNodes[0].id)'
                        onmouseout='imgOut(this.childNodes[1].childNodes[0].id)'>
                    </div>
                </td>
                <td align="center" width="6%" id="td16" runat="server" class="td2" onmouseover="showBorder(b, this.id);"
                    onmouseout="hideBorder(b, this.id);">
                    <div id="cont25" class="small" onmouseover='imgOver(this.childNodes[1].childNodes[0].id)'
                        onmouseout='imgOut(this.childNodes[1].childNodes[0].id)'>
                    </div>
                </td>
            </tr>
            <tr>
                <td align="center" width="6%" id="td17" runat="server" class="td2" onmouseover="showBorder(b, this.id);"
                    onmouseout="hideBorder(b, this.id);">
                    <div id="cont26" class="small" onmouseover='imgOver(this.childNodes[1].childNodes[0].id)'
                        onmouseout='imgOut(this.childNodes[1].childNodes[0].id)'>
                    </div>
                </td>
                <td align="center" width="6%" id="td18" runat="server" class="td2" onmouseover="showBorder(b, this.id);"
                    onmouseout="hideBorder(b, this.id);">
                    <div id="cont27" class="small" onmouseover='imgOver(this.childNodes[1].childNodes[0].id)'
                        onmouseout='imgOut(this.childNodes[1].childNodes[0].id)'>
                    </div>
                </td>
                <td align="center" width="6%" id="td19" runat="server" class="td2" onmouseover="showBorder(b, this.id);"
                    onmouseout="hideBorder(b, this.id);">
                    <div id="cont28" class="small" onmouseover='imgOver(this.childNodes[1].childNodes[0].id)'
                        onmouseout='imgOut(this.childNodes[1].childNodes[0].id)'>
                    </div>
                </td>
            </tr>
            <tr>
                <td align="center" width="6%" id="td20" runat="server" class="td2" onmouseover="showBorder(b, this.id);"
                    onmouseout="hideBorder(b, this.id);">
                    <div id="cont29" class="small" onmouseover='imgOver(this.childNodes[1].childNodes[0].id)'
                        onmouseout='imgOut(this.childNodes[1].childNodes[0].id)'>
                    </div>
                </td>
                <td align="center" width="6%" id="td21" runat="server" class="td2" onmouseover="showBorder(b, this.id);"
                    onmouseout="hideBorder(b, this.id);">
                    <div id="cont30" class="small" onmouseover='imgOver(this.childNodes[1].childNodes[0].id)'
                        onmouseout='imgOut(this.childNodes[1].childNodes[0].id)'>
                    </div>
                </td>
                <td align="center" width="6%" id="td22" runat="server" class="td2" onmouseover="showBorder(b, this.id);"
                    onmouseout="hideBorder(b, this.id);">
                    <div id="cont31" class="small" onmouseover='imgOver(this.childNodes[1].childNodes[0].id)'
                        onmouseout='imgOut(this.childNodes[1].childNodes[0].id)'>
                    </div>
                </td>
            </tr>
            <tr>
                <td align="center" width="6%" id="td23" runat="server" class="td2" onmouseover="showBorder(b, this.id);"
                    onmouseout="hideBorder(b, this.id);">
                    <div id="cont32" class="small" onmouseover='imgOver(this.childNodes[1].childNodes[0].id)'
                        onmouseout='imgOut(this.childNodes[1].childNodes[0].id)'>
                    </div>
                </td>
                <td align="center" width="6%" id="td23a" class="td2" onmouseover="showBorder(b, this.id);"
                    onmouseout="hideBorder(b, this.id);">
                    <div id="cont33" class="small" onmouseover='imgOver(this.childNodes[1].childNodes[0].id)'
                        onmouseout='imgOut(this.childNodes[1].childNodes[0].id)'>
                    </div>
                </td>
                <td align="center" width="6%" id="td24" runat="server" class="td2" onmouseover="showBorder(b, this.id);"
                    onmouseout="hideBorder(b, this.id);">
                    <div id="cont34" class="small" onmouseover='imgOver(this.childNodes[1].childNodes[0].id)'
                        onmouseout='imgOut(this.childNodes[1].childNodes[0].id)'>
                    </div>
                </td>
            </tr>
            <tr>
                <td align="center" width="6%" id="td25" runat="server" class="td2" onmouseover="showBorder(b, this.id);"
                    onmouseout="hideBorder(b, this.id);">
                    <div id="cont35" class="small" onmouseover='imgOver(this.childNodes[1].childNodes[0].id)'
                        onmouseout='imgOut(this.childNodes[1].childNodes[0].id)'>
                    </div>
                </td>
                <td align="center" width="6%" id="td26" runat="server" class="td2" onmouseover="showBorder(b, this.id);"
                    onmouseout="hideBorder(b, this.id);">
                    <div id="cont36" class="small" onmouseover='imgOver(this.childNodes[1].childNodes[0].id)'
                        onmouseout='imgOut(this.childNodes[1].childNodes[0].id)'>
                    </div>
                </td>
                <td align="center" width="6%" id="td27" runat="server" class="td2" onmouseover="showBorder(b, this.id);"
                    onmouseout="hideBorder(b, this.id);">
                    <div id="cont37" class="small" onmouseover='imgOver(this.childNodes[1].childNodes[0].id)'
                        onmouseout='imgOut(this.childNodes[1].childNodes[0].id)'>
                    </div>
                </td>
            </tr>
            <tr>
                <td align="center" width="6%" id="td28" runat="server" class="td2" onmouseover="showBorder(b, this.id);"
                    onmouseout="hideBorder(b, this.id);">
                    <div id="cont38" class="small" onmouseover='imgOver(this.childNodes[1].childNodes[0].id)'
                        onmouseout='imgOut(this.childNodes[1].childNodes[0].id)'>
                    </div>
                </td>
                <td align="center" width="6%" id="td29" runat="server" class="td2" onmouseover="showBorder(b, this.id);"
                    onmouseout="hideBorder(b, this.id);">
                    <div id="cont39" class="small" onmouseover='imgOver(this.childNodes[1].childNodes[0].id)'
                        onmouseout='imgOut(this.childNodes[1].childNodes[0].id)'>
                    </div>
                </td>
                <td align="center" width="6%" id="td30" runat="server" class="td2" onmouseover="showBorder(b, this.id);"
                    onmouseout="hideBorder(b, this.id);">
                    <div id="cont40" class="small" onmouseover='imgOver(this.childNodes[1].childNodes[0].id)'
                        onmouseout='imgOut(this.childNodes[1].childNodes[0].id)'>
                    </div>
                </td>
            </tr>
            <tr>
                <td align="center" width="6%" id="td31" runat="server" class="td2" onmouseover="showBorder(b, this.id);"
                    onmouseout="hideBorder(b, this.id);">
                    <div id="cont41" class="small" onmouseover='imgOver(this.childNodes[1].childNodes[0].id)'
                        onmouseout='imgOut(this.childNodes[1].childNodes[0].id)'>
                    </div>
                </td>
                <td align="center" width="6%" id="td32" runat="server" class="td2" onmouseover="showBorder(b, this.id);"
                    onmouseout="hideBorder(b, this.id);">
                    <div id="cont42" class="small" onmouseover='imgOver(this.childNodes[1].childNodes[0].id)'
                        onmouseout='imgOut(this.childNodes[1].childNodes[0].id)'>
                    </div>
                </td>
                <td align="center" width="6%" id="td33" runat="server" class="td2" onmouseover="showBorder(b, this.id);"
                    onmouseout="hideBorder(b, this.id);">
                    <div id="cont43" class="small" onmouseover='imgOver(this.childNodes[1].childNodes[0].id)'
                        onmouseout='imgOut(this.childNodes[1].childNodes[0].id)'>
                    </div>
                </td>
            </tr>
            <tr>
                <td align="center" width="6%" id="td34" runat="server" class="td2" onmouseover="showBorder(b, this.id);"
                    onmouseout="hideBorder(b, this.id);">
                    <div id="cont44" class="small" onmouseover='imgOver(this.childNodes[1].childNodes[0].id)'
                        onmouseout='imgOut(this.childNodes[1].childNodes[0].id)'>
                    </div>
                </td>
                <td align="center" width="6%" id="td35" runat="server" class="td2" onmouseover="showBorder(b, this.id);"
                    onmouseout="hideBorder(b, this.id);">
                    <div id="cont45" class="small" onmouseover='imgOver(this.childNodes[1].childNodes[0].id)'
                        onmouseout='imgOut(this.childNodes[1].childNodes[0].id)'>
                    </div>
                </td>
                <td align="center" width="6%" id="td35a" class="td2" onmouseover="showBorder(b, this.id);"
                    onmouseout="hideBorder(b, this.id);">
                    <div id="cont46" class="small" onmouseover='imgOver(this.childNodes[1].childNodes[0].id)'
                        onmouseout='imgOut(this.childNodes[1].childNodes[0].id)'>
                    </div>
                </td>
            </tr>
            <tr>
                <td align="center" width="6%" id="td36" runat="server" class="td2" onmouseover="showBorder(b, this.id);"
                    onmouseout="hideBorder(b, this.id);">
                    <div id="cont47" class="small" onmouseover='imgOver(this.childNodes[1].childNodes[0].id)'
                        onmouseout='imgOut(this.childNodes[1].childNodes[0].id)'>
                    </div>
                </td>
                <td align="center" width="6%" id="td37" runat="server" class="td2" onmouseover="showBorder(b, this.id);"
                    onmouseout="hideBorder(b, this.id);">
                    <div id="cont48" class="small" onmouseover='imgOver(this.childNodes[1].childNodes[0].id)'
                        onmouseout='imgOut(this.childNodes[1].childNodes[0].id)'>
                    </div>
                </td>
                <td align="center" width="6%" id="td38" runat="server" class="td2" onmouseover="showBorder(b, this.id);"
                    onmouseout="hideBorder(b, this.id);">
                    <div id="cont49" class="small" onmouseover='imgOver(this.childNodes[1].childNodes[0].id)'
                        onmouseout='imgOut(this.childNodes[1].childNodes[0].id)'>
                    </div>
                </td>
            </tr>
            <tr>
                <td align="center" width="6%" id="td2" runat="server" class="td2" onmouseover="showBorder(b, this.id);"
                    onmouseout="hideBorder(b, this.id);">
                    <div id="cont50" class="small" onmouseover='imgOver(this.childNodes[1].childNodes[0].id)'
                        onmouseout='imgOut(this.childNodes[1].childNodes[0].id)'>
                    </div>
                </td>
                <td align="center" width="6%" id="td46" runat="server" class="td2" onmouseover="showBorder(b, this.id);"
                    onmouseout="hideBorder(b, this.id);">
                    <div id="cont51" class="small" onmouseover='imgOver(this.childNodes[1].childNodes[0].id)'
                        onmouseout='imgOut(this.childNodes[1].childNodes[0].id)'>
                    </div>
                </td>
                <td align="center" width="6%" id="td47" runat="server" class="td2" onmouseover="showBorder(b, this.id);"
                    onmouseout="hideBorder(b, this.id);">
                    <div id="cont52" class="small" onmouseover='imgOver(this.childNodes[1].childNodes[0].id)'
                        onmouseout='imgOut(this.childNodes[1].childNodes[0].id)'>
                    </div>
                </td>
            </tr>
            <tr>
                <td align="center" width="6%" id="td48" runat="server" class="td2" onmouseover="showBorder(b, this.id);"
                    onmouseout="hideBorder(b, this.id);">
                    <div id="cont53" class="small" onmouseover='imgOver(this.childNodes[1].childNodes[0].id)'
                        onmouseout='imgOut(this.childNodes[1].childNodes[0].id)'>
                    </div>
                </td>
                <td align="center" width="6%" id="td49" runat="server" class="td2" onmouseover="showBorder(b, this.id);"
                    onmouseout="hideBorder(b, this.id);">
                    <div id="cont54" class="small" onmouseover='imgOver(this.childNodes[1].childNodes[0].id)'
                        onmouseout='imgOut(this.childNodes[1].childNodes[0].id)'>
                    </div>
                </td>
                <td align="center" width="6%" id="td50" runat="server" class="td2" onmouseover="showBorder(b, this.id);"
                    onmouseout="hideBorder(b, this.id);">
                    <div id="cont55" class="small" onmouseover='imgOver(this.childNodes[1].childNodes[0].id)'
                        onmouseout='imgOut(this.childNodes[1].childNodes[0].id)'>
                    </div>
                </td>
            </tr>
            <tr>
                <td align="center" width="6%" id="td51" runat="server" class="td2" onmouseover="showBorder(b, this.id);"
                    onmouseout="hideBorder(b, this.id);">
                    <div id="cont56" class="small" onmouseover='imgOver(this.childNodes[1].childNodes[0].id)'
                        onmouseout='imgOut(this.childNodes[1].childNodes[0].id)'>
                    </div>
                </td>
                <td align="center" width="6%" id="td52" runat="server" class="td2" onmouseover="showBorder(b, this.id);"
                    onmouseout="hideBorder(b, this.id);">
                    <div id="cont57" class="small" onmouseover='imgOver(this.childNodes[1].childNodes[0].id)'
                        onmouseout='imgOut(this.childNodes[1].childNodes[0].id)'>
                    </div>
                </td>
                <td align="center" width="6%" id="td53" class="td2" onmouseover="showBorder(b, this.id);"
                    onmouseout="hideBorder(b, this.id);">
                    <div id="cont58" class="small" onmouseover='imgOver(this.childNodes[1].childNodes[0].id)'
                        onmouseout='imgOut(this.childNodes[1].childNodes[0].id)'>
                    </div>
                </td>
            </tr>
            <tr>
                <td align="center" width="6%" id="td54" runat="server" class="td2" onmouseover="showBorder(b, this.id);"
                    onmouseout="hideBorder(b, this.id);">
                    <div id="cont59" class="small" onmouseover='imgOver(this.childNodes[1].childNodes[0].id)'
                        onmouseout='imgOut(this.childNodes[1].childNodes[0].id)'>
                    </div>
                </td>
                <td align="center" width="6%" id="td55" runat="server" class="td2" onmouseover="showBorder(b, this.id);"
                    onmouseout="hideBorder(b, this.id);">
                    <div id="cont60" class="small" onmouseover='imgOver(this.childNodes[1].childNodes[0].id)'
                        onmouseout='imgOut(this.childNodes[1].childNodes[0].id)'>
                    </div>
                </td>
                <td align="center" width="6%" id="td56" runat="server" class="td2" onmouseover="showBorder(b, this.id);"
                    onmouseout="hideBorder(b, this.id);">
                    <div id="cont61" class="small" onmouseover='imgOver(this.childNodes[1].childNodes[0].id)'
                        onmouseout='imgOut(this.childNodes[1].childNodes[0].id)'>
                    </div>
                </td>
            </tr>
            <tr>
                <td align="center" width="6%" id="td57" runat="server" class="td2" onmouseover="showBorder(b, this.id);"
                    onmouseout="hideBorder(b, this.id);">
                    <div id="cont62" class="small" onmouseover='imgOver(this.childNodes[1].childNodes[0].id)'
                        onmouseout='imgOut(this.childNodes[1].childNodes[0].id)'>
                    </div>
                </td>
            </tr>
        </table>  
    </div>
    
    </td>
    <td width="3%"></td>
    <td id="tdWidthId" width="65%" valign="top">
    <div><span class="subtitleblueblodtext">Lobby Preview</span></div>  
    <table id="main" cellpadding="1" cellspacing="1" width="100%" border="0" >
            <tr>
                <td align="center" width="11%" id="tdCalender" runat="server" class="tdx">
                    <div id="pane11" class="big" onmouseover="showBorder(a, this.parentNode.id);" onmouseout="hideBorder(a, this.parentNode.id);">
                    </div>
                </td>
                <td align="center" width="11%" id="gd" runat="server" class="tdx">
                    <div id="pane12" class="big" onmouseover="showBorder(a, this.parentNode.id);" onmouseout="hideBorder(a, this.parentNode.id);">
                    </div>
                </td>
                <td align="center" width="11%" id="tdNewConf" runat="server" class="tdx">
                    <div id="pane13" class="big" onmouseover="showBorder(a, this.parentNode.id);" onmouseout="hideBorder(a, this.parentNode.id);">
                    </div>
                </td>
                <td align="center" width="11%" id="tdMypreferences" runat="server" class="tdx">
                    <div id="pane14" class="big" onmouseover="showBorder(a, this.parentNode.id);" onmouseout="hideBorder(a, this.parentNode.id);">
                     </div>
                </td>
                <td align="center" width="11%" id="tdReports" runat="server" class="tdx">
                    <div id="pane15" class="big" onmouseover="showBorder(a, this.parentNode.id);" onmouseout="hideBorder(a, this.parentNode.id);">
                    </div>
                </td>
            </tr>
            <tr>
                <td align="center" width="11%" id="td1" runat="server" class="tdx">
                    <div id="pane16" class="big" onmouseover="showBorder(a, this.parentNode.id);" onmouseout="hideBorder(a, this.parentNode.id);">
                    </div>
                </td>
                <td align="center" width="11%" id="td1a" class="tdx">
                    <div id="pane17" class="big" onmouseover="showBorder(a, this.parentNode.id);" onmouseout="hideBorder(a, this.parentNode.id);">
                   </div>
                </td>
                <td align="center" width="11%" id="td1b" class="tdx">
                    <div id="pane18" class="big" onmouseover="showBorder(a, this.parentNode.id);" onmouseout="hideBorder(a, this.parentNode.id);">
                   </div>
                </td>
                <td align="center" width="11%" id="tdOrgoptions" runat="server" class="tdx">
                    <div id="pane19" class="big" onmouseover="showBorder(a, this.parentNode.id);" onmouseout="hideBorder(a, this.parentNode.id);">
                    </div>
                </td>
                <td align="center" width="11%" id="tdUserEmailQueue" runat="server" class="tdx">
                    <div id="pane20" class="big" onmouseover="showBorder(a, this.parentNode.id);" onmouseout="hideBorder(a, this.parentNode.id);">
                   </div>
                </td>
            </tr>
            <tr>
                <td align="center" width="11%" id="tdOrgSettings" runat="server" class="tdx">
                    <div id="pane21" class="big" onmouseover="showBorder(a, this.parentNode.id);" onmouseout="hideBorder(a, this.parentNode.id);">
                    </div>
                </td>
                <td align="center" width="11%" id="tdSysDiagnostic" runat="server" class="tdx">
                    <div id="pane22" class="big" onmouseover="showBorder(a, this.parentNode.id);" onmouseout="hideBorder(a, this.parentNode.id);">
                    </div>
                </td>
                <td align="center" width="11%" id="tdSiteSettings" runat="server" class="tdx">
                    <div id="pane23" class="big" onmouseover="showBorder(a, this.parentNode.id);" onmouseout="hideBorder(a, this.parentNode.id);">
                    </div>
                </td>
                <td align="center" width="11%" id="tdOrgUI" runat="server" class="tdx">
                    <div id="pane24" class="big" onmouseover="showBorder(a, this.parentNode.id);" onmouseout="hideBorder(a, this.parentNode.id);">
                    </div>
                </td>
                <td align="center" width="11%" id="tdSysAudit" runat="server" class="tdx">
                    <div id="pane25" class="big" onmouseover="showBorder(a, this.parentNode.id);" onmouseout="hideBorder(a, this.parentNode.id);">
                    </div>
                </td>
            </tr>
            <tr>
                <td align="center" width="11%" id="td1c" class="tdx">
                    <div id="pane26" class="big" onmouseover="showBorder(a, this.parentNode.id);" onmouseout="hideBorder(a, this.parentNode.id);">
                    </div>
                </td>
                <td align="center" width="11%" id="tdRooms" runat="server" class="tdx">
                    <div id="pane27" class="big" onmouseover="showBorder(a, this.parentNode.id);" onmouseout="hideBorder(a, this.parentNode.id);">
                    </div>
                </td>
                <td align="center" width="11%" id="tdGroups" runat="server" class="tdx">
                    <div id="pane28" class="big" onmouseover="showBorder(a, this.parentNode.id);" onmouseout="hideBorder(a, this.parentNode.id);">
                    </div>
                </td>
                <td align="center" width="11%" id="tdTemp" runat="server" class="tdx">
                    <div id="pane29" class="big" onmouseover="showBorder(a, this.parentNode.id);" onmouseout="hideBorder(a, this.parentNode.id);">
                    </div>
                </td>
                <td align="center" width="11%" id="td1d" class="tdx">
                    <div id="pane30" class="big" onmouseover="showBorder(a, this.parentNode.id);" onmouseout="hideBorder(a, this.parentNode.id);">
                    </div>
                </td>
            </tr>
            <tr>
                <td align="center" width="11%" id="tdInv" runat="server" class="tdx">
                    <div id="pane31" class="big" onmouseover="showBorder(a, this.parentNode.id);" onmouseout="hideBorder(a, this.parentNode.id);">
                    </div>
                </td>
                <td align="center" width="11%" id="tdHsKep" runat="server" class="tdx">
                    <div id="pane32" class="big" onmouseover="showBorder(a, this.parentNode.id);" onmouseout="hideBorder(a, this.parentNode.id);">
                    </div>
                </td>
                <td align="center" width="11%" id="tdCtr" runat="server" class="tdx">
                    <div id="pane33" class="big" onmouseover="showBorder(a, this.parentNode.id);" onmouseout="hideBorder(a, this.parentNode.id);">
                    </div>
                </td>
                <td align="center" width="11%" id="td1e" class="tdx">
                    <div id="pane34" class="big" onmouseover="showBorder(a, this.parentNode.id);" onmouseout="hideBorder(a, this.parentNode.id);">
                    </div>
                </td>
                <td align="center" width="11%" id="tdtiers" runat="server" class="tdx">
                    <div id="pane35" class="big" onmouseover="showBorder(a, this.parentNode.id);" onmouseout="hideBorder(a, this.parentNode.id);">
                    </div>
                </td>
            </tr>
        </table>
        <br />
        
    </td></tr>
    <tr>
    <td colspan="3">
    <table width="100%"><tr>
    <td align="right" valign="bottom">
    <input id="btnCancel" type="button" runat="server" value="Cancel" class="altLongBlueButtonFormat" onclick="javascript:fnNavigateLate()" />
    </td>
    <td style="width:100px"></td>
    <td align="left" valign="bottom">
        <asp:ScriptManager id="saveUpdt" runat="server" />
        <asp:UpdatePanel ID="updtPart" runat="server">
        <contenttemplate><br />
            <input type="hidden" id="cancelPage" runat="server" />            
            <asp:Button ID="saveLink" runat="server" Text="Submit" onclick="saveLink_Click" OnClientClick="javascript:return prepareChanges()" Width="150pt" /><%-- FB 2796--%>
        </contenttemplate>
        </asp:UpdatePanel>
    </td>
    </tr></table>
    </td>
    </tr></table>
    
    
    <div id="renameDiv" style="display: none; background-color: lightgray; border: 3px;
        border-color: gray; border-style: solid">
        <table id="xtable">
            <tr>
                <td align="left" colspan="3">
                    <b>Change Icon Name: </b>
                </td>
            </tr>
            <tr>
                <td colspan="3">
                    <input id="reIcon" type="text" style="outline: none; width:180px" onkeydown="return (event.keyCode!=13);"
                        maxlength="35" size="25" />
                    
                </td>
            </tr>
            <tr id="xtr">
                <td><%--FB 2768 Start--%>
                    <input id="Button1" type="button" value="Default" title="Click here to restore default name" class="altMedium0BlueButtonFormat" style="width:60px"
                        onclick="changeDefault();" />
                </td>
                <td>
                    <input id="Button2" type="button" value="Update" onclick="updtName();" class="altMedium0BlueButtonFormat" style="width:60px" />
                </td>
                <td id="xtd">
                    <input id="Button3" type="button" value="Cancel" onclick="document.getElementById('renameDiv').style.display = 'none';" class="altMedium0BlueButtonFormat" style="width:60px" />
                </td><%--FB 2768 End--%>
            </tr>
        </table>
    </div>
    <asp:HiddenField ID="selectedValues" runat="server" />
    </form>
</body>
</html>

<script language="javascript" type="text/javascript">
function openFeedback()
	{
		window.open ('feedback.aspx?wintype=pop', 'Feedback', 'width=450,height=410,resizable=no,scrollbars=no,status=no,left=' + (screen.availWidth-478) + ',top=198'); //Login Management
	}
function fnNavigateLate()
{
window.location = document.getElementById("cancelPage").value;
}

function fnSetWidth()
{
if(navigator.userAgent.indexOf("MSIE") != -1)
{
var tot = screen.width * (96/100);
var wid = (65/100) * tot;
//alert(wid);
document.getElementById("tdWidthId").width = wid;
}
}
fnSetWidth();
document.getElementById("form1").reset(); // FB 2687
</script>

<!-- #INCLUDE FILE="inc/mainbottomNET.aspx" -->
