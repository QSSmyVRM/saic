<%--ZD 100147 start--%>
<%--/* Copyright (C) 2013 myVRM - All Rights Reserved	
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.	
*
* You should have received a copy of the myVRM license with	
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/--%><%--ZD 100147 End--%>
<%@ Page Language="C#" Inherits="ns_Reports.MCUResourceReport" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> <!-- FB 2050 -->
<meta http-equiv="X-UA-Compatible" content="IE=8" /> <!-- FB 2050 -->
<!-- #INCLUDE FILE="inc/maintopNET.aspx" --> 
<script type="text/javascript">
  var servertoday = new Date();
</script>
<script type="text/javascript" src="inc/functions.js"></script>
<%--FB 1861--%>
  <%--<script type="text/javascript" src="script/cal.js"></script>--%>
<script type="text/javascript" src="script/cal-flat.js"></script>
<script type="text/javascript" src="lang/calendar-en.js"></script>
<script type="text/javascript" src="script/calendar-setup.js"></script>
<script type="text/javascript" src="script/calendar-flat-setup.js"></script>
<link rel="stylesheet" type="text/css" media="all" href="css/calendar-blue.css" /> <%--FB 1861--%> <%--FB 1982--%>

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title>MCU Resource Availability Report</title>
 
<script language="javascript">
function DataLoading()
{
    document.getElementById("dataLoadingDIV").innerHTML="<b><font color='#FF00FF' size='2'>Data loading ...</font></b>&nbsp;&nbsp;&nbsp;&nbsp;<img border='0' src='image/wait1.gif' >"; // FB 2742
}

function viewconf(cid)
{
	url = "ManageConference.aspx?confid=" + cid + "&t=hf";
	confdetail = window.open(url, "viewconference", "width=1,height=1,resizable=yes,scrollbars=yes,status=no");
	confdetail.focus();
}
</script>   
</head>
<body>
    <form id="frmReports" runat="server" method="post">
        <center><table border="0" width="98%" cellpadding="2" cellspacing="2">
            <tr>
                <td align="center" colspan="3" style="height: 23px">
                    <h3>MCU Resource Allocation Report</h3><br />
                     <%--Window Dressing--%>
                     <asp:Label ID="errLabel" CssClass="lblError" runat="server" Font-Size="Small" ForeColor="Red" Visible="False" Font-Bold="True" Font-Names="Verdana"></asp:Label></td>
            </tr>
            <tr>
                <td align="left">
                    <asp:Label ID="Label2" runat="server" CssClass="subtitleblueblodtext">Enter a Date Range:</asp:Label>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <table border="0" style="font-weight: bold; font-size: small; color: black; font-family: Verdana"
                        width="90%" cellpadding="2" cellspacing="5">
                        <tr>
                             <%--Window Dressing--%>
                            <td align="right"  class="blackblodtext">
                                Date From</td>
                            <td align="left">
                                <asp:TextBox ID="txtDateFrom" runat="server" CssClass="altText" EnableViewState="true" ></asp:TextBox>
                                <%-- Code added by Offshore for FB Issue 1073 -- Start
                               <img id="cal_triggerFrom" height="20px" onclick="javascript:showCalendar('<%=txtDateFrom.ClientID %>', 'cal_triggerFrom', 1, '%m/%d/%Y');" src="image/calendar.gif" width="20px" />--%>
                                <img id="cal_triggerFrom" height="20px" style="vertical-align:middle" onclick="javascript:showCalendar('<%=txtDateFrom.ClientID %>', 'cal_triggerFrom', 1, '<%=format%>');" src="image/calendar.gif" width="20px" /> <%-- Edited for FF--%>
                                <%--Code added by Offshore for FB Issue 1073 -- End --%>
                            </td>
                            <%--Window Dressing--%>
                            <td align="right" class="blackblodtext">
                                Date To</td>
                            <td align="left">
                                <asp:TextBox ID="txtDateTo" runat="server" CssClass="altText" EnableViewState="true"></asp:TextBox>
                                <%--Code added by Offshore for FB Issue 1073 -- Start
                                <img id="cal_triggerTo" height="20px" onclick="javascript:showCalendar('<%=txtDateTo.ClientID %>', 'cal_triggerTo', 1, '%m/%d/%Y');" src="image/calendar.gif" width="20px" /> --%>
                                <img id="cal_triggerTo" height="20px" style="vertical-align:middle" onclick="javascript:showCalendar('<%=txtDateTo.ClientID %>', 'cal_triggerTo', 1, '<%=format%>');" src="image/calendar.gif" width="20px" />  <%-- Edited for FF--%>
                                <%--Code added by Offshore for FB Issue 1073 -- End --%>
                            </td>
                            <td>
                                <asp:Button runat="server" ID="btnGenerateReport" OnClick="SearchConference" OnClientClick="javascript:DataLoading()" Text="Search Conferences" CssClass="altLongBlueButtonFormat" />
                            </td>
                        </tr>
                        <tr>
                            <td></td>
                            <td>
                                                                <%--Code changed by Offshore for FB Issue 1073 -- Start
                                 <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="txtDateFrom"
                                    ErrorMessage="Invalid Format: mm/dd/yyyy" ValidationExpression="\d{2}/\d{2}/\d{4}"></asp:RegularExpressionValidator>--%>
                                 <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="txtDateFrom"
                                    ErrorMessage="Invalid Date Format <%=format%>" ValidationExpression="(0[1-9]|1[012])[- /.](0[1-9]|[12][0-9]|3[01])[- /.](19|20)\d\d|(0[1-9]|[12][0-9]|3[01])[- /.](0[1-9]|1[012])[- /.](19|20)\d\d"></asp:RegularExpressionValidator>
                                <%--Code changed by Offshore for FB Issue 1073 -- eND--%>
                            </td>
                            <td></td>
                            <td>
                             <%--Code changed by Offshore for FB Issue 1073 -- Start
                                <asp:RegularExpressionValidator ID="reqDateTo" runat="server" ControlToValidate="txtDateTo"
                                    ErrorMessage="Invalid Date Format <%=format%>" ValidationExpression="\d{2}/\d{2}/\d{4}"></asp:RegularExpressionValidator>--%>
                                <asp:RegularExpressionValidator ID="reqDateTo" runat="server" ControlToValidate="txtDateTo"
                                    ErrorMessage="Invalid Date Format <%=format%>" ValidationExpression="(0[1-9]|1[012])[- /.](0[1-9]|[12][0-9]|3[01])[- /.](19|20)\d\d|(0[1-9]|[12][0-9]|3[01])[- /.](0[1-9]|1[012])[- /.](19|20)\d\d"></asp:RegularExpressionValidator>
                                <%--Code changed by Offshore for FB Issue 1073 -- End--%>
                            </td>
                            </td>
                            <td align="right">
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <div id="dataLoadingDIV" runat="server"></div>
                </td>
            </tr>
            <tr>
                <td align="left">
                    <asp:Label ID="Label1" runat="server" CssClass="subtitleblueblodtext"><%if (Application["Client"].ToString().ToUpper() == "MOJ"){%>Hearing List<%}else{ %>Conference List<%} %></asp:Label><%--Edited  For FB 1428--%>
                </td>
            </tr>
            <tr>
                <td colspan="2" align="center">
                <asp:DataGrid ID="dgConferences" runat="server" AutoGenerateColumns="False" Font-Names="Verdana" Font-Size="Small" Width="95%"
                    BorderStyle="None"  BorderWidth="1"> <%--Edited for FF--%><%--2908 Start--%>
                    <%--Window Dressing start--%>
                    <AlternatingItemStyle CssClass="tableBody"/>
                    <ItemStyle CssClass="tableBody" BorderWidth="1" />
                     <%--Window Dressing end--%>
                    <HeaderStyle CssClass="tableHeader" Height="25px" BorderWidth="1" />
                    <Columns>
                        <asp:BoundColumn DataField="confID" Visible="False"></asp:BoundColumn>
                        <asp:BoundColumn DataField="duration" Visible="False"></asp:BoundColumn>
                        <asp:TemplateColumn>
                            <HeaderTemplate>
                                <table width="100%" border="0">
                                    <tr>
                                        <td width="10%" align="center" class="tableHeader">Unique<br/>ID</td>
                                        <td width="30%" align="left" class="tableHeader">Name</td>
                                        <td width="40%" align="center" class="tableHeader">Date/Time</td>
                                        <td width="15%" align="center" class="tableHeader">Duration</td>
                                        <td width="5%" align="center" class="tableHeader">View</td>
                                    </tr>
                                </table>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <table width="100%">
                                    <tr>
                                    <%--Window Dresing--%>
                                        <td width="10%" class="tableBody" align="center"><asp:Label ID="lblConfUniqueID" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.confUniqueID") %>'></asp:Label></td>
                                        <td width="30%" class="tableBody" align="left"><asp:Label ID="lblConfName" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.confName") %>'></asp:Label></td>
                                        <td width="40%" class="tableBody" align="center"><asp:Label ID="lblConfStartDate" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.confDate") %> '></asp:Label>&nbsp;
                                        <asp:Label ID="lblConfStartTime" class="tableBody"  runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.confTime")%> '></asp:Label>&nbsp;
                                        <asp:Label ID="lblConfTimezone" class="tableBody" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.confTimezone")%> '></asp:Label>&nbsp;
                                        </td>
                                        <td width="15%" align="center" class="tableBody" ><asp:Label ID="lblConfDuration" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.confDuration") %>'></asp:Label></td>
                                        <td width="5%" align="center" class="tableBody" ><asp:LinkButton ID="btnView" Text="Details" runat="server"></asp:LinkButton></td>
                                    </tr>
                                    <tr>
                                        <td colspan="4">
                                            <asp:DataGrid ID="dgBridgeResources" AutoGenerateColumns="false" runat="server" Width="90%" BorderWidth="1" BorderStyle="Solid" BorderColor="Black" > <%--Edited for FF--%>
                                                 <%--Window Dressing start--%>
                                                <SelectedItemStyle CssClass="tableBody" />
                                                <EditItemStyle CssClass="tableBody" />
                                                <AlternatingItemStyle CssClass="tableBody" />    
                                                <ItemStyle CssClass="tableBody" BorderWidth="1"/>
                                                 <%--Window Dressing end--%>
                                                <HeaderStyle CssClass="tableHeader" BorderWidth="1"/>
                                                <Columns>
                                                    <asp:BoundColumn HeaderStyle-CssClass="tableHeader" DataField="BridgeID" Visible="false" ItemStyle-BorderWidth="1" ></asp:BoundColumn>
                                                    <asp:BoundColumn HeaderStyle-CssClass="tableHeader" ItemStyle-CssClass="tableBody" ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left" DataField="BridgeName" HeaderText="Bridge<br>Name" ItemStyle-BorderWidth="1"></asp:BoundColumn> <%-- FB 2050 --%>
                                                    <asp:BoundColumn HeaderStyle-CssClass="tableHeader" ItemStyle-CssClass="tableBody" ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left" DataField="AudioPorts" HeaderText="Sufficient<br>Audio Only Ports" Visible="true" ItemStyle-BorderWidth="1"></asp:BoundColumn> <%--FB 1938--%> <%-- FB 2050 --%>
                                                    <asp:BoundColumn HeaderStyle-CssClass="tableHeader" ItemStyle-CssClass="tableBody" ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left" DataField="VideoPorts" HeaderText="Sufficient<br>Audio/Video Ports" Visible="true" ItemStyle-BorderWidth="1"></asp:BoundColumn> <%-- FB 2050 --%><%--2908 End--%>
                                                </Columns>
                                            </asp:DataGrid>
                                            <asp:DropDownList runat="server" ID="lstBridgeCountV" DataTextField="BridgeID" DataValueField="BridgeCount" Visible="false"></asp:DropDownList>
                                            <asp:DropDownList runat="server" ID="lstBridgeCountA" DataTextField="BridgeID" DataValueField="BridgeCount" Visible="false"></asp:DropDownList>
                                        </td>
                                    </tr>
                                </table>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                    </Columns>
                    <ItemStyle Height="20px" />
                
                </asp:DataGrid>
                    <asp:Label ID="lblNoReports" runat="server" Font-Names="Verdana" Font-Size="Small"
                        Text="No Data found." Visible="False"></asp:Label>&nbsp;
                </td>
            </tr>
           <tr>
        </table>
<asp:DropDownList ID="lstBridges" runat="server" DataTextField="BridgeName" DataValueField="BridgeID" Visible="false"></asp:DropDownList>  <%-- SelectedValue='<%# DataBinder.Eval(Container, "DataItem.BridgeID") %>'--%>
</center>
                <input type="hidden" id="helpPage" value="81">

    </form>
</body>
</html>
<%--code added for Soft Edge button--%>
<script type="text/javascript" src="inc/softedge.js"></script>
<!-- #INCLUDE FILE="inc/mainbottomNET.aspx" -->