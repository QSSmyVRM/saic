﻿<%--ZD 100147 start--%>
<%--/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.	
*
* You should have received a copy of the myVRM license with	
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/--%><%--ZD 100147 End--%>
<%@ Page Language="C#" AutoEventWireup="true" Inherits="ns_MyVRM.ManageAudioAddOnBridges" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<meta http-equiv="X-UA-Compatible" content="IE=8" /> <%--FB 2779--%>
<!-- #INCLUDE FILE="inc/maintopNET.aspx" -->
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Manage Audio Bridges</title>

    <script type="text/javascript" src="script/mousepos.js"></script>

    <script type="text/javascript" src="script/managemcuorder.js"></script>

    <script type="text/javascript" src="inc/functions.js"></script>

    <link rel="StyleSheet" href="css/divtable.css" type="text/css" />
    <link rel="stylesheet" title="Expedite base styles" type="text/css" href="css/border-table.css" />
    <link rel="stylesheet" title="Expedite base styles" type="text/css" href="css/main-table.css" />
    <link rel="stylesheet" title="Expedite base styles" type="text/css" href="css/main.css" />
    <script type="text/javascript">        // FB 2790
        var path = '<%=Session["OrgCSSPath"]%>';
        path = path.split('.')[0] + '<%=Session["ThemeType"]%>' + ".css"; // FB 2815
        document.write('<link rel="stylesheet" title="Expedite base styles" type="text/css" href="' + path + '?' + new Date().getTime() + '">');
    </script>
   <script type="text/javascript">
       //ZD 100176 start
       function DataLoading(val) {
           if (val == "1")
               document.getElementById("dataLoadingDIV").innerHTML = "<img border='0' src='image/wait1.gif'>";
           else
               document.getElementById("dataLoadingDIV").innerHTML = "";
       }
       //ZD 100176 End
   </script>
    </head>
    
<body>
    <form id="form1" runat="server">
    <div>
        <input type="hidden" id="helpPage" value="65" />
        <table width="100%">
            <tr>
                <td align="center">
                    <h3>
                        <asp:Label ID="lblHeader" runat="server" Text="Manage Audio Bridges"></asp:Label>
                    </h3>
                </td>
            </tr>
            <tr>
                <td align="center">
                    <asp:Label ID="lblError" runat="server" Text="" CssClass="lblError"></asp:Label>
                </td>
            </tr>
            <div id="dataLoadingDIV" align="center"></div> <%--ZD 100176--%>
            <tr>
                <td align="center">
                    <table width="40%" cellspacing="0" cellpadding="0" border="0">
                        <tr>
                            <td align="left">
                                <span class="subtitleblueblodtext">Existing Audio Bridges</span>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td align="center">
                    <asp:DataGrid ID="dgAudioBridges" runat="server" AutoGenerateColumns="False" CellPadding="4"
                        GridLines="None" BorderColor="blue" BorderStyle="solid" BorderWidth="1" ShowFooter="false"
                        OnEditCommand="EditAudionAddonBridge" OnDeleteCommand="DeleteAudionAddonBridge"
                        Width="40%" Visible="true" Style="border-collapse: separate">
                        <SelectedItemStyle CssClass="tableBody" />
                        <EditItemStyle CssClass="tableBody" />
                        <AlternatingItemStyle CssClass="tableBody" />
                        <ItemStyle CssClass="tableBody" />
                        <HeaderStyle CssClass="tableHeader" HorizontalAlign="left" />
                        <Columns>
                            <asp:BoundColumn DataField="userID" Visible="false">
                                <HeaderStyle CssClass="tableHeader" HorizontalAlign="left" />
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="firstName" ItemStyle-CssClass="tableBody" HeaderStyle-CssClass="tableHeader"
                                ItemStyle-HorizontalAlign="Left" HeaderText="Name"></asp:BoundColumn>
                            <asp:BoundColumn DataField="Status" ItemStyle-CssClass="tableBody" HeaderStyle-CssClass="tableHeader"
                                ItemStyle-HorizontalAlign="Left" HeaderText="Status" Visible="false"></asp:BoundColumn>
                            <asp:TemplateColumn HeaderText="Actions" ItemStyle-CssClass="tableBody" ItemStyle-Width="28%"
                                ItemStyle-HorizontalAlign="Left">
                                <HeaderStyle CssClass="tableHeader" HorizontalAlign="left" />
                                <ItemTemplate>
                                    <asp:LinkButton runat="server" Text="Edit" ID="btnEdit" Visible='<%# Session["admin"].ToString().Trim().Equals("2") || Session["admin"].ToString().Trim().Equals("3")%>' CommandName="Edit" Width="28%" OnClientClick="DataLoading(1)"></asp:LinkButton> <%--ZD 100176--%> 
                                    <asp:LinkButton runat="server" Text="Delete" ID="btnDelete" Visible='<%# (Session["admin"].ToString().Trim().Equals("2") || Session["admin"].ToString().Trim().Equals("3"))%>' CommandName="Delete" OnClientClick="DataLoading(1)"></asp:LinkButton><%--ZD 100176--%> 
                                </ItemTemplate>
                            </asp:TemplateColumn>
                        </Columns>
                    </asp:DataGrid>
                    <asp:Table runat="server" ID="tblNoAudioBridges" Visible="false" Width="90%">
                        <asp:TableRow CssClass="lblError">
                            <asp:TableCell CssClass="lblError">
                                No Audio bridges found.
                            </asp:TableCell>
                        </asp:TableRow>
                    </asp:Table>
                </td>
            </tr>
            <tr>
                <td align="center">
                    <table width="40%" cellspacing="0" cellpadding="0" border="0">
                        <tr>
                            <td align="right" width="20%">
                                <b class="blackblodtext">Total Audio bridges: </b><b><asp:Label ID="lblTotalUsers" runat="server" Text=""></asp:Label> </b>
                            </td>
                            <td  align="right" width="20%" runat="server" id="tdLicencesRemaining">
				                <b class="blackblodtext">Licenses Remaining: </b><b><asp:Label ID="lblLicencesRemaining" runat="server" Text=""></asp:Label> </b>                                
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td align="center"> <%--FB 2023--%>
                    <asp:Table ID="tblPage" Visible="false" runat="server">
                        <asp:TableRow ID="TableRow1" runat="server">
                            <asp:TableCell ID="TableCell1" Font-Bold="True" Font-Names="Verdana" Font-Size="Small" ForeColor="Blue" runat="server"><span class="blackblodtext"> Pages:</span> </asp:TableCell>
                            <asp:TableCell ID="TableCell2" runat="server"></asp:TableCell>
                        </asp:TableRow>
                    </asp:Table>

                </td>
            </tr>
            <tr>
                <td align="left">
                </td>
            </tr>
            <tr id="trNewBridge" runat="server">
                <td align="center">
                    <table width="40%" border="0">
                        <tr>
                            <td width="50%">
                                &nbsp;
                            </td>
                            <td align="right">
                                <asp:Button ID="btnNewAudioBridge" runat="server" 
                                    Text="Create New Audio Bridge" Width="215px" OnClick="CreateNewAudioAddonBridge" OnClientClick="DataLoading(1)"  /> <%--FB 2796--%> <%--ZD 100176--%> 
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </div>
    </form>
</body>
</html>
<!-- #INCLUDE FILE="inc/mainbottomNET.aspx" -->
