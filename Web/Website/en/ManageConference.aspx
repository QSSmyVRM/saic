<%--ZD 100147 start--%>
<%--/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.	
*
* You should have received a copy of the myVRM license with	
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/--%><%--ZD 100147 End--%>
<%@ Page Language="C#" Inherits="ns_MyVRM.ManageConference" Buffer="true" EnableEventValidation="false" ValidateRequest="false" %><%--ZD 100170--%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> <%--FB 2779--%>
<meta http-equiv="X-UA-Compatible" content="IE=8" /> <!-- FB 2050 -->

<style type="text/css">
#Menu22 div
{
	width:123px; /* FB 2050 */
	height:35px;
	z-index:0;
}
#Menu22 td
{
	vertical-align:top; /* FB 2050 */
}
</style>

<script runat="server">

    protected void Menu22_MenuItemClick(object sender, MenuEventArgs e) // FB 2050

    {
      
        int index = Int32.Parse(e.Item.Value);
        MultiView1.ActiveViewIndex = index;
        if (index == 1)
        {
            if (ImagesPath.Text.Equals(""))
                GetVideoLayouts();
            LoadEndpoints();
        }
        if (index == 2)
            CheckResourceAvailability();
            
    }
</script>
<script language="javascript">

                   
                        if (document.getElementById("Menu22") != null) // FB 2050
                        {
                            document.getElementById("Menu22").innerHTML = document.getElementById("Menu22").innerHTML.replace("javascript:", "javascript:if (DataLoading(1)) "); // FB 2050
                        }
                    

</script>
<% 
    //Response.Write(Request.QueryString["t"]);
if (Request.QueryString["t"] != null)
if (!Request.QueryString["t"].ToString().Equals("hf"))
   { %>
    <!-- #INCLUDE FILE="inc/maintopNET.aspx" --> 
<% }
   else
   {
      // Response.Write(Request.QueryString["t"]);
%>
    <!-- #INCLUDE FILE="inc/maintop4.aspx" --> 
<%} %>

<% 
    //Response.Write(Request.QueryString["t"]);
if (Request.QueryString["hf"] != null)
if (Request.QueryString["hf"].ToString().Equals("1"))
   { %>
        <!-- #INCLUDE FILE="inc/maintop4.aspx" --> 
<%} %>


<link rel="stylesheet" title="Expedite base styles" type="text/css" href="css/border-table.css">
<link rel="stylesheet" title="Expedite base styles" type="text/css" href="css/main-table.css">
<script type="text/javascript">    // FB 2790
    var path = '<%=Session["OrgCSSPath"]%>';
    path = path.split('.')[0] + '<%=Session["ThemeType"]%>' + ".css"; // FB 2815
    document.write('<link rel="stylesheet" title="Expedite base styles" type="text/css" href="' + path + '?' + new Date().getTime() + '">');
</script>

<link rel="alternate" type="application/atom+xml" title="your feed title here" href= "http://www.buy.com/rss/feed.asp?loc=273&grp=4">
<script type="text/javascript" src="script/mytree.js"></script>
<script type="text/javascript" src="script/mousepos.js"></script>
<script type="text/javascript" src="script/DisplayDIV.js"></script>
<script type="text/javascript" src="inc/functions.js"></script>
<script type="text/javascript" src="script/Workorder.js"></script>

<script language="javascript" type="text/javascript">



function ViewBridgeDetails(bid)
{
    url = "BridgeDetailsViewOnly.aspx?hf=1&bid=" + bid;
    window.open(url, "BrdigeDetails", "width=900,height=800,resizable=yes,scrollbars=yes,status=no");
    return false;
}
    function ShowImage(obj)
    {
        //alert(obj.src);
        document.getElementById("myPic").src = obj.src;
        getMouseXY();
        //alert(document.body.scrollHeight);
        document.getElementById("divPic").style.position = 'absolute';
        document.getElementById("divPic").style.left = mousedownX + 20;
        document.getElementById("divPic").style.top = mousedownY;
        document.getElementById("divPic").style.display="";
        //alert(obj.style.height + " : " + obj.style.width);
    }

    function HideImage()
    {
        document.getElementById("divPic").style.display="none";
    }

function ViewDetails(id, confid)
{
    ViewWorkorderDetails(id, confid);
}
function ViewDetails(id, confid, tpe)
{
    ViewWorkorderDetails(id, confid, tpe);
}
var servertoday = new Date();

function displayDetails(str)
{
    var obj = document.getElementById(str);
    if (obj!= null)
    	display_prompt('image/pen.gif', 'Details', obj.outerHTML);
//    alert(obj.innerHTML);
}

function hideDetails()
{
    document.getElementById("prompt").style.display = "none";
}

function getCustomRecur(cid)
{
	if (cid != "") {
		if (ifrmPreloading != null)
			ifrmPreloading.window.location.href = "dispatcher/conferencedispatcher.asp?cmd=GetInstances&mode=21&frm=preload&cid=" + cid;
	}
}
function getRecur(cid)
{
//alert("in getrecur" + cid);
	if (cid != "") {
		if (ifrmPreloading != null)
			ifrmPreloading.window.location.href = "dispatcher/conferencedispatcher.asp?cmd=GetInstances&mode=22&frm=preload&cid=" + cid;
	}
}
function setConfInCalendar(f, instInfo)
{

    var confid = document.getElementById("<%=lblConfID.ClientID%>").value;
    if (confid == "")
        confid = document.getElementById("<%=lblConfUniqueID.ClientID%>").innerHTML;
	//url = "saveconfincal.asp?f=" + f + "&ii=" + confid;
	//alert(f);
	//Code Changed for FB 1410 - Start
//	url = "SetSessionOutXml.aspx?tp=saveconfincal.asp&ii=" + instInfo + "&f=" + f;
	url = "GetSessionOutXml.aspx?ii=" + instInfo + "&f=" + f;
	//Code Changed for FB 1410 - End
//	alert(url);
	if (!window.winrtc) {	// has not yet been defined
		winrtc = window.open(url, "", "width=600,height=350,top=0,left=0,resizable=no,scrollbars=no,status=no");
		winrtc.focus();
	} else {	// has been defined
	    if (!winrtc.closed) {     // still open
	    	winrtc.close();
	        winrtc = window.open(url, "", "width=600,height=350,top=0,left=0,resizable=no,scrollbars=no,status=no");
			winrtc.focus();
		} else {
	        winrtc = window.open(url, "", "width=600,height=350,top=0,left=0,resizable=no,scrollbars=no,status=no");
	        winrtc.focus();
		}
	}
}

  function saveToOutlookCalendar (iscustomrecur, isrecr, f, instInfo) 
  {
	// outlook do not support custom recur. Maybe LN can, but current can not test.
	var confid = document.getElementById("<%=lblConfID.ClientID%>").value;
	//alert(confid);
	instanceInfo = "";
	//instInfo = document.getElementById("Recur").value;
	//alert(f + " : " + instInfo);
	if (parseInt(iscustomrecur)) {
		getCustomRecur(confid);
	} else {
		//if (parseInt(isrecr))
		//	getRecur(confid);
		//else
			setConfInCalendar(f, confid);
	}
  }
  
  function pdfReport() //fogbugz case 38 and 386
    {
    
        var loc = document.location.href; 
        loc = loc.substring(0,loc.indexOf("ManageConference.aspx") - 3); //FB 1830
         
        var htmlString = document.getElementById("tblMain").innerHTML;
        //remove Actions table from PDF
        var toBeRemoved = document.getElementById("tblActions");
        var path = '<%=Session["OrgCSSPath"]%>';
        path = path.replace("../","");
        var imagepath = '<%=Session["OrgBanner1600Path"]%>'; //FB 1830        
        if (toBeRemoved != null)
            htmlString = htmlString.replace(toBeRemoved.innerHTML, "");
        //Remove Expand Collapse checkbox
        toBeRemoved = document.getElementById("tdExpandCollapse");
        
        if (toBeRemoved != null)
            htmlString = htmlString.replace(toBeRemoved.parentNode.innerHTML, "");
        //replace all doube " with single '        
        htmlString = htmlString.replace(new RegExp("\"","g"), "'");
        loc = "http://localhost" + loc.replace(loc.substring(0, loc.indexOf("/", 8)), "");   //fogbugz case 386 Saima
        //remove all image source relative paths with the absolute path
        //insert the banner on top of page and style sheet on top.
        //Code Changed for RSS
        //FB 1830
         if("<%=Session["ImageURL"]%>" == "")
         {
           htmlString = htmlString.replace(new RegExp("image/","g"), loc + "image/");
           //FB 2102 Start
           htmlString = "<html><link rel='stylesheet' type='text/css' href='" + loc + path +"' /><body><center><table><tr><td></td></tr></table>" + htmlString + "</center></body></html>";
          //htmlString = "<html><link rel='stylesheet' type='text/css' href='" + loc + path +"' /><body><center><table><tr><td><img src='" + loc + imagepath + "' width='100%' height='72'></td></tr></table>" + htmlString + "</center></body></html>";
          //FB 2102 End
        }
        else
        {
            if(path == "")            
                path = "/Organizations/Org_11/CSS/Mirror/Styles/main.css";
                
          var url = "<%=Session["ImageURL"]%>";
          htmlString = htmlString.replace(new RegExp("image/","g"), url + "/en/image/");
          htmlString = "<html><link rel='stylesheet' type='text/css' href='" + url + path +"' /><body><center>" + htmlString + "</center></body></html>";
        }
        if (document.getElementById("tempText") != null)
        {
            document.getElementById("tempText").value = "";
            document.getElementById("tempText").value = htmlString;
        }
        //alert(htmlString.length + " : " + document.getElementById("tempText").value.length);    
        
        return true;
    }

  function ExpandAll()
  {
//    var obj = document.getElementById("img_LOC");
//    obj.src = obj.src.replace("plus", "minus");
        ShowHideRow("LOC", document.getElementById("img_LOC"),true);
        ShowHideRow("PAR", document.getElementById("img_PAR"),true);
        ShowHideRow("CustOpt", document.getElementById("Img_CustOpt"),true);  //Custom attribute fixes
        ShowHideRow("AV", document.getElementById("Img_AV"),true);
        ShowHideRow("AVWO", document.getElementById("Img_AVWO"),true);
        ShowHideRow("CATWO", document.getElementById("Img_CATWO"),true);
        ShowHideRow("HKWO", document.getElementById("Img_HKWO"),true);
        ShowHideRow("CON", document.getElementById("Img_CON"),true);//FB 2359//FB 2632
  }
  function ShowHideRow(rType, obj, frmCheck)
  {
    var tempRow = document.getElementById("tr_" + rType);
    //var tempLbl = document.getElementById("lbl_" + rType);
    //alert(obj.src);
    if ( (tempRow != null) && (obj != null) )
    {
        if (frmCheck == true)
        {
            if (document.getElementById("chkExpandCollapse").checked)
            {
                obj.src = obj.src.replace("minus", "plus");
                tempRow.style.display = "none";
            }
            else
            {
                obj.src = obj.src.replace("plus", "minus");
                tempRow.style.display = "";
            }
        }
        if (frmCheck == false)
        {
            if (obj.src.indexOf("minus") > 0)
            {
                obj.src = obj.src.replace("minus", "plus");
                tempRow.style.display = "none";
                //tempLbl.style.display = "";
            }
            else
            {
                obj.src = obj.src.replace("plus", "minus");
                tempRow.style.display = "";
                //tempLbl.style.display = "none";
            }
        }
    }
  }
  
  function btnSetupAtMCU_Click()
  {
  
    if (confirm("Are you sure you want to setup this conference on bridge?\nThis will be an irreversible process."))
    {
        DataLoading(1);
        document.getElementById("<%=cmd.ClientID %>").value="1";
        document.frmSubmit.action="ManageConference.aspx?t=";
        document.frmSubmit.submit();
        return true;
    }
    else
        return false;
  }
  
  function btnAcceptReject_Click()
  {
    //FB 2438 - Start
    if ("<%=isAcceptDecline%>" == 1)
    {
        alert("Room has been added or modified in individual instances");
    }
    
    //alert("<%=Session["ConfID"] %>");
    window.location.href="ResponseConference.aspx?t=2&id=" + "<%=Session["ConfID"] %>" + "&req=" + "<%=Session["userID"] %>";
    //FB 2438 - End
  }
  
  function btnDeleteConference_Click()
  {
    //Code Edited for FB 1425 QA Bug -Start
  var msg;
  if('<%=Application["Client"]%>' == "MOJ")
  msg = "Are you sure you want to delete this hearing?";
  else
  msg = "Are you sure you want to delete this conference?";
  
    if (confirm(msg))
    //Code Edited for FB 1425 QA Bug -End
    {
//        document.getElementById("<%=cmd.ClientID %>").value="2";
//        document.frmSubmit.action="ManageConference.aspx?t=2";
//        document.frmSubmit.submit();
        DataLoading(1);
        return true;
    }
    else
        return false;
  }
  function ChangeView(id)
  {
    if (id == "1")
    {
        document.getElementById("divNormal").style.display="";
        document.getElementById("divEndpoint").style.display="none";
    }
    else
    {
        document.getElementById("divEndpoint").style.display="";
        document.getElementById("divNormal").style.display="none";
    }        
  }
  function managelayout (dl, epid, epty)
    {

    //	change_display_layout_prompt('image/pen.gif', 'Manage Display Layout', epid, epty, dl, 4, "", ""); 
	    change_display_layout_prompt('image/pen.gif', 'Manage Display Layout', epid, epty, dl, 5, document.getElementById('<%=ImageFiles.ClientID%>').value + '|' + document.getElementById('<%=ImageFilesBT.ClientID%>').value, document.getElementById('<%=ImagesPath.ClientID%>').value);
    }

function change_display_layout_prompt(promptpicture, prompttitle, epid, epty, dl, rowsize, images, imgpath) 
{
    var tempEpid = epid;
	var title = new Array()
	title[0] = "Default ";
	title[1] = "Custom ";
	promptbox = document.createElement('div'); 
	promptbox.setAttribute ('id' , 'prompt');
	document.getElementsByTagName('body')[0].appendChild(promptbox);
	promptbox = eval("document.getElementById('prompt').style");

    //FB 2950 - Start
    promptbox.position = 'absolute'	
    promptbox.top = -120+mousedownY + 'px';
	promptbox.left = mousedownX + 'px'; 
	promptbox.width = rowsize * 100 + 'px';
	promptbox.border = 'outset 1 #bbbbbb' 
	promptbox.height = 400 + 'px';	
	promptbox.overflow ='auto';
	//FB 2950 - End

	m = "<table cellspacing='0' cellpadding='0' border='0' width='100%'><tr valign='middle'><td width='22' height='22' style='text-indent:2;' class='titlebar'><img src='" + promptpicture + "' height='18' width='18'></td><td class='titlebar'>" + prompttitle + "</td></tr></table>" 
	m += "<table cellspacing='2' cellpadding='2' border='0' width='100%' class='promptbox'>";
	imagesary = images.split(":");
	rowNum = parseInt( (imagesary.length + rowsize - 2) / rowsize, 10 );
	m += "  <tr><td align='right' colspan='" + (rowsize * 2) + "'>"
	m += "    <input type='button' class='altMedium0BlueButtonFormat' value='Submit' onMouseOver='this.style.border=\"1 outset #dddddd\"' onMouseOut='this.style.border=\"1 solid transparent\"' onClick='saveOrder(\"" + dl + "\", epid, \"" + epty + "\",\"" + tempEpid + "\");'>"
	m += "    <input type='button' class='altMedium0BlueButtonFormat' value='Cancel' onMouseOver='this.style.border=\"1 outset transparent\"' onMouseOut='this.style.border=\"1 solid transparent\"' onClick='cancelthis();'>"
	m += "  </td></tr>"
	m += "	<tr>";
	m += "    <td colspan='" + (rowsize * 2) + "' align='left'>Display Layout</td>";//FB 2579
	m += "  </tr>"
	m += "  <tr>"
	m += "    <td colspan='" + (rowsize * 2) + "' height='5'></td>";
	m += "  </tr>"

	imgno = 0;
	for (i = 0; i < rowNum; i++) 
	{
		m += "  <tr>";
		for (j = 0; (j < rowsize) && (imgno < imagesary.length-1); j++) {
			
		
			m += "    <td valign='middle'>";
			m += "      <input type='radio' name='layout' id='layout' value='" + imagesary[imgno] + "' onClick='epid=" + imagesary[imgno] + ";'>";
			m += "    </td>";
			m += "    <td valign='middle'>";
			m += "      <img src='" + imgpath + imagesary[imgno] + ".gif' width='30' height='30'>";
			m += "    </td>";
			imgno ++;
		}
		m += "  </tr>";
	}
    
	m += "  <tr>";
	m += "    <td colspan='" + (rowsize * 2) + "' height='5'></td>";
	m += "  </tr>"
	m += "  <tr><td align='right' colspan='" + (rowsize * 2) + "'>"
	m += "    <input type='button' class='altMedium0BlueButtonFormat' value='Submit' onMouseOver='this.style.border=\"1 outset #dddddd\"' onMouseOut='this.style.border=\"1 solid transparent\"' onClick='saveOrder(\"" + dl + "\", epid, \"" + epty + "\",\"" + tempEpid + "\");'>"
	m += "    <input type='button' class='altMedium0BlueButtonFormat' value='Cancel' onMouseOver='this.style.border=\"1 outset transparent\"' onMouseOut='this.style.border=\"1 solid transparent\"' onClick='cancelthis();'>"
	m += "  </td></tr>"
	m += "</table>" 
	
	document.getElementById('prompt').innerHTML = m;
} 

function saveOrder(objid, id, epty, epid) 
{
//    alert(objid + " : " + id + " : " + epty + " : " + epid);
    if (id < 10)
        id = "0" + id;
	
	if (epty=="")
    {
        document.getElementById("<%=txtTempImage.ClientID %>").value=document.getElementById("<%=txtSelectedImage.ClientID %>").value;
        document.getElementById("<%=txtSelectedImage.ClientID %>").value = id;
	    document.getElementById("<%=cmd.ClientID %>").value="5";
        document.frmSubmit.action="ManageConference.aspx?t=5";
        document.frmSubmit.submit();
    }
    else
        if (epty != "1")
        {
	        document.getElementById("<%=txtTempImage.ClientID %>").value=document.getElementById("<%=txtSelectedImageEP.ClientID %>").value;
            document.getElementById("<%=txtSelectedImageEP.ClientID %>").value = id;
	        document.getElementById("<%=txtEndpointType.ClientID %>").value=epty + "," + epid;
            document.getElementById("<%=cmd.ClientID %>").value="5";
            document.frmSubmit.action="ManageConference.aspx?t=6"; 
            document.frmSubmit.submit();
        }
	document.getElementById(objid).src = document.getElementById("<%=ImagesPath.ClientID %>").value + document.getElementById("<%=txtSelectedImage.ClientID %>").value + ".gif";
	cancelthis();
} 


function cancelthis()
{
	document.getElementsByTagName("body")[0].removeChild(document.getElementById("prompt"));
	/*Code Commented for - FB 1400 - Start */
	//window.resizeTo(750,450);
}

function CheckEndpoint()
{
    if (document.getElementById("<%=imgVideoLayout.ClientID %>") != null)
        document.getElementById("<%=imgVideoLayout.ClientID %>").src = document.getElementById("<%=ImagesPath.ClientID %>").value + document.getElementById("<%=txtSelectedImage.ClientID %>").value + ".gif";    
}
/*Code Added for - FB 1400 - Start */
function fnValidateEndTime()
{
    var args = fnValidateEndTime.arguments; //FB 1562
    if(args[0] == 'p')
    {
		if ( (document.getElementById("txtPExtTime").value == "") ) {
				document.getElementById("LblPExtTimeMsg").style.visibility = "visible";
				document.getElementById("LblPExtTimeMsg").innerText = "Required";
				return (false);		
		}
	}
	else
	{
	    if ( (document.getElementById("txtExtendedTime").value == "") ) {
				document.getElementById("LblExtendedTimeMsg").style.visibility = "visible";
				document.getElementById("LblExtendedTimeMsg").innerText = "Required";
				return (false);		
		}
	}
	return true;
}
/*Code Added for - FB 1400 - Start */
//Code added for FB 1391 -- Start
function CustomEditAlert()
{
    var msg = "Some instances of this series have a unique Start Time/End Time.  Edit All will globally change the Start Time/End Time for all instances of this series.  Press OK to proceed and enter a new Start Time/End Time, or press Cancel to edit individual instances."
    var act;
    
    if ("<%=isCustomEdit%>" == "Y" )
    {
        act = confirm(msg);
        if(!act)
            DataLoading(0);
        return act;    
    }   
    
    return true;
}
//Code added for FB 1391 -- End

//Code added for P2P
function fnchkValue()
{
        var args = fnchkValue.arguments;
        var txtt
        if(args)
        {
            if(args[0])      
              txtt = document.getElementById(args[0]);
            
            if(txtt)
            {
                if(txtt.value == "")
                {
                    alert("Message cannot be empty.")
                    return false;
                }
            }
         }
            
        return true;
}

function fnOpenEpt()
{
    var args = fnOpenEpt.arguments;
    
    if(args[0])
    {
        window.open("http://"+args[0], "EndPointDetails", "width=900,height=800,resizable=yes,scrollbars=yes,status=no");
        return false;
    }
}

function fnOpenRemote()
{
    var args = fnOpenRemote.arguments;
    
    if(args[0])
    {
        window.open("http://"+ args[0] + "/a_tvmon.htm", "Monitor", "width=650,height=350,resizable=yes,scrollbars=yes,status=no");
        return false;
    }
}

function fnOpenMsg()
{

    var args = fnOpenMsg.arguments;
    var rw;
    if(args[0])
      rw = document.getElementById(args[0]);
    
    if(rw)
    {      
        if(args[1] == "1")
         rw.style.display = "block";
        else if(args[1] == "0")
             rw.style.display = "none"; 
    }
}

function shwHostDetails() //FB 1958
{
  document.getElementById("viewHostDetails").style.display = 'block';
  return false;
}
</script>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <link rel="stylesheet" type="text/css" href="css/myprompt.css" />
    
</head>
<body>
    <p>
        <br />
    </p>
    <%--EdIted For FB 1428 QA Bug--%>
  <input type="hidden" id="helpPage" value="66">

    <form id="frmSubmit" runat="server" method="post" onsubmit="return true">
    <input type="hidden" id="timezone" runat="server" /> <%--FB 1948--%>
    <%--<input type="hidden" name="hdnConceirgeSupp" id="hdnConceirgeSupp" runat="server" /> --%><%--FB 2359--%> <%--FB 2377--%> 
	<%--FB 2274 Starts--%>
    <input type="hidden" id="hdnCrossEnableBufferZone" runat="server" /> 
    <input type="hidden" id="hdnCrossEnableEntity" runat="server" /> 
    <input type="hidden" id="hdnCrossfoodModule" runat="server" />
    <input type="hidden" id="hdnCrosshkModule" runat="server" />
    <input type="hidden" id="hdnCrossroomModule" runat="server" />
    <%--FB 2274 Ends--%>
    <%--FB 2446 - Start --%>
    <input type="hidden" id="hdnCrossEnableConfPassword" runat="server" />
    <input type="hidden" id="hdnCrossEnablePublicConf" runat="server" />
    <%--FB 2446 - End --%>
	<input type="hidden" runat="server" id="hdnconfOriginID"/><%--2457 exchange round trip--%>
    <input type="hidden" id="hdnEnableEM7" runat="server" /><%--FB 2598--%>
    <input type="hidden" id="hdnNetworkSwitching" runat="server" /><%--FB 2595--%>
        
    <div id="viewHostDetails" runat="server" align="center" style="left:120px; top:150px; POSITION: absolute; HEIGHT: 350px;VISIBILITY: visible; Z-INDEX: 3; display:none; width:725px"> <%--FB 1958--%>
		<asp:PlaceHolder ID="HostDetailHolder" Runat="server"></asp:PlaceHolder>
    </div>
    <%--FB 2441 Starts--%>
    <div  id="MuteAllEndpointDiv"  style="left:700px; top:650px; display:none; POSITION: absolute; HEIGHT: 200px;VISIBILITY: visible; Z-INDEX: 3;  width:250px; background-color:#E1E1E1">
    <div style="HEIGHT: 170px;left:200px;overflow-y: scroll; background-color:#E1E1E1">
        <table>
        <tr>
        <td align="center"><span class="subtitleblueblodtext">Select Participants to UN-Mute</span></td>
        </tr>
        <tr>
        <td>
            
        <asp:DataGrid AutoGenerateColumns="false"  ShowHeader="false" GridLines="None" ID="dgMuteALL"  runat ="server">
            <Columns>
            <asp:BoundColumn DataField="ID" Visible="false"></asp:BoundColumn>
            <asp:BoundColumn DataField="type" Visible="false"></asp:BoundColumn>
            <asp:TemplateColumn>
                 <ItemTemplate>
                     <asp:CheckBox runat="server" ID="chk_muteall"/>
                 </ItemTemplate>
            </asp:TemplateColumn>
            <asp:BoundColumn DataField="Name"></asp:BoundColumn>
            </Columns>
        
       </asp:DataGrid>
        </td>
        </tr>
       
          </table>
          </div>
          <table> <tr>
        
           <td align="center">
                   <asp:Button ID="BtnSubmit" Text="Submit" CssClass="altMedium0BlueButtonFormat" OnClick="btnMuteAllExcept" runat="server" ></asp:Button>
              <asp:Button ID="BtnClose" Text="Close" CssClass="altMedium0BlueButtonFormat"  runat="server" OnClientClick="javascript:return fnShowHide('0');"></asp:Button>
           </td>
          </tr></table>
    </div>
    <%--FB 2441 Ends--%>
    <center>
    <div align="center" style="width:100%">
        <table width="100%" id="tblMain" name="tblMain">
            <tr>
            <td><%--Added For FB 1428 QA Bug--%>
            <div id="dataLoadingDIV" style="z-index:1" align="center"></div><%--FB 2814--%>
            <asp:CheckBox ID="Refreshchk" runat="server" style="display:none" />
            <table align="center"> <%--Added for FF--%>
            <tr nowrap>
                <td align="center" colspan="3" nowrap>
                    <h3><asp:Label ID="lblHeader" runat="server"></asp:Label>
                    <%--Added for FB 1428 START--%>
                    <%-- Organization Css Module --%>
                                <span id="Field15" runat="server">Manage Conference</span>
                                <%--Edited for FB 1428 END--%>
                    <asp:TextBox ID="cmd" runat="server" BorderStyle="None" BackColor="transparent" Width="0px" BorderWidth="0px"></asp:TextBox>
            <% if (Request.QueryString["t"] != null)
                   if (Request.QueryString["t"].ToString().Equals("hf"))
                    {
                       //window dressing
                        Response.Write("<input type='button' id='btnClose' onclick='javascript:window.close()' class='altMedium0BlueButtonFormat' value='Close'>");
                    }
           
            %>
               </h3> </td>
               </tr> <%--Added for FF--%>
               </table>
               </td><%--Added For FB 1428 QA Bug--%>
            </tr>
        
            <tr>
                <td align="center" colspan="3">
                    <asp:Label ID="errLabel" runat="server" CssClass="lblError"></asp:Label>
                    <asp:Table runat="server" ID="tblForceTerminate" Visible="false">
                        <asp:TableRow>
                            <asp:TableCell CssClass="lblError" Width="100%"><%--FB 2579 Start--%>
                                (WARNING: conference could be left running on the MCU)
			                    &nbsp;Do you want to terminate? <br />
			                    <asp:Button Text="Force Terminate" CssClass="altLongBlueButtonFormat" runat="server" OnClientClick="javascript:DataLoading(1)" OnClick="ForceTerminate" />
                                
                                
                            </asp:TableCell>
                        </asp:TableRow>
                    </asp:Table>
                    <%--<asp:Table Width="90%" runat="server">
                        <asp:TableRow Width="100%">
                            <asp:TableCell Width="100%">--%>
                                 <%--Window Dressing--%>
                                <br /><asp:Label ID="lblAlert" runat="server" CssClass="lblError" Width="90%"></asp:Label>
                           <%-- </asp:TableCell>
                        </asp:TableRow>
                    </asp:Table>--%>
                    </td>
            </tr>
            <tr>
                <td align="center" colspan="3">
                    <table width="100%" border="0"><%--Edited for FF--%>
                    <tr>
                    <td align="center"><%--FB 2446--%>                                
                       <%--Window Dressing--%>
                        <asp:TextBox ID="lblConfID" runat="server" width="0px" BorderStyle="None" BackColor="transparent"></asp:TextBox>
                    <table width="90%">
                        <tr>
                            <td align="left" width="120" Class="blackblodtext" valign="top"><%--FB 2446--%> 
                                Title</td>
                            <td style="width:1px; vertical-align:top"><b>:</b>&nbsp;</td>
                            <td align="left" width="150" style="padding-right:4px; COLOR: #046380; FONT-SIZE: 11pt; FONT-FAMILY: Arial;">
                                 <asp:Label ID="lblConfName" runat="server" Font-Bold="true"></asp:Label>
                                 <asp:LinkButton id="ConfTitle" runat="server">...</asp:LinkButton><%--FB 2508--%>
                            </td>
                            <td align="left" Class="blackblodtext" valign="top" nowrap="nowrap">
                                Unique ID</td>
                             <td style="width:1px; vertical-align:top"><b>:</b>&nbsp;</td>
                            <td align="left" colspan="2" style="height: 21px" width="200" valign="top">
                                <asp:Label ID="lblConfUniqueID" runat="server" ForeColor="Red"></asp:Label></td>
                            <td align="left" colspan="1" rowspan="8" width="290" valign="top" class="btprint">
                            <%--Window Dressing--%>
                                <asp:Table ID="tblActions" runat="server" BorderStyle="None" BorderWidth="0" CssClass="tableBody" CellPadding="0" CellSpacing="0" Width="100%">
                                    <asp:TableRow runat="server" CssClass="LinksHeader" Height="30px" >
                                        <asp:TableCell runat="server" VerticalAlign="Middle"  HorizontalAlign=right>Actions&nbsp;&nbsp;&nbsp;</asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow ID="trAcceptReject" runat="server" Height="20px">
                                        <asp:TableCell ID="TableCell3" runat="server">
                                            <a href="#" OnClick="javascript:btnAcceptReject_Click()">Accept/Decline</a>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow ID="trClone" runat="server" Height="20px">
                                        <asp:TableCell ID="TableCell4" runat="server">
                                            <asp:LinkButton ID="btnClone" Text="Clone" runat="server"  OnClientClick="javascript:DataLoading(1);" OnClick="CloneConference"></asp:LinkButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow runat="server" ID="trCancel" Height="20px">
                                        <asp:TableCell ID="TableCell2" runat="server"> 
                                            <asp:LinkButton runat="server" Text="Delete" ID="btnDeleteConf" OnClick="DeleteConference" OnClientClick="javascript:return btnDeleteConference_Click();" ></asp:LinkButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow ID="trEdit" runat="server" Height="20px">
                                        <asp:TableCell ID="TableCell5" runat="server">
                                            <asp:LinkButton ID="btnEdit" runat="server" Text="Edit" OnClientClick="javascript:DataLoading(1);" OnClick="EditConference" />
                                        </asp:TableCell></asp:TableRow><asp:TableRow runat="server" Height="20px">
                                        <asp:TableCell runat="server">
                                            <asp:LinkButton ID="btnPDF" Text="Export to PDF" runat="server" OnClick="ExportToPDF" OnClientClick="javascript:pdfReport();return true;"></asp:LinkButton>
                                        </asp:TableCell></asp:TableRow><asp:TableRow runat="server" Height="20px">
                                        <asp:TableCell runat="server">
                                            <a href="#" onclick="javascript:window.print();">Print</a>
                                        </asp:TableCell></asp:TableRow><asp:TableRow ID="TableRow4" runat="server" Visible="false" Height="20px"><%--FB 2152--%> <asp:TableCell ID="TableCell6" runat="server">
                                        <% if (Recur.Text.Equals(""))
                                           { %>
                                            <asp:LinkButton ID="btnOutlook" Text="Save to Outlook" runat="server" OnClientClick="javascript:saveToOutlookCalendar('0','0','1','');return false;"></asp:LinkButton>
                                        <% }
                                           else
                                           { %>
                                        <asp:LinkButton ID="btnOutlookR" Text="Save to Outlook" runat="server" OnClientClick="javascript:saveToOutlookCalendar('0','1','2','');return false;"></asp:LinkButton>
                                        <% } %>
                                        </asp:TableCell></asp:TableRow><asp:TableRow ID="trMCU" runat="server" Height="20px">
                                         <asp:TableCell ID="TableCell1" runat="server">
                                            <a href="#"  OnClick="javascript:btnSetupAtMCU_Click();">Setup on MCU</a>
                                        </asp:TableCell></asp:TableRow><asp:TableRow ID="tableRow12" runat="server" Height="20px"><%--Added For FB 1529--%> <asp:TableCell ID="TableCell12" runat='server'>
                                            <a href="javascript:location.reload(true);">Refresh</a>
                                        </asp:TableCell></asp:TableRow></asp:Table></td></tr><tr>
                             <td align="left" class="blackblodtext" valign="top" width="120"><%--FB 2446--%> <%if ((Application["Client"] == "MOJ")){%> <%--Added For FB 1428--%> Created By <%}else{ %> Host <%} %> </td><td style="width:1px; vertical-align:top"><b>:</b>&nbsp;</td><td align="left" width="150" valign="top">
                                <asp:Label ID="lblConfHost" runat="server" Font-Bold="False"></asp:Label><asp:Label ID="hdnConfHost" runat="server" Visible="false"></asp:Label><asp:ImageButton ID="imgHostDetails" runat="server"  ImageUrl="image/FaceSheet.GIF" OnClientClick="javascript:return shwHostDetails()" ToolTip="View Details" style="cursor: pointer;vertical-align:middle;"/><%--FB 1958--%> </td><td align="left" class="blackblodtext" valign="top" width="150">
                                Last Modified By</td><td style="width:1px; vertical-align:top"><b>:</b>&nbsp;</td><td align="left" colspan="2" style="height: 21px;" valign="top" width="200">
                                <asp:Label ID="lblLastModifiedBy" runat="server"></asp:Label>&nbsp;</td><asp:Label ID="hdnLastModifiedBy" runat="server" Visible="false"></asp:Label></tr>
                          <tr>
                            <td align="left" class="blackblodtext" valign="top" width="120">Date</td><%--FB 2446--%>
                            <td style="width:1px" valign="top"><b>:</b>&nbsp;</td>
                            <td align="left" width="250">
                                <asp:Label ID="lblConfDate" runat="server" Font-Bold="False"></asp:Label> 
                                <asp:Label ID="lblConfTime" runat="server" Font-Bold="False"></asp:Label>
                                <div style="height:auto; width:100%; overflow:auto" id="lblTimezoneDIV"><asp:Label ID="lblTimezone" runat="server"></asp:Label></div>
                                <%--Window Dressing--%>
                                <asp:TextBox runat=server id="Recur" Text="" TextMode="SingleLine" BorderStyle="None" Width="0px" BackColor="transparent" BorderColor="transparent" ForeColor="White" Height="10"></asp:TextBox>
                                <!--26&05&00&PM&60#1&2&-1&-1&-1&-1&-1&-1&-1&-1&-1&-1&-1&-1&-1&-1&-1#7/24/2007&2&5&-1-->
                            </td>
                            <td align="left" class="blackblodtext" valign="top">Duration</td>
                            <td style="width:1px" valign="top"><b>:</b>&nbsp;</td>
                            <td align="left" colspan="2" style="height: 21px;" valign="top" width="200" nowrap>
                                <asp:Label ID="lblConfDuration" runat="server"></asp:Label>
                                <asp:Label ID="hdnConfDuration" runat="server" Visible="false"></asp:Label></td>
                          </tr>                            
                            <%--code added for buffer zone -- start--%> <%if(!(Application["Client"] == "MOJ")){ %> <tr runat="server" id="bufferTableCell">
                            <td align="left" Class="blackblodtext" valign="top" nowrap style="height: 10px">
                                Conference Start</td><td style="width:1px"><b>:</b>&nbsp;</td><td align="left" width="150">
                                <asp:Label ID="lblSetupDur" runat="server" Font-Bold="False"></asp:Label></td><td align="left" Class="blackblodtext" valign="top" nowrap>
                                Conference End </td><td style="width:1px"><b>:</b>&nbsp;</td><td align="left" width="150" style="height: 10px;">
                                <asp:Label ID="lblTearDownDur" runat="server" Font-Bold="False"></asp:Label></td></tr><%}%> <%--code added for buffer zone -- End--%> <tr>
                            <td align="left" valign="top" width="120" Class="blackblodtext"><%--FB 2446--%> Status</td><td style="width:1px"><b>:</b>&nbsp;</td><td align="left" valign="top">
                                <asp:Label ID="lblStatus" runat="server" Font-Bold="False"></asp:Label></td><%if(!(Application["Client"] == "MOJ")){ %> <td align="left" valign="top" class="blackblodtext" id="tdType" runat="server">Type</td><td style="width:1px"><b>:</b>&nbsp;</td><td colspan="2" align="left">
                                    <asp:Label ID="lblConfType" runat="server" Font-Bold="false"></asp:Label></td><%}%> </tr><tr id="trPuPw" runat="server"><%--Edited for MOJ Phase 2 QA--%> <%--FB 2446 - Start --%> <td id="tdpu" runat="server" align="left" valign="top" width="120" class="blackblodtext">
                                Public</td><td id="tdpub" runat="server" style="width:1px"><b>:</b>&nbsp;</td><td id="tdpu1" runat="server"  align="left" valign="top">
                                <asp:Label ID="lblPublic" runat="server" Font-Bold="False"></asp:Label><asp:Label ID="lblRegistration" runat="server" Font-Bold="False"></asp:Label></td><td id="tdpw" runat="server" align="left" class="blackblodtext">
                                Password</td><td id="tdpwd" runat="server" style="width:1px"><b>:</b>&nbsp;</td><td id="tdpw1" runat="server" align="left" style="height: 21px;" colspan="2">
                                <asp:Label ID="lblPassword" runat="server"></asp:Label>&nbsp;</td><%--FB 2446 - End --%> </tr><tr><%--FB 1926--%> <td id="tdRemainder" runat="server" align="left" valign="top" width="120" class="blackblodtext"><%--FB 2446--%> Reminders</td><td id="tdRemainder1" runat="server" style="width:1px" valign="top"><b>:</b>&nbsp;</td><td id="tdRemainderselection" runat="server" align="left" valign="top"><%--FB 2694--%>
                                <asp:Label ID="lblReminders" runat="server" Font-Bold="False"></asp:Label>&nbsp;</td><%--FB 2501 Starts--%> <td id="trVNOC" runat="server" align="left" class="blackblodtext" valign="top" > 
                                VNOC Operator</td><td style="width:1px" valign="top"><b>:</b>&nbsp;</td><td id="trVNOCoptor" runat="server" align="left" style="height: 21px;" colspan="2" valign="top"> <%--FB 2670--%>
                                <asp:Label ID="lblConfVNOC" runat="server"></asp:Label>&nbsp;</td></tr><tr> <%--FB 2595 Starts--%> <td id="tdStartMode" runat="server" align="left" class="blackblodtext" valign="top" > 
                                Start Mode</td><td id="tdStartMode1" runat="server" style="width:1px"><b>:</b>&nbsp;</td><td id="tdStartModeSelection" runat="server" align="left">
                                <asp:Label ID="lblStartMode" runat="server"></asp:Label>&nbsp;</td><td id="tdSecured" runat="server" align="left" class="blackblodtext" valign="top" > 
                                Network State</td><td id="tdSecured1" runat="server" style="width:1px"><b>:</b>&nbsp;</td><td id="tdSecuredSelection" runat="server" align="left" style="height: 21px;" colspan="2" valign="top">
                                <asp:Label ID="lblSecured" runat="server"></asp:Label>&nbsp;</td></tr><%--FB 2501 Ends--%><%--FB 2595 Ends--%> <tr id="trFle" runat="server"><%--Edited for MOJ Phase 2 QA--%> <td align="left" valign="top" width="120" class="blackblodtext"><%--FB 2446--%> Files</td><td style="width:1px"><b>:</b>&nbsp;</td><td align="left" colspan="4" valign="top">
                                <asp:Label ID="lblFiles" runat="server" Font-Bold="False"></asp:Label></td></tr><tr>
                            <td align="left" valign="top" width="120" class="blackblodtext"><%--FB 2446--%> Description</td><td style="width:1px" valign="top"><b>:</b>&nbsp;</td><td align="left" colspan="6" valign="top"><%--FB 2508--%>
                                <asp:Label ID="lblDescription" runat="server" Font-Bold="False"></asp:Label>&nbsp;</td></tr><tr>
                <%--Window Dressing--%> <td align="right" colspan="8" valign="top" style="font-weight:bold" id="tdExpandCollapse"  class="subtitleblueblodtext">
                    <input id="chkExpandCollapse" type="checkbox" onclick="javascript:ExpandAll()" class="btprint" />Collapse All</td></tr></table></td></tr><%--FB 1985--%> <%if(Application["Client"].ToString().ToUpper() == "DISNEY") {%> <tr> 
                <td id="tblAVExpand" colspan="3" runat="server" align="center" > <%--Disney New Requirement--%> <table border="0" align="center" width="80px">
                        <tr> 
                            <td id="Td1" align="left"  onmouseover="javascript:return fnShowHideAVLink('1');" onmouseout="javascript:return fnShowHideAVLink('0');" runat="server">&nbsp; <asp:LinkButton ID="LnkAVExpand" style="display:none"  runat="server" Text="Expand" OnClick="fnShowEndpoint"></asp:LinkButton></td></tr></table></td></tr><%} %> <tr>
                <td colspan="3" align="center">
                    <asp:Menu
                        id="Menu22"
                        Orientation="Horizontal"
                        StaticMenuItemStyle-CssClass="tab"
                        StaticSelectedStyle-CssClass="selectedTab"
                        CssClass="tabs"
                        ItemWrap="true" 
                        OnMenuItemClick="Menu22_MenuItemClick"
                        Runat="server">
                        <Items>
                            <asp:MenuItem Text="<div align='center' valign='middle' style='width:123;'></div><br>" Value="0" /> <%--Edited for FF--%> <asp:MenuItem Text="<div align='center' valign='middle' style='width:123'>Endpoints</div><br>" Value="1" /><%--Edited for FF--%> <asp:MenuItem Text="<div align='center' style='width:123'>Resource<br>Availability</div>" Value="2" Selected="true" /><%--Edited for FF--%> <%--Code Added For FB 1422 - New Menu Item For Point to point Endpoint--%> <asp:MenuItem Text="<div align='center' valign='middle' style='width:123'>Point-To-Point</div><br>" Value="1" /><%--FB 2694--%><%--FB 2769--%>
                             
                         </Items>
                    </asp:Menu> <%-- FB 2050 --%> <div class="tabContents" style="width:90%" >
                            <asp:MultiView
                                id="MultiView1"
                                Runat="server">
                                <asp:View ID="NormalView" runat="server">
                                  <asp:Panel runat="server" Width="100%" Id="pnlNormal">
                                    <table cellspacing="0" cellpadding="0" border="0" width="100%">
                                        <tr>
                                            <td align="center">
                                              <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                                <tr>
                                                    <td align="left" colspan="2" class="subtitleblueblodtext">
                                                        <img border="0" id='img_LOC' src="image/loc/nolines_minus.gif" onclick="ShowHideRow('LOC', this,false)" />Locations <asp:Label ID="lblLocCount" runat="server" Font-Bold="True"></asp:Label></td></tr><tr id="tr_LOC">
                                                    <td width="5%">&nbsp;</td><td align="left" style="font-weight: bold;" valign="top">
                                                        <asp:Label ID="lblLocation" runat="server" Font-Bold="False"></asp:Label></td></tr></table></td></tr><tr id="trPrt" runat="server"> <%--Edited for FB 1425 QA Bug--%> <td align="center">
                                                <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                                                                <tr>
                                                            <td align="left" colspan="4" class="subtitleblueblodtext"><img border="0" id='img_PAR' src="image/loc/nolines_minus.gif" onclick="ShowHideRow('PAR', this,false)" />Participants <asp:Label ID="lblPartyCount" runat="server" Font-Bold="True"></asp:Label></td></tr><tr id="tr_PAR">
                                                            <td align="center" style="font-weight: bold;" colspan="4" rowspan="3">
                                                                <asp:Table ID="tblNoParty" runat="server" CellPadding="0" CellSpacing="0" Visible="False"
                                                                    Width="90%" BorderColor="Blue" BorderStyle="Solid" BorderWidth="1px">
                                                                    <asp:TableRow ID="TableRow6" runat="server" CssClass="tableHeader" Height="30px">
                                                                        <asp:TableCell ID="TableCell7" runat="server" CssClass="tableHeader">Name</asp:TableCell><asp:TableCell ID="TableCell8" runat="server" CssClass="tableHeader">Email</asp:TableCell><asp:TableCell ID="TableCell9" runat="server" CssClass="tableHeader">Status</asp:TableCell></asp:TableRow><%--Window Dressing--%> <asp:TableRow ID="TableRow7" runat="server" CssClass="lblError" Height="30px" HorizontalAlign="Center" VerticalAlign="Middle">
                                                                        <asp:TableCell ID="TableCell10" runat="server" ColumnSpan="3" CssClass="lblError"><%if (Application["Client"] == "MOJ"){%>There are no participants in this hearing.<%}else{ %>There are no participants in this conference.<%} %></asp:TableCell><%--Edited  For FB 1428--%></asp:TableRow></asp:Table><%--FB 2023--%> <asp:DataGrid runat="server" ID="partyGrid" OnSortCommand="SortGrid" AllowSorting="True" OnItemDataBound="partyGridBound"
                                                                 BorderColor="Blue" CellPadding="4" Font-Bold="False" AutoGenerateColumns="false"
                                                                 ForeColor="#333333" GridLines="None" Width="90%" BorderStyle="Solid" BorderWidth="1px" OnEditCommand="SendReminderToParticipant" style="border-collapse:separate"> <%--Edited for FF--%> <AlternatingItemStyle CssClass="tableBody" />
                                                                    <ItemStyle CssClass="tableBody"  />
                                                                    <HeaderStyle CssClass="tableHeader" Height="30px" />
                                                                    <Columns>
                                                                        <asp:BoundColumn DataField="partyID" Visible="false"></asp:BoundColumn>
                                                                        <asp:BoundColumn DataField="partyFirstName" HeaderText="First Name" ItemStyle-CssClass="tableBody"><HeaderStyle CssClass="tableHeader" /></asp:BoundColumn>
                                                                        <asp:BoundColumn DataField="partyLastName" HeaderText="Last Name" ItemStyle-CssClass="tableBody"><HeaderStyle CssClass="tableHeader" /></asp:BoundColumn>
                                                                        <asp:BoundColumn DataField="partyEmail" HeaderText="Email" ItemStyle-CssClass="tableBody"><HeaderStyle CssClass="tableHeader" /></asp:BoundColumn>
                                                                        <asp:BoundColumn DataField="partyStatus" HeaderText="Status" ItemStyle-CssClass="tableBody"><HeaderStyle CssClass="tableHeader" /></asp:BoundColumn>
                                                                        <asp:BoundColumn DataField="partyInvite" HeaderText="Invited As" ItemStyle-CssClass="tableBody"><HeaderStyle CssClass="tableHeader" /></asp:BoundColumn>
                                                                        <asp:ButtonColumn ButtonType="LinkButton" CausesValidation="true" CommandName="Edit" Text="Re-send Invitation" ItemStyle-CssClass="Link"></asp:ButtonColumn>
                                                                    </Columns>         
                                                                </asp:DataGrid>
                                                            </td>
                                                        </tr>
                                                </table>
                                            </td>
                                        </tr>                    
                                    </table>
                                  </asp:Panel>
                                </asp:View>
                                <asp:View ID="EndpointView" runat="server">
                                    <asp:Panel ID="pnlEndpoint" runat="server" Width="100%" style="font-weight: bold;">
                                        <asp:Table runat="server" ID="tblTerminalControl" Width="100%" Visible="false">
                                            <asp:TableRow>
                                                <asp:TableCell HorizontalAlign="center"> <table width="90%" border="0"><tr><%-- Window Dressing--%> <td align="left" style="width:10%" class="blackblodtext">Display Layout</td><%-- Code Modified FB 1400 - Changed allignments.Added TD tag - Start--%> <td align="left"><asp:Image ID="imgVideoLayout" runat="server" Width="30" Height="30" /></td><td align="left"><input type="button" id="ConfLayoutSubmit" name="ConfLayoutSubmit" runat="server" value="Change" class="altShortBlueButtonFormat" onclick="javascript: managelayout('<%=imgVideoLayout.ClientID %>', '01', '');" /> </td><%-- Code Modified FB 1400 - End--%> <%-- Window Dressing--%> <td align="left" class="blackblodtext">Extend End Time (in minutes) </td><td nowrap><%--Modified during FB 1562--%> <asp:TextBox ID="txtExtendedTime" CssClass="altText" runat="server" ValidationGroup="SubmitTime" ></asp:TextBox>&nbsp; <%-- Code Modified FB 1400 - Start--%> <asp:Button ID="btnExtendEndtime" CssClass="altShortBlueButtonFormat" OnClick="ExtendEndtime" ValidationGroup="SubmitTime" Text="Submit" runat="server" OnClientClick="javascript:return fnValidateEndTime('m');" /><%-- <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ValidationGroup="SubmitTime" ControlToValidate="txtExtendedTime"  ErrorMessage="<br>" runat="server" Display="dynamic"></asp:RequiredFieldValidator>--%> <br /><asp:Label ID="LblExtendedTimeMsg" style="visibility:hidden" Text="" ForeColor="red"  runat="server"></asp:Label><%-- Code Modified FB 1400 - End--%> <asp:RegularExpressionValidator ID="RegularExpressionValidator1" ValidationGroup="SubmitTime" ControlToValidate="txtExtendedTime" ErrorMessage="<br>Numeric Values only" ValidationExpression="\d+" runat="server" Display="dynamic"></asp:RegularExpressionValidator><asp:RangeValidator ID="RangeValidator1" ValidationGroup="SubmitTime" ControlToValidate="txtExtendedTime" ErrorMessage="<br>Range: 0 - 360 only" MinimumValue="0" MaximumValue="360" Type="Integer" runat="server" Display="dynamic"></asp:RangeValidator></td><%-- Window Dressing--%> <td align="left" class="blackblodtext">Auto Refresh (30 seconds) </td><td><asp:CheckBox ID="chkAutoRefresh" runat="server" Checked="true" onclick="javascript:UpdateEndpointStatus();" /></td></tr><%--FB 2441 Starts--%> <tr><td colspan="5" align="left" class="blackblodtext"><asp:LinkButton ID="lnkMuteAllExcept" Text="Mute All Party Except" runat="server" Visible="false" OnClientClick="javascript:return fnShowHide('1')"></asp:LinkButton>&nbsp;&nbsp;&nbsp;&nbsp; <asp:LinkButton ID="lnkUnMuteAllParties" Enabled="true" runat="server" Text="UnMute All Parties" Visible="false" OnClick="UnMuteAllParties"></asp:LinkButton>&nbsp;&nbsp; </td></tr><%--FB 2441 Ends--%> </table></asp:TableCell></asp:TableRow></asp:Table><asp:Table runat="server" ID="tblEndpoints" Width="100%">
                                        <%--  Code Modified For FB 1699--%> <asp:TableRow>
                                                <asp:TableCell HorizontalAlign="center"> <%--  Code Modified For FB 1367-  Nathira - Start--%> <asp:DataGrid runat="server" EnableViewState="true" OnItemDataBound="InitializeEndpoints" ID="dgEndpoints" AutoGenerateColumns="false"
                                                          style="border-collapse:separate" CellSpacing="0" CellPadding="4" GridLines="None" ShowFooter="true"  OnEditCommand="EditEndpoint" OnDeleteCommand="DeleteTerminal" OnUpdateCommand="ConnectEndpoint" OnCancelCommand="MuteEndpoint" BorderColor="blue" BorderStyle="solid" BorderWidth="1"  Width="90%"><%--Edited for FF--%> <%--  Code Modified For FB 1367-  Nathira - End--%> <AlternatingItemStyle CssClass="tableBody" /><ItemStyle CssClass="tableBody"  /><FooterStyle CssClass="tableBody" /><HeaderStyle CssClass="tableHeader" Height="30px" /><%-- Window Dressing start--%> <EditItemStyle CssClass="tableBody" /><SelectedItemStyle CssClass="tableBody"  Font-Bold="True"/><%-- Window Dressing end--%> <Columns><asp:BoundColumn DataField="ID" Visible="false"></asp:BoundColumn><asp:BoundColumn DataField="type" Visible="false"></asp:BoundColumn><asp:BoundColumn DataField="displayLayout" Visible="false"></asp:BoundColumn><asp:BoundColumn DataField="mute" Visible="false"></asp:BoundColumn><asp:BoundColumn DataField="ImageURL" Visible="false"></asp:BoundColumn><asp:BoundColumn DataField="Name" ItemStyle-HorizontalAlign="center" HeaderStyle-HorizontalAlign="center" HeaderText="Room/Attendee<br>Name" HeaderStyle-CssClass="tableHeader" ItemStyle-CssClass="tableBody"></asp:BoundColumn><asp:BoundColumn DataField="EndpointName" ItemStyle-HorizontalAlign="center" HeaderStyle-HorizontalAlign="center" HeaderText="Endpoint<br>Name" HeaderStyle-CssClass="tableHeader" ItemStyle-CssClass="tableBody"></asp:BoundColumn><%-- Code Modified For FB 1371 - Removed datafeild=status - Nathira - Start--%> <asp:BoundColumn ItemStyle-HorizontalAlign="center" HeaderStyle-HorizontalAlign="center" HeaderText="Endpoint Status" HeaderStyle-CssClass="tableHeader" Visible="false" ></asp:BoundColumn><%-- Code Modified For FB 1371 - Removed datafeild=status - Nathira - End--%> <asp:BoundColumn DataField="address" ItemStyle-HorizontalAlign="center" HeaderStyle-HorizontalAlign="center" HeaderText="Address" HeaderStyle-CssClass="tableHeader" ItemStyle-CssClass="tableBody"></asp:BoundColumn><asp:BoundColumn DataField="addressType" ItemStyle-HorizontalAlign="center" HeaderStyle-HorizontalAlign="center" HeaderText="Address<br>Type" HeaderStyle-CssClass="tableHeader" ItemStyle-CssClass="tableBody"></asp:BoundColumn><asp:BoundColumn DataField="connectionType" ItemStyle-HorizontalAlign="center" HeaderStyle-HorizontalAlign="center" HeaderText="Connection<br>Type" HeaderStyle-CssClass="tableHeader" ItemStyle-CssClass="tableBody"></asp:BoundColumn><asp:BoundColumn DataField="MCUName" ItemStyle-HorizontalAlign="center" HeaderStyle-HorizontalAlign="Center" HeaderText="MCU" HeaderStyle-CssClass="tableHeader" ItemStyle-CssClass="tableBody"></asp:BoundColumn><asp:BoundColumn DataField="BridgeAddress" ItemStyle-HorizontalAlign="center" HeaderStyle-HorizontalAlign="center" HeaderText="MCU<br>Address" HeaderStyle-CssClass="tableHeader" ItemStyle-CssClass="tableBody"></asp:BoundColumn><asp:BoundColumn DataField="BridgeAddressType" ItemStyle-HorizontalAlign="center" HeaderStyle-HorizontalAlign="center" HeaderText="MCU Address<br>Type" HeaderStyle-CssClass="tableHeader" ItemStyle-CssClass="tableBody"></asp:BoundColumn><asp:BoundColumn DataField="DefaultProtocol" ItemStyle-HorizontalAlign="center" HeaderStyle-HorizontalAlign="center" HeaderText="Protocol" HeaderStyle-CssClass="tableHeader" ItemStyle-CssClass="tableBody"></asp:BoundColumn><asp:BoundColumn DataField="EptOnlineStatus" ItemStyle-HorizontalAlign="center" HeaderStyle-HorizontalAlign="Center" HeaderText="EM7 Online<br> Status" HeaderStyle-CssClass="tableHeader" ItemStyle-CssClass="tableBody"></asp:BoundColumn><%--FB 2501 EM7--%> <asp:BoundColumn DataField="BridgeProfileName" ItemStyle-HorizontalAlign="center" HeaderStyle-HorizontalAlign="Center"  
                                                                            HeaderText="MCU<br> Profile" HeaderStyle-CssClass="tableHeader" ItemStyle-CssClass="tableBody"> <%--ZD 100298--%>
                                                                        </asp:BoundColumn> <%--FB 2839--%><asp:TemplateColumn HeaderText="Display Layout" ItemStyle-HorizontalAlign="center" HeaderStyle-HorizontalAlign="center" Visible="false" HeaderStyle-CssClass="tableHeader">
                                                                <ItemTemplate>
                                                                  <asp:Label ID="lblisTelepresence" style="visibility:hidden" Text='<%# DataBinder.Eval(Container, "DataItem.IsTelepresence") %>' runat="server"></asp:Label><%--FB 2400--%>                                                                
                                                                    <asp:Image ID="imgVideoLayout" runat="server" Width="30" Height="30" ImageUrl='<%# DataBinder.Eval(Container, "DataItem.ImageURL") %>' Visible='<%# DataBinder.Eval(Container, "DataItem.type").ToString().Trim() != "2" %>' />
                                                                    <asp:Button CssClass="altShortBlueButtonFormat" ID="btnChangeEndpointLayout"  runat="server" Text="Change"/>  <%-- Visible='<%# DataBinder.Eval(Container, "DataItem.type").ToString().Trim() != "2" %>'--%>
                                                                </ItemTemplate>
                                                            </asp:TemplateColumn><asp:TemplateColumn ItemStyle-HorizontalAlign="center" HeaderStyle-Width="100" ItemStyle-Width="100" HeaderStyle-CssClass="tableHeader" HeaderText="Actions (Modification are ONLY for this instance)" HeaderStyle-HorizontalAlign="center" FooterStyle-Horizontalalign="left">
                                                                <ItemTemplate>
                                                                    <table cellspacing="5" width="100%" border="0">
                                                                        <tr width="100%" align="center">
                                                                        <%--  Code Added For FB 1367- Nathira - Start--%><%--FB 3055 Start--%>
                                                                            <td><asp:LinkButton ID="btnCon"  CommandName="Update" Text="Connect" runat="server" Enabled='<%# (Request.QueryString["t"] != "hf") %>'></asp:LinkButton></td>                                                                       
                                                                            <%--  Code Added For FB 1367-  Nathira - End--%>
                                                                            <td><asp:LinkButton ID="btnEdit" Text="Edit" CommandName="Edit" runat="server" Enabled='<%# (Request.QueryString["t"] != "hf") %>'></asp:LinkButton></td>
                                                                            <td><asp:LinkButton ID="btnMute" Text="Mute" Visible="false" CommandName="Cancel" runat="server"></asp:LinkButton></td>
                                                                            <td><asp:LinkButton ID="btnDelete" Text="Delete" CommandName="Delete" runat="server" Enabled='<%# (Request.QueryString["t"] != "hf") %>'></asp:LinkButton></td>
                                                                        </tr>
                                                                    </table>
                                                                </ItemTemplate>
                                                                <FooterTemplate>
                                                                    <asp:Button ID="Button1" CssClass="altLongBlueButtonFormat" Text="Add New Endpoint" runat="server" OnClick="AddNewEndpoint" Enabled='<%# (Request.QueryString["t"] != "hf") %>' /><%--FB 3055 End--%>
                                                                </FooterTemplate>
                                                            </asp:TemplateColumn><%--FB 1650--%> <asp:BoundColumn DataField="CascadeLinkId" Visible="false"></asp:BoundColumn><asp:BoundColumn DataField="EndPointStatus" Visible="false"></asp:BoundColumn></Columns></asp:DataGrid></asp:TableCell></asp:TableRow></asp:Table><asp:DropDownList CssClass="altLong0SelectFormat" Visible="false" ID="lstAddressType" runat="server" DataTextField="Name" DataValueField="ID"></asp:DropDownList>
                                        <asp:Table runat="server" ID="tblP2PEndpoints" Width="100%" Visible="false">
                                            <asp:TableRow>
                                                <asp:TableCell HorizontalAlign="center" runat="server" Visible="false" ID="refreshCell"> <table width="90%" border="0"><tr><td align="left" class="blackblodtext">Broadcast Message </td><td><input type="text" class="altText" onkeyup="javascript:chkLimit(this,'50');" ID="TxtMessageBoxAll" runat="server" />&nbsp; <asp:Button ID="SendMsgAll" CssClass="altShortBlueButtonFormat" width="70px" Text="Send" runat="server"  OnClientClick="javascript:return fnchkValue('TxtMessageBoxAll');" OnClick="BroadCastP2PMsg"  /></td><td align="left" class="blackblodtext" nowrap><%--FB 1562--%> Extend End Time (in minutes) </td><td nowrap><asp:TextBox ID="txtPExtTime" CssClass="altText" runat="server" width="50px" ValidationGroup="SubmitTime" ></asp:TextBox>&nbsp; <asp:Button ID="btnPExtEndtime" CssClass="altShortBlueButtonFormat" width="70px" OnClick="ExtendEndtime" ValidationGroup="SubmitTime" Text="Submit" runat="server" OnClientClick="javascript:return fnValidateEndTime('p');" /><br /><asp:Label ID="LblPExtTimeMsg" style="visibility:hidden" Text="" ForeColor="red"  runat="server"></asp:Label><asp:RegularExpressionValidator ID="RegPExtTime" ValidationGroup="SubmitTime" ControlToValidate="txtPExtTime" ErrorMessage="<br>Numeric Values only" ValidationExpression="\d+" runat="server" Display="dynamic"></asp:RegularExpressionValidator><asp:RangeValidator ID="RangePExtTime" ValidationGroup="SubmitTime" ControlToValidate="txtPExtTime" ErrorMessage="<br>Range: 0 - 360 only" MinimumValue="0" MaximumValue="360" Type="Integer" runat="server" Display="dynamic"></asp:RangeValidator></td><td align="left" class="blackblodtext">Auto Refresh (30 seconds) </td><td><asp:CheckBox ID="P2pAutoRef" runat="server" Checked="true" onclick="javascript:UpdateP2PEndpointStatus();" /></td></tr></table></asp:TableCell></asp:TableRow><asp:TableRow>
                                                 <asp:TableCell HorizontalAlign="center"> <%--Code Modified For FB 1422 - To display Point to point End Point Details - Start--%> <asp:DataGrid runat="server" EnableViewState="true" ID="dgP2PEndpoints" AutoGenerateColumns="false" OnCancelCommand="SendP2PMessage" OnItemDataBound="InitializeP2PEndpoints"
                                                          style="border-collapse:separate" CellSpacing="0" CellPadding="4" GridLines="None" BorderColor="blue" BorderStyle="solid" BorderWidth="1"  Width="90%" OnUpdateCommand="ConnectP2PEndpoint" ><%--Edited for FF--%> <%-- Window Dressing start--%> <SelectedItemStyle CssClass="tableBody" Font-Bold="True"/><AlternatingItemStyle CssClass="tableBody" /><ItemStyle CssClass="tableBody"  /><HeaderStyle CssClass="tableHeader" Height="30px" /><EditItemStyle CssClass="tableBody" /><Columns><asp:BoundColumn DataField="ID" Visible="false"></asp:BoundColumn><asp:BoundColumn DataField="type" Visible="false"></asp:BoundColumn><asp:BoundColumn DataField="endpointID" Visible="false"></asp:BoundColumn><asp:BoundColumn DataField="name" HeaderStyle-CssClass="tableHeader" Visible="true" HeaderText="Room/Attendee<br>Name"></asp:BoundColumn><asp:BoundColumn DataField="EndpointName" HeaderStyle-CssClass="tableHeader" Visible="true" HeaderText="Endpoint<br>Name"></asp:BoundColumn><asp:BoundColumn DataField="address" HeaderStyle-CssClass="tableHeader" Visible="false"></asp:BoundColumn><asp:TemplateColumn ItemStyle-HorizontalAlign="center" HeaderStyle-Width="100" ItemStyle-Width="100" HeaderStyle-CssClass="tableHeader" HeaderText="Address" HeaderStyle-HorizontalAlign="center" FooterStyle-Horizontalalign="left">
                                                                <ItemTemplate>
                                                                    <asp:HyperLink style="cursor:hand;"  ID="EptWebsite" runat="server"></asp:HyperLink> 
                                                                </ItemTemplate>                                                               
                                                            </asp:TemplateColumn><asp:BoundColumn DataField="addressType" HeaderStyle-CssClass="tableHeader" Visible="true" HeaderText="Address<br>Type"></asp:BoundColumn><asp:BoundColumn DataField="DefaultProtocol" HeaderStyle-CssClass="tableHeader"  Visible="true" HeaderText="Protocol"></asp:BoundColumn><asp:BoundColumn DataField="Connect2" HeaderStyle-CssClass="tableHeader"  Visible="true" HeaderText="Caller/Callee"></asp:BoundColumn><asp:BoundColumn ItemStyle-HorizontalAlign="center" HeaderStyle-HorizontalAlign="center" HeaderText="Endpoint Status" HeaderStyle-CssClass="tableHeader"></asp:BoundColumn><%-- Window Dressing end--%> <asp:TemplateColumn ItemStyle-HorizontalAlign="center" HeaderStyle-Width="170px" ItemStyle-Width="100" HeaderStyle-CssClass="tableHeader" HeaderText="Actions" HeaderStyle-HorizontalAlign="center" FooterStyle-Horizontalalign="left">
                                                                <ItemTemplate>
                                                                    <table cellspacing="5" width="160px" border="0">
                                                                        <tr width="100%" align="center">
                                                                        <td><asp:LinkButton ID="btnConP2P" CommandName="Update" Text="Connect" runat="server"></asp:LinkButton></td>                                                                             
                                                                        <td><asp:HyperLink ID="btnMessage" style="cursor:hand;" Text="Send Message" runat="server"></asp:HyperLink></td> 
                                                                        <td><asp:HyperLink ID="EptMonitor" style="cursor:hand;" Text="Monitor" runat="server"></asp:HyperLink></td> 
                                                                      </tr>
                                                                        <tr  id="Messagediv" runat="server" style="display:none;z-index:999;cursor:hand" >
                                                                        <td colspan="2" width="120px">
                                                                            <div>
                                                                                <table cellspacing="5"  border="0">
                                                                                    <tr width="100%" align="center">
                                                                                    <td>
                                                                                        <input type="text" Height="35px" onkeyup="javascript:chkLimit(this,'50');" ID="TxtMessageBox" runat="server" />
                                                                                        <asp:Button ID="SendMsg" CssClass="altShortBlueButtonFormat" Text="Send" runat="server"  CommandName="Cancel" />
                                                                                        <input type="button" id="btnClose" runat="server" onclick="javascript:fnOpenMsg('0')" class="altMedium0BlueButtonFormat" value="Cancel"> <%--FB 1562--%> 
                                                                                    </td>
                                                                                    </tr>
                                                                                    </table>
                                                                        </div>
                                                                       </td>                                                                      </tr>
                                                                    </table>
                                                                </ItemTemplate>                                                               
                                                            </asp:TemplateColumn><%--Blue Status Project START--%> <asp:BoundColumn DataField="EndPointStatus" Visible="false"></asp:BoundColumn><asp:BoundColumn DataField="remoteEndpoint" Visible="false"></asp:BoundColumn><%--Blue Status Project End--%> <asp:BoundColumn DataField="EptOnlineStatus" HeaderStyle-CssClass="tableHeader" Visible="true" HeaderText="EM7 Online<br>Status"></asp:BoundColumn></Columns></asp:DataGrid><%--Code Modified Fo FB 1422 End--%> </asp:TableCell></asp:TableRow></asp:Table><h5>Alerts</h5><asp:Table runat="server" ID="tblNoEndpoints" Visible="false">
                                            <asp:TableRow>
                                                <asp:TableCell CssClass="lblError">No Endpoints found.</asp:TableCell></asp:TableRow><asp:TableRow>
                                                <asp:TableCell> <asp:Button ID="btnAddNewEndpoint" CssClass="altLongBlueButtonFormat" Text="Add New Endpoint" runat="server" OnClick="AddNewEndpoint" /></asp:TableCell></asp:TableRow></asp:Table><asp:Table ID="tblAlerts" Visible="false" runat="server" Width="100%">
                                            <asp:TableRow>
                                                <asp:TableCell HorizontalAlign="center"> <asp:DataGrid runat="server" EnableViewState="true" ID="dgAlerts" AutoGenerateColumns="false"
                                                          style="border-collapse:separate" CellSpacing="0" CellPadding="4" GridLines="None" BorderColor="blue" BorderStyle="solid" BorderWidth="1"  Width="90%"><%--Edited for FF--%> <%--Window Dressing--%> <SelectedItemStyle CssClass="tableBody" Font-Bold="True" /><AlternatingItemStyle CssClass="tableBody" /><ItemStyle CssClass="tableBody"  /><HeaderStyle CssClass="tableHeader" Height="30px" /><%--Window Dressing--%> <EditItemStyle CssClass="tableBody" /><Columns><asp:BoundColumn DataField="AlertID" Visible="false"></asp:BoundColumn><asp:BoundColumn DataField="Message" HeaderStyle-CssClass="tableHeader" Visible="true" HeaderText="Message"></asp:BoundColumn><asp:BoundColumn DataField="Timestamp" HeaderStyle-CssClass="tableHeader" Visible="true" HeaderText="Time Stamp"></asp:BoundColumn></Columns></asp:DataGrid><asp:Label ID="lblNoAlerts" Text="No Alerts found" CssClass="lblError" Visible="false" runat="server"></asp:Label></asp:TableCell></asp:TableRow></asp:Table><asp:TextBox ID="ImageFiles" runat="server" Text="" Width="0" Height="0" style="display:none"></asp:TextBox><%--Edited for FF--%> <asp:TextBox ID="ImageFilesBT" runat="server" Text="" Width="0" Height="0" style="display:none"></asp:TextBox><%--Edited for FF--%> <asp:TextBox ID="ImagesPath" runat="server" Text="" Width="0" Height="0" style="display:none"></asp:TextBox><%--Edited for FF--%> <asp:TextBox ID="txtSelectedImage" runat="server" Text="01" Width="0" Height="0" style="display:none"></asp:TextBox><%--Edited for FF--%> <asp:TextBox ID="txtSelectedImageEP" runat="server" Text="01" Width="0" Height="0" style="display:none"></asp:TextBox><%--Edited for FF--%> <asp:TextBox ID="txtTempImage" runat="server" Text="01" Width="0" Height="0" style="display:none"></asp:TextBox><%--Edited for FF--%></asp:Panel></asp:View><asp:View ID="ResourceView" runat="server">
                                    <asp:Panel ID="pnlResource" runat="server" Width="100%" Height="120%"> <%--FB 1982--%> <table width="100%">
                                            <tr  id="trAVCommonSettings" runat="server">
                                                <td align="center">
                                                    <asp:DataGrid ID="dgBridgeResources" GridLines="none" AutoGenerateColumns="false" runat="server" Width="90%" style="border-collapse:separate"> <%--Edited for FF--%> <AlternatingItemStyle CssClass="tableBody" />
                                                        <ItemStyle CssClass="tableBody"  />
                                                        <HeaderStyle CssClass="tableHeader" Height="30px" />
                                                        <%--Window Dressing--%> <EditItemStyle CssClass="tableBody" />
                                                      
                                                        <Columns>
                                                            <asp:BoundColumn DataField="BridgeID" Visible="false" ></asp:BoundColumn>
                                                            <asp:BoundColumn DataField="BridgeName" HeaderStyle-CssClass="tableHeader" HeaderText="Bridge<br>Name" ItemStyle-CssClass="tableBody" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left" ></asp:BoundColumn> <%-- FB 2050 --%> <asp:BoundColumn DataField="AudioPorts" HeaderStyle-CssClass="tableHeader" HeaderText="Sufficient<br>Audio-Only Ports" Visible="true" ItemStyle-CssClass="tableBody"></asp:BoundColumn>
                                                            <asp:BoundColumn DataField="VideoPorts" HeaderStyle-CssClass="tableHeader" HeaderText="Sufficient<br>Audio/Video Ports" Visible="true" ItemStyle-CssClass="tableBody"></asp:BoundColumn>
                                                            <asp:BoundColumn DataField="CardsAvailable" HeaderStyle-CssClass="tableHeader" HeaderText="Sufficient<br> Ports" Visible="false" ItemStyle-CssClass="tableBody"></asp:BoundColumn>
                                                            <asp:BoundColumn DataField="Description" HeaderStyle-CssClass="tableHeader" HeaderText="Description" Visible="true" ItemStyle-CssClass="tableBody"></asp:BoundColumn>
                                                        </Columns>
                                                    </asp:DataGrid>
                                                    <asp:DropDownList ID="lstBridges" runat="server" DataTextField="BridgeName" DataValueField="BridgeID" Visible="false"></asp:DropDownList>  <%-- SelectedValue='<%# DataBinder.Eval(Container, "DataItem.BridgeID") %>'--%> <asp:DropDownList runat="server" ID="lstBridgeCountV" DataTextField="BridgeID" DataValueField="BridgeCount" Visible="false"></asp:DropDownList>
                                                    <asp:DropDownList runat="server" ID="lstBridgeCountA" DataTextField="BridgeID" DataValueField="BridgeCount" Visible="false"></asp:DropDownList>
                                                </td>
                                            </tr>
                                            <tr    id="trp2pLinerate" runat="server" style="display:none;">
                                                
                                            <td align="left" valign="top">
                                            <span class="blackblodtext">Maximum Line Rate:</span> <asp:Label ID="LblLineRate" runat="server" Font-Bold="False"></asp:Label></td><%--FB 1982--%> </tr></table></asp:Panel></asp:View></asp:MultiView></div></td></tr><tr>
                <td colspan="3" align="center">
                 <asp:Table ID="tblAV" runat="server" Visible="true" Width="90%">
                    <asp:TableRow>
                        <asp:TableCell>
                            <table width="100%" border="0" cellpadding="0" cellspacing="0">
                            <tr>
                                <td align="left" colspan="4" class="subtitleblueblodtext" valign="top">
                                <%--Edited for FB 1428--%>
                                <%if(Session["systemTimezone"] !=null)%>
                                    <img border="0" id='Img_AV' src="image/loc/nolines_minus.gif" onclick="ShowHideRow('AV', this, false)" /><%if (Session["systemTimezone"] == "MOJ"){%>Audio & Video Hearing Parameters<%}else{ %>Audio & Video Conferencing Parameters<%} %></td>
                            </tr>
                            <tr id="tr_AV" align="center">
                                <td align="center" style="font-weight: bold;" valign="top" colspan="4">
                                    <table width="90%" cellspacing="1" cellpadding="1"><%--Modification for FB 1982 - start--%>
                                        <tr>
                                            <td class="blackblodtext" width="25%" align="left">Restrict Network Access to</td>
                                            <td style="width:1px"><b>:</b>&nbsp;</td>
                                            <td align="left">
                                                <asp:Label ID="lblNWAccess" runat="server" Font-Bold="False"></asp:Label></td>
                                            <td width="25%" class="blackblodtext" align="left">Restrict Usage to</td>
                                            <td style="width:1px"><b>:</b>&nbsp;</td>
                                            <td align="left" width="25%">
                                                <asp:Label ID="lblRestrictUsage" runat="server" Font-Bold="False"></asp:Label></td>
                                        </tr>
                                        <tr>
                                            <td width="25%" align="left">
                                                <span class="blackblodtext">Maximum Video Ports</span></td>
                                            <td style="width:1px"><b>:</b>&nbsp;</td>
                                            <td align="left">
                                                <asp:Label ID="lblMaxVideoPorts" runat="server" Font-Bold="False"></asp:Label></td>
                                            <td width="25%" align="left">
                                                <span class="blackblodtext">Maximum Audio Ports</span></td>
                                            <td style="width:1px"><b>:</b>&nbsp;</td>
                                            <td align="left" width="25%">
                                                <asp:Label ID="lblMaxAudioPorts" runat="server" Font-Bold="False"></asp:Label></td>
                                        </tr>
                                        <tr>
                                            <td style="height: 18px" width="25%" align="left">
                                                <span class="blackblodtext">Video Codecs</span></td>
                                            <td style="width:1px"><b>:</b>&nbsp;</td>
                                            <td align="left" style="height: 18px">
                                                <asp:Label ID="lblVideoCodecs" runat="server" Font-Bold="False"></asp:Label></td>
                                            <td style="height: 18px" width="25%" align="left">
                                                <span class="blackblodtext">Audio Codecs</span></td>
                                            <td style="width:1px"><b>:</b>&nbsp;</td>
                                            <td align="left" style="height: 18px" width="25%">
                                                <asp:Label ID="lblAudioCodecs" runat="server" Font-Bold="False"></asp:Label></td>
                                        </tr>
                                        <tr>
                                            <td width="25%" style="height: 18px" align="left">
                                                <span class="blackblodtext">Dual Stream Mode</span></td>
                                            <td style="width:1px"><b>:</b>&nbsp;</td>
                                            <td align="left" style="height: 18px">
                                                <asp:Label ID="lblDualStreamMode" runat="server" Font-Bold="False"></asp:Label></td>
                                            <td width="25%" style="height: 18px" align="left">
                                                <span class="blackblodtext">Password</span></td>
                                            <td style="width:1px"><b>:</b>&nbsp;</td>
                                            <td align="left" width="25%" style="height: 18px">
                                                <asp:Label ID="lblAVPassword" runat="server" Font-Bold="False" Text="None"></asp:Label></td>
                                        </tr>
                                        <tr>
                                            <td style="height: 18px" width="25%" align="left">
                                                <span class="blackblodtext">Encryption</span></td>
                                            <td style="width:1px"><b>:</b>&nbsp;</td>
                                            <td align="left" rowspan="1">
                                                <asp:Label ID="lblEncryption" runat="server" Font-Bold="False"></asp:Label></td>
                                            <td style="height: 18px" width="25%" align="left">
                                                <%--added for FB 1428 Start--%>
                                        <%if (Application["Client"] == "MOJ")
                                          {%>
                                        <span class="blackblodtext">Hearing on Port</span></td>
                                    <%}
                                      else
                                      {%>
                                    <span class="blackblodtext">Conference on Port</span>
                        </td>
                        <%} %>
                        <%--added for FB 1428 End--%>
                                        <td style="width:1px"><b>:</b>&nbsp;</td>
                                            <td align="left" rowspan="1" width="25%">
                                                <asp:Label ID="lblConfOnPort" runat="server" Font-Bold="False"></asp:Label></td>
                                        </tr>
                                        <tr>
                                            <td width="25%" valign="top" align="left">
                                                <span class="blackblodtext">Video Display</span></td>
                                            <td style="width:1px;vertical-align:top"><b>:</b>&nbsp;</td>
                                            <td align="left" valign="top">
                                                <asp:Image ID="imgVideoDisplay" runat="server" ImageUrl="image/displaylayout/01.gif" Height="30" Width="30" /></td>
                                            <td style="height: 18px" width="25%" valign="top" align="left">
                                                <span class="blackblodtext">Maximum Line Rate</span></td>
                                            <td style="width:1px;vertical-align:top"><b>:</b>&nbsp;</td>
                                            <td align="left" rowspan="1" valign="top">
                                                <asp:Label ID="lblMaxLineRate" runat="server" Font-Bold="False"></asp:Label></td>
                                        </tr>
                                        <tr>
                                            <td width="25%" valign="top" align="left">
                                                <span class="blackblodtext">Lecture Mode</span>
                                            </td>
                                            <td style="width:1px"><b>:</b>&nbsp;</td>
                                            <td align="left" valign="top">
                                                <asp:Label ID="lblLectureMode" runat="server" Font-Bold="False"></asp:Label>
                                            </td>
                                            <td width="25%" valign="top" align="left">
                                                <span class="blackblodtext">Single Dial-in</span>
                                            </td>
                                            <td style="width:1px"><b>:</b>&nbsp;</td>
                                            <td align="left" valign="top"><asp:Label ID="lblSingleDialin" runat="server" Font-Bold="False"></asp:Label></td><%--FB 2377--%>
                                        </tr>
                                        <%-- FB 2501 FECC Starts --%>
                                        <tr>
                                            <td colspan="3"></td>
                                            <td align="left"><span class="blackblodtext">FECC</span></td>
                                            <td style="width:1px"><b>:</b>&nbsp;</td>
                                            <td align="left"><asp:Label ID="lblFECCMode" runat="server" Font-Bold="False"></asp:Label></td>
                                        </tr>
                                        <%-- FB 2501 FECC Ends --%>
                                    </table>
                                    <%--Modification for FB 1982 - end--%>
                                    </asp:TableCell></asp:TableRow></asp:Table></td></tr></table></td></tr><%--FB 2359 Start--%> <%--FB 2377 Starts--%> 
                                    <tr id="trEnableConcierge" runat="server">
                <td colspan="3" align="center">
                    <asp:Table ID="tblConcierge" runat="server" Visible="true" Width="90%">
                        <asp:TableRow>
                            <asp:TableCell>
                                <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                    <tr>
                                        <td align="left" colspan="4" class="subtitleblueblodtext" valign="top">
                                            <%if(Session["systemTimezone"] !=null)%>
                                            <img border="0" id='Img_CON' src="image/loc/nolines_minus.gif" alt="" onclick="ShowHideRow('CON', this, false)" />Conference Support
                                        </td><%--FB 3023--%>
                                    </tr>
                                    <%--FB 2632 - Start--%>
                                    <tr id="tr_CON" align="center">
                                        <td align="center" valign="top" colspan="4">
                                            <table width="90%" cellspacing="1" cellpadding="1">
                                               <tr id="trOnSiteAVSupport" runat="server" ><%--FB 2670--%>
                                                    <td style="height: 18px" width="25%" align="left" nowrap="nowrap">
                                                        <span class="blackblodtext">On-Site A/V Support</span>
                                                    </td>
                                                    <td style="width:1px"><b>:</b>&nbsp;</td>
                                                    <td align="left" style="height: 18px">                                                        
                                                        <label id="lblOnsiteAV" style="font-weight: normal;" runat="server"></label>
                                                    </td>
                                                </tr>
                                                <tr id="trMeetandGreet" runat="server" nowrap="nowrap"><%--FB 2670--%>
                                                    <td class="blackblodtext" width="25%" align="left">Meet and Greet</td>
                                                    <td style="width:1px"><b>:</b>&nbsp;</td>
                                                    <td align="left">                                                        
                                                        <label id="lblmeet" style="font-weight: normal;" runat="server"></label>
                                                    </td>
                                                </tr>
                                                <tr id="trConciergeMonitoring" runat="server" nowrap="nowrap"><%--FB 2670--%>
                                                    <td style="height: 18px" width="25%" align="left">
                                                        <span class="blackblodtext">Call Monitoring</span> <%--FB 3023--%>
                                                    </td>
                                                    <td style="width:1px"><b>:</b>&nbsp;</td>
                                                    <td align="left" style="height: 18px">
                                                        <label id="lblConciergeMonitoring" runat="server" style="font-weight: normal;"></label>                                                        
                                                    </td>
                                                </tr>
                                                <tr id="trDedicatedVNOC" runat="server" >
                                                    <td width="25%" align="left"  nowrap="nowrap">
                                                        <span class="blackblodtext">Dedicated VNOC Operator</span>
                                                    </td>
                                                    <td style="width:1px"><b>:</b>&nbsp;</td>
                                                    <td align="left">
                                                        <label id="lblVNOC" runat="server" style="font-weight: normal;"></label>                                                        
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <%--FB 2632 - End --%>
                                    <%--FB 2377 - End--%>
                                </table>
                            </asp:TableCell></asp:TableRow></asp:Table></td></tr><%--FB 2359 End--%> <%--Custom Attribute Fixes start --%> 
                            <tr id="trEnableCustom" runat="server" >
                <td align="center" colspan="3">
                     <asp:Table runat="server" ID="tblCustOpt" Width="100%" CellPadding="0" CellSpacing="0">
                          <asp:TableRow>
                              <asp:TableCell HorizontalAlign="center" >
                                   <table width="90%" border="0" cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td align="left" colspan="3" class="subtitleblueblodtext" valign="top">
                                                <img border="0" id='Img_CustOpt' src="image/loc/nolines_minus.gif" onclick="ShowHideRow('CustOpt', this, false)" />Custom Options
                                                <asp:Label ID="lbl_CustOpt" runat="server" Font-Bold="True"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr id="tr_CustOpt">
                                            <td align="center" style="font-weight:normal;" valign="top" colspan="3" id="tdCustOpt" runat="server">
                                                
                                                <asp:Table ID="lblNoCustOption" Visible="false" runat="server" Width="90%"> 
                                                    <asp:TableRow CssClass="tableHeader">
                                                        <asp:TableCell Text="Message" CssClass="tableHeader" HorizontalAlign="center"></asp:TableCell>
                                                    </asp:TableRow>
                                                    <asp:TableRow Height="30px">
                                                        <asp:TableCell Text="No Custom Options found." CssClass="lblError" ></asp:TableCell>
                                                    </asp:TableRow>
                                                </asp:Table>
                                            </td>
                                        </tr>
                                    </table>
                              </asp:TableCell></asp:TableRow></asp:Table></td></tr><%--Custom Attribute Fixes end --%> <tr>
                <td align="center" colspan="3">
                            <asp:Table runat="server" ID="tblAVWO" Width="100%" CellPadding="0" CellSpacing="0">
                                <asp:TableRow>
                                    <asp:TableCell HorizontalAlign="center">
                    <table width="90%" border="0" cellpadding="0" cellspacing="0" align="center">
                        <tr>
                            <td align="left" colspan="6" class="subtitleblueblodtext" valign="top">
                                <img border="0" id='Img_AVWO' src="image/loc/nolines_minus.gif" onclick="ShowHideRow('AVWO', this,false)" />Audiovisual Work Orders<%-- FB 2570 --%>
                                <asp:Label ID="lblAVWO" runat="server" Font-Bold="True"></asp:Label></td>
                        </tr>
                        <tr id="tr_AVWO">
                            <td align="left" valign="top" colspan="6">
                                                                    <table width="100%" cellspacing="1" cellpadding="1">
                                <tr><td width="100%" align="center">
                    <asp:DataGrid ID="dgAVWO" runat="server" AllowSorting="false" AutoGenerateColumns="false" Width="90%" OnItemDataBound="AV_ItemDataBound"  OnEditCommand="SendReminderToHost" style="border-collapse:Collapse;" BorderColor="Black" BorderWidth="1px"> <%--Edited for FF FB 2288--%>
                        <AlternatingItemStyle CssClass="tableBody" />
                        <ItemStyle CssClass="tableBody"  />
                        <HeaderStyle CssClass="tableHeader"/>
                        <Columns>
                            <asp:BoundColumn DataField="ID" Visible="false" ItemStyle-CssClass="tableBody"  ItemStyle-BorderColor="Black" ItemStyle-BorderWidth="1px"></asp:BoundColumn>
                            <asp:BoundColumn DataField="Name" HeaderText="Name" ItemStyle-CssClass="tableBody" ItemStyle-HorizontalAlign="Left"  ItemStyle-BorderColor="Black" ItemStyle-BorderWidth="1px"><HeaderStyle CssClass="tableHeader" /></asp:BoundColumn><%--FB 2508--%>
                            <asp:BoundColumn DataField="StartByDate" HeaderText="Start<br>Date" ItemStyle-CssClass="tableBody"  ItemStyle-BorderColor="Black" ItemStyle-BorderWidth="1px"><HeaderStyle CssClass="tableHeader" /></asp:BoundColumn>
                            <asp:BoundColumn DataField="StartByTime" HeaderText="Time" ItemStyle-CssClass="tableBody"  ItemStyle-BorderColor="Black" ItemStyle-BorderWidth="1px"><HeaderStyle CssClass="tableHeader" /></asp:BoundColumn>
                            <asp:BoundColumn DataField="CompletedByDate" HeaderText="Completed<br>Date" ItemStyle-CssClass="tableBody"  ItemStyle-BorderColor="Black" ItemStyle-BorderWidth="1px"><HeaderStyle CssClass="tableHeader" /></asp:BoundColumn>
                            <asp:BoundColumn DataField="CompletedByTime" HeaderText="Time" ItemStyle-CssClass="tableBody"  ItemStyle-BorderColor="Black" ItemStyle-BorderWidth="1px"><HeaderStyle CssClass="tableHeader" /></asp:BoundColumn>
                            <asp:BoundColumn DataField="RoomName" HeaderText="Assigned to Rooms" ItemStyle-CssClass="tableBody"  ItemStyle-BorderColor="Black" ItemStyle-BorderWidth="1px"><HeaderStyle CssClass="tableHeader" /></asp:BoundColumn>
                            <asp:BoundColumn DataField="AssignedToID" Visible="true" HeaderText="Person-in-charge" ItemStyle-CssClass="tableBody"  ItemStyle-BorderColor="Black" ItemStyle-BorderWidth="1px"><HeaderStyle CssClass="tableHeader" /></asp:BoundColumn>
                            <asp:BoundColumn DataField="Status" HeaderText="Status" ItemStyle-CssClass="tableBody"  ItemStyle-BorderColor="Black" ItemStyle-BorderWidth="1px"><HeaderStyle CssClass="tableHeader" /></asp:BoundColumn>
                            <asp:BoundColumn DataField="Comments" HeaderText="Comments" ItemStyle-CssClass="tableBody"  ItemStyle-BorderColor="Black" ItemStyle-BorderWidth="1px"><HeaderStyle CssClass="tableHeader" /></asp:BoundColumn>
                            <asp:TemplateColumn HeaderText="Details" HeaderStyle-CssClass="tableHeader"  ItemStyle-BorderColor="Black" ItemStyle-BorderWidth="1px">
                                <ItemTemplate>
                                    <a href="#" onclick="ViewDetails('<%#DataBinder.Eval(Container, "DataItem.ID") %>', '<%#DataBinder.Eval(Container, "DataItem.ConfID") %>')">View</a>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:EditCommandColumn ButtonType="LinkButton" EditText="Send Reminder" HeaderText="Action" HeaderStyle-CssClass="tableHeader" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="center" ItemStyle-CssClass="Link"  ItemStyle-BorderColor="Black" ItemStyle-BorderWidth="1px" ></asp:EditCommandColumn>
                            <asp:TemplateColumn Visible="true">
                                <ItemTemplate>
                                    <asp:DataGrid ID="itemsGrid"  runat="server" AutoGenerateColumns="False" GridLines="None" Width="95%" Visible="true" style="border-collapse:collapse" BorderColor="Black" BorderWidth="1px"> <%--Edited for FF--%>
                                      <%--Window Dressing start--%>
                                    <FooterStyle  CssClass="tableBody"  Font-Bold="True"  />
                                    <SelectedItemStyle CssClass="tableBody" Font-Bold="True" />
                                    <EditItemStyle  CssClass="tableBody" />
                                    <AlternatingItemStyle  CssClass="tableBody" />
                                    <ItemStyle  CssClass="tableBody" BorderColor="Black" BorderWidth="1px"  />
                                    <HeaderStyle CssClass="tableHeader" Height="30px" />
                                    <Columns>
                                        <asp:BoundColumn DataField="ID" Visible="False" ItemStyle-CssClass="tableBody"  ItemStyle-BorderColor="Black" ItemStyle-BorderWidth="1px"><HeaderStyle CssClass="tableBody" Font-Bold="true" Height="30px" /></asp:BoundColumn>
                                        <asp:BoundColumn DataField="Name" HeaderText="Name" ItemStyle-CssClass="tableBody"  ItemStyle-BorderColor="Black" ItemStyle-BorderWidth="1px"><HeaderStyle CssClass="tableBody" Font-Bold="true" Height="30px" /></asp:BoundColumn>
                                        <asp:BoundColumn DataField="Price" HeaderText="Price($)" ItemStyle-CssClass="tableBody"  ItemStyle-BorderColor="Black" ItemStyle-BorderWidth="1px"><HeaderStyle CssClass="tableBody" Font-Bold="true" Height="30px" /></asp:BoundColumn>
                                        <asp:BoundColumn DataField="SerialNumber" HeaderText="Serial #" ItemStyle-CssClass="tableBody"  ItemStyle-BorderColor="Black" ItemStyle-BorderWidth="1px"><HeaderStyle CssClass="tableBody" Font-Bold="true" Height="30px" /></asp:BoundColumn>
                                        <asp:TemplateColumn HeaderText="Image">
                                            <HeaderStyle CssClass="tableBody" Font-Bold="true" Height="30px" />
                                            <ItemTemplate >
                                               <asp:Image runat="server" ID="imgItem" ImageUrl='<%# DataBinder.Eval(Container, "DataItem.Image") %>' Width="30" Height="30" onmouseover="javascript:ShowImage(this)" onmouseout="javascript:HideImage()" />
                                            </ItemTemplate>
                                        </asp:TemplateColumn> 
                                        <asp:BoundColumn DataField="Comments" HeaderText="Comments" ItemStyle-CssClass="tableBody"  ItemStyle-BorderColor="Black" ItemStyle-BorderWidth="1px"><HeaderStyle CssClass="tableBody" Font-Bold="true" Height="30px" /></asp:BoundColumn>
                                        <asp:BoundColumn DataField="ServiceCharge" HeaderText="Service<br>Charge($)" ItemStyle-CssClass="tableBody"  ItemStyle-BorderColor="Black" ItemStyle-BorderWidth="1px"><HeaderStyle Font-Bold="true" CssClass="tableBody" Height="30px" /></asp:BoundColumn>
                                        <asp:BoundColumn DataField="DeliveryCost" HeaderText="Delivery<br>Cost($)" ItemStyle-CssClass="tableBody"  ItemStyle-BorderColor="Black" ItemStyle-BorderWidth="1px"><HeaderStyle Font-Bold="true" CssClass="tableBody" Height="30px" /></asp:BoundColumn>
                                        <asp:TemplateColumn HeaderText="Requested Quantity" ItemStyle-CssClass="tableBody"  ItemStyle-BorderColor="Black" ItemStyle-BorderWidth="1px"><HeaderStyle CssClass="tableBody" Font-Bold="true" Height="30px" />
                                            <ItemTemplate>
                                                <asp:Label ID="lblReqQuantity" runat="server" Width="30px" Text='<%# DataBinder.Eval(Container, "DataItem.QuantityRequested") %>'></asp:Label>
                                             </ItemTemplate>
                                        </asp:TemplateColumn>
                                    <%--Window Dressing end--%>
                                    </Columns>
                                </asp:DataGrid>

                                </ItemTemplate>                               
                            </asp:TemplateColumn>
                        </Columns>
                    </asp:DataGrid>
                    <asp:Table ID="lblAVNoWorkOrder" Visible="false" runat="server" Width="90%" style="border-collapse:separate">  <%--Edited for FF--%>
                        <asp:TableRow CssClass="tableHeader">
                        <%--Window Dressing--%>
                            <asp:TableCell Text="Message" CssClass="tableHeader" HorizontalAlign="center"></asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow Height="30px">
                        <%--Window Dressing--%>
                            <asp:TableCell Text="No Work Orders found." CssClass="lblError" ></asp:TableCell>
                        </asp:TableRow>
                    </asp:Table>
                         </td></tr>    
                                </table>

                            </td>
                        </tr>
                    </table>
                                     </asp:TableCell></asp:TableRow></asp:Table></td></tr><tr>
                <td align="center" colspan="3"><center>
                              <asp:Table runat="server" ID="tblCATWO" Width="100%" CellPadding="0" CellSpacing="0">
                                <asp:TableRow>
                                    <asp:TableCell HorizontalAlign="center">
                   <table width="90%" border="0" cellpadding="0" cellspacing="0">
                        <tr>
                            <td align="left" colspan="6" class="subtitleblueblodtext" valign="top">
                                <img border="0" id='Img_CATWO' src="image/loc/nolines_minus.gif" onclick="ShowHideRow('CATWO', this, false)" />Catering Work Orders
                                <asp:Label ID="lblCATWO" runat="server" Font-Bold="True"></asp:Label></td>
                        </tr>
                        <tr id="tr_CATWO">
                            <td align="left" style="font-weight: bold;" valign="top" colspan="6">
                                <table width="100%" cellspacing="1" cellpadding="1">
                                <tr><td width="100%" align="center">
                    <asp:DataGrid ID="dgCATWO" runat="server" AutoGenerateColumns="False" OnSortCommand="SortGrid" AllowSorting="True" Width="90%" OnItemDataBound="CAT_ItemDataBound"  OnEditCommand="SendReminderToHost" style="border-collapse:collapse" BorderColor="Black" BorderWidth="1px"> <%--Edited for FF FB 2288 FB 2050--%>
                        <AlternatingItemStyle CssClass="tableBody" />
                        <ItemStyle CssClass="tableBody"  BorderColor="Black" BorderWidth="1px" />
                        <HeaderStyle CssClass="tableHeader" BorderColor="Black" BorderWidth="1px"/>
                        <Columns>
                            <asp:BoundColumn DataField="ID" Visible="false" ItemStyle-CssClass="tableBody"  ItemStyle-BorderColor="Black" ItemStyle-BorderWidth="1px"></asp:BoundColumn>
                            <asp:BoundColumn DataField="Name" HeaderText="Name" ItemStyle-CssClass="tableBody" ItemStyle-HorizontalAlign="Left"  ItemStyle-BorderColor="Black" ItemStyle-BorderWidth="1px"><HeaderStyle CssClass="tableHeader" /></asp:BoundColumn><%--FB 2508--%>
                            <asp:BoundColumn DataField="CompletedByDate" HeaderText="Deliver<br>by Date" ItemStyle-CssClass="tableBody"  ItemStyle-BorderColor="Black" ItemStyle-BorderWidth="1px"><HeaderStyle CssClass="tableHeader" /></asp:BoundColumn>
                            <asp:BoundColumn DataField="CompletedByTime" HeaderText="Deliver<br>by Time" ItemStyle-CssClass="tableBody"  ItemStyle-BorderColor="Black" ItemStyle-BorderWidth="1px"><HeaderStyle CssClass="tableHeader" /></asp:BoundColumn>
                            <asp:BoundColumn DataField="RoomName" HeaderText="Assigned to Rooms" ItemStyle-CssClass="tableBody"  ItemStyle-BorderColor="Black" ItemStyle-BorderWidth="1px"><HeaderStyle CssClass="tableHeader" /></asp:BoundColumn>
                            <asp:BoundColumn DataField="AssignedToID" Visible="false" HeaderText="Person-in-charge" ItemStyle-CssClass="tableBody"  ItemStyle-BorderColor="Black" ItemStyle-BorderWidth="1px"><HeaderStyle CssClass="tableHeader" /></asp:BoundColumn>
                            <asp:BoundColumn DataField="Status" HeaderText="Status" Visible="false" ItemStyle-CssClass="tableBody"  ItemStyle-BorderColor="Black" ItemStyle-BorderWidth="1px"><HeaderStyle CssClass="tableHeader" /></asp:BoundColumn>
                            <asp:BoundColumn DataField="Comments" HeaderText="Comments" Visible="false" ItemStyle-CssClass="tableBody"  ItemStyle-BorderColor="Black" ItemStyle-BorderWidth="1px"><HeaderStyle CssClass="tableHeader" /></asp:BoundColumn>
                            <asp:TemplateColumn HeaderText="Details" HeaderStyle-CssClass="tableHeader">
                                <ItemTemplate>
                                    <a href="#" onclick="ViewDetails('<%#DataBinder.Eval(Container, "DataItem.ID") %>', '<%#DataBinder.Eval(Container, "DataItem.ConfID") %>', '<%#DataBinder.Eval(Container, "DataItem.Type") %>')">View</a>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:EditCommandColumn ButtonType="LinkButton" EditText="Send Reminder" HeaderText="Action" HeaderStyle-CssClass="tableHeader" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="center" ItemStyle-CssClass="Link"  ItemStyle-BorderColor="Black" ItemStyle-BorderWidth="1px" ></asp:EditCommandColumn>
                            <asp:TemplateColumn Visible="true">
                                <ItemTemplate>
                                    <asp:DataGrid ID="itemsGrid"  runat="server" AutoGenerateColumns="False"
                                    GridLines="None" Width="95%" Visible="true" BorderColor="black" BorderStyle="Solid" BorderWidth="2" style="border-collapse:separate"> <%--Edited for FF--%>
                                      <%--Window Dressing start--%>
                                    <FooterStyle Font-Bold="True" />
                                    <SelectedItemStyle Font-Bold="True"/>
                                    <EditItemStyle />
                                    <AlternatingItemStyle />
                                    <ItemStyle  />
                                    <HeaderStyle  Height="30px" />
                                    <Columns>
                                        <asp:BoundColumn DataField="ID" Visible="False"  ItemStyle-BorderColor="Black" ItemStyle-BorderWidth="1px"><HeaderStyle  CssClass="tableHeader" Font-Bold="true" Height="30px" /></asp:BoundColumn>
                                        <asp:BoundColumn DataField="Name" HeaderText="Name"  ItemStyle-BorderColor="Black" ItemStyle-BorderWidth="1px"><HeaderStyle  CssClass="tableHeader" Font-Bold="true" Height="30px" /></asp:BoundColumn>
                                        <asp:BoundColumn DataField="Quantity" HeaderText="Quantity in hand"  ItemStyle-BorderColor="Black" ItemStyle-BorderWidth="1px"><HeaderStyle Font-Bold="true"  CssClass="tableHeader" Height="30px" /></asp:BoundColumn>
                                        <asp:TemplateColumn HeaderText="Requested Quantity"  ItemStyle-BorderColor="Black" ItemStyle-BorderWidth="1px"><HeaderStyle  CssClass="tableHeader" Font-Bold="true" Height="30px" />
                                            <ItemTemplate>
                                                <asp:Label ID="lblReqQuantity" runat="server" Width="30px" Text='<%# DataBinder.Eval(Container, "DataItem.QuantityRequested") %>'></asp:Label>
                                             </ItemTemplate>
                                        </asp:TemplateColumn>
                                    </Columns>
                                </asp:DataGrid>
                                    <%--Window Dressing end --%>
                                </ItemTemplate>                               
                            </asp:TemplateColumn>
                       </Columns>
                    </asp:DataGrid>
                    <asp:Table ID="lblCATNoWorkOrder" Visible="false" runat="server" Width="90%"> 
                        <asp:TableRow CssClass="tableHeader">
                            <asp:TableCell Text="Message" HorizontalAlign="center" CssClass="tableHeader"></asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow Height="30px">
                                    <%--Window Dressing --%>
                            <asp:TableCell Text="No Work Orders found." CssClass="lblError" ></asp:TableCell>
                        </asp:TableRow>
                    </asp:Table>
                         </td></tr>    
                                </table>
                            </td>
                        </tr>
                    </table>
                                                     </asp:TableCell></asp:TableRow></asp:Table></center></td></tr><tr>
                <td align="center" colspan="3">
                              <asp:Table runat="server" ID="tblHKWO" Width="100%" CellPadding="0" CellSpacing="0">
                                <asp:TableRow>
                                    <asp:TableCell HorizontalAlign="center">
                
                    <table width="90%" border="0" cellpadding="0" cellspacing="0">
                        <tr>
                            <td align="left" colspan="6" class="subtitleblueblodtext" valign="top">
                                <img border="0" id='Img_HKWO' src="image/loc/nolines_minus.gif" onclick="ShowHideRow('HKWO', this, false)" />Facility Work Orders <%-- FB 2570 --%>
                                <asp:Label ID="lblHKWO" runat="server" Font-Bold="True"></asp:Label></td>
                        </tr>
                        <tr id="tr_HKWO">
                            <td align="left" style="font-weight:normal;" valign="top" colspan="6">
                                <table width="100%" cellspacing="1" cellpadding="1">
                                <tr><td width="100%" align="center">
                                    <%--Window Dressing --%>
                    <asp:DataGrid ID="dgHKWO" runat="server" AutoGenerateColumns="False" CssClass="tableHeader" GridLines="Both" Width="90%" OnItemDataBound="HK_ItemDataBound"  OnEditCommand="SendReminderToHost" OnCancelCommand="SendReminderToHost" OnUpdateCommand="SendReminderToHost" style="border-collapse:collapse" Font-Bold="false"  BorderColor="Black" BorderWidth="1px" > <%--Editedfor FF--%> <%--FB 2181 FB 2288--%>
                        <AlternatingItemStyle CssClass="tableBody" BorderColor="Black" BorderWidth="1px"/>
                        <ItemStyle CssClass="tableBody" BorderColor="Black" BorderWidth="1px"  />
                        <HeaderStyle CssClass="tableHeader" BorderColor="Black" BorderWidth="1px"/>
                        <Columns>
                            <asp:BoundColumn DataField="ID" Visible="false" ItemStyle-CssClass="tableBody"></asp:BoundColumn>
                            <asp:BoundColumn DataField="Name" HeaderText="Name" ItemStyle-CssClass="tableBody" ItemStyle-HorizontalAlign="Left"><%--FB 2508--%>
                            <HeaderStyle CssClass="tableHeader" /></asp:BoundColumn>
                            <asp:BoundColumn DataField="CompletedByDate" HeaderText="Completed By: Date" ItemStyle-CssClass="tableBody"  ItemStyle-BorderColor="Black" ItemStyle-BorderWidth="1px"><HeaderStyle CssClass="tableHeader" /></asp:BoundColumn>
                            <asp:BoundColumn DataField="CompletedByTime" HeaderText="Time" ItemStyle-CssClass="tableBody"  ItemStyle-BorderColor="Black" ItemStyle-BorderWidth="1px"><HeaderStyle CssClass="tableHeader" /></asp:BoundColumn>
                            <asp:BoundColumn DataField="RoomName" HeaderText="Assigned to Rooms" ItemStyle-CssClass="tableBody"  ItemStyle-BorderColor="Black" ItemStyle-BorderWidth="1px"><HeaderStyle CssClass="tableHeader" /></asp:BoundColumn>
                            <asp:BoundColumn DataField="AssignedToID" Visible="true" HeaderText="Person-in-charge" ItemStyle-CssClass="tableBody"  ItemStyle-BorderColor="Black" ItemStyle-BorderWidth="1px"><HeaderStyle CssClass="tableHeader" /></asp:BoundColumn>
                            <asp:BoundColumn DataField="Status" HeaderText="Status" ItemStyle-CssClass="tableBody"  ItemStyle-BorderColor="Black" ItemStyle-BorderWidth="1px"><HeaderStyle CssClass="tableHeader" /></asp:BoundColumn>
                            <asp:BoundColumn DataField="Comments" HeaderText="Comments" ItemStyle-CssClass="tableBody" ItemStyle-BorderColor="Black" ItemStyle-BorderWidth="1px"><HeaderStyle CssClass="tableHeader" /></asp:BoundColumn>
                            <asp:TemplateColumn Visible="true">
                                <ItemTemplate>
                                    <asp:DataGrid ID="itemsGrid"  runat="server" AutoGenerateColumns="False"
                                    GridLines="None" Width="95%" Visible="true" BorderColor="black" BorderStyle="Solid" BorderWidth="1" style="border-collapse:separate"><%--Edited for FF--%>
                                    <%--Window Dressing start--%>
                                    <FooterStyle CssClass="tableBody" Font-Bold="True" />
                                    <SelectedItemStyle CssClass="tableBody" />
                                    <EditItemStyle CssClass="tableBody" />
                                    <AlternatingItemStyle CssClass="tableBody"/>
                                    <ItemStyle CssClass="tableBody"/>
                                    <HeaderStyle CssClass="tableHeader" Height="30px" />
                                    <Columns>
                                        <asp:BoundColumn DataField="ID" Visible="False"><HeaderStyle  CssClass="tableHeader" Font-Bold="true" Height="30px" /></asp:BoundColumn>
                                        <asp:BoundColumn DataField="Name" HeaderText="Task"><HeaderStyle  CssClass="tableHeader" Font-Bold="true" Height="30px" /></asp:BoundColumn>
                                        <asp:BoundColumn DataField="Price" HeaderText="Price($)"><HeaderStyle  CssClass="tableHeader" Font-Bold="true" Height="30px" /></asp:BoundColumn>
                                        <asp:BoundColumn DataField="SerialNumber" HeaderText="Serial #"><HeaderStyle  CssClass="tableHeader" Font-Bold="true" Height="30px" /></asp:BoundColumn>
                                        <asp:TemplateColumn HeaderText="Image">
                                            <HeaderStyle CssClass="tableHeader"  Font-Bold="true" Height="30px" />
                                            <ItemTemplate >
                                               <asp:Image runat="server" ID="imgItem" ImageUrl='<%# DataBinder.Eval(Container, "DataItem.Image") %>' onmouseover="javascript:ShowImage(this)" onmouseout="javascript:HideImage()" />
                                            </ItemTemplate>
                                        </asp:TemplateColumn> 
                                        <asp:BoundColumn DataField="Comments" HeaderText="Comments"><HeaderStyle CssClass="tableHeader" Font-Bold="true" Height="30px" /></asp:BoundColumn>
                                        <asp:BoundColumn DataField="Quantity" HeaderText="Quantity in hand"><HeaderStyle Font-Bold="true" CssClass="tableHeader" Height="30px" /></asp:BoundColumn>
                                        <asp:TemplateColumn HeaderText="Requested Quantity"><HeaderStyle Font-Bold="true" CssClass="tableHeader" Height="30px" />
                                            <ItemTemplate>
                                                <asp:Label ID="lblReqQuantity" runat="server" Width="30px" Text='<%# DataBinder.Eval(Container, "DataItem.QuantityRequested") %>'></asp:Label>
                                             </ItemTemplate>
                                        </asp:TemplateColumn>
                                    </Columns>
                                </asp:DataGrid>
                                    <%--Window Dressing end--%>

                                </ItemTemplate>                               
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="Details" HeaderStyle-CssClass="tableHeader"  ItemStyle-BorderColor="Black" ItemStyle-BorderWidth="1px">
                                <ItemTemplate>
                                    <a href="#" onclick="ViewDetails('<%#DataBinder.Eval(Container, "DataItem.ID") %>', '<%#DataBinder.Eval(Container, "DataItem.ConfID") %>')">View</a>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:EditCommandColumn ButtonType="LinkButton" EditText="Send Reminder" HeaderText="Action" HeaderStyle-CssClass="tableHeader" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="center" ItemStyle-CssClass="Link" >
                            </asp:EditCommandColumn>
                        </Columns>
                    </asp:DataGrid>
                    <asp:Table ID="lblHKNoWorkOrder" Visible="false" runat="server" Width="90%" style="border-collapse:separate">  <%--Edited for FF--%>
                        <asp:TableRow CssClass="tableHeader">
                            <%--Window Dressing --%>
                            <asp:TableCell Text="Message" CssClass="tableHeader" HorizontalAlign="center"></asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow Height="30px">
                            <%--Window Dressing --%>
                            <asp:TableCell Text="No Work Orders found." CssClass="lblError" ></asp:TableCell>
                        </asp:TableRow>
                    </asp:Table>
                         </td></tr>    
                                </table>
                            </td>
                        </tr>
                    </table>
                                  </asp:TableCell></asp:TableRow></asp:Table></td></tr></table></div></center><script language="javascript" type="text/javascript">
    function calculateRecurText()
    {
        if (document.getElementById("<%=Recur.ClientID %>").value != "" ) 
        {
            AnalyseRecurStr(document.getElementById("<%=Recur.ClientID %>").value);
            
            st = calStart(atint[1], atint[2], atint[3]);
            document.getElementById("<%=lblTimezone.ClientID%>").innerHTML = "";
            et = calEnd(st, parseInt(atint[4], 10));
            document.getElementById("<%=lblTimezone.ClientID%>").innerHTML = recur_discription(document.getElementById("<%=Recur.ClientID %>").value, et
            , document.getElementById("timezone").value, Date(),"<%=Session["timeFormat"].ToString()%>","<%=Session["timezoneDisplay"].ToString()%>");//FB 1728 //FB 1948
            if(document.getElementById("lblTimezoneDIV").innerText.length > 200)
                document.getElementById("lblTimezoneDIV").style.height = "200";
        }
        else
            document.getElementById("lblTimezoneDIV").style.height = "20";
    }
    setTimeout("calculateRecurText()", 100);
    //Blue Status Project
    function ShowEpID()
    {
        if(arguments[1] != "")
            document.getElementById(arguments[0]).innerHTML = arguments[1];
    }
    </script><%--FB 1985 --%> <script type="text/javascript">
    function fnShowHideAVLink()
	{
	  var args = fnShowHideAVLink.arguments;
	  var obj = eval(document.getElementById("LnkAVExpand"));
	  var Conftype = eval(document.getElementById("hdnConfType")); 
      if(Conftype.value != "7" )
      {
        if(Conftype.value != "4")
         {
           if(obj)
            {
              obj.style.display = 'none';
               if(args[0] == '1')
                {
                 obj.style.display = '';
                }
            }
         }
      }
	}
    //FB 2441 Starts
	function fnShowHide(arg) {
	    
	        if (arg == '1')
	            document.getElementById("MuteAllEndpointDiv").style.display = 'block';
	        else
	            document.getElementById("MuteAllEndpointDiv").style.display = 'none';

	        return false;
	  	}
	//FB 2441 Ends
</script><div id="divPic" style="display:none;  z-index:1;">
    <img src="" name="myPic" width="200" height="200"> </div><iframe src="" name="ifrmPreloading" width="0" height="0" style="display:none"><%--Edited for FF--%>>
    <p>Loading page</p>
  </iframe><asp:DropDownList Visible="false" ID="lstProtocol" runat="server" DataTextField="Name" DataValueField="ID">
</asp:DropDownList>

<asp:TextBox ID="txtTimeDifference" Visible="false" runat="server"></asp:TextBox><asp:TextBox ID="tempText" TextMode="multiline" Rows="4" runat="server" width="0" Height="0" ForeColor="black" BackColor="transparent" BorderColor="transparent" style="display:none"></asp:TextBox><%--Edited for FF--%> <asp:TextBox ID="txtEndpointType" runat="server" width="0" Height="0" ForeColor="transparent" BackColor="transparent" BorderColor="transparent" style="display:none"></asp:TextBox><%--Edited for FF--%> <asp:Button ID="btnRefreshEndpoints" runat="server" Visible="false" OnClick="UpdateEndpoints" />
<asp:HiddenField ID="hdnConfStatus" runat="server" />
<asp:HiddenField ID="hdnConfType" runat="server" />
<asp:HiddenField ID="hdnConfLockStatus" runat="server" /> <%--FB 2501 2012-12-07--%> </form></body></html><% 
 //   Response.Write(Request.QueryString["t"]);
if (Request.QueryString["t"] != null)
    if (!Request.QueryString["t"].ToString().Equals("hf"))
    { %> <script language=javascript>
        CheckEndpoint();
        //FB Case 944, 936, 861 and 730 Saima
        function UpdateEndpointStatus()
        {   
                     
            if ( (document.getElementById("dgEndpoints")!= null && document.getElementById("chkAutoRefresh").checked == true)  )
            {
                setTimeout("DataLoading(1);__doPostBack('btnRefreshEndpoints', '')", 30000);
            }
        }
        //FB Case 944, 936, 861 and 730 Saima
        function UpdateP2PEndpointStatus()
        {   
            if ( (document.getElementById("dgP2PEndpoints")!= null) && (document.getElementById("P2pAutoRef").checked == true) )
            {
                
                 var tim = '15000';
            
                var chk = document.getElementById("Refreshchk");
                
                if(chk)
                {
                    if(chk.checked == true)
                        tim = '30000'
                }
            
                setTimeout("DataLoading(1);__doPostBack('btnRefreshEndpoints', '')", tim);
            }
        }
        
        
        if (document.getElementById("P2pAutoRef") != null)
            if ( (document.getElementById("dgP2PEndpoints")!= null) && (document.getElementById("P2pAutoRef").checked == true) )
            UpdateP2PEndpointStatus();
            
        if (document.getElementById("chkAutoRefresh") != null)
            if ( (document.getElementById("dgEndpoints")!= null) && (document.getElementById("chkAutoRefresh").checked == true) )
            UpdateEndpointStatus();
</script><%--<script language="javascript">

                   
                        if (document.getElementById("Menu1") != null)
                        {
                            document.getElementById("Menu1").innerHTML = document.getElementById("Menu1").innerHTML.replace("javascript:", "javascript:if (DataLoading(1)) ");
                        }
                    

</script>--%> <!-- #INCLUDE FILE="inc/mainbottomNET.aspx" --><% }
   else
   { %> <%--code added for Soft Edge button--%> <script type="text/javascript" src="inc/softedge.js"></script><script language=javascript>
window.resizeTo(1000,700);
document.getElementById("chkExpandCollapse").checked = true;
ExpandAll();
</script><% } %>

<%--FB 2508 Start--%>
<script type="text/javascript">
    var label = document.getElementById('lblConfName');
    var caption = document.getElementById('ConfTitle');
    if (label.innerHTML.length > 50) {
        caption.title = label.innerHTML;
        label.innerHTML = label.innerHTML.substr(0, 50);
    }
    else {
        caption.style.visibility = 'hidden';
    }
</script>
<%--FB 2508 End--%>