﻿<%--ZD 100147 start--%>
<%--/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.	
*
* You should have received a copy of the myVRM license with	
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/--%><%--ZD 100147 End--%>
<%@ Page Language="C#" EnableSessionState="True" Inherits="ns_ManageCustomAttribute.ManageCustomAttribute" %><%--ZD 100170--%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> <%--FB 2779--%>
<meta http-equiv="X-UA-Compatible" content="IE=8" /> <!-- FB 2050 -->
<!-- #INCLUDE FILE="inc/maintopNET.aspx" --> 
<script type="text/javascript">

function fnOnComboChange()
{
    var optionTR = document.getElementById('TrOptionEntry');
    var optionTRD = document.getElementById('TrOptionDisplay');
    var drpCA = document.getElementById('<%=DrpCAType.ClientID%>');
    if(drpCA)
    {
        if(optionTR && optionTRD)
        {
            if(drpCA.value=='5' || drpCA.value=='6'|| drpCA.value=='8') //FB 1718
            {
                optionTR.style.display = 'block';
                optionTRD.style.display = 'block';
            }
            else
            {
                optionTR.style.display = 'none';
                optionTRD.style.display = 'none';
            }
        }
        if(drpCA.value == '2')//FB 2377
            document.getElementById('ChkCARequired').disabled = true;
        else
            document.getElementById('ChkCARequired').disabled = false;
    }
}
function FnCancel() {
    DataLoading(1); //ZD 100176
	window.location.replace('ViewCustomAttributes.aspx');
}
function fnValidator()
{
    var txtcaname = document.getElementById('<%=TxtCAName.ClientID%>');
    if(txtcaname.value == "")
    {        
        ReqCAName.style.display = 'block';
        txtcaname.focus();
        return false;
    }//FB 2377
    else if (txtcaname.value.search(/^(a-z|A-Z|0-9)*[^\\<>^+;?|!`,\[\]{}\x22;=:@#$%&()'~]*$/)==-1)
    {        
        regItemName1.style.display = 'block';
        txtcaname.focus();
        return false;
    }    
    
    var txtcadesc = document.getElementById('<%=TxtCADesc.ClientID%>');
    
    if(txtcadesc.value != "")
    {//FB 2377
        if (txtcadesc.value.search(/^(a-z|A-Z|0-9)*[^\\<>^+;?|!`,\[\]{}\x22;=:@#$%&()'~]*$/)==-1)
        {        
            RegCADesc.style.display = 'block';
            txtcadesc.focus();
            return false;
        }    
    }
    //FB 1767 start
    if(document.getElementById('errLabel') != null)//FB 1970
        document.getElementById('errLabel').style.display = "none";
    if(document.getElementById('ChkCAEmail').checked == true)
    {
        if(document.getElementById('chkHost').checked == false)
            if(document.getElementById('ChkRoomApp').checked == false)
                if(document.getElementById('chkScheduler').checked == false)
                    if(document.getElementById('ChkMcuApp').checked == false)
                        if(document.getElementById('chkParty').checked == false)
                            if(document.getElementById('ChkSysApp').checked == false)
                                if (document.getElementById('chkRoomAdmin').checked == false)
                                    if(document.getElementById('chkVNOCOperator').checked ==false) //FB 2501
                                        if(document.getElementById('chkMcuAdmin').checked == false)
                                          {                                        
                                             document.getElementById('errLabel').innerText = "Please select any one Mailing Rights";
                                             document.getElementById('errLabel').style.display = "block" ;
                                             return false;
                                          }
    }
	//FB 1767 end
    return(true);

}
function frmValidator()
{   
    document.getElementById("txtMaxChar").style.display = "none"; //FB 1970
    var txtentityname = document.getElementById('<%=txtEntityName.ClientID%>');
    if(txtentityname.value == "")
    {        
        reqEntityName.style.display = 'block';
        txtentityname.focus();
        return false;
    }//FB 2377
    else if (txtentityname.value.search(/^(a-z|A-Z|0-9)*[^\\<>^+;?|!`,\[\]{}\x22;=:@#$%&()'~]*$/)==-1)
    {        
        regItemName1.style.display = 'block';
        txtentityname.focus();
        return false;
    }    
    
    var txtentitydesc = document.getElementById('<%=txtEntityDesc.ClientID%>');
    
    if(txtentitydesc.value != "")
    {//FB 2377
        if (txtentitydesc.value.search(/^(a-z|A-Z|0-9)*[^\\<>^+;?|!`,\[\]{}\x22;=:@#$%&()'~]*$/)==-1)
        {        
            regItemDesc.style.display = 'block';
            txtentitydesc.focus();
            return false;
        }    
    }
    
    return(true);
}

function fnGridValidation()
{
    
    var args = document.getElementById("hdnValue").value;
    var recCount = 2;	    
    var gridName = document.getElementById("dgEntityCode");
    
    for(var i=2; i<=args + 1; i++)
	{				
		statusValue = "";
		if(i < 10)
		    i = "0" + i;
		 
		var newText = document.getElementById("dgEntityCode_ctl"+ i + "_txtEntityName");
		var rqName = document.getElementById("dgEntityCode_ctl"+ i + "_reqName");
		var rgName = document.getElementById("dgEntityCode_ctl"+ i + "_regItemName1");
		
		if(newText)
		{
			if(newText.value == "")
            {        
                rqName.style.display = 'block';
                newText.focus();
                return false;
            }
            
            else if (newText.value.search(/^(a-z|A-Z|0-9)*[^\\<>^+;?|!`,\[\]{}\x22;=:@#$%&()'~]*$/)==-1)
            {        
                rgName.style.display = 'block';
                newText.focus();
                return false;
            }   
		}	
		
		var newText1 = document.getElementById("dgEntityCode_ctl"+ i + "_txtEntityDesc");		
		var rgName1 = document.getElementById("dgEntityCode_ctl"+ i + "_regItemName");
		
		if(newText1)
		{
			if(newText1.value != "")
            {        
               if (newText1.value.search(/^(a-z|A-Z|0-9)*[^\\<>^+;?|!`,\[\]{}\x22;=:@#$%&()'~]*$/)==-1)
                {      
                    rgName1.style.display = 'block';
                    newText1.focus();
                    return false;
                }   
		    }	
		}
    }
    
    return true;
}

//ZD 100176 start
function DataLoading(val) {
    if (val == "1")
        document.getElementById("dataLoadingDIV").innerHTML = "<img border='0' src='image/wait1.gif'>";
    else
        document.getElementById("dataLoadingDIV").innerHTML = "";
}
//ZD 100176 End
</script>

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title>Custom Options</title>
</head>
<body>
    <form id="frmItemsList" runat="server" method="post">
        <center>
        <input type="hidden" runat="server" id="hdnValue" />
        <input type="hidden" runat="server" id="hdnCustomAttID" enableviewstate="true" />
        <input type="hidden" runat="server" id="hdnCreateby" /><%--FB 2349--%>
        <div>
            <table width="100%" border="0">
                <tr>
                    <td align="center">
                        <h3><asp:Label ID="lblHeader" runat="server">Manage Custom Option</asp:Label></h3><%-- FB 2670--%>
                    </td>
                </tr>
				<%--FB 1767 start--%>
                <tr>
                    <td align="center">
                        <asp:Label ID="errLabel" runat="server" CssClass="lblError" style="display:none" ></asp:Label>
                        <div id="dataLoadingDIV" align="center"></div><%--ZD 100176--%>
                    </td>
                </tr>
                 <%--FB 1970 start--%>
                <tr>
                    <td align="center">
                      <table width="100%">
                      <tr>
                        <td width="50%">
                         <table cellspacing="3" border="0" width="100%">
                            <tr>
                                <td align="left" width="40%" class="blackblodtext" valign="top">Title in English<span class="reqfldstarText">*</span></td> <%-- FB 2050 --%>
                                <td align="left" width="60%"> <%-- FB 2050 --%>
                                    <asp:TextBox ID="TxtCAName" runat="server" class="altText" Width="200" MaxLength="200"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="ReqCAName" ValidationGroup="Upload" runat="server" ControlToValidate="TxtCAName" ErrorMessage="Required" Display="dynamic" ></asp:RequiredFieldValidator>                                            
                                    <asp:RegularExpressionValidator ID="RegCAName" ValidationGroup="CustAdd" ControlToValidate="TxtCAName" Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<br>& < > ' + % \ ( ) ; ? | ^ = ! ` , [ ] { } : # $ @ ~ and &#34; are invalid characters." ValidationExpression="^(a-z|A-Z|0-9)*[^\\<>^+;?|!`,\[\]{}\x22;=:@#$%&()'~]*$"></asp:RegularExpressionValidator>
                                </td><%--FB 2377--%>
							<%--FB 1767 end--%>
                            </tr>
                            <tr>
                              <td valign="bottom" colspan="2" align="left">
                                <asp:ImageButton ID="imgLangsTitle" runat="server" ImageUrl="image/loc/nolines_minus.gif" Height="20" Width="20" vspace="0" hspace="0" ToolTip="View Other Language" />
                                <a class="blackblodtext" style="vertical-align:top">Other Languages</a> 
                              </td>
                            </tr>
                            <tr id="otherLangRow" runat="server" valign="top">
                              <td colspan="2" valign="top" style="padding:opx"> 
                                <asp:DataGrid ID="dgTitlelist" runat="server" AutoGenerateColumns="false" GridLines="None" CellPadding="2" CellSpacing="0" style="border-collapse:separate; vertical-align:top" ShowHeader="false" Width="100%">
                                  <Columns>
                                    <asp:TemplateColumn ItemStyle-CssClass="blackblodtext" ItemStyle-Width="40%" ItemStyle-HorizontalAlign="Left"> <%-- FB 2050 --%>
                                      <ItemTemplate>
                                         <asp:Label ID="lblLangID" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.ID") %>' Visible="false"></asp:Label>
                                         <asp:Label ID="lblLangName" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.name") %>'></asp:Label>
                                      </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn ItemStyle-Height="30px" ItemStyle-HorizontalAlign="Left"> <%-- FB 2050 --%>
                                      <ItemTemplate>
                                         <asp:TextBox ID="txtTitle" CssClass="altText" runat="server" Width="200" MaxLength="200" Text='<%#DataBinder.Eval(Container, "DataItem.Title") %>'></asp:TextBox>
                                         <asp:RegularExpressionValidator ID="reglangName" ValidationGroup="Submit" ControlToValidate="txtTitle" Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<br>& < > ' + % \ ( ) ; ? | ^ = ! ` , [ ] <br>{ } : # $ @ ~ and &#34; are invalid <br>characters." ValidationExpression="^(a-z|A-Z|0-9)*[^\\<>+;?|!`,\[\]{}\x22;=^:@#$%&()'~]*$"></asp:RegularExpressionValidator>
                                      </ItemTemplate><%--FB 2377--%>
                                    </asp:TemplateColumn>
                                  </Columns>
                                </asp:DataGrid>
                              </td>
                            </tr>
                            <tr>
                                <td align="left" class="blackblodtext">Description</td>
                                <td align="left">
                                    <asp:TextBox ID="TxtCADesc" runat="server" class="altText" Width="260" MaxLength="250"></asp:TextBox>
                                    <asp:RegularExpressionValidator ID="RegCADesc" ControlToValidate="TxtCADesc" Display="dynamic" runat="server" ValidationGroup="CustAdd" SetFocusOnError="true" ErrorMessage="<br>& < > ' + % \ ( ) ; ? | ^ = ! ` , [ ] { } : # $ @ ~ and &#34; are invalid characters." ValidationExpression="^(a-z|A-Z|0-9)*[^\\<>^+;?|!`,\[\]{}\x22;=:@#$%&()'~]*$"></asp:RegularExpressionValidator>
                                </td><%--FB 2377--%>
                            </tr>
                            <tr>
                                <td align="left" class="blackblodtext">Is Mandatory</td>
                                <td align="left">
                                    <asp:CheckBox ID="ChkCARequired" runat="server" EnableViewState="true" />
                                </td>
                            </tr>
                            <tr>
                                <td align="left" class="blackblodtext">Enable Status</td>
                                <td align="left">
                                    <asp:CheckBox ID="ChkCAStatus" runat="server" EnableViewState="true" />
                                </td>
                            </tr>
                            <tr>
                                <td align="left" class="blackblodtext">Include in E-mail</td>
                                <td align="left">
                                    <asp:CheckBox ID="ChkCAEmail" runat="server" EnableViewState="true" onClick="javascript:Incmailselectall()" /><%--FB 1767--%>
                                </td>
                            </tr>
                            <tr><%--FB 2013--%>
                                <td align="left" class="blackblodtext">Include in Calendar</td>
                                <td align="left">
                                    <asp:CheckBox ID="ChkCAinCalendar" runat="server"/>
                                </td>
                            </tr>
                            <tr>
                                <td align="left" class="blackblodtext">Field Type</td>
                                <%-- FB 1718 --%>
                                <td align="left">
                                    <asp:DropDownList ID="DrpCAType" runat="server" class="altText">
                                        <%--<asp:ListItem Value="3" Text="Radio Button"></asp:ListItem>--%> <%--Search Fixes - FB 1787 --%>
                                        <asp:ListItem Value="8" Text="Radio Button List"></asp:ListItem>
                                        <asp:ListItem Value="4" Text="Text"></asp:ListItem>
                                        <asp:ListItem Value="10" Text="Text-Multiline"></asp:ListItem>
                                        <asp:ListItem Value="5" Text="List Box"></asp:ListItem>
                                        <asp:ListItem Value="6" Text="Drop-Down List"></asp:ListItem>
                                        <asp:ListItem Value="7" Text="URL"></asp:ListItem>
                                        <asp:ListItem Value="2" Text="Check Box"></asp:ListItem> <%--FB 2377--%>
                                    </asp:DropDownList>
                                </td>
                            </tr>
                          </table>
						<%--FB 1767 start--%>
                        </td>
                        <td  width="50%" valign="top">
                         <table width="100%">
                          <tr>
                           <td style="width:20%" >
                             <table width="100%">
                              <tr>
                               <td width="55%">
                                  <span id="mailListHead" runat="server" class="subtitleblueblodtext">Mailing Rights</span>
                               </td>
                               <td>
                                  <asp:CheckBox ID="ChkAll" runat="server" Text="Select All" onClick="javascript:selectAllMailRight()" />
                               </td>
                              </tr>
                             </table>
                           </td>
                          </tr>
                          <tr>
                           <td width="80%">
                            <div id="InputParamDiv"  style="border-color:Blue;border-width:1px;border-style:Solid;width:74%;Height:auto;overflow:auto;text-align:left;">
                              <table width="100%">
                                 <tr><td style="height:3px" colspan="3"></td></tr>
                                 <tr>
                                 <td width="2%"></td>
                                  <td width="55%">
                                     <asp:CheckBox ID="chkHost" runat="server" Text="Host" onClick="javascript:clearErrLabel(this)"/>
                                  </td>
                                  <td>
                                     <asp:CheckBox ID="ChkRoomApp" runat="server" Text="Room Approver" onClick="javascript:clearErrLabel(this)"/></td>
                                 </tr>
                                 <tr>
                                  <td width="2%"></td>
                                  <td>
                                     <asp:CheckBox ID="chkScheduler" runat="server" Text="Scheduler" onClick="javascript:clearErrLabel(this)"/>
                                  </td>
                                  <td>
                                     <asp:CheckBox ID="ChkMcuApp" runat="server" Text="Mcu Approver" onClick="javascript:clearErrLabel(this)"/>
                                  </td>
                                 </tr>
                                 <tr>
                                  <td width="2%"></td>
                                  <td>
                                     <asp:CheckBox ID="chkParty" runat="server" Text="Participant" onClick="javascript:clearErrLabel(this)"/>
                                  </td>
                                  <td>
                                     <asp:CheckBox ID="ChkSysApp" runat="server" Text="System Approver" onClick="javascript:clearErrLabel(this)"/>
                                  </td>
                                 </tr>
                                 <tr>
                                  <td width="2%"></td>
                                  <td>
                                     <asp:CheckBox ID="chkRoomAdmin" runat="server" Text="Room Admin" onClick="javascript:clearErrLabel(this)"/>
                                  </td>
                                  <td>
                                    <asp:CheckBox ID="chkVNOCOperator" runat="server" Text="VNOC Opertaor" onClick="javascript:clearErrLabel(this)"/>
                                  </td>
                                 </tr>
                                 <tr>
                                  <td width="2%"></td>
                                  <td>
                                     <asp:CheckBox ID="chkMcuAdmin" runat="server" Text="Mcu Admin" onClick="javascript:clearErrLabel(this)"/>
                                  </td>
                                 </tr>
                                 <tr><td style="height:3px" colspan="3"></td></tr>
                              </table>
                            </div>
                           </td>
                          </tr> 
                         </table>
                        </td>
                      </tr>
                      </table>
					<%--FB 1767 End--%>	
                    </td>
                </tr>
                <tr id="TrOptionEntry" runat="server" align="center"> <%-- FB 2050 --%>
                    <td align="center">
                        <table width="755px" cellspacing="0" cellpadding="2" border="0"> <%--Edited for FF & FB 2050--%>
                            <tr>
                                <td align="left" class="subtitleblueblodtext" colspan="4"> <%-- FB 2050 --%>
                                    Add Options to the Custom Option <br /> 
                                </td>
                            </tr>
                            <tr>
                               <td colspan="4"> <%-- FB 2050 --%>
                                  <table class="tableHeader" border="0" width="100%">
                                    <tr>
                                       <td style="width: 135px" align="left" valign="top" class="blackblodtext">Language Name</td> <%--FB 1970--%><%--FB 2579--%>
                                       <td align="left" valign="top" style="width: 230px" class="blackblodtext">Option Name<span class="reqfldstarText">*</span></td>
                                       <td align="left" valign="top" class="blackblodtext">Option Description</td>
                                    </tr>
                                  </table>
                               </td>
                            </tr>
                            <tr>
                                <td valign="top" style="width:13px"><br /> <%-- FB 2050 --%>
                                 <asp:ImageButton ID="imgLangsOption" runat="server" ImageUrl="image/loc/nolines_minus.gif" Height="20" Width="20" vspace="0" hspace="0" ToolTip="View Other Languages"/>
                                </td>
                                <td style="width: 130.5px" align="left" valign="top"><br /><a class="blackblodtext">English</a></td>
                                <td align="left" valign="top" style="width: 228px"><br />
                                    <asp:TextBox ID="txtEntityID" Text="new" Visible="false" runat="server" Width="32px"></asp:TextBox>
                                    <asp:TextBox ID="txtEntityName" runat="server"  Width="200" CssClass="altText" MaxLength="35"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="reqEntityName" ValidationGroup="Upload" runat="server" ControlToValidate="txtEntityName" ErrorMessage="Required" Display="dynamic" ></asp:RequiredFieldValidator>  <%--FB 2377--%>                                          
                                    <asp:RegularExpressionValidator ID="regItemName1" ControlToValidate="txtEntityName" Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<br>& < > ' + % \ ( ) ; ? | ^ = ! ` , [ ] { } : # $ @ ~ and &#34; are invalid characters." ValidationExpression="^(a-z|A-Z|0-9)*[^\\<>^+;?|!`,\[\]{}\x22;=:@#$%&()'~]*$"></asp:RegularExpressionValidator>
                                </td>
                                <td align="left" valign="top"><br />
                                  <asp:TextBox ID="txtEntityDesc" Width="220" runat="server" CssClass="altText" MaxLength="35"></asp:TextBox><%--FB 2377--%>
                                  <asp:RegularExpressionValidator ID="regItemDesc" ControlToValidate="txtEntityDesc" Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<br>& < > ' + % \ ( ) ; ? | ^ = ! ` , [ ] { } : #<br> $ @ ~ and &#34; are invalid characters." ValidationExpression="^(a-z|A-Z|0-9)*[^\\<>^+;?|!`,\[\]{}\x22;=:@#$%&()'~]*$"></asp:RegularExpressionValidator>
                                </td>
                                <td align="center" valign="top"><br />
                                    <asp:Button ID="btnAddEntityCode" OnClick="AddEntityCode" ValidationGroup="Upload" runat="server" CssClass="altLongBlueButtonFormat" Text="Add New Option" OnClientClick="javascript:return frmValidator()" Width="155px" ></asp:Button>
                                </td>
                            </tr>
                            <tr id="LangsOptionRow" runat="server"> <%-- FB 1970--%>
                              <td></td>
                              <td colspan="5" style="text-align:left"> <%-- FB 2050--%>
                                 <asp:DataGrid ID="dgLangOptionlist" runat="server" AutoGenerateColumns="false" GridLines="None" CellPadding="1" CellSpacing="0" style="border-collapse:separate" ShowHeader="false">
                                  <ItemStyle Height="2" VerticalAlign="Top" />
                                  <Columns>
                                    <asp:TemplateColumn ItemStyle-CssClass="blackblodtext" ItemStyle-Width="131px">
                                      <ItemTemplate>
                                         <asp:Label ID="lblLangID" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.ID") %>' Visible="false"></asp:Label>
                                         <asp:Label ID="lblLangName" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.name") %>'></asp:Label>
                                      </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn ItemStyle-Width="230px" ItemStyle-Height="30px">
                                      <ItemTemplate>
                                            <asp:TextBox ID="txtOptionID" Text="new" Visible="false" runat="server" Width="32px"></asp:TextBox>
                                            <asp:TextBox ID="txtOptionName" runat="server"  Width="200" CssClass="altText" MaxLength="35" Text=""></asp:TextBox><%--FB 2377--%>
                                            <asp:RegularExpressionValidator ID="regItemName1" ControlToValidate="txtOptionName" Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<br>& < > ' + % \ ( ) ; ? | ^ = ! ` , [ ] { } : # $ @ ~ and &#34; are invalid characters." ValidationExpression="^(a-z|A-Z|0-9)*[^\\<>^+;?|!`,\[\]{}\x22;=:@#$%&()'~]*$"></asp:RegularExpressionValidator>
                                      </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn>
                                      <ItemTemplate>
                                            <asp:TextBox ID="txtOptionDesc" Width="220" runat="server" CssClass="altText" MaxLength="35"></asp:TextBox><%--FB 2377--%>
                                            <asp:RegularExpressionValidator ID="regItemDesc" ControlToValidate="txtOptionDesc" Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<br>& < > ' + % \ ( ) ; ? | ^ = ! ` , [ ] { } : # $ @<br> ~ and &#34; are invalid characters." ValidationExpression="^(a-z|A-Z|0-9)*[^\\<>^+;?|!`,\[\]{}\x22;=:@#$%&()'~]*$"></asp:RegularExpressionValidator>
                                      </ItemTemplate>
                                    </asp:TemplateColumn>
                                  </Columns>
                                </asp:DataGrid>
                              </td>
                            </tr>
                            <tr>
                              <td id="txtMaxChar" runat="server" colspan="4" style="font-size:xx-small; color:Red; display:none" align="right">Maximum 35 Characters</td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr style="width:100%" align="center"><td align="center" style="width:100%"> <table border="0" style="width:755px" cellpadding="2" cellspacing="0"><%--Added for FF and FB 2050--%>
                <tr id="TrOptionDisplay" align="left" runat="server" style="width:100%"> <%-- FB 2050 --%>
                    <td align="left" style="width:100%">
                        <asp:DataGrid BorderColor="blue" BorderStyle="solid" BorderWidth="1" ID="dgEntityCode" AutoGenerateColumns="false"
                         OnEditCommand="EditItem" OnDeleteCommand="DeleteItem" OnCancelCommand="CancelItem" OnUpdateCommand="UpdateItem"
                         runat="server" Width="750Px" GridLines="None" style="border-collapse:separate"> <%--Edited for FF--%>
                            <HeaderStyle Height="30" CssClass="tableHeader" HorizontalAlign="Center" />
                            <AlternatingItemStyle CssClass="tableBody"/>
                            <ItemStyle CssClass="tableBody" />
                            <FooterStyle CssClass="tableBody" />
                            <Columns>
                                <asp:BoundColumn DataField="RowUID" Visible="false"></asp:BoundColumn>
                                <asp:BoundColumn DataField="OptionID" Visible="false"></asp:BoundColumn>
                                <asp:BoundColumn DataField="DisplayValue" Visible="false"></asp:BoundColumn>
                                <asp:TemplateColumn ItemStyle-CssClass="blackblodtext"  HeaderStyle-CssClass="tableHeader" ItemStyle-Width="143" HeaderText="Language Name" HeaderStyle-HorizontalAlign="Left"> <%--FB 1970--%>
                                      <ItemTemplate>
                                         <asp:Label ID="lblLangID" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.ID") %>' Visible="false"></asp:Label>
                                         <asp:Label ID="lblLangName" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.name") %>'></asp:Label>
                                      </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="Option Name" HeaderStyle-CssClass="tableHeader" ItemStyle-CssClass="tableBody">
                                    <ItemTemplate>
                                        <asp:Label ID="lblEntityName" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.DisplayCaption") %>' ></asp:Label>
                                    </ItemTemplate>
                                    <EditItemTemplate>
                                        <asp:TextBox ID="txtEntityName" CssClass="altText" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.DisplayCaption") %>'></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="reqName" ValidationGroup="Update" runat="server" ControlToValidate="txtEntityName" ErrorMessage="Required" Display="dynamic" ></asp:RequiredFieldValidator><%--FB 2377--%>
                                        <asp:RegularExpressionValidator ID="regItemName1" ControlToValidate="txtEntityName" Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<br>& < > ' + % \ ( ) ; ? | ^ = ! ` , [ ] { } : # $ @ ~ <br> and &#34; are invalid characters." ValidationExpression="^(a-z|A-Z|0-9)*[^\\<>^+;?|!`,\[\]{}\x22;=:@#$%&()'~]*$"></asp:RegularExpressionValidator>
                                    </EditItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="Option Description" HeaderStyle-CssClass="tableHeader" ItemStyle-CssClass="tableBody">
                                    <ItemTemplate>
                                        <asp:Label ID="lblEntityDesc" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.HelpText") %>' ></asp:Label>
                                    </ItemTemplate>
                                    <EditItemTemplate>
                                        <asp:TextBox ID="txtEntityDesc" CssClass="altText" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.HelpText") %>'></asp:TextBox><%--FB 2377--%>
                                        <asp:RegularExpressionValidator ID="regItemName" ControlToValidate="txtEntityDesc" Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<br>& < > ' + % \ ( ) ; ? | ^ = ! ` , [ ] { } : # $ @ ~ <br> and &#34; are invalid characters." ValidationExpression="^(a-z|A-Z|0-9)*[^\\<>^+;?|!`,\[\]{}\x22;=:@#$%&()'~]*$"></asp:RegularExpressionValidator>
                                    </EditItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="Actions" HeaderStyle-CssClass="tableHeader" ItemStyle-CssClass="tableBody" >
                                    <ItemTemplate>
                                        <asp:LinkButton ID="btnEdit" Text="Edit" CommandName="Edit" runat="server"></asp:LinkButton>
                                        <asp:LinkButton ID="btnDelete" Text="Delete" CommandName="Delete" runat="server"></asp:LinkButton>
                                    </ItemTemplate>
                                    <EditItemTemplate>
                                        <asp:LinkButton ID="btnUpdate" Text="Update" CommandName="Update" runat="server" OnClientClick="javascript:return fnGridValidation()" ValidationGroup="Update"></asp:LinkButton>
                                        <asp:LinkButton ID="btnCancel" Text="Cancel" CommandName="Cancel" runat="server"></asp:LinkButton>
                                    </EditItemTemplate>
                                </asp:TemplateColumn>
                            </Columns>
                        </asp:DataGrid>
                        <asp:Table runat="server" ID="tblNoOptions" Visible="false" Width="90%">
                        <asp:TableRow CssClass="lblError">
                            <asp:TableCell CssClass="lblError" HorizontalAlign="center">
                                No Options found.
                            </asp:TableCell>
                        </asp:TableRow>
                    </asp:Table>
                    </td>
                </tr>
                </table></td></tr><%--Added for FF--%>
                 <%--FB 1970 end--%>
                <tr>
                    <td align="center">
                        <br />
                        <asp:Button ID="BtnAddCustom" OnClick="SetCustomAttribute"  runat="server" Width="100pt" Text="Submit" OnClientClick="javascript:return fnValidator()" ></asp:Button><%-- FB 2796--%>
                        <input id="Cancel" type="button" onclick="FnCancel()" value="Cancel" class="altMedium0BlueButtonFormat"/>
                    </td>
                </tr>
            </table>
        </div>
        </center>
    </form>
</body>
</html>
<!-- #INCLUDE FILE="inc/mainbottomNET.aspx" -->
<%--code added for Soft Edge button--%>
<script type="text/javascript" src="inc/softedge.js"></script>
<script language="javascript">
    fnOnComboChange();
    //FB 1767 start
    Incmailselectall();
    function Incmailselectall()
    {
      var chkall = document.getElementById('ChkAll');
      clearErrLabel(chkall);
      if(document.getElementById('ChkCAEmail').checked == true)
      {
        document.getElementById('chkHost').disabled = false ;
        document.getElementById('ChkRoomApp').disabled = false ;
        document.getElementById('chkScheduler').disabled = false ;
        document.getElementById('ChkMcuApp').disabled = false ;
        document.getElementById('chkParty').disabled = false ;
        document.getElementById('ChkSysApp').disabled = false ;
        document.getElementById('chkRoomAdmin').disabled = false ;
        document.getElementById('chkMcuAdmin').disabled = false ;
        document.getElementById('ChkAll').disabled = false;
        document.getElementById('chkVNOCOperator').disabled = false; //FB 2501
      }
      else
      {
        chkall.checked = false;
        document.getElementById('ChkAll').disabled = true ;
        document.getElementById('chkHost').disabled = true ;
        document.getElementById('ChkRoomApp').disabled =  true ;
        document.getElementById('chkScheduler').disabled =  true ;
        document.getElementById('ChkMcuApp').disabled =  true ;
        document.getElementById('chkParty').disabled =  true ;
        document.getElementById('ChkSysApp').disabled =  true ;
        document.getElementById('chkRoomAdmin').disabled =  true ;
        document.getElementById('chkMcuAdmin').disabled = true;
        document.getElementById('chkVNOCOperator').disabled = true; //FB 2501
        selectAllMailRight()
      }
      
       
    }
    function selectAllMailRight()
    {
        var chkall = document.getElementById('ChkAll');
        if (chkall.checked == true)
            {
              if(document.getElementById('ChkCAEmail').checked == true)
              {
                clearErrLabel(chkall);
                document.getElementById('chkHost').checked = true;
                document.getElementById('ChkRoomApp').checked = true;
                document.getElementById('chkScheduler').checked = true ;
                document.getElementById('ChkMcuApp').checked = true;
                document.getElementById('chkParty').checked = true;
                document.getElementById('ChkSysApp').checked = true;
                document.getElementById('chkRoomAdmin').checked = true;
                document.getElementById('chkMcuAdmin').checked = true;
                document.getElementById('chkVNOCOperator').checked = true; //FB 2501
              }
            }
         else
          {
                document.getElementById('chkHost').checked = false;
                document.getElementById('ChkRoomApp').checked = false;
                document.getElementById('chkScheduler').checked = false ;
                document.getElementById('ChkMcuApp').checked = false;
                document.getElementById('chkParty').checked = false;
                document.getElementById('ChkSysApp').checked = false;
                document.getElementById('chkRoomAdmin').checked = false;
                document.getElementById('chkMcuAdmin').checked = false;
                document.getElementById('chkVNOCOperator').checked = false; //FB 2501                
          }
    }
    function clearErrLabel(obj) 
    {
        if (document.getElementById('errLabel') != null) //FB 1970
        {
            document.getElementById('errLabel').innertext = "";
            document.getElementById('errLabel').style.display = "none";
        }
      if (obj.checked == false)
          document.getElementById('ChkAll').checked = false;
      else if (obj.checked == true)
      {
          if(document.getElementById('chkHost').checked == true)
           if(document.getElementById('ChkRoomApp').checked == true)
            if(document.getElementById('chkScheduler').checked == true)
             if(document.getElementById('ChkMcuApp').checked == true)
              if(document.getElementById('chkParty').checked == true)
               if(document.getElementById('ChkSysApp').checked == true)
                 if(document.getElementById('chkRoomAdmin').checked == true)
                     if (document.getElementById('chkMcuAdmin').checked == true)
                        if(document.getElementById('chkVNOCOperator').checked ==true) //FB 2501
                            document.getElementById('ChkAll').checked = true;
      }
  }
  if (document.getElementById('errLabel') != null) //FB 1970
    document.getElementById('errLabel').style.display = "block";
  //FB 1767 end

  //FB 1970 start
  ExpandCollapse(document.getElementById("imgLangsTitle"), "<%=otherLangRow.ClientID %>", true)
  ExpandCollapse(document.getElementById("imgLangsOption"), "<%=LangsOptionRow.ClientID %>", true);
  
  function maxCharRowShow()
  {
      document.getElementById("txtMaxChar").style.display = "block";
  }
  
  function ExpandCollapse(img, str, frmCheck)
   {
     
      obj = document.getElementById(str);
      if (obj != null)
      {
          if (frmCheck == true)
          {
                  img.src = img.src.replace("minus", "plus");
                  obj.style.display = "none";
          }
          if (frmCheck == false)
          {
              if (img.src.indexOf("minus") >= 0)
               {
                  img.src = img.src.replace("minus", "plus");
                  obj.style.display = "none";
              }
              else 
              {
                  img.src = img.src.replace("plus", "minus");
                  obj.style.display = "";
              }
          }
      }
  } 
  //FB 1970 end   
</script>