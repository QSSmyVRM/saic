<%--ZD 100147 start--%>
<%--/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.	
*
* You should have received a copy of the myVRM license with	
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/--%><%--ZD 100147 End--%>
<%@ Page Language="C#" Inherits="ns_Department.Department" Buffer="true" %>
<%--FB 2779 Starts--%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<meta http-equiv="X-UA-Compatible" content="IE=8" />
<!-- #INCLUDE FILE="inc/maintopNET.aspx" -->
<%--FB 2779 Ends--%>


<script language="javascript">
function CheckName()
{
    if (document.getElementById("<%=txtNewDepartmentName.ClientID %>").value == "")
    {
        document.getElementById("<%=lblRequired.ClientID %>").innerText = "Required";
        return false;
    }
    else
    {
        alert("To add users to this department, edit each user profile as necessary."); // this alert has been added after discussing with Bri
        document.getElementById("<%=lblRequired.ClientID %>").innerText = "";
        return true;
    }
}
//ZD 100176 start
function DataLoading(val) {
    if (val == "1")
        document.getElementById("dataLoadingDIV").innerHTML = "<img border='0' src='image/wait1.gif'>";
    else
        document.getElementById("dataLoadingDIV").innerHTML = "";
}
//ZD 100176 End
</script>
<head runat="server">
    <title>Manage Departments</title>
    <script type="text/javascript" src="inc/functions.js"></script>

</head>
<body>
    <form id="frmInventoryManagement" runat="server" method="post" onsubmit="return true;DataLoading(1);"> <%--ZD 100176--%>
      <input type="hidden" id="helpPage" value="97">

    <div>
        <table width="100%">
            <tr>
                <td align="center">
                    <h3>
                        <asp:Label ID="lblHeader" runat="server"></asp:Label>
                    </h3>
                </td>
            </tr>
            <tr>
                <td align="center">
                    <asp:Label ID="errLabel" runat="server" Text="" CssClass="lblError"></asp:Label>
                </td>
            </tr>
            <div id="dataLoadingDIV" style="z-index: 1" align="center"> <%--ZD 100176--%>
            <tr>
                <td align="Left">
                    <table cellspacing="5">
                        <tr>
                            <td >&nbsp;</td>
                            <td>
                                <SPAN class=subtitleblueblodtext>Existing Departments</SPAN>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td align="center">
                    <asp:DataGrid ID="dgDepartments" runat="server" AutoGenerateColumns="False" CellPadding="4" GridLines="None"
                         BorderColor="blue" BorderStyle="solid" BorderWidth="1" ShowFooter="true" OnItemCreated="BindRowsDeleteMessage"
                        OnDeleteCommand="DeleteDepartment" OnEditCommand="EditDepartment" OnUpdateCommand="UpdateDepartment" OnCancelCommand="CancelDepartment" Width="90%" Visible="true" style="border-collapse:separate"> <%--Edited for FF--%>
                       <%--Window Dressing - Start--%>
                        <SelectedItemStyle  CssClass="tableBody"  Font-Bold="True"  />
                        <EditItemStyle CssClass="tableBody" />
                        <AlternatingItemStyle CssClass="tableBody" />
                        <ItemStyle CssClass="tableBody" />
                        <FooterStyle CssClass="tableBody" />
                        <%--Window Dressing - End--%>
                        <HeaderStyle CssClass="tableHeader" />
                        <Columns>
                            <asp:BoundColumn DataField="id" Visible="false" ><HeaderStyle CssClass="tableHeader" /></asp:BoundColumn>
                            <asp:TemplateColumn HeaderText="Name" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left" ItemStyle-CssClass="tableBody" HeaderStyle-CssClass="tableHeader"> <%-- FB 2050 --%>
                                <ItemTemplate>
                                    <asp:Label ID="lblDepartmentName" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.name") %>'></asp:Label>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:TextBox ID="txtDepartmentName" CssClass="altText" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.name")%>'></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="reqDepartmentName1" ControlToValidate="txtDepartmentName" runat="server" ErrorMessage="Required" Display="dynamic" ValidationGroup="Update" ></asp:RequiredFieldValidator>
                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator4" ControlToValidate="txtDepartmentName" Display="dynamic" runat="server" ValidationGroup="Update" SetFocusOnError="true" ErrorMessage="<br>& < > ' + % \ / ( ) ; ? | ^ = ! ` , [ ] { } : # $ @ ~ and &#34; are invalid characters." ValidationExpression="^(a-z|A-Z|0-9)*[^\\/<>+;?|!`,\[\]{}\x22;=^:@#$%&()'~]*$"></asp:RegularExpressionValidator>
                                </EditItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="Actions" ItemStyle-Width="150px"> <%-- FB 2050 --%>
                                <HeaderStyle CssClass="tableHeader" />
                                <ItemTemplate>
                                    <asp:LinkButton runat="server" Text="Edit" ID="btnEdit" CommandName="Edit" OnClientClick="DataLoading(1)"></asp:LinkButton>
                                    &nbsp;<asp:LinkButton runat="server" Text="Delete" ID="btnDelete" CommandName="Delete" OnClientClick="DataLoading(1)"></asp:LinkButton><%--ZD 100176--%> 
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:LinkButton runat="server" Text="Update" ID="btnUpdate" CommandName="Update" ValidationGroup="Update" OnClientClick="DataLoading(1)"></asp:LinkButton>
                                    &nbsp;<asp:LinkButton runat="server" Text="Cancel" ID="btnCancel" CommandName="Cancel" OnClientClick="DataLoading(1)"></asp:LinkButton><%--ZD 100176--%> 
                                </EditItemTemplate>
                                <FooterTemplate>
                                    <%--Window Dressing --%>
                                    <b class="blackblodtext">Total &nbsp;Departments:&nbsp;</b><b> <asp:Label ID="lblTotalRecords" runat="server" Text=""></asp:Label> </b>
                                </FooterTemplate>
                            </asp:TemplateColumn>
                        </Columns>
                    </asp:DataGrid>
                    <asp:Table runat="server" ID="tblNoDepartments" Visible="false" Width="90%">
                        <asp:TableRow CssClass="lblError">
                            <asp:TableCell CssClass="lblError">
                                No departments found.
                            </asp:TableCell>
                        </asp:TableRow>
                    </asp:Table>                    
                </td>
            </tr>
            <tr id="trNew" runat="server" > <%--FB 2670--%>
                <td align="Left">
                    <table cellspacing="5">
                        <tr>
                            <td>&nbsp;</td>
                            <td>
                                <SPAN class=subtitleblueblodtext><asp:Label ID="lblCreateEditDepartment" runat="server" Text="Create New"></asp:Label> Department</SPAN>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr id="trNew1" runat="server"> <%--FB 2670--%>
                <td align="center">
                    <table border="0" cellpadding="5" cellspacing="5" width="90%">
                        <tr>
                             <%--Window Dressing --%>
                            <td align="center" width="20%" class="blackblodtext">
                                Department Name: 
                                <asp:TextBox ID="txtNewDepartmentName" CssClass="altText" runat="server" Text="" ></asp:TextBox>
                                <asp:Label ID="lblRequired" CssClass="lblError" runat="server"></asp:Label>
                                <asp:Button runat="server" ID="btnCreateNewDepartment" Width="100pt" Text="Submit"  OnClick="CreateNewDepartment" ValidationGroup="Submit" /> <%--FB 2796--%>
                                <asp:RequiredFieldValidator ID="reqName1" ControlToValidate="txtNewDepartmentName" ErrorMessage="Required" ValidationGroup="Submit" runat="server" Display="dynamic"></asp:RequiredFieldValidator>
                                <asp:RegularExpressionValidator ID="RegularExpressionValidator4" ControlToValidate="txtNewDepartmentName" Display="dynamic" runat="server" ValidationGroup="Submit" SetFocusOnError="true" ErrorMessage="<br>& < > ' + % \ / ( ) ; ? | ^ = ! ` , [ ] { } : # $ @ ~ and &#34; are invalid characters." ValidationExpression="^(a-z|A-Z|0-9)*[^\\/<>+;?|!`,\[\]{}\x22;=^:@#$%&()'~]*$"></asp:RegularExpressionValidator>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td align="center"> <%--fogbugz case 413--%>
                    <table border="0" cellpadding="5" cellspacing="5" width="90%">
                        <tr>
                            <td align="center" width="20%" class="blackblodtext"><%--FB 2579--%>
                                (To add existing users to this department, make the necessary modifications in user profile.)
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:TextBox ID="txtMultiDepartment" runat="server" Text="" Visible="false"></asp:TextBox>
                </td>
            </tr>
        </table>
    </div>

<img src="keepalive.asp" name="myPic" width="1px" height="1px">
    </form>

<%--code added for Soft Edge button--%>
<script type="text/javascript" src="inc/softedge.js"></script>
    <!-- #INCLUDE FILE="inc/mainbottomNET.aspx" -->
</body>
</html>

