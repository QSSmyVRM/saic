﻿<%--ZD 100147 start--%>
<%--/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.	
*
* You should have received a copy of the myVRM license with	
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/--%><%--ZD 100147 End--%>
<%@ Page Language="C#" Inherits="ns_ManageEntityCode.ManageEntityCode" %><%--ZD 100170--%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> <!-- FB 2050 -->
<meta http-equiv="X-UA-Compatible" content="IE=8" /> <!-- FB 2050 -->

<!-- #INCLUDE FILE="inc/maintopNET.aspx" -->
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Manage Entity Codes</title>
</head>
<body>
    <form id="frmEntityCode" runat="server">
    <asp:ScriptManager ID="EntityScript" runat="server">
    </asp:ScriptManager>
    <div>
        <center>
            <input type="hidden" runat="server" id="hdnLangName" />
            <input type="hidden" runat="server" id="hdnOptionId" />
            <table width="100%">
                <tr>
                    <td align="center">
                        <h3>
                            Manage Entity Codes
                        </h3>
                    </td>
                </tr>
                <tr>
                    <td align="center">
                        <h3>
                            <asp:Label ID="errLabel" runat="server" Text="" CssClass="lblError"></asp:Label>
                           <div id="dataLoadingDIV" align="center"></div><%--ZD 100176--%>
                        </h3>
                    </td>
                </tr>
                <tr>
                    <td height="35">
                    </td>
                </tr>
                <tr>
                    <td align="center">
                        <asp:DataGrid ID="dgEntityCode" runat="server" AutoGenerateColumns="false" BorderColor="Blue"
                            GridLines="None" BorderStyle="Solid" BorderWidth="1" ShowFooter="true" OnEditCommand="EditEntityCode"
                            OnDeleteCommand="DeleteEntityCode" Visible="true" Style="border-collapse: separate"
                            Width="60%">
                            <SelectedItemStyle CssClass="tableBody" />
                            <EditItemStyle CssClass="tableBody" />
                            <AlternatingItemStyle CssClass="tableBody" />
                            <ItemStyle CssClass="tableBody" />
                            <FooterStyle CssClass="tableBody" />
                            <HeaderStyle CssClass="tableHeader" />
                            <Columns>
                                <asp:BoundColumn DataField="RowUID" Visible="false"></asp:BoundColumn>
                                <asp:BoundColumn DataField="OptionID" Visible="false"></asp:BoundColumn>
                                <asp:BoundColumn DataField="DisplayValue" Visible="false"></asp:BoundColumn>
                                <asp:BoundColumn DataField="ID" Visible="false"></asp:BoundColumn>
                                <%--<asp:TemplateColumn ItemStyle-CssClass="blackblodtext" HeaderStyle-CssClass="tableHeader"
                                    ItemStyle-Width="13%" HeaderStyle-HorizontalAlign="Left">
                                    <ItemTemplate>
                                        <asp:Label ID="lblLangID" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.ID") %>'
                                            Visible="false"></asp:Label>
                                        <asp:Label ID="lblLangName" Visible="false" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.name") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateColumn>--%>
                                <asp:TemplateColumn HeaderText="Code" HeaderStyle-CssClass="tableHeader" ItemStyle-CssClass="tableBody">
                                    <ItemTemplate>
                                        <asp:Label ID="lblEntityName" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.DisplayCaption") %>'></asp:Label>
                                    </ItemTemplate>
                                    <EditItemTemplate>
                                        <asp:TextBox ID="txtEntityName" CssClass="altText" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.DisplayCaption") %>'></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="reqName" ValidationGroup="Update" runat="server"
                                            ControlToValidate="txtEntityName" ErrorMessage="Required" Display="dynamic"></asp:RequiredFieldValidator>
                                        <asp:RegularExpressionValidator ID="regItemName1" ControlToValidate="txtEntityName"
                                            Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<br>& < > ' + % \ / ( ) ; ? | ^ = ! ` , [ ] { } : # $ @ ~ <br> and &#34; are invalid characters."
                                            ValidationExpression="^(a-z|A-Z|0-9)*[^\\/<>^+;?|!`,\[\]{}\x22;=:@#$%&()'~]*$"></asp:RegularExpressionValidator>
                                    </EditItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="Description" HeaderStyle-CssClass="tableHeader" ItemStyle-CssClass="tableBody">
                                    <ItemTemplate>
                                        <asp:Label ID="lblEntityDesc" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.HelpText") %>'></asp:Label>
                                    </ItemTemplate>
                                    <EditItemTemplate>
                                        <asp:TextBox ID="txtEntityDesc" CssClass="altText" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.HelpText") %>'></asp:TextBox>
                                        <asp:RegularExpressionValidator ID="regItemName" ControlToValidate="txtEntityDesc"
                                            Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<br>& < > ' + % \ / ( ) ; ? | ^ = ! ` , [ ] { } : # $ @ ~ <br> and &#34; are invalid characters."
                                            ValidationExpression="^(a-z|A-Z|0-9)*[^\\/<>^+;?|!`,\[\]{}\x22;=:@#$%&()'~]*$"></asp:RegularExpressionValidator>
                                    </EditItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="Actions" HeaderStyle-CssClass="tableHeader" ItemStyle-CssClass="tableBody">
                                    <ItemTemplate>
                                        <asp:LinkButton ID="btnEdit" Text="Edit" CommandName="Edit" runat="server"></asp:LinkButton>
                                        <asp:LinkButton ID="btnDelete" Text="Delete" CommandName="Delete" runat="server"></asp:LinkButton>
                                    </ItemTemplate>
                                    <EditItemTemplate>
                                        <asp:LinkButton ID="btnUpdate" Text="Update" CommandName="Update" runat="server"
                                            OnClientClick="javascript:return fnGridValidation()" ValidationGroup="Update"></asp:LinkButton>
                                        <asp:LinkButton ID="btnCancel" Text="Cancel" CommandName="Cancel" runat="server"></asp:LinkButton>
                                    </EditItemTemplate>
                                </asp:TemplateColumn>
                            </Columns>
                        </asp:DataGrid>
                    </td>
                </tr>
                <tr>
                    <td align="center">
                        <table width="100%">
                            <tr id="trDisplay" runat="server">
                                <td width="61%">
                                </td>
                                <td style="color: #666666; font-size: 75%;">
                                    <asp:Label ID="lblDisplay" runat="server" Text=""></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td align="center" colspan="2">
                                    <asp:Table runat="server" ID="tblNoOptions" Visible="false" Width="90%">
                                        <asp:TableRow CssClass="lblError">
                                            <asp:TableCell CssClass="lblError" HorizontalAlign="center">
                                             No Options found.
                                            </asp:TableCell>
                                        </asp:TableRow>
                                    </asp:Table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td height="15">
                    </td>
                </tr>
                <tr>
                    <td align="center">
                        <asp:Button ID="btnCancel" runat="server" Text="Go Back" CssClass="altMedium0BlueButtonFormat" OnClientClick="javascript:DataLoading(1);"
                            OnClick="btnCancel_Click" /> <%--ZD 100176--%> 
                        <asp:Button ID="btnSubmit" runat="server" Text="Create New Entity Code" OnClientClick="javascript:DataLoading(1);"
                            OnClick="CreateNewEntityCode" Width="215px" /> <%-- FB 2050 2796 --%><%--ZD 100176--%>                      
                    </td>
                </tr>
                <tr>
                    <td style="display: none">
                        <asp:Button ID="btnEditHidden" BackColor="Transparent" BorderColor="White" runat="server"
                            OnClick="EditConfByOptionId" />
                        <asp:Button ID="btnConfDeleteHidden" runat="server" BackColor="Transparent" BorderColor="White"
                            OnClick="DeleteConfByOptionId" />
                        <asp:Button ID="btnDeleteHidden" runat="server" BackColor="Transparent" BorderColor="White"
                            OnClick="DeleteConfEntityCode" />
                    </td>
                </tr>
            </table>
        </center>
    </div>

    <script type="text/javascript">
    
    function frmClearValidator()
    {
        var regItemName1 = document.getElementById("regItemName1");
        if(regItemName1 != null)
            regItemName1.innerHTML = "";
        var regItemDesc = document.getElementById("regItemDesc");
        if(regItemDesc != null)
            regItemDesc.innerHTML = "";
    
    }
    
    var globMsg = null;
    var globMode = null;
    var globDel = null;

 
    function FnConfirm2() 
    {
        var msg = globMsg;
        var mode = globMode;
        
        if(msg==null || mode==null)
        {
        return false;
        }
        
        if(confirm(msg))
        {
            var btn;
            if(mode == "E")
            {
                btn = document.getElementById("btnEditHidden");
                btn.click();
            }
            else if(mode == "D")
            {
                btn = document.getElementById("btnConfDeleteHidden");
                btn.click();
            }
            
            return true;
        }
        else
        {
            return false;
        }
    }
    
    //FB 2535 Starts -Code added for SetTimeout() function & Code Commented for fnload
    
    function FnConfirm(xmsg, xmode) {
        globMsg = xmsg;
        globMode = xmode;
        setTimeout('FnConfirm2()', 100);
        //FnConfirm2();
    }

    
    function FnDeleteConfirm2() {

        if (globDel == null)
            return false;

        if (confirm("Are you sure you want to delete this Option?")) {
            var btn;
            btn = document.getElementById("btnDeleteHidden");
            btn.click();
            return true;
        }
        else {
            return false;
        }
    }
    
    
    function FnDeleteConfirm(del) {
        globDel = del;
        setTimeout('FnDeleteConfirm2()', 100);
       // FnDeleteConfirm2(); 
    }

    /*  function fnload()
    {
    FnConfirm2();
    FnDeleteConfirm2();
    }
    
    window.onload = fnload; */

    //FB 2535 Ends
    //ZD 100176 start
    function DataLoading(val) {
        if (val == "1")
            document.getElementById("dataLoadingDIV").innerHTML = "<img border='0' src='image/wait1.gif'>";
        else
            document.getElementById("dataLoadingDIV").innerHTML = "";
    }
    //ZD 100176 End
    
    </script>

    </form>
</body>
</html>
<%--FB 2500--%>
<!-- #INCLUDE FILE="inc/mainbottomNET.aspx" -->
