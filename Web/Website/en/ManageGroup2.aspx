<%--ZD 100147 start--%>
<%--/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.	
*
* You should have received a copy of the myVRM license with	
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/--%><%--ZD 100147 End--%>
<%@ Page Language="C#" AutoEventWireup="true" Inherits="ns_MyVRM.ManageGroup2"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> <!-- FB 2050 -->
<meta http-equiv="X-UA-Compatible" content="IE=8" /> <!-- FB 2050 -->

<!-- #INCLUDE FILE="inc/maintopNET.aspx" --> 

<script runat="server">

</script>
<script type="text/javascript" src="script/mousepos.js"></script>
<script type="text/javascript" src="script/managemcuorder.js"></script>
<script type="JavaScript" src="inc/functions.js"></script>
<script type="text/javascript" src="extract.js"></script> <%--Login Management--%>
<script type="text/javascript" src="script/group2.js"></script>


<script type="text/javascript" language="javascript">

	function frmManagegroup2_Validator()
    {	
        var groupname = document.getElementById('<%=GroupName.ClientID%>');
        if( groupname.value == "")
        {
            var txtReqFieldGName = document.getElementById('<%=ReqFieldGName.ClientID%>');
            txtReqFieldGName.style.visibility = 'visible';
            groupname.focus();
            return false;
        }
        else if (groupname.value.search(/^(a-z|A-Z|0-9)*[^\\/<>^+;?|!`,\[\]{}\x22;=:@#$%&()'~]*$/)==-1)
        {        
            regGName.style.display = 'block';
            groupname.focus();
            return false;
        }           
	    // chk group number
	    if(document.frmManagegroup2.PartysInfo.value == "")  //EDITED for FF
	    {
		    alert(EN_128);
		    return false;
	    }
	    else
	    {
	        if(document.frmManagegroup2.PartysInfo.value == "|") //FB 1914
	        {
		        alert(EN_128);
		        return false;
	        }
	    }
	    if(chkGroupExists(document.frmManagegroup2.GroupName.value) == true) 
	    {	
		    alert("Group already exists! Please enter another name.");
		    document.frmManagegroup2.GroupName.focus();
		    return false;
	    }	    
    	
    	var grpDesc = document.getElementById('<%=GroupDescription.ClientID%>');
	    if (Trim(grpDesc.value) != "")
	    {
	        if (grpDesc.value.search(/^(a-z|A-Z|0-9)*[^\\/<>^+;?|!`,\[\]{}\x22;=:@#$%&()'~]*$/))
            {
                regGrpDescription.style.display = 'block';
                grpDesc.focus();
                return false;
            }
	    }
    	
	    // refresh the party list and calculate the party number
	    willContinue = ifrmMemberlist.bfrRefresh(); 
	    if (!willContinue)
		    return false;
        DataLoading(1); // ZD 100176  
	    return true;
    }
    
    function chkGroupExists(gName)
    {
	    for(var i=0; i< document.frmManagegroup2.Group.options.length; i++)
	    {
		    if (gName.toLowerCase() == document.frmManagegroup2.Group.options(i).text.toLowerCase())
		    {	
			    return true;
		    }
	    }
    }
    
    // FB 2050 Starts
    function refreshIframe()
    {
    var iframeId = document.getElementById('ifrmMemberlist');
    iframeId.src = iframeId.src;
    }
    // FB 2050 Ends
    //ZD 100176 start
    function DataLoading(val) {
     if (val == "1")
        document.getElementById("dataLoadingDIV").innerHTML = "<img border='0' src='image/wait1.gif'>";
     else
        document.getElementById("dataLoadingDIV").innerHTML = "";
        }
        //ZD 100176 End

</script>

  <div id="tblViewDetails" style="display:none">
  </div>
<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server" id="Head1">
    <title>My Groups</title>
    <script type="text/javascript" src="inc/functions.js"></script>
</head>
<body>
    <form id="frmManagegroup2" runat="server" onsubmit="DataLoading('1')"> <%--ZD 100176--%>
    <div>
     <input type="hidden" id="helpPage" value="73" />
        <table width="75%" align="center" cellpadding="4" border="0">
            <tr>
                <td align="center" colspan="4">
                    <h3>
                        <asp:Label ID="lblHeader" runat="server" Text="Create Group"></asp:Label>
                    </h3>
                </td>
            </tr>
            <tr>
                <td align="center" colspan="4" style="width: 1168px">
                    <asp:Label ID="errLabel" runat="server" Text="" CssClass="lblError"></asp:Label>
                </td>
            </tr>
            <tr><div id="dataLoadingDIV" align="center"></div></tr><%--ZD 100176--%>
            <tr>
                <td colspan="4" align="left">
                    <span class="reqfldstarText">*</span>
                    <span class="reqfldText">Required Field</span>
                 </td>
            </tr>
            <tr>
                <td colspan="4">
                  <span class="subtitleblueblodtext" style="margin-left:-20px">Group Information </span>       
                </td>
            </tr>
            <tr>
                 <%--Window Dressing--%>
                <td align="left" style="width:16%;">
                   <span class="blackblodtext"> Group Name</span>
                    <span class="reqfldstarText">*</span>                   
                </td>
                <td colspan="2">
                     <asp:TextBox ID="GroupName" runat="server" CssClass="altText" Width="170"></asp:TextBox>
                     <asp:RequiredFieldValidator ID="ReqFieldGName" Text="Required"  ErrorMessage="Required" ControlToValidate="GroupName" runat="server" SetFocusOnError="true"></asp:RequiredFieldValidator>                     
                    <asp:RegularExpressionValidator ID="regGName" ControlToValidate="GroupName" Display="dynamic" runat="server" ValidationGroup="Submit" SetFocusOnError="true" ErrorMessage="<br>& < > ' + % \ / ( ) ; ? | ^ = ! ` , [ ] { } : # $ @ ~ and &#34; are invalid characters." ValidationExpression="^(a-z|A-Z|0-9)*[^\\/<>^+;?|!`,\[\]{}\x22;=:@#$%&()'~]*$"></asp:RegularExpressionValidator>
                </td>
                <%--Window Dressing--%>
                <td align="left">
                    <span class="blackblodtext">Public Group</span>
                    <asp:CheckBox ID="GroupPublic" runat="server"  />
                </td>
            </tr>
            <tr>
                <td align="left">
                   <span class="blackblodtext">Description&nbsp;</span>
                </td>
                <td colspan="3">
                    <asp:TextBox ID="GroupDescription" TextMode="MultiLine" Width="200" Rows="4" Columns="15" runat="server" CssClass="altText"></asp:TextBox>                    
                    <asp:RegularExpressionValidator ID="regGrpDescription" ControlToValidate="GroupDescription" Display="dynamic" runat="server" ValidationGroup="Submit" SetFocusOnError="true" ErrorMessage="<br>& < > ' + % \ / ( ) ; ? | ^ = ! ` , [ ] { } : # $ @ ~ and &#34; are invalid characters." ValidationExpression="^(a-z|A-Z|0-9)*[^\\/<>^+;?|!`,\[\]{}\x22;=:@#$%&()'~]*$"></asp:RegularExpressionValidator>
                </td>
            </tr>
            <tr>
                <td colspan="4">
		          <span class="subtitleblueblodtext" style="margin-left:-20px">Member Information</span>
		        </td>
		     </tr>
		
		     <tr>
		       
		        <td colspan="4">
		            <table  cellpadding="2" cellspacing="0" width="100%"  height="95">
                      <tr>
                      <td></td>                        
                        <td  width="90%" bordercolor="#0000FF" align="left">
                          <table border="0" cellpadding="2" cellspacing="0" width="100%">
                           <%--Window Dressing start--%>
                            <tr class="tableHeader">
                              <td align="center" style="width:10%" class="tableHeader">DELETE</td><%--FB 2579--%>
                              <td align="center" style="width:50%" class="tableHeader">NAME</td>
                              <td align="center" style="width:40%" class="tableHeader">EMAIL</td>
                          <%--Window Dressing end--%>
                            </tr>
                          </table>
                        </td>
                      </tr>
                      <tr> 
                       <td height="10" align="left" valign="top" width="16%">
                 <span class="blackblodtext">Members</span>
                </td>                     
                        <td bordercolor="#0000FF" align="center">
                          <table  cellpadding="0" cellspacing="0" width="100%" style="height:99">
                            <tr >
                              <td style="width:100%;height:100" valign="top" align="left">                              
                                <!--Added for Group2Member.asp Start-->                             
                                <iframe src="group2member.aspx?wintype=ifr" name="ifrmMemberlist" id="ifrmMemberlist" width="100%" height="100" style="vertical-align:top; "> <%--Edited for FF--%>
                                  <p>go to <a href="group2member.aspx?wintype=ifr">Members</a></p>
                                </iframe> 
                                <!--Added for Group2Member.asp End-->   
                              </td>
                            </tr>
                          </table>
                        </td>
                      </tr>    
                    </table> 		            
		        </td>
		    </tr>
		    <tr >
                <td style="height:62" valign="top" align="left">
                  <span class="blackblodtext">Groups&nbsp;</span>
                </td>
                <td style="width:30%">
                   <asp:ListBox ID="Group" CssClass="altText" style="scrollbar-shadow-color: #DEE3E7;" runat="server" SelectionMode="Multiple" Width="100%">
                    </asp:ListBox>   
                </td>
                <td>
                    <span class="blackblodtext">Click on group name to add. Double-click to show participant details.<br /> To deselect the group press Ctrl + Click</span>
                    <asp:TextBox ID="UsersStr" runat="server" Width="0px" ForeColor="transparent" BackColor="transparent" BorderStyle="None" BorderColor="Transparent"></asp:TextBox>
                    <asp:TextBox ID="PartysInfo" runat="server" Width="0px" ForeColor="Black" BackColor="transparent" BorderStyle="None" BorderColor="Transparent"></asp:TextBox>
                </td>
                <td align="left" style="width:15%">
                <table> <%--Edited for FF--%>
                <tr>
                <td>              
                    <input type="button" name="Managegroup2Submit" value="Remove All" style="width:160px" class="altShortBlueButtonFormat" onclick="deleteAllParty();" lang="JavaScript" />                    
                    </td>
                    </tr>
                <tr>
                <td>
                    <%--code changed for Softedge button--%>
                    <input type="button" onfocus="this.blur()" name="Managegroup2Submit" value="myVRM Look Up" style="width:160px" class="altShortBlueButtonFormat" onclick="getYourOwnEmailList();" lang="JavaScript" />
                 </td>
                </tr>
                </table>
                </td>
            </tr>
            <tr>
                <td colspan="4" align="left">
		          <span class="subtitleblueblodtext" style="margin-left:-20px">Confirm Your Group</span>
                </td>
            </tr>
            <tr>
                <td colspan="4" align="right">                    
                    <%--code changed for Softedge button--%>
                    <input type="button" name="btnCancel" onfocus="this.blur()" class="altShortBlueButtonFormat" value="Cancel" style="width:150px" onclick="javascript:DataLoading('1');window.location.replace('ManageGroup.aspx');" /><%--ZD 100176--%>
                    <asp:Button ID="Managegroup2Submit" onfocus="this.blur()" runat="server" Text="Submit" ValidationGroup="Submit"  style="width:150px" OnClick="Managegroup2Submit_Click" OnClientClick="javascript:return frmManagegroup2_Validator();" CssClass="altShortBlueButtonFormat" />
                </td>
            </tr>
         </table>
      </div>
    </form> 
<%--code added for Soft Edge button--%>
<script type="text/javascript" src="inc/softedge.js"></script>
    <!-- #INCLUDE FILE="inc/mainbottomNET.aspx" -->
</body>
</html>
