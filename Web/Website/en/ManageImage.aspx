<%--ZD 100147 start--%>
<%--/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.	
*
* You should have received a copy of the myVRM license with	
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/--%><%--ZD 100147 End--%>
<%@ Page Language="C#" AutoEventWireup="true" Inherits="ns_ManageImage.ManageImage" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head runat="server">
<script type="text/javascript" src="script/wincheck.js"></script> <%--FB 3055--%>
   <script type="text/javascript"> // FB 2815
    var path = '<%=Session["OrgCSSPath"]%>';
    path = path.split('.')[0] + '<%=Session["ThemeType"]%>' + ".css";
    document.write('<link rel="stylesheet" title="Expedite base styles" type="text/css" href="' + path + '?' + new Date().getTime() + '">');
   </script>   
    <title>myVRM</title>
    <style type="text/css">
	input.prompt
	{
		border: 1 solid transparent; 
		background-color: #99ccff;
		width: 70;
		font-family: arial;
		font-size: 12; 
		color: black;
	} 

	input.longprompt
	{
		border: 1 solid transparent; 
		background-color: #99ccff;
		width: 120;
		font-family: arial;
		font-size: 12; 
		color: black;
	} 
</style>

    <script type="text/javascript" src="inc/functions.js"></script>

    <script type="text/javascript" src="script/errorList.js"></script>

    <link rel="StyleSheet" href="css\myprompt.css" type="text/css" />

    <script type="text/javascript" src="script\uploadfile.js"></script>

    <script type="text/javascript" src="script\mousepos.js"></script>

</head>

<body>
    <form name="frmManageimage" method="Post" action="" id="frmManageimage" runat="server">
        <input type="hidden" name="conffile1" id="conffile1" value="" />
        <input type="hidden" name="conffilepath" id="conffilepath" value="" />
        <input type="hidden" name="fileNM" id="fileNM" value="" runat="server" />
        <input type="hidden" name="RoomImage" id="RoomImage" runat="server" value="" />
        <input type="hidden" name="RoomName" id="RoomName" runat="server" value="" />
        <div align="center" style="height: 50%">
            <table border="0" style="height:50%">
                <tr>
                    <td colspan="3" align="center">
                            <%--code added for Soft Edge button--%>
                        <h3>Room Image Management</h3></td>
                </tr>
                <tr>
                    <td width="167" align="left" valign="top" style="height: 600px">
                        <iframe src="ifrmImageList.aspx" id="ifrmRoomimages" runat="server" width="167" height="70%">
                            <p>
                                Loading page
                            </p>
                        </iframe>
                    </td>
                    <td width="2" style="height: 487px">
                    </td>
                    <td width="450" align="left" valign="top" style="height: 487px">
                        <table border="0" width="100%">
                            <tr>
                                <td colspan="2" align="center" id="RoomImgDIV" width="412" height="309" valign="top" runat="server">
                                    <img width="400" height="300" id="Images" alt="Images" visible="false" src=""/></td>
                                <!--  -->
                            </tr>
                            <tr>
                            <%--code added for Soft Edge button--%>
                                <td align="left" id="ImgNameDIV" class="blackblodtext" style="width: 600px" runat="server">
                                </td>
                                <td align="right" style="width: 73px">
                            <%--code added for Soft Edge button--%>
                                    <asp:Button CssClass="altShortBlueButtonFormat" ID="btnRemove" Text="Remove" runat="server" OnClientClick="javascript:return deleteimg();" />
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2" align="center" style="height: 5px">
                                </td>
                            </tr>
                            <tr>
                            <%--code added for Soft Edge button--%>
                                <td style="width: 500px" class="blackblodtext">
                                    <span class="blackblodtext" id="RmImg1" runat="server">Room Image:</span><input type="file" id="file1" class="altText" runat="server" style="width: 260px" contenteditable="false" />&nbsp;<br />
                                    <img src='image/arrow.gif' border="0" id="img1" runat="server" visible="false" alt="Image1" />
                                    <asp:Label ID="lblUploadFile1" Text="" Visible="false" runat="server"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2" id='dlgcontent'>
                                </td>
                            </tr>
                            <tr>
                                <td align="center" style="width: 600px; height: 26px;">
                            <%--code added for Soft Edge button--%>
                                    <asp:Button CssClass='altShortBlueButtonFormat' name="btnCancel" Text='Cancel' id="btnCancel" runat="server" OnClientClick='window.close();'/>
                                    <asp:Button CssClass="altShortBlueButtonFormat" Text="Submit/Upload" ID="btnUploadFiles" 
                                        runat="server" OnClientClick= "javascript:return getFileExt();" />
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </div>
    </form>

    <script language="JavaScript" type="text/javascript">
<!--
    
    if(document.getElementById("fileNM").value != "")
    {
        var fN = document.getElementById("fileNM").value;
        var rN;
        var iND;
        if(fN != "")
        {
            iND = fN.toString().replace(".jpg","");
            document.getElementById ("ImgNameDIV").innerHTML = "Room : <span class=srcstext3><b><u>" + iND + "</u></b></span>"
            document.getElementById ("RoomImgDIV").innerHTML = "<img style='width:400;height:300' src='image\\room\\"+fN+"'>";
            document.frmManageimage.RoomImage.value = fN;
//            document.getElementById("fileNM").value = "";
            
        }
    }
     else 
    {   //Window Dressing
        document.getElementById ("RoomImgDIV").innerHTML = "<table width=100%><tr><td align='center' class='subtitleblueblodtext'><b>Image Preview Area</b></font></td></tr><tr><td height=10></td></tr><tr><td class='blackblodtext'>Please select one room image from left to preview or delete.</td></tr></table>"
    }
    if(document.getElementById("RoomName").value != null)
      {
          if(document.getElementById("RoomName").value != "")
          {
//              if(document.frmManageimage.file1.value != null)
//                  {
                       if(document.getElementById("fileNM").value != "") 
                       {
                            var rVal = document.getElementById("RoomName").value;
                          if(rVal != "")
                            opener.AddImage(rVal);
                            if(fN == "")
                            document.frmManageimage.RoomImage.value = "";
                            document.getElementById("fileNM").value = ""
                        }
//                  }
           }
      } 
    
    if(document.frmManageimage.RoomImage.value != null)
      { 
          if(document.getElementById("RoomImage").value != "")
            {
                var tmp = "<%=RoomImage.Value%>";
                if(tmp != "")
                {
                    tmp = tmp.toString().replace(".jpg","");
                    opener.DelImage(tmp);
                }
                if(fN == "")
                document.frmManageimage.RoomImage.value = "";
            }
      }
    function deleteimg()
    {
	    if (document.frmManageimage.RoomImage.value == "") 
	    {
		    alert(EN_158);
		    return false;
	    }
                
	    var isRemove = confirm("Are you sure you want to remove this room image permanently?");
	    if (isRemove == true) 
	    {
	    var f = document.getElementById('ifrmRoomimages');
              f.contentWindow.location.reload(false);
	        if(fN != "")
	        {
              var tmp2= document.getElementById("RoomImage").value;
                if(tmp2 != "")
                {
                    tmp2 = tmp2.toString().replace(".jpg","");
                    opener.DelImage(tmp2);
                }
              fN = "";
            }
		    return true;
	    }
	    else
	    return false;
    } 

    
    //-->
    </script>

    <script language="JavaScript" type="text/javascript">
    function getFileExt ()
    {
    var fpn = document.getElementById("file1").value;
    
    if(document.frmManageimage.file1.value == "")
		{
		    var selFil = confirm("Please click Browse to Upload New Room Image");
		    return false;
		}
	    
        if (fpn.indexOf(".") == -1)
		    return "";
    		
    		
	    fpnary = fpn.split(".");

	    var fpn1 = fpnary[fpnary.length-1];
	    
	    if ((fpn1.toLowerCase()) != "jpg") 
	    {
				alert(EN_178);
				//document.frmManageimage.file1.value = "";
				return false;
		}
		
		
		return true;
    }
    </script>
    
</body>

</html>
<%--code added for Soft Edge button--%>
<script type="text/javascript" src="inc/softedge.js"></script>