﻿<%--ZD 100147 start--%>
<%--/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.	
*
* You should have received a copy of the myVRM license with	
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/--%><%--ZD 100147 End--%>
<%@ Page Language="C#" AutoEventWireup="true" Inherits="ns_MyVRM.ManageMessages" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<meta http-equiv="X-UA-Compatible" content="IE=8" />
<!-- #INCLUDE FILE="inc/maintopNET.aspx" -->

<script type="text/javascript">

    function fnDelete(arg)
    {  
        if(arg == true)
            return false;
            
        var msg = "Are you sure you want to delete this message?";
        if (confirm(msg))
            return true;
        else
            return false;            
    }

    function fnClear()
    {
        var lblError = document.getElementById("lblError");
        lblError.innerText = "";
    }

    ExpandCollapse(document.getElementById("imgLangsOption"), "<%=LangsOptionRow.ClientID %>", true);

    function maxCharRowShow() 
    {
        document.getElementById("txtMaxChar").style.display = "block";
    }

    function ExpandCollapse(img, str, frmCheck) 
    {
        obj = document.getElementById(str);
        if (obj != null) {
            if (frmCheck == true) {
                img.src = img.src.replace("minus", "plus");
                obj.style.display = "none";
            }
            if (frmCheck == false) {
                if (img.src.indexOf("minus") >= 0) {
                    img.src = img.src.replace("minus", "plus");
                    obj.style.display = "none";
                }
                else {
                    img.src = img.src.replace("plus", "minus");
                    obj.style.display = "";
                }
            }
        }
    }
    
    function frmValidator() {
        document.getElementById("txtMaxChar").style.display = "none";
        var txtentityname = document.getElementById('<%=txtTxtMsg.ClientID%>');
        if (txtentityname.value == "") {
            reqEntityName.style.display = 'block';
            txtentityname.focus();
            return false;
        } 
        else if (txtentityname.value.search(/^(a-z|A-Z|0-9)*[^\\<>^+;?|!`,\[\]{}\x22;=:@#$%&()'~]*$/) == -1) {
            regItemName1.style.display = 'block';
            txtentityname.focus();
            return false;
        }
        return (true);
    }

    function fnCancel() //FB 2565
    {
        DataLoading(1); //ZD 100176
        window.location.replace('OrganisationSettings.aspx');  //CSS Project
    }
    //ZD 100176 start
    function DataLoading(val) {
        if (val == "1")
            document.getElementById("dataLoadingDIV").innerHTML = "<img border='0' src='image/wait1.gif'>";
        else
            document.getElementById("dataLoadingDIV").innerHTML = "";
    }
    //ZD 100176 End
</script>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Manage Messages</title>

    <script type="text/javascript" src="script/mousepos.js"></script>

    <script type="text/javascript" src="script/managemcuorder.js"></script>

    <script type="text/javascript" src="inc/functions.js"></script>

    <link rel="StyleSheet" href="css/divtable.css" type="text/css" />
    <link rel="stylesheet" title="Expedite base styles" type="text/css" href="css/border-table.css" />
    <link rel="stylesheet" title="Expedite base styles" type="text/css" href="css/main-table.css" />
    <%--<link rel="stylesheet" title="Expedite base styles" type="text/css" href="css/main.css" />--%>
</head>
<body>
    <form id="form1" runat="server" onsubmit="DataLoading(1);"><%--ZD 100176--%> 
    <div>
        <input type="hidden" id="helpPage" value="65" />
        <table width="50%" align="center">
            <tr>
                <td align="center">
                    <h3>
                        <asp:Label ID="lblHeader" runat="server" Text="Active Message Delivery"></asp:Label>
                        <div id="dataLoadingDIV" align="center"></div><%--ZD 100176--%>
                    </h3><%--FB 2934--%>
                </td>
            </tr>
            <tr>
                <td align="center">
                    <asp:Label ID="lblError" runat="server" Text="" CssClass="lblError"></asp:Label>
                </td>
            </tr>
            <tr id="trTextMsg" runat="server" align="center">
                <td align="center">
                    <table width="755px" cellspacing="0" cellpadding="2" border="0">
                        <tr style="display:none;">
                            <td align="left" class="subtitleblueblodtext" colspan="4">
                                Text Messages
                                <br />
                            </td>
                        </tr>
                        <tr style="display:none;">
                            <td colspan="4"  >
                                <table class="tableHeader" border="0" width="100%">
                                    <tr>
                                        <td style="width: 135px" align="left" valign="top" class="blackblodtext">
                                            <b>Language Name</b>
                                        </td>
                                        <td align="left" valign="top" style="width: 230px" class="blackblodtext">
                                            <b>Message</b><span class="reqfldstarText">*</span>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td valign="top" style="width: 13px; display:none;">
                                <br />
                                <asp:ImageButton ID="imgLangsOption" runat="server" ImageUrl="image/loc/nolines_minus.gif"
                                    Height="20" Width="20" vspace="0" hspace="0" ToolTip="View Other Languages" />
                            </td>
                            <td style="width: 130.5px" align="left" valign="top">
                                <br />
                                <a class="blackblodtext">Enter Message</a>
                            </td>
                            <td align="left" valign="top" >
                                <br />
                                <asp:TextBox ID="txtTxtMsgID" Text="new" Visible="false" runat="server" Width="32px"></asp:TextBox>
                                <asp:TextBox ID="txtTxtMsg" runat="server" Width="470px" CssClass="altText" ValidationGroup="Upload" MaxLength="180"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="reqTxtMs" ValidationGroup="Upload" runat="server"
                                    ControlToValidate="txtTxtMsg" ErrorMessage="Required" Display="dynamic"></asp:RequiredFieldValidator>
                                <asp:RegularExpressionValidator ID="regTxtMs" ControlToValidate="txtTxtMsg" Display="dynamic"
                                   ValidationGroup="Upload" runat="server" SetFocusOnError="true" ErrorMessage="<br> & < and > are invalid characters."
                                    ValidationExpression="^[^<>&]*$"></asp:RegularExpressionValidator>
                            </td>                           
                        </tr>
                        <tr id="LangsOptionRow" runat="server" style="display:none;">
                            <td>
                            </td>
                            <td colspan="2" style="text-align: left">
                                <asp:DataGrid ID="dgLangOptionlist" runat="server" AutoGenerateColumns="false" GridLines="None"
                                    CellPadding="1" CellSpacing="0" Style="border-collapse: separate" ShowHeader="false">
                                    <ItemStyle Height="2" VerticalAlign="Top" />
                                    <Columns>
                                        <asp:TemplateColumn ItemStyle-CssClass="blackblodtext" ItemStyle-Width="131px" >
                                            <ItemTemplate>
                                                <asp:Label ID="lblLangID" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.ID") %>'
                                                    Visible="false"></asp:Label>
                                                <asp:Label ID="lblLangName" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.name") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn ItemStyle-Width="230px" ItemStyle-Height="30px">
                                            <ItemTemplate>
                                                <asp:TextBox ID="txtMsgID" Text="new" Visible="false" runat="server" Width="32px"></asp:TextBox>
                                                <asp:TextBox ID="txtMsg" runat="server" Width="470px" CssClass="altText" MaxLength="180"
                                                    Text=""></asp:TextBox>
                                                <asp:RegularExpressionValidator ID="regItemName1" ControlToValidate="txtMsg" Display="dynamic"
                                                   ValidationGroup="Upload" runat="server" SetFocusOnError="true" ErrorMessage="<br> & < and > are invalid characters."
                                                    ValidationExpression="^[^<>&]*$"></asp:RegularExpressionValidator>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                    </Columns>
                                </asp:DataGrid>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="3" align="center">
                                 <br />
                                <asp:Button ID="btnNewMessage" runat="server" 
                                    ValidationGroup="Upload" OnClientClick="javascript:fnClear()" Text="Submit" Width="180px"
                                    OnClick="SetMessages" /> <%--FB 2796--%>
                            </td>
                        </tr>
                        <tr>
                            <td id="txtMaxChar" runat="server" colspan="4" style="font-size: xx-small; color: Red;display: none" align="right">
                                Maximum 180 Characters
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr style="width: 100%" align="center">
                <td align="center" style="width: 100%">
                    <table border="0" style="width: 755px" cellpadding="2" cellspacing="0">
                        <tr id="trTxtMsgDisplay" align="left" runat="server" style="width: 100%">
                            <td align="left" style="width: 100%">
                                <asp:DataGrid BorderColor="blue" BorderStyle="solid" BorderWidth="1" ID="dgTxtMsg" OnPageIndexChanged="dgTxtMsg_PageIndexChanged"
                                    AllowPaging="true" PageSize="10" PagerStyle-HorizontalAlign="Right" PagerStyle-Mode="NumericPages" 
                                    AutoGenerateColumns="false" OnEditCommand="EditItem" OnDeleteCommand="DeleteItem"
                                    OnCancelCommand="CancelItem" OnUpdateCommand="UpdateItem" runat="server" Width="750Px"
                                    GridLines="None" Style="border-collapse: separate" OnItemCreated="BindRowsDeleteMessage"
                                    OnItemDataBound="BindMessages" >
                                    <HeaderStyle Height="30" CssClass="tableHeader" HorizontalAlign="Center" />
                                    <AlternatingItemStyle CssClass="tableBody" />
                                    <ItemStyle CssClass="tableBody" />
                                    <FooterStyle CssClass="tableBody" />
                                    <Columns>
                                        <asp:TemplateColumn>
                                             <ItemTemplate>            
                                                 <asp:Label ID="lblID" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.ID") %>' Visible="false"></asp:Label>   
                                                 <asp:Label ID="lblMsgID" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.MsgID") %>' Visible="false"></asp:Label>   
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn ItemStyle-CssClass="blackblodtext" HeaderStyle-CssClass="tableHeader"
                                            ItemStyle-Width="143" HeaderText="Language Name" HeaderStyle-HorizontalAlign="Left" Visible="false">
                                            <ItemTemplate>            
                                                 <asp:Label ID="lblLangID" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.LangID") %>'
                                                    Visible="false"></asp:Label>                                    
                                                <asp:Label ID="lblLangName" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.LangName") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderText="Active Message Delivery" HeaderStyle-CssClass="tableHeader"
                                            ItemStyle-CssClass="tableBody"><%--FB 2934--%>
                                            <ItemTemplate>
                                                <asp:Label ID="lblTxtMsg" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.TxtMsg") %>'></asp:Label>                                                
                                            </ItemTemplate>
                                            <EditItemTemplate>
                                                <asp:TextBox ID="txtTxtMsg" Width="300px" CssClass="altText" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.TxtMsg") %>'
                                                MaxLength="180" > </asp:TextBox>                                                
                                                <asp:RequiredFieldValidator ID="reqName" ValidationGroup="Update" runat="server"
                                                    ControlToValidate="txtTxtMsg" ErrorMessage="Required" Display="dynamic"></asp:RequiredFieldValidator>
                                                <asp:RegularExpressionValidator ID="regItemName1" ControlToValidate="txtTxtMsg" Display="dynamic"
                                                    ValidationGroup="Update" runat="server" SetFocusOnError="true" ErrorMessage="<br> & < and > are invalid characters."
                                                    ValidationExpression="^[^<>&]*$"></asp:RegularExpressionValidator>
                                            </EditItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderText="Actions" HeaderStyle-CssClass="tableHeader" ItemStyle-CssClass="tableBody"
                                        ItemStyle-Wrap="false">
                                            <ItemTemplate>
                                                <asp:LinkButton ID="btnEdit" Text="Edit" CommandName="Edit" runat="server"></asp:LinkButton>
                                                <asp:LinkButton ID="btnDelete" Text="Delete" CommandName="Delete" runat="server"></asp:LinkButton>
                                            </ItemTemplate>
                                            <EditItemTemplate>
                                                <asp:LinkButton ID="btnUpdate" Text="Update" CommandName="Update" runat="server" ValidationGroup="Update"></asp:LinkButton>
                                                <asp:LinkButton ID="btnCancel" Text="Cancel" CommandName="Cancel" runat="server"></asp:LinkButton>
                                            </EditItemTemplate>
                                        </asp:TemplateColumn>
                                    </Columns>
                                </asp:DataGrid>                                
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
            <td align="right">
            <input type="button" id="Close" class="altMedium0BlueButtonFormat" value="Go Back" onclick="fnCancel()" /> <%--FB 2565--%>
            </td>
            </tr>
        </table>
    </div>
    </form>
</body>
</html>
<!-- #INCLUDE FILE="inc/mainbottomNET.aspx" -->
