﻿<%--ZD 100147 start--%>
<%--/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.	
*
* You should have received a copy of the myVRM license with	
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/--%><%--ZD 100147 End--%>
<%@ Page Language="C#" Inherits="ns_MyVRM.ManageOrganization" Buffer="true" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> <!-- FB 2050 -->
<meta http-equiv="X-UA-Compatible" content="IE=8" /> <!-- FB 2050 -->

<!-- #INCLUDE FILE="inc/maintopNET.aspx" --> 
<script type="text/javascript" src="inc/functions.js"></script>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <script language="javascript" type="text/javascript">
        function FnCancel()
		{
		    DataLoading(1); // ZD 100176
			window.location.replace('SuperAdministrator.aspx');
		}
		//ZD 100176 start
		function DataLoading(val) {
		    if (val == "1")
		        document.getElementById("dataLoadingDIV").innerHTML = "<img border='0' src='image/wait1.gif'>";
		    else
		        document.getElementById("dataLoadingDIV").innerHTML = "";
		}
		//ZD 100176 End
    </script>
    <title>Manage Organization</title>
</head>
<body>
    <form id="frmManageOrganization" runat="server">
    
    <div>
        <table cellpadding="0" cellspacing="0" id="OuterTable" width="100%" border="0">
            <tr>
                <td align="center">
                    <h3>
                        <asp:Label ID="lblHeader" runat="server" Text="Manage Organizations"></asp:Label><!-- FB 2570 -->
                    </h3>
                </td>
            </tr>
            <tr>
                <td align="center">
                    <asp:Label ID="errLabel" runat="server" Text="" CssClass="lblError"></asp:Label>
                    <div id="dataLoadingDIV" align="center"></div> <%--ZD 100176--%>
                </td>
            </tr>
            <tr>
                <td align="Left">
                    <table width="100%">
                        <tr>
                            <td >&nbsp;</td>
                            <td>
                                <SPAN class="subtitleblueblodtext"></SPAN>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td align="center">
                    <asp:DataGrid ID="dgOrganizations" runat="server" AutoGenerateColumns="False" CellPadding="3" GridLines="None"
                         BorderColor="blue" BorderStyle="solid" BorderWidth="1" OnEditCommand="EditOrganizationProfile" OnDeleteCommand="DeleteOrganizationProfile" OnCancelCommand="PurgeNow_Click"  OnItemDataBound="BindRowsDeleteMessage" ShowFooter="false" Width="90%" Visible="true" style="border-collapse:separate"><%-- Edited for FF--%> <%--FB 1753 //FB 2074--%>
                        <%--Window Dressing - Start--%>                        
                        <SelectedItemStyle CssClass="tableBody" />
                        <EditItemStyle CssClass="tableBody" />
                        <AlternatingItemStyle CssClass="tableBody" />
                        <ItemStyle CssClass="tableBody" />
                         <%--Window Dressing - End--%> 
                        <HeaderStyle CssClass="tableHeader" HorizontalAlign="left" />
                        <Columns>
                            <asp:BoundColumn DataField="orgId" Visible="false" ><HeaderStyle CssClass="tableHeader" /></asp:BoundColumn> 
                            <asp:BoundColumn DataField="organizationName" ItemStyle-CssClass="tableBody"  HeaderStyle-CssClass="tableHeader" ItemStyle-HorizontalAlign="Left" HeaderText="Organization Name"></asp:BoundColumn>
                            <asp:BoundColumn DataField="phone" ItemStyle-CssClass="tableBody" HeaderStyle-CssClass="tableHeader" ItemStyle-HorizontalAlign="Left" HeaderText="Phone"></asp:BoundColumn>
                            <asp:BoundColumn DataField="emailID" ItemStyle-CssClass="tableBody" HeaderStyle-CssClass="tableHeader" ItemStyle-HorizontalAlign="Left" HeaderText="Email ID"></asp:BoundColumn>
                            <asp:TemplateColumn HeaderText="Actions" ItemStyle-CssClass="tableBody" HeaderStyle-HorizontalAlign="Center">
                                <HeaderStyle CssClass="tableHeader" HorizontalAlign="Center" />
                                <ItemTemplate>
                                    <table width="50%" align="center">
                                        <tr>
                                            <td>
                                                <asp:LinkButton ID="lnkEdit" runat="server" Text="Edit" CommandName="Edit" OnClientClick="DataLoading(1)"></asp:LinkButton>  <%--ZD 100176--%> 
                                            </td>
                                            <td>
                                                <asp:LinkButton ID="lnkDelete" runat="server" Text="Delete" CommandName="Delete" Visible='<%# !DataBinder.Eval(Container, "DataItem.orgId").ToString().Trim().Equals("11")%>' OnClientClick="DataLoading(1)"></asp:LinkButton> <%--ZD 100176--%>  <%--ZD 100263--%>
                                            </td>
                                            <td>

                                                <asp:LinkButton ID="lnkPurge" runat="server" Text="Purge" CommandName="Cancel"  Visible='<%# !DataBinder.Eval(Container, "DataItem.orgId").ToString().Trim().Equals("11")%>' OnClientClick="DataLoading(1)"></asp:LinkButton><%--ZD 100176--%>  <%--ZD 100263--%>
                                            </td>
                                        </tr>
                                    </table>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                        </Columns>
                    </asp:DataGrid>
                    <asp:Table runat="server" ID="tblNoOrganizations" Visible="false" Width="90%">
                        <asp:TableRow CssClass="lblError">
                            <%--Windows Dressing--%>
                            <asp:TableCell CssClass="lblError" HorizontalAlign="center">
                                No Organization(s) found.  <%--Edited for FB 1405--%>
                            </asp:TableCell>
                        </asp:TableRow>
                    </asp:Table>
                    <table cellspacing="0" cellpadding="0" width="90%" border="0" align="center">
                        <tr>
                            <td width="60%">&nbsp;</td>
                            <td class="blackblodtext" align="right" nowrap>Total Organizations:&nbsp;
                                <span id="SpnActiveOrgs" runat="server" class="blackblodtext"></span>
                            </td>
                            <td class="blackblodtext" align="right" nowrap>License Remaining:&nbsp;
                                <span id="SpnLicense" runat="server" class="blackblodtext"></span>
                            </td>
                        </tr>
                    </table>                    
                </td>
            </tr>
            <tr>
                <td align="center" width="90%"><br />
                    <table cellspacing="0" cellpadding="1" width="90%" border="0">
                        <tr>
                            <td>
                                <SPAN class=subtitleblueblodtext>Create New Organization</SPAN>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td align="center"><br />
                    <table cellspacing="0" cellpadding="3" width="90%" border="0" align="center">
                        <tr >
                            <td align="right">
								<input class="altMedium0BlueButtonFormat" onclick="FnCancel()" type="button" value="Cancel" name="btnCancel">
								&nbsp;&nbsp;
								<asp:Button ID="btnNewOrganization"  Width="100pt" runat="server"  Text="Submit" OnClick="CreateNewOrganization"  OnClientClick="javascript:DataLoading('1');"/> <%--FB 2664--%> <%--ZD 100176--%> <%--ZD 100176--%> 
                            </td>
                            
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
         <script type="text/javascript" src="inc/softedge.js"></script>
    </div>
    </form>
</body>
</html>
<!-- #INCLUDE FILE="inc/mainbottomNET.aspx" -->
