﻿<%--ZD 100147 start--%>
<%--/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.	
*
* You should have received a copy of the myVRM license with	
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/--%><%--ZD 100147 End--%>
<%@ Page Language="C#" AutoEventWireup="true" Inherits="ns_OrgProfile.ManageOrganizationProfile"
    Buffer="true" %><%--ZD 100170--%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<meta http-equiv="X-UA-Compatible" content="IE=8" />
<!-- FB 2050 -->
<!-- #INCLUDE FILE="inc/maintopNET.aspx" -->

<script type="text/javascript" src="inc/functions.js"></script>

<html xmlns="http://www.w3.org/1999/xhtml">
<%--FB 2678 start--%>
<%--FB 2693 start--%>
<style type="text/css">
    .txtStyle
    {
        background-color: Transparent;
        border-style: none;
    }
</style>
<%--FB 2678 start--%>

<script type="text/javascript" language="javascript">
function fnCheckOrg()
{
    var txtorgname = document.getElementById('<%=txtOrgName.ClientID%>');
    var reqOrgName = document.getElementById('<%=reqOrgName.ClientID%>');
    var regOrgName = document.getElementById('<%=regOrgName.ClientID%>');
    var txtzipcode = document.getElementById('<%=txtZipCode.ClientID%>');
    var regZipCode = document.getElementById('<%=regZipCode.ClientID%>');
    var txtPhone = document.getElementById('<%=txtPhoneNumber.ClientID%>');
    var regPhone = document.getElementById('<%=regPhone.ClientID%>');
    var txtfaxNumber = document.getElementById('<%=txtFaxNumber.ClientID%>');
    var regFaxNumber = document.getElementById('<%=regFaxNumber.ClientID%>');
    var txtemailID = document.getElementById('<%=txtEmailID.ClientID%>');
    var regEmailID = document.getElementById('<%=regEmailID.ClientID%>');
    var txtorgwebsite = document.getElementById('<%=txtOrgWebsite.ClientID%>');
    var regWebsite = document.getElementById('<%=regWebsite.ClientID%>');
    var txtCity = document.getElementById('<%=txtCity.ClientID%>');
    var regCity = document.getElementById('<%=regCity.ClientID%>');
        if(txtorgname.value == '')
        {        
            reqOrgName.style.display = 'block';
            regOrgName.style.display = 'none';
            regZipCode.style.display = 'none';
            regPhone.style.display = 'none';
            regFaxNumber.style.display = 'none';
            regEmailID.style.display = 'none';
            regWebsite.style.display = 'none';
            regCity.style.display = 'none';
            txtorgname.focus();
            return false;
        }
        else if(txtorgname.value != '' && txtorgname.value.search(/^(a-z|A-Z|0-9)*[^\\/<>+;?|!`,\[\]{}\x22;=^:@#$%&()'~]*$/)==-1)
        {
            regOrgName.style.display = 'block';
            reqOrgName.style.display = 'none';
            regZipCode.style.display = 'none';
            regPhone.style.display = 'none';
            regFaxNumber.style.display = 'none';
            regEmailID.style.display = 'none';
            regWebsite.style.display = 'none';
            regCity.style.display = 'none';
            return false;
        }
        //FB 2222
        if (txtzipcode.value != '' && txtzipcode.value.search(/^(a-z|A-Z|0-9)*[^\\/<>+;?|!`,\[\]{}\x22;=^:@#$%&()'~]*$/)==-1)
        {        
            regZipCode.style.display = 'block';
            regOrgName.style.display = 'none';
            reqOrgName.style.display = 'none';
            regPhone.style.display = 'none';
            regFaxNumber.style.display = 'none';
            regEmailID.style.display = 'none';
            regWebsite.style.display = 'none';
            regCity.style.display = 'none';
            txtzipcode.focus();
            return false;
        }  
        
        if(txtPhone.value != '' && txtPhone.value.search(/^(\(|\d| |-|\))*$/)==-1)
        {
            regPhone.style.display = 'block';
            regZipCode.style.display = 'none';
            regOrgName.style.display = 'none';
            reqOrgName.style.display = 'none';
            regFaxNumber.style.display = 'none';
            regEmailID.style.display = 'none';
            regWebsite.style.display = 'none';
            regCity.style.display = 'none';
            txtPhone.focus();
            return false;
        }
    
        if(txtfaxNumber.value != '' && txtfaxNumber.value.search(/^(\(|\d| |-|\))*$/)==-1)
        {
            regFaxNumber.style.display = 'block';
            regPhone.style.display = 'none';
            regZipCode.style.display = 'none';
            regOrgName.style.display = 'none';
            reqOrgName.style.display = 'none';
            regEmailID.style.display = 'none';
            regWebsite.style.display = 'none';
            regCity.style.display = 'none';
            txtfaxNumber.focus();
            return false;
        }
    
        if(txtemailID.value != '' && txtemailID.value.search(/^[a-zA-Z][\w\.-]*[a-zA-Z0-9]@[a-zA-Z0-9][\w\.-]*[a-zA-Z0-9]\.[a-zA-Z][a-zA-Z\.]*[a-zA-Z]$/)==-1)
        {
            regEmailID.style.display = 'block';
            regFaxNumber.style.display = 'none';
            regPhone.style.display = 'none';
            regZipCode.style.display = 'none';
            regOrgName.style.display = 'none';
            reqOrgName.style.display = 'none';
            regWebsite.style.display = 'none';
            regCity.style.display = 'none';
            txtemailID.focus();
            return false;
        }
    
        if(txtorgwebsite.value != '' && txtorgwebsite.value.search(/^(a-z|A-Z|0-9)*[^<>+;|!`,\[\]{}\x22;=^@#$%()'~]*$/)==-1)
        {
            regWebsite.style.display = 'block';
            regEmailID.style.display = 'none';
            regFaxNumber.style.display = 'none';
            regPhone.style.display = 'none';
            regZipCode.style.display = 'none';
            regOrgName.style.display = 'none';
            reqOrgName.style.display = 'none';
            regWebsite.style.display = 'none';
            regCity.style.display = 'none';
            txtorgwebsite.focus();
            return false;
        }
        
        if(txtCity.value != '' && txtCity.value.search(/^(a-z|A-Z|0-9)*[^\\/<>+;?|!`,\[\]{}\x22;=^:@#$%&()'~]*$/)==-1)
        
        {
            regWebsite.style.display = 'none';
            regEmailID.style.display = 'none';
            regFaxNumber.style.display = 'none';
            regPhone.style.display = 'none';
            regZipCode.style.display = 'none';
            regOrgName.style.display = 'none';
            reqOrgName.style.display = 'none';
            regWebsite.style.display = 'none';
            regCity.style.display = 'block';
            txtCity.focus();
            return false;
        
        }
        DataLoading(1);// ZD 100176
        return (true);
}
function fnGoBack()
{
    //url = "ManageOrganization.aspx";  //FB 2565
    DataLoading(1); // ZD 100176
    window.location.replace('ManageOrganization.aspx');
    return;
}
//FB 2659 Starts
    function isNumberKey(evt)
      {
         var charCode = (evt.which) ? evt.which : event.keyCode
         if (charCode > 31 && (charCode < 48 || charCode > 57))
            return false;

         return true;
      }
//FB 2659 Ends
// FB 2693 Start
function ExpandCollapse(img, str, frmCheck) 
{

 obj = document.getElementById(str);
 
         if (obj != null) 
         {
            if (frmCheck == true) 
            {
                if (document.getElementById("chkExpandCollapse").checked)
                 {
                    img.src = img.src.replace("minus", "plus");
                    obj.style.display = "none";
                 }
                else 
                {
                    img.src = img.src.replace("plus", "minus");
                    obj.style.display = "";
                }
            }
            if (frmCheck == false) 
            {
                if (img.src.indexOf("minus") >= 0)
                 {
                    img.src = img.src.replace("minus", "plus");
                    obj.style.display = "none";
                 }
                else 
                {
                    img.src = img.src.replace("plus", "minus");
                    obj.style.display = "";
                }
            }
        }
  }            
    // FB 2693 End
    
    //ZD 100176 start
		function DataLoading(val) {
		    if (val == "1")
		        document.getElementById("dataLoadingDIV").innerHTML = "<img border='0' src='image/wait1.gif'>";
		    else
		        document.getElementById("dataLoadingDIV").innerHTML = "";
	}
    //ZD 100176 End

</script>

<head runat="server">
    <title>Manage Organization Profile</title>
</head>
<body>
    <form id="frmOrgProfile" runat="server" method="post" enctype="multipart/form-data" onsubmit="DataLoading(1)"
    defaultbutton="btnSubmit"> <%--ZD 100176--%>
    <%--FB 1840--%>
    <input type="hidden" runat="server" id="hdnOrganizationID" />
    <asp:TextBox ID="txtOrgID" Style="width: 0" Height="0" runat="server" BorderStyle="none"
        BorderWidth="0"></asp:TextBox>
    <div>
        <center>
            <div id="dataLoadingDIV" style="z-index: 1" align="center"><%--ZD 100176--%> 
            </div>
            <h3>
                <asp:Label ID="lblTitle" runat="server" CssClass="h3" Text=""></asp:Label>
            </h3>
            <asp:Label ID="errLabel" runat="server" CssClass="lblError"></asp:Label>
        </center>
        <table cellpadding="0" cellspacing="0" border="0" style="width: 100%">
            <tr>
                <td align="center">
                    <table id="tblBasicDetails" cellpadding="2" cellspacing="2" border="0" style="width: 95%">
                        <tr>
                            <td align="left">
                                <span class="subtitleblueblodtext" style="margin-left: -15px">Basic Details</span>
                            </td>
                            <td class="reqfldText" align="center">
                                * Required Field
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <table id="tblOrgBasicDetails" cellpadding="2" cellspacing="2" border="0" style="width: 95%">
                                    <tr>
                                        <%--FB 2579 Start--%>
                                        <td style="width: 15%" align="left" valign="top" class="blackblodtext">
                                            Name
                                            <%--FB 2693--%>
                                            <span class="reqfldText">*</span>
                                        </td>
                                        <%-- FB 1839 Start--%>
                                        <td style="width: 25%" align="left" valign="top">
                                            <asp:TextBox ID="txtOrgName" runat="server" CssClass="altText"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="reqOrgName" runat="server" ControlToValidate="txtOrgName"
                                                Display="Dynamic" SetFocusOnError="true" Text="Required"></asp:RequiredFieldValidator>
                                            <asp:RegularExpressionValidator ID="regOrgName" ControlToValidate="txtOrgName" Display="dynamic"
                                                runat="server" SetFocusOnError="true" ErrorMessage="<br>& < > ' + % \ / ( ) ; ? | ^ = ! ` , [ ] { } : # $ @ ~ and &#34; are invalid characters."
                                                ValidationExpression="^(a-z|A-Z|0-9)*[^\\/<>+;?|!`,\[\]{}\x22;=^:@#$%&()'~]*$"></asp:RegularExpressionValidator>
                                        </td>
                                        <%-- FB 2678 Start--%><%--FB 2659--%>
                                        <td id="tdOrgExpiry" runat="server" style="width: 20%" align="left" valign="top"
                                            class="blackblodtext">
                                            Account Expiration key
                                            <%--FB 2693--%>
                                        </td>
                                        <td style="width: 25%" align="left" valign="top">
                                            <asp:TextBox ID="txtOrgexpirykey" runat="server" CssClass="altText" TextMode="MultiLine"></asp:TextBox>
                                            <table id="tbllicense" style="display: none" runat="server" width="100%" cellspacing="3"
                                                cellpadding="3" border="0">
                                                <tr>
                                                    <td width="30%" valign="top" nowrap>
                                                        <span class="blackblodtext">Status: </span>
                                                        <asp:Label ID="ActStatus" runat="server"></asp:Label>
                                                        <asp:Label ID="EncrypTxt" runat="server" Style="display: none;"></asp:Label>
                                                    </td>
                                                    <td width="70%" id="ExportTD" runat="server" style="display: none;" align="left">
                                                        <asp:Button ID="TxyButton" runat="server" CssClass="altMedium0BlueButtonFormat" OnClick="btnExportTxt_Click"
                                                            Text="Export to Text" />
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                        <%-- FB 2678 End--%>
                                    </tr>
                                    <%--FB 2678 starts--%>
                                    <tr id="trexpirydate" runat="server" width="100%">
                                        <td style="width: 15%">
                                            &nbsp;
                                        </td>
                                        <td style="width: 25%">
                                            &nbsp;
                                        </td>
                                        <td style="width: 15%" align="left" class="blackblodtext">
                                            Account Expiry Date
                                        </td>
                                        <td style="width: 25%" align="left">
                                            <asp:TextBox ID="txtExpiryDate" ReadOnly="true" runat="server" CssClass="txtStyle"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <%--FB 2678 End--%>
                                    <tr>
                                        <td style="width: 15%" align="left" valign="top" class="blackblodtext">
                                            Address 1
                                        </td>
                                        <td style="width: 25%" align="left" valign="top">
                                            <asp:TextBox ID="txtAddress1" runat="server" TextMode="MultiLine" CssClass="altText"></asp:TextBox>
                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator2" ControlToValidate="txtAddress1"
                                                Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<br>& < > ' + % \ / ( ) ; ? | ^ = ! ` , [ ] { } : # $ @ ~ and &#34; are invalid characters."
                                                ValidationExpression="^(a-z|A-Z|0-9)*[^\\/<>+;?|!`,\[\]{}\x22;=^:@$%&()'~]*$"></asp:RegularExpressionValidator>
                                        </td>
                                        <%-- FB 2678 Start--%>
                                        <td style="width: 15%" align="left" valign="top" class="blackblodtext">
                                            Email ID
                                        </td>
                                        <td style="width: 25%" align="left" valign="top">
                                            <asp:TextBox ID="txtEmailID" runat="server" CssClass="altText"></asp:TextBox>
                                            <asp:RegularExpressionValidator ID="regEmailID" ControlToValidate="txtEmailID" runat="server"
                                                Display="Dynamic" SetFocusOnError="true" ErrorMessage="Email ID should be Example@exp.com format."
                                                ValidationExpression="^[a-zA-Z][\w\.-]*[a-zA-Z0-9]@[a-zA-Z0-9][\w\.-]*[a-zA-Z0-9]\.[a-zA-Z][a-zA-Z\.]*[a-zA-Z]$"></asp:RegularExpressionValidator>
                                        </td>
                                        <%-- FB 2678 End--%>
                                    </tr>
                                    <tr>
                                        <td style="width: 15%" align="left" valign="top" class="blackblodtext">
                                            Address 2
                                        </td>
                                        <td style="width: 25%" align="left" valign="top">
                                            <asp:TextBox ID="txtAddress2" runat="server" TextMode="MultiLine" CssClass="altText"></asp:TextBox>
                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator1" ControlToValidate="txtAddress2"
                                                Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<br>& < > ' + % \ / ( ) ; ? | ^ = ! ` , [ ] { } : # $ @ ~ and &#34; are invalid characters."
                                                ValidationExpression="^(a-z|A-Z|0-9)*[^\\/<>+;?|!`,\[\]{}\x22;=^:@$%&()'~]*$"></asp:RegularExpressionValidator>
                                        </td>
                                        <%-- FB 2678 Start--%>
                                        <td style="width: 15%" align="left" valign="top" class="blackblodtext">
                                            Website
                                        </td>
                                        <td style="width: 25%" align="left" valign="top">
                                            <asp:TextBox ID="txtOrgWebsite" runat="server" CssClass="altText"></asp:TextBox>
                                            <asp:RegularExpressionValidator ID="regWebsite" runat="server" ControlToValidate="txtOrgWebsite"
                                                Display="Dynamic" SetFocusOnError="true" ErrorMessage="<br>< > ' + % ( ) ;  | ^ = ! ` , [ ] { } # $ @ ~ and &#34; are invalid characters."
                                                ValidationExpression="^(a-z|A-Z|0-9)*[^<>+;|!`,\[\]{}\x22;=^@#$%()'~]*$"></asp:RegularExpressionValidator>
                                        </td>
                                        <%-- FB 2678 End--%>
                                    </tr>
                                    <tr>
                                        <td style="width: 15%" align="left" valign="top" class="blackblodtext">
                                            City
                                        </td>
                                        <td style="width: 25%" align="left" valign="top">
                                            <asp:TextBox ID="txtCity" runat="server" CssClass="altText"></asp:TextBox>
                                            <asp:RegularExpressionValidator ID="regCity" ControlToValidate="txtCity" ValidationExpression="^(a-z|A-Z|0-9)*[^\\/<>+;?|!`,\[\]{}\x22;=^:@#$%&()'~]*$"
                                                Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<br>& < > ' + % \ / ( ) ; ? | ^ = ! ` , [ ] { } : # $ @ ~ and &#34; are invalid characters."></asp:RegularExpressionValidator>
                                        </td>
                                        <%-- FB 2678 Start--%>
                                        <td style="width: 15%" align="left" valign="top" class="blackblodtext">
                                            Phone Number
                                        </td>
                                        <td style="width: 25%" align="left" valign="top">
                                            <asp:TextBox ID="txtPhoneNumber" runat="server" CssClass="altText"></asp:TextBox>
                                            <asp:RegularExpressionValidator ID="regPhone" runat="server" ControlToValidate="txtPhoneNumber"
                                                Display="Dynamic" SetFocusOnError="true" ErrorMessage="Enter Numbers Only" ValidationExpression="^(\(|\d| |-|\))*$"></asp:RegularExpressionValidator>
                                        </td>
                                        <%-- FB 2678 End--%>
                                    </tr>
                                    <tr>
                                        <td style="width: 15%" align="left" valign="top" class="blackblodtext">
                                            Country
                                        </td>
                                        <td style="width: 25%" align="left" valign="top">
                                            <asp:DropDownList ID="lstCountries" runat="server" CssClass="altText" DataTextField="Name"
                                                Width="225px" DataValueField="ID" OnSelectedIndexChanged="UpdateStates" AutoPostBack="true">
                                                <%--FB 2730--%>
                                            </asp:DropDownList>
                                            <br />
                                        </td>
                                        <%-- FB 2678 Start--%>
                                        <td style="width: 15%" align="left" valign="top" class="blackblodtext">
                                            Fax Number
                                        </td>
                                        <td style="width: 25%" align="left" valign="top">
                                            <asp:TextBox ID="txtFaxNumber" runat="server" CssClass="altText"></asp:TextBox>
                                            <asp:RegularExpressionValidator ID="regFaxNumber" runat="server" ControlToValidate="txtFaxNumber"
                                                Display="Dynamic" ErrorMessage="Enter Numbers Only." SetFocusOnError="true" ValidationExpression="^(\(|\d| |-|\))*$"></asp:RegularExpressionValidator>
                                        </td>
                                        <%-- FB 2678 End--%>
                                    </tr>
                                    <tr>
                                        <td style="width: 15%" align="left" valign="top" class="blackblodtext">
                                            <b>State/Province</b>
                                        </td>
                                        <%--FB 2657--%>
                                        <td style="width: 25%" align="left" valign="top">
                                            <asp:DropDownList ID="lstStates" runat="server" CssClass="altText" DataTextField="Code"
                                                DataValueField="ID">
                                            </asp:DropDownList>
                                            <b>Postal Code</b><%--FB 2657--%>
                                            <asp:TextBox ID="txtZipCode" Width="50" runat="server" CssClass="altText"></asp:TextBox>
                                            <asp:RegularExpressionValidator ID="regZipCode" ControlToValidate="txtZipCode" Display="dynamic"
                                                runat="server" SetFocusOnError="true" ErrorMessage="<br> & < > ' + % \ ; ? | ^ = ! ` [ ] { } : # $ @ ~ and &#34; are invalid characters."
                                                ValidationExpression="^(a-z|A-Z|0-9)*[^\\<>^+;?|!`\[\]{}\x22;=:@#$%&'~]*$"></asp:RegularExpressionValidator><%--FB 2222--%>
                                        </td>
                                    </tr>
                                    <%-- FB 1839 End--%>
                                    <tr>
                                        <td align="left" class="subtitleblueblodtext" colspan="4" style="height: 21; font-weight: bold">
                                            <span style="margin-left: -20px">Organization Resources</span>
                                        </td>
                                    </tr>
                                    <%-- FB 2693 start--%>
                                    <tr>
                                        <td>
                                            &nbsp;&nbsp;
                                        </td>
                                        <td align="left" valign="top">
                                            <table border="0" cellpadding="0" cellspacing="0">
                                                <tr>
                                                    <td class="blackblodtext">
                                                        <b>
                                                            <asp:Label ID="Label3" runat="server" Text="Assigned" Width="48pt" Height="20pt"
                                                                Style="text-align: center"></asp:Label></b>
                                                    </td>
                                                    <td align="center">
                                                        <%--FB 2732--%>
                                                        <asp:Label ID="Label4" runat="server" Text=" <b>Shared Resources Remaining</b> (*Includes the resources assigned to this organization)"
                                                            Style="text-align: center" Font-Size="9pt"></asp:Label>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <%--FB 2659 Starts--%>
                                    <tr>
                                        <td colspan="4">
                                            <table id="tblSeats" runat="server" border="0" width="100%">
                                                <tr style="width: 100%">
                                                    <td style="width: 17.5%" align="left" valign="top" nowrap="nowrap" class="blackblodtext">
                                                        Number of Seats
                                                    </td>
                                                    <%--FB 2659--%>
                                                    <td align="left" valign="top">
                                                        <asp:TextBox ID="TxtSeats" runat="server" onkeypress="return isNumberKey(event)"
                                                            CssClass="altText" Width="50"></asp:TextBox>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span id="Span2" runat="server"
                                                                class="orangesboldtext"></span>
                                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator5" ControlToValidate="TxtSeats"
                                                            Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="Enter Numbers only."
                                                            ValidationExpression="^(\(|\d|\))*$"></asp:RegularExpressionValidator>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="left" valign="top" class="blackblodtext">
                                                        Number of Users
                                                    </td>
                                                    <td align="left" valign="top" colspan="3">
                                                        <asp:TextBox ID="TxtActiveUsers" runat="server" onkeypress="return isNumberKey(event)"
                                                            CssClass="altText" Width="50"></asp:TextBox>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span id="SpanUsers" runat="server"
                                                                class="orangesboldtext"></span>
                                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator6" ControlToValidate="TxtActiveUsers"
                                                            Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="Enter Numbers only."
                                                            ValidationExpression="^(\(|\d|\))*$"></asp:RegularExpressionValidator>
                                                        <input type="hidden" runat="server" id="hdnActiveUsers" />
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <%--FB 2659 Ends--%>
                                    <tr>
                                        <table id="tblallLicense" border="0" runat="server" width="100%">
                                            <tr>
                                                <%--FB 2659 Starts--%>
                                                <%--FB 2693 Starts--%>
                                                <td colspan="2" style="vertical-align: top">
                                                    <table width="100%" border="0" style="vertical-align: top; margin-top: inherit;">
                                                        <tr>
                                                            <td valign="top" align="left" style="width: 2px">
                                                                <asp:ImageButton ID="img_Rooms" runat="server" ImageUrl="image/loc/nolines_plus.gif"
                                                                    Style="margin-left: -21px" vspace="0" hspace="0" Height="25" Width="25" /><%--FB 2730--%>
                                                            </td>
                                                            <td align="left">
                                                                <span class="blackblodtext" style="margin-left: -5px">Rooms</span>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                    <table width="95%" id="tblRooms" align="left" runat="server" style="display: none;"
                                                        border="0" cellpadding="0">
                                                        <%--FB 2730--%>
                                                        <%-- FB 2693 End--%>
                                                        <tr style="display: none">
                                                            <td style="width: 25%" align="left" valign="top" class="blackblodtext">
                                                                Active Rooms
                                                            </td>
                                                            <td style="width: 20%" align="left" valign="top">
                                                                <asp:TextBox ID="TxtRooms" runat="server" CssClass="altText" Width="50"></asp:TextBox>
                                                                &nbsp;&nbsp;<span id="SpanActiveRooms" runat="server" class="orangesboldtext"></span>
                                                                <asp:RegularExpressionValidator ID="regRooms" ControlToValidate="TxtRooms" Display="dynamic"
                                                                    runat="server" SetFocusOnError="true" ErrorMessage="Enter Numbers only." ValidationExpression="^(\(|\d|\))*$"></asp:RegularExpressionValidator>
                                                                <input type="hidden" runat="server" id="hdnRooms" />
                                                            </td>
                                                            <td style="width: 15%" align="left" valign="top" class="blackblodtext">
                                                                &nbsp;
                                                            </td>
                                                            <td style="width: 25%" align="left" valign="top">
                                                                &nbsp;
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td align="left" valign="top" class="blackblodtext">
                                                                <%--FB 2730--%>
                                                                Video Rooms
                                                            </td>
                                                            <%-- FB 2693--%>
                                                            <td style="width: 68%" align="left" valign="top">
                                                                <%--Edited for FF--%><%-- FB 2693--%>
                                                                <asp:TextBox ID="TxtVRooms" runat="server" CssClass="altText" Width="50"></asp:TextBox>
                                                                &nbsp;&nbsp;<span id="SpanActiveVRooms" runat="server" class="orangesboldtext" style="margin-left: 57px"></span> <%-- FB 3021--%>
                                                                <asp:RegularExpressionValidator ID="regVRooms" ControlToValidate="TxtVRooms" Display="dynamic"
                                                                    runat="server" SetFocusOnError="true" ErrorMessage="Enter Numbers only." ValidationExpression="^(\(|\d|\))*$"></asp:RegularExpressionValidator>
                                                                <input type="hidden" runat="server" id="hdnVRooms" />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="width: 15%" align="left" valign="top" class="blackblodtext">
                                                                End Points
                                                            </td>
                                                            <%--FB 2693--%>
                                                            <td style="width: 25;" align="left" valign="top">
                                                                <asp:TextBox ID="TxtEndPoint" runat="server" CssClass="altText" Width="50"></asp:TextBox>
                                                                &nbsp;&nbsp;<span id="SpanActiveEpts" runat="server" class="orangesboldtext" style="margin-left: 57px"></span>  <%-- FB 3021--%>
                                                                <asp:RegularExpressionValidator ID="regEndPoint" ControlToValidate="TxtEndPoint"
                                                                    Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="Enter Numbers only."
                                                                    ValidationExpression="^(\(|\d|\))*$"></asp:RegularExpressionValidator>
                                                                <input type="hidden" runat="server" id="hdnEndPoint" />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <%--FB 2586 Start--%>
                                                            <td style="width: 15%" align="left" valign="top" class="blackblodtext">
                                                                Non-Video Rooms
                                                            </td>
                                                            <%-- FB 2693--%>
                                                            <td style="width: 25%;" align="left" valign="top">
                                                                <asp:TextBox ID="TxtNVRooms" runat="server" CssClass="altText" Width="50"></asp:TextBox>
                                                                &nbsp;&nbsp;<span id="SpanActiveNVRooms" runat="server" class="orangesboldtext" style="margin-left: 57px"></span> <%-- FB 3021--%>
                                                                <asp:RegularExpressionValidator ID="regNVRooms" ControlToValidate="TxtNVRooms" Display="dynamic"
                                                                    runat="server" SetFocusOnError="true" ErrorMessage="Enter Numbers only." ValidationExpression="^(\(|\d|\))*$"></asp:RegularExpressionValidator>
                                                                <input type="hidden" runat="server" id="hdnNVRooms" />
                                                            </td>
                                                        </tr>
                                                        <%--FB 2693 start--%>
                                                        <tr>
                                                            <%--UI MODIFICATION--%>
                                                            <td style="width: 15%" align="left" valign="top" class="blackblodtext">
                                                                VMR Rooms
                                                            </td>
                                                            <%-- FB 2693--%>
                                                            <td style="width: 25%" align="left" valign="top">
                                                                <asp:TextBox ID="TxtVMR" runat="server" CssClass="altText" Width="50"></asp:TextBox>
                                                                &nbsp;&nbsp;<span id="SpanVMRRooms" runat="server" class="orangesboldtext" style="margin-left: 57px"></span> <%-- FB 3021--%>
                                                                <asp:RegularExpressionValidator ID="regVMR" ControlToValidate="TxtVMR" Display="dynamic"
                                                                    runat="server" SetFocusOnError="true" ErrorMessage="Enter Numbers only." ValidationExpression="^(\(|\d|\))*$"></asp:RegularExpressionValidator>
                                                                <input type="hidden" runat="server" id="hdnVMR" />
                                                            </td>
                                                        </tr>
                                                        <%--FB 2426 start--%>
                                                        <tr>
                                                            <td style="width: 15%" align="left" valign="top" class="blackblodtext">
                                                                Guest Rooms
                                                            </td>
                                                            <td style="width: 25%" align="left" valign="top">
                                                                <asp:TextBox ID="TxtExtRooms" runat="server" CssClass="altText" Width="50"></asp:TextBox>
                                                                &nbsp;&nbsp;<span id="SpanExternalRooms" runat="server" class="orangesboldtext" style="margin-left: 57px"></span> <%-- FB 3021--%>
                                                                <asp:RegularExpressionValidator ID="regExtRooms" ControlToValidate="TxtExtRooms"
                                                                    Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="Enter Numbers only."
                                                                    ValidationExpression="^(\(|\d|\))*$"></asp:RegularExpressionValidator>
                                                                <input type="hidden" runat="server" id="hdnExtRooms" />
                                                            </td>
                                                        </tr>
                                                        <%--FB 2694  Starts--%>
                                                        <tr>
                                                            <td style="width: 15%" align="left" valign="top" class="blackblodtext">
                                                                Active VC Hotdesking Rooms
                                                            </td>
                                                            <td style="width: 25%" align="left" valign="top">
                                                                <asp:TextBox ID="txtVCHotRooms" runat="server" CssClass="altText" Width="50"></asp:TextBox>&nbsp;&nbsp;<span
                                                                    id="SpanActiveVC" runat="server" class="orangesboldtext" style="margin-left: 62px"></span> <%-- FB 3021--%>
                                                                <asp:RegularExpressionValidator ID="RegularExpressionValidator3" ControlToValidate="txtVCHotRooms"
                                                                    Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="Enter Numbers only."
                                                                    ValidationExpression="^(\(|\d|\))*$"></asp:RegularExpressionValidator>
                                                                <input type="hidden" runat="server" id="hdnVCHotdesking" />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="width: 15%" align="left" valign="top" class="blackblodtext">
                                                                Active RO Hotdesking Rooms
                                                            </td>
                                                            <td style="width: 25%" align="left" valign="top">
                                                                <asp:TextBox ID="txtROHotRooms" runat="server" CssClass="altText" Width="50"></asp:TextBox>&nbsp;&nbsp;<span
                                                                    id="SpanActiveRO" runat="server" class="orangesboldtext" style="margin-left: 62px"></span> <%-- FB 3021--%>
                                                                <asp:RegularExpressionValidator ID="RegularExpressionValidator4" ControlToValidate="txtROHotRooms"
                                                                    Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="Enter Numbers only."
                                                                    ValidationExpression="^(\(|\d|\))*$"></asp:RegularExpressionValidator>
                                                                <input type="hidden" runat="server" id="hdnROHotdesking" />
                                                            </td>
                                                        </tr>
                                                        <%--FB 2694  Ends--%>
                                                    </table>
                                                    <%-- FB 2730 Start--%>
                                                    <%--<td colspan="2" style="vertical-align: top">--%>
                                                    <table width="100%">
                                                        <%--FB 2730--%>
                                                        <tr>
                                                            <td align="left">
                                                                <table width="100%">
                                                                    <tr>
                                                                        <td valign="top" align="left" style="width: 2px">
                                                                            <asp:ImageButton ID="img_MCUs" runat="server" ImageUrl="image/loc/nolines_plus.gif"
                                                                                Style="margin-left: -24px; vertical-align: top;" vspace="0" hspace="0" Height="25"
                                                                                Width="25" /><%--FB 2730--%>
                                                                        </td>
                                                                        <td align="left">
                                                                            <span class="blackblodtext" style="margin-left: -6px">MCUs</span>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                    <table width="95%" id="tblMCUs" align="left" runat="server" style="display: none;"
                                                        border="0" cellpadding="0">
                                                        <%--FB 2730--%>
                                                        <%--FB 2693 End--%>
                                                        <tr style="width:100%">
                                                            <%--FB 2586 UI MODIFICATION--%>
                                                            <td style="width: 18%;" align="left" valign="top" class="blackblodtext">
                                                                <%--FB 2730--%>
                                                                Standard MCU
                                                            </td>
                                                            <%--FB 2693--%>
                                                            <td style="width: 85%;" align="left" valign="top">
                                                                <%--FB 2730--%>
                                                                <%--FB 2693--%>
                                                                <asp:TextBox ID="TxtMCU" runat="server" CssClass="altText" Width="50"></asp:TextBox>
                                                                &nbsp;&nbsp;<span id="SpanActiveMCU" runat="server" class="orangesboldtext" style="margin-left: 58px"></span>  <%-- FB 3021--%>
                                                                <asp:RegularExpressionValidator ID="regMcu" ControlToValidate="TxtMCU" Display="dynamic"
                                                                    runat="server" SetFocusOnError="true" ErrorMessage="Enter Numbers only." ValidationExpression="^(\(|\d|\))*$"></asp:RegularExpressionValidator>
                                                                <input type="hidden" runat="server" id="hdnMCU" />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <%--FB 2586 UI MODIFICATION--%>
                                                            <td style="width: 15%" align="left" valign="top" class="blackblodtext">
                                                                Enhanced MCU
                                                            </td>
                                                            <%--FB 2693--%>
                                                            <td style="width: 85%;" align="left" valign="top">
                                                                <%--FB 2693--%>
                                                                <asp:TextBox ID="TxtMCUEncha" runat="server" CssClass="altText" Width="50"></asp:TextBox>
                                                                &nbsp;&nbsp;<span id="SpanActiveMCUEnchanced" runat="server" class="orangesboldtext"
                                                                    style="margin-left: 58px"></span> <%-- FB 3021--%>
                                                                <asp:RegularExpressionValidator ID="regMcuencha" ControlToValidate="TxtMCUEncha"
                                                                    Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="Enter Numbers only."
                                                                    ValidationExpression="^(\(|\d|\))*$"></asp:RegularExpressionValidator>
                                                                <input type="hidden" runat="server" id="hdnMCUEncha" />
                                                            </td>
                                                        </tr>
                                                    </table>
                                                    <%-- FB 2730 End--%>
                                                    <%--FB 2693 start--%>
                                                    <table width="100%">
                                                        <%--FB 2730--%>
                                                        <tr>
                                                            <td align="left">
                                                                <%--FB 2730--%>
                                                                <table border="0" width="100%">
                                                                    <tr>
                                                                        <td valign="top" align="left" style="width: 2px">
                                                                            <asp:ImageButton ID="img_Users" runat="server" ImageUrl="image/loc/nolines_plus.gif"
                                                                                Style="margin-left: -24px; vertical-align: top;" vspace="0" hspace="0" Height="25"
                                                                                Width="25" />
                                                                        </td>
                                                                        <td align="left">
                                                                            <span class="blackblodtext" style="margin-left: -6px;">Users</span>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                    <table id="tblusers" runat="server" align="left" style="display: none;" width="95%%"
                                                        cellpadding="0" border="0">
                                                        <%--FB 2693 End--%>
                                                        <tr style="width:100%">
                                                            <%--UI MODIFICATION--%>
                                                            <td style="width: 18%" align="left" valign="top" class="blackblodtext">
                                                                Active Users
                                                            </td>
                                                            <td style="width: 85%" align="left" valign="top">
                                                                <%--FB 2347--%>
                                                                <asp:TextBox ID="TxtUsers" runat="server" CssClass="altText" Width="50"></asp:TextBox>&nbsp;&nbsp;<span
                                                                    id="SpanActiveUsers" runat="server" class="orangesboldtext" style="margin-left: 62px"></span><%--FB 2694--%><%-- FB 3021--%>
                                                                <asp:RegularExpressionValidator ID="regUsers" ControlToValidate="TxtUsers" Display="dynamic"
                                                                    runat="server" SetFocusOnError="true" ErrorMessage="Enter Numbers only." ValidationExpression="^(\(|\d|\))*$"></asp:RegularExpressionValidator>
                                                                <input type="hidden" runat="server" id="hdnUsers" />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="width: 18%" align="left" valign="top" class="blackblodtext">
                                                                Outlook Users
                                                            </td>
                                                            <%--Edited for FB 2098--%>
                                                            <td style="width: 85%" align="left" valign="top" colspan="3">
                                                                <asp:TextBox ID="TxtExUsers" runat="server" CssClass="altText" Width="50"></asp:TextBox>&nbsp;&nbsp;<span
                                                                    id="SpanActiveExUsers" runat="server" class="orangesboldtext" style="margin-left: 62px"></span><%--FB 2694--%>
                                                                <asp:RegularExpressionValidator ID="regExcUsers" ControlToValidate="TxtExUsers" Display="dynamic"
                                                                    runat="server" SetFocusOnError="true" ErrorMessage="Enter Numbers only." ValidationExpression="^(\(|\d|\))*$"></asp:RegularExpressionValidator>
                                                                <input type="hidden" runat="server" id="hdnEUsers" />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="width: 18%" align="left" valign="top" class="blackblodtext">
                                                                Notes Users
                                                            </td>
                                                            <%--Edited for FB 2098--%>
                                                            <td style="width: 85%" align="left" valign="top" colspan="3">
                                                                <asp:TextBox ID="TxtDomUsers" runat="server" CssClass="altText" Width="50"></asp:TextBox>&nbsp;&nbsp;<span
                                                                    id="SpanActiveDomUsers" runat="server" class="orangesboldtext" style="margin-left: 62px"></span><%--FB 2694--%>
                                                                <asp:RegularExpressionValidator ID="regDomUsers" ControlToValidate="TxtDomUsers"
                                                                    Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="Enter Numbers only."
                                                                    ValidationExpression="^(\(|\d|\))*$"></asp:RegularExpressionValidator>
                                                                <input type="hidden" runat="server" id="hdnDUsers" />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <%--FB 1979--%>
                                                            <td style="width: 18%" align="left" valign="top" class="blackblodtext">
                                                                Mobile Users
                                                            </td>
                                                            <td style="width: 85%" align="left" valign="top" colspan="3">
                                                                <asp:TextBox ID="TxtMobUsers" runat="server" CssClass="altText" Width="50"></asp:TextBox>&nbsp;&nbsp;<span
                                                                    id="SpanActiveMobUsers" runat="server" class="orangesboldtext" style="margin-left: 62px"></span><%--FB 2694--%>
                                                                <asp:RegularExpressionValidator ID="regMobUsers" ControlToValidate="TxtMobUsers"
                                                                    Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="Enter Numbers only."
                                                                    ValidationExpression="^(\(|\d|\))*$"></asp:RegularExpressionValidator>
                                                                <input type="hidden" runat="server" id="hdnMUsers" />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <%--FB 2693--%>
                                                            <td style="width: 18%" align="left" valign="top" class="blackblodtext">
                                                                PC Users
                                                            </td>
                                                            <td style="width: 85%" align="left" valign="top" colspan="3">
                                                                <asp:TextBox ID="txtPCUser" runat="server" CssClass="altText" Width="50"></asp:TextBox>&nbsp;&nbsp;<span
                                                                    id="SpanActivePCUsers" runat="server" class="orangesboldtext" style="margin-left: 62px"></span><%--FB 2694--%>
                                                                <asp:RegularExpressionValidator ID="regPCUsers" ControlToValidate="txtPCUser" Display="dynamic"
                                                                    runat="server" SetFocusOnError="true" ErrorMessage="Enter Numbers only." ValidationExpression="^(\(|\d|\))*$"></asp:RegularExpressionValidator>
                                                                <input type="hidden" runat="server" id="hdnPCUser" />
                                                            </td>
                                                        </tr>
                                                        <%--FB 2426 Start--%>
                                                        <tr>
                                                            <td style="width: 18%" align="left" valign="top" class="blackblodtext">
                                                                Guest Room Per User
                                                            </td>
                                                            <td style="width: 85%" align="left" valign="top" colspan="3">
                                                                <asp:TextBox ID="TxtGstPerUser" runat="server" CssClass="altText" Width="50"></asp:TextBox>&nbsp;&nbsp;<span
                                                                    id="SpanGuRoomUser" runat="server" class="orangesboldtext" style="margin-left: 62px"></span><%--FB 2694--%>
                                                                <asp:RegularExpressionValidator ID="RegGstPerUser" ControlToValidate="TxtGstPerUser"
                                                                    Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="Enter Numbers only."
                                                                    ValidationExpression="^(\(|\d|\))*$"></asp:RegularExpressionValidator>
                                                                <input type="hidden" runat="server" id="hdnGstPerUser" />
                                                            </td>
                                                        </tr>
                                                    </table>
                                                    <%-- FB 2730 start--%>
                                                    <table width="100%">
                                                        <%--FB 2730--%>
                                                        <tr>
                                                            <td align="left">
                                                    <table style="vertical-align: top">
                                                        <tr>
                                                            <td align="left" valign="top">
                                                                <asp:ImageButton ID="img_Modules" runat="server" ImageUrl="image/loc/nolines_plus.gif"
                                                                    Style="margin-left: -24px" vspace="0" hspace="0" Height="25" Width="25" /> <%-- FB 3021--%>
                                                            </td>
                                                            <td align="left">
                                                                <span class="blackblodtext" style="margin-left: -5px">Modules</span>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                    </td>
                                                        </tr>
                                                    </table>
                                                    <table width="95%" height="150pt" id="tblModules" runat="server" style="display: none;"
                                                        border="0" cellpadding="1">
                                                        <%--FB 2730 start--%>
                                                        <tr style="width:100%">
                                                            <td style="width: 18%;" align="left" valign="top" class="blackblodtext">
                                                                <%--FB 2730--%>
                                                                Audio/Visual
                                                            </td>
                                                            <%-- FB 2570 --%><%-- FB 2693--%>
                                                            <td style="width: 85%" align="left" valign="top">
                                                                <%--FB 2730--%>
                                                                <asp:CheckBox ID="ChkFacility" runat="server" />
                                                                &nbsp;&nbsp;<span id="SpanActiveFacility" runat="server" class="orangesboldtext"
                                                                    style="margin-left: 90px"></span> <%-- FB 3021--%>
                                                                <input type="hidden" runat="server" id="hdnFacility" />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="width: 18%;" align="left" valign="top" class="blackblodtext">
                                                                Catering
                                                            </td>
                                                            <%-- FB 2693--%>
                                                            <td style="width: 85%" align="left" valign="top">
                                                                <asp:CheckBox ID="ChkCatering" runat="server" />
                                                                &nbsp;&nbsp;<span id="SpanActiveCat" runat="server" class="orangesboldtext" style="margin-left: 90px"></span> <%-- FB 3021--%>
                                                                <input type="hidden" runat="server" id="hdnCatering" />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <%--Edited for FB 1706--%>
                                                            <td style="width: 18%;" align="left" valign="top" class="blackblodtext">
                                                                Faciltity Services
                                                            </td>
                                                            <%--FB 2570--%>
                                                            <td style="width: 85%" align="left" valign="top">
                                                                <asp:CheckBox ID="ChkHK" runat="server" />
                                                                &nbsp;&nbsp;<span id="SpanActiveHK" runat="server" class="orangesboldtext" style="margin-left: 90px"></span> <%-- FB 3021--%>
                                                                <input type="hidden" runat="server" id="hdnHK" />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="width: 18%;" align="left" valign="top" class="blackblodtext">
                                                                API
                                                            </td>
                                                            <%-- FB 2693--%>
                                                            <td style="width: 85%" align="left" valign="top">
                                                                <asp:CheckBox ID="ChkAPI" runat="server" />
                                                                &nbsp;&nbsp;<span id="SpanActiveAPI" runat="server" class="orangesboldtext" style="margin-left: 90px"></span> <%-- FB 3021--%>
                                                                <input type="hidden" runat="server" id="hdnAPI" />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="width: 18%; height: 16pt" align="left" valign="top" class="blackblodtext">
                                                                Blue Jeans
                                                            </td>
                                                            <td style="width: 85%" align="left" valign="top">
                                                                <asp:CheckBox ID="chkBlueJeans" runat="server" />
                                                                &nbsp;&nbsp;<span id="SpanBJ" runat="server" class="orangesboldtext" style="margin-left: 90px"></span> <%-- FB 3021--%>
                                                                <input type="hidden" runat="server" id="hdnBJ" />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="width: 18%; height: 16pt" align="left" valign="top" class="blackblodtext">
                                                                Jabber
                                                            </td>
                                                            <td style="width: 85%" align="left" valign="top">
                                                                <asp:CheckBox ID="chkJabber" runat="server" />
                                                                &nbsp;&nbsp;<span id="SpanJabber" runat="server" class="orangesboldtext" style="margin-left: 90px"></span> <%-- FB 3021--%>
                                                                <input type="hidden" runat="server" id="hdnJabber" />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="width: 18%; height: 16pt" align="left" valign="top" class="blackblodtext">
                                                                Lync
                                                            </td>
                                                            <td style="width: 85%" align="left" valign="top">
                                                                <asp:CheckBox ID="chkLync" runat="server" />
                                                                &nbsp;&nbsp;<span id="SpanLync" runat="server" class="orangesboldtext" style="margin-left: 90px"></span> <%-- FB 3021--%>
                                                                <input type="hidden" runat="server" id="hdnLync" />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="width: 18%; height: 16pt" align="left" valign="top" class="blackblodtext">
                                                                Vidtel
                                                            </td>
                                                            <%-- FB 2693--%>
                                                            <td style="width: 85%" align="left" valign="top">
                                                                <asp:CheckBox ID="chkVidtel" runat="server" />
                                                                &nbsp;&nbsp;<span id="SpanVidtel" runat="server" class="orangesboldtext" style="margin-left: 90px"></span> <%-- FB 3021--%>
                                                                <input type="hidden" runat="server" id="hdnVidtel" />
                                                            </td>
                                                        </tr>
                                                        <%--FB 2593 Start--%>
                                    <tr>
                                        <td style="width: 15%; height: 16pt" align="left" valign="top" class="blackblodtext">
                                            Adv Rep Mod
                                        </td>
                                        <td style="width: 25%" align="left" valign="top">
                                            <asp:CheckBox ID="ChkAdvReport" runat="server" />
                                            &nbsp;&nbsp;<span id="SpanAdvReport" runat="server" class="orangesboldtext" style="margin-left: 90px"></span> <%-- FB 3021--%>
                                            <input type="hidden" runat="server" id="hdnAdvReport" />
                                        </td>
                                    </tr>
                                    <%--FB 2593 End--%>
                                    <tr>
                                        <td id="tdcloud" runat="server" style="width: 15%; height: 16pt" align="left" valign="top"
                                            class="blackblodtext">
                                            Vidyo
                                        </td>
                                        <%-- FB 2693--%>
                                        <td id="tdChkCloud" runat="server" style="width: 25%" align="left" valign="top">
                                            <asp:CheckBox ID="ChkCloud" runat="server" />
                                            &nbsp;&nbsp;<span id="SpanActiveCloud" runat="server" class="orangesboldtext" style="margin-left: 90px"></span> <%-- FB 3021--%>
                                            <input type="hidden" runat="server" id="hdnCloud" />
                                        </td>
                                    </tr>
                                        <%--FB 2594 Starts--%><%-- FB 3021 start--%>
                                        <tr>
                                             <td style="width: 15%;" align="left" valign="top" class="blackblodtext" runat="server" id="tdPublicRoom">
                                                   Public Room Service
                                             </td>
                                             <td style="width: 25%;" align="left" valign="top" runat="server" id="tdChkPublicRoom">
                                                   <asp:CheckBox ID="ChkPublicRoom" runat="server" />
                                                        &nbsp;&nbsp;<span id="SpanActivePublicRoom" runat="server" class="orangesboldtext" style="margin-left: 90px"></span>
                                             </td>
                                                            <%--FB 2594 Ends--%>   <%-- FB 3021 End--%>                               
                                    </tr>
                                    <%--FB 2693 - Commented--%>
                                    <%-- <tr>

                                                        <%--FB 2693 - Commented--%>
                                                        <%-- <tr>
                                                <td style="height:16pt" align="left" valign="top" class="blackblodtext">&nbsp;&nbsp;PC Module</td> 
                                                <td style="width:25%" align="left" valign="top">
                                                  <asp:CheckBox ID="ChkPC" runat="server" />&nbsp;&nbsp;<span id="SpanActivePC" runat="server" class="orangesboldtext" style="margin-left:95px"></span>
                                                   <input type="hidden" runat="server" id="hdnPC" />
                                                     </td>
                                                      </tr>--%>
                                                        <tr>
                                                        </tr>
                                                    </table>
                                                    <%--FB 2730 End--%>
                                                </td>
                                            </tr>
                                        </table>
                                    </tr>
                                    <%--FB 2693 Ends--%>
                                    <%--FB 2426 End--%>
                                    <%--FB 2579 End--%>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
        <table cellpadding="0" cellspacing="0" border="0" style="width: 100%">
            <tr>
                <td align="center">
                    <table id="tblButtons" cellpadding="2" cellspacing="2" style="width: 90%">
                        <tr>
                            <td align="center" style="width: 20%">
                                <asp:Button ID="btnReset" runat="server" Text="Reset" CssClass="altLongBlueButtonFormat" OnClientClick="javascript:DataLoading(1);"
                                    OnClick="ResetOrganizationProfile" /><%--ZD 100176--%> 
                            </td>
                            <td align="center" style="width: 20%">
                                <input name="Go" type="button" class="altLongBlueButtonFormat" value=" Go Back "
                                    onclick="javascript:return fnGoBack();" />
                            </td>
                            <td align="center" style="width: 20%">
                                <asp:Button ID="btnSubmitAddNew" runat="server" Text="Submit / New Organization"
                                    CssClass="altLongBlueButtonFormat" OnClientClick="javascript:return fnCheckOrg();"
                                    OnClick="SubmitAddNewOrganizationProfile" />
                            </td>
                            <td align="center" style="width: 20%">
                                <asp:Button ID="btnSubmit" runat="server" Text="Submit" CssClass="altLongBlueButtonFormat"
                                    OnClick="SubmitOrganizationProfile" OnClientClick="javascript:return fnCheckOrg();" />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </div>
    </form>
</body>
</html>

<script type="text/javascript" src="inc/softedge.js"></script>

<!-- #INCLUDE FILE="inc/mainbottomNET.aspx" -->
