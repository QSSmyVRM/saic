<%--ZD 100147 start--%>
<%--/* Copyright (C) 2013 myVRM - All Rights Reserved	
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.	
*
* You should have received a copy of the myVRM license with	
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/--%><%--ZD 100147 End--%>
<%@ Page Language="C#" Inherits="ns_ManageRoom.ManageRoom" %><%--ZD 100170--%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> <!-- FB 2050 -->
<meta http-equiv="X-UA-Compatible" content="IE=8" /> <!-- FB 2050 -->
<!-- #INCLUDE FILE="inc/maintopNet.aspx" -->
<%
    if(Session["userID"] == null)
    {
        Response.Redirect("~/en/genlogin.aspx"); //FB 1830

    }    
%>
<script language="javascript" src="inc/functions.js" type="text/javascript" />
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Manage Room</title>
<!-- For Location Issues - Script Code Moved in below Else loop-->
    <script language="JavaScript" type="text/javascript">
<!--
	function frm_validate(){
	
		// room name
		if (document.getElementById("RoomName"))
		if (document.getElementById("RoomName").value != "") {
			
			if(checkInvalidChar(document.getElementById("RoomName").value) == false){
				return false;
			}
		}
}
//ZD 100176 start
function DataLoading(val) {
    if (val == "1")
        document.getElementById("dataLoadingDIV").innerHTML = "<img border='0' src='image/wait1.gif'>";
    else
        document.getElementById("dataLoadingDIV").innerHTML = "";
}
//ZD 100176 End
//-->
    </script>

</head>
<body>
    <input name="selectedloc" type="hidden" id="selectedloc" runat="server"  /> <!--Added room search-->
     <input name="locstrname" type="hidden" id="locstrname" runat="server"  /> <!--Added room search-->
      
    
    <table width="95%" style="vertical-align:bottom" cellpadding="3" cellspacing="2">
    <tr>
        <td height="15px"> 
        </td>
    </tr>
    <tr>
    <td>
    <center>
        <h3 id="hMgHdg" runat="server" visible="True">
            Manage <!-- FB 2570 -->
            <asp:Label ID="title" runat="server" Text=""></asp:Label> 
            Rooms
        </h3>
        <h3 id="hSearchHdg" runat="server" visible="false">
            <asp:Label ID="lblSearchHdg" runat="server" Text="Search Rooms"></asp:Label>
        </h3>
        <div id="dataLoadingDIV" align="center"></div> <%--ZD 100176--%>
    </center>
    </td>
    </tr>
        <tr align="center">
            <td align="center">
                <asp:Label ID="LblError" CssClass="lblError" runat="server" 
                    Visible="false"></asp:Label>
            </td>
        </tr>
    </table>
    <%--Edited FOr Location Issue--%>
    <form method="post" id="frmManageroom" name="frmManageroom" runat="server">
        <input name="settings2locstr" type="hidden" id="settings2locstr" runat="server" />
        <input name="settings2locpg" type="hidden" id="settings2locpg" runat="server" />
        <input name="getLocID" type="hidden" id="getLocID" runat="server" />
        <input name="sSession" type="hidden" id="sSession" runat="server" />
        <input type="hidden" id="MainLoc" runat="server" name="MainLoc" />
        <input id="helpPage" type="hidden" value="63" />
        <div>

<!--Location Issues Start-->            
<%
if(settings2locpg.Value == "settings2locfail.aspx?wintype=ifr")
{
%>
<!--Location Issues End-->
<br><br>
  <center><b>There are no rooms available. Please create a new room.</b></center>
  <br><br><br><br><br>
  <input type="hidden" name="cmd" value="GetNewRoom">

  <center>
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr> 
        <td align="center"> 
          
        </td>
      </tr>
    </table>
  </center>
<!--Location Issues Start-->
<%
}
else
{
%>
            <script language="JavaScript" type="text/javascript">
<!--

	var RoomDisplayMod = ( ("<%=Session["RoomListView"]%>" == "list") ? 2 : 1 );
	
	var RoomCheckableMod = 1;
	
	
	
	function chgRoomDisplay(locpghref, mod, forced)
	{
	
	locpghref = "<%=settings2locpg.Value%>"
		var special;
		mod = parseInt(mod);
		if ((mod != RoomDisplayMod) || forced)   {
			RoomDisplayMod = mod;
			var tmpstr = "";
			if (ifrmLocation) {
			if(ifrmLocation.document.frmSettings2loc.selectedloc.value != "") // added for Location Issues
				tmpstr = ifrmLocation.document.frmSettings2loc.selectedloc.value;
				ctmpstr="";
				if (typeof(ifrmLocation.document.frmSettings2loc.comparedsellocs) != "undefined")
					ctmpstr = ifrmLocation.document.frmSettings2loc.comparedsellocs.value; }
				if (typeof(ifrmLocation.document.frmSettings2loc.special)!="undefined") {
					if (ifrmLocation.document.frmSettings2loc.special.value == "")
						special = "0";
					else
						special = ifrmLocation.document.frmSettings2loc.special.value;}
			ifrmLocation.location.href = locpghref + "&mod=" + mod + "&cursel=" + tmpstr + "&comp=" + ctmpstr + "&special=" + special + "&";
		}
	}


	function roomCheckable(needenable)
	{
	
		RoomCheckableMod = needenable;
		
		updateRoomCheckable();
		
	}


	function updateRoomCheckable()
	{
		els = ifrmLocation.document.frmSettings2loctree.elements;
		for (var i = 0; i < els.length; i++) {
			if (els[i].type == "checkbox") {
				els[i].disabled = !RoomCheckableMod;
			}
		}
		
	}

//-->
    </script>
<!--Location Issues End-->
            <input name="cmd" type="hidden" />
                <table border="0" cellpadding="4" cellspacing="6" width="98%">
                <tr>
                    <td colspan="3"> <%-- FB 2612 iframe height 633--%>
                         <iframe id="RoomFrame" runat="server" width="100%" valign="top" height="685px" scrolling="no"></iframe> <%--NGC UI Issue--%><%--FB 2694--%>
                    </td>
                </tr>
                
                    <tr style="display:none;">
                   
                        <td valign="top" style="width: 450">
                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                <!-- -->
                                <tr>
                                    <td style="height: 20; width: 3%">
                                        <!--<table width=25 border=0><tr><td height=20 bgcolor=blue align=center>
              <SPAN class=numwhiteblodtext>1</SPAN>
            </td></tr></table>-->
                                    </td>
                                    <td style="width: 1%">
                                        &nbsp;</td>
                                    <td style="width: 96%" align="left">
                                        <span class="subtitleblueblodtext" id="spMgRooms" runat="server" visible="false">Existing
                                            Rooms</span><br />
                                        <span class="subtitleblueblodtext" id="spSearch" runat="server" visible="false">Search
                                            Results</span><br />
                                       <span class="blackblodtext">Choose a room to edit, deactivate or reactivate.</span> 
                                    </td>
                                </tr>
                                <tr>
                                    <td style="height: 15">
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                    </td>
                                    <td>
                                    </td>
                                    <td align="left">
                                        <font size="1" class="blackblodtext">
                                            <img height="15" src="image/deleted.gif" width="16" alt="ImgDel" style="cursor:pointer;" title="Delete"  /> <%--FB 2798--%>
                                            - Room Deactivated. Click icon to reactivate the room. </font>
                                        <br />
                                        <font size="1" class="blackblodtext">
                                            <img height="16" src="image/locked.gif" width="15" alt="ImgLock" />
                                            <%if (Application["Client"].ToString().ToUpper() == "MOJ"){%>- Room Locked. Room is reserved for a hearing.<%}else{ %>- Room Locked. Room is reserved for a conference.<%} %> </font><%--Edited  For FB 1428--%>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                    </td>
                                    <td>
                                    </td>
                                    <td align="left" colspan="3" style="height: 30" valign="bottom">
                                        <table border="0" width="305">
                                            <tr>
                                                <td align="right">
                                                    <table border="0" cellpadding="0" cellspacing="0">
                                                        <tr>
                                                            <%--Window Dressing--%>
                                                            <td class="blackblodtext">
                                                                <input id="RdroomLevDisplayMod" name="roomListDisplayMod" onclick="JavaScript:chgRoomDisplay('',1);"
                                                                    type="radio" value="1" runat="server" />
                                                                Level View
                                                            </td>
                                                            <td style="width: 10">
                                                            </td>
                                                             <%--Window Dressing--%>
                                                            <td class="blackblodtext">
                                                                <input id="RdroomListDisplayMod" name="roomListDisplayMod" onclick="JavaScript:chgRoomDisplay('',2);"
                                                                    type="radio" value="2" runat="server" />
                                                                List View
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                    </td>
                                    <td>
                                    </td>
                                    <td id="lociframe">
                                        <iframe align="left" height="350" name="ifrmLocation" src="<%=settings2locpg.Value%>"
                                            valign="top" width="305" id="ifrmLocation" runat="server">
                                            <p>
                                                go to <a href="<%=settings2locpg.Value%>">Location List</a></p>
                                        </iframe>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                    </td>
                                    <td>
                                    </td>
                                    <td align="left">
                                    <asp:Label ID="lblTtlRooms" CssClass="blackblodtext" Text="Total Video Rooms: " runat="server"></asp:Label>
                                         <span class="summaryText">
                                            <asp:Label ID="totalNumber" runat="server"></asp:Label>
                                        <asp:Label ID="Label1" CssClass="blackblodtext" Text="&#59; &nbsp; &nbsp;Total  Non-Video Rooms: " runat="server"></asp:Label>
                                            <asp:Label ID="ttlnvidLbl" runat="server"></asp:Label>
                                        <asp:Label ID="lblVMRRooms" CssClass="blackblodtext" Text="&#59;&nbsp; &nbsp;Total VMR Rooms: " runat="server"></asp:Label><%--FB 2586--%>
                                            <asp:Label ID="tntvmrrooms" runat="server"></asp:Label>
                                            </span>; &nbsp; <br /><span class="blackblodtext">Licenses Remaining: </span> <span class="summaryText">
                                            <asp:Label ID="licensesRemain" runat="server"></asp:Label>
                                        </span><span class="blackblodtext">Video Rooms Remaining: </span> <span class="summaryText">
                                            <asp:Label ID="vidLbl" runat="server"></asp:Label>
                                        </span><span class="blackblodtext"> &nbsp;&nbsp;Non-Video Rooms Remaining: </span> <span class="summaryText">
                                            <asp:Label ID="nvidLbl" runat="server"></asp:Label>
                                        </span><span class="blackblodtext">&nbsp;&nbsp;VMR Rooms Remaining: </span> <span class="summaryText"><%--FB 2586--%>
                                            <asp:Label ID="vmrvidLbl" runat="server"></asp:Label>
                                        </span>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="3" style="height: 10">
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                    </td>
                                    <td>
                                    </td>
                                    <td align="center">
                                        <asp:Button ID="btnDeleteRoom" CssClass="altShortBlueButtonFormat" OnClientClick="javascript:return ChkRoomNumValid(-1);"
                                            runat="server"  Text="Delete Room"></asp:Button>
                                        <asp:Button ID="btnEdit" CssClass="altShortBlueButtonFormat" OnClientClick="javascript:return ChkRoomNumValid(0);"
                                            runat="server"  Text="Edit"></asp:Button>
                                    </td>
                                </tr>
                                <!-- -->
                            </table>
                        </td>
                        <td style="width: 50" style="display:none;">
                            <table border="0" cellpadding="0" cellspacing="5" width="100%" style="display:none;">
                                <tr>
                                    <td align="center" valign="middle">
                                        <img height="260" src="image/aqualine.gif" valign="middle" width="2" alt="ImgAqua" />
                                    </td>
                                </tr>
                                <tr>
                                    <td align="center" valign="middle">
                                        <font color="#00ccff" size="3"><b>OR</b></font>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="center" valign="middle">
                                        <img height="140" src="image/aqualine.gif" valign="middle" width="2" alt="ImgAq" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <td valign="top" style="width: 400" style="display:none;">
                            <table border="0" cellpadding="0" cellspacing="0" width="100%" style="display:none;">
                                <!-- -->
                                <tr>
                                    <td style="height: 20; width: 1%">
                                        <!--<table width=25 border=0><tr><td height=20 bgcolor=blue align=center>
              <SPAN class=numwhiteblodtext>2</SPAN>
            </td></tr></table>-->
                                    </td>
                                    <td style="width: 1%">
                                        &nbsp;</td>
                                    <td width="96%" align="left">
                                        <span class="subtitleblueblodtext" id="spSrhRm" runat="server" visible="false">Search
                                            Rooms</span><br />
                                        <span class="subtitleblueblodtext" id="spNwRm" runat="server" visible="false">New Room
                                            Search</span><br />
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="3" style="height: 15">
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                    </td>
                                    <td>
                                    </td>
                                    <td align="center">
                                        <table border="0" cellpadding="4" cellspacing="0" width="100%">
                                            <tr>
                                                <td align="right">
                                                    <span class="blackblodtext">Room Name</span>
                                                </td>
                                                <td align="left">
                                                    <asp:TextBox ID="txtRoomName" CssClass="altText" MaxLength="256" name="RoomName"
                                                        onkeyup="javascript:chkLimit(this,'2');" runat="server" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="right">
                                                    <span class="blackblodtext">Room Capacity Greater Than </span>
                                                </td>
                                                <td align="left">
                                                    <asp:TextBox ID="txtRoomCapacity" CssClass="altText" name="RoomCapacity" size="10"
                                                        type="text" runat="server" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="right">
                                                    <span class="blackblodtext">Projector</span>
                                                </td>
                                                <td align="left">
                                                <%--window dressing--%>
                                                    <asp:DropDownList ID="Projector" CssClass="altText" name="Projector" runat="server" AutoPostBack="false">
                                                        <asp:ListItem Selected="True" Value="-1" Text="any"></asp:ListItem>
                                                        <asp:ListItem Value="1" Text="yes"></asp:ListItem>
                                                        <asp:ListItem Value="0" Text="no"></asp:ListItem>
                                                    </asp:DropDownList>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="2" style="height: 110">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="right" colspan="2">
                                                    <%--code added for Soft Edge button--%>                                                    
                                                    <input type="button" name="Reset" class="altShortBlueButtonFormat" value="Reset" onclick="javascript:fnResetValues(0);" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="right" colspan="2">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="right" colspan="2">
                                                    <asp:Button ID="btnSearchSubmit" CssClass="altShortBlueButtonFormat" name="ManageroomSubmit"
                                                        Text="Search" runat="server"/>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="3" style="height: 10">
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" colspan="3" style="height: 2">
                                        <img height="2" src="image/aqualine.gif" valign="middle" width="95%" alt="Img" />
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="3" style="height: 20">
                                    </td>
                                </tr>
                                <tr>
                                    <td style="height: 20; width: 1%">
                                        <!--<table width=25 border=0><tr><td height=20 bgcolor=blue align=center>
              <SPAN class=numwhiteblodtext>3</SPAN>
            </td></tr></table>-->
                                    </td>
                                    <td style="width: 1%">
                                        &nbsp;</td>
                                    <td style="width: 96%">
                                        <span class="subtitleblueblodtext">Create a New Room</span><br />
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="3" style="height: 100">
                                    </td>
                                </tr>
                                
                                <!-- -->
                            </table>
                        </td>
                    </tr>
                    <tr>
                                    <td style="height: 20; width: 1%">
                                        <!--<table width=25 border=0><tr><td height=20 bgcolor=blue align=center>
              <SPAN class=numwhiteblodtext>3</SPAN>
            </td></tr></table>-->
                                    </td>
                                    <td style="width: 1%">
                                        &nbsp;</td>
                                    <td style="width: 96%">
                                        <!--<span class="subtitleblueblodtext"></span><br />--><%--Commented for FB 2094--%>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                    </td>
                                    <td>
                                    </td>
                                    <td align="right">
                                    <asp:Button  name="ManageroomSubmit"
                                            ID="btnNewSubmit" Text="Create a New Room" runat="server" Width="25%" /><%-- FB 2694 FB 2796 --%>
                                        <asp:Button CssClass="altShortBlueButtonFormat" value="Submit" name="ManageroomSubmit" style="display:none;" OnClientClick="DataLoading(1)"
                                            ID="btnSubmit" Text="Submit" runat="server" /> <%--ZD 100176--%> 
                                    </td>
                                </tr>
                                <tr> <%-- FB 2448 --%>
                                   <td></td>
                                   <td></td>
                                   <td align="right">
                                      <asp:Button ID="btnCreateVMRRoom" runat="server" Text = "Create New VMR Room"   Width="25%"  OnClientClick="DataLoading(1)"/> <%--FB 2796--%> <%--ZD 100176--%> 
                                   </td>
                                </tr>
                                
                                <%--FB 2694 Starts--%>
                                <tr> 
                                   <td></td>
                                   <td></td>
                                   <td align="right">
                                      <asp:Button ID="btnHotdeskingRoom" runat="server" Text = "Create New Hotdesking Room" Width="25%"  OnClientClick="DataLoading(1)"/> <%--FB 2796--%><%--ZD 100176--%> 
                                   </td>
                                </tr>
                                <%--FB 2694 End--%>                    
                </table>
            

            <script language="JavaScript" type="text/javascript">
<!--

if (typeof(ifrmLocation) != "undefined") {
tmpstr = "<%=getLocID.Value%>";
	switch ("<%= Session["RoomListView"] %>") {
		case "level":
			document.frmManageroom.roomListDisplayMod[0].checked = true;
			ifrmLocation.location.href = "<%=settings2locpg.Value%>" + "&mod=1&cursel=" + tmpstr + "&";
			break;
//			alert("<%=settings2locpg.Value%>");
		case "list":
			document.frmManageroom.roomListDisplayMod[1].checked = true;
			ifrmLocation.location.href = "<%=settings2locpg.Value%>" + "&mod=2&cursel=" + tmpstr + "&";
			break;
	}
}


//-->
            </script>

            
<%
}
%>

        </div>
    </form>
</body>
</html>
<%--code added for Soft Edge button--%>
<script type="text/javascript" src="inc/softedge.js"></script>
<!-- #INCLUDE FILE="inc/mainbottomNET.aspx" -->

<script language="JavaScript" type="text/javascript">
<!--
function ChkRoomNumValid (opr)
{
	var t = (s = ifrmLocation.document.frmSettings2loc.selectedloc.value).substring (2, s.length);
	
	rn = t.split(", ").length - 1;
	
	switch (opr) 
	{
		case -1:
			switch (rn) 
			{
				case 0:
					alert("Please select at least one Conference Room to delete.")
					return false;
					break;
				default:
					var isRemoveRms = confirm("Are you sure you want to deactivate the selected rooms?")
					if (isRemoveRms == false) 
					{
						return (false);
					}
					document.frmManageroom.cmd.value = "DeleteRoom";
					break;				
			}
			break;
		case 0:
			switch (rn) 
			{
				case 0:
					alert("Please select one Conference Room to edit.")
					return false;
					break;
				case 1:
					document.frmManageroom.cmd.value = "GetOldRoom";
					break;
				default:
					alert("Please select only one Conference Room to edit.")
					return false;
					break;				
			}
			break;
	}
   document.frmManageroom.MainLoc.value = t;
   document.frmManageroom.submit();
}
//-->
</script>

