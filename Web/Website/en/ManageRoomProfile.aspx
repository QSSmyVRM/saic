<%--ZD 100147 start--%>
<%--/* Copyright (C) 2013 myVRM - All Rights Reserved	
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.	
*
* You should have received a copy of the myVRM license with	
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/--%><%--ZD 100147 End--%>
<%@ Page Language="C#" AutoEventWireup="true" Inherits="ns_MyVRM.Room" %>

<%@ Register TagPrefix="cc1" Namespace="myVRMWebControls" Assembly="myVRMWebControls" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<!-- FB 2050 -->
<meta http-equiv="X-UA-Compatible" content="IE=8" />
<!-- FB 2050 -->
<!--window Dressing start-->
<% 
    if (Request.QueryString["cal"] == "2")
    { 
%>
<!-- #INCLUDE FILE="inc/maintopNET.aspx" -->
<% 
    } 
%>
<!--window Dressing end-->

<script type="text/javascript" src="inc/functions.js"></script>

<script type="text/javascript" src="script/errorList.js"></script>

<script type="text/javascript" src="extract.js"></script>

<%--Login Management--%>

<%--FB 3055-URL Starts--%>
<script>
    var ar = window.location.href.split("/");
    ar = ar[ar.length - 1];
    var pg = "";
    if (ar.indexOf("?") > -1)
        pg = ar.split("?")[1];
    else
        pg = ar;

    var page = pg.toLowerCase();
    //alert(page);
    var cnt = 0;

    if (page.indexOf("cal=2") > -1)
        cnt++;
    if (page.indexOf("cal=2") > -1 && page.indexOf("rid") > -1)
        cnt++;
    if (page.indexOf("cal=2") > -1 && page.indexOf("pageid") > -1)
        cnt++;

    if (cnt == 0)
        window.location.href = "thankyou.aspx";
</script>
<%--FB 3055-URL End--%>

<script type="text/javascript" language="javascript">



function ViewEndpointDetails()
{
    val = document.getElementById("lstEndpoint").value;
	
    if (val == "" || val == "-1") {
        alert("Please select an Endpoint from the list first.");
    } else {
        url = "dispatcher/admindispatcher.asp?eid=" + val + "&cmd=GetEndpoint&ed=1&wintype=pop";

        if (!window.winrtc) {	// has not yet been defined
            winrtc = window.open(url, "", "width=400,height=310,top=0,left=0,resizable=yes,scrollbars=yes,status=no");
            winrtc.focus();
        } else { // has been defined
            if (!winrtc.closed) {     // still open
                winrtc.close();
                winrtc = window.open(url, "", "width=400,height=310,top=0,left=0,resizable=yes,scrollbars=yes,status=no");
                winrtc.focus();
            } else {
                winrtc = window.open(url, "", "width=400,height=310,top=0,left=0,resizable=yes,scrollbars=yes,status=no");
                winrtc.focus();
            }
        }
    }
}

function getYourOwnEmailList (i)
{
    if (i == -2)//Login Management
    {
//        url = "dispatcher/conferencedispatcher.asp?frm=roomassist&frmname=frmMainroom&cmd=GetEmailList&emailListPage=1&wintype=pop";
      if(queryField("sb") > 0 )
            url = "emaillist2.aspx?t=e&frm=roomassist&wintype=ifr&fn=frmMainroom&n=";
            else
            url = "emaillist2main.aspx?t=e&frm=roomassist&fn=frmMainroom&n=";
    }
    else
    {
//        url = "dispatcher/conferencedispatcher.asp?frm=approver&frmname=frmMainroom&no=" + i + "&cmd=GetEmailList&emailListPage=1&wintype=pop";
        url = "emaillist2main.aspx?t=e&frm=approver&fn=frmMainroom&n=" + i;
	}
    if (!window.winrtc) {	// has not yet been defined
        winrtc = window.open(url, "", "width=920,height=470,top=0,left=0,resizable=yes,scrollbars=yes,status=no"); //FB 2735
        winrtc.focus();
    } else // has been defined
        if (!winrtc.closed) {     // still open
            winrtc.close();
            winrtc = window.open(url, "", "width=920,height=470,top=0,left=0,resizable=yes,scrollbars=yes,status=no"); //FB 2735
            winrtc.focus();
        } else {
            winrtc = window.open(url, "", "width=920,height=470,top=0,left=0,resizable=yes,scrollbars=yes,status=no");//FB 2735
            winrtc.focus();
        }
}


function deleteApprover(id)
{
    eval("document.frmMainroom.Approver" + id + "ID").value = "";
    eval("document.frmMainroom.Approver" + id).value = "";
}

//Endpoint Fix
function deleteEndpoint()
{
    eval("document.frmMainroom.hdnEPID").value = "";
    eval("document.frmMainroom.txtEndpoint").value = "";
}

function deleteAssistant()
{
    eval("document.frmMainroom.AssistantID").value = "";
    eval("document.frmMainroom.Assistant").value = "";
}


function frmMainroom_Validator()
{
    //fB 2415 Start
    if (!Page_ClientValidate())
	        return Page_IsValid; 
	//FB 2415 End
    //for DQA comments - start
    var txtroomname = document.getElementById('<%=txtRoomName.ClientID%>');
    if(txtroomname.value == "")
    {        
        reqName.style.display = 'block';
        txtroomname.focus();
        return false;
    } // FB 1640
    else if (txtroomname.value.search(/^(a-z|A-Z|0-9)*[^\\<>^+?|!`\[\]{}\=@$%&~]*$/)==-1) //FB 1888
    {        
        regRoomName.style.display = 'block';
        txtroomname.focus();
        return false;
    }    
    
    //Coomented for FB 2594
    //var txtroomphone = document.getElementById('<%=txtRoomPhone.ClientID%>');
    //if (txtroomphone.value != '' && txtroomphone.value.search(/^(\(|\d| |-|\))*$/)==-1)
    //{        
      //  regRoomPhone.style.display = 'block';
        //regRoomPhone.innerText = 'Numeric values only';
        //errLabel.innerText = 'Please Enter - Valid Room Phone Number';//Added for FB 1459
        //txtroomphone.focus();
        //return false;
    //}   
    
    var txtmaximumcapacity = document.getElementById('<%=txtMaximumCapacity.ClientID%>')
    if(txtmaximumcapacity != null && txtmaximumcapacity.value != '') //FB 2694
    {
        var maxCapVal = parseInt(txtmaximumcapacity.value);    
        if( !isFinite(maxCapVal) || maxCapVal < 0 || maxCapVal > 10000)
        {
            CapacityValidator.style.display = 'block';
            txtmaximumcapacity.focus();
            return false;
        }
    }   
    
    var txtmaxconcurrentCalls = document.getElementById('<%=txtMaxConcurrentCalls.ClientID%>');
    if (txtmaxconcurrentCalls != null && txtmaxconcurrentCalls.value != '' && txtmaxconcurrentCalls.value.search(/^(\(|\d|\))*$/)==-1) //FB 2694
    {        
        regMaxCall.style.display = 'block';
        txtmaxconcurrentCalls.focus();
        return false;
    }    
    
    var txtsetuptime = document.getElementById('<%=txtSetupTime.ClientID%>');
    if (txtsetuptime != null && txtsetuptime.value != '' && txtsetuptime.value.search(/^(\(|\d|\))*$/)==-1) //FB 2694
    {        
        regMaxCall.style.display = 'block';
        regMaxCall.focus();
        return false;
    }    

    var txtteardowntime = document.getElementById('<%=txtTeardownTime.ClientID%>');
    if (txtteardowntime != null && txtteardowntime.value != '' && txtteardowntime.value.search(/^(\(|\d|\))*$/)==-1) //FB 2694
    {        
        regTeardownTime.style.display = 'block';
        txtteardowntime.focus();
        return false;
    }    
  
    
    var assistant = document.getElementById('<%=Assistant.ClientID%>');
    var editHref = document.getElementById("EditHref");
    
    if(assistant.value == "")
    {
        //alert("Please select the Assistant-in-charge");        
        AssistantValidator.style.display = 'block';
        editHref.focus();
        return false;
    }
    //for DQA comments - end
    
//    var lstvideo = document.getElementById('<%=lstVideo.ClientID%>');
//    if (lstvideo.value == "1" || lstvideo.value == "3") {//Edited for FB 1459
//    errLabel.innerText = ""; //Added for FB 1459
//        if (txtroomphone.value == "") {
//            //alert(EN_150);
//            regRoomPhone.style.display = 'block'; 
//            regRoomPhone.innerText = 'Required';
//            errLabel.innerText = 'Please Enter - Room Phone Number';//Added for FB 1459
//            txtroomphone.focus();
//            return (false);
//        }
//    }   
   
    var cb = document.getElementById('<%=txtMultipleAssistant.ClientID%>');
    if(cb.value != '')
    {
        if(cb.value.search(/^(a-z|A-Z|0-9)*[^\\/<>^+?|!`,\[\]{}\x22=:#$%&()'~]*$/)==-1)
        {   
            regMultipleAssistant.style.display = 'block';
            cb.focus();
            return false;
        }
    }
    
    var emailsary = (cb.value).split(/,| |:|;/g);
    var newval = "";	var num = 0;
    for (i = 0; i<emailsary.length; i++) {
        if (Trim(emailsary[i]) != "") {
            newval += Trim(emailsary[i]) + ";"
            num ++;
        }
    }
    newval = Trim(newval);
    while ( (newval.length > 0) && (newval.charAt(newval.length-1) == ";") )
        newval = newval.substring(0, newval.length-1);

    cb.value = newval;
    if (num > 10) {
        alert(EN_192);
        cb.focus();
        return false;
    }
    
    var lsttoptier = document.getElementById('<%=lstTopTier.ClientID%>');
    var lstmiddletier = document.getElementById('<%=lstMiddleTier.ClientID%>');
    
    if(lsttoptier.value == "-1")
    {
        //alert("Please select the Top Tier");
        reqTopTier.style.display = 'block';
        lsttoptier.focus();
        return false;
    }

    if(lstmiddletier.value == "-1")
    {
        //alert("Please select the Middle Tier");
        reqMiddleTier.style.display = 'block';
        lstmiddletier.focus();
        return false;
    }

    var txtzipcode = document.getElementById('<%=txtZipCode.ClientID%>');
    //FB 2222
    if (txtzipcode.value != '' && txtzipcode.value.search(/^(a-z|A-Z|0-9)*[^\\/<>^+;?|!`,\[\]{}\x22;=:@#$%&()'~]*$/)==-1) 
    {        
        regZipCode.style.display = 'block';
        txtzipcode.focus();
        return false;
    }  
    
    var txtparkingdirections = document.getElementById('<%=txtParkingDirections.ClientID%>');
    if (txtparkingdirections.value != '' && txtparkingdirections.value.search(/[^`']*/)==-1)
    {        
        regParkDirections.style.display = 'block';
        txtparkingdirections.focus();
        return false;
   }     
    
    var txtaddcomments = document.getElementById('<%=txtAdditionalComments.ClientID%>');
    if (txtaddcomments.value != '' && txtaddcomments.value.search(/^(a-z|A-Z|0-9)*[^\\/<>^+;?|!`,\[\]{}\x22;=:@#$%&()'~]*$/)==-1)
    {        
        regAddComments.style.display = 'block';
        txtaddcomments.focus();
        return false;
    }  
    
    var txtmaplink = document.getElementById('<%=txtMapLink.ClientID%>');
    if (txtmaplink.value != '' && txtmaplink.value.search(/[^`']*/)==-1)
    {        
        regMapLink.style.display = 'block';
        txtmaplink.focus();
        return false;
    }  
    
    var lsttimezone = document.getElementById('<%=lstTimezone.ClientID%>');
    if(lsttimezone.value == "-1")
    {    
        //alert("Please select the Timezone");
        regTimeZone.style.display = 'block';
        lsttimezone.focus();
        return false;
    }   
    
   /* var txtlongitude = document.getElementById('<%=txtLongitude.ClientID%>');
    if (txtlongitude.value != '' && txtlongitude.value.search(/^(\(|\d| |-|\))*$/)==-1)
    {        
        regLongitude.style.display = 'block';
        txtlongitude.focus();
        return false;
    }  
   
    var txtlatitude = document.getElementById('<%=txtLatitude.ClientID%>');
    if (txtlatitude.value != '' && txtlatitude.value.search(/^(\(|\d| |-|\))*$/)==-1)
    {        
        regLatitude.style.display = 'block';
        txtlatitude.focus();
        return false;
    }  */
    // dept
    // FB 2342 starts
    //FB 2342 stopped validation
//    var txtroomemail1 = document.getElementById('<%=txtRoomEmail.ClientID%>');
//    if(txtroomemail1.value == "")
//    {        
//        reqRoomEmail.style.display = 'block';
//        txtroomemail1.focus();
//        return false;
//    }
//    else 
//    {
//         var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
//         if(reg.test (txtroomemail1 .value)==false)
//         {
//                reqroomEmail1_1.style.display='block';
//                txtroomemail1.focus();
//                return false;
//         }
//    } 
    // FB 2342 end
    var cb1 = document.getElementById('<%=lstVideo.ClientID%>');
    var cb2 = document.getElementById('<%=txtEndpoint.ClientID%>');
    if ( (cb1.selectedIndex != 0) && (cb2.value == "") ){
        //alert(EN_205);
        regEndPoint.style.display = 'block';
        regEndPoint.innerText = 'Please add a endpoint, because you select a type of media.';
        cb2.focus();
        return false;
    }
     if ( (cb1.selectedIndex <= 0) && (cb2.value != "") ) {
        //alert(EN_204);
        regVideo.style.display = 'block';
        regVideo.innerText = 'When selecting endpoint, please select Media for it.';
        cb1.focus();
        return false;
    }
    
    var hdnmultipledept = document.getElementById('<%=hdnMultipleDept.ClientID%>');
    var departmentlist = document.getElementById('<%=DepartmentList.ClientID%>');

    if (hdnmultipledept.value == 1) 
    {
        if ((departmentlist.value == "") && (departmentlist.length > 0) ) 
        {
            isConfirm = confirm("Are you sure you want to set up this room with no department(s) assigned?\n")
            if (isConfirm == false)
            {
                return(false);
            }
        }
    }
   
    //FB 2400 Starts
    if(cb1.value != "0")
    {
        var RoomTele = document.getElementById('<%=DrpisTelepresence.ClientID%>')
        var EPTele = document.getElementById('<%=hdnisEPTelePresence.ClientID%>');
        if(EPTele.value != RoomTele.value)
        {
              alert("Please check the Telepresence type of the endpoint selected for the room.")
              return(false);
        }
    }
    //FB 2400 Ends
    DataLoading(1);//ZD 100176
    return(true);
    
}

function fnClose()
{
    DataLoading(1);//ZD 100176 
//    window.location.replace('dispatcher/admindispatcher.asp?cmd=ManageConfRoom'); //Login management
    window.location.replace("manageroom.aspx?hf=&m=&pub=&d=&comp=&f=&frm=");//Login management
    
    return true;
}


function fnOpen()
{
   //var url = "manageimage.asp?p=image/room/";
   var url = "manageimage.aspx";// This code is added for ManageImage.aspx Conversion
   window.open(url, "", "width=700,height=500,top=0,left=0,resizable=yes,scrollbars=no,status=no");// This code is edited for ManageImage.aspx Conversion
   return false;
}

function AddImage(imgname)
{
	addopt(document.getElementById('<%=drpSecImgList.ClientID%>'), imgname, imgname, true, true);
}

function DelImage(imgname)
{
	deloptbyval(document.getElementById('<%=drpSecImgList.ClientID%>'), imgname);
}

function chgEndpoint()
{
    var endPt = document.getElementById('<%=lstEndpoint.ClientID%>');
	document.getElementById('<%=btnEndpointDetails.ClientID%>').disabled = (endPt.value == "-1") ? "none" : "";
}

//Added for FB 1459  Start
//function fnRmIsEmpty()
//{
//    var ltvideo = document.getElementById('<%=lstVideo.ClientID%>');
//    if (ltvideo.value == "1" || ltvideo.value == "3") {
//        if(document.getElementById("txtroomphone").value == " ") {
//            errLabel.innerText = 'Please Enter - Valid Room Phone Number';
//            document.getElementById("txtroomphone").focus();
//            return false;
//        }
//    }
//}

//Added for FB 1459  End

//added for Endpoint Search - Start
function OpenEndpointSearch()
{

        if(OpenEndpointSearch.arguments != null)
        {
            var rmargs = OpenEndpointSearch.arguments;
            var prnt = rmargs[0];
            var isTele = document.getElementById('<%=DrpisTelepresence.ClientID%>'); //FB 2400
            var url = "";
            //FB 2694 Starts
            if(isTele != null)
                url = "EndpointSearch.aspx?frm=" + prnt + "&isRoomTel=" + isTele.value; //FB 2400
            else
                url = "EndpointSearch.aspx?frm="+prnt + "&isRoomTel=0";
           //FB 2694 Ends
            window.open(url, "EndpointSearch", "width="+ screen. availWidth +",height=666px,resizable=no,scrollbars=yes,status=no,top=0,left=0");
        }
       
    
}
function AddEndpoint()
{
  
   
}

//added for Endpoint Search - End

function toggleDiv(id,flagit) 
{
    if (flagit=="1")
    {
        if (document.layers) document.layers[''+id+''].visibility = "show"
        else if (document.all) document.all[''+id+''].style.visibility = "visible"
        else if (document.getElementById) document.getElementById(''+id+'').style.visibility = "visible"
    }
    else
        if (flagit=="0")
        {
            if (document.layers) document.layers[''+id+''].visibility = "hide"
            else if (document.all) document.all[''+id+''].style.visibility = "hidden"
            else if (document.getElementById) document.getElementById(''+id+'').style.visibility = "hidden"
        }
}

// FB 2136 Start
function fnOpenSecurityBadge()
{

var e = document.getElementById("drpSecImgList");
var selOption = e.options[e.selectedIndex].text;
var optionArray = selOption.split(".");
var url = "ManageSecurityBadge.aspx?drpSelOption=" + optionArray[0] + "_" + "<%=Session["organizationID"]%>" + ".jpg" ;
window.open(url, "", "width=675,height=500,top=0,left=0,resizable=no,scrollbars=no,status=no");// This code is edited for ManageImage.aspx Conversion
return false;

}

// >> Please refer updateImage() in ManageSecurityBadge.aspx <<
function fnUpdateHdnField(selValue)
{
alert('auto' + selValue.substring(0, selValue.length-4));
document.getElementById('hdnSelecOption').value = selValue.substring(0, selValue.length-4);
}

// FB 2136 End

function fnSecImgSelection()
{
    var e = document.getElementById("drpSecImgList");
    var selOption = e.options[e.selectedIndex].value;
    document.getElementById('hdnSecSelection').value = selOption;
}
//ZD 100176 start
function DataLoading(val)
{
if (val == "1")
    document.getElementById("dataLoadingDIV").innerHTML = "<img border='0' src='image/wait1.gif'>";
 else
    document.getElementById("dataLoadingDIV").innerHTML = "";
}
//ZD 100176 End



</script>

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>myVRM</title>
   <script type="text/javascript"> // FB 2815
    var path = '<%=Session["OrgCSSPath"]%>';
    path = path.split('.')[0] + '<%=Session["ThemeType"]%>' + ".css";
    document.write('<link rel="stylesheet" title="Expedite base styles" type="text/css" href="' + path + '?' + new Date().getTime() + '">');
   </script>   
</head>
<body>
    <form id="frmMainroom" runat="server" onsubmit="DataLoading(1)"><%--ZD 100176--%> 
    <asp:ScriptManager ID="RoomImageScript" runat="server">
    </asp:ScriptManager>
    <input type="hidden" runat="server" id="hdnMultipleDept" />
    <input type="hidden" runat="server" id="hdnRoomID" />
    <input type="hidden" runat="server" id="hdnSelecOption" />
    <input type="hidden" runat="server" id="hdnSecSelection" />
    <input type="hidden" runat="server" id="hdnisEPTelePresence" /> <%--FB 2400--%>
    <%--FB 2136--%>
    <input name="hdnEPID" type="hidden" id="hdnEPID" runat="server" />
    <%--Endpoint Search--%>
    <%--Code changed for FB 1425 QA Bug -Start--%>
    <input type="hidden" id="hdntzone" runat="server" />
    <%--Code changed for FB 1425 QA Bug -End--%>
    <div>
        <center>
            <div id="dataLoadingDIV" style="z-index: 1" align="center"> <%--ZD 100176--%>
            </div>
            <h3>
                <asp:Label ID="lblTitle" runat="server" CssClass="h3" Text="Room"></asp:Label>
            </h3>
            <asp:Label ID="errLabel" runat="server" CssClass="lblError"></asp:Label>
        </center>
        <table cellpadding="0" cellspacing="0" border="0" style="width: 100%">
            <%--table 1 starts here--%>
            <tr>
                <%--Basic Configuration--%>
                <td align="center">
                    <table id="tblBasicConfiguration" cellpadding="2" cellspacing="2" border="0" style="width: 95%">
                        <tr>
                            <td colspan="2" align="left">
                                <table id="Table4" cellpadding="2" cellspacing="2" border="0" style="width: 100%; margin-left:-20px">
                                    <tr align="left">
                                        <td style="width: 10%" align="left" valign="top" class="blackblodtext" nowrap>
                                            Last Modified by :
                                        </td>
                                        <td style="width: 85%" align="left" valign="top">
                                            <asp:Label ID="lblMUser" runat="server" CssClass="active"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 10%" align="left" valign="top" class="blackblodtext" nowrap>
                                            Last Modified at :
                                        </td>
                                        <td style="width: 85%" align="left" valign="top">
                                            <asp:Label ID="lblMdate" runat="server" CssClass="active"></asp:Label>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td align="left">
                                <span class="subtitleblueblodtext" style="margin-left:-15px">Basic Configuration</span>
                            </td>
                            <td class="reqfldText" align="center">
                                * Required Field
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <table id="tblBasicConfigurationDetials" cellpadding="2" cellspacing="2" border="0"
                                    style="width: 95%">
                                    <%-- Basic Configuration Parameters starts here --%>
                                    <tr>
                                        <%--Window Dressing--%><%--FB 2579 Start--%>
                                        <td style="width: 15%" align="left" valign="top" class="blackblodtext">
                                            Room Name <span class="reqfldText">*</span>
                                        </td>
                                        <td style="width: 35%" align="left" valign="top">
                                            <%--Edited For FF & FB 2050--%>
                                            <asp:TextBox ID="txtRoomName" runat="server" CssClass="altText"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="reqName" runat="server" ControlToValidate="txtRoomName"
                                                Display="dynamic" CssClass="lblError" SetFocusOnError="true" Text="Required"
                                                ValidationGroup="Submit"></asp:RequiredFieldValidator>
                                            <%-- Code Added for FB 1640--%>
                                            <asp:RegularExpressionValidator ID="regRoomName" ControlToValidate="txtRoomName"
                                                Display="dynamic" runat="server" ValidationGroup="Submit" SetFocusOnError="true"
                                                ErrorMessage="<br> & < > + % \ ? | ^ = ! ` [ ] { } $ @  and ~ are invalid characters."
                                                ValidationExpression="^(a-z|A-Z|0-9)*[^\\<>^+?|!`\[\]{}\=@$%&~]*$"></asp:RegularExpressionValidator>
                                            <%--FB 1888--%>
                                        </td>
                                        <%--Window Dressing--%>
                                        <td style="width: 20%" align="left" valign="top" class="blackblodtext">
                                            Room Phone Number
                                        </td>
                                        <%--Edited For FF--%>
                                        <td style="width: 30%" align="left" valign="top">
                                            <%--Edited For FF--%>
                                            <asp:TextBox ID="txtRoomPhone" runat="server" CssClass="altText"></asp:TextBox>
                                            <%--Edited for FB 1459--%><%--FB 2594--%>
                                            <asp:RegularExpressionValidator ID="regRoomPhone" ControlToValidate="txtRoomPhone"
                                                Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="& < > ' % \ ; ? | ^ = ! ` [ ] { } : # $  ~ and &#34; are invalid characters."
                                                ValidationGroup="Submit" ValidationExpression="^(a-z|A-Z|0-9)*[^\\<>^;?|!`\[\]{}\x22;=:#$%&'~]*$"></asp:RegularExpressionValidator>
                                        </td>
                                    </tr>
                                    <tr id="trCallCapacity" runat="server"> <%--FB 2694--%>
                                        <%--Window Dressing--%>
                                        <td style="width: 15%" align="left" valign="top" class="blackblodtext">
                                            Maximum Capacity
                                        </td>
                                        <td style="width: 30%" align="left" valign="top">
                                            <%--Edited For FF--%>
                                            <asp:TextBox ID="txtMaximumCapacity" runat="server" CssClass="altText"></asp:TextBox>
                                            <asp:RangeValidator ID="CapacityValidator" runat="server" ControlToValidate="txtMaximumCapacity"
                                                SetFocusOnError="true" Type="integer" MinimumValue="0" MaximumValue="10000" CssClass="lblError"
                                                Text="Maximum Capacity between 0 and 10,000. "></asp:RangeValidator>
                                        </td>
                                        <%--Window Dressing--%>
                                        <td style="width: 20%" align="left" valign="top" class="blackblodtext">
                                            Maximum # of Concurrent Phone Calls
                                        </td>
                                        <%--Edited For FF--%>
                                        <td style="width: 30%" align="left" valign="top">
                                            <%--Edited For FF--%>
                                            <asp:TextBox ID="txtMaxConcurrentCalls" runat="server" CssClass="altText"></asp:TextBox>
                                            <asp:RegularExpressionValidator ID="regMaxCall" ControlToValidate="txtMaxConcurrentCalls"
                                                Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="Enter Numbers only."
                                                ValidationGroup="Submit" ValidationExpression="^(\(|\d|\))*$"></asp:RegularExpressionValidator>
                                        </td>
                                    </tr>
                                    <tr id="trsetupteardown" runat="server"> <%--FB 2694--%>
                                        <%--Window Dressing--%>
                                        <td style="width: 15%" align="left" valign="top" class="blackblodtext">
                                            Setup Time Buffer (minutes)
                                        </td>
                                        <td style="width: 30%" align="left" valign="top">
                                            <%--Edited For FF--%>
                                            <asp:TextBox ID="txtSetupTime" runat="server" CssClass="altText"></asp:TextBox>
                                            <asp:RegularExpressionValidator ID="regSetupTime" ControlToValidate="txtSetupTime"
                                                Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="Enter Numbers only."
                                                ValidationGroup="Submit" ValidationExpression="^(\(|\d|\))*$"></asp:RegularExpressionValidator>
                                        </td>
                                        <%--Window Dressing--%>
                                        <td style="width: 20%" align="left" valign="top" class="blackblodtext">
                                            Teardown Time Buffer (minutes)
                                        </td>
                                        <%--Edited For FF--%>
                                        <td style="width: 30%" align="left" valign="top">
                                            <%--Edited For FF--%>
                                            <asp:TextBox ID="txtTeardownTime" runat="server" CssClass="altText"></asp:TextBox>
                                            <asp:RegularExpressionValidator ID="regTeardownTime" ControlToValidate="txtTeardownTime"
                                                Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="Enter Numbers only."
                                                ValidationGroup="Submit" ValidationExpression="^(\(|\d|\))*$"></asp:RegularExpressionValidator>
                                        </td>
                                    </tr>
                                    <tr>
                                        <%--Window Dressing--%>
                                        <td align="left" valign="top" class="blackblodtext">
                                            <asp:Label runat="server" ID="lblProjector">Projector Available?</asp:Label>   <%--FB 2694--%>
                                        </td>
                                        <td align="left" valign="top">
                                            <asp:DropDownList ID="lstProjector" runat="server" CssClass="SelectFormat">
                                                <asp:ListItem Text="Yes" Value="1" Selected="True" />
                                                <asp:ListItem Text="No" Value="0" />
                                            </asp:DropDownList>
                                        </td>
                                        <%--Window Dressing--%>
                                        <td align="left" valign="top" class="blackblodtext">
                                            Media
                                        </td>
                                        <td align="left" valign="top">
                                            <asp:DropDownList ID="lstVideo" runat="server" CssClass="SelectFormat">
                                                <asp:ListItem Text="[None]" Value="0" Selected="True" />
                                                <asp:ListItem Text="Audio-only" Value="1" />
                                                <asp:ListItem Text="Audio, Video" Value="2" />
                                                <%--FB 1744--%>
                                            </asp:DropDownList>
                                            <asp:RequiredFieldValidator ID="regVideo" runat="server" ControlToValidate="lstVideo"
                                                Display="dynamic" CssClass="lblError" SetFocusOnError="true" Text="Required"
                                                ValidationGroup="Submit"></asp:RequiredFieldValidator>
                                        </td>
                                    </tr>
                                    <tr>
                                        <%--Window Dressing--%>
                                        <td style="width: 15%" align="left" valign="top" class="blackblodtext">
                                            Assistant-In-Charge<span class="reqfldText">*</span><%--FB 2974--%>
                                        </td>
                                        <td style="width: 30%" align="left" valign="top">
                                            <%--Edited For FF--%>
                                            <asp:TextBox ID="Assistant" runat="server" CssClass="altText"></asp:TextBox>
                                            <a id="EditHref" href="javascript: getYourOwnEmailList(-2);" onmouseover="window.status='';return true;">
                                                <img border="0" src="image/edit.gif" alt="edit" width="17" height="15" style="cursor:pointer;" title="myVRM Address Book" ></a> <%--FB 2798--%>
                                            <a href="javascript: deleteAssistant();" onmouseover="window.status='';return true;">
                                                <img border="0" src="image/btn_delete.gif" alt="delete" width="16" height="16" style="cursor:pointer;" title="Delete"></a> <%--FB 2798--%>
                                            <asp:RequiredFieldValidator ID="AssistantValidator" runat="server" ControlToValidate="Assistant"
                                                Display="dynamic" CssClass="lblError" SetFocusOnError="true" Text="Required"
                                                ValidationGroup="Submit"></asp:RequiredFieldValidator>
                                        </td>
                                        <%--Window Dressing--%>
                                        <td style="width: 20%" align="left" valign="top" class="blackblodtext">
                                            Multiple assistant emails<br />
                                            (Semicolon separated)
                                        </td>
                                        <%--Edited For FF--%>
                                        <td style="width: 30%" align="left" valign="top">
                                            <%--Edited For FF--%>
                                            <asp:TextBox ID="txtMultipleAssistant" Rows="2" TextMode="MultiLine" Width="275px"
                                                runat="server" CssClass="altText"></asp:TextBox> <%-- FB 2050 --%>
                                            <asp:RegularExpressionValidator ID="regMultipleAssistant" ControlToValidate="txtMultipleAssistant"
                                                Display="dynamic" runat="server" ValidationGroup="Submit" SetFocusOnError="true"
                                                ErrorMessage="<br>& < > ' + % \ / ( ) ; ? | ^ = ! ` , [ ] { } : # $ ~ and &#34 are invalid characters."
                                                ValidationExpression="^(a-z|A-Z|0-9)*[^\\/<>^+?|!`,\[\]{}\x22=:#$%&()'~]*$"></asp:RegularExpressionValidator>
                                        </td>
                                    </tr>
                                    <tr id="trVIP" runat="server"> <%--FB 2694--%>
                                        <%--FB 1982--%>
                                        <%--Window Dressing--%>
                                        <td align="left" valign="top" class="blackblodtext">
                                            VIP Room
                                        </td>
                                        <td align="left">
                                            <asp:DropDownList ID="DrpisVIP" runat="server" CssClass="SelectFormat">
                                                <asp:ListItem Text="Yes" Value="1" />
                                                <asp:ListItem Text="No" Value="0" Selected="True" />
                                            </asp:DropDownList>
                                        </td>
                                        <td align="left" valign="top" class="blackblodtext">
                                            &nbsp;
                                        </td>
                                        <td align="left">
                                            &nbsp;
                                        </td>
                                    </tr>
                                    <tr>
                                        <%--Window Dressing--%>
                                        <td align="left" valign="top" class="blackblodtext">
                                            Caterer Facility?
                                        </td>
                                        <td align="left">
                                            <asp:DropDownList ID="CatererList" runat="server" CssClass="SelectFormat">
                                                <asp:ListItem Text="Yes" Value="1" Selected="True" />
                                                <asp:ListItem Text="No" Value="0" />
                                            </asp:DropDownList>
                                        </td>
                                        <td align="left" valign="top" class="blackblodtext">
                                            Handicapped Access
                                        </td>
                                        <td align="left">
                                            <asp:DropDownList ID="DrpHandi" runat="server" CssClass="SelectFormat">
                                                <asp:ListItem Text="Yes" Value="1" />
                                                <asp:ListItem Text="No" Value="0" Selected="True" />
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                    <tr id="trServiceType" runat="server"> <%--FB 2694--%>
                                        <td align="left" valign="top" class="blackblodtext">
                                            Telepresence Room
                                        </td>
                                        <td align="left">
                                            <asp:DropDownList ID="DrpisTelepresence" runat="server" CssClass="SelectFormat">
                                                <asp:ListItem Text="Yes" Value="1" />
                                                <asp:ListItem Text="No" Value="0" Selected="True" />
                                            </asp:DropDownList>
                                        </td>
                                        <td align="left" valign="Middle" class="blackblodtext"><%--FB 2219--%>
                                            Service Type
                                        </td>
                                        <td align="left">
                                            <asp:DropDownList ID="DrpServiceType" CssClass="SelectFormat" DataTextField="Name"
                                                DataValueField="ID" runat="server">
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                    <tr id="trDedication" runat="server"> <%--FB 2694--%>
                                        <%--FB 2334--%>
                                        <td align="left" height="38">
                                            <span class="blackblodtext">Dedicated Video</span>
                                        </td>
                                        <td align="left">
                                            <input type="checkbox" id="Chkdedicatedvideo" runat="server" />
                                        </td>
                                        <%--FB 2390--%>
                                        <td align="left" height="38"><span class="blackblodtext">Dedicated Presentation Codec</span></td>
                                        <td align="left">
                                            <input type="checkbox" id="Chkdedpresentcodec" runat="server" />
                                        </td>
                                    </tr>
                                    <%--FB 2415 Start--%>
                                    <tr id="trAVEmail" runat="server"> <%--FB 2694--%>
                                        <td align="left" height="38">
                                            <span class="blackblodtext">AV Onsite support Email</span>
                                        </td>
                                        <td align="left">
                                            <asp:TextBox ID="txtAVOnsiteSupportEmail" runat="server" CssClass="altText"></asp:TextBox>
                                            <asp:RegularExpressionValidator ID="regEmail1_1" ControlToValidate="txtAVOnsiteSupportEmail"
                                                Display="dynamic" runat="server" ErrorMessage="<br>Invalid email address." ValidationExpression="[a-zA-Z][\w\.-]*[a-zA-Z0-9]@[a-zA-Z0-9][\w\.-]*[a-zA-Z0-9]\.[a-zA-Z][a-zA-Z\.]*[a-zA-Z]$"></asp:RegularExpressionValidator>
                                            <asp:RegularExpressionValidator ID="regEmail1_2" ControlToValidate="txtAVOnsiteSupportEmail"
                                                Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<br>& < > ' + % \ /(); ? | ^= ! ` , [ ] { } : # $ ~ and &#34; are invalid characters."
                                                ValidationExpression="^(a-z|A-Z|0-9)*[^\\/<>+;?|!`,\[\]{}\x22;=:#$%&'~]*$"></asp:RegularExpressionValidator>
                                        </td>
                                        <td colspan="2">
                                        </td>
                                    </tr>
                                    <%--FB 2415 End--%>
                                </table>
                                <%-- Basic Configuration Parameters end here --%>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <%--Location--%>
                <td align="center">
                    <table id="tblLocation" cellpadding="2" cellspacing="2" border="0" style="width: 95%">
                        <tr>
                            <%--Window Dressing--%>
                            <td align="left">
                                <span class="subtitleblueblodtext" style="margin-left:-15px">Location</span>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <table id="tblLocationDetails" cellpadding="2" cellspacing="2" border="0" style="width: 95%">
                                    <%-- Basic Configuration Parameters starts here --%>
                                    <tr>
                                        <%--Window Dressing--%>
                                        <td style="width: 15%" align="left" valign="top" class="blackblodtext">
                                            Top Tier <span class="reqfldText">*</span>
                                        </td>
                                        <td style="width: 34%" align="left" valign="top"><%-- FB 2694--%>
                                            <%--Edited For FF & FB 2050--%>
                                            <asp:DropDownList ID="lstTopTier" DataTextField="Name" DataValueField="ID" runat="server"
                                                CssClass="altSelectFormat" OnSelectedIndexChanged="UpdateMiddleTiers" AutoPostBack="true"
                                                onchange="javascript:DataLoading(1)">
                                            </asp:DropDownList>
                                            <asp:RequiredFieldValidator ID="reqTopTier" runat="server" ControlToValidate="lstTopTier"
                                                Display="dynamic" CssClass="lblError" SetFocusOnError="true" Text="Required"
                                                ValidationGroup="Submit"></asp:RequiredFieldValidator>
                                        </td>
                                        <%--Window Dressing--%>
                                        <td style="width: 21%" align="left" valign="top" class="blackblodtext"><%-- FB 2694--%>
                                            Middle Tier<%--Edited For FF--%>
                                            <span class="reqfldText">*</span>
                                        </td>
                                        <td style="width: 30%" align="left" valign="top">
                                            <%--Edited For FF--%>
                                            <asp:DropDownList ID="lstMiddleTier" DataTextField="Name" DataValueField="ID" runat="server"
                                                CssClass="altSelectFormat">
                                            </asp:DropDownList>
                                            <asp:RequiredFieldValidator ID="reqMiddleTier" runat="server" ControlToValidate="lstMiddleTier"
                                                Display="dynamic" CssClass="lblError" SetFocusOnError="true" Text="Required"
                                                ValidationGroup="Submit"></asp:RequiredFieldValidator>
                                        </td>
                                    </tr>
                                    <tr>
                                        <%--Window Dressing--%>
                                        <td style="width: 16%" align="left" valign="top" class="blackblodtext"><%-- FB 2694--%>
                                            Floor
                                        </td>
                                        <td style="width: 30%" align="left" valign="top">
                                            <%--Edited For FF--%>
                                            <asp:TextBox ID="txtFloor" runat="server" CssClass="altText"></asp:TextBox>
                                        </td>
                                        <%--Window Dressing--%>
                                        <td style="width: 20%" align="left" valign="top" class="blackblodtext">
                                            Room #
                                        </td>
                                        <%--Edited For FF--%>
                                        <td style="width: 30%" align="left" valign="top">
                                            <%--Edited For FF--%>
                                            <asp:TextBox ID="txtRoomNumber" runat="server" CssClass="altText"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <%--Window Dressing--%>
                                        <td style="width: 15%" align="left" valign="top" class="blackblodtext">
                                            Street Address 1
                                        </td>
                                        <td style="width: 30%" align="left" valign="top">
                                            <%--Edited For FF--%>
                                            <asp:TextBox ID="txtStreetAddress1" runat="server" CssClass="altText"></asp:TextBox>
                                        </td>
                                        <%--Window Dressing--%>
                                        <td style="width: 20%" align="left" valign="top" class="blackblodtext">
                                            Street Address 2
                                        </td>
                                        <%--Edited For FF--%>
                                        <td style="width: 30%" align="left" valign="top">
                                            <%--Edited For FF--%>
                                            <asp:TextBox ID="txtStreetAddress2" runat="server" CssClass="altText"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <%--Window Dressing--%>
                                        <td style="width: 15%" align="left" valign="top" class="blackblodtext">
                                            City
                                        </td>
                                        <td style="width: 30%" align="left" valign="top">
                                            <%--Edited For FF--%>
                                            <asp:TextBox ID="txtCity" runat="server" CssClass="altText"></asp:TextBox>
                                        </td>
                                        <td style="width:20%" ><%--FB 2657 UI Modification Start--%>
                                            <table cellpadding="2"  style="width:100%; border-collapse:collapse; text-align:left; vertical-align:top; margin-left:-1px" ><%-- FB 2694--%>
                                                <tr>
                                                    <td align="left" ><%-- FB 2694--%>
                                                        <span class="blackblodtext" align="left">Country </span>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="left"><%-- FB 2694--%>
                                                        <span class="blackblodtext"><b>State/Province</b><%--FB 2657--%>
                                                        </span>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                        <td style="width: 30%">
                                            <table cellpadding="2" style="width: 100%; border-collapse: collapse; text-align: left">
                                                <tr>
                                                    <td>
                                                        <asp:DropDownList ID="lstCountries" CssClass="altText" runat="server" DataTextField="Name"
                                                            DataValueField="ID" OnSelectedIndexChanged="UpdateStates" AutoPostBack="true"
                                                            Width="275px">
                                                            <%-- FB 2050 --%>
                                                        </asp:DropDownList>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <asp:DropDownList ID="lstStates" CssClass="altText" Width="110" runat="server" DataTextField="Code"
                                                            DataValueField="ID">
                                                            <%--FB 2657--%>
                                                        </asp:DropDownList>
                                                        &nbsp&nbsp<b>Postal Code</b><%--FB 2657--%>
                                                        <asp:TextBox ID="txtZipCode" Width="50" runat="server" CssClass="altText"></asp:TextBox>
                                                        <asp:RegularExpressionValidator ID="regZipCode" ControlToValidate="txtZipCode" Display="dynamic"
                                                            runat="server" SetFocusOnError="true" ErrorMessage="<br> & < > ' + % \ ; ? | ^ = ! ` [ ] { } : # $ @ ~ and &#34; are invalid characters."
                                                            ValidationGroup="Submit" ValidationExpression="^(a-z|A-Z|0-9)*[^\\<>^+;?|!`\[\]{}\x22;=:@#$%&'~]*$"></asp:RegularExpressionValidator>
                                                        <%--FB 2222--%><%--FB 2657 UI Modification End--%>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <%--FB 2342 starts--%>
                                    <tr>
                                        <td style="width: 15%" align="left" valign="top" class="blackblodtext">
                                            Room Email Queue
                                        </td>
                                        <td align="left" valign="top" height="10" style="width: 50">
                                            <asp:TextBox ID="txtRoomEmail" runat="server" CssClass="altText"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="reqRoomEmail" ControlToValidate="txtRoomEmail" Display="dynamic"
                                                ErrorMessage="<B>Required" runat="server" Enabled="false" CssClass="lblError"></asp:RequiredFieldValidator>
                                            <asp:RegularExpressionValidator ID="reqroomEmail1_1" ValidationGroup="Submit" ControlToValidate="txtRoomEmail"
                                                Display="dynamic" runat="server" Enabled="false" ErrorMessage="<B>Invalid Email Id"
                                                ValidationExpression="[a-zA-Z][\w\.-]*[a-zA-Z0-9]@[a-zA-Z0-9][\w\.-]*[a-zA-Z0-9]\.[a-zA-Z][a-zA-Z\.]*[a-zA-Z]$"
                                                SetFocusOnError="true" CssClass="lblError"></asp:RegularExpressionValidator>
                                            <asp:RegularExpressionValidator ID="reqroomEmail1_2" ControlToValidate="txtRoomEmail"
                                                ValidationGroup="Submit" Display="dynamic" runat="server" Enabled="false" SetFocusOnError="true"
                                                ErrorMessage="<br>& < > ' + % \ /(); ? | ^= ! ` , [ ] { } : # $ ~ and &#34; are invalid characters."
                                                ValidationExpression="^(a-z|A-Z|0-9)*[^\\/<>+;?|!`,\[\]{}\x22;=:#$%&'~]*$" CssClass="lblError"></asp:RegularExpressionValidator>
                                        </td>
                                          <%--ZD 100196 Start--%>
                                        <td style="width: 20%" align="left" valign="top" class="blackblodtext">
                                            Room UID
                                        </td>
                                        <td style="width: 30%" align="left" valign="top">
                                            <asp:TextBox ID="txtRoomUID" runat="server" CssClass="altText"></asp:TextBox>
                                        </td>
                                        <%--FB 2724 End--%>
                                    </tr>
                                    <%--FB 2342 end--%>
                                    <tr>
                                        <%--Window Dressing--%>
                                        <td style="width: 15%" align="left" valign="top" class="blackblodtext">
                                            Parking Directions
                                        </td>
                                        <td style="width: 30%" align="left" valign="top">
                                            <%--Edited For FF--%>
                                            <asp:TextBox ID="txtParkingDirections" TextMode="MultiLine" Rows="2" Width="275"
                                                runat="server" CssClass="altText"></asp:TextBox>
                                            <%-- FB 2050 --%>
                                            <asp:RegularExpressionValidator ID="regParkDirections" ControlToValidate="txtParkingDirections"
                                                Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="' and ` is Invalid Character."
                                                ValidationGroup="Submit" ValidationExpression="[^`']*"></asp:RegularExpressionValidator>
                                        </td>
                                        <%--Window Dressing--%>
                                        <td style="width: 20%" align="left" valign="top" class="blackblodtext">
                                            Additional Comments
                                        </td>
                                        <%--Edited For FF--%>
                                        <td style="width: 30%" align="left" valign="top">
                                            <%--Edited For FF--%>
                                            <asp:TextBox ID="txtAdditionalComments" TextMode="MultiLine" Rows="2" Width="275"
                                                runat="server" CssClass="altText"></asp:TextBox>
                                            <%-- FB 2050 --%>
                                            <asp:RegularExpressionValidator ID="regAddComments" ControlToValidate="txtAdditionalComments"
                                                Display="dynamic" runat="server" ValidationGroup="Submit" SetFocusOnError="true"
                                                ErrorMessage="<br>& < > ' + % \ / ( ) ; ? | ^ = ! ` , [ ] { } : # $ @ ~ and &#34; are invalid characters."
                                                ValidationExpression="^(a-z|A-Z|0-9)*[^\\/<>^+;?|!`,\[\]{}\x22;=:@#$%&()'~]*$"></asp:RegularExpressionValidator>
                                        </td>
                                    </tr>
                                    <tr>
                                        <%--Window Dressing--%>
                                        <td style="width: 15%" align="left" valign="top" class="blackblodtext">
                                            Map Link
                                        </td>
                                        <td style="width: 30%" align="left" valign="top">
                                            <%--Edited For FF--%>
                                            <asp:TextBox ID="txtMapLink" TextMode="MultiLine" Rows="2" Width="275" runat="server"
                                                CssClass="altText"></asp:TextBox>
                                            <%-- FB 2050 --%>
                                            <asp:RegularExpressionValidator ID="regMapLink" ControlToValidate="txtMapLink" Display="dynamic"
                                                runat="server" SetFocusOnError="true" ErrorMessage="' and ` are Invalid Characters."
                                                ValidationGroup="Submit" ValidationExpression="[^`']*"></asp:RegularExpressionValidator>
                                        </td>
                                        <%--Window Dressing--%>
                                        <%--Code changed for FB 1425 QA Bug -Start--%>
                                        <td style="width: 20%" align="left" valign="top" id="TzTD1" runat="server" class="blackblodtext">
                                            Timezone<%--Edited For FF--%>
                                            <span class="reqfldText">*</span>
                                        </td>
                                        <td style="width: 30%" align="left" valign="top" id="TzTD2" runat="server">
                                            <%--Edited For FF--%>
                                            <asp:DropDownList ID="lstTimezone" runat="server" CssClass="altSelectFormat" DataTextField="timezoneName"
                                                DataValueField="timezoneID">
                                            </asp:DropDownList>
                                            <asp:RequiredFieldValidator ID="regTimeZone" runat="server" ControlToValidate="lstTimezone"
                                                Display="dynamic" CssClass="lblError" SetFocusOnError="true" Text="Required"
                                                ValidationGroup="Submit"></asp:RequiredFieldValidator>
                                        </td>
                                        <%--Code changed for FB 1425 QA Bug -End--%>
                                    </tr>
                                    <tr id="trLatitude" runat="server"> <%--FB 2694--%>
                                        <%--Window Dressing--%>
                                        <td style="width: 15%" align="left" valign="top" class="blackblodtext">
                                            Latitude
                                        </td>
                                        <%--Edited For FF and FB 2050 --%>
                                        <td style="width: 30%" align="left" valign="top">
                                            <%--Edited For FF--%>
                                            <asp:TextBox ID="txtLatitude" runat="server" CssClass="altText"></asp:TextBox>
                                        </td>
                                        <%--Window Dressing--%>
                                        <td style="width: 20%" align="left" valign="top" class="blackblodtext">
                                            Longitude
                                        </td>
                                        <%-- FB 2050 --%>
                                        <td style="width: 30%" align="left" valign="top">
                                            <%--Edited For FF--%>
                                            <asp:TextBox ID="txtLongitude" runat="server" CssClass="altText"></asp:TextBox>
                                        </td>
                                    </tr>
                                </table>
                                <%-- Location Parameters end here --%>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <%--Images--%>
                <td align="center">
                    <table id="tblImages" cellpadding="2" cellspacing="2" border="0" style="width: 90%">
                        <tr>
                            <td align="left" colspan="4">
                                <span class="subtitleblueblodtext" style="margin-left:-20px">Images</span>
                            </td>
                        </tr>
                        <tr style="display: none">
                            <%--Window Dressing--%>
                            <%--Window Dressing--%>
                            <td align="left" valign="top" class="blackblodtext" style="display: none">
                                Room Image
                                <br />
                                <i style="font-size: xx-small">Use CTRL key to select multiple images.</i>
                            </td>
                            <td align="left" valign="top" style="display: none">
                                <asp:ListBox ID="lstRoomImage" runat="server" CssClass="SelectFormat" SelectionMode="multiple">
                                </asp:ListBox>
                                <%--code added for Soft Edge button--%>
                                <input type="button" name="Edit" class="altShortBlueButtonFormat" onclick="javascript:fnOpen()"
                                    style="width: 30pt" value="Edit" />
                            </td>
                        </tr>
                        <tr>
                            <td align="left" colspan="4">
                                <%-- <asp:UpdatePanel ID="RoomImgUpdatePanel" runat="server"  UpdateMode="Always" RenderMode="Inline" >
                                    <Triggers>
                                        <asp:AsyncPostBackTrigger ControlID="BtnUploadRmImg"  />
                                    </Triggers>
                                    <ContentTemplate>
                                        <asp:Panel  id="RoomImagePanel" runat="server">--%>
                                <table align="left" cellpadding="2" cellspacing="0" border="0" style="width:95%"> <!-- FB 2050 -->
                                    <!-- FB 2050 -->
                                    <tr id="trDynamicRoomLayout" runat="server"> <%--FB 2694--%>
                                        <td align="left" style="width: 25%" valign="top" class="blackblodtext">
                                            <!-- FB 2050 -->
                                            Dynamic Room Layout
                                        </td>
                                        <td align="left" style="width: 75%" valign="top">
                                            <!-- FB 2050 -->
                                            <asp:DropDownList ID="lstDynamicRoomLayout" runat="server" CssClass="SelectFormat"
                                                Width="175px">
                                                <%-- FB 2050 --%>
                                                <asp:ListItem Text="enable" Value="1" Selected="True" />
                                                <asp:ListItem Text="disable" Value="0" />
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                    <tr id="trSecurityImage" runat="server"> <%--FB 2694--%>
                                        <%--FB 2050--%>
                                        <%--FB 2136 Start--%>
                                        <td align="left" class="blackblodtext" valign="top">
                                            Security Image
                                        </td>
                                        <td colspan="3">
                                            <%--FB 2050--%>
                                            <asp:DropDownList ID="drpSecImgList" runat="server" DataTextField="badgename" DataValueField="badgeid"
                                                CssClass="SelectFormat" Width="175px" EnableViewState="true" onchange="javascript:fnSecImgSelection();">
                                            </asp:DropDownList>
                                            <input id="btnMngSecImg" type="button" value="Manage" class="altMedium0BlueButtonFormat"
                                                onclick="javascript:return fnOpenSecurityBadge()" />
                                            <br />
                                            <br />
                                        </td>
                                        <%--FB 2136 End--%>
                                    </tr>
                                    <tr>
                                        <td align="left" style="width:25%" valign="top" class="blackblodtext"><%--FB 2694--%>
                                            Room Image
                                        </td>
                                        <td colspan="3" align="left" class="blackblodtext"> <%--FB 2909 Start--%>
                                        <div>
                                        <input type="text" class="file_input_textbox" readonly="readonly" value='No file selected'>
                                        <div class="file_input_div"><input type="button" value="Browse" class="file_input_button"  />  <%--accept="image/*" --%>
                                            <input type="file" class="file_input_hidden" id="roomfileimage" accept="image/*" contenteditable="false" enableviewstate="true" runat="server" onchange="getfilename(this)"/></div></div> <%--FB 3055-2ndIssue--%>
                                            <asp:RegularExpressionValidator ID="regRoomfileimage" runat="server" Display="Dynamic" ValidationGroup="Submit1" ControlToValidate="roomfileimage" CssClass="lblError" ErrorMessage="File type is invalid." ValidationExpression="^.*\.((j|J)(p|P)(e|E)?(g|G)|(g|G)(i|I)(f|F)|(p|P)(n|N)(g|G)|(b|B)(m|M)(p|P))$"></asp:RegularExpressionValidator>
                                           <%-- <input type="file" id="roomfileimage" contenteditable="false" enableviewstate="true"
                                                size="50" class="altText" runat="server" />--%>  <%--FB 2909 End--%>
                                            <asp:Button ID="BtnUploadRmImg" CssClass="altLongBlueButtonFormat" runat="server"  style="margin-left:2px"
                                                Text="Upload Room Image" OnClick="UploadRoomImage"  ValidationGroup="Submit1"  OnClientClick="DataLoading(1)"/> <%--FB 2909 End--%> <%--ZD 100176--%> 
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                        </td>
                                         <%-- FB 2136 Starts --%>
                                        <td align="left" colspan = "3"> 
                                           <div style="overflow-y: hidden; overflow-x: auto; height: auto; width: 600px;">
                                                <asp:DataGrid BorderColor="blue" BorderStyle="solid" BorderWidth="1" ID="dgItems"
                                                    AutoGenerateColumns="false" OnItemCreated="BindRowsDeleteMessage" OnDeleteCommand="RemoveImage"
                                                    runat="server" Width="70%" GridLines="None" Visible="false" Style="border-collapse: separate">
                                                    <HeaderStyle Height="30" CssClass="tableHeader" HorizontalAlign="Center" />
                                                    <AlternatingItemStyle CssClass="tableBody" />
                                                    <ItemStyle CssClass="tableBody" />
                                                    <FooterStyle CssClass="tableBody" />
                                                    <Columns>
                                                        <asp:BoundColumn DataField="ImageName" Visible="true" HeaderText="Name" HeaderStyle-CssClass="tableHeader"
                                                            ItemStyle-Width="20%"></asp:BoundColumn>
                                                        <asp:BoundColumn DataField="Image" Visible="false"></asp:BoundColumn>
                                                        <asp:BoundColumn DataField="Imagetype" Visible="false"></asp:BoundColumn>
                                                        <asp:BoundColumn DataField="ImagePath" Visible="false"></asp:BoundColumn>
                                                        <asp:TemplateColumn HeaderText="Image" HeaderStyle-CssClass="tableHeader" ItemStyle-Width="40%"
                                                            ItemStyle-HorizontalAlign="center">
                                                            <ItemTemplate>
                                                                <asp:Image ID="itemImage" ImageUrl='<%# DataBinder.Eval(Container, "DataItem.ImagePath") %>'
                                                                    Width="30" Height="30" runat="server" />
                                                            </ItemTemplate>
                                                        </asp:TemplateColumn>
                                                        <asp:TemplateColumn HeaderText="Actions" HeaderStyle-CssClass="tableHeader" ItemStyle-HorizontalAlign="Center">
                                                            <ItemTemplate>
                                                                <asp:LinkButton ID="btnDelete" Text="Remove" CommandName="Delete" runat="server"></asp:LinkButton>
                                                            </ItemTemplate>
                                                        </asp:TemplateColumn>
                                                    </Columns>
                                                </asp:DataGrid><br />
                                            </div>
                                            <%-- FB 2136 Ends --%>
                                        </td>
                                    </tr>
                                </table>
                                <%-- </asp:Panel>
                                     </ContentTemplate>
                                </asp:UpdatePanel>--%>
                            </td>
                        </tr>
                        <tr>
                            <td align="left" colspan="4">
                                <table align="left" id="tblImagesDetails" cellpadding="2" cellspacing="0" border="0"
                                    style="width: 95%"> <%--FB 2136--%>
                                    <%-- Images Parameters starts here & FB 2050 --%>
                                    <tr>
                                        <%--Window Dressing--%>
                                        <td style="width: 15%" align="left" valign="top" class="blackblodtext">
                                            Map 1
                                        </td>
                                        <td style="width: 85%" align="left" valign="top">
                                            <table width="100%">
                                                <tr>
                                                    <td>
                                                      <%--<input type="file" id="fleMap1" contenteditable="false" enableviewstate="true" size="50"
                                                            class="altText" runat="server" />--%> <%--FB 2909 Start--%>
                                                            <div>
                                                            <input type="text" class="file_input_textbox" readonly="readonly" value='No file selected'/>
                                                            <div class="file_input_div"><input type="button" value="Browse" class="file_input_button"  />
                                                            <input type="file"  class="file_input_hidden" id="fleMap1" accept="image/*" contenteditable="false" enableviewstate="true" runat="server" onchange="getfilename(this)"/></div></div> <%--FB 2909 End--%>  <%--FB 3055-2ndIssue--%>
                                                            <asp:RegularExpressionValidator ID="regfleMap1" runat="server" Display="Dynamic" ValidationGroup="Submit1" ControlToValidate="fleMap1" CssClass="lblError" ErrorMessage="File type is invalid." ValidationExpression="^.*\.((j|J)(p|P)(e|E)?(g|G)|(g|G)(i|I)(f|F)|(p|P)(n|N)(g|G)|(b|B)(m|M)(p|P))$"></asp:RegularExpressionValidator>
                                                        <cc1:ImageControl ID="Map1ImageCtrl" Width="30" Height="30" Visible="false" runat="server">
                                                        </cc1:ImageControl>
                                                        <asp:Label ID="lblUploadMap1" Text="" Visible="false" runat="server"></asp:Label>
                                                        <asp:Button ID="btnRemoveMap1" CssClass="altShortBlueButtonFormat" Text="Remove"
                                                            Visible="false" runat="server" OnCommand="RemoveFile" CommandArgument="1" />
                                                        <asp:Label ID="hdnUploadMap1" Text="" Visible="false" runat="server"></asp:Label>
                                                        <input type="hidden" id="Map1ImageDt" name="Map1ImageDt" runat="server" />
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <%--Window Dressing--%>
                                        <td style="width: 25%" align="left" valign="top" class="blackblodtext">
                                            Map 2
                                        </td>
                                        <td style="width: 75%" align="left" valign="top">
                                            <table width="100%">
                                                <tr>
                                                    <td>
                                                        <%--<input type="file" id="fleMap2" contenteditable="false" enableviewstate="true" size="50"
                                                            class="altText" runat="server" />--%> <%--FB 2909 Start--%>
                                                            <div>
                                                            <input type="text" class="file_input_textbox" readonly="readonly" value='No file selected'/>
                                                            <div class="file_input_div"><input type="button" value="Browse" class="file_input_button"  />
                                                            <input type="file"  class="file_input_hidden" id="fleMap2" accept="image/*" contenteditable="false" enableviewstate="true" runat="server" onchange="getfilename(this)"/></div></div> <%--FB 2909 End--%> <%--FB 3055-2ndIssue--%>
                                                            <asp:RegularExpressionValidator ID="regfleMap2" runat="server" Display="Dynamic" ValidationGroup="Submit1" ControlToValidate="fleMap2" CssClass="lblError" ErrorMessage="File type is invalid." ValidationExpression="^.*\.((j|J)(p|P)(e|E)?(g|G)|(g|G)(i|I)(f|F)|(p|P)(n|N)(g|G)|(b|B)(m|M)(p|P))$"></asp:RegularExpressionValidator>
                                                        <cc1:ImageControl ID="Map2ImageCtrl" Width="30" Height="30" Visible="false" runat="server">
                                                        </cc1:ImageControl>
                                                        <asp:Label ID="lblUploadMap2" Text="" Visible="false" runat="server"></asp:Label>
                                                        <asp:Button ID="btnRemoveMap2" CssClass="altShortBlueButtonFormat" Text="Remove"
                                                            Visible="false" runat="server" OnCommand="RemoveFile" CommandArgument="2" />
                                                        <asp:Label ID="hdnUploadMap2" Text="" Visible="false" runat="server"></asp:Label>
                                                        <input type="hidden" id="Map2ImageDt" name="Map2ImageDt" runat="server" />
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr style="display: none">
                                        <%--FB 2136--%>
                                        <%--Window Dressing--%>
                                        <td style="width: 25%" align="left" valign="top" class="blackblodtext">
                                            Security 1
                                        </td>
                                        <td style="width: 75%" align="left" valign="top">
                                            <table width="100%">
                                                <tr>
                                                    <td>
                                                        <input type="file" id="fleSecurity1" contenteditable="false" enableviewstate="true"
                                                            size="50" class="altText" runat="server" />
                                                        <cc1:ImageControl ID="Sec1ImageCtrl" Width="30" Height="30" Visible="false" runat="server">
                                                        </cc1:ImageControl>
                                                        <asp:Label ID="lblUploadSecurity1" Text="" Visible="false" runat="server"></asp:Label>
                                                        <asp:Button ID="btnRemoveSecurity1" CssClass="altShortBlueButtonFormat" Text="Remove"
                                                            Visible="false" runat="server" OnCommand="RemoveFile" CommandArgument="3" />
                                                        <asp:Label ID="hdnUploadSecurity1" Text="" Visible="false" runat="server"></asp:Label>
                                                        <input type="hidden" id="Sec1ImageDt" name="Sec1ImageDt" runat="server" />
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr style="display: none">
                                        <%--FB 2136--%>
                                        <%--Window Dressing--%>
                                        <td style="width: 25%" align="left" valign="top" class="blackblodtext">
                                            Security 2
                                        </td>
                                        <td style="width: 75%" align="left" valign="top">
                                            <table width="100%">
                                                <tr>
                                                    <td>
                                                        <input type="file" id="fleSecurity2" contenteditable="false" enableviewstate="true"
                                                            size="50" class="altText" runat="server" />
                                                        <cc1:ImageControl ID="Sec2ImageCtrl" Width="30" Height="30" Visible="false" runat="server">
                                                        </cc1:ImageControl>
                                                        <asp:Label ID="lblUploadSecurity2" Text="" Visible="false" runat="server"></asp:Label>
                                                        <asp:Button ID="btnRemoveSecurity2" CssClass="altShortBlueButtonFormat" Text="Remove"
                                                            Visible="false" runat="server" OnCommand="RemoveFile" CommandArgument="4" />
                                                        <asp:Label ID="hdnUploadSecurity2" Text="" Visible="false" runat="server"></asp:Label>
                                                        <input type="hidden" id="Sec2ImageDt" name="Sec2ImageDt" runat="server" />
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr id="trMisc1" runat="server"> <%--FB 2694--%>
                                        <%--Window Dressing--%>
                                        <td style="width: 25%" align="left" valign="top" class="blackblodtext">
                                            Misc 1
                                        </td>
                                        <td style="width: 75%" align="left" valign="top">
                                            <table width="100%">
                                                <tr>
                                                    <td>
                                                        <%--<input type="file" id="fleMisc1" contenteditable="false" enableviewstate="true" size="50"
                                                            class="altText" runat="server" />--%> <%--FB 2909 Start--%>
                                                            <div>
                                                            <input type="text" class="file_input_textbox" readonly="readonly" value='No file selected'/>
                                                            <div class="file_input_div"><input type="button" value="Browse" class="file_input_button"  />
                                                            <input type="file"  class="file_input_hidden" id="fleMisc1" accept="image/*" contenteditable="false" enableviewstate="true" runat="server" onchange="getfilename(this)"/></div></div><%--FB 2909 End--%> <%--FB 3055-2ndIssue--%>
                                                            <asp:RegularExpressionValidator ID="regfleMisc1" runat="server" Display="Dynamic" ValidationGroup="Submit1" ControlToValidate="fleMisc1" CssClass="lblError" ErrorMessage="File type is invalid." ValidationExpression="^.*\.((j|J)(p|P)(e|E)?(g|G)|(g|G)(i|I)(f|F)|(p|P)(n|N)(g|G)|(b|B)(m|M)(p|P))$"></asp:RegularExpressionValidator>
                                                        <cc1:ImageControl ID="Misc1ImageCtrl" Width="30" Height="30" Visible="false" runat="server">
                                                        </cc1:ImageControl>
                                                        <asp:Label ID="lblUploadMisc1" Text="" Visible="false" runat="server"></asp:Label>
                                                        <asp:Button ID="btnRemoveMisc1" CssClass="altShortBlueButtonFormat" Text="Remove"
                                                            Visible="false" runat="server" OnCommand="RemoveFile" CommandArgument="5" />
                                                        <asp:Label ID="hdnUploadMisc1" Text="" Visible="false" runat="server"></asp:Label>
                                                        <input type="hidden" id="Misc1ImageDt" name="Misc1ImageDt" runat="server" />
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr id="trMisc2" runat="server"> <%--FB 2694--%>
                                        <%--Window Dressing--%>
                                        <td style="width: 25%" align="left" valign="top" class="blackblodtext">
                                            Misc 2
                                        </td>
                                        <td style="width: 75%" align="left" valign="top">
                                            <table width="100%">
                                                <tr>
                                                    <td>
                                                        <%--<input type="file" id="fleMisc2" contenteditable="false" enableviewstate="true" size="50"
                                                            class="altText" runat="server" />--%><%--FB 2909 Start--%>
                                                            <div>
                                                            <input type="text" class="file_input_textbox" readonly="readonly" value='No file selected'>
                                                            <div class="file_input_div"><input type="button" value="Browse" class="file_input_button" />
                                                            <input type="file"  class="file_input_hidden" id="fleMisc2" accept="image/*" contenteditable="false" enableviewstate="true" runat="server" onchange="getfilename(this)"/></div></div><%--FB 2909 End--%> <%--FB 3055-2ndIssue--%>
                                                            <asp:RegularExpressionValidator ID="regfleMisc2" runat="server" Display="Dynamic" ValidationGroup="Submit1" ControlToValidate="fleMisc2" CssClass="lblError" ErrorMessage="File type is invalid." ValidationExpression="^.*\.((j|J)(p|P)(e|E)?(g|G)|(g|G)(i|I)(f|F)|(p|P)(n|N)(g|G)|(b|B)(m|M)(p|P))$"></asp:RegularExpressionValidator>
                                                        <cc1:ImageControl ID="Misc2ImageCtrl" Width="30" Height="30" Visible="false" runat="server">
                                                        </cc1:ImageControl>
                                                        <asp:Label ID="lblUploadMisc2" Text="" Visible="false" runat="server"></asp:Label>
                                                        <asp:Button ID="btnRemoveMisc2" CssClass="altShortBlueButtonFormat" Text="Remove"
                                                            Visible="false" runat="server" OnCommand="RemoveFile" CommandArgument="6" />
                                                        <asp:Label ID="hdnUploadMisc2" Text="" Visible="false" runat="server"></asp:Label>
                                                        <input type="hidden" id="Misc2ImageDt" name="Misc2ImageDt" runat="server" />
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                                <%-- Images Parameters ends here --%>
                            </td>
                        </tr>
                        <tr>
                            <td align="center" colspan="4">
                                <%--<asp:Button ID="btnUploadFiles" OnClick="UploadFiles" runat="server" Text="Upload Images" CssClass="altLongBlueButtonFormat" />--%><%-- FB 3055 />--%>
                                <asp:Button ID="btnUploadImages" OnClick="UploadOtherImages" runat="server" Text="Upload Images" ValidationGroup="Submit1" OnClientClick="DataLoading(1)"
                                    CssClass="altLongBlueButtonFormat" /><%--ZD 100176--%> 
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <%--Approvers--%>
                <td align="center">
                    <table id="tblApprovers" cellpadding="2" cellspacing="2" border="0" style="width: 90%">
                        <tr>
                            <td align="left">
                                <span class="subtitleblueblodtext" runat="server" id="spnConf" style="margin-left:-20px">Conference Room Approvers</span>
                            </td>
                        </tr>
                        <tr>
                            <td align="left">
                                <table id="Table1" cellpadding="2" cellspacing="2" border="0" style="width: 95%">
                                    <%-- Images Parameters starts here --%>
                                    <tr>
                                        <%--Window Dressing--%>
                                        <td style="width: 25%" align="left" valign="top" class="blackblodtext">
                                            Primary Approver
                                        </td>
                                        <td style="width: 75%" align="left" valign="top">
                                            <asp:TextBox ID="Approver0" CssClass="altText" runat="server" />
                                            <a href="javascript: getYourOwnEmailList(0);" onmouseover="window.status='';return true;">
                                                <img border="0" src="image/edit.gif" alt="edit" width="17" height="15" style="cursor:pointer;" title="myVRM Address Book" ></a> <%--FB 2798--%>
                                            <a href="javascript: deleteApprover(0);" onmouseover="window.status='';return true;">
                                                <img border="0" src="image/btn_delete.gif" alt="delete" width="16" height="16" style="cursor:pointer;" title="Delete"></a> <%--FB 2798--%>
                                        </td>
                                    </tr>
                                    <tr>
                                        <%--Window Dressing--%>
                                        <td style="width: 25%" align="left" valign="top" class="blackblodtext">
                                            Secondary Approver 1
                                        </td>
                                        <td style="width: 75%" align="left" valign="top">
                                            <asp:TextBox ID="Approver1" CssClass="altText" runat="server" />
                                            <!-- Code Modified by Offshore FB # 412 Start(Changed getYourOwnEmailList(0) and deleteApprover() to getYourOwnEmailList(1))  -->
                                            <a href="javascript: getYourOwnEmailList(1);" onmouseover="window.status='';return true;">
                                                <img border="0" src="image/edit.gif" alt="edit" width="17" height="15" style="cursor:pointer;" title="myVRM Address Book"/></a> <%--FB 2798--%>
                                            <a href="javascript: deleteApprover(1);" onmouseover="window.status='';return true;">
                                                <img border="0" src="image/btn_delete.gif" alt="delete" width="16" height="16"  style="cursor:pointer;" title="Delete"></a> <%--FB 2798--%>
                                            <!-- Code Modified by Offshore FB # 412 End  -->
                                        </td>
                                    </tr>
                                    <tr>
                                        <%--Window Dressing--%>
                                        <td style="width: 25%" align="left" valign="top" class="blackblodtext">
                                            Secondary Approver 2
                                        </td>
                                        <td style="width: 75%" align="left" valign="top">
                                            <asp:TextBox ID="Approver2" CssClass="altText" runat="server" />
                                            <!-- Code Modified by Offshore FB # 412 Start(Changed getYourOwnEmailList(0) to getYourOwnEmailList(2))  -->
                                            <a href="javascript: getYourOwnEmailList(2);" onmouseover="window.status='';return true;">
                                                <img border="0" src="image/edit.gif" alt="edit" width="17" height="15" style="cursor:pointer;" title="myVRM Address Book"></a> <%--FB 2798--%>
                                            <a href="javascript: deleteApprover(2);" onmouseover="window.status='';return true;">
                                                <img border="0" src="image/btn_delete.gif" alt="delete" width="16" height="16" style="cursor:pointer;" title="Delete"></a> <%--FB 2798--%>
                                            <!-- Code Modified by Offshore FB # 412 Start(Changed getYourOwnEmailList(0) to getYourOwnEmailList(2))  -->
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <%--Endpoint--%>
                <td align="center">
                    <table id="tblEndpoint" cellpadding="2" cellspacing="2" border="0" style="width: 90%">
                        <tr>
                            <td align="left">
                                <span class="subtitleblueblodtext" style="margin-left:-20px">Endpoint Assignment</span>
                            </td>
                        </tr>
                        <tr>
                            <td align="left">
                                <table id="Table2" cellpadding="2" cellspacing="2" border="0" style="width: 95%">
                                    <%-- Images Parameters starts here --%>
                                    <tr>
                                        <%--Window Dressing--%>
                                        <%--Endpoint Search--%>
                                        <td style="width: 25%" align="left" valign="top" class="blackblodtext">
                                            Endpoint
                                        </td>
                                        <td>
                                            <asp:TextBox runat="server" ID="txtEndpoint" CssClass="altText" Width="40%" ReadOnly="true"></asp:TextBox>
                                            <a href="javascript: deleteEndpoint();" onmouseover="window.status='';return true;">
                                                <img border="0" src="image/btn_delete.gif" alt="delete" width="16" height="16" style="cursor:pointer;" title="Delete"></a> <%--FB 2798--%>
                                            <asp:Button Text="Add Endpoint" class="altMedium0BlueButtonFormat" OnClientClick="javascript:OpenEndpointSearch('frmMainroom');"
                                                OnClick="BindEndpoint" runat="server" ID="addEndpoint" Width="180px" />
                                            <input name="opnEndpoint" type="button" id="opnEndpoint" onclick="javascript:OpenEndpointSearch('frmMainroom');"
                                                value="Add Endpoint" class="altShortBlueButtonFormat" style="display: none;" />
                                            <input name="addEndpoint" type="button" id="addEndpoint1" onclick="javascript:AddEndpoint();"
                                                style="display: none;" /><br />
                                            <asp:RequiredFieldValidator ID="regEndPoint" runat="server" ControlToValidate="lstEndpoint"
                                                Display="dynamic" CssClass="lblError" SetFocusOnError="true" Text="Required"
                                                ValidationGroup="Submit"></asp:RequiredFieldValidator>
                                        </td>
                                        <td style="width: 75%; display: none" align="left" valign="top">
                                            <%--Edited for FF--%>
                                            <asp:DropDownList ID="lstEndpoint" runat="server" CssClass="altSelectFormat" DataTextField="Name"
                                                DataValueField="ID" onChange="chgEndpoint();">
                                                <asp:ListItem Text="Please Select..." Value="-1"></asp:ListItem>
                                                <%--Code added for FB 1257--%>
                                            </asp:DropDownList>
                                            <asp:Button ID="btnEndpointDetails" CssClass="altShortBlueButtonFormat" Text="View Details"
                                                runat="server" OnClientClick="ViewEndpointDetails();return false;" />
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <%if (hdnMultipleDept.Value == "1")
              {
            %>
            <tr>
                <td align="center">
                    <table cellpadding="2" cellspacing="2" border="0" style="width: 90%">
                        <tr>
                            <td align="left">
                                <span class="subtitleblueblodtext" style="margin-left:-20px">Department</span>
                            </td>
                        </tr>
                        <tr align="left">
                            <td align="left">
                                <table id="Table3" cellpadding="2" cellspacing="2" border="0" style="width: 95%">
                                    <%-- Images Parameters starts here --%>
                                    <tr>
                                        <%--Window Dressing--%>
                                        <td width="25%" align="left" valign="top" class="blackblodtext">
                                            Room's Departments
                                        </td><%--FB 2579 End--%>
                                        <td style="width: 75%" align="left" valign="top">
                                            <asp:ListBox ID="DepartmentList" runat="server" CssClass="altText" DataTextField="Name"
                                                DataValueField="ID" SelectionMode="multiple"></asp:ListBox>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <%} %>
            <tr>
                <td style="height: 60px">
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td align="center">
                    <table id="tblButtons" cellpadding="2" cellspacing="2" border="0" style="width: 90%">
                        <tr>
                            <td align="center" style="width: 33%">
                                <asp:Button ID="btnReset" runat="server" Text="Reset" CssClass="altLongBlueButtonFormat" OnClientClick="javascript:DataLoading('1');"
                                    OnClick="ResetRoomProfile" /><%--ZD 100176--%> 
                            </td>
                            <td align="center" style="width: 33%">
                                <%--code added for Soft Edge button--%>
                                <input name="Go" type="button" class="altLongBlueButtonFormat" onclick="javascript:fnClose();"
                                    value=" Go Back " />
                            </td>
                            <td align="center" style="width: 33%">
                                <asp:Button ID="btnSubmitAddNew" runat="server" ValidationGroup="Submit" Text="Submit / New Room"
                                      Width="150pt" OnClick="SubmitAddNewRoomProfile" OnClientClick="javascript:return frmMainroom_Validator()" /><%-- FB 2796--%>
                            </td>
                            <td align="center" style="width: 33%">
                                <asp:Button ID="btnSubmit" ValidationGroup="Submit" runat="server" Text="Submit"
                                   Width="150pt" OnClick="SubmitRoomProfile" OnClientClick="javascript:return frmMainroom_Validator()" /><%-- FB 2796--%>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
        <%--table 1 ends here--%>
        <input type="hidden" id="Approver0ID" runat="server" />
        <input type="hidden" id="Approver1ID" runat="server" />
        <input type="hidden" id="Approver2ID" runat="server" />
        <input type="hidden" id="AssistantID" runat="server" />
        <input type="hidden" id="AssistantName" runat="server" />
    </div>
    </form>
</body>
</html>
<%--code added for Soft Edge button--%>
<%--FB 2694 Start--%>
<%--<script type="text/javascript">
if("<%=Session["isVIP"]%>" !=null)
    if("<%=Session["isVIP"]%>" == "0")//FB 1982
    {
        document.getElementById("trVIP").style.display = "none"; 
    }
</script>--%>
<%--FB 2694 End--%>
<script type="text/javascript" src="inc/softedge.js"></script>

<% 
    if (Request.QueryString["cal"] == "2")
    { 
%>
<!-- #INCLUDE FILE="inc/mainbottomNET.aspx" -->
<%} %>

<script>chgEndpoint();</script>

