<%--ZD 100147 start--%>
<%--/* Copyright (C) 2013 myVRM - All Rights Reserved	
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.	
*
* You should have received a copy of the myVRM license with	
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/--%><%--ZD 100147 End--%>
<%@ Page Language="C#" Inherits="ns_MyVRM.Template" %>
<%--FB 2779 Starts--%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<meta http-equiv="X-UA-Compatible" content="IE=8" />
<!-- #INCLUDE FILE="inc/maintopNET.aspx" -->
<%--FB 2779 Ends--%>


<script type="text/javascript" src="script/mousepos.js"></script>
<script type="text/javascript" src="script/managemcuorder.js"></script>

<script language="javascript">

	function ManageOrder ()
	{
		change_mcu_order_prompt('image/pen.gif', 'Manage Template Order', document.frmManagebridge.Bridges.value, "Templates");
	}
	function CreateNewConference(confid)
    {
//        window.location.href = "aspToAspNet.asp?tp=ConferenceSetup.aspx&t=t&confid=" + confid; //Login Management
          window.location.href = "ConferenceSetup.aspx&t=t&confid=" + confid;
    }
	function showTemplateDetails(tid)
	{
//		popwin = window.open("dispatcher/conferencedispatcher.asp?cmd=GetTemplate&f=td&tid=" + tid,'templatedetails','status=yes,width=750,height=400,scrollbars=yes,resizable=yes')
        popwin = window.open("TemplateDetails.aspx?nt=1&f=td&tid=" + tid,'templatedetails','status=yes,width=750,height=400,scrollbars=yes,resizable=yes') //Login Management
		if (popwin)
			popwin.focus();
		else
			alert(EN_132);
	}
	function frmsubmit(save, order)
	{
	    document.getElementById("__EVENTTARGET").value="ManageOrder";
	    document.frmManagebridge.submit();
	}
	//ZD 100176 start
	function DataLoading(val) {
	    if (val == "1")
	        document.getElementById("dataLoadingDIV").innerHTML = "<img border='0' src='image/wait1.gif'>";
	    else
	        document.getElementById("dataLoadingDIV").innerHTML = "";
	}
	//ZD 100176 End
</script>
<head runat="server">
 
    <title>My Templates</title>
    <script type="text/javascript" src="inc/functions.js"></script>

</head>
<body>
    <form id="frmManagebridge" runat="server" method="post" onsubmit="return true">
    <div>
      <input type="hidden" id="helpPage" value="73">

        <table width="100%">
            <tr>
                <td align="center">
                    <h3>
                        <asp:Label ID="lblHeader" runat="server" Text="Manage Templates"></asp:Label><!-- FB 2570 -->
                    </h3>
                </td>
            </tr>
            <tr>
                <td align="center">
                    <asp:Label ID="errLabel" runat="server" Text="" CssClass="lblError"></asp:Label>
                </td>
            </tr>
            <div id="dataLoadingDIV" align="center"></div> <%--ZD 100176--%>
            <tr>
                <td align="Left">
                    <table cellspacing="5">
                        <tr>
                            <td width="20" >&nbsp;</td>
                            <td>
                                <SPAN class=subtitleblueblodtext>Existing Templates</SPAN>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td align="center">
                    <asp:DataGrid ID="dgTemplates" runat="server" AutoGenerateColumns="False" CellPadding="4" GridLines="None" AllowSorting="true" OnSortCommand="SortTemplates"
                         BorderColor="blue" BorderStyle="solid" BorderWidth="1" ShowFooter="true" OnItemCreated="BindRowsDeleteMessage"
                        OnDeleteCommand="DeleteTemplate" OnEditCommand="EditTemplate" OnCancelCommand="CreateConference" Width="90%" Visible="true" style="border-collapse:separate"> <%--Edited for FF--%>
                        <SelectedItemStyle  CssClass="tableBody"/>
                         <AlternatingItemStyle CssClass="tableBody" />
                         <ItemStyle CssClass="tableBody"  />
                        <HeaderStyle CssClass="tableHeader" Height="30px" />
                        <EditItemStyle CssClass="tableBody" />
                         <%--Window Dressing--%>
                        <FooterStyle CssClass="tableBody" />
                        <Columns>
                            <asp:BoundColumn DataField="ID" Visible="false" ><HeaderStyle CssClass="tableHeader" /></asp:BoundColumn>
                            <asp:BoundColumn DataField="name" ItemStyle-CssClass="tableBody" HeaderStyle-HorizontalAlign="center" ItemStyle-HorizontalAlign="center" HeaderStyle-CssClass="tableHeader" HeaderText="Name" SortExpression="1"></asp:BoundColumn> <%-- FB 2050 --%> <%--FB 2922--%>
                            <asp:BoundColumn DataField="description" ItemStyle-CssClass="tableBody" HeaderStyle-HorizontalAlign="center" ItemStyle-HorizontalAlign="center" HeaderStyle-CssClass="tableHeader" HeaderText="Description"></asp:BoundColumn> <%-- FB 2050 --%>  <%--FB 2922--%>
                            <asp:BoundColumn DataField="administrator" ItemStyle-CssClass="tableBody" HeaderStyle-HorizontalAlign="center" ItemStyle-HorizontalAlign="center" HeaderStyle-CssClass="tableHeader" HeaderText="Owner" SortExpression="2"></asp:BoundColumn> <%-- FB 2050 --%>  <%--FB 2922--%>
                            <asp:BoundColumn DataField="public" ItemStyle-CssClass="tableBody"  HeaderStyle-CssClass="tableHeader" HeaderText="Private/Public" SortExpression="3" HeaderStyle-HorizontalAlign="center" ItemStyle-HorizontalAlign="center"></asp:BoundColumn>  <%--FB 2922--%>
                            <asp:TemplateColumn HeaderText="View Details" ItemStyle-CssClass="tableBody"  HeaderStyle-CssClass="tableHeader" HeaderStyle-HorizontalAlign="center" ItemStyle-HorizontalAlign="center"> <%--FB 2922--%>
                                <ItemTemplate>
                                    <asp:Button ID="btnViewDetails" Text="View" runat="server" CssClass="altShortBlueButtonFormat"/> 
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="Actions" ItemStyle-CssClass="tableBody"  ItemStyle-Width="25%" FooterStyle-HorizontalAlign="Right" HeaderStyle-HorizontalAlign="center">   <%--FB 2922--%>
                                <HeaderStyle CssClass="tableHeader" />
                                <ItemTemplate>
                                    <table width="100%">
                                        <tr>
                                            <td>
                                                <%--Code Changed for FB 1428--%>
                                                <asp:LinkButton runat="server" ID="btnCreateConf" CommandName="Cancel">
                                                    <span id="Field1">Create Conference</span>
                                                </asp:LinkButton>
                                            </td>
                                            <td>
                                                <asp:LinkButton runat="server" Text="Edit" ID="btnEdit" CommandName="Edit"></asp:LinkButton>
                                            </td>
                                            <td>
                                                <asp:LinkButton runat="server" Text="Delete" ID="btnDelete" CommandName="Delete"></asp:LinkButton>
                                            </td>
                                        </tr>
                                    </table>
                                </ItemTemplate>
                                <FooterTemplate>
                                    <span class="blackblodtext"> Total Templates: </span><asp:Label ID="lblTotalRecords" runat="server" Text=""></asp:Label> <%-- FB 2579 --%>
                                </FooterTemplate>
                            </asp:TemplateColumn>
                        </Columns>
                    </asp:DataGrid>
                    <asp:Table runat="server" ID="tblNoTemplates" Visible="false" Width="90%">
                        <asp:TableRow CssClass="lblError">
                            <asp:TableCell CssClass="lblError">
                                No Templates found.
                            </asp:TableCell>
                        </asp:TableRow>
                    </asp:Table>                    
                </td>
            </tr>
            <tr>
                <td align="center">
                    <table width="90%">
                        <tr>
                            <td align="center">
                                <asp:Button ID="btnManageOrder" runat="server" OnClientClick="javascript:ManageOrder();return false;" Text="Manage Template Order" CssClass="altLongBlueButtonFormat" />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td align="Left">
                    <table cellspacing="5">
                        <tr>
                            <td width="20" >&nbsp;</td>
                            <td>
                                <SPAN class=subtitleblueblodtext>Search Templates</SPAN>
                            </td>
                        </tr>
                        <tr>
                            <td></td>
                           <%--Removed Class for Window Dressing                      --%>
                            <td class="blackblodtext">
                                Enter search criteria to search all private and public templates.
                                <br />Although you can only edit your private templates, you can use a public or private template as a basis to create a new template.    
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td align="center">
                    <table width="90%"> 
                        <tr>
                            <td align="center">
                                <table width="100%">
                                    <tr>
                                        <td align="left" class="blackblodtext">Template Name</td>
                                        <td align="left">
                                            <asp:TextBox ID="txtSTemplateName" runat="server" CssClass="altText"></asp:TextBox>
                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator4" ControlToValidate="txtSTemplateName" Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<br>& < > ' + % \ / ( ) ; ? | ^ = ! ` , [ ] { } : # $ @ ~ and &#34; are invalid characters." ValidationExpression="^(a-z|A-Z|0-9)*[^\\/<>+^;?|!`,\[\]{}\x22;=:@#$%&()'~]*$"></asp:RegularExpressionValidator>
                                        </td>
                                        <%if(!(Application["Client"].ToString().ToUpper().Equals(ns_MyVRMNet.vrmClient.MOJ))){%> <%--Added for FB 1425 MOJ--%>
                                        <td align="left" class="blackblodtext">Included Participants</td>
                                        <td align="left">
                                            <asp:TextBox ID="txtSParticipant" runat="server" CssClass="altText"></asp:TextBox>
                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator1" ControlToValidate="txtSParticipant" Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<br>& < > + % \ /  ? | ^ = ! ` [ ] { } # $ @ and ~ are invalid characters." ValidationExpression="^(a-z|A-Z|0-9)*[^\\/<>+^?|!`\[\]{}\=@#$%&~]*$"></asp:RegularExpressionValidator> <%--FB 1888--%>
                                        </td> 
                                        <%}%> <%--Added for FB 1425 MOJ--%>                                      
                                    </tr>
                                    <tr>
                                        <td align="left" class="blackblodtext">Description</td>
                                        <td align="left">
                                            <asp:TextBox ID="txtSDescription" TextMode="multiline" Rows="2" Width="200" runat="server" CssClass="altText"></asp:TextBox>
                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator2" ControlToValidate="txtSDescription" Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<br>& < > ' + % \ / ( ) ; ? | ^ = ! ` , [ ] { } : # $ @ ~ and &#34; are invalid characters." ValidationExpression="^(a-z|A-Z|0-9)*[^\\/<>+^;?|!`,\[\]{}\x22;=:@#$%&()'~]*$"></asp:RegularExpressionValidator>
                                        </td>
                                        <td align="left">&nbsp;</td>
                                        <td align="left">&nbsp;</td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td align="right">
                                <asp:Button ID="btnSearch" OnClick="SearchTemplate" runat="server" CssClass="altLongBlueButtonFormat" Text="Submit"  OnClientClick="DataLoading(1)"/> <%--ZD 100176--%>
                            </td>                            
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td align="Left">
                    <table cellspacing="5">
                        <tr>
                            <td width="20" >&nbsp;</td>
                            <td>
                                <!--<SPAN class=subtitleblueblodtext></SPAN>--><%--Commented for FB 2094--%>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
               <td align="center">
                    <table width="90%">
                        <tr>
                            <td align="right">
                               <asp:Button ID="btnCreate" OnClick="CreateNewTemplate" runat="server" CssClass="altLongBlueButtonFormat" Text="Create New Template" OnClientClick="DataLoading(1)" /><%--FB 2094--%><%-- ZD 100176--%>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </div>
    <asp:TextBox ID="Bridges" runat="server" Width="0" Height="0" ForeColor="transparent" BackColor="transparent" BorderColor="transparent"></asp:TextBox>
    <asp:TextBox ID="txtSortBy" runat="server" Visible="false"></asp:TextBox>
</form>
<%--code added for Soft Edge button--%>
<script type="text/javascript" src="inc/softedge.js"></script>
    <!-- #INCLUDE FILE="inc/mainbottomNET.aspx" -->
</body>
</html>

