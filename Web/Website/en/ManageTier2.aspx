<%--ZD 100147 start--%>
<%--/* Copyright (C) 2013 myVRM - All Rights Reserved	
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.	
*
* You should have received a copy of the myVRM license with	
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/--%><%--ZD 100147 End--%>
<%@ Page Language="C#" Inherits="ns_Tiers2.Tiers2" Buffer="true" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> <!-- FB 2050 -->
<meta http-equiv="X-UA-Compatible" content="IE=8" /> <!-- FB 2050 --> <%--FB 2779--%>

<!--window Dressing-->
<!-- #INCLUDE FILE="inc/maintopNET.aspx" --> 

<script runat="server">

</script>
<script language="javascript">
    //ZD 100176 start
    function DataLoading(val) {
        if (val == "1")
            document.getElementById("dataLoadingDIV").innerHTML = "<img border='0' src='image/wait1.gif'>";
        else
            document.getElementById("dataLoadingDIV").innerHTML = "";
    }
    //ZD 100176 End
</script>
<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Manage Tiers</title>
    <script type="text/javascript" src="inc/functions.js"></script>

</head>
<body>
    <form id="frmTierManagement" runat="server" method="post" onsubmit="return true">
    <div>
      <input type="hidden" id="helpPage" value="65">

        <table width="100%">
            <tr>
                <td align="center">
                    <h3>
                        <asp:Label ID="lblTier1Name" runat="server" Text="" ></asp:Label>
                        <asp:TextBox ID="txtTier1ID" Visible="false" Text="" runat="server"></asp:TextBox>
                    </h3>
                </td>
            </tr>
            <tr>
                <td align="center">
                    <asp:Label ID="errLabel" runat="server" Text="" CssClass="lblError"></asp:Label>
                </td>
            </tr>
            <div id="dataLoadingDIV" style="z-index: 1" align="center"> <%--ZD 100176--%>
            <tr>
                <td align="Left">
                    <table cellspacing="5">
                        <tr>
                            <td>&nbsp;</td>
                            <td>
                                <SPAN class=subtitleblueblodtext>Existing Tiers</SPAN>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td align="center">
                    <asp:DataGrid ID="dgTier2s" runat="server" AutoGenerateColumns="False" CellPadding="4" GridLines="None"
                         BorderColor="blue" BorderStyle="solid" BorderWidth="1" ShowFooter="true" OnUpdateCommand="UpdateTier2" OnCancelCommand="CancelTier2" OnItemCreated="BindRowsDeleteMessage2"
                        OnDeleteCommand="DeleteTier2" OnEditCommand="EditTier2" Width="90%" Visible="true">
                         <%--Window Dressing - Start--%>
                        <SelectedItemStyle CssClass="tableBody" />
                        <EditItemStyle CssClass="tableBody" />
                        <AlternatingItemStyle CssClass="tableBody" />
                        <ItemStyle CssClass="tableBody" />
                        <FooterStyle CssClass="tableBody"  />
                        <%--Window Dressing - End--%>
                        <HeaderStyle CssClass="tableHeader" />
                        <Columns>
                            <asp:BoundColumn DataField="ID" Visible="false" ><HeaderStyle CssClass="tableHeader" /></asp:BoundColumn>
                            <asp:TemplateColumn HeaderText="Name" ItemStyle-CssClass="tableBody" HeaderStyle-CssClass="tableHeader" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left" > <%-- FB 2050 --%>
                                <ItemTemplate>
                                    <asp:Label ID="lblTier2Name" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Name") %>'></asp:Label>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:TextBox ID="txtTier2Name" CssClass="altText" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Name")%>'></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="reqTier2Name1" ControlToValidate="txtTier2Name" ValidationGroup="Update" runat="server" ErrorMessage="Required" Display="dynamic"></asp:RequiredFieldValidator>
                                    <asp:RegularExpressionValidator ID="regName" ValidationGroup="Update" ControlToValidate="txtTier2Name" runat="server" Display="dynamic" SetFocusOnError="true" ErrorMessage="<br>& < > + % \ / ( ) ? | ^ = ! `[ ] { } # $ and ~ are invalid characters." ValidationExpression="^(a-z|A-Z|0-9)*[^\\/<>+?|!`\[\]{}\=^@#$%&()~]*$"></asp:RegularExpressionValidator> <%-- fogbugz case 137 for extra junk character validation--%> <%--FB 1888--%> 
                                </EditItemTemplate>
                             </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="Actions" ItemStyle-Width="15%">
                                <HeaderStyle CssClass="tableHeader" />
                                <ItemTemplate>
                                    <asp:LinkButton runat="server" Text="Edit" ID="btnEdit" CommandName="Edit" OnClientClick="DataLoading(1)"></asp:LinkButton> <%--ZD 100176--%> 
                                    &nbsp;<asp:LinkButton runat="server" Text="Delete" ID="btnDelete" CommandName="Delete" OnClientClick="DataLoading(1)"></asp:LinkButton><%--ZD 100176--%> 
                                    <asp:LinkButton runat="server" Text="Update" ID="btnUpdate" ValidationGroup="Update" CommandName="Update" Visible="false" OnClientClick="DataLoading(1)"></asp:LinkButton><%--ZD 100176--%> 
                                    &nbsp;<asp:LinkButton runat="server" Text="Cancel" ID="btnCancel" CommandName="Cancel" Visible="false" OnClientClick="DataLoading(1)"></asp:LinkButton><%--ZD 100176--%> 
                                </ItemTemplate>
                                <FooterTemplate>
                                <%--Window Dressing--%>
                                    <span class="blackblodtext"> Total Tiers:</span> <asp:Label ID="lblTotalRecords" runat="server" Text=""></asp:Label><%--FB 2579--%>
                                </FooterTemplate>
                            </asp:TemplateColumn>
                        </Columns>
                    </asp:DataGrid>
                    <asp:Table runat="server" ID="tblNoTier2s" Visible="false" Width="90%">
                        <asp:TableRow CssClass="lblError">
                            <asp:TableCell CssClass="lblError">
                                No Middle Tiers found.
                            </asp:TableCell>
                        </asp:TableRow>
                    </asp:Table>     
                </td>
            </tr>
            <tr>
                <td align="Left">
                    <table cellspacing="5">
                        <tr>
                            <td>&nbsp;</td>
                            <td>
                               <%-- <SPAN class=subtitleblueblodtext>Create New Tier2</SPAN>--%><%--Commented for FB 2094 --%>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr id="trNew" runat="server"> <%--FB 2670--%>
                <%--Window Dressing --%>            
                    <td align="center" class="blackblodtext">Create New Tier2<span class="reqfldstarText">*</span><%--FB 2094--%>
                   <asp:TextBox ID="txtNewTier2Name" runat="server" Text="" CssClass="altText"></asp:TextBox>
                   <asp:Button runat="server" ID="btnCreateNewTier2" ValidationGroup="Create2" Text="Submit" CssClass="altMedium0BlueButtonFormat" OnClick="CreateNewTier2" CommandName="Update" />
                   <asp:RequiredFieldValidator ID="reqTier2Name2" ValidationGroup="Create2" ControlToValidate="txtNewTier2Name" runat="server" ErrorMessage="Required" Display="dynamic"></asp:RequiredFieldValidator>
                   <%-- fogbugz case 137 for extra junk character validation--%>
                    <asp:RegularExpressionValidator ID="regName1" ValidationGroup="Create2" ControlToValidate="txtNewTier2Name" runat="server" SetFocusOnError="true" ErrorMessage="<br>& < > + % \ / ( ) ? | ^ = ! `[ ] { } # $ and ~ are invalid characters." ValidationExpression="^(a-z|A-Z|0-9)*[^\\/<>+?|!`\[\]{}\=^@#$%&()~]*$"></asp:RegularExpressionValidator> <%--FB 1888--%> 
                </td>
            </tr>
            <tr>
                <td align="Left">
                    <table cellspacing="5">
                        <tr>
                            <td>&nbsp;</td>
                            <td>
                                <SPAN class=subtitleblueblodtext>Manage Top Tiers</SPAN>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td align="center">
                   <asp:Button runat="server" ID="btnGoBack" Text="Go Back" CssClass="altLongBlueButtonFormat" OnClick="GoBack" OnClientClick="DataLoading(1)" /><%--ZD 100176--%> 
                </td>
            </tr>
        </table>
    </div>

<img src="keepalive.asp" name="myPic" width="1px" height="1px">
    </form>
<%--code added for Soft Edge button--%>
<script type="text/javascript" src="inc/softedge.js"></script>
    <!-- #INCLUDE FILE="inc/mainbottomNET.aspx" -->
</body>
</html>

