<%--ZD 100147 start--%>
<%--/* Copyright (C) 2013 myVRM - All Rights Reserved	
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.	
*
* You should have received a copy of the myVRM license with	
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/--%><%--ZD 100147 End--%>
<%@ Page Language="C#" Inherits="ns_MyVRM.Users" Buffer="true" %>
<%--FB 2779 Starts--%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<meta http-equiv="X-UA-Compatible" content="IE=8" />
<!-- #INCLUDE FILE="inc/maintopNET.aspx" -->
<%--FB 2779 Ends--%>

 
<script type="text/javascript" src="script/mousepos.js"></script>
<script type="text/javascript" src="script/managemcuorder.js"></script>

<script language="javascript">

	function ManageOrder ()
	{
		change_mcu_order_prompt('image/pen.gif', 'Manage MCU Order', document.frmManagebridge.Bridges.value);
		
//		frmsubmit('SAVE', '');
	}
	
	function frmsubmit()
	{
	    document.getElementById("__EVENTTARGET").value="ManageOrder";
	    document.frmManagebridge.submit();
	}
	//Added for FB 1405 -Start 
	
	function fnSearch()
	{
	    DataLoading(1);//ZD 100176
	    var obj = document.getElementById("hdnAction");
	    obj.value = 'Y';
	    return true;
	    

	}
	//ZD 100176 start
	function DataLoading(val) {
	    if (val == "1")
	        document.getElementById("dataLoadingDIV").innerHTML = "<img border='0' src='image/wait1.gif'>";
	    else
	        document.getElementById("dataLoadingDIV").innerHTML = "";
	}
	//ZD 100176 End
	
	//Added for FB 1405 -End 
</script>
<head runat="server">
    <title>Manage Users</title>
    <script type="text/javascript" src="inc/functions.js"></script>

</head>
<body>
    <form id="frmManagebridge" runat="server" method="post" onsubmit="return true">
    <div>
      <input type="hidden" id="helpPage" value="65">
        <input type="hidden" id="hdnAction" runat="server"> <%--Added for FB 1405--%>
        <table width="100%">
            <tr>
                <td align="center">
                    <h3>
                        <asp:Label ID="lblHeader" runat="server"></asp:Label>
                    </h3>
                </td>
            </tr>
            <tr>
                <td align="center">
                    <asp:Label ID="errLabel" runat="server" Text="" CssClass="lblError"></asp:Label>
                    <div id="dataLoadingDIV" style="z-index: 1" align="center"> <%--ZD 100176--%>
                </td>
            </tr>
            <tr>
                <td align="Left">
                    <table width="100%">
                        <tr>
                            <td >&nbsp;</td>
                            <td>
                                <SPAN class="subtitleblueblodtext" style="margin-left:-15px">Existing Users</SPAN>
                            </td>
                        </tr>
                        <tr>
                            <td width="20"></td>
                            <td align="center">
                                <table width="90%">
                                    <tr>
                                        <td align="right">
                                            <asp:DropDownList ID="lstSortBy" CssClass="alt2SelectFormat" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ChangeAlphabets">
                                                <asp:ListItem Value="1" Text="First Name"></asp:ListItem>
                                                <asp:ListItem Value="2" Text="Last Name"></asp:ListItem>
                                                <asp:ListItem Value="3" Text="Login"></asp:ListItem>
                                                <asp:ListItem Value="4" Text="Email"></asp:ListItem>
                                            </asp:DropDownList> 
                                            <%--Window Dressing--%>
                                            <span class="blackblodtext"> Starts with:</span>
                                        </td>
                                        <td align="left">
                                             <asp:Table runat="server"  ID="tblAlphabet" CellPadding="2" CellSpacing="5"></asp:Table>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td align="center">
                    <asp:DataGrid ID="dgUsers" runat="server" AutoGenerateColumns="False" CellPadding="4" GridLines="None"
                         BorderColor="blue" BorderStyle="solid" BorderWidth="1" ShowFooter="false" OnItemDataBound="BindRowsDeleteMessage"
                        OnDeleteCommand="DeleteUser" OnEditCommand="EditUser" OnCancelCommand="LockUser" OnUpdateCommand="RestoreUser" Width="90%" Visible="true" style="border-collapse:separate"> <%--Edited for FF--%>
                        <%--Window Dressing - Start--%>                        
                        <SelectedItemStyle CssClass="tableBody" />
                        <EditItemStyle CssClass="tableBody" />
                        <AlternatingItemStyle CssClass="tableBody" />
                        <ItemStyle CssClass="tableBody" />
                         <%--Window Dressing - End--%> 
                        <HeaderStyle CssClass="tableHeader" HorizontalAlign="left" />
                        <Columns>
                            <asp:BoundColumn DataField="userID" Visible="false" ><HeaderStyle CssClass="tableHeader" HorizontalAlign="left" /></asp:BoundColumn>
                            <asp:BoundColumn DataField="level" Visible="false" ><HeaderStyle CssClass="tableHeader" HorizontalAlign="left" /></asp:BoundColumn>
                            <asp:BoundColumn DataField="deleted" Visible="false" ><HeaderStyle CssClass="tableHeader" HorizontalAlign="left" /></asp:BoundColumn>
                            <asp:BoundColumn DataField="locked" Visible="false" ><HeaderStyle CssClass="tableHeader" HorizontalAlign="left" /></asp:BoundColumn>
                            <asp:BoundColumn DataField="firstName" ItemStyle-CssClass="tableBody"  HeaderStyle-CssClass="tableHeader" ItemStyle-HorizontalAlign="Left" HeaderText="First Name"></asp:BoundColumn>
                            <asp:BoundColumn DataField="lastName" ItemStyle-CssClass="tableBody"  HeaderStyle-CssClass="tableHeader" ItemStyle-HorizontalAlign="Left" HeaderText="Last Name"></asp:BoundColumn>
                            <asp:BoundColumn DataField="login"  ItemStyle-CssClass="tableBody" HeaderStyle-CssClass="tableHeader" ItemStyle-HorizontalAlign="Left" HeaderText="Login"></asp:BoundColumn>
                            <asp:BoundColumn DataField="email" ItemStyle-CssClass="tableBody" HeaderStyle-CssClass="tableHeader" ItemStyle-HorizontalAlign="Left" HeaderText="Email"></asp:BoundColumn>
                            <asp:TemplateColumn HeaderText="Role" ItemStyle-CssClass="tableBody" HeaderStyle-CssClass="tableHeader" ItemStyle-HorizontalAlign="Left">
                                <ItemTemplate>
                                    <asp:Image ID="imgRole2" Width="25" Height="25" ImageUrl="image/superadmin.gif" Visible='<%# (DataBinder.Eval(Container, "DataItem.crossaccess").ToString().Trim().Equals("1") && DataBinder.Eval(Container, "DataItem.level").ToString().Trim().Equals("2"))%>' runat="server" />
                                    <asp:Image ID="imgRole1" Width="25" Height="25" ImageUrl="image/admin.gif" Visible='<%# (DataBinder.Eval(Container, "DataItem.crossaccess").ToString().Trim().Equals("0") && DataBinder.Eval(Container, "DataItem.level").ToString().Trim().Equals("2"))%>' runat="server" />
                                    <asp:Image ID="Image2" Width="25" Height="25" ImageUrl="image/admin.gif" Visible='<%# (DataBinder.Eval(Container, "DataItem.crossaccess").ToString().Trim().Equals("0") && DataBinder.Eval(Container, "DataItem.level").ToString().Trim().Equals("1"))%>' runat="server" />
                                    <asp:Image ID="Image1" Width="25" Height="25" ImageUrl="image/VNOCadmin.png" Visible='<%# (DataBinder.Eval(Container, "DataItem.level").ToString().Trim().Equals("3"))%>' runat="server" /> <%--FB 2670--%><%--FB 2816--%>
                                    <asp:Image ID="imgRole0" Width="25" Height="25" ImageUrl="image/user.gif" Visible='<%# (DataBinder.Eval(Container, "DataItem.level").ToString().Trim().Equals("0") && DataBinder.Eval(Container, "DataItem.crossaccess").ToString().Trim().Equals("0"))%>' runat="server" />
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="Actions" ItemStyle-CssClass="tableBody" ItemStyle-Width="15%" ItemStyle-HorizontalAlign="Left">
                                <HeaderStyle CssClass="tableHeader" HorizontalAlign="left" />
                                <ItemTemplate>
                                <%--ZD 100263 Start--%>
                                <% if (Session["roomCascadingControl"].ToString().Equals("1"))
                                  {%>
                                    <asp:LinkButton runat="server" Text="Edit" Visible='<%# Session["admin"].ToString().Trim().Equals("2") && txtType.Text.Equals("1") && !DataBinder.Eval(Container, "DataItem.userID").ToString().Trim().Equals("11") && !DataBinder.Eval(Container, "DataItem.userID").ToString().Trim().Equals(Session["userID"].ToString()) && (Session["UsrCrossAccess"].ToString().Trim().Equals("1") || !DataBinder.Eval(Container, "DataItem.crossaccess").ToString().Trim().Equals("1") || DataBinder.Eval(Container, "DataItem.level").ToString().Trim().Equals("3")) %>' ID="btnEdit1" CommandName="Edit" OnClientClick="DataLoading(1)"></asp:LinkButton> <%--FB 2670--%><%--ZD 100176--%> 
                                    <asp:LinkButton runat="server" Text="Delete" Visible='<%# (Session["admin"].ToString().Trim().Equals("2")) && (!txtType.Text.Equals("3")) && !DataBinder.Eval(Container, "DataItem.userID").ToString().Trim().Equals("11") && !DataBinder.Eval(Container, "DataItem.userID").ToString().Trim().Equals(Session["userID"].ToString()) && (Session["UsrCrossAccess"].ToString().Trim().Equals("1") || !DataBinder.Eval(Container, "DataItem.crossaccess").ToString().Trim().Equals("1") || DataBinder.Eval(Container, "DataItem.level").ToString().Trim().Equals("3")) %>' ID="btnDelete1" CommandName="Delete" ></asp:LinkButton> <%--FB 2670--%><%--ZD 100176--%> 
                                    <asp:LinkButton runat="server" Text="Block" Visible='<%# (Session["admin"].ToString().Trim().Equals("2") || Session["admin"].ToString().Trim().Equals("1"))  && txtType.Text.Equals("1") && !DataBinder.Eval(Container, "DataItem.userID").ToString().Trim().Equals("11") && !DataBinder.Eval(Container, "DataItem.userID").ToString().Trim().Equals(Session["userID"].ToString()) && (Session["UsrCrossAccess"].ToString().Trim().Equals("1") || !DataBinder.Eval(Container, "DataItem.crossaccess").ToString().Trim().Equals("1") || DataBinder.Eval(Container, "DataItem.level").ToString().Trim().Equals("3"))%>' ID="btnLock1" CommandName="Cancel" ></asp:LinkButton><%--FB 2670--%><%--ZD 100176--%> 
                                    <asp:LinkButton runat="server" Text="Delete" Visible='<%# (txtType.Text.Equals("3") && (Application["ssoMode"].ToString().ToUpper().Equals("NO"))) %>' CommandName="Update" CommandArgument="1" ID="btnDeleteInactive1" ></asp:LinkButton>
                                    <asp:LinkButton runat="server" Text="Restore" Visible='<%# txtType.Text.Equals("3") %>' CommandName="Update" CommandArgument="2" ID="btnReset1"></asp:LinkButton>
                                 <%}
                                 else
                                 {%>
                                 <asp:LinkButton runat="server" Text="Edit" Visible='<%# Session["admin"].ToString().Trim().Equals("2") && txtType.Text.Equals("1") %>' Enabled='<%# !DataBinder.Eval(Container, "DataItem.userID").ToString().Trim().Equals("11") && !DataBinder.Eval(Container, "DataItem.userID").ToString().Trim().Equals(Session["userID"].ToString()) && (Session["UsrCrossAccess"].ToString().Trim().Equals("1") || !DataBinder.Eval(Container, "DataItem.crossaccess").ToString().Trim().Equals("1") || DataBinder.Eval(Container, "DataItem.level").ToString().Trim().Equals("3")) %>' ID="btnEdit0" CommandName="Edit" OnClientClick="DataLoading(1)"></asp:LinkButton> <%--FB 2670--%><%--ZD 100176--%> 
                                    <asp:LinkButton runat="server" Text="Delete" Visible='<%# (Session["admin"].ToString().Trim().Equals("2")) && (!txtType.Text.Equals("3")) %>' Enabled='<%# !DataBinder.Eval(Container, "DataItem.userID").ToString().Trim().Equals("11") && !DataBinder.Eval(Container, "DataItem.userID").ToString().Trim().Equals(Session["userID"].ToString()) && (Session["UsrCrossAccess"].ToString().Trim().Equals("1") || !DataBinder.Eval(Container, "DataItem.crossaccess").ToString().Trim().Equals("1") || DataBinder.Eval(Container, "DataItem.level").ToString().Trim().Equals("3")) %>' ID="btnDelete0" CommandName="Delete" ></asp:LinkButton> <%--FB 2670--%><%--ZD 100176--%> 
                                    <asp:LinkButton runat="server" Text="Block" Visible='<%# (Session["admin"].ToString().Trim().Equals("2") || Session["admin"].ToString().Trim().Equals("1"))  && txtType.Text.Equals("1") %>' Enabled='<%# !DataBinder.Eval(Container, "DataItem.userID").ToString().Trim().Equals("11") && !DataBinder.Eval(Container, "DataItem.userID").ToString().Trim().Equals(Session["userID"].ToString()) && (Session["UsrCrossAccess"].ToString().Trim().Equals("1") || !DataBinder.Eval(Container, "DataItem.crossaccess").ToString().Trim().Equals("1") || DataBinder.Eval(Container, "DataItem.level").ToString().Trim().Equals("3"))%>' ID="btnLock0" CommandName="Cancel" ></asp:LinkButton><%--FB 2670--%><%--ZD 100176--%> 
                                    <asp:LinkButton runat="server" Text="Delete" Visible='<%# (txtType.Text.Equals("3") && (Application["ssoMode"].ToString().ToUpper().Equals("NO"))) %>' CommandName="Update" CommandArgument="1" ID="btnDeleteInactive0" ></asp:LinkButton>
<%--                                <asp:LinkButton runat="server" Text="Delete" Visible='<%# (txtType.Text.Equals("2")) %>' CommandName="Update" CommandArgument="9" ID="btnDeleteGuest"></asp:LinkButton>
--%>                                <asp:LinkButton runat="server" Text="Restore" Visible='<%# txtType.Text.Equals("3") %>' CommandName="Update" CommandArgument="2" ID="btnReset0"></asp:LinkButton>
                                    <%}%>
                                 <%--ZD 100263 End--%>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:BoundColumn DataField="crossaccess" Visible="false" ><HeaderStyle CssClass="tableHeader" HorizontalAlign="left" /></asp:BoundColumn>
                        </Columns>
                    </asp:DataGrid>
                    <asp:Table runat="server" ID="tblNoUsers" Visible="false" Width="90%">
                        <asp:TableRow CssClass="lblError">
                            <%--Windows Dressing--%>
                            <asp:TableCell CssClass="lblError" HorizontalAlign="center">
                                No User(s) found.  <%--Edited for FB 1405--%>
                            </asp:TableCell>
                        </asp:TableRow>
                    </asp:Table>                    
                </td>
            </tr>
            <tr>
                <td align="center">
                    <table width="90%" cellspacing="0" cellpadding="0" border="0">
                        <tr>
                             <%--FB 2919--%>
                            <%--Windows Dressing--%>
                            <td width="40%" valign="middle" align="left" id="tdLegends" runat="server" class="blackblodtext"><%--FB 2579--%>
                                Legend:
                                <font size="1" color="blue">
                                    <asp:Image ID="imgRole2" Width="25" Height="25" ImageUrl="image/superadmin.gif" runat="server" /> Site Administrator
                                    <asp:Image ID="imgRole1" Width="25" Height="25" ImageUrl="image/admin.gif" runat="server" /> Administrator 
                                    <asp:Image ID="imgRole3" Width="25" Height="25" ImageUrl="image/VNOCadmin.png" runat="server" /> VNOC Operator <%--FB 2816--%><%--FB 2972--%>
                                    <asp:Image ID="imgRole0" Width="25" Height="25" ImageUrl="image/user.gif" runat="server" /> User
                                    
                                </font>
                            </td>
                            <td align="right" width="15%">
                                <asp:Button ID="btnDeleteAllGuest" Text="Delete All Guests" Width="160pt" runat="server" OnClick="DeleteAllGuest"  OnClientClick="DataLoading(1)"/> <%--FB 2664--%><%--ZD 100176--%> 
                        <%--Windows Dressing--%>
                                <b class="blackblodtext">Total Users: </b><b><asp:Label ID="lblTotalUsers" runat="server" Text=""></asp:Label> </b>
                            </td>
                            <td  align="right" width="15%" runat="server" id="tdLicencesRemaining">
                        <%--Windows Dressing--%>
				                <b class="blackblodtext">Licenses Remaining: </b><b><asp:Label ID="lblLicencesRemaining" runat="server" Text=""></asp:Label> </b>                                
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td align="center">
                    <asp:Table ID="tblPage" Visible="false" runat="server">
                        <asp:TableRow ID="TableRow1" runat="server">
                            <asp:TableCell ID="TableCell1" Font-Bold="True" Font-Names="Verdana" Font-Size="Small" ForeColor="Blue" runat="server"><span class="blackblodtext"> Pages:</span> </asp:TableCell>
                            <asp:TableCell ID="TableCell2" runat="server"></asp:TableCell>
                        </asp:TableRow>
                    </asp:Table>

                </td>
            </tr>
            <tr id="trSearchH" runat="server">
                <td align="Left">
                    <table cellspacing="5">
                        <tr>
                            <td>&nbsp;</td>
                            <td>
                                <SPAN class=subtitleblueblodtext>Search Users</SPAN>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr id="trSearch" runat="server">
                <td align="center">
                    <table width="80%" cellpadding="2" cellspacing="5">
                        <tr>
                             <%--Window Dressing --%>                        
                            <td align="right" style="font-weight:bold" class="blackblodtext">First Name</td>
                            <td align="left">
                                <asp:TextBox ID="txtFirstName" CssClass="altText" runat="server"></asp:TextBox>
                                 <asp:RegularExpressionValidator ID="regTemplateName" ControlToValidate="txtFirstName" Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<br>& < > + % \ / ? | ^ = ! `[ ] { } # $ and ~ are invalid characters." ValidationExpression="^(a-z|A-Z|0-9)*[^\\<>^+?|!`\[\]{}\=@#$%&~]*$"></asp:RegularExpressionValidator><%--FB 1888--%>
                            </td>
                            <%--Window Dressing --%>                        
                            <td align="right" style="font-weight:bold" class="blackblodtext">Last Name</td>
                            <td align="left">
                                <asp:TextBox ID="txtLastName" CssClass="altText" runat="server"></asp:TextBox>
                                 <asp:RegularExpressionValidator ID="RegularExpressionValidator1" ControlToValidate="txtLastName" Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<br>& < > + % \ / ? | ^ = ! `[ ] { } # $ and ~ are invalid characters." ValidationExpression="^(a-z|A-Z|0-9)*[^\\<>^+?|!`\[\]{}\=@#$%&~]*$"></asp:RegularExpressionValidator><%--FB 1888--%>
                            </td>
                            <%--Window Dressing --%>                        
                            <td align="right" style="font-weight:bold" class="blackblodtext">Email Address</td>
                            <td align="left">
                                <asp:TextBox ID="txtEmailAddress" CssClass="altText" runat="server"></asp:TextBox>
                                 <asp:RegularExpressionValidator ID="RegularExpressionValidator2" ControlToValidate="txtEmailAddress" Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<br>& < > ' + % \ / ( ) ; ? | ^ = ! ` , [ ] { } : # $ ~ and &#34; are invalid characters." ValidationExpression="^(a-z|A-Z|0-9)*[^\\/<>+;?|!`,\[\]{}\x22;=^:#$%&()'~]*$"></asp:RegularExpressionValidator>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr id="trSearchB" runat="server">
                <td align="center">
                    <table width="90%">
                        <tr>
                            <td width="50%">&nbsp;</td>
                            <td align="center"> <%--FB 2920--%>
                                <asp:Button ID="btnSearchUser" OnClientClick="javascript:return fnSearch();" OnClick="SearchUser" runat="server" CssClass="altLongBlueButtonFormat" Text="Submit" /> <%--Edited for FB 1405--%>
                            </td>
                            
                        </tr>
                    </table>
                </td>
            </tr>
            <tr id="trNewH" runat="server">
                <td align="Left">
                    <table cellspacing="5">
                        <tr>
                            <td>&nbsp;</td>
                            <td>
                                <SPAN class=subtitleblueblodtext>Create New User</SPAN>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr id="trNew" runat="server">
                <td align="center">
                    <table width="90%">
                        <tr>
                            <td width="50%">&nbsp;</td>
                            <td align="center"> <%--FB 2920--%>
                                <asp:Button ID="btnNewMCU" OnClick="CreateNewUser" runat="server" CssClass="altLongBlueButtonFormat" Text="Submit"  OnClientClick="DataLoading(1)"/>  <%--ZD 100176--%> 
                            </td>
                            
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </div>
<asp:TextBox ID="txtBridges" runat="server" Width="0" Height="0" ForeColor="transparent" BackColor="transparent" BorderColor="transparent"></asp:TextBox>
  <input type="hidden" name="Bridges" width="200">
  <asp:TextBox ID="txtType" Visible="false" CssClass="altText" runat="server" />

<img src="keepalive.asp" name="myPic" width="1px" height="1px">
    </form>
    <%--code added for Soft Edge button--%>
<script type="text/javascript" src="inc/softedge.js"></script>
    <!-- #INCLUDE FILE="inc/mainbottomNET.aspx" -->
</body>
</html>

