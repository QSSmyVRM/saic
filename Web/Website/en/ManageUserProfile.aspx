<%--ZD 100147 start--%>
<%--/* Copyright (C) 2013 myVRM - All Rights Reserved	
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.	
*
* You should have received a copy of the myVRM license with	
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/--%><%--ZD 100147 End--%>
<%@ Page Language="C#" Inherits="ns_MyVRM.UserProfile" Buffer="true" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajax" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"><%--FB 2779--%>
<meta http-equiv="X-UA-Compatible" content="IE=8">
<!-- FB 2719 Starts -->
<% if (Session["isExpressUser"].ToString() == "1"){%>
<!-- #INCLUDE FILE="inc/maintopNETExp.aspx" -->
<%}else{%>
<!-- #INCLUDE FILE="inc/maintopNET.aspx" -->
<%}%>
<!-- FB 2719 Ends -->
<%--FB 2481 start--%>
<%@ Register Assembly="DevExpress.SpellChecker.v10.2.Core, Version=10.2.3.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.XtraSpellChecker" TagPrefix="dxXSC" %>
<%@ Register Assembly="DevExpress.Web.ASPxSpellChecker.v10.2, Version=10.2.3.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxSpellChecker" TagPrefix="dxSC" %>
<%@ Register Assembly="DevExpress.Web.ASPxHtmlEditor.v10.2, Version=10.2.3.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxHtmlEditor" TagPrefix="dxHE" %>
<%@ Register Assembly="DevExpress.Web.ASPxEditors.v10.2, Version=10.2.3.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dxE" %>
<%@ Register Assembly="DevExpress.Web.v10.2, Version=10.2.3.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxPanel" TagPrefix="dxP" %>
<%@ Register Assembly="DevExpress.Web.v10.2, Version=10.2.3.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxRoundPanel" TagPrefix="dxRP" %>
    <%@ Register Assembly="DevExpress.Web.v10.2, Version=10.2.3.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGlobalEvents" TagPrefix="dx" %>
<%--FB 2481 end--%>
<script type="text/javascript">
  var servertoday = new Date();
</script>
<script type="text/javascript" src="inc/functions.js"></script>
<%--FB 1861--%>
<%--<script type="text/javascript" src="script/cal.js"></script>--%>
<script type="text/javascript" src="script/cal-flat.js"></script>
<script type="text/javascript" src="lang/calendar-en.js"></script>
<script type="text/javascript" src="script/calendar-setup.js"></script>
<script type="text/javascript" src="script/calendar-flat-setup.js"></script>

<script type="text/javascript" src="script/mytreeNET.js"></script>
<script language="javascript" src="../en/Organizations/Original/Javascript/RGBColorPalette.js"> </script>
<script type="text/javascript" src="script/RoomSearch.js"></script>
<link rel="stylesheet" type="text/css" media="all" href="css/calendar-blue.css" /> <%--FB 1861--%> <%--FB 1982--%>

<style type="text/css">
#lstDepartment option /* FB 2611 */
{
	color:Black;
}

</style>

<script runat="server">

</script>
<script language="javascript">

function DisplayWarning(obj)
{
    //ZD 100263
    //if ("<%=Session["userID"] %>" == document.getElementById("txtUserID").value)
    if ("<%=Session["userID"] %>" == "<%=Session["ModifiedUserID"] %>")
        if (obj.value != "-1")
        {
            alert("Default search results will display in lobby upon submit.\r\nPlease log off and log back in for changes to take affect.");
        }
}
function CheckDate(obj)
{
    //alert("in CheckDate" + Date.parse(obj.value) + " : " + Date.parse(new Date()) );
    if (Date.parse(obj.value) < Date.parse(new Date()))
    {
        alert("Invalid Date");
        //obj.focus(); FOGBUGZ CASE 100 - case was misexplained it is not for conferencesetup.
    }   
}

function CheckSecondaryEmail()
{
    var objL = document.getElementById("lstSendBoth");
    var objT = document.getElementById("txtUserEmail2");
    var objemail = document.getElementById("txtUserEmail");//FB 289
    if ( (objL.value == "1") && (objT.value == "") )
        {
            alert("Please enter a valid secondary email address");
            objL.value = "0";
            return false;
        }
/* FB 289 Starts */        
    if((objT.value != "") && (objemail.value != ""))
    {
        if(objT.value.toUpperCase() == objemail.value.toUpperCase())
        {
            alert("Secondary email should not be same as User email.");
            objT.focus();
            return false;
        } 
    }
 /* FB 289 Ends */
}
//FB 2339 Start

function validatePassword (pw, options) 
{
	// default options (allows any password)
	var o = {
		lower:    0,
		upper:    0,
		alpha:    0, /* lower + upper */
		numeric:  0,
		special:  0,
		length:   [0, Infinity],
		custom:   [ /* regexes and/or functions */ ],
		badWords: [],
		badSequenceLength: 0,
		noQwertySequences: false,
		noSequential:      false
	};

	for (var property in options)
		o[property] = options[property];

	var	re = {
			lower:   /[a-z]/g,
			upper:   /[A-Z]/g,
			alpha:   /[A-Z]/gi,
			numeric: /[0-9]/g,
			special: /[\W_]/g
		},
		rule, i;

	// enforce min/max length
	if (pw.length < o.length[0] || pw.length > o.length[1])
		return false;

	// enforce lower/upper/alpha/numeric/special rules
	for (rule in re) {
		if ((pw.match(re[rule]) || []).length < o[rule])
			return false;
	}

	// enforce word ban (case insensitive)
	for (i = 0; i < o.badWords.length; i++) {
		if (pw.toLowerCase().indexOf(o.badWords[i].toLowerCase()) > -1)
			return false;
	}

	// enforce the no sequential, identical characters rule
	if (o.noSequential && /([\S\s])\1/.test(pw))
		return false;

	// enforce alphanumeric/qwerty sequence ban rules
	if (o.badSequenceLength) {
		var	lower   = "abcdefghijklmnopqrstuvwxyz",
			upper   = lower.toUpperCase(),
			numbers = "0123456789",
			qwerty  = "qwertyuiopasdfghjklzxcvbnm",
			start   = o.badSequenceLength - 1,
			seq     = "_" + pw.slice(0, start);
		for (i = start; i < pw.length; i++) {
			seq = seq.slice(1) + pw.charAt(i);
			if (
				lower.indexOf(seq)   > -1 ||
				upper.indexOf(seq)   > -1 ||
				numbers.indexOf(seq) > -1 ||
				(o.noQwertySequences && qwerty.indexOf(seq) > -1)
			) {
				return false;
			}
		}
	}

	// enforce custom regex/function rules
	for (i = 0; i < o.custom.length; i++) {
		rule = o.custom[i];
		if (rule instanceof RegExp) {
			if (!rule.test(pw))
				return false;
		} else if (rule instanceof Function) {
			if (!rule(pw))
				return false;
		}
	}
	return true;
}

function PreservePassword()
{
        document.getElementById("txtPassword1_1").value = document.getElementById("txtPassword1").value;
        document.getElementById("txtPassword1_2").value = document.getElementById("txtPassword2").value;
        var password = document.getElementById("txtPassword1").value;
        if("<%=Session["EnablePasswordRule"]%>" == "1")
        {
            var passed = validatePassword(password, {
	                length:   [6, Infinity],
	                lower:    1,
	                upper:    1,
	                numeric:  1,
	                special:  0,
	                badWords: [],
	                badSequenceLength: 0
                    });
            if(passed == false)
            {
                document.getElementById("cmpValPassword1").style.visibility = 'visible';
            }
            else
            {
                document.getElementById("cmpValPassword1").style.visibility = 'hidden';
            }
        }
}
function RulePassword()
{
        if(document.getElementById("reqPassword1").style.display == "inline" || document.getElementById("cmpValPassword1").style.display == "inline")
        {
            window.scrollTo(0,0);
            return false;
        }
        // ZD 100263
        if(document.getElementById("txtPassword1").style.backgroundImage == "" && document.getElementById("txtPassword1").value == "")
        {
            ValidatorEnable(document.getElementById('reqPassword1'), true);
            window.scrollTo(0,0);
            return false;
        }


        var password = document.getElementById("txtPassword1").value;
        if("<%=Session["EnablePasswordRule"]%>" == "1")
        {
            var passed = validatePassword(password, {
	                length:   [6, Infinity],
	                lower:    1,
	                upper:    1,
	                numeric:  1,
	                special:  0,
	                badWords: [],
	                badSequenceLength: 0
                    });
            if(passed == false)
            {
               document.getElementById("cmpValPassword1").innerHTML = "Weak Password";  
                document.getElementById("txtPassword1").focus();
                document.getElementById("cmpValPassword1").style.display = "block";
                return false;          
            }
        }
        IsNumeric();
       DataLoading(1); // ZD 100176   
        
}
//FB 2339 End

function SavePassword()
{
        document.getElementById("txtPassword1").value = document.getElementById("txtPassword1_1").value;
        document.getElementById("txtPassword2").value = document.getElementById("txtPassword1_2").value;
}

function GetLocations()
{
    url = "LocationList.aspx?roomID=" + document.getElementById("hdnLocation").value;
	if (!window.winrtc) {	// has not yet been defined
		winrtc = window.open(url, "", "width=700,height=300,top=0,left=0,resizable=yes,scrollbars=yes,status=no");
		winrtc.focus();
	} else // has been defined
	    if (!winrtc.closed) {     // still open
	    	winrtc.close();
	        winrtc = window.open(url, "", "width=700,height=300,top=0,left=0,resizable=yes,scrollbars=yes,status=no");
			winrtc.focus();
		} else {
	        winrtc = window.open(url, "", "width=700,height=300,top=0,left=0,resizable=yes,scrollbars=yes,status=no");
	        winrtc.focus();
		}

}
//Code added for Ticker -Start

function fnShowFeed()
 {
    var tickerDisplay = document.getElementById("drpTickerDisplay");
    var tickerDisplay1 = document.getElementById("drpTickerDisplay1");
    
   
    if(tickerDisplay1.selectedIndex == "1")
    {
         document.getElementById("feedLink1").style.display = "Block";
         document.getElementById("txtFeedLink1").style.display = "Block";
    }
    else
    {
       document.getElementById("feedLink1").style.display = "None";
       document.getElementById("txtFeedLink1").style.display = "None";
    }
    
    if(tickerDisplay.selectedIndex == "1")
    {
         document.getElementById("feedLink").style.display = "Block";
         document.getElementById("txtFeedLink").style.display = "Block";
    }
    else
    {
       document.getElementById("feedLink").style.display = "None";
       document.getElementById("txtFeedLink").style.display = "None";
    }
 }
//Code changed for Ticker End
//API Port Starts...
function IsNumeric()

{
   
   var ValidChars = "0123456789";
   var IsNumber=true;
   var Char;
   var error = document.getElementById('lblapierror');
   var sTexttemp = document.getElementById("txtApiportno").value;
   for (i = 0; i < sTexttemp.length && IsNumber == true; i++) 
      { 
      Char = sTexttemp.charAt(i); 
      if (ValidChars.indexOf(Char) == -1) 
         {
         IsNumber = false;
         }
      }
   if(IsNumber == false)
    {
        error.style.display = 'block';
        error.innerHTML = "Numeric values only.";
        
    }
   if(IsNumber == true)
    {
        error.style.display = 'none';
        error.innerHTML = "";
        
    } 
   return IsNumber;
   
   }
//API Port Ends...
//FB 2594 Starts
    function isNumberKey(evt)
      {
         var charCode = (evt.which) ? evt.which : event.keyCode
         if (charCode > 31 && (charCode < 48 || charCode > 57))
            return false;

         return true;
      }
      //FB 2594 Ends

//FB 2565 
function fnCancel()
{
    window.location.replace('ManageUser.aspx?t=1');
    
}

// FB 2693 Starts

function fnShowPcConf(stat)
{
    if(stat == true)
    {
        document.getElementById("tblPcConf").style.display = "block";
    }
    else
    {
        document.getElementById("tblPcConf").style.display = "none";
    }
    
    document.getElementById("tblBlueJeans").style.display = "none";
    document.getElementById("tblVidtel").style.display = "none";
     document.getElementById("tblJabber").style.display = "none";
    document.getElementById("tblLync").style.display = "none";
    
    document.getElementById("expBlueJeans").src = "../image/loc/nolines_plus.gif";
    document.getElementById("expVidtel").src = "../image/loc/nolines_plus.gif";
    document.getElementById("expJabber").src = "../image/loc/nolines_plus.gif";
    document.getElementById("expLync").src = "../image/loc/nolines_plus.gif";
}

function fnShowPcConfOpt(par1, par2)
{

    document.getElementById("tblBlueJeans").style.display = "none";
    document.getElementById("tblVidtel").style.display = "none";
    document.getElementById("tblJabber").style.display = "none";
    document.getElementById("tblLync").style.display = "none";
    
    
    if(par1.src.indexOf('plus') > -1)
    {
        document.getElementById(par2).style.display = "block";
        par1.src = "../image/loc/nolines_minus.gif";
    }
    else if(par1.src.indexOf('minus') > -1)
    {
        document.getElementById(par2).style.display = "none";
        par1.src = "../image/loc/nolines_plus.gif";
    }
    
    if(document.getElementById("tblBlueJeans").style.display == "none")
        document.getElementById("expBlueJeans").src = "../image/loc/nolines_plus.gif";
    if(document.getElementById("tblVidtel").style.display == "none")
        document.getElementById("expVidtel").src = "../image/loc/nolines_plus.gif";
    if(document.getElementById("tblJabber").style.display == "none")
        document.getElementById("expJabber").src = "../image/loc/nolines_plus.gif";
    if(document.getElementById("tblLync").style.display == "none")
        document.getElementById("expLync").src = "../image/loc/nolines_plus.gif";
}
function toggle(par1, par2) 
    {
	    var ele = document.getElementById("par1");
	    var text = document.getElementById("par2");
	    if(ele.style.display == "block")
	    {
    		ele.style.display = "none";
		    text.innerHTML = "More";
  	    }
	    else 
	    {
		    ele.style.display = "block";
		    text.innerHTML = "Less";
	    }
    } 

// FB 2693 Ends
//FB 3054 Starts
function PasswordChange(par) 
{  
    if(document.getElementById('reqPassword1') != null) // ZD 100263
    {
        ValidatorEnable(document.getElementById('reqPassword1'), true);
    }
    
    document.getElementById("hdnPasschange").value = true;
    
    if (par == 1)
        document.getElementById("hdnPW1Visit").value = true;
    else
        document.getElementById("hdnPW2Visit").value = true;
} 
function EPPasswordChange(par) 
{  

    document.getElementById("hdnEPPWD").value = true;
    
    if (par == 1)
        document.getElementById("hdnEP1Visit").value = true;
    else
        document.getElementById("hdnEP2Visit").value = true;
}   
 function fnTextFocus(xid,par) {
     
     // ZD 100263 Starts
     var obj1 = document.getElementById("txtPassword1");
     var obj2 = document.getElementById("txtPassword2");
     
     if(document.getElementById("hdnPasschange").value == "false")
     {
         if(obj1.value == "" && obj2.value == "")
         {
             document.getElementById("txtPassword1").style.backgroundImage="";
             document.getElementById("txtPassword2").style.backgroundImage="";
             document.getElementById("txtPassword1").value="";
             document.getElementById("txtPassword2").value="";
         }
     }
     return false;
     // ZD 100263 Ends
     
      var obj = document.getElementById(xid);
          
    if (par == 1) {
        if(document.getElementById("hdnPW2Visit") != null)
        {
            if(document.getElementById("hdnPW2Visit").value == "false")
            { 
                document.getElementById("txtPassword1").value = "";
                document.getElementById("txtPassword1_1").value="";
                document.getElementById("txtPassword2").value = "";
                document.getElementById("txtPassword1_2").value="";
            }else
            {
                document.getElementById("txtPassword1").value = "";
                document.getElementById("txtPassword1_1").value="";
            }
        }
        else
        {
            document.getElementById("txtPassword1").value = "";
            document.getElementById("txtPassword1_1").value="";
            document.getElementById("txtPassword2").value = "";
            document.getElementById("txtPassword1_2").value="";
        }
    }
       else{
           if(document.getElementById("hdnPW1Visit") != null)
           {
            if(document.getElementById("hdnPW1Visit").value == "false")
            { 
                document.getElementById("txtPassword1").value = "";
                document.getElementById("txtPassword1_1").value="";
                document.getElementById("txtPassword2").value = "";
                document.getElementById("txtPassword1_2").value="";
            }
            else{
                document.getElementById("txtPassword2").value = "";
                document.getElementById("txtPassword1_2").value="";
                }
           
           }
           else{
                document.getElementById("txtPassword2").value = "";
                document.getElementById("txtPassword1_2").value="";
                }
        }
        
        if(document.getElementById("reqPassword1")!= null)
        {
            ValidatorEnable(document.getElementById('reqPassword1'), false);
            ValidatorEnable(document.getElementById('reqPassword1'), true);
        }
         if(document.getElementById("cmpValPassword1")!= null)
        {
            ValidatorEnable(document.getElementById('cmpValPassword1'), false);
            ValidatorEnable(document.getElementById('cmpValPassword1'), true);
        }
         if(document.getElementById("reqPassword2")!= null)
        {
            ValidatorEnable(document.getElementById('reqPassword2'), false);
            ValidatorEnable(document.getElementById('reqPassword2'), true);
        }
         if(document.getElementById("cmpValPassword2")!= null)
        {
            ValidatorEnable(document.getElementById('cmpValPassword2'), false);
            ValidatorEnable(document.getElementById('cmpValPassword2'), true);
        }
}
 function fnEPTextFocus(xid,par) {
  // ZD 100263 Starts
     var obj1 = document.getElementById("txtEPPassword1");
     var obj2 = document.getElementById("txtEPPassword2");
     if(obj1.value == "" && obj2.value == "")
     {
         document.getElementById("txtEPPassword1").style.backgroundImage="";
         document.getElementById("txtEPPassword2").style.backgroundImage="";
         document.getElementById("txtEPPassword1").value="";
         document.getElementById("txtEPPassword2").value="";
     }
     return false;
     // ZD 100263 Ends
      var obj = document.getElementById(xid);
          
    if (par == 1) {
        if(document.getElementById("hdnEP2Visit") != null)
        {
            if(document.getElementById("hdnEP2Visit").value == "false")
            { 
                document.getElementById("txtEPPassword1").value = "";
                document.getElementById("txtEPPassword2").value="";
            }else
            {
                document.getElementById("txtPassword1").value = "";
            }
        }
        else
        {
             document.getElementById("txtEPPassword1").value = "";
                document.getElementById("txtEPPassword2").value="";
        }
    }
       else{
           if(document.getElementById("hdnEP1Visit") != null)
           {
            if(document.getElementById("hdnEP1Visit").value == "false")
            { 
                document.getElementById("txtEPPassword2").value = "";
                document.getElementById("txtEPPassword1").value="";
            }
            else{
                document.getElementById("txtEPPassword2").value = "";
                }
           
           }
           else{
                document.getElementById("txtEPPassword2").value = "";
                document.getElementById("txtEPPassword1").value="";
                }
        }
        
        if(document.getElementById("RegularExpressionValidator9")!= null)
        {
            ValidatorEnable(document.getElementById('RegularExpressionValidator9'), false);
            ValidatorEnable(document.getElementById('RegularExpressionValidator9'), true);
        }
         if(document.getElementById("CmpEP1")!= null)
        {
            ValidatorEnable(document.getElementById('CmpEP1'), false);
            ValidatorEnable(document.getElementById('CmpEP1'), true);
        }
         if(document.getElementById("CmpEP2")!= null)
        {
            ValidatorEnable(document.getElementById('CmpEP2'), false);
            ValidatorEnable(document.getElementById('CmpEP2'), true);
        }
}
//FB 3054 Ends
</script>
<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title>User Profile</title>
    <script type="text/javascript" src="inc/functions.js"></script>

</head>
<body>
    <form id="frmUserProfile" runat="server" method="post" onsubmit="return true">
    <input type="hidden" value="<%=Session["ThemeType"]%>" id="sessionThemeType" /> <%--FB 2815--%>
    <div id="hideScreen" style="position:absolute; background-color:white; top:0; left:0; width:1500px; height:2000px; z-index:980; display:block"></div><%--FB 2491 FB 2811--%>
        <asp:ScriptManager ID="PrivateVMRScriptManager" runat="server" AsyncPostBackTimeout="600">
                 </asp:ScriptManager><!--Fb 2481-->
    <input id="txtPassword1_1" runat="server" type="hidden" />
    <input id="txtPassword1_2" runat="server" type="hidden" />
    <input name="selectedloc" type="hidden" id="selectedloc" runat="server"  /> <!--Added room search-->
     <input name="locstrname" type="hidden" id="locstrname" runat="server"  /> <!--Added room search-->
    <input id="txtLevel" runat="server" type="hidden" />
    <%--Code changed for FB 1425 QA Bug -Start--%>
      <input type="hidden" id="hdntzone" runat="server"/>
      <%--Code changed for FB 1425 QA Bug -End--%>
      <input type="hidden" name="hdnAVParamState" id="hdnAVParamState" runat="server" /> <%--FB 1985--%>
      <input type="hidden" name="hdnHelpReq" id="hdnHelpReq" runat="server" /> <%--FB 2268--%>
       <input type="hidden" id="hdnPasschange" value="false" runat="server"/> <%--FB 3054--%>
       <input type="hidden" id="hdnPW1Visit" value="false" runat="server"/> <%--FB 3054--%>
       <input type="hidden" id="hdnPW2Visit" value="false" runat="server"/> <%--FB 3054--%>
       <input type="hidden" id="hdnEPPWD" value="false" runat="server"/> <%--FB 3054--%>
       <input type="hidden" id="hdnEP1Visit" value="false" runat="server"/> <%--FB 3054--%>
       <input type="hidden" id="hdnEP2Visit" value="false" runat="server"/> <%--FB 3054--%>
    <div>
      <input type="hidden" id="helpPage" value="65" /><%-- ZD 100263 --%>
      <%--<asp:TextBox ID="txtUserID" style="width:0" Height="0" runat="server" BorderStyle="none" BorderWidth="0"></asp:TextBox>--%>

        <table style="width:95%;" align="center" border="0"><%--FB 2611--%>
            <tr>
                <td align="center">
                    <h3>
                        <asp:Label ID="lblHeader" runat="server"></asp:Label>
                    </h3>
                </td>
            </tr>
            <tr>
                <td align="center">
                    <asp:Label ID="errLabel" runat="server" Text="" CssClass="lblError"></asp:Label>
                    <asp:CustomValidator runat="server" Display="dynamic" ID="cusVal1" OnServerValidate="ValidateIPAddress" CssClass="lblError"></asp:CustomValidator>
                </td>
            </tr>
            <tr><div id="dataLoadingDIV" align="center"></div></tr><%--ZD 100176--%>
            <tr>
                <td align="Left">
                    <table cellspacing="3" style="width:100%; margin-left:-60px; position:relative"> <%--FB 2611--%>
                        <tr>
                            <td>&nbsp;</td>
                            <td>
                                <span class="subtitleblueblodtext">Select Personal Options</span>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td align="center">
                    <table style="width:100%" cellspacing="3" cellpadding="2" border="0"> <%--FB 2611--%>
                        <tr>
                            <td style="width:20%" align="left" class="blackblodtext">First Name<span class="reqfldstarText">*</span></td><%-- FB 1773 --%>
                            <td style="width:30%" align="left">
                            <table border="0" cellspacing="0" cellpadding="1" align="left"><tr><td valign="bottom" align="left"> <%--Edited for FF--%>
                                <asp:TextBox ID="txtUserFirstName" Enabled='<%# Application["ssoMode"].ToString().ToUpper().Equals("NO") %>' CssClass="altText" runat="server"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="reqFirstName" runat="server" ControlToValidate="txtUserFirstName" Display="dynamic" ErrorMessage="Required"></asp:RequiredFieldValidator>
                                <asp:RegularExpressionValidator ID="RegularExpressionValidator20" ControlToValidate="txtUserFirstName" Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<br>& < > + % \ / ? | = ! ` [ ] { } # $ @ and ~ are invalid characters." ValidationExpression="^(a-z|A-Z|0-9)*[^\\/<>+?|!`\[\]{}\=@#$%&~]*$"></asp:RegularExpressionValidator> <%--FB 1888--%>
                                </td></tr>
                             </table><%-- FB 1773 --%>
                                <%--<tr><td valign="top" align="left">
                                <asp:TextBox ID="txtUserLastName" CssClass="altText" runat="server"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="reqLastName" runat="server" ControlToValidate="txtUserLastName" Display="dynamic" ErrorMessage="Required"></asp:RequiredFieldValidator>
                                <asp:RegularExpressionValidator ID="RegularExpressionValidator1" ControlToValidate="txtUserLastName" Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<br>& < > ' + % \ / ; ? | = ! ` , [ ] { } : # $ @ ~ and &#34; are invalid characters." ValidationExpression="^(a-z|A-Z|0-9)*[^\\/<>+;?|!`,\[\]{}\x22;=:@#$%&'~]*$"></asp:RegularExpressionValidator>
                                </td></tr>--%><%--Edited for FF--%>
                                </td>
                            <td style="width:20%" align="left" class="blackblodtext">Last Name<span class="reqfldstarText">*</span></td>
                            <td valign="top" align="left">
                                <asp:TextBox ID="txtUserLastName" CssClass="altText" runat="server"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="reqLastName" runat="server" ControlToValidate="txtUserLastName" Display="dynamic" ErrorMessage="Required"></asp:RequiredFieldValidator>
                                <asp:RegularExpressionValidator ID="RegularExpressionValidator7" ControlToValidate="txtUserLastName" Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<br>& < > + % \ / ? | = ! ` [ ] { } # $ @ and ~ are invalid characters." ValidationExpression="^(a-z|A-Z|0-9)*[^\\/<>+?|!`\[\]{}\=@#$%&~]*$"></asp:RegularExpressionValidator><%--FB 1888--%>
                             </td>
                            <%--<td style="width:30%" align="left">
                                <asp:TextBox CssClass="altText" ID="txtUserLogin" runat="server"></asp:TextBox>
                                
                            </td>--%><%-- FB 1773 --%>
                         </tr>   

                        <tr>
                            <td style="width:20%" align="left" class="blackblodtext">Password<span class="reqfldstarText">*</span></td>
                            <td style="width:30%" align="left">
                                <asp:TextBox ID="txtPassword1" style="background-image:url('../image/pwdbg.png'); background-repeat:no-repeat" onchange="javascript:PreservePassword()" TextMode="Password"  onblur="PasswordChange(1)" onfocus="fnTextFocus(this.id,1)" CssClass="altText" runat="server"></asp:TextBox> <%--FB 3054  ZD 100263--%>
                                <asp:RequiredFieldValidator ID="reqPassword1" Enabled="false" ControlToValidate="txtPassword1" Display="dynamic" ErrorMessage="Required" runat="server"></asp:RequiredFieldValidator>
                                <asp:RegularExpressionValidator ID="regPassword1" ControlToValidate="txtPassword1" Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<br> & < and > are invalid characters." ValidationExpression="^[^<>&]*$"></asp:RegularExpressionValidator><%--FB 2339--%>
                                <asp:CompareValidator ID="cmpValPassword1" runat="server" ControlToCompare="txtPassword2"
                                    ControlToValidate="txtPassword1" Display="Dynamic" ErrorMessage="<br>Re-enter password."></asp:CompareValidator>
                            </td>
                            <td style="width:20%" align="left" class="blackblodtext">Retype Password<span class="reqfldstarText">*</span></td>
                            <td style="width:30%" align="left">
                                <asp:TextBox CssClass="altText" style="background-image:url('../image/pwdbg.png'); background-repeat:no-repeat" ID="txtPassword2" onchange="javascript:PreservePassword()"  onblur="PasswordChange(2)" onfocus="fnTextFocus(this.id,2)" TextMode="Password" runat="server"></asp:TextBox> <%--FB 3054  ZD 100263--%>
                                <asp:RegularExpressionValidator ID="regPassword2" ControlToValidate="txtPassword2" Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<br> & < and > are invalid characters." ValidationExpression="^[^<>&]*$"></asp:RegularExpressionValidator><%--FB 2339--%>
                                <asp:CompareValidator ID="cmpValPassword2" runat="server" ControlToCompare="txtPassword1"
                                    ControlToValidate="txtPassword2" Display="Dynamic" ErrorMessage="<br>Passwords do not match."></asp:CompareValidator>
                            </td>
                        </tr>
                         <%--FB 2339 - Start--%>
                        <tr>
                        <td id=""  colspan = "2" align="left">
                           <span id="spanpassword" runat="server">Info :password should have atleast one upper case,one lower case,<br />one numeric and minimum length of 6 characters</span>
                        </td>
                        </tr>
                         <%--FB 2339 - End--%>
                        <tr>
                            <td style="width:20%" align="left" class="blackblodtext">User Email<span class="reqfldstarText">*</span></td>
                            <td align="left">
                                <asp:TextBox ID="txtUserEmail" runat="server" CssClass="altText"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="reqUserEmail" ControlToValidate="txtUserEmail" Display="dynamic" ErrorMessage="Required" runat="server"></asp:RequiredFieldValidator>
                                <%--Code Modification For NGC Enhancement - Start--%>
                                <asp:RegularExpressionValidator ID="regEmail1_1" ControlToValidate="txtUserEmail" Display="dynamic" runat="server" 
                                    ErrorMessage="<br>Invalid email address." ValidationExpression="[a-zA-Z][\w\.-]*[a-zA-Z0-9]@[a-zA-Z0-9][\w\.-]*[a-zA-Z0-9]\.[a-zA-Z][a-zA-Z\.]*[a-zA-Z]$"></asp:RegularExpressionValidator>
                                <asp:RegularExpressionValidator ID="regEmail1_2" ControlToValidate="txtUserEmail" Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<br>& < > ' + % \ /(); ? | ^= ! ` , [ ] { } : # $ ~ and &#34; are invalid characters." ValidationExpression="^(a-z|A-Z|0-9)*[^\\/<>+;?|!`,\[\]{}\x22;=:#$%&'~]*$"></asp:RegularExpressionValidator>
                            </td>
                            <td style="width:20%" align="left" class="blackblodtext">Secondary Email</td>
                            <td style="width:30%" align="left">
                                <asp:TextBox CssClass="altText" ID="txtUserEmail2" runat="server" onblur="CheckSecondaryEmail()"></asp:TextBox>
                                <asp:RegularExpressionValidator ID="regEmail2_1" ControlToValidate="txtUserEmail2" Display="dynamic" runat="server" 
                                    ErrorMessage="<br>Invalid email address." ValidationExpression="[a-zA-Z][\w\.-]*[a-zA-Z0-9]@[a-zA-Z0-9][\w\.-]*[a-zA-Z0-9]\.[a-zA-Z][a-zA-Z\.]*[a-zA-Z]$"></asp:RegularExpressionValidator>
                                <asp:RegularExpressionValidator ID="regEmail2_2" ControlToValidate="txtUserEmail2" Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<br>& < > ' + % \ /() ; ? | ^= ! ` , [ ] { } : # $ and &#34; are invalid characters." ValidationExpression="^(a-z|A-Z|0-9)*[^\\/<>+;?|!`,\[\]{}\x22;=:#$%&'~]*$"></asp:RegularExpressionValidator>
                                <%--Code Modification For NGC Enhancement - End--%>
                            </td>
                        </tr>
                        <tr><%-- FB 1773-Starts --%>
                        <td style="width:20%" align="left" class="blackblodtext">
                                <asp:Label ID="Label4" runat="server" Text="AD/LDAP Login"></asp:Label>
                        </td>
                        <td style="width:30%" align="left">
                        <asp:TextBox CssClass="altText" ID="txtUserLogin" runat="server"></asp:TextBox>
                        <asp:RegularExpressionValidator ID="regUserLogin" ControlToValidate="txtUserLogin" Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<br> & < and > are invalid characters." ValidationExpression="^[^<>&]*$"></asp:RegularExpressionValidator> <%-- ZD 100263 --%>
                        </td>
                        <td style="width:20%" align="left" class="blackblodtext">Email Notification</td>
                        <td style="width:30%" align="left">
                                <asp:DropDownList ID="lstEmailNotification" runat="server" CssClass="altText">
                                    <asp:ListItem Selected="True" Value="1" Text="Yes"></asp:ListItem>
                                    <asp:ListItem Value="0" Text="No"></asp:ListItem>
                                </asp:DropDownList>
                        </td>
                        </tr><%-- FB 1773-End --%>
                        <tr><%-- Phone - New Design START--%>
                            <td style="width:20%" align="left" class="blackblodtext">
                                <asp:Label ID="Label2" runat="server" Text="Work"></asp:Label>
                            </td>
                            <td style="width:30%" align="left" >
                                <asp:TextBox CssClass="altText" ID="txtWorkPhone" runat="server"></asp:TextBox>                               
                            </td>
                            <td style="width:20%" align="left" class="blackblodtext">
                                <asp:Label ID="Label3" runat="server" Text="Cell"></asp:Label>
                            </td>
                            <td style="width:30%" align="left" >
                                <asp:TextBox CssClass="altText" ID="txtCellPhone" runat="server"></asp:TextBox>                               
                            </td>
                        </tr><%-- Phone - New Design End--%>
                        <tr>
                            <%--Code changed for FB 1425 QA Bug -Start--%>
                            <td style="width:20%" align="left" class="blackblodtext" id ="TzTD1" runat="server">User-preferred Time Zone<span class="reqfldstarText">*</span></td>
                            <td style="width:30%" align="left"  id ="TzTD2" runat="server">
                            <div style="width:172px; overflow:hidden;"> <%--FB 2982--%>
                                <asp:DropDownList ID="lstTimeZone" runat="server" CssClass="altSelectFormat" DataTextField="timezoneName" DataValueField="timezoneID" Width="172px" onchange='ResetWidth(this)'  onblur='ResetWidth(this)' onmousedown='SetWidthToAuto(this)'> <%--FB 2982--%>
                                </asp:DropDownList>
                                <asp:RequiredFieldValidator ID="reqTZ" ControlToValidate="lstTimeZone" ErrorMessage="Required" InitialValue="-1" Display="dynamic" runat="server"></asp:RequiredFieldValidator>
                                </div><%--FB 2982--%>
                            </td>
                            <%--Code changed for FB 1425 QA Bug -End--%>
                            <td style="width:20%" align="left" class="blackblodtext">Preferred Address Book</td>
                            <td style="width:30%" align="left">
                                <asp:DropDownList ID="lstAddressBook" runat="server" CssClass="altSelectFormat" >
                                    <%--Code Modified. Removed selected text from existing on 21MAr09 - FB 412 - Start --%>
                                    <asp:ListItem  Value="-1" Text="None"></asp:ListItem> <%-- FB Case 526: Saima--%>
                                    <%--Code Modified. Removed selected text from existing on 21MAr09 - FB 412 - End    --%> 
                                    <%--<asp:ListItem Selected="True" Value="-1" Text="None"></asp:ListItem>--%> <%-- FB Case 526: Saima--%>
                                    <%--<asp:ListItem Value="1" Text="MS Outlook 2000/2002/XP"></asp:ListItem>--%>
<%--                                    <asp:ListItem Value="2" Text="Lotus Notes 6.x"></asp:ListItem>
--%>                                </asp:DropDownList>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" Enabled="false" ControlToValidate="lstAddressBook" ErrorMessage="Required" InitialValue="-1" Display="dynamic" runat="server"></asp:RequiredFieldValidator> <%-- FB Case 526: Saima--%>
                            </td>
                        </tr>
                        <tr>
                            <td style="width:20%" align="left" class="blackblodtext">Default Group</td>
                            <td style="width:30%" align="left">
                                <asp:DropDownList ID="lstDefaultGroup" CssClass="altSelectFormat" DataValueField="groupID" DataTextField="groupName" runat="server"></asp:DropDownList>
                            </td> <%-- ZD 100263 --%>
                            <td id="tdUsrRole" runat="server" style="width:20%" align="left" class="blackblodtext">User Role<span class="reqfldstarText">*</span></td>
                            <td id="tdDrpDownUsrRole" runat="server" style="width:30%" align="left">
                                <asp:DropDownList ID="lstUserRole" CssClass="altSelectFormat" DataValueField="ID" DataTextField="name" runat="server" OnSelectedIndexChanged="UpdateDepartments" AutoPostBack="true">
                                </asp:DropDownList>
                                <asp:DropDownList ID="lstAdmin" Visible="false" DataValueField="ID" DataTextField="level" runat="server"></asp:DropDownList>
                                <asp:RequiredFieldValidator ID="reqRole" ControlToValidate="lstUserRole" ErrorMessage="Required" InitialValue="-1" Display="dynamic" runat="server"></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td style="width:20%" align="left" class="blackblodtext">Account Expiration<span class="reqfldstarText">*</span></td>
                            <td style="width:30%" align="left">
                                <asp:TextBox ID="txtAccountExpiry" onblur="javascript:CheckDate(this)" onchange="javascript:CheckDate(this)" CssClass="altText" runat="server" ></asp:TextBox>
                                <%-- Code added by Offshore for FB Issue 1073 -- Start
                                 Code Modified For FB 1425- For Javascript date error.
                                 <img src="image/calendar.gif" border="0" style="width:20" height="20" id="Img1" style="cursor: pointer;" title="Date selector" onclick="return showCalendar('<%=txtAccountExpiry.ClientID %>', 'cal_triggerd', 1, '%m/%d/%Y');" />--%>
                                <img src="image/calendar.gif" border="0" width="20" height="20" id="cal_triggerd" style='cursor: pointer;<%=((txtAccountExpiry.Enabled)? "" : "display:none;")%>' title="Date selector" onclick="return showCalendar('<%=txtAccountExpiry.ClientID %>', 'cal_triggerd', 1, '<%=format%>');" />                                
                                 <%-- Code added by Offshore for FB Issue 1073 -- End --%>
                                <asp:RequiredFieldValidator ID="reqAccExp" ControlToValidate="txtAccountExpiry" runat="server" ErrorMessage="Required" Display="Dynamic"></asp:RequiredFieldValidator>
<%--                                <asp:RangeValidator ID="rangeExpiry" ControlToValidate="txtAccountExpiry" runat="server" ErrorMessage="Invalid Expiry Date" Display="dynamic"></asp:RangeValidator>
--%>                            </td>
                            <td id="tdScheduleMin" runat="server" style="width:20%" align="left" class="blackblodtext">Scheduling Minutes Remaining<span class="reqfldstarText">*</span></td><%--FB 2659--%>
                            <td style="width:30%" align="left" id="tdTimeRm" runat="server" > <%--ZD 100263--%>
                                <asp:TextBox ID="txtTimeRemaining" runat="server"></asp:TextBox><%-- FB 1773 --%>
                                <asp:RequiredFieldValidator ID="reqMinutes" SetFocusOnError="true" runat="server" ErrorMessage="Required" CssClass="lblError" ControlToValidate="txtTimeRemaining" ></asp:RequiredFieldValidator>
                                <asp:RangeValidator ID="ValidatorTimeremaining" SetFocusOnError="true" runat="server" CssClass="lblError" Display="dynamic" ControlToValidate="txtTimeRemaining"
                                    ErrorMessage="<br>Please enter a value between 0 and 2000000000." MaximumValue="2000000000" MinimumValue="0" Type="Integer"></asp:RangeValidator>                            </td>
                        </tr>
                        <%--<tr>
                            <td style="width:20%" align="left" class="blackblodtext">Email Notification</td>
                            <td style="width:30%" align="left">
                                <asp:DropDownList ID="lstEmailNotification" runat="server" CssClass="altText">
                                    <asp:ListItem Selected="True" Value="1" Text="Yes"></asp:ListItem>
                                    <asp:ListItem Value="0" Text="No"></asp:ListItem>
                                </asp:DropDownList>
                            </td>
                            <td style="width:20%" align="left" class="blackblodtext">My Default Search</td>
                            <td style="width:30%" align="left">
                                <asp:DropDownList ID="lstSearchTemplate" CssClass="altSelectFormat" DataTextField="Name" DataValueField="ID" runat="server" onchange="javascript:DisplayWarning(this);"></asp:DropDownList>
                            </td>
                        </tr>--%><%-- FB 1773 --%>
                        <tr>
                            <td style="width:20%" align="left" class="blackblodtext" valign="top">Preferred Location</td>
                            <td style="width:30%" align="left" valign="top" runat="server" id="TDprefLocation">
                                <asp:TextBox ID="txtLocation" runat="server" CssClass="altText" style="display:none;" Enabled="false" ></asp:TextBox>
                                <select size="4" name="RoomList" runat="server" id="RoomList" class="treeSelectedNode" Rows="3"></select>
                                 <input name="addRooms" type="button" id="addRooms" onclick="javascript:AddRooms();" style="display:none;" />
                                <a href="javascript: OpenRoomSearch('frmUserProfile');" onmouseover="window.status='';return true;" runat="server" id="roomclick"><img border="0" runat="server" id="roomimage" src="image/edit.gif" alt="edit" style="width:17;HEIGHT:15;cursor:pointer;"  title="Select Location"></a>  <%--FB 2798--%>                             
                                <input type="hidden" id="hdnLocation" runat="server" />
                            </td>
                            <td style="width:20%" align="left" class="blackblodtext" valign="top">User's Departments</td><%-- FB 1773 --%>
                            <td style="width:30%" align="left" valign="top">
                                <asp:ListBox runat="server" ID="lstDepartment" CssClass="altSelectFormat" DataTextField="name" DataValueField="id" Rows="6" SelectionMode="Multiple"></asp:ListBox><%-- Added for FB 1507 --%>
                            </td>
                        </tr>
                        <tr>
                            <td align="left" class="blackblodtext" valign="top" style="width:20%">
                                Send Email to Both Primary and Secondary Addresses</td>
                            <td align="left" valign="top" style="width:30%">
                                <asp:DropDownList ID="lstSendBoth" CssClass="altText" runat="server" onchange="javascript:CheckSecondaryEmail()">  <%-- --%>
                                    <asp:ListItem Text="No" Value="0"></asp:ListItem>
                                    <asp:ListItem Text="Yes" Value="1"></asp:ListItem>
                                </asp:DropDownList>
                            </td><%-- FB 1773 --%>
                            <td style="width:20%" align="left" class="blackblodtext">My Default Search</td>
                            <td style="width:30%" align="left">
                                <asp:DropDownList ID="lstSearchTemplate" CssClass="altSelectFormat" DataTextField="Name" DataValueField="ID" runat="server" onchange="javascript:DisplayWarning(this);"></asp:DropDownList>
                            </td>
                        </tr>
                        <%--Code added For FB Issue 1073 Start --%> 
                        <tr>
                            <td style="width:20%" align="left" class="blackblodtext">Date Format</td>
                            <td style="width:30%" align="left" nowrap="nowrap"> <%--FB 2611--%>
                                <asp:DropDownList ID="DrpDateFormat" runat="server" CssClass="altSelectFormat" >
                                    <asp:ListItem Selected="True" Value="MM/dd/yyyy" Text="US (MM/dd/yyyy)"></asp:ListItem> 
                                    <asp:ListItem Value="dd/MM/yyyy" Text="UK (dd/MM/yyyy)"></asp:ListItem>                                  
                              </asp:DropDownList>/ 
                               <asp:DropDownList ID="lstTimeFormat" CssClass="altText" runat="server">
                                    <asp:ListItem Selected="True" Value="1" Text="12 hour (01:00 PM)"></asp:ListItem>
                                    <asp:ListItem Value="0" Text="24 hour (13:00)"></asp:ListItem>
                                    <asp:ListItem Value="2" Text="Zulu time (1300Z)"></asp:ListItem><%--FB 2588--%>
                                </asp:DropDownList>
                                
                            </td>
                            <%--Code changed for FB 1425 QA Bug -Start--%>
                            <td style="width:20%" align="left" class="blackblodtext" id ="TzTD3" runat="server">Display Timezone <br />on all Screens</td>
                            <td style="width:50%" align="left" id ="TzTD4" runat="server">
                            <%-- Organization Css Module --%>   
                                <asp:DropDownList ID="lstTimeZoneDisplay" runat="server" CssClass="altSelectFormat">
                                    <asp:ListItem Selected="True" Value="1" Text="Yes"></asp:ListItem>
                                    <asp:ListItem Value="0" Text="No"></asp:ListItem>
                                </asp:DropDownList>
                            </td>
                            <%--Code changed for FB 1425 QA Bug -Start--%>
                        </tr>
                    <%--Code added For FB Issue 1073 End --%>
                        <%--Code changed for MOJ Phase 2 QA Bug -Start--%>
                        <tr id="EnableTR" runat="server">
                       <%--Code changed for MOJ Phase 2 QA Bug -End--%>
                            <td style="width:20%" align="left" class="blackblodtext">
                                <asp:Label ID="ParLBL" runat="server" Text="Enable Participants"></asp:Label>
                            </td>
                            <td style="width:30%" align="left" >
                                <asp:DropDownList ID="DrpEnvPar" runat="server" CssClass="altSelectFormat" >
                                    <asp:ListItem Selected="True" Value="1" Text="Yes"></asp:ListItem> 
                                    <asp:ListItem Value="0" Text="No"></asp:ListItem>                                  
                              </asp:DropDownList>                                
                            </td>
                            <td style="width:20%" align="left" class="blackblodtext">
                                <asp:Label ID="AVLBL" runat="server" Text="Enable A/V Settings"></asp:Label>
                            </td>
                            <td style="width:30%" align="left" >
                                <asp:DropDownList ID="DrpEnableAV" runat="server" CssClass="altSelectFormat" >
                                    <asp:ListItem Selected="True" Value="1" Text="Yes"></asp:ListItem> 
                                    <asp:ListItem Value="0" Text="No"></asp:ListItem>                                  
                              </asp:DropDownList>                                
                            </td>
                        </tr>
                        <%--Code Added for License modification START--%>
                        <tr id="trUser" runat="server">
                            <td style="width:20%" align="left" class="blackblodtext">
                          <%-- FB 2098 --%> 
                          <asp:Label ID="LblExc" runat="server" Text="Enable Outlook"></asp:Label>
                            </td>
                            <td style="width:30%" align="left" >
                                <asp:DropDownList ID="DrpExc" runat="server" CssClass="altSelectFormat" >
                                    <asp:ListItem Value="0" Selected="True" Text="No"></asp:ListItem> 
                                    <asp:ListItem  Value="1" Text="Yes"></asp:ListItem>                                  
                              </asp:DropDownList>                                
                            </td>
                            <td style="width:20%" align="left" class="blackblodtext">
                          <%--  FB 2098  --%>  
                          <asp:Label ID="LblDom" runat="server" Text="Enable Notes"></asp:Label>
                            </td>
                            <td style="width:30%" align="left" >
                                <asp:DropDownList ID="DrpDom" runat="server" CssClass="altSelectFormat" >
                                    <asp:ListItem Value="0" Selected="True" Text="No"></asp:ListItem> 
                                    <asp:ListItem  Value="1" Text="Yes"></asp:ListItem> 
                                                                     
                              </asp:DropDownList>                                
                            </td>
                        </tr>
                        <%--FB 2023 start --%> 
                        <tr id="tr3" runat="server">
                            <td style="width:20%" align="left" class="blackblodtext" id="tdLblMob" runat="server"> <%--ZD 100263--%>
                                    <asp:Label ID="LblMob" runat="server" Text="Enable Mobile"></asp:Label>
                                </td>
                                <td style="width:30%" align="left" id="tdDrpMob" runat="server" > <%--ZD 100263--%>
                                <asp:DropDownList ID="DrpMob" runat="server" CssClass="altSelectFormat" >
                                    <asp:ListItem Value="0" Selected="True" Text="No"></asp:ListItem> 
                                    <asp:ListItem  Value="1" Text="Yes"></asp:ListItem>                                  
                                </asp:DropDownList>                                
                                </td>
                            <%--FB 1830 - Start--%>                               
                            <td style="width:20%" align="left" class="blackblodtext">
                            <asp:Label ID="PrefLang" runat="server" Text="Preferred Language"></asp:Label>
                            </td>
                            <td style="width:30%" align="left" >
                                <asp:DropDownList ID="lstLanguage" runat="server" CssClass="altSelectFormat" DataTextField="name" DataValueField="ID"></asp:DropDownList>
                                <asp:RequiredFieldValidator ID="reglstLanguage" ControlToValidate="lstLanguage" ErrorMessage="Required" InitialValue="-1" Display="dynamic" runat="server"></asp:RequiredFieldValidator>
                            </td>
                            </tr>
                            <%--FB 1979 - Start--%>
                            <tr>
                                <td style="width:19%" align="left" valign="top" class="blackblodtext">
                                <asp:Label ID="emaillang" runat="server" Text="Email Language"></asp:Label>
                                </td>
                                <td align="left" style="width:30%" > <%--FB 1830 - DeleteEmailLang start--%>  
                                    <%-- FB 2029 starts --%>
                                    <asp:Button ID="btnDefine" runat="server" Text="Customize" OnClick="DefineEmailLanguage" class="altLongBlueButtonFormat"  OnClientClick="DataLoading(1);"/> <%--ZD 100176--%> 
                                    <asp:TextBox ID="txtEmailLang" runat="server" ReadOnly="true" CssClass="altText" Visible="false"></asp:TextBox>  <%--FB 2104 --%> <%-- FB 2029 end --%>                          
                                    <asp:ImageButton ImageUrl="image/btn_delete.gif" runat="server" ID="delEmailLang" ToolTip="Delete Email Language" onclick="DeleteEmailLangugage" OnClientClick="javascript:return fnDelEmailLan()" />
                                </td>  
                                <td style="width:20%" align="left" class="blackblodtext" id="tdLblPIM" runat="server" > <%--ZD 100263--%>
                                    <asp:Label ID="Label5" runat="server" Text="PIM Notifications"></asp:Label>
                                </td>
                                <td style="width:30%" align="left" id="tdDrpPIM" runat="server" >
                                <asp:DropDownList ID="DrpPIMNotifications" runat="server" CssClass="altSelectFormat" >
                                    <asp:ListItem Value="0" Selected="True" Text="No"></asp:ListItem> 
                                    <asp:ListItem  Value="1" Text="Yes"></asp:ListItem>                                  
                                </asp:DropDownList>                                
                                </td>
                            </tr>
                            <%--FB 1979 - End--%>
                            <%--New Lobby Page--%>                            
                            <tr id="tr4" runat="server">
                            <td style="width:19%" align="left" valign="top" class="blackblodtext">
                                <asp:Label ID="Loginmgmt" runat="server" Text="Lobby Management"></asp:Label>
                            </td>
                            <td style="width:10%" align="left" valign="bottom">
                                <asp:Button id="btnlogin" runat="server" Text="Customize" class="altLongBlueButtonFormat" onclick="lobbyManage"  OnClientClick="DataLoading(1)"/><%--ZD 100176--%> 
                            </td>
                            <%-- FB 2029 starts --%>
                            <td  align="left" valign="center" class="blackblodtext"> <%--FB 1860--%>
                                <asp:Label ID="LblBlockEmails" runat="server" Text="Block Emails"></asp:Label>
                            </td>
                            <td align="left"  colspan = "4"  > <%--FB 1830 - DeleteEmailLang end--%>  
                            <table><tr><td>
                                <asp:CheckBox ID="ChkBlockEmails" runat="server" />
                                </td><td>
                                <asp:Button ID="BtnBlockEmails" style="display:none;" runat="server" Text="Edit" OnClick="EditBlockEmails"  class="altShortBlueButtonFormat" />
                                </td></tr></table>
                            </td> <%-- FB 2029 end --%>
                           </tr>
                           <tr> <%--FB 2268 --%> 
                             <td style="width:19%" align="left" valign="top" class="blackblodtext">
                                <asp:Label ID="lblHelpReqPhone" runat="server" Text="Help Requestor Phone"></asp:Label>
                            </td>
                            <td style="width:10%" align="left" valign="top" >
                                <asp:TextBox ID="txtHelpReqPhone" runat="server" CssClass="altText"></asp:TextBox>                                 
                            </td>
                            <td  align="left" valign="top" class="blackblodtext">
                              <asp:Label ID="lblHelpReqEmail" runat="server" Text="Help Requestor Email"></asp:Label>
                             </td>
                             <td align="left" style="width:30%" valign="top">
                                <asp:TextBox ID="txtHelpReqEmail" runat="server" CssClass="altText"></asp:TextBox> 
                                <asp:Button id="btnAddHelpReq" runat="server" Text="Add" class="altShortBlueButtonFormat" OnClientClick="javascript:return AddRemoveHelpReq('add')" />
                                 <asp:RegularExpressionValidator ID="RegtxtHelpReqEmail" ControlToValidate="txtHelpReqEmail" Display="dynamic" runat="server" 
                                    ErrorMessage="<br>Invalid email address." ValidationExpression="[a-zA-Z][\w\.-]*[a-zA-Z0-9]@[a-zA-Z0-9][\w\.-]*[a-zA-Z0-9]\.[a-zA-Z][a-zA-Z\.]*[a-zA-Z]$"></asp:RegularExpressionValidator>
                                <asp:RegularExpressionValidator ID="RegtxtHelpReqEmail_1" ControlToValidate="txtHelpReqEmail" Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<br>& < > ' + % \ /(); ? | ^= ! ` , [ ] { } : # $ ~ and &#34; are invalid characters." ValidationExpression="^(a-z|A-Z|0-9)*[^\\/<>+;?|!`,\[\]{}\x22;=:#$%&'~]*$"></asp:RegularExpressionValidator>                                    
                                <br \ /><br \ />
                                <asp:ListBox runat="server" ID="lstHelpReqEmail" CssClass="altSelectFormat" Rows="5" SelectionMode="Multiple"  onDblClick="javascript:return AddRemoveHelpReq('Rem')"  ></asp:ListBox>
                             </td>
                             
                           </tr>
                           <%--FB 2348 Start--%><%--FB 2595 Start--%>
                           <tr>
                           <%--<td style="width:20%;" align="left" class="blackblodtext">
                                <asp:Label ID="LblSecured" runat="server" Text="Require Secure"></asp:Label>
                            </td>
                            <td id="tdchkSecured" style="width:30%;" align="left" >
                                <asp:CheckBox ID="chkSecured" runat="server" />
                            </td>--%>
                             <td id="tdSurveyEmail" style="width:20%;visibility:hidden" align="left" class="blackblodtext">
                                <asp:Label ID="LblSurveyEmail" runat="server" Text="Send Survey Email"></asp:Label>
                            </td>
                            <td id="tddrpSurveyEmail" style="width:30%;visibility:hidden" align="left" >
                                <asp:DropDownList Width="20%" ID="drpSurveyEmail" runat="server" CssClass="altText" >
                                    <asp:ListItem Value="0" Selected="True" Text="No"></asp:ListItem> 
                                    <asp:ListItem  Value="1" Text="Yes"></asp:ListItem>                                  
                                </asp:DropDownList>   
                            </td>
                           </tr>
                            <%--FB 2348 End--%><%--FB 2595 End--%>
                            <%--FB 2608 Start--%>                            
                            <tr>
                             <td style="width:20%" align="left" class="blackblodtext">
                                <asp:Label ID="Label1" runat="server" Text="Enable VNOC Selection"></asp:Label>
                            </td>
                            <td style="width:30%" align="left" >
                               <asp:CheckBox ID="chkVNOC" runat="server" />
                            </td>
                             <%--FB 2724 Start--%>        
                             <td style="width:20%" align="left" class="blackblodtext">
                                <asp:Label ID="lblRFID" runat="server" Text="RFID Value"></asp:Label>
                            </td>
                            <td style="width:30%" align="left" >
                                <asp:TextBox ID="txtRFIDValue" runat="server" CssClass="altText"></asp:TextBox> 
                            </td>
                             <%--FB 2724 Start--%>        
                           </tr>                           
                           <%--FB 2608 End--%>                           
                           <tr style="display:none"><%--Audio Add On--%>
                             <td style="width:20%" align="left" class="blackblodtext">
                                <asp:Label ID="Label6" runat="server" Text="Enable Audio Add-On"></asp:Label>
                            </td>
                            <td style="width:30%" align="left" >
                                <asp:CheckBox ID="chkAudioAddon" runat="server" />
                            </td>
                            <td colspan="2"></td>
                           </tr>
                            <%--FB 1830 - End--%><%--FB 2023 end --%> 
                        <%--Code Added for License modification END--%>                        
                         <%--Code changed for RSS Feed--%>
                         <%-- FB 1985--%>
                         <%if ((Application["Client"].ToString().ToUpper().Equals("DISNEY")))
                           {%>
                         <tr>
                            <td id="tblAVExpand" runat="server" colspan="4"> <%--Disney New Requirement--%>
                                <table border="0" align="left" width="80px">
                                    <tr> 
                                        <td id="Td1"  align="left"  onmouseover="javascript:return fnShowHideAVLink('1');" onmouseout="javascript:return fnShowHideAVLink('0');" runat="server">&nbsp;
                                            <asp:LinkButton ID="LnkAVExpand" style="display:none" runat="server" Text="Expand" OnClientClick="javascript:return fnShowAVParams()"></asp:LinkButton>
                                        </td>
                                     </tr>
                                </table>
                             </td>
                            </tr>    <%} %> 
                        <tr style="display:none;">
                        <td style="width:20%" align="left" class="blackblodtext">RSS Feed</td>
                        <td style="width:30%" align="left" >
                        <asp:ImageButton ID="ImageButton1"  src="image/rss.gif"  runat="server"  OnClick="RSSFeed" ToolTip="RSS Feed" />
                        </td>
                        </tr>
                    </table>
                </td>
            </tr>   
             <%--FB 2392-Whygo Start--%>
            <tr>
                <td >
                    <table cellpadding="2" border="0" cellspacing="3" id="whygouserSettings" runat="server" width="100%" style="margin-left:-38px;position:relative">      
            <tr id="trWhygUser" runat="server" >
                <td colspan="4">
                    <table cellspacing="3" border="0">
                        <tr>     
                        <td>&nbsp;</td>                        
                            <td>
                                <span class="subtitleblueblodtext">WhyGo User Settings</span>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            </table>
            <table cellpadding="2" id="whygousersettings1" runat="server" border="0" cellspacing="3" runat="server" width="100%">      
            <tr runat="server" id="trwhygoRegion"> <%--FB 2594--%>
             <td style="width:18%" align="left" valign="top" class="blackblodtext">
                <asp:Label ID="lblParent" runat="server" Text="Parent/Reseller "></asp:Label>
            </td>
            <td style="width:35%" align="left" valign="top" >
                 <input id="txtParentReseller" onkeypress="return isNumberKey(event)" maxlength="8" runat="server" type="text" />
            </td>
            <td style="width:19%" align="left" valign="top" class="blackblodtext">
                <asp:Label ID="lblRegion" runat="server" Text="Region"></asp:Label>
             </td>
             <td align="left" valign="top" >
                <asp:DropDownList ID="lstRegion" runat="server" Width="25%" CssClass="altText"></asp:DropDownList>                                 
            </td>
           </tr>
            <tr runat="server" id="trCropUser">  <%--FB 2594--%>
             <td align="left" valign="top" class="blackblodtext">
              <asp:Label ID="lblCorp" runat="server" Text="Corp User"></asp:Label>
            </td>
            <td align="left" valign="top">
                <input id="txtCorpUser" onkeypress="return isNumberKey(event)" maxlength="8" runat="server" type="text" name="txtCorpUser" />
             </td>
            <td align="left" valign="top" class="blackblodtext">
                <asp:Label ID="lblWhyGoUsrId" runat="server" Text="Whygo UserID "></asp:Label>
            </td>
            <td  align="left" valign="top" >
                 <input id="txtESUserID" onkeypress="return isNumberKey(event)"  maxlength="8"  runat="server" type="text" />
            </td>
           </tr>
           <tr runat="server" id="trRoomBrokerNum">  <%--FB 3001 Starts--%>
                 <td align="left" valign="top" class="blackblodtext">
                  <asp:Label ID="Label7" runat="server" Text="Room Broker Phone Number"></asp:Label>
                </td>
                <td align="left" valign="top">
                <input id="txtBrokerNum" maxlength="25"  runat="server" type="text" />
                 </td>
                <td align="left" valign="top" class="blackblodtext">
                </td>
                <td  align="left" valign="top" >
                </td>
           </tr><%--FB 3001 Ends--%>
                    </table>
                </td>
            </tr>
            <%--FB 2392-Whygo End--%>
           
            <%--FB 2693 - Start--%>
            <tr id="tdPC" runat="server"> <%--ZD 100263--%>
                <td>
                    <table width="100%" border="0" style="border-collapse:collapse" >
                        <tr>
                            <td colspan="2" >
                                <span style="margin-left:-15px; overflow:visible; z-index:900; position:absolute" class="subtitleblueblodtext">PC Conferencing</span> <%-- FB 2811 --%>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <table border="0" style="margin-top:50px" >
                                    <tr>
                                        <td width="18%"><b>Enable PC Conferencing</b></td>
                                        <td width="32%"><input id="chkPcConf" type="checkbox" onclick="javascript:fnShowPcConf(this.checked)" runat="server" /> </td>
                                    </tr>
                                </table>
                            </td>
                            <td>
                            </td>
                        </tr>
                        <tr>
                            <td valign="top" width="53%">
                                <table width="100%" border="0" id="tblPcConf" runat="server" style="display:none" >
                                    <tr id="trBJ" runat="server" >
                                        <td width="32%" align="right">
                                        </td>
                                        <td width="18%" nowrap="nowrap">
                                            <input id="chkBJ" type="checkbox" runat="server" /> 
                                            <img alt="" src="../image/BlueJeans.jpg" title="Blue Jeans" />
                                            <textarea class="altText" cols="20" rows="2" id="txtAreaBlueJeans" runat="server"></textarea>
                                            <img id="expBlueJeans" src="../image/loc/nolines_plus.gif" alt="" style="width:25px; cursor:pointer" title="Options"  onclick="javascript:fnShowPcConfOpt(this,'tblBlueJeans')" />
                                            <asp:RegularExpressionValidator ID="rgVdBlueJeans" ControlToValidate="txtAreaBlueJeans" Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<br>& < and > are invalid characters." ValidationExpression="^[^<>&]*$"></asp:RegularExpressionValidator> 
                                        </td>
                                    </tr>
                                    <tr id="trJB" runat="server" >
                                        <td width="32%" align="right">
                                        </td>
                                        <td width="18%" nowrap="nowrap">
                                            <input id="chkJB" type="checkbox" runat="server" /> 
                                            <img alt="" src="../image/Jabber.jpg"  title="Jabber" />
                                            <textarea class="altText" cols="20" rows="2" id="txtAreaJabber" runat="server"></textarea>
                                            <img id="expJabber" src="../image/loc/nolines_plus.gif" alt="" style="width:25px; cursor:pointer" title="Options" onclick="javascript:fnShowPcConfOpt(this,'tblJabber')" />
                                            <asp:RegularExpressionValidator ID="rgVdJabber" ControlToValidate="txtAreaJabber" Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<br>& < and > are invalid characters." ValidationExpression="^[^<>&]*$"></asp:RegularExpressionValidator> 
                                        </td>
                                    </tr>
                                    <tr id="trLync" runat="server" >
                                        <td width="32%" align="right">
                                        </td>
                                        <td width="18%" nowrap="nowrap">
                                            <input id="chkLync" type="checkbox" runat="server" />
                                            <img alt="" src="../image/Lync.jpg"  title="Lync" />
                                            <textarea class="altText" cols="20" rows="2" id="txtAreaLync" runat="server"></textarea>
                                            <img id="expLync" src="../image/loc/nolines_plus.gif" alt="" style="width:25px; cursor:pointer" title="Options" onclick="javascript:fnShowPcConfOpt(this,'tblLync')" />
                                            <asp:RegularExpressionValidator ID="rgVdLync" ControlToValidate="txtAreaLync" Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<br>& < and > are invalid characters." ValidationExpression="^[^<>&]*$"></asp:RegularExpressionValidator> 
                                        </td>
                                    </tr>
                                    <tr id="trVidtel" runat="server" >
                                        <td width="32%" align="right">
                                        </td>
                                        <td width="18%" nowrap="nowrap">
                                            <input id="chkVidtel" type="checkbox" runat="server" /> 
                                            <img alt="" src="../image/Vidtel.jpg"  title="Vidtel" />
                                            <textarea class="altText" cols="20" rows="2" id="txtAreaVidtel" runat="server"></textarea>
                                            <img id="expVidtel" src="../image/loc/nolines_plus.gif" alt="" style="width:25px; cursor:pointer" title="Options" onclick="javascript:fnShowPcConfOpt(this,'tblVidtel')" />
                                            <asp:RegularExpressionValidator ID="rgVdVidtel" ControlToValidate="txtAreaVidtel" Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<br>& < and > are invalid characters." ValidationExpression="^[^<>&]*$"></asp:RegularExpressionValidator> 
                                        </td>
                                    </tr>
                                    <tr id="trVidyo" runat="server" style="display:none" >
                                        <td width="20%" align="right">
                                            <input id="chkVidyo" type="checkbox" runat="server" /> 
                                            <img alt="" src="../image/Vidyo.jpg" />
                                        </td>
                                        <td width="30%">
                                            <textarea class="altText" cols="20" rows="2" id="txtAreaVidyo" runat="server"></textarea>
                                            <asp:RegularExpressionValidator ID="rgVDVidyo" ControlToValidate="txtAreaVidyo" Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<br>& < and > are invalid characters." ValidationExpression="^[^<>&]*$"></asp:RegularExpressionValidator> 
                                        </td>
                                    </tr>
                                </table>
                            </td>
                            <td width="47%">
                                <table width="100%" id="tblBlueJeans" style="display:none; border:1px solid black;">
                                    <tr>
                                        <td width="20%">
                                            <img alt="" src="../image/BlueJeans.jpg" />
                                        </td>
                                        <td width="30%">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2" class="subtitleblueblodtext">Options:</td>
                                    </tr>
                                    <tr>
                                        <td class="blackblodtext" >Skype</td>
                                        <td>
                                            <input type="text" id="txtSkypeBJ" runat="server" />
                                            <asp:RegularExpressionValidator ID="rgVDSkypeBJ" ControlToValidate="txtSkypeBJ" Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<br>& < and > are invalid characters." ValidationExpression="^[^<>&]*$"></asp:RegularExpressionValidator> 
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2" class="subtitleblueblodtext">
                                            Video Conferencing Systems
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="blackblodtext" >Dial-in IP</td>
                                        <td>
                                            <input type="text" id="txtDialinIP" runat="server" />
                                            <asp:RegularExpressionValidator ID="rgExVdDialinIP" ControlToValidate="txtDialinIP" Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<br>& < and > are invalid characters." ValidationExpression="^[^<>&]*$"></asp:RegularExpressionValidator> 
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="blackblodtext" >Meeting ID</td>
                                        <td>
                                            <input type="text" id="txtMeetingVC" runat="server" />
                                            <asp:RegularExpressionValidator ID="rgExVdtxtMeetingVC" ControlToValidate="txtMeetingVC" Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<br>& < and > are invalid characters." ValidationExpression="^[^<>&]*$"></asp:RegularExpressionValidator> 
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="subtitleblueblodtext">Phone Conferencing</td>
                                    </tr>
                                    <tr>
                                        <td class="blackblodtext" >Dial-in Toll Number</td>
                                        <td>
                                            <input type="text" id="txtTollBJ" runat="server" />
                                            <asp:RegularExpressionValidator ID="rgExVdtxtTollBJ" ControlToValidate="txtTollBJ" Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<br>& < and > are invalid characters." ValidationExpression="^[^<>&]*$"></asp:RegularExpressionValidator> 
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="blackblodtext" >Dial-in Toll Free Number</td>
                                        <td>
                                            <input type="text" id="txtTollFreeBJ" runat="server" />
                                            <asp:RegularExpressionValidator ID="rgExVdtxtTollFreeBJ" ControlToValidate="txtTollFreeBJ" Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<br>& < and > are invalid characters." ValidationExpression="^[^<>&]*$"></asp:RegularExpressionValidator> 
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="blackblodtext" >Meeting ID</td>
                                        <td>
                                            <input type="text" id="txtMeetingPC" runat="server" />
                                            <asp:RegularExpressionValidator ID="rgExVdtxtMeetingPC" ControlToValidate="txtMeetingPC" Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<br>& < and > are invalid characters." ValidationExpression="^[^<>&]*$"></asp:RegularExpressionValidator> 
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2" class="subtitleblueblodtext">
                                        First time joining a Blue Jeans Video Meeting?
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="blackblodtext" >For detailed instructions:</td>
                                        <td>
                                            <input type="text" id="txtDetail" runat="server" />
                                            <asp:RegularExpressionValidator ID="rgVdDetail" ControlToValidate="txtDetail" Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<br>& < and > are invalid characters." ValidationExpression="^[^<>&]*$"></asp:RegularExpressionValidator> 
                                        </td>
                                    </tr>
                                </table>
                                <table width="100%" id="tblVidtel" style="display:none; border:1px solid black;">
                                    <tr>
                                        <td width="20%">
                                            <img alt="" src="../image/Vidtel.jpg" />
                                        </td>
                                        <td width="30%">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2" class="subtitleblueblodtext">Options:</td>
                                    </tr>
                                    <tr>
                                        <td class="blackblodtext" >Skype</td>
                                        <td>
                                            <input type="text" id="txtSkypeVidtel" runat="server" />
                                            <asp:RegularExpressionValidator ID="rgExVdtxtSkypeVidtel" ControlToValidate="txtSkypeVidtel" Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<br>& < and > are invalid characters." ValidationExpression="^[^<>&]*$"></asp:RegularExpressionValidator> 
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2" class="subtitleblueblodtext">
                                            Video Conferencing Systems
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="blackblodtext" >Dial-in SIP</td>
                                        <td>
                                            <input type="text" id="txtDialinSIP" runat="server" />
                                            <asp:RegularExpressionValidator ID="rgExVdtxtDialinSIP" ControlToValidate="txtDialinSIP" Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<br>& < and > are invalid characters." ValidationExpression="^[^<>&]*$"></asp:RegularExpressionValidator> 
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="blackblodtext" >Dial-in H.323</td>
                                        <td>
                                            <input type="text" id="txtDialinH323" runat="server" />
                                            <asp:RegularExpressionValidator ID="rgExtxtDialinH323" ControlToValidate="txtDialinH323" Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<br>& < and > are invalid characters." ValidationExpression="^[^<>&]*$"></asp:RegularExpressionValidator> 
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="blackblodtext" >
                                            PIN
                                        </td>
                                        <td>
                                            <input type="text" id="txtPinVc" runat="server" />
                                            <asp:RegularExpressionValidator ID="rgExtxtPinVc" ControlToValidate="txtPinVc" Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<br>& < and > are invalid characters." ValidationExpression="^[^<>&]*$"></asp:RegularExpressionValidator> 
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="subtitleblueblodtext">Phone Conferencing</td>
                                    </tr>
                                    <tr>
                                        <td class="blackblodtext" >Dial-in Toll Number</td>
                                        <td>
                                            <input type="text" id="txtTollVidtel" runat="server" />
                                            <asp:RegularExpressionValidator ID="rgExtxtTollVidtel" ControlToValidate="txtTollVidtel" Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<br>& < and > are invalid characters." ValidationExpression="^[^<>&]*$"></asp:RegularExpressionValidator> 
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="blackblodtext" >Dial-in Toll Free Number</td>
                                        <td>
                                            <input type="text" id="txtTollFreeVidtel" runat="server" />
                                            <asp:RegularExpressionValidator ID="rgExtxtTollFreeVidtel" ControlToValidate="txtTollFreeVidtel" Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<br>& < and > are invalid characters." ValidationExpression="^[^<>&]*$"></asp:RegularExpressionValidator> 
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="blackblodtext" >PIN</td>
                                        <td>
                                            <input type="text" id="txtPinPc" runat="server" />
                                            <asp:RegularExpressionValidator ID="rgExtxtPinPc" ControlToValidate="txtPinPc" Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<br>& < and > are invalid characters." ValidationExpression="^[^<>&]*$"></asp:RegularExpressionValidator> 
                                        </td>
                                    </tr>
                                </table>
                                <table width="100%" id="tblLync" style="display:none; border:1px solid black;">
                                    <tr>
                                        <td width="20%">
                                            <img alt="" src="../image/Lync.jpg" />
                                        </td>
                                        <td width="30%">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2" class="subtitleblueblodtext">Options:</td>
                                    </tr>
                                    <tr>
                                        <td class="blackblodtext" >Skype</td>
                                        <td>
                                            <input type="text" id="txtSkypeLync" runat="server" />
                                            <asp:RegularExpressionValidator ID="rgExtxtSkypeLync" ControlToValidate="txtSkypeLync" Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<br>& < and > are invalid characters." ValidationExpression="^[^<>&]*$"></asp:RegularExpressionValidator> 
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2" class="subtitleblueblodtext">
                                            Video Conferencing Systems
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="blackblodtext" >Dial-in IP</td>
                                        <td>
                                            <input type="text" id="txtDialinIPLync" runat="server" />
                                            <asp:RegularExpressionValidator ID="rgExtxtDialinIPLync" ControlToValidate="txtDialinIPLync" Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<br>& < and > are invalid characters." ValidationExpression="^[^<>&]*$"></asp:RegularExpressionValidator> 
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="blackblodtext" >Meeting ID</td>
                                        <td>
                                            <input type="text" id="txtMeetingVCLync" runat="server" />
                                            <asp:RegularExpressionValidator ID="rgExtxtMeetingVCLync" ControlToValidate="txtMeetingVCLync" Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<br>& < and > are invalid characters." ValidationExpression="^[^<>&]*$"></asp:RegularExpressionValidator> 
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="subtitleblueblodtext">Phone Conferencing</td>
                                    </tr>
                                    <tr>
                                        <td class="blackblodtext" >Dial-in Toll Number</td>
                                        <td>
                                            <input type="text" id="txtTollLync" runat="server" />
                                            <asp:RegularExpressionValidator ID="rgExtxtTollLync" ControlToValidate="txtTollLync" Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<br>& < and > are invalid characters." ValidationExpression="^[^<>&]*$"></asp:RegularExpressionValidator> 
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="blackblodtext" >Dial-in Toll Free Number</td>
                                        <td>
                                            <input type="text" id="txtTollFreeLync" runat="server" />
                                            <asp:RegularExpressionValidator ID="rgExtxtTollFreeLync" ControlToValidate="txtTollFreeLync" Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<br>& < and > are invalid characters." ValidationExpression="^[^<>&]*$"></asp:RegularExpressionValidator> 
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="blackblodtext" >Meeting ID</td>
                                        <td>
                                            <input type="text" id="txtMeetingPCLync" runat="server" />
                                            <asp:RegularExpressionValidator ID="rgExtxtMeetingPCLync" ControlToValidate="txtMeetingPCLync" Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<br>& < and > are invalid characters." ValidationExpression="^[^<>&]*$"></asp:RegularExpressionValidator> 
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2" class="subtitleblueblodtext">
                                        First time joining a Lync Meeting?
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="blackblodtext" >For detailed instructions:</td>
                                        <td>
                                            <input type="text" id="txtDetailLync" runat="server" />
                                            <asp:RegularExpressionValidator ID="rgExtxtDetailLync" ControlToValidate="txtDetailLync" Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<br>& < and > are invalid characters." ValidationExpression="^[^<>&]*$"></asp:RegularExpressionValidator> 
                                            
                                        </td>
                                    </tr>
                                </table>
                                <table width="100%" id="tblJabber" style="display:none; border:1px solid black;">
                                    <tr>
                                        <td width="20%">
                                            <img alt="" src="../image/Jabber.jpg" />
                                        </td>
                                        <td width="30%">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2" class="subtitleblueblodtext">Options:</td>
                                    </tr>
                                    <tr>
                                        <td class="blackblodtext" >Skype</td>
                                        <td>
                                            <input type="text" id="txtSkypeJB" runat="server" />
                                            <asp:RegularExpressionValidator ID="rgExtxtSkypeJB" ControlToValidate="txtSkypeJB" Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<br>& < and > are invalid characters." ValidationExpression="^[^<>&]*$"></asp:RegularExpressionValidator> 
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2" class="subtitleblueblodtext">
                                            Video Conferencing Systems
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="blackblodtext" >Dial-in IP</td>
                                        <td>
                                            <input type="text" id="txtDialinIPJB" runat="server" />
                                            <asp:RegularExpressionValidator ID="rgExtxtDialinIPJB" ControlToValidate="txtDialinIPJB" Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<br>& < and > are invalid characters." ValidationExpression="^[^<>&]*$"></asp:RegularExpressionValidator> 
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="blackblodtext" >Meeting ID</td>
                                        <td>
                                            <input type="text" id="txtMeetingVCJB" runat="server" />
                                            <asp:RegularExpressionValidator ID="rgExtxtMeetingVCJB" ControlToValidate="txtMeetingVCJB" Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<br>& < and > are invalid characters." ValidationExpression="^[^<>&]*$"></asp:RegularExpressionValidator> 
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="subtitleblueblodtext">Phone Conferencing</td>
                                    </tr>
                                    <tr>
                                        <td class="blackblodtext" >Dial-in Toll Number</td>
                                        <td>
                                            <input type="text" id="txtTollJB" runat="server" />
                                            <asp:RegularExpressionValidator ID="rgExtxtTollJB" ControlToValidate="txtTollJB" Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<br>& < and > are invalid characters." ValidationExpression="^[^<>&]*$"></asp:RegularExpressionValidator> 
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="blackblodtext" >Dial-in Toll Free Number</td>
                                        <td>
                                            <input type="text" id="txtTollFreeJB" runat="server" />
                                            <asp:RegularExpressionValidator ID="rgExtxtTollFreeJB" ControlToValidate="txtTollFreeJB" Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<br>& < and > are invalid characters." ValidationExpression="^[^<>&]*$"></asp:RegularExpressionValidator> 
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="blackblodtext" >Meeting ID</td>
                                        <td>
                                            <input type="text" id="txtMeetingPCJB" runat="server" />
                                            <asp:RegularExpressionValidator ID="rgExtxtMeetingPCJB" ControlToValidate="txtMeetingPCJB" Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<br>& < and > are invalid characters." ValidationExpression="^[^<>&]*$"></asp:RegularExpressionValidator> 
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2" class="subtitleblueblodtext">
                                        First time joining a Jabber Meeting?
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="blackblodtext" >For detailed instructions:</td>
                                        <td>
                                            <input type="text" id="txtDetailJB" runat="server" />
                                            <asp:RegularExpressionValidator ID="rgExtxtDetailJB" ControlToValidate="txtDetailJB" Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<br>& < and > are invalid characters." ValidationExpression="^[^<>&]*$"></asp:RegularExpressionValidator> 
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>  
            <%--FB 2693 - Ends--%> 
             <%--FB 1985 - Start--%>      
            <tr id="tdOtherSettings"  runat="server"><td align="center">
            <table style="width:100%;" border="0" cellspacing="3" cellpadding="2">  <%--FB 2611--%>
            <tr id="trLblAV" runat="server"> <%--<Code Modified For MOJ Phase2--%> 
                <td align="Left">
                    <table cellspacing="5" style="width:100%; margin-left:-50px; position:relative;"> <%--FB 2611--%>
                        <tr>
                            <td style="width:20; margin-left:-50px">&nbsp;</td>
                            <td>
                                <span class="subtitleblueblodtext">Determine Audio/Video Connection Parameters</span>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr id="AVParams" runat="server">
                <%--Window Dressing--%>
                <td align="center" class="tableBody" > 
                    <table style="width:100%;" cellpadding="2" cellspacing="3"> <%--FB 2611--%>
                        <tr>
                            <td align="left" class="blackblodtext">Endpoint Profile Name</td>
                            <td align="left">
                                <asp:TextBox ID="txtEndpointID" runat="server" Visible=false></asp:TextBox>
                                <asp:TextBox ID="txtEndpointName" runat="server" MaxLength="20" CssClass="altText"></asp:TextBox><%--FB 2523--%><%--FB 2995--%>
                                 <asp:RegularExpressionValidator ID="regConfName" ControlToValidate="txtEndpointName" Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<br>& < > + % \ / ( ) ? | ^ = ! ` [ ] { } # $ @ and ~ are invalid characters." ValidationExpression="^(a-z|A-Z|0-9)*[^\\/<>+?|!`\[\]{}\=@#$%&()~]*$"></asp:RegularExpressionValidator> <%--FB 1888--%>
<%--                                <asp:RequiredFieldValidator ID="reqEPName" runat="server" ControlToValidate="txtEndpointName" ErrorMessage="Required" Display="dynamic"></asp:RequiredFieldValidator>
--%>                            </td>
                            <td align="left" class="blackblodtext">Endpoint Password</td>
                            <td align="left">
                                <asp:TextBox ID="txtEPPassword1" style="background-image:url('../image/pwdbg.png'); background-repeat:no-repeat" runat="server" TextMode="password" CssClass="altText" onblur="EPPasswordChange(1)" onfocus=" fnEPTextFocus(this.id,1)" ></asp:TextBox>
                                 <asp:RegularExpressionValidator ID="RegularExpressionValidator9" ControlToValidate="txtEPPassword1" Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<br>& < > ' + % \ / ( ) ; ? | ^ = ! ` , [ ] { } : # $ ~ and &#34; are invalid characters." ValidationExpression="^(a-z|A-Z|0-9)*[^\\/<>+;?|!`,\[\]{}\x22;=:#$%&()'~]*$"></asp:RegularExpressionValidator><%--FB 2319--%>
                                <asp:CompareValidator ID="CmpEP1" runat="server" ControlToCompare="txtEPPassword2"
                                    ControlToValidate="txtEPPassword1" Display="Dynamic" ErrorMessage="<br>Re-enter password."></asp:CompareValidator>
                            </td>
                            <td align="left" class="blackblodtext">Confirm Password</td>
                            <td align="left"><asp:TextBox ID="txtEPPassword2" style="background-image:url('../image/pwdbg.png'); background-repeat:no-repeat" runat="server" TextMode="password" CssClass="altText" onblur="EPPasswordChange(2)" onfocus=" fnEPTextFocus(this.id,2)"></asp:TextBox>
                                <asp:CompareValidator ID="CmpEP2" runat="server" ControlToCompare="txtEPPassword1"
                                    ControlToValidate="txtEPPassword2" Display="Dynamic" ErrorMessage="<br>Your passwords do not match."></asp:CompareValidator>
                            </td>
                        </tr>
                        <tr>
                            <td align="left" class="blackblodtext">Address Type</td>
                            <td align="left">
                                <asp:DropDownList ID="lstAddressType" CssClass="altSelectFormat" DataTextField="Name" DataValueField="ID" runat="server" OnSelectedIndexChanged="ValidateTypes" AutoPostBack="true"></asp:DropDownList>
                            </td>
                            <td align="left" class="blackblodtext">Default IP/ISDN Address</td>
                            <td align="left">
                                <asp:TextBox id="txtAddress" runat="server" CssClass="altText" ></asp:TextBox>
                                <%--FB 1972--%>
                                <asp:RegularExpressionValidator ID="RegularExpressionValidator4" ControlToValidate="txtAddress" Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<br>& < > + % \ / ( ) ? | ^ = ` [ ] { } $ and ~ are invalid characters." ValidationExpression="^(a-z|A-Z|0-9)*[^\\/<>+?|`\[\]{}\=$%&()~]*$"></asp:RegularExpressionValidator> <%--FB 2267--%>
                                <%--<asp:RegularExpressionValidator ID="RegularExpressionValidator4" ControlToValidate="txtAddress" Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<br>& < > ' + % \ / ( ) ; ? | ^ = ! ` , [ ] { } : # $ @ ~ and &#34; are invalid characters." ValidationExpression="^(a-z|A-Z|0-9)*[^\\/<>+;?|!`,\[\]{}\x22;=:@#$%&()'~]*$"></asp:RegularExpressionValidator>--%>
                            </td>
                            <td align="left" class="blackblodtext">Default Equipment</td>
                            <td align="left">
                                <asp:DropDownList ID="lstVideoEquipment" CssClass="altSelectFormat" DataTextField="VideoEquipmentName" DataValueField="VideoEquipmentID" runat="server"></asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td align="left" class="blackblodtext">Default Line Rate</td>
                            <td align="left">
                                <asp:DropDownList ID="lstLineRate" CssClass="altSelectFormat" DataTextField="LineRateName" DataValueField="LineRateID" runat="server"></asp:DropDownList>
                            </td>
                            <td align="left" class="blackblodtext">Assigned MCU</td>
                            <td align="left">
                                <asp:DropDownList ID="lstBridges" CssClass="altSelectFormat" DataTextField="BridgeName" DataValueField="BridgeID" runat="server"></asp:DropDownList>
                                <asp:RequiredFieldValidator ID="reqBridges" Enabled="false" Display="dynamic" ControlToValidate="lstBridges" InitialValue="-1" ErrorMessage="Required for MPI." runat="server" SetFocusOnError="true" ></asp:RequiredFieldValidator>  
                            </td>
                            <td align="left" class="blackblodtext">Default Connection Type</td>
                            <td align="left">
                                <asp:DropDownList ID="lstConnectionType" CssClass="altSelectFormat" runat="server" DataTextField="Name" DataValueField="ID"></asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td align="left" class="blackblodtext">Web Access URL</td>
                            <td align="left">
                                <asp:TextBox ID="txtWebAccessURL" CssClass="altText" runat="server"></asp:TextBox>
                                <asp:RegularExpressionValidator ID="RegularExpressionValidator3" ControlToValidate="txtWebAccessURL" Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<br>& < > ' + % \ ( ) ; ? | ^ = ! ` , [ ] { } # $ @ ~ and &#34; are invalid characters." ValidationExpression="^(a-z|A-Z|0-9)*[^\\<>+?|!`,;\[\]{}\x22;=@#$%&()'~]*$"></asp:RegularExpressionValidator>                                
                            </td>
                            <td align="left" class="blackblodtext">Associate with MCU Address</td>
                            <td align="left">
                                <asp:TextBox ID="txtAssociateMCUAddress" CssClass="altText" runat="server"></asp:TextBox>
                                <asp:RegularExpressionValidator ID="RegularExpressionValidator10" ControlToValidate="txtAssociateMCUAddress" Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<br>& < > ' + % \ / ( ) ; ? | ^ = ` , [ ] { } : $ @ ~ and &#34; are invalid characters." ValidationExpression="^(a-z|A-Z|0-9)*[^\\/<>^+;?|`,\[\]{}\x22;=:@$%&()'~]*$"></asp:RegularExpressionValidator> <%--FB 2267--%>
                            </td>
                            <td align="left" class="blackblodtext">MCU Address Type</td>
                            <td align="left">
                                <asp:DropDownList ID="lstMCUAddressType" CssClass="altSelectFormat" DataTextField="Name" DataValueField="ID" runat="server"></asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td align="left" class="blackblodtext">Default Protocol</td>
                            <td align="left">
                                <asp:DropDownList CssClass="altSelectFormat" ID="lstProtocol" runat="server" DataTextField="Name" DataValueField="ID"></asp:DropDownList>
                            </td>
                            <td align="left" class="blackblodtext">Outside Network</td>
                            <td align="left">
                                <!--[Vivek 29th Apr 2008] Defaulted Outside Network to No as per Issue No: 299-->
                            <%--Window Dressing--%>
                                <asp:DropDownList ID="lstIsOutsideNetwork" CssClass="altText" runat="server">
                                    <asp:ListItem Value="1" Text="Yes"></asp:ListItem>                                    
                                    <asp:ListItem Selected="True" Value="0" Text="No"></asp:ListItem>
                                </asp:DropDownList>
                            </td>
                            <td align="left" class="blackblodtext">Encryption Preferred</td>
                            <td align="left">
                                <asp:DropDownList ID="lstEncryption" CssClass="altText" runat="server">
                                    <asp:ListItem Selected="True" Value="1" Text="Yes"></asp:ListItem>
                                    <asp:ListItem Value="0" Text="No"></asp:ListItem>
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td align="left" class="blackblodtext">
                                Email ID
                            </td>
                            <%-- ICAL Cisco Telepresence fix--%>
                            <td align="left">
                                <asp:TextBox CssClass="altText" ID="txtExchangeID" runat="server" Width="230px" TextMode="SingleLine"></asp:TextBox>
                                <asp:RegularExpressionValidator ID="RegularExpressionValidator2" ControlToValidate="txtExchangeID"
                                    ValidationGroup="Submit" Display="dynamic" runat="server" SetFocusOnError="true"
                                    ErrorMessage="<br>& < > ' + % \ ( ) ; ? | ^ = ! ` , [ ] { } # $ ~ and &#34; are invalid characters."
                                    ValidationExpression="^(a-z|A-Z|0-9)*[^\\<>+;?|!`,\[\]{}\x22;=^#$%&()'~]*$"></asp:RegularExpressionValidator>
                            </td>
                            <%--API Port Starts--%>
                            <td align="left" class="blackblodtext">
                                API Port
                            </td>
                            <td align="left" nowrap>
                                <asp:TextBox ID="txtApiportno" CssClass="altText" onblur="javascript:return IsNumeric()" MaxLength="5" runat="server"></asp:TextBox>
                                <label id="lblapierror" style="display:none;font-weight:normal" class="lblError" />
                            </td>
                            <%-- FB 1642 Audio add on- Starts --%>
                            <td align="left" class="blackblodtext">
                                Conference Code
                            </td>
                            <td align="left" >
                                <asp:TextBox CssClass="altText" ID="txtConfCode" runat="server"></asp:TextBox> 
                            </td> 
                            <%-- FB 1642 Audio add on- End --%>                     
                            <%--API Port Ends--%>
                        </tr>
                        <%-- FB 1642 Audio add on- Starts --%> 
                        <tr>
                            <td align="left" class="blackblodtext">
                                Leader Pin
                            </td>
                            <td align="left" >
                                <asp:TextBox CssClass="altText" ID="txtLeaderPin" runat="server"></asp:TextBox>                               
                            </td> 
                            <%--FB 2227--%>
                            <td align="left" class="blackblodtext">
                                Internal Video Bridge Number <%--FB 2611--%>
                            </td>
                            <td align="left" >
                                <asp:TextBox CssClass="altText" ID="txtIntvideonum" MaxLength="25" runat="server"></asp:TextBox>                               
                            </td>
                            <td align="left" class="blackblodtext">
                                External Video Bridge Number <%--FB 2611--%>
                            </td>
                            <td align="left" >
                                <asp:TextBox CssClass="altText" ID="txtExtvideonum" runat="server"></asp:TextBox>                               
                            </td>
                            <%--FB 2227--%>
                        </tr>
                       
                       <%--FB 2262 //FB 2599--%>
                        <tr>
                            <%--<td align="right" class="blackblodtext"><%--FB 2481--%>
                                <%--Private VMR--%>
                            <%--</td>--%>
                            <%--<td align="left">
                                <asp:Button ID="btnPrivateVMR1" runat="server" Text="Manage" class="altLongBlueButtonFormat" /> 
                            </td>--%>
                             <td align="left" class="blackblodtext">
                                External VMR
                            </td>
                            <td align="left" >
                                <asp:Button ID="btnPrivateVMR" runat="server" Text="Manage" class="altLongBlueButtonFormat" /> 
                            </td>
                            <td id="tdMeetlink" runat="server" visible="false" align="left" class="blackblodtext"> <%--FB 2834--%>
                                Vidyo Meeting Link 
                            </td>
                            <td align="left" >
                                <asp:TextBox Visible="false" CssClass="altText" ID="txtVidyoURL" runat="server"></asp:TextBox>                               
                            </td> 
                            <td id="tdExten" Visible="false" runat="server" align="left" class="blackblodtext">
                                Extension
                            </td>
                            <td align="left" >
                                <asp:TextBox Visible="false" CssClass="altText" ID="txtExtension" runat="server" MaxLength="50" ></asp:TextBox>                               
                            </td> 
                            
                        </tr>
                        <tr id="trpin" runat="server" visible="false">
                            <td align="left" class="blackblodtext">
                               Pin
                            </td>
                            <td align="left" >
                                <asp:TextBox CssClass="altText" ID="txtPin" MaxLength="25" runat="server"></asp:TextBox>  
                                <asp:CompareValidator ID="cmpPin" runat="server" ControlToCompare="txtConfirmPin"
                                   ControlToValidate="txtPin" Display="Dynamic" ErrorMessage="<br>Re-enter pin."></asp:CompareValidator>
                            </td>
                            <td align="left" class="blackblodtext">
                                Confirm Pin
                            </td>
                            <td align="left" >
                                <asp:TextBox CssClass="altText" ID="txtConfirmPin" runat="server" MaxLength="25"></asp:TextBox>   
                                <asp:CompareValidator ID="cmpConfirmPin" runat="server" ControlToCompare="txtPin"
                                    ControlToValidate="txtConfirmPin" Display="Dynamic" ErrorMessage="<br>Pin do not match."></asp:CompareValidator>
                            </td>
                            <td colspan="2"></td>
                         </tr>
                        <%--FB 2262 //FB 2599--%>
						 <%--FB 2481 Start--%>
                        <%--<tr>
                            <%--<td align="right" class="blackblodtext">
                                External VMR
                            </td>
                            <td align="left" >
                                <asp:Button ID="btnPrivateVMR" runat="server" Text="Manage" class="altLongBlueButtonFormat" /> 
                            </td>
                            <td colspan="4"></td>                           
                        </tr>--%>
                        
                        <tr>
                        <td colspan="4">
                            <ajax:ModalPopupExtender ID="PrivateVMRPopup" runat="server" TargetControlID="btnPrivateVMR"
                                PopupControlID="PopupPrivateVMRPanel" DropShadow="false" Drag="true" BackgroundCssClass="modalBackground"
                                CancelControlID="ClosePUp" BehaviorID="btnPrivateVMR">
                            </ajax:ModalPopupExtender>
                            <asp:Panel ID="PopupPrivateVMRPanel" runat="server" Width="60%" Height="60%" HorizontalAlign="Center"
                                CssClass="treeSelectedNode" ScrollBars="Vertical">
                                <table align="center" cellpadding="3" cellspacing="0" width="98%" style="border-collapse: collapse;height: 50%;">
                                    <tr>
                                        <td align="center">
                                            <table width="100%" border="0" cellpadding="3" style="border-collapse: collapse;height: 100%;">
                                                <tr>
                                                    <td colspan="6">
                                                        <h3>Private VMR Code</h3>
                                                    </td>
                                                </tr>
                                                <tr align="center" style="height: 50%">
                                                <td  align="center" colspan="6" height="21" align="left" valign="top" class="blackblodtext">
                                                    <dxHE:ASPxHtmlEditor ID="PrivateVMR" runat="server" Height="200px">
                                                        <SettingsImageUpload UploadImageFolder="~/image/maillogo/">
                                                            <ValidationSettings MaxFileSize="100000" MaxFileSizeErrorText="Footer Image attachment is greater than 100KB. File has not been uploaded" />
                                                        </SettingsImageUpload>
                                                    </dxHE:ASPxHtmlEditor>
                                                    <input type="file" id="fmMap" contenteditable="false" size="50" class="altText" runat="server"
                                                        visible="false" />
                                                    <input type="hidden" id="fmMapImage" name="Map1ImageDt" runat="server" height="21%"
                                                        style="display: none" /><%--FB 1982 --%>
                                                 </td>
                                                 </tr>
                                                <tr align="center" style="height: 50%">
                                                    <td align="center" colspan="6">
                                                    <br />
                                                    <input align="middle" type="button" runat="server" id="ClosePUp"
                                                        value=" Close " class="altMedium0BlueButtonFormat" onserverclick="fnPrivateVMRCancel"/>
                                                    <asp:Button ID="btnPrivateVMRSubmit" runat="server" Text="Submit" 
                                                        OnClick="fnPrivateVMRSubmit"  Width="100pt" /> <%--FB 2796--%>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </asp:Panel>
                        </td>
                        </tr>
                        <%--FB 2481 End--%>
                        <%-- FB 1642 Audio add on- End --%> 
                    </table>
                </td>
            </tr>
            <%--Tickers Start--%>
            <tr> 
                <td align="Left">
                    <table cellspacing="5" style="width:100%; margin-left:-30px; position:relative"> <%--FB 2611--%>
                        <tr>
                            <%--<td style="width:20">&nbsp;</td>--%>
                            <td>
                                <span class="subtitleblueblodtext">Ticker Settings 1</span>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr id="Tr2" runat="server">
                <td align="center" class="tableBody"> 
                    <table style="width:100%" cellpadding="2" cellspacing="3"><%--FB 2611--%>
                        <tr>
                            <td align="left" class="blackblodtext">Ticker Status</td>
                             <td align="left">
                                <asp:DropDownList ID="drpTickerStatus" CssClass="altText" runat="server">
                                    <asp:ListItem  Value="0" Text="Show"></asp:ListItem>                                    
                                    <asp:ListItem  Selected="True" Value="1" Text="Hide"></asp:ListItem>
                                </asp:DropDownList>
                            </td>
                            <td align="left" class="blackblodtext">Ticker Position</td>
                            <td align="left">
                             <asp:DropDownList ID="drpTickerPosition" CssClass="altText" runat="server">
                                    <asp:ListItem Selected="True" Value="0" Text="Top"></asp:ListItem>                                    
                                    <asp:ListItem  Value="1" Text="Bottom"></asp:ListItem>
                                </asp:DropDownList>
                            </td>
                            <td align="left" class="blackblodtext">Ticker Speed</td>
                            <td align="left">
                            <asp:DropDownList ID="drpTickerSpeed" CssClass="altText" runat="server">
                                    <asp:ListItem  Value="3" Text="Slow"></asp:ListItem>                                    
                                    <asp:ListItem   Value="6" Text="Medium"></asp:ListItem>
                                    <asp:ListItem  Value="18" Text="Fast"></asp:ListItem>
                            </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td align="left" class="blackblodtext">Ticker Background Color</td>
                            <td align="left">
                                <asp:TextBox ID="txtTickerBknd" CssClass="altText" runat="server" EnableViewState="True"></asp:TextBox>
                                <IMG title="Click to select the color" onclick="show_RGBPalette('txtTickerBknd',event)"
					                height="23" alt="Color" src="../Image/color.jpg" width="27" name="imggen" style="vertical-align:middle">
					               
			                </td>
                             <td align="left" class="blackblodtext">Ticker Display</td>
                            <td align="left">
                               <asp:DropDownList ID="drpTickerDisplay" CssClass="altText" runat="server" onclick="javascript:fnShowFeed();">
                                    <asp:ListItem  Selected="True"  Value="0" Text="My Conferences"></asp:ListItem>                                    
                                    <asp:ListItem  Value="1" Text="RSS feed"></asp:ListItem>
                            </asp:DropDownList>
                            </td>
                            <td align="left" class="blackblodtext" id="feedLink" runat="server" style="display:none;">RSS Feed link</td>
                            <td align="left">
                                <asp:TextBox ID="txtFeedLink" CssClass="altText" runat="server"></asp:TextBox>
                                
                                
                            </td>
                          </tr>
                           <tr>
                          <td colspan="6" align="left">
                          <asp:RegularExpressionValidator ID="RegularExpressionValidator6" ControlToValidate="txtFeedLink" Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<br>& < > ' + %  | = ! ` , [ ] { }  # $ ~ and &#34; are invalid characters." ValidationExpression="^(a-z|A-Z|0-9)*[^<>+|!`,[\]{};=#$%&~]*$"></asp:RegularExpressionValidator></td>
                          </tr>
                    </table>
                </td>
            </tr>
            <tr> 
                <td align="left">
                    <table cellspacing="5" style="width:100%; margin-left:-30px; position:relative"><%--FB 2611--%>
                        <tr>
                            <%--<td style="width:20">&nbsp;</td>--%>
                            <td>
                                <span class="subtitleblueblodtext">Ticker Settings 2</span>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr id="Tr1" runat="server">
                <td align="center" class="tableBody"> 
                    <table style="width:100%" cellpadding="2" cellspacing="3" border="0"> <%--FB 2611--%>
                        <tr>
                            <td align="left" class="blackblodtext">Ticker Status</td>
                             <td align="left">
                                <asp:DropDownList ID="drpTickerStatus1" CssClass="altText" runat="server">
                                    <asp:ListItem  Value="0" Text="Show"></asp:ListItem>                                    
                                    <asp:ListItem  Selected="True" Value="1" Text="Hide"></asp:ListItem>
                                </asp:DropDownList>
                            </td>
                            <td align="left" class="blackblodtext">Ticker Position</td>
                            <td align="left">
                             <asp:DropDownList ID="drpTickerPosition1" CssClass="altText" runat="server">
                                    <asp:ListItem Selected="True" Value="0" Text="Top"></asp:ListItem>                                    
                                    <asp:ListItem  Value="1" Text="Bottom"></asp:ListItem>
                                </asp:DropDownList>
                            </td>
                            <td align="left" class="blackblodtext">Ticker Speed</td>
                            <td align="left">
                            <asp:DropDownList ID="drpTickerSpeed1" CssClass="altText" runat="server">
                                    <asp:ListItem  Value="3" Text="Slow"></asp:ListItem>                                    
                                    <asp:ListItem  Selected="True" Value="6" Text="Medium"></asp:ListItem>
                                    <asp:ListItem  Value="18" Text="Fast"></asp:ListItem>
                            </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td align="left" class="blackblodtext">Ticker Background Color</td>
                            
                            <td align="left">
                                <asp:TextBox ID="txtTickerBknd1" CssClass="altText" runat="server" EnableViewState="True"></asp:TextBox>
                                <IMG title="Click to select the color" onclick="show_RGBPalette('txtTickerBknd1',event)"
					                height="23" alt="Color" src="../Image/color.jpg" width="27" name="imggen" style="vertical-align:middle">
					                
			                </td>
                             <td align="left" class="blackblodtext">Ticker Display</td>
                            <td align="left">
                               <asp:DropDownList ID="drpTickerDisplay1" CssClass="altText" runat="server" onclick="javascript:fnShowFeed();">
                                    <asp:ListItem  Selected="True"  Value="0" Text="My Conferences"></asp:ListItem>                                    
                                    <asp:ListItem  Value="1" Text="RSS feed"></asp:ListItem>
                            </asp:DropDownList>
                            </td>
                            <td align="left" class="blackblodtext" id="feedLink1" runat="server">RSS Feed link</td>
                            <td align="left">
                                <asp:TextBox ID="txtFeedLink1" CssClass="altText" runat="server" style="display:none;"></asp:TextBox>
                                
                                
                            </td>
                          </tr>
                          <tr>
                          <td colspan="6" align="left">
                          &nbsp;&nbsp;&nbsp;<asp:RegularExpressionValidator ID="RegularExpressionValidator5" ControlToValidate="txtFeedLink1" Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<br>& < > ' + %  | = ! ` , [ ] { }  # $ ~ and &#34; are invalid characters." ValidationExpression="^(a-z|A-Z|0-9)*[^<>+|!`,[\]{};=#$%&~]*$"></asp:RegularExpressionValidator></td>
                          </tr>
                    </table>
                </td>
            </tr>
             <%--Tickers End--%>
             </table></td></tr> <%--FB 1985 - End--%>
            <tr>
                <td style="height:30">&nbsp;</td>
            </tr>            
            <tr>
                <td align="center">
                    <table style="width:100%" border="0" ><%--FB 2611--%>
                        <tr>
                            <td style="width:15%" align="center" id="tdreset" runat="server"><%--FB 2611--%><%--FB 2565 Start--%>
                                <asp:Button runat="server" ID="btnReset" Text="Reset" ValidationGroup="Reset" OnClick="BindData" CssClass="altLongBlueButtonFormat" OnClientClick="DataLoading('1')"/>  <%--OnClientClick="javascript:testConnection();return false;"--%> <%--ZD 100176--%>                                
                            </td>
                            <td style="width:30%;" align="center" id="tdcancel" runat="server"><%--FB 2679--%>
                            <input type="button" id="btnCanel"  runat="server" class="altLongBlueButtonFormat" value="Cancel" onclick="fnCancel()" OnClientClick="DataLoading('1')"/><%--ZD 100176--%>  
                            </td>
                            <%-- FB 2025 starts --%>
                            <td style="width:30%;" align="center" id="tdnewsubmit1" runat="server"> <%--FB 2611--%><%--FB 2679--%>
                            <asp:Button runat ="server" ID="btnnewSubmit" OnClientClick ="javascript:return RulePassword()" OnClick ="SubmitNewUser"
                                 Width="150pt"  Text ="Submit / Add New User"/><%--FB 2339 FB 2796--%>
                            </td><%-- FB 2025 end --%>
                            <td style="width:20%" align="center" id="tdsubmit" runat="server"> <%--FB 2611--%>
                                <asp:Button ID="btnSubmit" OnClientClick="javascript:return RulePassword()"  OnClick="SubmitUser"
                                  Width="150pt"  runat="server"  Text="Submit" /><%--API Port--%> <%--FB 2339 FB 2796--%>
                            </td><%--FB 2565 End--%>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </div>
<script language="javascript">
    SavePassword();
//FB 1830 - DeleteEmailLang start
    function fnDelEmailLan() {
        if (document.getElementById("txtEmailLang").value == "")
            return false;
        else
            return true;
    }
//FB 1830 - DeleteEmailLang end

    //FB 1985 - Start
    function fnShowHideAVLink()
	{
	  
	    var args = fnShowHideAVLink.arguments;
	    var obj = eval(document.getElementById("LnkAVExpand"));
	  
	    if(obj)
	    {
	       
	        obj.style.display = 'none';
	        if(args[0] == '1')
	        {
	            obj.style.display = '';
	        }
	        
	    }
	}
	
	function fnShowAVParams()
	{
	    //var obj = eval(document.getElementById("tdrssFeed"));
	    var obj1 = eval(document.getElementById("tdOtherSettings"));
	    var linkState = eval(document.getElementById("hdnAVParamState"));
	    var expandlink = eval(document.getElementById("LnkAVExpand"));
	    
	    if(linkState)
	    {
	        //if(obj && obj1)
	        if(obj1)
	        {
	            //obj.style.display = 'none';
	            obj1.style.display = 'none';
	            if(linkState.value == '')
	            {
	                //obj.style.display = '';
	                obj1.style.display = '';
	                linkState.value = '1';
	                
	                if(expandlink)
	                {
	                    expandlink.innerHTML = 'Collapse';
	                }
	            }
	            else
	            {
	                //obj.style.display = 'none';
	                obj1.style.display = 'none';
	                linkState.value = '';
	                if(expandlink)
	                {
	                    expandlink.innerHTML = 'Expand';
	                }
	            }
	        }
	     }
	     return false;
	}
	//FB 1985 - End

	function AddRemoveHelpReq(opr)//FB 2268
	{
	    var lstHelpReq = document.getElementById("lstHelpReqEmail");
	    var txtHelpReq = document.getElementById("txtHelpReqEmail");
	    var hdnHelpReq = document.getElementById("hdnHelpReq");

	    if (!Page_ClientValidate())
	        return Page_IsValid;
	    
	    if (opr == "Rem")
	    {
	        var i;
	        for (i = lstHelpReq.options.length - 1; i >= 0; i--) {
	            if (lstHelpReq.options[i].selected)
	            {
	                hdnHelpReq.value = hdnHelpReq.value.replace(lstHelpReq.options[i].text, "").replace(/��/i, "�");
	                lstHelpReq.remove(i);
	            }
	        }
	    }
	    else if (opr == "add")
	    {
	        if (txtHelpReq.value.replace(/\s/g, "") == "") //trim the textbox
	            return false;
	            
	        if (lstHelpReq.options.length >= 5) {
	            document.getElementById("errLabel").innerHTML = "Maximum 5 Emails";
	            document.getElementById("errLabel").className = "lblError";//FB 2487
	            document.getElementById("errLabel").focus();
	            return false;
	        }
	        else {
	            if (hdnHelpReq.value.indexOf(txtHelpReq.value) >= 0) 
	            {
	                document.getElementById("errLabel").innerHTML = "Already Added Email address";
	                document.getElementById("errLabel").className = "lblError";//FB 2487
	                return false;
	            }
	        }
	        
	        if (lstHelpReq.options.length > 0)
	            hdnHelpReq.value = hdnHelpReq.value + "�";

	        var option = document.createElement("Option");
	        option.text = txtHelpReq.value;
	        option.title = txtHelpReq.value;
	        lstHelpReq.add(option);
	        hdnHelpReq.value = hdnHelpReq.value + txtHelpReq.value;
	        
	        txtHelpReq.value = "";
	        txtHelpReq.focus();
	    }

	    return false;
	}
	
	//FB 2348 Start
    if("<%=Session["EnableSurvey"]%>" == "1")
    {
        document.getElementById("tdSurveyEmail").style.visibility = "visible";
        document.getElementById("tddrpSurveyEmail").style.visibility = "visible";
    }
	//FB 2348 End
	//FB 2481 Start
	if(document.getElementById("dxHTMLEditor_TD_T0_DXI15_Img"))
	       document.getElementById("dxHTMLEditor_TD_T0_DXI15_Img").style.display = "none";
	//FB 2481 End
</script>
    </form>
<%--code added for Soft Edge button--%>
<script type="text/javascript" src="inc/softedge.js"></script>
<!-- FB 2719 Starts -->
<% if (Session["isExpressUser"].ToString() == "1"){%>
<!-- #INCLUDE FILE="inc/mainbottomNETExp.aspx" -->
<%}else{%>
<!-- #INCLUDE FILE="inc/mainbottomNET.aspx" -->
<%}%>
<!-- FB 2719 Ends -->
</body>
</html>
<%--FB 2491 Start--%>
<script type="text/javascript">
    // FB 2693 Starts
    function fnIEuiAlign() {
        var agent = navigator.userAgent;
        if (agent.indexOf("Trident/6.0") != -1) {
            // IE 10
            document.getElementById("chkPcConf").style.marginLeft = "2px";
            document.getElementById("tblPcConf").style.marginLeft = "-22px";
        }
        else if (agent.indexOf("MSIE") != -1) {
            // IE 6 7 8 9
            document.getElementById("chkPcConf").style.marginLeft = "-2px";
            document.getElementById("tblPcConf").style.marginLeft = "-6px";
        }
    }
    // FB 2693 Ends
    
function fnScrollTop() 
    {
       fnIEuiAlign(); // FB 2693
       window.scrollTo(0, 0);
       document.getElementById('hideScreen').style.display = "none";
    }
    setTimeout("fnScrollTop()", 1);
    //FB 2982 start
    var isIE = navigator.appVersion.indexOf("MSIE 8.0");
    var LstTimeZone = document.getElementById("lstTimeZone");
    
    function SetWidthToAuto(LstTimeZone) {
        if (isIE > -1) {
            LstTimeZone.style.width = 'auto';
        }
    }
    function ResetWidth(lstTimeZone) {
        if (isIE > -1) {
            LstTimeZone.style.width = '172px';
        }
    }
    //FB 2982 End

    // ZD 100263
    var obj1 = document.getElementById("txtPassword1");
    var obj2 = document.getElementById("txtPassword2");

    if ((obj1.value != "" && obj2.value != "") || '<%=Session["ModifiedUserID"]%>' == 'new') {
        document.getElementById("txtPassword1").style.backgroundImage = "";
        document.getElementById("txtPassword2").style.backgroundImage = "";
    }

</script>
<%--FB 2491 End--%>