<%--ZD 100147 start--%>
<%--/* Copyright (C) 2013 myVRM - All Rights Reserved	
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.	
*
* You should have received a copy of the myVRM license with	
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/--%><%--ZD 100147 End--%>
<%@ Page Language="C#" Inherits="ns_UserTemplates.UserTemplates" Buffer="true" %>
<%--FB 2779 Starts--%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<meta http-equiv="X-UA-Compatible" content="IE=8" />
<!-- #INCLUDE FILE="inc/maintopNET.aspx" -->
<%--FB 2779 Ends--%>

    <script type="text/javascript" src="inc/functions.js"></script>
<script language="javascript">
function CheckName()
{
}
</script>
<head runat="server">
    <title>Manage User Templates</title>

</head>
<body>
    <form id="frmInventoryManagement" runat="server" method="post" onsubmit="return true">
      <input type="hidden" id="helpPage" value="105">

    <div>
        <table width="100%">
            <tr>
                <td align="center">
                    <h3>
                        Manage User Templates
                    </h3>
                </td>
            </tr>
            <tr>
                <td align="center">
                    <asp:Label ID="errLabel" runat="server" Text="" CssClass="lblError"></asp:Label>
                    <div id="dataLoadingDIV" style="z-index:1"></div>
                </td>
            </tr>
            <tr>
                <td align="Left">
                    <table cellspacing="5">
                        <tr>
                            <td >&nbsp;</td>
                            <td>
                                <SPAN class=subtitleblueblodtext>Existing User Templates</SPAN>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td align="center">
                    <asp:DataGrid ID="dgUserTemplates" runat="server" AutoGenerateColumns="False" CellPadding="4" GridLines="None"
                        BorderColor="blue" BorderStyle="none" BorderWidth="0" ShowFooter="true" OnItemCreated="BindRowsDeleteMessage"
                        OnDeleteCommand="DeleteUserTempate" OnEditCommand="EditUserTemplate" Width="90%" Visible="true" >
                        <AlternatingItemStyle CssClass="tableBody" />
                        <ItemStyle CssClass="tableBody" HorizontalAlign="left" />
                        <HeaderStyle CssClass="tableHeader" HorizontalAlign="left" />
                        <Columns>
                            <asp:BoundColumn DataField="ID" Visible="false" ><HeaderStyle CssClass="tableHeader" /></asp:BoundColumn>
                            <asp:BoundColumn DataField="Name" HeaderText="Template Name"><HeaderStyle CssClass="tableHeader" /></asp:BoundColumn>
                            <asp:BoundColumn DataField="RoleName" HeaderText="Role Name"><HeaderStyle CssClass="tableHeader" /></asp:BoundColumn>
                            <asp:TemplateColumn HeaderText="Actions" ItemStyle-Width="15%">
                                <HeaderStyle CssClass="tableHeader" />
                                <ItemTemplate>
                                    <asp:LinkButton runat="server" Text="Edit" OnClientClick="DataLoading(1)" ID="btnEdit" CommandName="Edit"></asp:LinkButton>
                                    &nbsp;<asp:LinkButton runat="server" Text="Delete" ID="btnDelete" CommandName="Delete"></asp:LinkButton>
                                </ItemTemplate>
                                <FooterTemplate>
                                    <span class="blackblodtext">Total Templates:</sapn> <asp:Label ID="lblTotalRecords" runat="server" Text=""></asp:Label><%--FB 2579--%>
                                </FooterTemplate>
                            </asp:TemplateColumn>
                        </Columns>
                    </asp:DataGrid>
                    <asp:Table runat="server" ID="tblNoUserTemplates" Visible="false" Width="90%">
                        <asp:TableRow CssClass="lblError">
                            <asp:TableCell CssClass="lblError">
                                No User Templates found.
                            </asp:TableCell>
                        </asp:TableRow>
                    </asp:Table>                    
                </td>
            </tr>
            <tr>
                <td align="Left">
                    <table cellspacing="5">
                        <tr>
                            <td>&nbsp;</td>
                            <td>
                                <SPAN class=subtitleblueblodtext>Create New Template</SPAN>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td align="center">
                    <table width="90%"><tr><td align="right">
                        <asp:Button ID="btnCreateNew" OnClick="CreateNewTemplate" Width="150pt"  Text="Submit" runat="server" /> <%--FB 2796--%>
                    </td></tr></table>
                </td>
            </tr>
        </table>
    </div>

<img src="keepalive.asp" name="myPic" width="1px" height="1px">
    </form>
<%--code added for Soft Edge button--%>
<script type="text/javascript" src="inc/softedge.js"></script>
    <!-- #INCLUDE FILE="inc/mainbottomNET.aspx" -->
</body>
</html>

