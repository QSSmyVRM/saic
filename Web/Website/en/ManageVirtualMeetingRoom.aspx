﻿<%--ZD 100147 start--%>
<%--/* Copyright (C) 2013 myVRM - All Rights Reserved	
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.	
*
* You should have received a copy of the myVRM license with	
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/--%><%--ZD 100147 End--%>
<%@ Page Language="C#" AutoEventWireup="true" Inherits="ns_MyVRM.en_ManageVirtualMeetingRoom" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<meta http-equiv="X-UA-Compatible" content="IE=8" />
<!-- #INCLUDE FILE="inc/maintopNET.aspx" -->


<script type="text/javascript">

    function getYourOwnEmailList(i) {
        if (i == -2) {
            if (queryField("sb") > 0)
                url = "emaillist2.aspx?t=e&frm=roomassist&wintype=ifr&fn=frmVirtualMettingRoom&n=";
            else
                url = "emaillist2main.aspx?t=e&frm=roomassist&fn=frmVirtualMettingRoom&n=";
        }
        else {
            url = "emaillist2main.aspx?t=e&frm=approver&fn=frmVirtualMettingRoom&n=" + i;
        }
        if (!window.winrtc) {	// has not yet been defined
            winrtc = window.open(url, "", "width=920,height=470,top=0,left=0,resizable=yes,scrollbars=yes,status=no"); //FB 2735
            winrtc.focus();
        } else // has been defined
            if (!winrtc.closed) {     // still open
            winrtc.close();
            winrtc = window.open(url, "", "width=920,height=470,top=0,left=0,resizable=yes,scrollbars=yes,status=no"); //FB 2735
            winrtc.focus();
        } else {
        winrtc = window.open(url, "", "width=920,height=470,top=0,left=0,resizable=yes,scrollbars=yes,status=no"); //FB 2735
            winrtc.focus();
        }
    }


    function deleteAssistant() {//FB 2448
        eval("document.frmVirtualMettingRoom.AssistantID").value = "";
        eval("document.frmVirtualMettingRoom.Assistant").value = "";
    }

    function fnClose() {
        window.location.replace("manageroom.aspx?hf=&m=&pub=&d=&comp=&f=&frm=");
        return true;
    }

    function frmMainroom_Validator() {

        if (!Page_ClientValidate())
            return Page_IsValid;

        var txtroomname = document.getElementById('<%=txtRoomName.ClientID%>');
        if (txtroomname.value == "") {
            reqName.style.display = 'block';
            txtroomname.focus();
            return false;
        }
        else if (txtroomname.value.search(/^(a-z|A-Z|0-9)*[^\\<>^+?|!`\[\]{}\=@$%&~]*$/) == -1) {
            regRoomName.style.display = 'block';
            txtroomname.focus();
            return false;
        }

        var hdnmultipledept = document.getElementById('<%=hdnMultipleDept.ClientID%>');
        var departmentlist = document.getElementById('<%=DepartmentList.ClientID%>');

        if (hdnmultipledept.value == 1) {
            if ((departmentlist.value == "") && (departmentlist.length > 0)) {
                isConfirm = confirm("Are you sure you want to set up this room with no department(s) assigned?\n")
                if (isConfirm == false) {
                    return (false);
                }
            }
        }

        return (true);
    }
</script>

<script type="text/javascript" src="inc/functions.js"></script>
<script type="text/javascript" src="script/errorList.js"></script>
<script type="text/javascript" src="extract.js"></script>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Manage Virtual Meeting Room</title>
    <link rel="StyleSheet" href="css/divtable.css" type="text/css" />
    <link rel="stylesheet" title="Expedite base styles" type="text/css" href="css/border-table.css" />
    <link rel="stylesheet" title="Expedite base styles" type="text/css" href="css/main-table.css" />
    <%--<link rel="stylesheet" title="Expedite base styles" type="text/css" href="css/main.css" />--%>
</head>
<body>
    <form id="frmVirtualMettingRoom" runat="server">
    <input type="hidden" id="hdnMultipleDept" runat="server" />
    <input type="hidden" id="AssistantID" runat="server" />
    <input type="hidden" id="hdnRoomID" runat="server" />
    <input type="hidden" id="AssistantName" runat="server" />
    
    <div>
        <center>
            <table border="0" width="100%" cellpadding="2" cellspacing="2">
                <tr>
                    <td align="center">
                        <h3>
                            <asp:Label ID="lblTitle" runat="server"></asp:Label></h3> <%--FB 2994--%>
                        <br />
                        <asp:Label ID="errLabel" runat="server" CssClass="lblError"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td align="left">
                        <table id="Table4" cellpadding="2" cellspacing="2" border="0" style="width: 100%">
                            <tr align="left">
                                <td style="width: 10%" align="left" valign="top" class="blackblodtext" nowrap>
                                    Last Modified by :
                                </td>
                                <td style="width: 85%" align="left" valign="top">
                                    <asp:Label ID="lblMUser" runat="server" CssClass="active"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 10%" align="left" valign="top" class="blackblodtext" nowrap>
                                    Last Modified at :
                                </td>
                                <td style="width: 85%" align="left" valign="top">
                                    <asp:Label ID="lblMdate" runat="server" CssClass="active"></asp:Label>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td class="subtitleblueblodtext" align="left">
                        Basic Configuration
                    </td>
                </tr>
                <tr>
                    <td>
                        <table border="0" width="100%" cellpadding="2" cellspacing="2" style="margin-left:20px">
                            <tr>
                                <td style="width: 15%" align="left" valign="top" class="blackblodtext">
                                    Room Name <span class="reqfldText">*</span>
                                </td>
                                <td style="width: 35%" align="left" valign="top">
                                    <asp:TextBox ID="txtRoomName" runat="server" CssClass="altText"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="reqName" runat="server" ControlToValidate="txtRoomName"
                                        Display="dynamic" CssClass="lblError" SetFocusOnError="true" Text="Required"
                                        ValidationGroup="Submit"></asp:RequiredFieldValidator>
                                    <asp:RegularExpressionValidator ID="regRoomName" ControlToValidate="txtRoomName"
                                        Display="dynamic" runat="server" ValidationGroup="Submit" SetFocusOnError="true"
                                        ErrorMessage="<br> & < > + % \ ? | ^ = ! ` [ ] { } $ @  and ~ are invalid characters."
                                        ValidationExpression="^(a-z|A-Z|0-9)*[^\\<>^+?|!`\[\]{}\=@$%&~]*$"></asp:RegularExpressionValidator>
                                </td>
                                <td style="width: 15%" align="left" valign="top" class="blackblodtext">
                                    Assistant-In-Charge<span class="reqfldText">*</span><%--FB 2975--%>
                                </td>
                                <td style="width: 30%" align="left" valign="top">
                                    <asp:TextBox ID="Assistant" runat="server" CssClass="altText"></asp:TextBox>
                                    <a id="EditHref" href="javascript:getYourOwnEmailList(-2);" onmouseover="window.status='';return true;">
                                        <img border="0" src="image/edit.gif" alt="edit" width="17" height="15" style="cursor:pointer;" title="myVRM Address Book" /></a> <%--FB 2798--%>
                                    <a href="javascript: deleteAssistant();" onmouseover="window.status='';return true;">
                                        <img border="0" src="image/btn_delete.gif" alt="delete" width="16" height="16" style="cursor:pointer;" title="Delete" /></a> <%--FB 2798--%>
                                    <asp:RequiredFieldValidator ID="AssistantValidator" runat="server" ControlToValidate="Assistant"
                                        Display="dynamic" CssClass="lblError" SetFocusOnError="true" Text="Required"
                                        ValidationGroup="Submit"></asp:RequiredFieldValidator>
                                </td>
                            </tr>
                            <tr>
                                <td align="left" class="blackblodtext">
                                    Internal number
                                </td>
                                <td align="left">
                                    <asp:TextBox CssClass="altText" ID="txtInternalnum" MaxLength="25" runat="server"></asp:TextBox>
                                </td>
                                <td align="left" class="blackblodtext">
                                    External number
                                </td>
                                <td align="left">
                                    <asp:TextBox CssClass="altText" ID="txtExternalnum" runat="server"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td align="left" class="blackblodtext">
                                    VMR Link
                                </td>
                                <td align="left">
                                    <asp:TextBox CssClass="altText" ID="txtVMRLink" runat="server"  MaxLength="325" Rows="5" ></asp:TextBox>
                                </td>
                                <td align="left">
                                    &nbsp;
                                </td>
                                <td align="left" class="blackblodtext">
                                    &nbsp;
                                </td>
                                
                            </tr>
                        </table>
                    </td>
                </tr>
                
                <%--FB 2994 Start--%>
                
                <tr>
                    <td class="subtitleblueblodtext" align="left">
                        Image
                    </td>
                </tr>
                <tr>
                    <td>
                        <table id="Table1" cellpadding="2" cellspacing="2" border="0" style="width: 90%; margin-left:20px">
                            <tr>
                                <td align="left" style="width:18%" valign="top" class="blackblodtext">
                                    Room Image
                                </td>
                                <td colspan="3" align="left" class="blackblodtext"> 
                                    <div>
                                        <input type="text" class="file_input_textbox" readonly="readonly" value='No file selected' />
                                        <div class="file_input_div">
                                            <input type="button" value="Browse" class="file_input_button"  /><%--FB 3055-Filter in Upload Files--%>
                                            <input type="file"  class="file_input_hidden" id="roomfileimage" accept="image/*" contenteditable="false" enableviewstate="true" runat="server" onchange="getfilename(this)"/>
                                        </div>
                                    </div>
                                    <asp:RegularExpressionValidator ID="regroomfileimage" runat="server" Display="Dynamic" ValidationGroup="Submit1" ControlToValidate="roomfileimage" CssClass="lblError" ErrorMessage="File type is invalid." ValidationExpression="^.*\.((j|J)(p|P)(e|E)?(g|G)|(g|G)(i|I)(f|F)|(p|P)(n|N)(g|G)|(b|B)(m|M)(p|P))$"></asp:RegularExpressionValidator>
                                    <asp:Button ID="BtnUploadRmImg" CssClass="altLongBlueButtonFormat" runat="server"  style="margin-left:2px"
                                        Text="Upload Room Image" OnClick="UploadRoomImage" ValidationGroup="Submit1" /> <%--FB 3055-Filter in Upload Files--%>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td align="left" colspan = "4"> 
                        <div style="overflow-y: hidden; overflow-x: auto; height: auto; width: 600px; margin-left:18%">
                            <asp:DataGrid BorderColor="blue" BorderStyle="solid" BorderWidth="1" ID="dgItems"
                                AutoGenerateColumns="false" OnItemCreated="BindRowsDeleteMessage" OnDeleteCommand="RemoveImage"
                                runat="server" Width="70%" GridLines="None" Visible="false" Style="border-collapse: separate">
                                <HeaderStyle Height="30" CssClass="tableHeader" HorizontalAlign="Center" />
                                <AlternatingItemStyle CssClass="tableBody" />
                                <ItemStyle CssClass="tableBody" />
                                <FooterStyle CssClass="tableBody" />
                                <Columns>
                                    <asp:BoundColumn DataField="ImageName" Visible="true" HeaderText="Name" HeaderStyle-CssClass="tableHeader"
                                        ItemStyle-Width="20%"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="Image" Visible="false"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="Imagetype" Visible="false"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="ImagePath" Visible="false"></asp:BoundColumn>
                                    <asp:TemplateColumn HeaderText="Image" HeaderStyle-CssClass="tableHeader" ItemStyle-Width="40%" ItemStyle-HorizontalAlign="center">
                                        <ItemTemplate>
                                            <asp:Image ID="itemImage" ImageUrl='<%# DataBinder.Eval(Container, "DataItem.ImagePath") %>'
                                                Width="30" Height="30" runat="server" />
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="Actions" HeaderStyle-CssClass="tableHeader" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="btnDelete" Text="Remove" CommandName="Delete" runat="server"></asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                </Columns>
                            </asp:DataGrid><br />
                        </div>
                    </td>
                </tr>
                
                <%--FB 2994 End--%>
                
                <tr>
                    <td align="left">
                        <span class="subtitleblueblodtext">Department</span>
                    </td>
                </tr>
                <tr>
                    <td>
                        <table id="tbRoomsDept" cellpadding="2" cellspacing="2" border="0" style="width: 90%; margin-left:20px">
                            <tr>
                                <td width="16%" align="left" valign="top" class="blackblodtext">
                                    Room's Departments
                                </td>
                                <td style="width: 75%" align="left" valign="top">
                                    <asp:ListBox ID="DepartmentList" runat="server" CssClass="altText" DataTextField="Name"
                                        DataValueField="ID" SelectionMode="multiple"></asp:ListBox>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td style="height: 8px">
                    </td>
                </tr>
                <tr>
                    <td align="center">
                        <table id="tblButtons" cellpadding="2" cellspacing="2" border="0" style="width: 90%">
                            <tr>
                                <td align="center" style="width: 20%">
                                    <asp:Button ID="btnReset" runat="server" Text="Reset" CssClass="altMedium0BlueButtonFormat"
                                        OnClick="ResetRoomProfile" />
                                </td>
                                <td align="center" style="width: 20%">
                                    <input name="Go" type="button" class="altMedium0BlueButtonFormat" onclick="javascript:fnClose();"
                                        value=" Go Back " />
                                </td>
                                <td align="center" style="width: 20%">
                                    <asp:Button ID="btnSubmitAddNew" runat="server" ValidationGroup="Submit" Text="Submit / New Room"
                                         OnClick="SetRoomProfile" OnClientClick="javascript:return frmMainroom_Validator()" style="width:200px" />
                                </td>
                                <td align="center" style="width: 20%">
                                    <asp:Button ID="btnSubmit" ValidationGroup="Submit" runat="server" Text="Submit" Width="150pt"
                                         OnClick="SetRoomProfile" OnClientClick="javascript:return frmMainroom_Validator()" />
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </center>
    </div>
    </form>
</body>
</html>

<script type="text/javascript">

    if (document.getElementById("hdnMultipleDept"))
        if (document.getElementById("hdnMultipleDept").value == "0") {
        if (document.getElementById("trRoomsDept") != null)
            document.getElementById("trRoomsDept").style.display = "none";
    }
    //        
</script>

<!-- #INCLUDE FILE="inc/mainbottomNET.aspx" -->
