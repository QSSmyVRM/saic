<%--ZD 100147 start--%>
<%--/* Copyright (C) 2013 myVRM - All Rights Reserved	
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.	
*
* You should have received a copy of the myVRM license with	
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/--%><%--ZD 100147 End--%>
<%@ Page Language="C#" AutoEventWireup="true" Inherits="MasterChildReport" EnableEventValidation="false"  Debug="true" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd"> <%--FB 2779--%>
<meta http-equiv="X-UA-Compatible" content="IE=8" /> 
<!-- FB 2885 Starts -->
<% if (Session["isExpressUser"].ToString() == "1"){%>
<!-- #INCLUDE FILE="inc/maintopNETExp.aspx" -->
<%}else{%>
<!-- #INCLUDE FILE="inc/maintopNET.aspx" -->
<%}%>
<!-- FB 2885 Ends -->
<%@ Register Assembly ="DevExpress.Web.ASPxEditors.v10.2, Version=10.2.3.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v10.2, Version=10.2.3.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxPanel" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v10.2, Version=10.2.3.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxPopupControl" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v10.2, Version=10.2.3.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxMenu" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.ASPxGridView.v10.2, Version=10.2.3.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" 
    Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.ASPxGridView.v10.2.Export, Version=10.2.3.0, Culture=neutral,PublicKeyToken=b88d1754d700e49a" 
    Namespace="DevExpress.Web.ASPxGridView.Export" TagPrefix="dx" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajax" %> 


<%--Basic validation function--%>
<script type="text/javascript">   
  
  var servertoday = new Date();
     
    function OnShowButtonClick(s) 
    {    
           
        var hdnSubmitValue = document.getElementById("hdnSubmitValue");
        var ctrl;
         // Code for Department Selection
//        if(hdnSubmitValue.value == "3")
//           ctrl = chk_DeptR;
//        else if(hdnSubmitValue.value == "4")
//           ctrl = chk_DeptE;
//        else if(hdnSubmitValue.value == "5")
//           ctrl = chk_DeptM;
        
        if (s == "dptPopup" || s == "dptPopupChk")   
        {
            var canShow = 0;
             // Code for Department Selection
//            if( (s== "dptPopup" && (hdnSubmitValue.value == "1" || hdnSubmitValue.value == "2")) 
//             || (s== "dptPopup" && (hdnSubmitValue.value == "3" || hdnSubmitValue.value == "4" || hdnSubmitValue.value == "5") && ctrl != undefined && ctrl.GetChecked())    
//             || (s=="dptPopupChk" && ctrl != undefined && ctrl.GetChecked()) )
//                canShow = 1;
            if(hdnSubmitValue.value == "4" || hdnSubmitValue.value =="5" || hdnSubmitValue.value == "6") //FB 2593
            {
                canShow = 0; 
                
                if(chk_SpecMCUM.GetChecked() || hdnSubmitValue.value == "6")
                {
                    mcuPopup.ShowAtPos(355,0);
                    BindMCU();
                    
                    if(chk_SpecEptM.GetChecked())
                        BindEndpointList();
                }
            }
            else
                canShow = 1;
                
            if(OrgList.GetSelectedItems().length > 0 && canShow == 1) 
                dptPopup.ShowAtPos(365, 0);
            else
                dptPopup.Hide();
        }
        else if (s == "assignmentPopup") 
        {
            if(lstDepartment.GetSelectedItems().length > 0)
            {
               if(hdnSubmitValue.value == "1")
               {
                    showAssignmentCtrols('block')                
                    assignmentPopup.ShowAtPos(530, 0);        
                    resourcePopup.ShowAtPos(700, 0);  
               } 
               else if(hdnSubmitValue.value == "2")
               {
                    showAssignmentCtrols('block')                
                    assignmentPopup.ShowAtPos(530, 0);        
                    confPopup.ShowAtPos(700, 0);  
               }
            }
            else
            {
                assignmentPopup.Hide();
                resourcePopup.Hide();
                confPopup.Hide();
            }
            
           if(hdnSubmitValue.value == "3")
           {
                if(chk_TiersR.GetChecked())
                    tierPopup.ShowAtPos(530, 0);  
                else 
                    tierPopup.Hide();
           }
           
           if(hdnSubmitValue.value == "4")
           {
                //FB 2593
                var e = 0;
                if(chk_ENameE.GetChecked())
                {
                    eptPopup.ShowAtPos(365, 0);
                    e = 1;
                }
                else
                    eptPopup.Hide();
                    
                if(chk_RmNameE.GetChecked())
                {
                    tierPopup.Hide();
                    rmInfoPopup.Hide();
                    if(e == 1)
                    {
                        tierPopup.ShowAtPos(530, 0);  
                        rmInfoPopup.ShowAtPos(690, 0);  
                    }
                    {
                        tierPopup.ShowAtPos(365, 0);  
                        rmInfoPopup.ShowAtPos(530, 0);
                    }
                }
                else
                {
                    tierPopup.Hide();
                    rmInfoPopup.Hide();
                }   
           }
           
           if(hdnSubmitValue.value == "5")
           {
                var r = 0;
                if(chk_RmNameM.GetChecked() || chk_SpecRoomM.GetChecked()) //FB 2593
                {
                    if(chk_SpecRoomM.GetChecked())
                    {
                        rmPopup.ShowAtPos(200, 150);  
                        r = 1;
                    }
                    else
                        rmPopup.Hide();
                    
                    tierPopup.Hide();
                    rmInfoPopup.Hide();
                    if(r == 1)
                    {
                        tierPopup.ShowAtPos(365, 150);  
                        rmInfoPopup.ShowAtPos(530, 150);  
                    }
                    else
                    {
                        tierPopup.ShowAtPos(200, 150);  
                        rmInfoPopup.ShowAtPos(365, 150);
                    }
                }
                else
                {
                    tierPopup.Hide();
                    rmInfoPopup.Hide();
                    rmPopup.Hide();
                }
                
                var i = 0, j = 0
                if(chk_SpecMCUM.GetChecked())
                {
                    mcuPopup.Hide()
                    mcuPopup.ShowAtPos(365,0)
                    i = 1;
                    
                    if(OrgList.GetSelectedItems().length > 0)
                    {
                        BindMCU();
                        
                        if(chk_SpecEptM.GetChecked())
                            BindEndpointList();
                    }
                }
                else
                    mcuPopup.Hide()
                
                if(chk_ENameM.GetChecked() || chk_SpecEptM.GetChecked())
                {
                    if(chk_SpecEptM.GetChecked())
                    {
                        endPtPopup.Hide();
                        if(i == 0)
                        {
                            endPtPopup.ShowAtPos(365, 0);
                            i = 1;
                        }
                        else
                        {
                            endPtPopup.ShowAtPos(530, 0);
                            i = 2;j = 1;
                        }
                        
                        if(lstMCU.GetSelectedItems().length > 0)
                            BindEndpointList();
                    } 
                    
                    eptPopup.Hide();
                    if(i == 0)
                    { 
                        if(j == 0)
                            eptPopup.ShowAtPos(365, 0);
                        else
                            eptPopup.ShowAtPos(530, 0);
                    }                        
                    else 
                    {
                        if(j == 0)
                            eptPopup.ShowAtPos(530, 0);
                        else
                            eptPopup.ShowAtPos(690, 0);
                    }
                }
                else
                {
                    eptPopup.Hide();
                    endPtPopup.Hide();
                }
           }
        }
        else if(s == "rmInfoPopup")
        {
//            if(lstTiers.GetSelectedItems().length > 0)
//            {
                if(hdnSubmitValue.value == "3" && chk_NameR.GetChecked())
                   rmInfoPopup.ShowAtPos(700, 0);  
                else if((hdnSubmitValue.value == "4" || hdnSubmitValue.value == "5") && chk_RmNameE.GetChecked())
                   rmInfoPopup.ShowAtPos(850, 0);  
                else
                   rmInfoPopup.Hide();
                    
//            }
//            else
//               rmInfoPopup.Hide();
        }
        else if(s == "rmAssetPopup")
        {
            if(chk_AssetsR.GetChecked())
                rmAssetPopup.ShowAtPos(700, 0);  
            else
                rmAssetPopup.Hide();
        }
        else if(s == "assignment")
        {
            if(chk_UserAssignmentR.GetChecked())
            {
                showAssignmentCtrols('None')
                if(assignmentPopup_lstRmApprover)
                    assignmentPopup_lstRmApprover.style.display = 'block';
                assignmentPopup.ShowAtPos(530, 0);        
            }
            else
                assignmentPopup.Hide();
        }        
        else if(s == "tierPopup")
        {
            if(lstEptDetailsM.GetSelectedItems().length > 0)
                tierPopup.ShowAtPos(690, 0);        
            else
                tierPopup.Hide();
        }  
         else if(s == "confOccurencePopup")
        {
            if(lstConfStatus.GetSelectedItems().length > 0)
                confOccurencePopup.ShowAtPos(355, 285);        
            else
                confOccurencePopup.Hide();
        }
         else if(s == "confDetailsPopup")
        {
            if(lstConfOccurence.GetSelectedItems().length > 0)
                confDetailsPopup.ShowAtPos(505, 365);        
            else
                confDetailsPopup.Hide();
        }
        else if(s == "CDR")
        {
            popup.Hide();
            popup.ShowAtPos(202, 0);
            
            if(OrgList.GetSelectedItems().length > 0) 
            {
                mcuPopup.Hide();
                mcuPopup.ShowAtPos(355,0);
            }
        }
        else
        {
            var isChecked
            
            if(s == "popupU")
                isChecked = chk_OrgU.GetChecked();    
            else if(s == "popupR")
                isChecked = chk_OrgR.GetChecked();        
            else if(s == "popupE")
                isChecked = chk_OrgE.GetChecked();        
            else if(s == "popupM")
                isChecked = chk_OrgM.GetChecked();                
            else
                isChecked = chk_Org.GetChecked();
            
            if(isChecked)
                popup.ShowAtPos(202, 0);
            else
            {
                popup.Hide(); 
                fnClearListSelection(OrgList);
                
                dptPopup.Hide();                
                if(ctrl != undefined)
                    ctrl.SetChecked(false);                    
                             
                if(hdnSubmitValue.value == "1" || hdnSubmitValue.value == "2")
                {
                    fnRightParamsClear('O')
                    assignmentPopup.Hide();
                    resourcePopup.Hide();
                    confPopup.Hide();
                }
                //rmInfoPopup.Hide();                
                //eptPopup.Hide();
            }
        }
            
        if(OrgList.GetSelectedItems().length > 0) 
        {        
            if ((s == "dptPopup" && (hdnSubmitValue.value == "1" || hdnSubmitValue.value == "2")) || s == "dptPopupChk" 
            || (ctrl != undefined && ctrl.GetChecked() && s != "assignmentPopup" && s != "rmInfoPopup"))        
                getArrayList();
                
            if(s == "popupR" || s == "popupE" || s == "popupM")
                getArrayList();
        }
        else if(ctrl != undefined && s == "dptPopupChk")
            ctrl.SetChecked (false);
            
        if(hdnSubmitValue.value == "3" || hdnSubmitValue.value == "4" || hdnSubmitValue.value == "5")
            PopupPositions(hdnSubmitValue.value)
    }
    
    function PopupPositions(popVal)
    {
        if(popVal == "2")
        {
            
        }
        else if(popVal == "3")
        {
        
        }
        else if(popVal == "4")
        {
        
        }
    }
    
    function showAssignmentCtrols(SorHVar)
    {   
        if(assignmentPopup_lstRmApprover)
            assignmentPopup_lstRmApprover.style.display = SorHVar;
        if(document.getElementById(assignmentPopup_ASPxMenu1))
            document.getElementById(assignmentPopup_ASPxMenu1).style.display = SorHVar;
        if(document.getElementById(assignmentPopup_ASPxMenu2))
            document.getElementById(assignmentPopup_ASPxMenu2).style.display = SorHVar;
        if(document.getElementById(assignmentPopup_ASPxMenu3))
            document.getElementById(assignmentPopup_ASPxMenu3).style.display = SorHVar;
        if(assignmentPopup_lstMCUApprover)
            assignmentPopup_lstMCUApprover.style.display = SorHVar;
        if(assignmentPopup_lstWOAdmin)
            assignmentPopup_lstWOAdmin.style.display = SorHVar;
    }
    
    function getArrayList()
    {        
        var hdnDeptList = document.getElementById("hdnDepartmentList");        
        
        //hdnDeptList.value = "11!1!Sales``11!2!Engineering``11!3!larry``12!4!New Dept";
        var AllDeptList = hdnDeptList.value.split("``");
        
        var deptList;
        
        lstDepartment.BeginUpdate();
        
        var lstCnt = lstDepartment.GetItemCount();
         for (var c = 0; c < lstCnt; c++) 
		    lstDepartment.RemoveItem(0);		 
        
        var n =0;
        for (var i = 0; i < AllDeptList.length; i++) 
        {                
            deptList = AllDeptList[i].split('!'); 
            var s = OrgList.GetSelectedItems()
            if(deptList[0] == s[0].value)
            {
                lstDepartment.InsertItem(n,deptList[2],deptList[1]);                    
                n = n + 1;
            }
        }
        
        if(lstDepartment.GetItemCount() > 0)
            lstDepartment.InsertItem(n,"All","0");        
        else
            lstDepartment.InsertItem(n,"No Departments","-1");        

        lstDepartment.EndUpdate();
    }
    //FB 2593
    function BindEptFromMCU()
    {
        if(chk_SpecEptM.GetChecked())
              BindEndpointList();
    }
    
    function BindMCU()
    {        
        var hdnMCUList = document.getElementById("hdnMCUList");        
        
        //hdnMCUList.value = "11!1!Sales``11!2!Engineering``11!3!larry``12!4!New MCU";
        var AllMCUList = hdnMCUList.value.split("``");
        
        var mcuList;
        
        lstMCU.BeginUpdate();
        
        var lstCnt = lstMCU.GetItemCount();
         for (var c = 0; c < lstCnt; c++) 
		    lstMCU.RemoveItem(0);		 
        
        var n =0; p = 0
        for (var i = 0; i < AllMCUList.length; i++) 
        {                
            var mcuName = "";
            mcuList = AllMCUList[i].split('!'); 
            if(OrgList.GetSelectedItems().length > 0)
            {
                var s = OrgList.GetSelectedItems()
                if(mcuList[0] == s[0].value || mcuList[0] == "0")
                {   
                    p = 0;
                    mcuName = mcuList[2];
                    if(mcuList[0] == "0")
                        p = 1;
                    
                    if(p == 1)
                        mcuName = "(*) " + mcuName ;
                    
                    lstMCU.InsertItem(n,mcuName,mcuList[1]);   
//                    if(p == 1)
//                    {  
//                        var html = document.getElementById("mcuPopup_lstMCU_LBT").childNodes[i].innerHTML;
//                        var htmlArray = html.split('>');
//                        var htmlArray2 = htmlArray[1].split('<');
//                        var starInsert = "<span style='color:red'>*</span>";
//                        var newHtml = htmlArray[0] + '>' + htmlArray2[0] + starInsert + '<' + htmlArray2[1];
//                        document.getElementById("mcuPopup_lstMCU_LBT").childNodes[i].innerHTML = newHtml;
//                    }
                                     
                    n = n + 1;
                }
            }
            else
            {
                mcuName = mcuList[2];
                if(mcuList[0] == "0")
                    mcuName = "(*) " + mcuName ;
                lstMCU.InsertItem(n,mcuName,mcuList[1]);                    
                n = n + 1;
            }
        }
        
        if(lstMCU.GetItemCount() > 0)
            lstMCU.InsertItem(n,"All","0");        
        else
            lstMCU.InsertItem(n,"No MCUs","-1");        

        lstMCU.EndUpdate();
    }
    
    function BindEndpointList()
    {        
        var hdnEptList = document.getElementById("hdnEptList");        
        
        //hdnEptList.value = "11!1!Sales``11!2!Engineering``11!3!larry``12!4!New MCU";
        var AllEptList = hdnEptList.value.split("``");
        
        var eptList;
        
        lstendPt.BeginUpdate();
        
        var lstCnt = lstendPt.GetItemCount();
         for (var c = 0; c < lstCnt; c++) 
		    lstendPt.RemoveItem(0);		 
        
        var ol = "";
        if(OrgList.GetSelectedItems().length > 0)
            ol = OrgList.GetSelectedItems()[0].value;   
                    
        var n =0;
        for (var i = 0; i < AllEptList.length; i++) 
        {                
            eptList = AllEptList[i].split('!'); 
            if(lstMCU.GetSelectedItems().length > 0 && lstMCU.GetSelectedItems()[0].value != "0")
            {
                var s = lstMCU.GetSelectedItems()
                if(eptList[0] == s[0].value)
                {
                    if(ol != "" && ol != eptList[3])
                        continue;
                    
                    lstendPt.InsertItem(n,eptList[2],eptList[1]);                    
                    n = n + 1;
                }
            }
            else
            {
                if(ol != "" && ol != eptList[3])
                    continue;
                        
                lstendPt.InsertItem(n,eptList[2],eptList[1]);                    
                n = n + 1;
            }
        }
        
        if(lstendPt.GetItemCount() > 0)
            lstendPt.InsertItem(n,"All","0");        
        else
            lstendPt.InsertItem(n,"No Endpoints","-1");        

        lstendPt.EndUpdate();
    } 
	//FB 2593 Starts
    function BindRoomList()
    {        
        var hdnRoomList = document.getElementById("hdnRoomList");        
        
        //hdnEptList.value = "11!1!Sales``11!2!Engineering``11!3!larry``12!4!New MCU";
        var AllRoomList = hdnRoomList.value.split("``");
        
        var roomList;
        lstRm.BeginUpdate();
        
        var lstCnt = lstRm.GetItemCount();
        for (var c = 0; c < lstCnt; c++) 
		   lstRm.RemoveItem(0);		 
        
        var n =0;
        for (var i = 0; i < AllRoomList.length; i++) 
        {                
            roomList = AllRoomList[i].split('!'); 
            if(OrgList.GetSelectedItems().length > 0)
            {
                var s = OrgList.GetSelectedItems()
                if(roomList[0] == s[0].value)
                {
                    lstRm.InsertItem(n,roomList[2],roomList[1]);                    
                    n = n + 1;
                }
            }
            else
            {
                lstRm.InsertItem(n,roomList[2],roomList[1]);                    
                n = n + 1;
            }
        }
        
        if(lstRm.GetItemCount() > 0)
            lstRm.InsertItem(n,"All","0");        
        else
            lstRm.InsertItem(n,"No Rooms","-1");        

        lstRm.EndUpdate();
    } 
	//FB 2593 End
</script>

<%--Show or Hide Popup function --%>
<script type="text/javascript">
    
    function HidePopups()
    {
        popup.Hide();
        dptPopup.Hide();
        assignmentPopup.Hide();
        resourcePopup.Hide();
        confPopup.Hide();
        tierPopup.Hide();
        rmInfoPopup.Hide();
        rmAssetPopup.Hide();
        confStatusPopup.Hide();
        confOccurencePopup.Hide();
        confDetailsPopup.Hide();
        eptDetailsPopup.Hide();
        woDetailsPopup.Hide();
        mcuDetailsPopup.Hide();
        eptPopup.Hide();
        mcuPopup.Hide();//FB 2593
    }
    
    function fnShow(submitVal)
    {
        var hdnSubmitValue = document.getElementById("hdnSubmitValue");    
        var popup2 = $find('ConferencePopUp');
        
        if(rbAdhoc.GetChecked())
        {
            SavedRptListPopup.Hide();
            
            if(popup2)
            {
                if(hdnSubmitValue.value != submitVal)
                {
                    HidePopups();
                    fnClear();
                }
                
                hdnSubmitValue.value = submitVal;
                popup2.show();
                MainPopup.ShowAtPos(0, 0);
                
                var tblConference = document.getElementById("tblConference");            
                var tblUser = document.getElementById("tblUser");            
                var tblRoom = document.getElementById("tblRoom");    
                var tblEndPoint = document.getElementById("tblEndPoint");    
                var tblMCU = document.getElementById("tblMCU");    
                var tblCDR = document.getElementById("tblCDR"); //FB 2593
                    
                tblConference.style.display = 'None';
                tblUser.style.display = 'None';
                tblRoom.style.display = 'None';
                tblEndPoint.style.display = 'None';
                tblMCU.style.display = 'None';
                tblCDR.style.display = 'None'; //FB 2593
                
//                btnSave.SetVisible(true);
//                txtSaveReport.SetVisible(true);
                    
                if(submitVal == "1")
                {
                    tblConference.style.display = 'block';
                    MainPopup.SetHeaderText("Conferences");
                }
                else  if(submitVal == "2")
                {
                    tblUser.style.display = 'block';
                    MainPopup.SetHeaderText("Users");
                }                
                else  if(submitVal == "3")
                {
                    tblRoom.style.display = 'block';
                    MainPopup.SetHeaderText("Rooms");
                }    
                else  if(submitVal == "4")
                {
                    tblEndPoint.style.display = 'block';
                    MainPopup.SetHeaderText("Endpoints");
                }    
                else  if(submitVal == "5")
                {
                    tblMCU.style.display = 'block';
                    MainPopup.SetHeaderText("MCU");
                }
                else  if(submitVal == "6") //FB 2593
                {
                    tblCDR.style.display = 'block';
                    MainPopup.SetHeaderText("CDR");
                    //btnSave.SetVisible(false);
                    //txtSaveReport.SetVisible(false);
                    //rbConferenceCDR.SetChecked(true);
                    OnShowButtonClick('CDR');
                }
            }
         
          fnDefaultOrgSelection(submitVal); //FB 2593
                           
        }
        else if (rbSaved.GetChecked())
        {
            var hdnReportNames = document.getElementById("hdnReportNames");        
            var hdnAllReports = document.getElementById("hdnAllReports");        
            var hdnSubmitValue = document.getElementById("hdnSubmitValue");
            var rptList;
            var detailsList;
            var subdetailsList;
            
            hdnSubmitValue.value = submitVal;            
            lstReportList.ClearItems()
            
            var AllRptList = hdnAllReports.value.split("�");
            var n = 0;
            
            for (var i = 0; i < AllRptList.length; i++)
            {
                rptList = AllRptList[i].split('�');
                
                if(rptList[0] == hdnSubmitValue.value)
                {
                    detailsList = rptList[1].split('�');
                                        
                    for (var j = 0; j < detailsList.length; j++)
                    {
                        if(detailsList[j] != "")
                        {
                            subdetailsList = detailsList[j].split('�');
                            
                            lstReportList.InsertItem(n, subdetailsList[0], subdetailsList[1]);
                            
                            n = n + 1;
                        }
                    }
                }
            }
            //FB 2808             
            lstReportList.SetEnabled(true);
                        
            if(lstReportList.GetItemCount() == 0)
            {
                lstReportList.InsertItem(n,"No Saved Reports","-1");
                lstReportList.SetEnabled(false);
                document.getElementById('<%= btnRptDelete.ClientID %>').disabled = true; //FB 2886
                document.getElementById('<%= btnRptView.ClientID %>').disabled = true;
                document.getElementById('<%= btnRptDelete.ClientID %>').className="btndisable";
                document.getElementById('<%= btnRptView.ClientID %>').className="btndisable";
                //btnRptDelete.SetEnabled(false);
                //btnRptView.SetEnabled(false);
            }
            else
            {
                document.getElementById('<%= btnRptDelete.ClientID %>').disabled = false;//FB 2886
                document.getElementById('<%= btnRptView.ClientID %>').disabled = false;
                document.getElementById('<%= btnRptDelete.ClientID %>').className="altShortBlueButtonFormat";
                document.getElementById('<%= btnRptView.ClientID %>').className="altShortBlueButtonFormat";
                //btnRptDelete.SetEnabled(true);
                //btnRptView.SetEnabled(true);
                lstReportList.SetEnabled(true);
            }
            
            SavedRptListPopup.ShowAtPos(160, 220);
            
        }
    }
    //FB 2808 - start
    function fnSelect()
    {
        var items = lstReportList.GetSelectedItems();
        var hdnReportSelection = document.getElementById("hdnReportSelection");
        hdnReportSelection.value = ""; //FB 2886
        //FB 2808 
        if(lstReportList.GetSelectedItems().length > 0)
        { 
            if(lstReportList.GetSelectedItems()[0].value != "-1" )
            {
                for(var i = 0; i < items.length; i++) 
                {
                    if(hdnReportSelection.value == "")
                        hdnReportSelection.value = items[i].index;
                    else
                        hdnReportSelection.value = hdnReportSelection.value + "," + items[i].index;
                }
                //FB 2886
                if(hdnReportSelection.value.indexOf(',') > 0)
                {
                    document.getElementById('<%= btnRptView.ClientID %>').disabled = true;
                    document.getElementById('<%= btnRptView.ClientID %>').className="btndisable";
                    
                    //btnRptView.SetEnabled(false);
                }
                else
                {
                    document.getElementById('<%= btnRptView.ClientID %>').disabled = false;
                    document.getElementById('<%= btnRptView.ClientID %>').className="altShortBlueButtonFormat";
                    //btnRptView.SetEnabled(true);
                }
            }
        }
    }
    
    function fnDelete()
    {
        var hdnReportSelection = document.getElementById("hdnReportSelection");
        
        if(hdnReportSelection.value != "" && lstReportList.GetSelectedItems()[0].value != "-1")
        {
			//FB 2886
            var r= confirm("Are you sure you want to delete the report?");
            if (r == true)
            {
                var hdnRptSave = document.getElementById("hdnRptSave");
                if(hdnRptSave)
                    hdnRptSave.value = "1";

                return true;
                DataLoading(1); //ZD 100176       
            }
            else  //FB 2886
                return false;
        }
        else
        {
            alert("Please select the report name to delete.");
            return false;
        }
    }
    
    function SavedReportSelection()
    {
        var hdnMainIPValue = document.getElementById("hdnMainIPValue");
        var hdnRightMenuValue = document.getElementById("hdnRightMenuValue");
       
        if(lstReportList.GetSelectedItems().length > 0) {
            DataLoading(1); //ZD 100176
            var sr = lstReportList.GetSelectedItems();
            var rptValue = sr[0].value; 
            
            if(rptValue != "" && rptValue != "-1")
            {
                var subdetailsList = rptValue.split('??');
                
                if(subdetailsList[0] != 'undefined' && subdetailsList[0] != null)                
                    hdnMainIPValue.value = subdetailsList[0];
                    
                if(subdetailsList[1] != 'undefined' && subdetailsList[1] != null)                
                    hdnRightMenuValue.value = subdetailsList[1];
                    
                //fnSubmit();
                //fnClosePopup();
                var hdnOkValue = document.getElementById("hdnOkValue");
                hdnOkValue.value = "1";
            
                SavedRptListPopup.Hide();
            
                //FB 2886
                var btnPreview = document.getElementById("btnPreview");
                btnPreview.click(); 
                //btnPreview.DoClick(); 
                
            }
        }
        else //FB 2808
        {
            alert("Please select the report name to view the details.");
            return false;
        }
        
        return true;
    }
    //FB 2593 - end
   
    function fnClose()
    {    
        var closePup = document.getElementById("ClosePUp");
        
        if(closePup != null)
            closePup.click();        
            
        return false;
    }
    
    function fnPersonAll()
    { 
        if(chk_AllPerson.GetChecked())
        {
            chk_PFN.SetChecked (true); 
            chk_PLN.SetChecked (true); 
            chk_PMail.SetChecked (true); 
            chk_PRole.SetChecked (true); 
            //chk_PPerson.SetChecked (true); 
        }
        else
        {
            chk_PFN.SetChecked (false); 
            chk_PLN.SetChecked (false); 
            chk_PMail.SetChecked (false); 
            chk_PRole.SetChecked (false); 
            //chk_PPerson.SetChecked (false);
        }
    }
    
    function fnHostAll()
    { 
        if(chk_AllHost.GetChecked())
        {
            chk_HFN.SetChecked (true); 
            chk_HLN.SetChecked (true); 
            chk_HMail.SetChecked (true); 
            chk_HRole.SetChecked (true); 
            //chk_HPerson.SetChecked (true); 
        }
        else
        {
            chk_HFN.SetChecked (false); 
            chk_HLN.SetChecked (false); 
            chk_HMail.SetChecked (false); 
            chk_HRole.SetChecked (false); 
            //chk_HPerson.SetChecked (false);             
        }
    }
        
    function fnScheduledAll()
    { 
        if(chk_AllConf.GetChecked())
        {
            chk_Completed.SetChecked (true); 
            chk_Scheduled.SetChecked (true); 
            chk_Deleted.SetChecked (true); 
            chk_Terminated.SetChecked (true); 
            chk_Pending.SetChecked (true); 
        }
        else
        {
            chk_Completed.SetChecked (false); 
            chk_Scheduled.SetChecked (false); 
            chk_Deleted.SetChecked (false); 
            chk_Terminated.SetChecked (false); 
            chk_Pending.SetChecked (false); 
        }
    }
    
    function fnConfTypeAll()
    { 
        if(chk_AllConfTypes.GetChecked())
        {
            chk_Audio.SetChecked (true); 
            chk_Video.SetChecked (true); 
            chk_Room.SetChecked (true); 
            chk_PP.SetChecked (true); 
            //chk_TP.SetChecked (true); 
            
        }
        else
        {
            chk_Audio.SetChecked (false); 
            chk_Video.SetChecked (false); 
            chk_Room.SetChecked (false); 
            chk_PP.SetChecked (false); 
            //chk_TP.SetChecked (false); 
        }
    }
    
    function fnRecurAll()
    { 
        if(chk_AllMode.GetChecked())
        {
            chk_Single.SetChecked (true); 
            chk_Recurring.SetChecked (true); 
        }
        else
        {
            chk_Single.SetChecked (false); 
            chk_Recurring.SetChecked (false); 
        }
    }
    
    function fnRmTypeAll()
    { 
        if(chk_AllTypeR.GetChecked())
        {
            chk_VideoRoomR.SetChecked (true); 
            chk_AudioRoomR.SetChecked (true); 
            chk_MeetingRoomR.SetChecked (true); 
        }
        else
        {
            chk_VideoRoomR.SetChecked (false); 
            chk_AudioRoomR.SetChecked (false); 
            chk_MeetingRoomR.SetChecked (false); 
        }
    }
    
    function fnBehaviorAll()
    { 
        if(chk_AllBehaviorR.GetChecked())
        {
            chk_VideoConferenceR.SetChecked (true); 
            chk_AudioConferenceR.SetChecked (true); 
            chk_RoomConferenceR.SetChecked (true); 
            
            fnBehaviorAllChecked();
        }
        else
        {
            chk_VideoConferenceR.SetChecked (false); 
            chk_AudioConferenceR.SetChecked (false); 
            chk_RoomConferenceR.SetChecked (false);  
            
            confStatusPopup.Hide();
            confOccurencePopup.Hide();
            confDetailsPopup.Hide();
            eptDetailsPopup.Hide();
            woDetailsPopup.Hide();
            mcuDetailsPopup.Hide();
        }
    }
    
    function RemoveALL(e1,e2)
    {
        if(e1.GetChecked() == false)
            e2.SetChecked(false);
    }
    //FB 2654
    function fnConceirgeAll()
    { 
        if(chk_AllConcierge.GetChecked())
        {
            chk_Onsite.SetChecked (true); 
            chk_Meet.SetChecked (true); 
            chk_Monitor.SetChecked (true); 
            chk_VNOC.SetChecked (true); 
        }
        else
        {
            chk_Onsite.SetChecked (false); 
            chk_Meet.SetChecked (false); 
            chk_Monitor.SetChecked (false); 
            chk_VNOC.SetChecked (false); 
        }
    }
        
</script>

<%--fnSubmit function --%>
<script type="text/javascript">

    function fnCheck()
    {
        var hdnSubmitValue = document.getElementById("hdnSubmitValue");
        if(hdnSubmitValue.value == "")
            return false;
        DataLoading(1);//ZD 100176
        return true;        
    }
    
    function fnSubmit() 
    {
        var hdnSubmitValue = document.getElementById("hdnSubmitValue");
        var args = fnSubmit.arguments; //FB 2886
        var hdnGroup = document.getElementById("hdnGroup"); //FB 2965
        hdnGroup.value = "";
        if(rbAdhoc.GetChecked()) {
            if(hdnSubmitValue.value == "1")
                fnGetConferenceMenuInput();
            else if(hdnSubmitValue.value == "2")
                fnGetUserMenuInput();
            else if(hdnSubmitValue.value == "3")
                fnGetRoomMenuInput();
            else if(hdnSubmitValue.value == "4")
                fnGetMCUMenuInput(); //FB 2593
                //fnGetEndPointMenuInput();
            else if(hdnSubmitValue.value =="5") //FB 2593
                fnGetMCUMenuInput();
            else if(hdnSubmitValue.value == "6")
                fnGetCDRMenuInput();
        }
        //FB 2886
        if(args[0] == "1")
        {
            fnClose();
            return false;
        }
        else
            return true;
    }
    
    function fnAssignValue(element,type,cellVal)
    {
        memo.SetText(cellVal.replace(new RegExp(",", "g"), ";\n")); 
        MoreInfoPopup.ShowAtElement(element)
        
        if(type == "R")
            MoreInfoPopup.SetHeaderText("Room(s)");
        else if(type == "E")
            MoreInfoPopup.SetHeaderText("Endpoint(s)");
        else if(type == "D")
            MoreInfoPopup.SetHeaderText("Department(s)");
        
        return false; 
    }
</script>

<%--Clear functions--%>
<script type="text/javascript">
    
    function fnClear()
    {
        var hdnSubmitValue = document.getElementById("hdnSubmitValue");
        
        if(hdnSubmitValue.value == "1")
            fnConferenceMenuClear();
        else if(hdnSubmitValue.value == "2")
            fnUserMenuClear();
        else if(hdnSubmitValue.value == "3")
            fnRoomMenuClear();
        else if(hdnSubmitValue.value == "4")
            fnEndPointMenuClear()
        else if(hdnSubmitValue.value == "5")
            fnMCUMenuClear()
        else if(hdnSubmitValue.value == "6")
            fnCDRMenuClear()
            
        return false;
    }
    
    function fnConferenceMenuClear()
    {
        var hdnAdmin = document.getElementById("hdnAdmin"); //FB 2593
        if(hdnAdmin.value == "")
        {
            chk_Org.SetChecked (false); 
            OnShowButtonClick();
        }
        else
        {
			//FB 2882
            //fnClearListSelection(OrgList);
            //dptPopup.Hide();              
            fnClearListSelection(lstDepartment);  
            assignmentPopup.Hide();
            resourcePopup.Hide();
        }    
        chk_ConfID.SetChecked (false); 
        chk_ConfTitle.SetChecked (false); 
		chk_CTSNumericID.SetChecked(false);//Fb 2870
        
        fnRightParamsClear('');
               
        chk_AllHost.SetChecked (false); 
        fnHostAll()
        
        chk_AllPerson.SetChecked (false); 
        fnPersonAll()
        
        chk_AllConf.SetChecked (false);         
        fnScheduledAll()
        
        chk_AllConfTypes.SetChecked (false);         
        fnConfTypeAll();
        
        chk_AllMode.SetChecked (false);         
        fnRecurAll();
        //FB 2654
        chk_AllConcierge.SetChecked (false);         
        fnConceirgeAll();
        
        chk_Date.SetChecked (false); 
        chk_Time.SetChecked (false); 
        //chk_Hours.SetChecked (false); 
        chk_Minutes.SetChecked (false); 
    }
    
    function fnUserMenuClear()
    {
        var hdnAdmin = document.getElementById("hdnAdmin"); //FB 2593
        if(hdnAdmin.value == "")
        {
            chk_OrgU.SetChecked (false); 
                OnShowButtonClick('popupU');
        }
        else
        {
			//FB 2882
            //fnClearListSelection(OrgList);
            //dptPopup.Hide();
            fnClearListSelection(lstDepartment);  
        }    
        fnRightParamsClear('');
       
        chk_DeptU.SetChecked (false); 
            OnShowButtonClick('assignmentPopup');
            
        chk_FNU.SetChecked (false); 
        chk_LNU.SetChecked (false); 
        //chk_PersonU.SetChecked (false); 
        
        chk_UserEmailU.SetChecked (false); 
        chk_SecondaryEmailU.SetChecked (false); 
        chk_RoleU.SetChecked (false); 
        chk_TimeZoneU.SetChecked (false); 
        chk_ConfHostU.SetChecked (false); 
        chk_ConfPartU.SetChecked (false); 
        
        chk_AccExpirU.SetChecked (false); 
        chk_MinU.SetChecked (false); 
        chk_ExchangeU.SetChecked (false); 
        chk_DominoU.SetChecked (false); 
    }
    
    function fnRoomMenuClear()
    {
        var hdnAdmin = document.getElementById("hdnAdmin"); //FB 2593
        if(hdnAdmin.value == "")
        {
            chk_OrgR.SetChecked (false); 
                OnShowButtonClick('popupR');
        }    
        else
        {
			//FB 2882
            //fnClearListSelection(OrgList);
            //dptPopup.Hide();
            fnClearListSelection(lstDepartment);  
        }    
        
        fnRightParamsClear('');
        
        chk_DeptR.SetChecked (false); 
            OnShowButtonClick('assignmentPopup');
            
        chk_TiersR.SetChecked (false); 
            OnShowButtonClick('assignmentPopup');
            
        chk_NameR.SetChecked (false); 
            OnShowButtonClick('rmInfoPopup');
            
        chk_HostR.SetChecked (false); 
    
        chk_AllTypeR.SetChecked (false);         
        fnRmTypeAll();
        
        chk_AllBehaviorR.SetChecked (false);         
        fnBehaviorAll();
    }
    
    function fnEndPointMenuClear()
    {
        var hdnAdmin = document.getElementById("hdnAdmin"); //FB 2593
        if(hdnAdmin.value == "")
        {
            chk_OrgE.SetChecked (false); 
                OnShowButtonClick('popupE');
        }   
        //else //FB 2882
        //  fnClearListSelection(OrgList);
        
        fnRightParamsClear('');
        
        //chk_DeptE.SetChecked (false); //FB 2593
            
        chk_ENameE.SetChecked (false); 
        //chk_SpecEptE.SetChecked (false); 
        chk_RmNameE.SetChecked (false); 
        //chk_SpecRoomE.SetChecked (false); 
        
            OnShowButtonClick('assignmentPopup');
        
        chk_ConfE.SetChecked (false); 
            fnBehaviorAllChecked();
    }
    
    function fnMCUMenuClear()
    {
        var hdnAdmin = document.getElementById("hdnAdmin"); //FB 2593
        if(hdnAdmin.value == "")
        {
            chk_OrgM.SetChecked (false); 
                OnShowButtonClick('popupM');
        }    
        //else //FB 2882
            //fnClearListSelection(OrgList);
            
        fnRightParamsClear('');
		//FB 2593
        //chk_DeptM.SetChecked (false); 
            //OnShowButtonClick('assignmentPopup');
        chk_MCUNameM.SetChecked (false); 
        chk_SpecMCUM.SetChecked (false); 
        chk_ENameM.SetChecked (false); 
        chk_SpecEptM.SetChecked (false); 
        chk_RmNameM.SetChecked (false); 
        chk_SpecRoomM.SetChecked(false);
            OnShowButtonClick('assignmentPopup');
        
        chk_ConfM.SetChecked (false); 
            fnBehaviorAllChecked();
    }
    //FB 2593
    function fnCDRMenuClear()
    {
		//FB 2882
        if(document.getElementById("hdnAdmin").value != "U") //FB 2882
            fnClearListSelection(OrgList);
        fnClearListSelection(lstMCU);
        
        if(document.getElementById("hdnAdmin").value != "U") //FB 2882
           mcuPopup.Hide();
    }
    
    function fnRightParamsClear(m)
    {
        var hdnSubmitValue = document.getElementById("hdnSubmitValue");
        
        if(hdnSubmitValue.value == "1")
        {
            if(document.getElementById("hdnAdmin").value != "U")
                fnClearListSelection(OrgList);
            fnClearListSelection(lstDepartment);
            fnClearListSelection(lstRmApprover);
            fnClearListSelection(lstMCUApprover);
            fnClearListSelection(lstWOAdmin);
            fnClearListSelection(lstAttendee);
            fnClearListSelection(lstRmType);
            fnClearListSelection(lstEPYes);
            fnClearListSelection(lstMCUYes);
            fnClearListSelection(lstConfSpeedYes);
            fnClearListSelection(lstConfProYes);
            fnClearListSelection(lstWOYes);
        }   
        else if(hdnSubmitValue.value == "2")
        {
            //FB 2882
            if(document.getElementById("hdnAdmin").value != "U")
                fnClearListSelection(OrgList);
            fnClearListSelection(lstDepartment);
            fnClearListSelection(lstRmApprover);
            fnClearListSelection(lstMCUApprover);
            fnClearListSelection(lstWOAdmin);
            fnClearListSelection(lstConfType);
        }       
        else if(hdnSubmitValue.value == "3" || hdnSubmitValue.value == "4" || hdnSubmitValue.value == "5")
        {
            if(m == "" || m == "O")
            {
				//FB 2882
                if(document.getElementById("hdnAdmin").value != "U")
                    fnClearListSelection(OrgList);
                fnClearListSelection(lstDepartment);

                if(hdnSubmitValue.value == "3")
                    fnClearListSelection(lstRmInfo);
                
                if(hdnSubmitValue.value == "4" || hdnSubmitValue.value == "5")
                    fnClearListSelection(lstEptDetailsM);
                    
                fnClearListSelection(lstTiers);
                fnClearListSelection(lstRmInfo);
            }
            
            if(m == "" || m == "C")
            {
            
                fnClearListSelection(lstConfStatus);
                fnClearListSelection(lstConfOccurence);
                fnClearListSelection(lstConfDetails);
                fnClearListSelection(lstEptDetails);
                fnClearListSelection(lstWODetails);
                fnClearListSelection(lstMCUDetails);
            }
        }
    }
    
    function fnClearListSelection(e)
    {   
        //OrgList.UnselectAll();
        
        if(e.GetSelectedItems().length > 0)
        {   
            var lstCnt = e.GetItemCount();
            
            var n =0;
            for (var i = 0; i < lstCnt; i++) 
            {
                TempList.InsertItem(n,e.GetItem(i).value,e.GetItem(i).text);                    
                n = n + 1;
            }
        
            e.ClearItems()
            
            n = 0;
            for (var j = 0; j < lstCnt; j++) 
            {
                e.InsertItem(n,TempList.GetItem(j).value,TempList.GetItem(j).text);                    
                n = n + 1;
            } 
            
            TempList.ClearItems()
        }
    }
    
</script>

<%--Get Input Values from Conference Menu--%>
<script type="text/javascript">

    function fnClosePopup()
    {
        MainPopup.Hide();
//        popup.Hide();
//        dptPopup.Hide();
        //assignmentPopup.Hide();
        //resourcePopup.Hide();
    }
    
    function fnGetConferenceMenuInput()
    {
        var Conference = "";
        var hosts = "";
        var participants = "";
        var scheduledConf = "";
        var confTypes = "";
        var confBehavior = "";
        var confTime = "";
        var duraion = "";
        var hdnMainIPValue = document.getElementById("hdnMainIPValue");
            hdnMainIPValue.value = "";
        var orgvalue = "";
        var departments = "";
        var rmIncharge = "";
        var mcuIncharge = "";
        var woIncharge = "";
        var attendees = "";
        var roomsParty = "";
        var endPoints = "";
        var mcu = "";
        var confSpeed = "";
        var conProtocol = "";
        var confWO = "";
        
        var hdnOkValue = document.getElementById("hdnOkValue");
            hdnOkValue.value = "1";
        
        if(chk_ConfID.GetChecked())
            Conference += "ConfnumName as [Conf.ID] ,";
        if(chk_ConfTitle.GetChecked())
            Conference += "ExternalName as [Conf.Title],";
        if (chk_CTSNumericID.GetChecked()) //FB 2870
            Conference += "E164DialNumber as [CTS Numeric ID],";
        
        var hdnRightMenuValue = document.getElementById("hdnRightMenuValue");
            
        if(chk_Org.GetChecked())
        {
            //Conference += "OrgID,";
            
            hdnRightMenuValue.value = "";
            //Right Side Popups
            if(OrgList.GetSelectedItems().length >0)
            {
                var s = OrgList.GetSelectedItems()
                
                hdnRightMenuValue.value = "1:" + s[0].value + "|";
                //1:orgid;2:Departmentid;3(Rooms):LastName [Assistant Name],LastName as [Pri Appr.],LastName as [Sec. Appr. 1],LastName as [Sec. Appr. 2]
                //;4(MCU):LastName [Assistant Name],LastName as [Pri Appr.],LastName as [Sec. Appr. 1],LastName as [Sec. Appr. 2]
                //;5(WO):LastName [In Charge],
                //6:[Company Relationship];7:(invitee) 1 - External / 2 - Internal
                //8:(EP)0|1 End Point refer "GetConferenceEndpoint"
                //9:(MCU)0|1 can get from End Point
                //10:(Conf Speed)0|1 can get from End Point
                //11:(Conn Protocol)0|1 can get from End Point
                //12:(WO) 0|1 Name check with type is it A/V | CAT | HK
                if(lstDepartment.GetSelectedItems().length > 0)
                {
                    var i = 0;
                    var d = lstDepartment.GetSelectedItems()
                    
                    for(i = 0; i < d.length; i++)
                    {
                        if(d[i].value == "0")
                        {
                            var lstCnt = lstDepartment.GetItemCount();  
                            departments = "";                          
                            for (var lc = 0; lc < lstCnt; lc++)                             
                                departments += lstDepartment.GetItem(lc).value + ",";
                        }
                        else if(d[i].value == "-1")
                            departments = "";
                        else
                            departments += d[i].value +  ",";
                    }
                    
                    if(departments != "")
                        hdnRightMenuValue.value += "2:" + departments.substring(0, departments.length-1) + "|";                    
                    
                    //Assignments Menu Start
                    if(lstRmApprover.GetSelectedItems().length > 0)
                    {
                        var r = lstRmApprover.GetSelectedItems()
                    
                        for(i = 0; i < r.length; i++)
                        {
                           if(r[i].value == 5)
                            rmIncharge = "1,2,3,4,";
                           else
                            rmIncharge += r[i].value +  ",";
                        }
                        
                        if(rmIncharge != "")
                            hdnRightMenuValue.value += "3:" + rmIncharge.substring(0, rmIncharge.length-1) + "|";    
                    }
                    
                    if(lstMCUApprover.GetSelectedItems().length > 0)
                    {
                        lr = "";
                        var m = lstMCUApprover.GetSelectedItems()
                        
                        for(i = 0; i < m.length; i++)
                        {
                            if(m[i].value == 5)
                                mcuIncharge = "1,2,3,4,";
                            else
                                mcuIncharge += m[i].value +  ",";
                        }
                        
                        if(mcuIncharge != "")
                            hdnRightMenuValue.value += "4:" + mcuIncharge.substring(0, mcuIncharge.length-1) + "|";    
                    }
                    
                    if(lstWOAdmin.GetSelectedItems().length > 0)
                    {
                        var w = lstWOAdmin.GetSelectedItems()
                        for(i = 0; i < w.length; i++)
                        {
                            if(w[i].value == 3) 
                                woIncharge = "1,2,";
                            else                            
                                woIncharge += w[i].value +  ",";
                        }
                        if(woIncharge != "")
                            hdnRightMenuValue.value += "5:" + woIncharge.substring(0, woIncharge.length-1) + "|";    
                    }
                    //Assignments Menu End
                    
                    //Resource Menu Start
                    if(lstAttendee.GetSelectedItems().length > 0)
                    {
                        var at = lstAttendee.GetSelectedItems()
                        for(i = 0; i < at.length; i++)
                        {
                            if(at[i].value == 3) 
                                attendees = "1,2,";
                            else   
                                attendees += at[i].value +  ",";
                        }
                        
                        if(attendees != "")
                            hdnRightMenuValue.value += "6:" + attendees.substring(0, attendees.length-1) + "|";    
                    }
                    
                    if(lstRmType.GetSelectedItems().length > 0)
                    {
                        var rm = lstRmType.GetSelectedItems()
                        
                        for(i = 0; i < rm.length; i++)
                        {
                            if(rm[i].value == 3) 
                                attendees = "1,2,";
                            else                            
                                roomsParty += rm[i].value +  ",";
                        }
                        
                        if(roomsParty != "")
                            hdnRightMenuValue.value += "7:" + roomsParty.substring(0, roomsParty.length-1) + "|";    
                    }
                    
                    if(lstEPYes.GetSelectedItems().length > 0)
                    {
                        var ep = lstEPYes.GetSelectedItems()
                    
                        for(i = 0; i < ep.length; i++)
                            endPoints += ep[i].value +  ",";
                        
                        if(endPoints != "")
                            hdnRightMenuValue.value += "8:" + endPoints.substring(0, endPoints.length-1) + "|";    
                    }
                    
                    if(lstMCUYes.GetSelectedItems().length > 0)
                    {
                        var mc = lstMCUYes.GetSelectedItems()
                        for(i = 0; i < mc.length; i++)
                            mcu += mc[i].value +  ",";
                        
                        if(mcu != "")
                            hdnRightMenuValue.value += "9:" + mcu.substring(0, mcu.length-1) + "|";    
                    }
                    
                    if(lstConfSpeedYes.GetSelectedItems().length > 0)
                    {
                        var sp = lstConfSpeedYes.GetSelectedItems()
                        for(i = 0; i < sp.length; i++)
                            confSpeed += sp[i].value +  ",";
                        
                        if(confSpeed != "")
                            hdnRightMenuValue.value += "10:" + confSpeed.substring(0, confSpeed.length-1) + "|";    
                    }
                    var conProtocol = "";
                    if(lstConfProYes.GetSelectedItems().length > 0)
                    {
                        var pr = lstConfProYes.GetSelectedItems()
                        for(i = 0; i < pr.length; i++)
                            conProtocol += pr[i].value +  ",";
                        
                        if(conProtocol != "")
                            hdnRightMenuValue.value += "11:" + conProtocol.substring(0, conProtocol.length-1) + "|";    
                    }
                    
                    if(lstWOYes.GetSelectedItems().length > 0)
                    {
                        var wo = lstWOYes.GetSelectedItems()
                        for(i = 0; i < wo.length; i++)
                            confWO += wo[i].value +  ",";
                        
                        if(confWO != "")
                            hdnRightMenuValue.value += "12:" + confWO.substring(0, confWO.length-1) + "|";    
                    }
                    
                    //Resource Menu End                    
                }
            }
        }
        
        if(hdnRightMenuValue.value != "")
            Conference += "right,";
            
        if(Conference != "")
            hdnMainIPValue.value = "1:" + Conference.substring(0, Conference.length-1) + "|";
        
        if(chk_HFN.GetChecked())
            hosts += "h.FirstName as [Host First Name],";
        if(chk_HLN.GetChecked())
            hosts += "h.LastName as [Host Last Name],";
        if(chk_HMail.GetChecked())
            hosts += "h.Email as [Host Email],";
        if(chk_HRole.GetChecked())
            hosts += "hr.roleName as [Host Role],";
//        if(chk_HPerson.GetChecked())
//            hosts += "Person,";
            
        if(hosts != "")
            hdnMainIPValue.value += "2:" + hosts.substring(0, hosts.length-1) + "|";
            
        if(chk_PFN.GetChecked())
            participants += "p.FirstName as [Part. First Name],";
        if(chk_PLN.GetChecked())
            participants += "p.LastName as [Part. Last Name],";
        if(chk_PMail.GetChecked())
            participants += "p.Email as [Part. Email],";
        if(chk_PRole.GetChecked())
            participants += "p.roleName as [Part. Role],";
//        if(chk_PPerson.GetChecked())
//            participants += "Person,";
            
        if(participants != "")
            hdnMainIPValue.value += "3:" + participants.substring(0, participants.length-1) + "|";
        
        if(chk_Completed.GetChecked())
            scheduledConf += "7,";
        if(chk_Scheduled.GetChecked())
            scheduledConf += "0,";
        if(chk_Deleted.GetChecked())
            scheduledConf += "9,";
        if(chk_Terminated.GetChecked())
            scheduledConf += "3,";
        if(chk_Pending.GetChecked())
            scheduledConf += "1,";
            
        if(scheduledConf != "")
            hdnMainIPValue.value += "4:" + scheduledConf.substring(0, scheduledConf.length-1) + "|";
            
        if(chk_Audio.GetChecked())
            confTypes += "6,";
        if(chk_Video.GetChecked())
            confTypes += "2,";
        if(chk_Room.GetChecked())
            confTypes += "7,";    
        if(chk_PP.GetChecked())
            confTypes += "4,";   
//        if(chk_TP.GetChecked())
//            confTypes += "3,";      
       
        if(confTypes != "")
            hdnMainIPValue.value += "5:" + confTypes.substring(0, confTypes.length-1) + "|";
        
        if(chk_Single.GetChecked())
            confBehavior += "0,";
        if(chk_Recurring.GetChecked())
            confBehavior += "1,";          
       
        if(confBehavior != "")
            hdnMainIPValue.value += "6:" + confBehavior.substring(0, confBehavior.length-1) + "|";
            
        if(chk_Date.GetChecked())
            confTime += "ConfDate as [Date of Conf.],";
        if(chk_Time.GetChecked())
            confTime += "ConfTime [Start Time],";   
       
        if(confTime != "")
            hdnMainIPValue.value += "7:" + confTime.substring(0, confTime.length-1) + "|";
        //FB 2654
        var concierge = "";
        if(chk_Onsite.GetChecked())
            concierge += "O,";
        if(chk_Meet.GetChecked())
            concierge += "M,";
        if(chk_Monitor.GetChecked())
            concierge += "C,";
        if(chk_VNOC.GetChecked())
            concierge += "D,";
            
        if(concierge != "")
            hdnMainIPValue.value += "21:" + concierge.substring(0, concierge.length-1) + "|";
        
//        if(chk_Hours.GetChecked())
//            duraion += "Hours,";

        if(chk_Minutes.GetChecked())
            duraion += "a.Duration as [Duration (Mins)],";   //FB 2654     
       
        if(duraion != "")
            hdnMainIPValue.value += "8:" + duraion.substring(0, duraion.length-1);
    }
    
</script>

<%--Get Input Values from User Menu--%>
<script type="text/javascript">
    
    function fnGetUserMenuInput()
    {
        //hdnMainIPValue Values Start from 9
        var hdnMainIPValue = document.getElementById("hdnMainIPValue");
        var commonSelect = "";
        var conference = "";
        hdnMainIPValue.value = "";
        var hdnOkValue = document.getElementById("hdnOkValue");
        hdnOkValue.value = "1";
        
        var hdnRightMenuValue = document.getElementById("hdnRightMenuValue");
        
        if(chk_OrgU.GetChecked())
        {
            //conference += "OrgID,";
            hdnRightMenuValue.value = "";
            if(OrgList.GetSelectedItems().length >0)
            {
                var s = OrgList.GetSelectedItems()
                var departments = "";
                hdnRightMenuValue.value = "1:" + s[0].value + "|";
                if(lstDepartment.GetSelectedItems().length > 0)
                {
                    var i = 0;
                    var d = lstDepartment.GetSelectedItems()
                    
                    for(i = 0; i < d.length; i++)
                    {
                        if(d[i].value == "0")
                        {
                            var lstCnt = lstDepartment.GetItemCount();  
                            departments = "";                          
                            for (var lc = 0; lc < lstCnt; lc++)                             
                                departments += lstDepartment.GetItem(lc).value + ",";
                        }
                        else if(d[i].value == "-1")
                            departments = "";
                        else
                            departments += d[i].value +  ",";
                    }
                    
                    if(departments != "")
                        hdnRightMenuValue.value += "2:" + departments.substring(0, departments.length-1) + "|";     
                        
                    if(lstConfType.GetSelectedItems().length > 0)
                    {
                        var r = lstConfType.GetSelectedItems()
                        var conftypeVal = "";
                        for(i = 0; i < r.length; i++)
                        {
                           if(r[i].value == 4)
                            conftypeVal = "6,2,7,";
                           else
                            conftypeVal += r[i].value +  ",";
                        }
                        
                         if(conftypeVal != "")
                            hdnMainIPValue.value += "5:" + conftypeVal.substring(0, conftypeVal.length-1) + "|";    
                    }
                    
                    var rmIncharge = "";
                    var mcuIncharge = "";
                    var woIncharge = "";
                    //Assignments Menu Start
                    if(lstRmApprover.GetSelectedItems().length > 0)
                    {
                        var r = lstRmApprover.GetSelectedItems()
                    
                        for(i = 0; i < r.length; i++)
                        {
                           if(r[i].value == 5)
                            rmIncharge = "1,2,3,4,";
                           else
                            rmIncharge += r[i].value +  ",";
                        }
                        
                        if(rmIncharge != "")
                            hdnRightMenuValue.value += "3:" + rmIncharge.substring(0, rmIncharge.length-1) + "|";    
                    }
                    
                    if(lstMCUApprover.GetSelectedItems().length > 0)
                    {
                        lr = "";
                        var m = lstMCUApprover.GetSelectedItems()
                        
                        for(i = 0; i < m.length; i++)
                        {
                            if(m[i].value == 5)
                                mcuIncharge = "1,2,3,4,";
                            else
                                mcuIncharge += m[i].value +  ",";
                        }
                        
                        if(mcuIncharge != "")
                            hdnRightMenuValue.value += "4:" + mcuIncharge.substring(0, mcuIncharge.length-1) + "|";    
                    }
                    
                    if(lstWOAdmin.GetSelectedItems().length > 0)
                    {
                        var w = lstWOAdmin.GetSelectedItems()
                        for(i = 0; i < w.length; i++)
                        {
                            if(w[i].value == 3) 
                                woIncharge = "1,2,";
                            else                            
                                woIncharge += w[i].value +  ",";
                        }
                        if(woIncharge != "")
                            hdnRightMenuValue.value += "5:" + woIncharge.substring(0, woIncharge.length-1) + "|";    
                    }
                    //Assignments Menu End
                }
            }
        }        
        
        if(hdnRightMenuValue.value != "")
            conference += "right,";
        
        if(conference != "")
            hdnMainIPValue.value = "1:" + conference.substring(0, conference.length-1) + "|";
            
        var hosts = "";
        if(chk_ConfHostU.GetChecked())
            hosts += "h.FirstName as [Host First Name], h.LastName as [Host Last Name],";
            
        if(hosts != "")
            hdnMainIPValue.value += "2:" + hosts.substring(0, hosts.length-1) + "|";
          
        var participants = "";
        if(chk_ConfPartU.GetChecked())
            participants = "p.FirstName as [Part. First Name],p.LastName as [Part. Last Name],p.Email as [Part. Email],";
            
        if(participants != "")
            hdnMainIPValue.value += "3:" + participants.substring(0, participants.length-1) + "|";
        
        if(chk_FNU.GetChecked())
            commonSelect += "a.FirstName as [First Name],";
        if(chk_LNU.GetChecked())
            commonSelect += "a.LastName as [Last Name],";
        if(chk_UserEmailU.GetChecked())
            commonSelect += "a.Email,";
        if(chk_SecondaryEmailU.GetChecked())
            commonSelect += "a.AlternativeEmail as [Secondary Email],";
        if(chk_RoleU.GetChecked())
            commonSelect += "c.RoleName as [Role Name],";
        if(chk_TimeZoneU.GetChecked())
            commonSelect += "d.StandardName as [Time Zone],";
        if(chk_AccExpirU.GetChecked())
            commonSelect += "a.AccountExpiry as [Account Expiration],";
        if(chk_MinU.GetChecked())
            commonSelect += "e.Timeremaining as [Minutes],";
            
        if(commonSelect != "")
            hdnMainIPValue.value += "9:" + commonSelect.substring(0, commonSelect.length-1) + "|";
        
        if(chk_ExchangeU.GetChecked())                     
            hdnMainIPValue.value += "10:1|";
        
        if(chk_DominoU.GetChecked())                     
            hdnMainIPValue.value += "11:1|";
    }
</script>

<%--Get Input Values from Room Menu--%>
<script type="text/javascript">
    
    function ShowConfMenu(s)
    {   
    
//        if(s.GetSelectedItems().length >0)
//        {
            var items = s.GetSelectedItems()
            var isEptChecked = 0
            var isMCUChecked = 0
            var isWOChecked = 0
            
            for(i = 0; i < items.length; i++)
            {
                if(items[i].value == 3)
                    isEptChecked = 1;
                else if(items[i].value == 5)
                    isMCUChecked = 1;
                else if(items[i].value == 6)
                    isWOChecked = 1;
                else if(items[i].value == 7)
                {
                    isEptChecked = 1;
                    isWOChecked = 1;
                    isMCUChecked = 1;
                }
            }
            
            if(isEptChecked == 1)
                eptDetailsPopup.ShowAtPos(680, 310);
            else
                eptDetailsPopup.Hide();
                
            if(isMCUChecked == 1)
            {
                if("<%=Session["browser"]%>" == "Firefox") 
                    mcuDetailsPopup.ShowAtPos(365, 442);
                else
                    mcuDetailsPopup.ShowAtPos(365, 610);
            }
            else
                mcuDetailsPopup.Hide();
                
            if(isWOChecked == 1)
                woDetailsPopup.ShowAtPos(665, 665);
            else
                woDetailsPopup.Hide();
//        }
    }
    
    function fnBehaviorAllChecked()
    {   
        if(chk_VideoConferenceR.GetChecked() || chk_AudioConferenceR.GetChecked() || chk_RoomConferenceR.GetChecked() || chk_AllBehaviorR.GetChecked() 
        || chk_ConfE.GetChecked() || chk_ConfM.GetChecked()) 
            confStatusPopup.ShowAtPos(205, 285);
        else
        {
            fnRightParamsClear('C');
            
            confStatusPopup.Hide();
            confOccurencePopup.Hide();
            confDetailsPopup.Hide();
            eptDetailsPopup.Hide();
            woDetailsPopup.Hide();
            mcuDetailsPopup.Hide();
        }
    }
    
    function fnGetRoomMenuInput()
    {
        //hdnMainIPValue Values Start from 12
        //hdnRightMenuValue Values Start from 13
        var hdnMainIPValue = document.getElementById("hdnMainIPValue");
        hdnMainIPValue.value = "";
        var commonSelect = "";
        var conference = "";
        var hdnOkValue = document.getElementById("hdnOkValue");
        hdnOkValue.value = "1";
            
        if(chk_TiersR.GetChecked())
            commonSelect += " [Top Tier], [Middle Tier],";
        if(chk_NameR.GetChecked())
            commonSelect += " a.[Name],";
            
        var hdnRightMenuValue = document.getElementById("hdnRightMenuValue");
            hdnRightMenuValue.value = "";
            
        if(chk_OrgR.GetChecked())
        {
            //conference += "OrgID,";
            
            if(OrgList.GetSelectedItems().length >0)
            {
                var s = OrgList.GetSelectedItems()
                
                var departments = "";
                hdnRightMenuValue.value = "1:" + s[0].value + "|";
                if(lstDepartment.GetSelectedItems().length > 0)
                {
                    var i = 0;
                    var d = lstDepartment.GetSelectedItems()
                    
                    for(i = 0; i < d.length; i++)
                    {
                        if(d[i].value == "0")
                        {
                            var lstCnt = lstDepartment.GetItemCount();  
                            departments = "";                          
                            for (var lc = 0; lc < lstCnt; lc++)                             
                                departments += lstDepartment.GetItem(lc).value + ",";
                        }
                        else if(d[i].value == "-1")
                            departments = "";
                        else
                            departments += d[i].value +  ",";
                    }
                    
                    if(departments != "")
                        hdnRightMenuValue.value += "2:" + departments.substring(0, departments.length-1) + "|";     
                    
                }
            }
        }
        
        var rmIncharge = "";
       
        //Assignments Menu Start
        if(lstRmApprover.GetSelectedItems().length > 0)
        {
            var r = lstRmApprover.GetSelectedItems()
        
            for(i = 0; i < r.length; i++)
            {
               if(r[i].value == "5")
                rmIncharge = "1,2,3,4,";
               else
                rmIncharge += r[i].value +  ",";
            }
            
            if(rmIncharge != "")
                hdnRightMenuValue.value += "3:" + rmIncharge.substring(0, rmIncharge.length-1) + "|";    
        }
        //Assignments Menu End
                
        var tierInfo = "";
        if(lstTiers.GetSelectedItems().length > 0)
        {
            var ti = lstTiers.GetSelectedItems()
            for(i = 0; i < ti.length; i++)
                tierInfo += ti[i].value + ",";
        }
        
        if(tierInfo != "")
            hdnRightMenuValue.value += "20:" + tierInfo.substring(0, tierInfo.length-1) + "|";  
                        
        var rminfo = "";
        if(lstRmInfo.GetSelectedItems().length > 0)
        {
            var l = lstRmInfo.GetSelectedItems()
            for(i = 0; i < l.length; i++)
            {
                if(l[i].value == "1") commonSelect += " RoomPhone,";
                if(l[i].value == "2") commonSelect += " Capacity,";
                if(l[i].value == "3") commonSelect += " Address1,";
                if(l[i].value == "4") commonSelect += " Address2,";
                if(l[i].value == "5") commonSelect += " RoomFloor,";
                if(l[i].value == "6") commonSelect += " RoomNumber,";
                if(l[i].value == "7") commonSelect += " City,";
                if(l[i].value == "8") commonSelect += " State,";
                if(l[i].value == "9") commonSelect += " Country,";
                if(l[i].value == "10") commonSelect += " ZipCode,";
                if(l[i].value == "11") commonSelect += " TimeZone,";
                if(l[i].value == "12") commonSelect += " [Primary Approver],";
                if(l[i].value == "13") commonSelect += " [Sec. Approver 1],";
                if(l[i].value == "14") commonSelect += " [Sec. Approver 2],";
                
                if(l[i].value == "15")
                {
                    commonSelect += "RoomPhone,Capacity,Address1,Address2,RoomFloor,RoomNumber,City,State,Country,ZipCode,[Primary Approver]";
                    commonSelect += ",[Sec. Approver 1],[Sec. Approver 2],";
                }
            }
        }
        
         var rmAssetinfo = "";
        if(lstRmAsset.GetSelectedItems().length > 0)
        {
            var ra = lstRmAsset.GetSelectedItems()
            for(i = 0; i < ra.length; i++)
            {
                if(ra[i].value == "3")
                    rmAssetinfo = "1,2";
                else 
                    rmAssetinfo += ra[i].value;
            }
            
            if(rmAssetinfo != "")
                hdnRightMenuValue.value += "18:" + rmAssetinfo.substring(0, rmAssetinfo.length-1) + "|";    
        }
                
        if(hdnRightMenuValue.value != "")
            conference += "right,"
        
        if(conference != "")
            hdnMainIPValue.value = "1:" + conference.substring(0, conference.length-1) + "|";
            
        var hosts = "";
        if(chk_HostR.GetChecked())
            hosts += "h.FirstName as [Host First Name], h.LastName as [Host Last Name],";
            
        if(hosts != "")
            hdnMainIPValue.value += "2:" + hosts.substring(0, hosts.length-1) + "|";
        
        var rmType = "";
        if(chk_VideoRoomR.GetChecked())
            rmType += "2,";
        if(chk_AudioRoomR.GetChecked())
            rmType += "1,";
        if(chk_MeetingRoomR.GetChecked())
            rmType += "0,";  
        
        if(rmType != "")
            hdnMainIPValue.value += "12:" + rmType.substring(0, rmType.length-1) + "|";
            
        var confType = "";
        if(chk_VideoConferenceR.GetChecked())
            confType += "2,";
        if(chk_AudioConferenceR.GetChecked())
            confType += "6,";
        if(chk_RoomConferenceR.GetChecked())
            confType += "7,";  
        
        if(confType != "") //If Behavior Checkbox's clicked need to handle right menus
        {
            hdnMainIPValue.value += "5:" + confType.substring(0, confType.length-1) + "|";
        
            var scheduledConf = "";
            var immediate = "";
            if(lstConfStatus.GetSelectedItems().length > 0)
            {
                var cs = lstConfStatus.GetSelectedItems()
                        
                for(i = 0; i < cs.length; i++)
                {
                    if(cs[i].value == "15")
                        immediate = "1," ;
                    if(cs[i].value == "16")
                        scheduledConf = "7,0,9,3,1,";
                    else
                        scheduledConf += cs[i].value +  ",";
                }
            }
            
            if(scheduledConf != "")
                hdnMainIPValue.value += "4:" + scheduledConf.substring(0, scheduledConf.length-1) + "|";
            
            if(immediate != "")
                hdnMainIPValue.value += "13:" + immediate.substring(0, immediate.length-1) + "|";
            
            var behavConf = "";
            if(lstConfOccurence.GetSelectedItems().length > 0)
            {
                var co = lstConfOccurence.GetSelectedItems()
                for(i = 0; i < co.length; i++)
                {
                    if(co[i].value == "2")
                        behavConf = "0,1,";
                    else
                        behavConf += co[i].value +  ",";
                }
            }
           
            if(behavConf != "")
                hdnMainIPValue.value += "6:" + behavConf.substring(0, behavConf.length-1) + "|";
            
            var subConfSelect = "";
            if(lstConfDetails.GetSelectedItems().length > 0)
            {
                var cd = lstConfDetails.GetSelectedItems()
                
                var eptDetails = "";
                var mcuDetails = "";
                var woDetails = "";
                       
                for(i = 0; i < cd.length; i++)
                {
                    if(cd[i].value == "1")
                        subConfSelect += "ConfNumName,";
                    else if(cd[i].value == "2")
                        subConfSelect += "ExternalName,";
                    else if(cd[i].value == "3")
                    {
                        if(lstEptDetails.GetSelectedItems().length > 0)
                        {
                            var e = lstEptDetails.GetSelectedItems()
                            for(j = 0; j < e.length; j++)
                            {
                                if(e[j].value == "5")
                                    eptDetails = "1,2,3,4,";
                                else 
                                    eptDetails += e[j].value +  ",";
                            }
                                
                           if(eptDetails != "")
                                hdnMainIPValue.value += "15:" + eptDetails.substring(0, eptDetails.length-1) + "|";
                        }
                    }
                    else if(cd[i].value == "5")
                    {
                        if(lstMCUDetails.GetSelectedItems().length > 0)
                        {
                        var k = 0;
                            var m = lstMCUDetails.GetSelectedItems()
                            for(k = 0; k < m.length; k++)       
                            {
                                if(m[k].value == "4")                         
                                    mcuDetails = "1,2,3,";
                                else
                                    mcuDetails += m[k].value +  ",";
                            }
                                
                           if(mcuDetails != "")
                                hdnMainIPValue.value += "16:" + mcuDetails.substring(0, mcuDetails.length-1) + "|";
                        }
                    }
                    else if(cd[i].value == "6")
                    {
                        if(lstWODetails.GetSelectedItems().length > 0)
                        {
                            var m = lstWODetails.GetSelectedItems()
                            for(l = 0; l < m.length; l++)      
                            {                          
                                if(m[l].value == "4")
                                    woDetails =  "1,2,3,";
                                else
                                    woDetails += m[l].value +  ",";
                            }
                                
                           if(woDetails != "")
                                hdnMainIPValue.value += "17:" + woDetails.substring(0, woDetails.length-1) + "|";
                        }
                    }
                }
            }
           
            if(subConfSelect != "")
                hdnMainIPValue.value += "14:" + subConfSelect.substring(0, subConfSelect.length-1) + "|";
        }
       
        if(commonSelect == "")
            commonSelect = " a.[Name],";
            
        if(commonSelect != "")
            hdnMainIPValue.value += "9:" + commonSelect.substring(0, commonSelect.length-1) + "|";
        
    }
</script>

<%--Get Input Values from Endpoint Menu--%>
<script type="text/javascript">
    
    function fnGetEndPointMenuInput()
    {
        //hdnMainIPValue Values Start from 12
        //hdnRightMenuValue Values Start from 13
        var hdnMainIPValue = document.getElementById("hdnMainIPValue");
        var commonSelect = "";
        
        //if(chk_DeptE.GetChecked())
        //    commonSelect += "[Top Tier], [Middle Tier],";
        if(chk_RmNameE.GetChecked())
            commonSelect += " a.[Name],";
            
        var hdnOkValue = document.getElementById("hdnOkValue");
        hdnOkValue.value = "1";
        var hdnRightMenuValue = document.getElementById("hdnRightMenuValue");
        
        var conference = ""; 
        
        if(chk_OrgE.GetChecked() || chk_OrgM.GetChecked())
        {
            //conference += "OrgID,";            
            
            if(OrgList.GetSelectedItems().length >0)
            {
                var s = OrgList.GetSelectedItems()
                
                var departments = "";
                hdnRightMenuValue.value = "1:" + s[0].value + "|";
                if(lstDepartment.GetSelectedItems().length > 0)
                {
                    var i = 0;
                    var d = lstDepartment.GetSelectedItems()
                    
                    for(i = 0; i < d.length; i++)
                    {
                        if(d[i].value == "0")
                        {
                            var lstCnt = lstDepartment.GetItemCount();  
                            departments = "";                          
                            for (var lc = 0; lc < lstCnt; lc++)                             
                                departments += lstDepartment.GetItem(lc).value + ",";
                        }
                        else if(d[i].value == "-1")
                            departments = "";
                        else
                            departments += d[i].value +  ",";
                    }
                    
                    if(departments != "")
                        hdnRightMenuValue.value += "2:" + departments.substring(0, departments.length-1) + "|";     
                }
            }
        }
        
        
        var rmIncharge = "";
        var mcuIncharge = "";
        var woIncharge = "";
        //Assignments Menu Start
        if(lstRmApprover.GetSelectedItems().length > 0)
        {
            var r = lstRmApprover.GetSelectedItems()
        
            for(i = 0; i < r.length; i++)
            {
               if(r[i].value == "5")
                rmIncharge = "1,2,3,4,";
               else
                rmIncharge += r[i].value +  ",";
            }
            
            if(rmIncharge != "")
                hdnRightMenuValue.value += "3:" + rmIncharge.substring(0, rmIncharge.length-1) + "|";    
        }
        if(lstMCUApprover.GetSelectedItems().length > 0)
        {
            lr = "";
            var m = lstMCUApprover.GetSelectedItems()
            
            for(i = 0; i < m.length; i++)
            {
                if(m[i].value == 5)
                    mcuIncharge = "1,2,3,4,";
                else
                    mcuIncharge += m[i].value +  ",";
            }
            
            if(mcuIncharge != "")
                hdnRightMenuValue.value += "4:" + mcuIncharge.substring(0, mcuIncharge.length-1) + "|";    
        }
        
        if(lstWOAdmin.GetSelectedItems().length > 0)
        {
            var w = lstWOAdmin.GetSelectedItems()
            for(i = 0; i < w.length; i++)
            {
                if(w[i].value == 3) 
                    woIncharge = "1,2,";
                else                            
                    woIncharge += w[i].value +  ",";
            }
            if(woIncharge != "")
                hdnRightMenuValue.value += "5:" + woIncharge.substring(0, woIncharge.length-1) + "|";    
        }
        
        //Assignments Menu End
        
        var tierInfo = "";
        if(lstTiers.GetSelectedItems().length > 0)
        {
            var ti = lstTiers.GetSelectedItems()
            for(i = 0; i < ti.length; i++)
                tierInfo += ti[i].value + ",";
        }
        
        if(tierInfo != "")
            hdnRightMenuValue.value += "20:" + tierInfo.substring(0, tierInfo.length-1) + "|";  
            
        var rminfo = "";
        if(lstRmInfo.GetSelectedItems().length > 0)
        {
            var l = lstRmInfo.GetSelectedItems()
            for(i = 0; i < l.length; i++)
            {
                if(l[i].value == "1") commonSelect += " RoomPhone,";
                if(l[i].value == "2") commonSelect += " Capacity,";
                if(l[i].value == "3") commonSelect += " Address1,";
                if(l[i].value == "4") commonSelect += " Address2,";
                if(l[i].value == "5") commonSelect += " RoomFloor,";
                if(l[i].value == "6") commonSelect += " RoomNumber,";
                if(l[i].value == "7") commonSelect += " City,";
                if(l[i].value == "8") commonSelect += " State,";
                if(l[i].value == "9") commonSelect += " Country,";
                if(l[i].value == "10") commonSelect += " ZipCode,";
                if(l[i].value == "11") commonSelect += " TimeZone,";
                if(l[i].value == "12") commonSelect += " [Primary Approver],";
                if(l[i].value == "13") commonSelect += " [Sec. Approver 1],";
                if(l[i].value == "14") commonSelect += " [Sec. Approver 2],";
                
                if(l[i].value == "15")
                {
                    commonSelect += "RoomPhone,Capacity,Address1,Address2,RoomFloor,RoomNumber,City,State,Country,ZipCode,[Primary Approver]";
                    commonSelect += ",[Sec. Approver 1],[Sec. Approver 2],";
                }
            }
        }
        
        var rmAssetinfo = "";
        if(lstRmAsset.GetSelectedItems().length > 0)
        {
            var ra = lstRmAsset.GetSelectedItems()
            for(i = 0; i < ra.length; i++)
            {
                if(ra[i].value == "3")
                    rmAssetinfo = "1,2";
                else 
                    rmAssetinfo += ra[i].value;
            }
            
            if(rmAssetinfo != "")
                hdnRightMenuValue.value += "18:" + rmAssetinfo.substring(0, rmAssetinfo.length-1) + "|";    
        }
        
        var eptInfo = "";
        
        if(chk_ENameE.GetChecked())
            eptInfo += "[Endpoint Name],";
            
        if(lstEptDetailsM.GetSelectedItems().length > 0)
        {
            var ed = lstEptDetailsM.GetSelectedItems()
            for(i = 0; i < ed.length; i++)
            {
                if(ed[i].value == "1") eptInfo += " [Default Profile Name],";
                if(ed[i].value == "2") eptInfo += " [Number of Profiles],";
                if(ed[i].value == "3") eptInfo += " [Address Type],";
                if(ed[i].value == "4") eptInfo += " Address,";
                if(ed[i].value == "5") eptInfo += " Model,";
                if(ed[i].value == "6") eptInfo += " [Preferred Bandwidth],";
                if(ed[i].value == "7") eptInfo += " [MCU Assignment],";
                if(ed[i].value == "8") eptInfo += " [Pre. Dialing Option],";
                if(ed[i].value == "9") eptInfo += " [Default Protocol],";
                if(ed[i].value == "10") eptInfo += " [Web Access URL],";
                if(ed[i].value == "11") eptInfo += " [Network Location],";                            
                if(ed[i].value == "12") eptInfo += " [Telnet Enabled],";
                if(ed[i].value == "13") eptInfo += " [Email ID],";
                if(ed[i].value == "14") eptInfo += " [API Port],";
                if(ed[i].value == "15") eptInfo += " [iCal Invite],";
                
                if(ed[i].value == "16")
                {
                     eptInfo = " [Endpoint Name],[Default Profile Name], [Number of Profiles], [Address Type], Address, Model, [Preferred Bandwidth],";
                     eptInfo += " [MCU Assignment], [Pre. Dialing Option], [Default Protocol], [Web Access URL], [Network Location], ";
                     eptInfo += " [Primary Approver], [Telnet Enabled], [Email ID], [API Port], [iCal Invite],";
                }
            }
            
            if(eptInfo != "")
                hdnMainIPValue.value += "19:" + eptInfo.substring(0, eptInfo.length-1) + "|";
        }
        
        if(hdnRightMenuValue.value != "")
            conference += "right,"
            
        if(conference != "")
            hdnMainIPValue.value += "1:" + conference.substring(0, conference.length-1) + "|";
                
        if(chk_ConfE.GetChecked())//If Behavior Checkbox's clicked need to handle right menus
        {
            var scheduledConf = "";
            var immediate = "";
            if(lstConfStatus.GetSelectedItems().length > 0)
            {
                var cs = lstConfStatus.GetSelectedItems()
                        
                for(i = 0; i < cs.length; i++)
                {
                    if(cs[i].value == "9")
                        immediate = "1," ;
                    if(cs[i].value == "16")
                        scheduledConf = "7,0,9,3,1,";
                    else
                        scheduledConf += cs[i].value +  ",";
                }
            }
            
            if(scheduledConf != "")
                hdnMainIPValue.value += "4:" + scheduledConf.substring(0, scheduledConf.length-1) + "|";
            
            if(immediate != "")
                hdnMainIPValue.value += "13:" + immediate.substring(0, immediate.length-1) + "|";
            
            var behavConf = "";
            if(lstConfOccurence.GetSelectedItems().length > 0)
            {
                var co = lstConfOccurence.GetSelectedItems()
                for(i = 0; i < co.length; i++)
                {
                    if(co[i].value == "2")
                        behavConf = "0,1,";
                    else
                        behavConf += co[i].value +  ",";
                }
            }
           
            if(behavConf != "")
                hdnMainIPValue.value += "6:" + behavConf.substring(0, behavConf.length-1) + "|";
            
            var subConfSelect = "";
            if(lstConfDetails.GetSelectedItems().length > 0)
            {
                var cd = lstConfDetails.GetSelectedItems()
                
                var eptDetails = "";
                var mcuDetails = "";
                var woDetails = "";
                       
                for(i = 0; i < cd.length; i++)
                {
                    if(cd[i].value == "1")
                        subConfSelect += "ConfNumName,";
                    else if(cd[i].value == "2")
                        subConfSelect += "ExternalName,";
                    else if(cd[i].value == "3")
                    {
                        if(lstEptDetails.GetSelectedItems().length > 0)
                        {
                            var e = lstEptDetails.GetSelectedItems()
                            for(i = 0; i < e.length; i++)                                
                                eptDetails += e[i].value +  ",";
                                
                           if(eptDetails != "")
                                hdnMainIPValue.value += "15:" + eptDetails.substring(0, eptDetails.length-1) + "|";
                        }
                    }
                    else if(cd[i].value == "5")
                    {
                        if(lstMCUDetails.GetSelectedItems().length > 0)
                        {
                            var m = lstMCUDetails.GetSelectedItems()
                            for(i = 0; i < m.length; i++)                                
                                mcuDetails += m[i].value +  ",";
                                
                           if(mcuDetails != "")
                                hdnMainIPValue.value += "16:" + mcuDetails.substring(0, mcuDetails.length-1) + "|";
                        }
                    }
                    else if(cd[i].value == "6")
                    {
                        if(lstWODetails.GetSelectedItems().length > 0)
                        {
                            var m = lstWODetails.GetSelectedItems()
                            for(i = 0; i < m.length; i++)                                
                                woDetails += m[i].value +  ",";
                                
                           if(woDetails != "")
                                hdnMainIPValue.value += "17:" + woDetails.substring(0, woDetails.length-1) + "|";
                        }
                    }
                }
            }
           
            if(subConfSelect != "")
                hdnMainIPValue.value += "14:" + subConfSelect.substring(0, subConfSelect.length-1) + "|";
        }
       
            
        if(commonSelect != "")
            hdnMainIPValue.value += "9:" + commonSelect.substring(0, commonSelect.length-1) + "|";
    }
</script>

<%--Get Input Values from MCU Menu--%>
<script type="text/javascript">
    
    function fnGetMCUMenuInput()
    {
    
        //hdnMainIPValue Values Start from 12
        //hdnRightMenuValue Values Start from 13
        var hdnMainIPValue = document.getElementById("hdnMainIPValue");
        var commonSelect = "";
        hdnMainIPValue.value = "";
        //if(chk_DeptE.GetChecked())
        //    commonSelect += "[Top Tier], [Middle Tier],";
        if(chk_RmNameE.GetChecked() || chk_RmNameM.GetChecked())
            commonSelect += " a.[Name] as [Room Name],";
            
        var hdnOkValue = document.getElementById("hdnOkValue");
        hdnOkValue.value = "1";
        var hdnRightMenuValue = document.getElementById("hdnRightMenuValue");
        
        var conference = ""; 
        
        if(chk_OrgE.GetChecked() || chk_OrgM.GetChecked())
        {
            //conference += "OrgID,";            
            
            if(OrgList.GetSelectedItems().length >0)
            {
                var s = OrgList.GetSelectedItems()
                
                var departments = "";
                hdnRightMenuValue.value = "1:" + s[0].value + "|";
            }
        }
        
        var tierInfo = "";
        if((chk_RmNameE.GetChecked() || chk_SpecRoomM.GetChecked() || chk_RmNameM.GetChecked()) && lstTiers.GetSelectedItems().length > 0)
        {
            var ti = lstTiers.GetSelectedItems()
            for(i = 0; i < ti.length; i++)
            {
                if(ti[i].value == "1") tierInfo += " Tier1 as [Tier 1],";
                if(ti[i].value == "2") tierInfo += " Tier2 as [Tier 2],";
                
                if(ti[i].value == "3")
                    tierInfo += " Tier1 as [Tier 1], Tier2 as [Tier 2],";
            }
        }
        
        if(tierInfo != "")
            hdnMainIPValue.value += "20:" + tierInfo.substring(0, tierInfo.length-1) + "|"; 
            
        var mcu = "";
        if(chk_SpecMCUM.GetChecked())
        {
            if(lstMCU.GetSelectedItems().length > 0)
            {
                var m = lstMCU.GetSelectedItems();
                for(i = 0; i < m.length; i++)
                {
                    if(m[i].value == "0")
                    {
                        var lstCnt = lstMCU.GetItemCount();  
                        mcu = "";                          
                        for (var lc = 0; lc < lstCnt; lc++)                             
                            mcu += lstMCU.GetItem(lc).value + ",";
                    }
                    else if(m[i].value == "-1")
                        mcu = "";
                    else
                        mcu += m[i].value +  ",";
                }
            }
        }
        
        if(mcu != "")
            hdnMainIPValue.value += "24:" + mcu.substring(0, mcu.length-1) + "|";   
        
        var specEpt = "";
        if(chk_SpecEptM.GetChecked())
        {
            if(lstendPt.GetSelectedItems().length > 0)
            {
                var m = lstendPt.GetSelectedItems();
                for(i = 0; i < m.length; i++)
                {
                    if(m[i].value == "0")
                    {
                        var lstCnt = lstendPt.GetItemCount();  
                        specEpt = "";                          
                        for (var lc = 0; lc < lstCnt; lc++)                             
                            specEpt += lstendPt.GetItem(lc).value + ",";
                    }
                    else if(m[i].value == "-1")
                        specEpt = "";
                    else
                        specEpt += m[i].value +  ",";
                }
            }
        }
        
        if(specEpt != "")
            hdnMainIPValue.value += "26:" + specEpt.substring(0, specEpt.length-1) + "|";   
            
        var specRoom = "";
        if(chk_SpecRoomM.GetChecked())
        {
            if(lstRm.GetSelectedItems().length > 0)
            {
                var m = lstRm.GetSelectedItems();
                for(i = 0; i < m.length; i++)
                {
                    if(m[i].value == "0")
                    {
                        var lstCnt = lstRm.GetItemCount();  
                        specRoom = "";                          
                        for (var lc = 0; lc < lstCnt; lc++)                             
                            specRoom += lstRm.GetItem(lc).value + ",";
                    }
                    else if(m[i].value == "-1")
                        specRoom = "";
                    else
                        specRoom += m[i].value +  ",";
                }
            }
        }
        
        if(specRoom != "")
            hdnMainIPValue.value += "25:" + specRoom.substring(0, specRoom.length-1) + "|";                 
            
        var rminfo = "";
        if((chk_RmNameE.GetChecked() || chk_SpecRoomM.GetChecked() || chk_RmNameM.GetChecked()) && lstRmInfo.GetSelectedItems().length > 0)
        {
            var l = lstRmInfo.GetSelectedItems()
            for(i = 0; i < l.length; i++)
            {
                if(l[i].value == "1") commonSelect += " RoomPhone,";
                if(l[i].value == "2") commonSelect += " Capacity,";
                if(l[i].value == "3") commonSelect += " Address1,";
                if(l[i].value == "4") commonSelect += " Address2,";
                if(l[i].value == "5") commonSelect += " RoomFloor,";
                if(l[i].value == "6") commonSelect += " RoomNumber,";
                if(l[i].value == "7") commonSelect += " City,";
                if(l[i].value == "8") commonSelect += " State,";
                if(l[i].value == "9") commonSelect += " Country,";
                if(l[i].value == "10") commonSelect += " ZipCode,";
                if(l[i].value == "11") commonSelect += " TimeZone,";
                if(l[i].value == "12") commonSelect += " [Primary Approver],";
                if(l[i].value == "13") commonSelect += " [Sec. Approver 1],";
                if(l[i].value == "14") commonSelect += " [Sec. Approver 2],";
                
                if(l[i].value == "15")
                {
                    commonSelect += "RoomPhone,Capacity,Address1,Address2,RoomFloor,RoomNumber,City,State,Country,ZipCode,[Primary Approver]";
                    commonSelect += ",[Sec. Approver 1],[Sec. Approver 2],";
                }
            }
        }
        
        var eptInfo = "";
        
        if(chk_ENameE.GetChecked() || chk_ENameM.GetChecked())
            eptInfo += "[Endpoint Name],";
            
        if((chk_ENameE.GetChecked() || chk_ENameM.GetChecked()) && lstEptDetailsM.GetSelectedItems().length > 0)
        {
            var ed = lstEptDetailsM.GetSelectedItems()
            for(i = 0; i < ed.length; i++)
            {
                if(ed[i].value == "1") eptInfo += " [Default Profile Name],";
                if(ed[i].value == "2") eptInfo += " [Number of Profiles],";
                if(ed[i].value == "3") eptInfo += " [Address Type],";
                if(ed[i].value == "4") eptInfo += " Address,";
                if(ed[i].value == "5") eptInfo += " Model,";
                if(ed[i].value == "6") eptInfo += " [Preferred Bandwidth],";
                if(ed[i].value == "7") eptInfo += " [MCU Assignment],";
                if(ed[i].value == "8") eptInfo += " [Pre. Dialing Option],";
                if(ed[i].value == "9") eptInfo += " [Default Protocol],";
                if(ed[i].value == "10") eptInfo += " [Web Access URL],";
                if(ed[i].value == "11") eptInfo += " [Network Location],";                            
                if(ed[i].value == "12") eptInfo += " [Telnet Enabled],";
                if(ed[i].value == "13") eptInfo += " [Email ID],";
                if(ed[i].value == "14") eptInfo += " [API Port],";
                if(ed[i].value == "15") eptInfo += " [iCal Invite],";
                
                if(ed[i].value == "16")
                {
                     eptInfo = " [Endpoint Name],[Default Profile Name], [Number of Profiles], [Address Type], Address, Model, [Preferred Bandwidth],";
                     eptInfo += " [MCU Assignment], [Pre. Dialing Option], [Default Protocol], [Web Access URL], [Network Location], ";
                     eptInfo += " [Primary Approver], [Telnet Enabled], [Email ID], [API Port], [iCal Invite],";
                }
            }
            
            if(eptInfo != "")
                hdnMainIPValue.value += "19:" + eptInfo.substring(0, eptInfo.length-1) + "|";
        }
        
        if(hdnRightMenuValue.value != "")
            conference += "right,"
            
        if(conference != "")
            hdnMainIPValue.value += "1:" + conference.substring(0, conference.length-1) + "|";
                
        if(chk_ConfE.GetChecked() || chk_ConfM.GetChecked())//If Behavior Checkbox's clicked need to handle right menus
        {
            hdnMainIPValue.value += "23:1|";
            
            var scheduledConf = "";
            var immediate = "";
            if(lstConfStatus.GetSelectedItems().length > 0)
            {
                var cs = lstConfStatus.GetSelectedItems()
                        
                for(i = 0; i < cs.length; i++)
                {
                    if(cs[i].value == "9")
                        immediate = "1," ;
                    if(cs[i].value == "16")
                        scheduledConf = "7,0,9,3,1,";
                    else
                        scheduledConf += cs[i].value +  ",";
                }
            }
            
            if(scheduledConf != "")
                hdnMainIPValue.value += "4:" + scheduledConf.substring(0, scheduledConf.length-1) + "|";
            
            if(immediate != "")
                hdnMainIPValue.value += "13:" + immediate.substring(0, immediate.length-1) + "|";
            
            var behavConf = "";
            if(lstConfOccurence.GetSelectedItems().length > 0)
            {
                var co = lstConfOccurence.GetSelectedItems()
                for(i = 0; i < co.length; i++)
                {
                    if(co[i].value == "2")
                        behavConf = "0,1,";
                    else
                        behavConf += co[i].value +  ",";
                }
            }
           
            if(behavConf != "")
                hdnMainIPValue.value += "6:" + behavConf.substring(0, behavConf.length-1) + "|";
            
            var subConfSelect = "";
            if(lstConfDetails.GetSelectedItems().length > 0)
            {
                var cd = lstConfDetails.GetSelectedItems()
                
                var eptDetails = "";
                var mcuDetails = "";
                var woDetails = "";
                var confDetails = "";       
                for(c = 0; c < cd.length; c++)
                {
                    if(cd[c].value == "7")
                    {
                        subConfSelect = "";eptDetails=""; mcuDetails = ""; woDetails = ""; confDetails = "";
                    }
                    if(cd[c].value == "1" || cd[c].value == "7")
                        subConfSelect += "ConfNumName,";
                    if(cd[c].value == "2" || cd[c].value == "7")
                        subConfSelect += "ExternalName,";
                    if(cd[c].value == "3" || cd[c].value == "7")
                    {
                        if(lstEptDetails.GetSelectedItems().length > 0)
                        {
                            var e = lstEptDetails.GetSelectedItems()
                            for(i = 0; i < e.length; i++)                                
                            {
                                if(e[i].value == "5") 
                                    eptDetails = "1,2,3,4,";
                                else
                                    eptDetails += e[i].value +  ",";
                                
                            }   
                           if(eptDetails != "")
                                confDetails += "15:" + eptDetails.substring(0, eptDetails.length-1) + "|";
                        }
                    }
                    if(cd[c].value == "5" || cd[c].value == "7")
                    {
                        if(lstMCUDetails.GetSelectedItems().length > 0)
                        {
                            var m = lstMCUDetails.GetSelectedItems()
                            for(i = 0; i < m.length; i++)                                
                            {
                                if(m[i].value == "4")
                                    mcuDetails = "1,2,3,";
                                else
                                    mcuDetails += m[i].value +  ",";
                            }   
                           if(mcuDetails != "")
                                confDetails += "16:" + mcuDetails.substring(0, mcuDetails.length-1) + "|";
                        }
                    }
                    if(cd[c].value == "6" || cd[c].value == "7")
                    {
                        if(lstWODetails.GetSelectedItems().length > 0)
                        {
                            var m = lstWODetails.GetSelectedItems()
                            for(i = 0; i < m.length; i++)                                
                            {                          
                                if(m[i].value == "4")
                                    woDetails =  "1,2,3,";
                                else
                                    woDetails += m[i].value +  ",";
                            }
                                
                           if(woDetails != "")
                                confDetails += "17:" + woDetails.substring(0, woDetails.length-1) + "|";
                        }
                    }
                    
                }
                
                if(confDetails != "")
                    hdnMainIPValue.value += confDetails;
            }
           
            if(subConfSelect != "")
                hdnMainIPValue.value += "14:" + subConfSelect.substring(0, subConfSelect.length-1) + "|";
        }
            
        if(commonSelect != "")
            hdnMainIPValue.value += "9:" + commonSelect.substring(0, commonSelect.length-1) + "|";
    }
</script>

<%--Get Input Values from CDR Menu--%>
<script type="text/javascript">
    
    function fnGetCDRMenuInput()
    {
    
        var hdnMainIPValue = document.getElementById("hdnMainIPValue");
        var commonSelect = "";
        var hdnOkValue = document.getElementById("hdnOkValue");
        var hdnCDROption = document.getElementById("hdnCDROption");
        
        hdnOkValue.value = "1";
        
        if(rbConferenceCDR.GetChecked())
        {
            commonSelect += "C,";
            hdnCDROption.value = "C";
        }
        else
        {
            commonSelect += "V,";
            hdnCDROption.value = "V";
        }   
        if(commonSelect != "")
            hdnMainIPValue.value = "22:" + commonSelect.substring(0, commonSelect.length-1) + "|";
        //FB 2944    
		var conference = "";
        var hdnRightMenuValue = document.getElementById("hdnRightMenuValue");
        if(OrgList.GetSelectedItems().length >0)
        {
            var s = OrgList.GetSelectedItems()
            
            var departments = "";
            hdnRightMenuValue.value = "1:" + s[0].value + "|";
        }
        if(hdnRightMenuValue.value != "")
            conference += "right,"
            
        if(conference != "")
            hdnMainIPValue.value += "1:" + conference.substring(0, conference.length-1) + "|";
        var mcu = "";
        if(lstMCU.GetSelectedItems().length > 0)
        {
            var m = lstMCU.GetSelectedItems();
            for(i = 0; i < m.length; i++)
            {
                if(m[i].value == "0")
                {
                    var lstCnt = lstMCU.GetItemCount();  
                    mcu = "";                          
                    for (var lc = 0; lc < lstCnt; lc++)                             
                        mcu += lstMCU.GetItem(lc).value + ",";
                }
                else if(m[i].value == "-1")
                    mcu = "";
                else
                    mcu += m[i].value +  ",";
            }
        }
        
        if(mcu != "")
            hdnMainIPValue.value += "24:" + mcu.substring(0, mcu.length-1) + "|";   
    }
</script>

<%--Save funtion--%>
<script  type="text/javascript">

function fnSave()
{

    //var errLabel = document.getElementById('errLabel');
    
    if(txtSaveReport.GetValue().trim() == "Report Name..." || txtSaveReport.GetValue().trim() == "")
    {
        alert("Please enter the valid Report Name.");
//        if(errLabel)
//        {
//            errLabel.innerText = "Please enter the valid Report Name.";
//            errLabel.style.display = "";
//        }
        return false;
    }
    else
    {
        var hdnSave = document.getElementById("hdnSave");
        var hdnRptSave = document.getElementById("hdnRptSave");
        
        hdnSave.value = "1";
        fnSubmit();
        fnClose();
        if(hdnRptSave)
            hdnRptSave.value = "1";
        DataLoading(1); //ZD 100176
        return true;
        //cmdConfOk.DoClick();
    }
}
       

function OntextFocus(s, e) 
{
    var ele = s.GetInputElement();
	if (ele.value == "Report Name...")
		ele.value = "";
	else if(ele.value.trim() == "")
		ele.value = "Report Name...";
}

function OntextunFocus(s, e) 
{
    var ele = s.GetInputElement();
	
	if(ele.value.trim() == "")
		ele.value = "Report Name...";
}

function fnValidation()
{
    var sDate = startDateedit.GetDate();
    var eDate = endDateedit.GetDate();
    
    if(sDate == null || eDate == null)
    {
        var isExport = confirm("You have not selected any start/end time. This request may take a long time are you sure you want to proceed.!")
        
        if(isExport == false)
            return false;
        else
            return true;
    }
    
    return true;
}

//FB 2808
function fnClearRptValue()
{
    var hdnReportSelection = document.getElementById("hdnReportSelection");
    
    SavedRptListPopup.Hide()
    hdnReportSelection.value = "";
    
    fnClearListSelection(lstReportList);
}

//FB 2808
function fnClearErrlbl()
{
    var errLabel =  document.getElementById("errLabel");
    if(errLabel)
    {
        errLabel.innerText = "";
        errLabel.innerHTML = "";
    }
}
//FB 2801 Start
function fndaterange() 
{
    if(rbAdhoc.GetChecked())
    {
        var sDate = startDateedit.GetDate();
        var eDate = endDateedit.GetDate();

        if (sDate == null || eDate == null) 
        {
            alert("Please enter date range")
            return false;
        }
        DataLoading(1); //ZD 100176
    }
    return true;
}
//FB 2801 End

    
</script>

<html xmlns="http://www.w3.org/1999/xhtml">
<head> 
    <title>Report</title>    
    <script type="text/javascript" src="inc/functions.js"></script>
    <%--FB 2886--%>
    <style type="text/css">   
    LABEL 
    {
        FONT-SIZE: 8pt;
        VERTICAL-ALIGN: top;
        COLOR: black;
        FONT-FAMILY: Arial, Helvetica;
        TEXT-DECORATION: none;
    }    
    .altLongSmallButtonFormat
    {
    	width:132pt;
    }
    .altLongSmallButtonFormat:active
    {
    	width:132pt;
    }
    .altLongSmallButtonFormat:hover
    {
    	width:132pt;
    }
    .altBlueButtonFormat
    {
    	width:40pt;
    }
    .altBlueButtonFormat:active
    {
    	width:40pt;
    }
    .altBlueButtonFormat:hover
    {
    	width:40pt;
    }
    .btndisable
    {
    	width:40pt;
    }
    
    
    </style>
</head>
<body>
    <form id="frmReport" runat="server">   
    <asp:ScriptManager ID="ScriptManager1" runat="server" > </asp:ScriptManager>
    <input type="hidden" id="hdnMainIPValue" runat="server" />
    <input type="hidden" id="hdnDepartmentList" runat="server" />
    <input type="hidden" id="hdnRightMenuValue" runat="server" />
    <input type="hidden" id="hdnOkValue" runat="server" />
    <input type="hidden" id="hdnSubmitValue" runat="server" />
    <input type="hidden" id="hdnSave" runat="server" />
    <input type="hidden" id="hdnReportNames" runat="server" />
    <input type="hidden" id="hdnAllReports" runat="server" />
    <input type="hidden" id="hdnAdmin" runat="server" /> <%--FB 2593--%>
    <input type="hidden" id="hdnMCUList" runat="server" />
    <input type="hidden" id="hdnEptList" runat="server" />
    <input type="hidden" id="hdnRoomList" runat="server" />
    <input type="hidden" id="hdnRptSave" runat="server" /> <%--FB 2593--%>
    <input type="hidden" id="hdnCDROption" runat="server" /> <%--FB 2593--%>
    <input type="hidden" id="hdnReportSelection" runat="server" /><%--FB 2808--%>
	<input type="hidden" id="hdnGroup" runat="server" /><%--FB 2965--%>
    <%--FB 2886 Start--%>
	<table width="100%" >
    <tr>
            <td colspan="2"  align="center" height="10px" >
                <h3> 
                      <asp:Label ID="lblHeading" runat="server">Advanced Reports</asp:Label><!-- FB 2570 -->
                </h3>                
            </td>
        </tr>
    </table>
    <div id="dataLoadingDIV" align="center"></div> <%--ZD 100176--%>
    <table width="100%" border="1" style="border-color:Gray;border-style:solid;" cellpadding="0" cellspacing="0">
        
        <tr>
            <td colspan="2" >
                <table width="100%">
                    <tr>
                        <td align="center">
                            <asp:Label ID="errLabel" runat="server" Text="" CssClass="lblError" ></asp:Label><br />
                        </td>
                    </tr>
                    <tr>
                        <td >
                            <table width="100%" border="0" style="left: 0;" id="tbleBtns">
                                <tr>
                                    <td width="10%" class="blackblodtext">Report Type
                                         <dx:ASPxRadioButton ID="rbAdhoc" runat="server" Text="Adhoc" GroupName="ReportType" Checked="True" TextWrap="False" ClientInstanceName="rbAdhoc">                                            
                                            <ClientSideEvents CheckedChanged="function(s, e){fnClearRptValue();btnPreview.style.display='';}" /><%--FB 2808 FB 2886--%>
                                        </dx:ASPxRadioButton>  
                                    </td>
                                    <td width="8%" class="blackblodtext">&nbsp;
                                        <dx:ASPxRadioButton ID="rbSaved" runat="server" Text="Saved" GroupName="ReportType"  TextWrap="False" ClientInstanceName="rbSaved">   
                                            <ClientSideEvents CheckedChanged="function(s, e){fnClearErrlbl(); btnPreview.style.display='None';}" /> <%--FB 2808 FB 2886--%>
                                        </dx:ASPxRadioButton> 
                                    </td>
                                    <td width="10%" class="blackblodtext">Start Date
                                       <dx:ASPxDateEdit ID="startDateedit" ClientInstanceName="startDateedit" runat="server" Width="100px">                                        
                                       </dx:ASPxDateEdit>
                                    </td>
                                    <td width="15%" class="blackblodtext">End Date
                                       <dx:ASPxDateEdit ID="endDateedit" ClientInstanceName="endDateedit" runat="server" Width="100px">                                       
                                       </dx:ASPxDateEdit>
                                    </td>	
                                     <td width="10%" class="blackblodtext" valign="middle"> <%--FB 2886--%><br />
                                       <asp:Button ID="btnPreview" runat="server" CssClass="altShortBlueButtonFormat"  OnClick="btnOk_Click" 
                                        OnClientClick="javascript:return fndaterange();fnCheck();fnSubmit();" Text="View" />         
                                    </td>							                                    
                                    <td align="right">
                                        <dx:ASPxMenu ID="menuExport" runat="server" AutoSeparators="RootOnly" SeparatorHeight="100%" SeparatorWidth="3px"
                                            CssClass="altLong1BlueButtonFormat"  ItemSpacing="2px" ItemStyle-ForeColor="White" ItemStyle-Font-Bold = "true" 
                                             OnItemClick="menuExport_OnItemClick">
                                            <Items>
                                                <dx:MenuItem Text="XLS" Name="mXls" ToolTip="Export to Excel"><%-- FB 3034--%>
                                                    <Image Url="../en/App_Themes/Plastic Blue/Web/Ntable-excel.png" />
                                                </dx:MenuItem>
                                                <dx:MenuItem Text="PDF" Name="mPdf" ToolTip="Export to PDF"><%-- FB 3034--%>
                                                    <Image Url="../en/App_Themes/Plastic Blue/Web/NPDFpage_white_acrobat.png" />
                                                </dx:MenuItem>
                                                <dx:MenuItem Text="RTF" Name="mDoc"  ToolTip="Export to Word"><%-- FB 3034--%>
                                                    <Image Url="../en/App_Themes/Plastic Blue/Web/Nwordpage_white_word.png" />
                                                </dx:MenuItem>                                               
                                            </Items>
                                            <ItemSubMenuOffset FirstItemY="-1" LastItemY="-1" Y="-1" />
                                            <RootItemSubMenuOffset FirstItemX="1" LastItemX="1" X="1" />
                                            <SubMenuStyle GutterWidth="0px" />
                                            <ClientSideEvents ItemClick="function(s, e) {e.processOnServer = fnValidation();} " />
                                        </dx:ASPxMenu>
                                        <span class="lblError"> * Please click 'View' after making your selections.</span>
                                    </td>
                                </tr>
                             </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td width="10%" style="border-right-color:Gray;" valign="top"> <%--FB 2050 FB 2886--%>
                 <table id="tblLeft" style="padding-right:5px" cellpadding="1" cellspacing="0" border="0" width="100%">
                    <tr>
                        <td>           
                            <ajax:ModalPopupExtender ID="ConferencePopUp" runat="server" TargetControlID="ChgOrg" BackgroundCssClass="modalBackground"
                                 PopupControlID="ConferencePnl" DropShadow="false" Drag="true" CancelControlID="ClosePUp">
                            </ajax:ModalPopupExtender>                
                            <asp:Panel ID="ConferencePnl" runat="server" HorizontalAlign="Center" style="width:100%;height:100%; overflow:auto;"><%--FB 2654--%>
                                <table width="100%">
                                    <tr>
                                        <td><%--FB 2886--%>
                                            <dx:ASPxPopupControl ID="MainPopup" runat="server" HeaderStyle-CssClass = "altLongSmallButtonFormat"  HeaderStyle-ForeColor="White" HeaderStyle-Font-Bold = "true"
                                                HeaderText="Conference Selection" 
                                                 PopupHorizontalAlign="WindowCenter"  HeaderStyle-HorizontalAlign="Center"
                                                PopupVerticalAlign="WindowCenter" ShowPageScrollbarWhenModal="True" ClientInstanceName="MainPopup"
                                                CloseAction="None" ShowCloseButton="False">                                                
                                                                                               
                                                <ContentCollection>
                                                    <dx:PopupControlContentControl>
                                                        <table width="100%" cellpadding="0" cellspacing="0" border="0"> <%--FB 2654--%>
                                                            <tr>
                                                                <td colspan="3">
                                                                   <span style="color:Red;font-size:smaller;"> * - Submenu multiple Select/Deselect use Ctrl+Click </span>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td colspan="3">
                                                                    <table width="100%" cellpadding="0" cellspacing="0" border="0" id="tblConference" style="display:none;" >
                                                                        <tr valign="top">
                                                                            <td width="50%" align="left" >
                                                                                <dx:ASPxLabel ID="ASPxLabel13" runat="server"  Text="Conference:" ForeColor="Black"></dx:ASPxLabel>
                                                                            </td>
                                                                            <td valign="top" align="right" ><%--FB 2886--%>
                                                                                <asp:Button ID="btnCClear" runat="server" CssClass="altShortBlueButtonFormat"  UseSubmitBehavior="false"
                                                                                    OnClientClick="javascript:return fnClear();" Text="Clear" />                                                                                 
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td width="50%" align="left"  colspan="2">
                                                                                <dx:ASPxCheckBox ID="chk_ConfID" runat="server" ClientInstanceName="chk_ConfID" Text="Conference ID" Font-Size="5px">
                                                                                </dx:ASPxCheckBox>
                                                                                <dx:ASPxCheckBox ID="chk_ConfTitle" runat="server" ClientInstanceName="chk_ConfTitle" Text="Conference Title">
                                                                                </dx:ASPxCheckBox>
                                                                                <%--FB 2870 Start--%>
                                                                                <dx:ASPxCheckBox ID="chk_CTSNumericID" runat="server" ClientInstanceName="chk_CTSNumericID" Text="CTS Numeric ID">
                                                                                </dx:ASPxCheckBox>
                                                                                <%--FB 2870 End--%>
                                                                                <dx:ASPxCheckBox ID="chk_Org" runat="server" ClientInstanceName="chk_Org" Text="Organization">
                                                                                    <ClientSideEvents CheckedChanged="OnShowButtonClick" />
                                                                                </dx:ASPxCheckBox>
                                                                            </td>
                                                                        </tr>
                                                                        <tr valign="top">
                                                                            <td align="left" colspan="2">
                                                                                <dx:ASPxLabel ID="ASPxLabel1" runat="server" Font-Bold="True" Text="  Hosts:" ForeColor="Black">
                                                                                </dx:ASPxLabel>
                                                                            </td>
                                                                        </tr>
                                                                        <tr valign="top">
                                                                            <td align="left">                                                                  
                                                                                <dx:ASPxCheckBox ID="chk_HFN" runat="server" ClientInstanceName="chk_HFN" Text="First Name">
                                                                                    <ClientSideEvents CheckedChanged="function(s, e){if(!s.GetChecked()) chk_AllHost.SetChecked(false);}" />
                                                                                </dx:ASPxCheckBox>
                                                                                <dx:ASPxCheckBox ID="chk_HLN" runat="server" ClientInstanceName="chk_HLN" Text="Last Name">
                                                                                    <ClientSideEvents CheckedChanged="function(s, e){if(!s.GetChecked()) chk_AllHost.SetChecked(false);}" />
                                                                                </dx:ASPxCheckBox>
                                                                                <dx:ASPxCheckBox ID="chk_HMail" runat="server" ClientInstanceName="chk_HMail" Text="Email">
                                                                                    <ClientSideEvents CheckedChanged="function(s, e){if(!s.GetChecked()) chk_AllHost.SetChecked(false);}" />
                                                                                </dx:ASPxCheckBox>
                                                                            </td>
                                                                            <td>                                                                     
                                                                                <dx:ASPxCheckBox ID="chk_HRole" runat="server" ClientInstanceName="chk_HRole" Text="Role">
                                                                                    <ClientSideEvents CheckedChanged="function(s, e){if(!s.GetChecked()) chk_AllHost.SetChecked(false);}" />
                                                                                </dx:ASPxCheckBox>
                                                                                <dx:ASPxCheckBox ID="chk_HPerson" runat="server" ClientInstanceName="chk_HPerson" Text="Person" Enabled="false">
                                                                                    <ClientSideEvents CheckedChanged="function(s, e){if(!s.GetChecked()) chk_AllHost.SetChecked(false);}" />
                                                                                </dx:ASPxCheckBox>
                                                                                <dx:ASPxCheckBox ID="chk_AllHost" runat="server" ClientInstanceName="chk_AllHost" Text="All">
                                                                                    <ClientSideEvents CheckedChanged="function(s, e) {fnHostAll();}" />
                                                                                </dx:ASPxCheckBox>
                                                                            </td>
                                                                        </tr>
                                                                        <tr valign="top">
                                                                            <td align="left" colspan="2">
                                                                                <dx:ASPxLabel ID="ASPxLabel5" runat="server" Font-Bold="True" Text="  Participants:" ForeColor="Black">
                                                                                </dx:ASPxLabel>
                                                                            </td>
                                                                        </tr>
                                                                        <tr valign="top">
                                                                            <td align="left">                                                                
                                                                                <dx:ASPxCheckBox ID="chk_PFN" runat="server" ClientInstanceName="chk_PFN" Text="First Name">
                                                                                    <ClientSideEvents CheckedChanged="function(s, e){if(!s.GetChecked()) chk_AllPerson.SetChecked(false);}" />
                                                                                </dx:ASPxCheckBox>
                                                                                <dx:ASPxCheckBox ID="chk_PLN" runat="server" ClientInstanceName="chk_PLN" Text="Last Name">
                                                                                    <ClientSideEvents CheckedChanged="function(s, e){if(!s.GetChecked()) chk_AllPerson.SetChecked(false);}" />
                                                                                </dx:ASPxCheckBox>
                                                                                <dx:ASPxCheckBox ID="chk_PMail" runat="server" ClientInstanceName="chk_PMail" Text="Email">
                                                                                    <ClientSideEvents CheckedChanged="function(s, e){if(!s.GetChecked()) chk_AllPerson.SetChecked(false);}" />
                                                                                </dx:ASPxCheckBox>
                                                                            </td>
                                                                            <td>                                                                     
                                                                                <dx:ASPxCheckBox ID="chk_PRole" runat="server" ClientInstanceName="chk_PRole" Text="Role">
                                                                                    <ClientSideEvents CheckedChanged="function(s, e){if(!s.GetChecked()) chk_AllPerson.SetChecked(false);}" />
                                                                                </dx:ASPxCheckBox>
                                                                                <dx:ASPxCheckBox ID="chk_PPerson" runat="server" ClientInstanceName="chk_PPerson" Text="Person"  Enabled="false">
                                                                                    <ClientSideEvents CheckedChanged="function(s, e){if(!s.GetChecked()) chk_AllPerson.SetChecked(false);}" />
                                                                                </dx:ASPxCheckBox>
                                                                                <dx:ASPxCheckBox ID="chk_AllPerson" runat="server" ClientInstanceName="chk_AllPerson" Text="All">
                                                                                    <ClientSideEvents CheckedChanged="function(s, e) {fnPersonAll();}" />
                                                                                </dx:ASPxCheckBox>
                                                                            </td>
                                                                        </tr>
                                                                        <tr valign="top">
                                                                            <td align="left" colspan="2">
                                                                                <dx:ASPxLabel ID="ASPxLabel2" runat="server" Font-Bold="True" Text="  Scheduled Conferences:"
                                                                                    ForeColor="Black">
                                                                                </dx:ASPxLabel>
                                                                            </td>
                                                                        </tr>
                                                                        <tr valign="top">
                                                                            <td align="left" >                                                                
                                                                                <dx:ASPxCheckBox ID="chk_Completed" runat="server" ClientInstanceName="chk_Completed" Text="Completed">
                                                                                    <ClientSideEvents CheckedChanged="function(s, e){if(!s.GetChecked()) chk_AllConf.SetChecked(false);}" />
                                                                                </dx:ASPxCheckBox>
                                                                                <dx:ASPxCheckBox ID="chk_Scheduled" runat="server" ClientInstanceName="chk_Scheduled" Text="Scheduled">
                                                                                    <ClientSideEvents CheckedChanged="function(s, e){if(!s.GetChecked()) chk_AllConf.SetChecked(false);}" />
                                                                                </dx:ASPxCheckBox>
                                                                                <dx:ASPxCheckBox ID="chk_Deleted" runat="server" ClientInstanceName="chk_Deleted" Text="Deleted">
                                                                                    <ClientSideEvents CheckedChanged="function(s, e){if(!s.GetChecked()) chk_AllConf.SetChecked(false);}" />
                                                                                </dx:ASPxCheckBox>
                                                                            </td>
                                                                            <td>
                                                                                <dx:ASPxCheckBox ID="chk_Terminated" runat="server" ClientInstanceName="chk_Terminated" Text="Terminated">
                                                                                    <ClientSideEvents CheckedChanged="function(s, e){if(!s.GetChecked()) chk_AllConf.SetChecked(false);}" />
                                                                                </dx:ASPxCheckBox>
                                                                                <dx:ASPxCheckBox ID="chk_Pending" runat="server" ClientInstanceName="chk_Pending" Text="Pending">
                                                                                    <ClientSideEvents CheckedChanged="function(s, e){if(!s.GetChecked()) chk_AllConf.SetChecked(false);}" />
                                                                                </dx:ASPxCheckBox>
                                                                                <dx:ASPxCheckBox ID="chk_AllConf" runat="server" ClientInstanceName="chk_AllConf" Text="All">
                                                                                    <ClientSideEvents CheckedChanged="function(s, e) {fnScheduledAll();}" />
                                                                                </dx:ASPxCheckBox>
                                                                            </td>
                                                                        </tr>
                                                                        <tr valign="top">
                                                                            <td align="left" colspan="2">
                                                                                <dx:ASPxLabel ID="ASPxLabel4" runat="server" Font-Bold="True" Text="  Conference Types:" ForeColor="Black">
                                                                                </dx:ASPxLabel>
                                                                            </td>
                                                                        </tr>
                                                                        <tr valign="top">
                                                                            <td align="left">                                                                    
                                                                                <dx:ASPxCheckBox ID="chk_Audio" runat="server" ClientInstanceName="chk_Audio" Text="Audio Only">
                                                                                    <ClientSideEvents CheckedChanged="function(s, e){if(!s.GetChecked()) chk_AllConfTypes.SetChecked(false);}" />
                                                                                </dx:ASPxCheckBox>
                                                                                <dx:ASPxCheckBox ID="chk_Video" runat="server" ClientInstanceName="chk_Video" Text="Video Only">
                                                                                    <ClientSideEvents CheckedChanged="function(s, e){if(!s.GetChecked()) chk_AllConfTypes.SetChecked(false);}" />
                                                                                </dx:ASPxCheckBox>
                                                                                <dx:ASPxCheckBox ID="chk_Room" runat="server" ClientInstanceName="chk_Room" Text="Room Only">
                                                                                    <ClientSideEvents CheckedChanged="function(s, e){if(!s.GetChecked()) chk_AllConfTypes.SetChecked(false);}" />
                                                                                </dx:ASPxCheckBox>
                                                                            </td>
                                                                            <td>
                                                                                 <dx:ASPxCheckBox ID="chk_PP" runat="server" ClientInstanceName="chk_PP" Text="Point to Point" >
                                                                                    <ClientSideEvents CheckedChanged="function(s, e){if(!s.GetChecked()) chk_AllConfTypes.SetChecked(false);}" />
                                                                                </dx:ASPxCheckBox>
                                                                                <dx:ASPxCheckBox ID="chk_TP" runat="server" ClientInstanceName="chk_TP" Text="Telepresence" Enabled="False">
                                                                                </dx:ASPxCheckBox>
                                                                                <dx:ASPxCheckBox ID="chk_AllConfTypes" runat="server" ClientInstanceName="chk_AllConfTypes" Text="All">
                                                                                    <ClientSideEvents CheckedChanged="function(s, e) {fnConfTypeAll();}" />
                                                                                </dx:ASPxCheckBox>
                                                                            </td>
                                                                        </tr>
                                                                         <tr valign="top">
                                                                            <td align="left" colspan="2">
                                                                                <dx:ASPxLabel ID="ASPxLabel6" runat="server" Font-Bold="True" Text="  Conferences Behavior:" ForeColor="Black">
                                                                                </dx:ASPxLabel>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td >                                                                   
                                                                                <dx:ASPxCheckBox ID="chk_Single" runat="server" ClientInstanceName="chk_Single" Text="Single">
                                                                                    <ClientSideEvents CheckedChanged="function(s, e){if(!s.GetChecked()) chk_AllMode.SetChecked(false);}" />
                                                                                </dx:ASPxCheckBox>
                                                                                <dx:ASPxCheckBox ID="chk_Recurring" runat="server" ClientInstanceName="chk_Recurring" Text="Recurring">
                                                                                    <ClientSideEvents CheckedChanged="function(s, e){if(!s.GetChecked()) chk_AllMode.SetChecked(false);}" />
                                                                                </dx:ASPxCheckBox>
                                                                            </td>
                                                                            <td valign="top">
                                                                                <dx:ASPxCheckBox ID="chk_AllMode" runat="server" ClientInstanceName="chk_AllMode" Text="All">
                                                                                    <ClientSideEvents CheckedChanged="function(s, e) {fnRecurAll();}" />
                                                                                </dx:ASPxCheckBox>
                                                                            </td>
                                                                        </tr>
                                                                         <tr valign="top">
                                                                            <td align="left" colspan="2">
                                                                                <dx:ASPxLabel ID="ASPxLabecon" runat="server" Font-Bold="True" Text="  Conference Support:" ForeColor="Black"><%--FB 3023--%>
                                                                                </dx:ASPxLabel>
                                                                            </td>
                                                                        </tr>
                                                                        <tr valign="top">
                                                                            <td align="left" colspan="2">      <%--FB 2654--%>                                                              
                                                                                <dx:ASPxCheckBox ID="chk_Onsite" runat="server" ClientInstanceName="chk_Onsite" Text="On-Site A/V Support">
                                                                                    <ClientSideEvents CheckedChanged="function(s, e){if(!s.GetChecked()) chk_AllConcierge.SetChecked(false);}" />
                                                                                </dx:ASPxCheckBox>
                                                                                <dx:ASPxCheckBox ID="chk_Meet" runat="server" ClientInstanceName="chk_Meet" Text="Meet and Greet">
                                                                                    <ClientSideEvents CheckedChanged="function(s, e){if(!s.GetChecked()) chk_AllConcierge.SetChecked(false);}" />
                                                                                </dx:ASPxCheckBox>
                                                                                <dx:ASPxCheckBox ID="chk_Monitor" runat="server" ClientInstanceName="chk_Monitor" Text="Call Monitoring"> <%--FB 3023--%>
                                                                                    <ClientSideEvents CheckedChanged="function(s, e){if(!s.GetChecked()) chk_AllConcierge.SetChecked(false);}" />
                                                                                </dx:ASPxCheckBox>
                                                                                 <dx:ASPxCheckBox ID="chk_VNOC" runat="server" ClientInstanceName="chk_VNOC" Text="Dedicated VNOC Operator" >
                                                                                    <ClientSideEvents CheckedChanged="function(s, e){if(!s.GetChecked()) chk_AllConcierge.SetChecked(false);}" />
                                                                                </dx:ASPxCheckBox>
                                                                                <dx:ASPxCheckBox ID="chk_AllConcierge" runat="server" ClientInstanceName="chk_AllConcierge" Text="All">
                                                                                    <ClientSideEvents CheckedChanged="function(s, e) {fnConceirgeAll();}" />
                                                                                </dx:ASPxCheckBox>
                                                                            </td>
                                                                        </tr>
                                                                        <tr valign="top">
                                                                            <td align="left" colspan="2">
                                                                                <dx:ASPxLabel ID="ASPxLabel7" runat="server" Font-Bold="True" Text="  Conferences Time:" ForeColor="Black">
                                                                                </dx:ASPxLabel>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>      
                                                                                <dx:ASPxCheckBox ID="chk_Date" runat="server" ClientInstanceName="chk_Date" Text="Date">
                                                                                </dx:ASPxCheckBox>
                                                                            </td>
                                                                            <td>
                                                                                <dx:ASPxCheckBox ID="chk_Time" runat="server" ClientInstanceName="chk_Time" Text="Time">
                                                                                </dx:ASPxCheckBox>
                                                                            </td>
                                                                        </tr>
                                                                         <tr valign="top">
                                                                            <td align="left" colspan="2">
                                                                                <dx:ASPxLabel ID="ASPxLabel8" runat="server" Font-Bold="True" ClientInstanceName="ASPxLabel8" Text=" Duration:" ForeColor="Black">
                                                                                </dx:ASPxLabel>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>
                                                                                <dx:ASPxCheckBox ID="chk_Minutes" runat="server" ClientInstanceName="chk_Minutes" Text="Minutes">
                                                                                </dx:ASPxCheckBox>
                                                                            </td>
                                                                            <td>                                                                   
                                                                                <dx:ASPxCheckBox ID="chk_Hours" runat="server" ClientInstanceName="chk_Hours" Text="Hours" Visible="False"> <%--FB 2654--%>
                                                                                </dx:ASPxCheckBox>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                            <tr> 
                                                                <td colspan="3">
                                                                    <table width="100%" cellpadding="0" cellspacing="0" id="tblUser" style="display:none;">
                                                                        <tr valign="top">
                                                                            <td width="50%" align="left">
                                                                                <dx:ASPxLabel ID="ASPxLabel14" runat="server"  Text="  Conference:"
                                                                                    ForeColor="Black">
                                                                                </dx:ASPxLabel>
                                                                            </td>
                                                                             <td valign="top" align="right"><%--FB 2886--%>
                                                                                <asp:Button ID="btnUClear" runat="server" CssClass="altShortBlueButtonFormat"  UseSubmitBehavior="false"
                                                                                    OnClientClick="javascript:return fnClear();" Text="Clear" />
                                                                            </td>
                                                                        </tr>
                                                                        <tr valign="top">
                                                                            <td width="50%" align="left" colspan="2">   
                                                                                <dx:ASPxCheckBox ID="chk_OrgU" runat="server" ClientInstanceName="chk_OrgU" Text="Organization" Font-Size="5px">
                                                                                    <ClientSideEvents CheckedChanged="function(s,e){OnShowButtonClick('popupU');}" />
                                                                                </dx:ASPxCheckBox>
                                                                                <dx:ASPxCheckBox ID="chk_DeptU" runat="server" ClientInstanceName="chk_DeptU" Text="Department">                                                                                    
                                                                                </dx:ASPxCheckBox>
                                                                                <dx:ASPxCheckBox ID="chk_FNU" runat="server" ClientInstanceName="chk_FNU" Text="First Name">
                                                                                </dx:ASPxCheckBox>
                                                                                <dx:ASPxCheckBox ID="chk_LNU" runat="server" ClientInstanceName="chk_LNU" Text="Last Name">
                                                                                </dx:ASPxCheckBox>
                                                                                 <dx:ASPxCheckBox ID="chk_PersonU" runat="server" ClientInstanceName="chk_PersonU" Text="Person" Enabled="false">
                                                                                </dx:ASPxCheckBox>
                                                                                <hr />
                                                                                <dx:ASPxCheckBox ID="chk_UserEmailU" runat="server" ClientInstanceName="chk_UserEmailU" Text="User Email">
                                                                                </dx:ASPxCheckBox>
                                                                                <dx:ASPxCheckBox ID="chk_SecondaryEmailU" runat="server" ClientInstanceName="chk_SecondaryEmailU" Text="Secondary Email">
                                                                                </dx:ASPxCheckBox>
                                                                                <dx:ASPxCheckBox ID="chk_RoleU" runat="server" ClientInstanceName="chk_RoleU" Text="Role">
                                                                                </dx:ASPxCheckBox>
                                                                                <dx:ASPxCheckBox ID="chk_TimeZoneU" runat="server" ClientInstanceName="chk_TimeZoneU" Text="Time Zone">
                                                                                </dx:ASPxCheckBox>
                                                                                <dx:ASPxCheckBox ID="chk_ConfHostU" runat="server" ClientInstanceName="chk_ConfHostU" Text="Conference Host">
                                                                                </dx:ASPxCheckBox>
                                                                                <dx:ASPxCheckBox ID="chk_ConfPartU" runat="server" ClientInstanceName="chk_ConfPartU" Text="Conference Participant">
                                                                                </dx:ASPxCheckBox>
                                                                                <hr />
                                                                                <dx:ASPxCheckBox ID="chk_AccExpirU" runat="server" ClientInstanceName="chk_AccExpirU" Text="Account Expiration">
                                                                                </dx:ASPxCheckBox>
                                                                                <dx:ASPxCheckBox ID="chk_MinU" runat="server" ClientInstanceName="chk_MinU" Text="Minutes">
                                                                                </dx:ASPxCheckBox>
                                                                                <dx:ASPxCheckBox ID="chk_ExchangeU" runat="server" ClientInstanceName="chk_ExchangeU" Text="Exchange">
                                                                                </dx:ASPxCheckBox>
                                                                                <dx:ASPxCheckBox ID="chk_DominoU" runat="server" ClientInstanceName="chk_DominoU" Text="Domino">
                                                                                </dx:ASPxCheckBox>
                                                                            </td>
                                                                            
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>    
                                                             <tr> 
                                                                <td colspan="3">
                                                                    <table width="100%" cellpadding="0" cellspacing="0" id="tblRoom" style="display:none;">
                                                                        <tr valign="top">
                                                                            <td width="50%" align="left" >   
                                                                                <dx:ASPxLabel ID="ASPxLabel15" runat="server"  Text="  Location:"
                                                                                    ForeColor="Black">
                                                                                </dx:ASPxLabel>     
                                                                            <td valign="top" align="right"><%--FB 2886--%>
                                                                                <asp:Button ID="btnRClear" runat="server" CssClass="altShortBlueButtonFormat"  UseSubmitBehavior="false"
                                                                                    OnClientClick="javascript:return fnClear();" Text="Clear" />
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td width="50%" align="left" colspan="2">
                                                                                <dx:ASPxCheckBox ID="chk_OrgR" runat="server" ClientInstanceName="chk_OrgR" Text="Organization" Font-Size="5px">
                                                                                    <ClientSideEvents CheckedChanged="function(s,e){OnShowButtonClick('popupR');}" />
                                                                                </dx:ASPxCheckBox>
                                                                                <dx:ASPxCheckBox ID="chk_DeptR" runat="server" ClientInstanceName="chk_DeptR" Text="Department">
                                                                                    <%--<ClientSideEvents CheckedChanged="function(s,e){OnShowButtonClick('dptPopupChk');}" />--%>
                                                                                </dx:ASPxCheckBox>
                                                                                <dx:ASPxCheckBox ID="chk_TiersR" runat="server" ClientInstanceName="chk_TiersR" Text="Tiers">
                                                                                    <ClientSideEvents CheckedChanged="function(s,e){OnShowButtonClick('assignmentPopup');}" />
                                                                                </dx:ASPxCheckBox>
                                                                                <dx:ASPxCheckBox ID="chk_NameR" runat="server" ClientInstanceName="chk_NameR" Text="Room Name">
                                                                                    <ClientSideEvents CheckedChanged="function(s,e){OnShowButtonClick('rmInfoPopup');}" />
                                                                                </dx:ASPxCheckBox>
                                                                                 <dx:ASPxCheckBox ID="chk_HostR" runat="server" ClientInstanceName="chk_HostR" Text="Host">
                                                                                </dx:ASPxCheckBox>
                                                                                <dx:ASPxLabel ID="ASPxLabel16" runat="server"  Text="  Room Type:"
                                                                                    ForeColor="Black">
                                                                                </dx:ASPxLabel>
                                                                                <dx:ASPxCheckBox ID="chk_VideoRoomR" runat="server" ClientInstanceName="chk_VideoRoomR" Text="Video Room">
                                                                                    <ClientSideEvents CheckedChanged="function(s, e) {if(!s.GetChecked()) chk_AllTypeR.SetChecked(false);}" />
                                                                                </dx:ASPxCheckBox>
                                                                                <dx:ASPxCheckBox ID="chk_AudioRoomR" runat="server" ClientInstanceName="chk_AudioRoomR" Text="Audio Room">
                                                                                    <ClientSideEvents CheckedChanged="function(s, e) {if(!s.GetChecked()) chk_AllTypeR.SetChecked(false);}" />
                                                                                </dx:ASPxCheckBox>
                                                                                <dx:ASPxCheckBox ID="chk_MeetingRoomR" runat="server" ClientInstanceName="chk_MeetingRoomR" Text="Meeting Room">
                                                                                    <ClientSideEvents CheckedChanged="function(s, e) {if(!s.GetChecked()) chk_AllTypeR.SetChecked(false);}" />
                                                                                </dx:ASPxCheckBox>
                                                                                <dx:ASPxCheckBox ID="chk_AllTypeR" runat="server" ClientInstanceName="chk_AllTypeR" Text="All Type">
                                                                                    <ClientSideEvents CheckedChanged="function(s, e) {fnRmTypeAll();}" />
                                                                                </dx:ASPxCheckBox>
                                                                                <dx:ASPxLabel ID="ASPxLabel17" runat="server"  Text="  Behavior:"
                                                                                    ForeColor="Black">
                                                                                </dx:ASPxLabel>
                                                                                <dx:ASPxCheckBox ID="chk_VideoConferenceR" runat="server" ClientInstanceName="chk_VideoConferenceR" Text="Video Conference">
                                                                                    <ClientSideEvents CheckedChanged="function(s, e) {if(!s.GetChecked()) chk_AllBehaviorR.SetChecked(false);fnBehaviorAllChecked();}" />
                                                                                </dx:ASPxCheckBox>
                                                                                <dx:ASPxCheckBox ID="chk_AudioConferenceR" runat="server" ClientInstanceName="chk_AudioConferenceR" Text="Audio Conference">
                                                                                    <ClientSideEvents CheckedChanged="function(s, e) {if(!s.GetChecked()) chk_AllBehaviorR.SetChecked(false);fnBehaviorAllChecked();}" />
                                                                                </dx:ASPxCheckBox>
                                                                                <dx:ASPxCheckBox ID="chk_RoomConferenceR" runat="server" ClientInstanceName="chk_RoomConferenceR" Text="Room Conference">
                                                                                    <ClientSideEvents CheckedChanged="function(s, e) {if(!s.GetChecked()) chk_AllBehaviorR.SetChecked(false);fnBehaviorAllChecked();}" />
                                                                                </dx:ASPxCheckBox>
                                                                                <dx:ASPxCheckBox ID="chk_AllBehaviorR" runat="server" ClientInstanceName="chk_AllBehaviorR" Text="All Types">
                                                                                    <ClientSideEvents CheckedChanged="function(s, e) {fnBehaviorAll();}" />
                                                                                </dx:ASPxCheckBox>
                                                                                <hr />
                                                                                <dx:ASPxLabel ID="ASPxLabel18" runat="server"  Text="  Other:"
                                                                                    ForeColor="Black" Visible="false" >
                                                                                </dx:ASPxLabel>
                                                                                <dx:ASPxCheckBox ID="chk_UserAssignmentR" runat="server" ClientInstanceName="chk_UserAssignmentR" Text="User Assignment" Visible="false">
                                                                                    <ClientSideEvents CheckedChanged="function(s,e){OnShowButtonClick('assignment');}" />
                                                                                </dx:ASPxCheckBox>
                                                                                <dx:ASPxCheckBox ID="chk_MaximumCapacityR" runat="server" ClientInstanceName="chk_MaximumCapacityR" Text="Maximum Capacity" Visible="false">
                                                                                </dx:ASPxCheckBox>
                                                                                <dx:ASPxCheckBox ID="chk_AssetsR" runat="server" ClientInstanceName="chk_AssetsR" Text="Assets" Visible="false">
                                                                                    <ClientSideEvents CheckedChanged="function(s,e){OnShowButtonClick('rmAssetPopup');}" />
                                                                                </dx:ASPxCheckBox>
                                                                                <dx:ASPxCheckBox ID="chk_ScheduledDurationR" runat="server" Visible="false" ClientInstanceName="chk_ScheduledDurationR" Text="Scheduled Duration">
                                                                                </dx:ASPxCheckBox>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>      
                                                            <tr> 
                                                                <td colspan="3">
                                                                    <table width="100%" cellpadding="0" cellspacing="0" id="tblEndPoint" style="display:none;">
                                                                        <tr valign="top">
                                                                            <td width="50%" align="left" >   
                                                                                <dx:ASPxLabel ID="ASPxLabel19" runat="server"  Text="  End Point:"
                                                                                    ForeColor="Black">
                                                                                </dx:ASPxLabel>   
                                                                            </td>
                                                                            <td valign="top" align="right"><%--FB 2886--%>
                                                                                <asp:Button ID="btnEClear" runat="server" CssClass="altShortBlueButtonFormat"  UseSubmitBehavior="false"
                                                                                    OnClientClick="javascript:return fnClear();" Text="Clear" />
                                                                            </td>  
                                                                        </tr>                                                                        
                                                                        <tr>
                                                                            <td width="50%" align="left" colspan="2">
                                                                                <dx:ASPxCheckBox ID="chk_OrgE" runat="server" ClientInstanceName="chk_OrgE" Text="Organization" Font-Size="5px">
                                                                                    <ClientSideEvents CheckedChanged="function(s,e){OnShowButtonClick('popupE');}" />
                                                                                </dx:ASPxCheckBox>
                                                                                <dx:ASPxCheckBox ID="chk_DeptE" runat="server" Visible="false" ClientInstanceName="chk_DeptE" Text="Department">
                                                                                    <%--<ClientSideEvents CheckedChanged="function(s,e){OnShowButtonClick('dptPopupChk');}" />--%>
                                                                                </dx:ASPxCheckBox>
                                                                                <dx:ASPxCheckBox ID="chk_ENameE" runat="server" ClientInstanceName="chk_ENameE" Text="Endpoint Name">
                                                                                    <ClientSideEvents CheckedChanged="function(s,e){OnShowButtonClick('assignmentPopup');}" />
                                                                                </dx:ASPxCheckBox>
                                                                                <dx:ASPxCheckBox ID="chk_SpecEptE" runat="server" ClientInstanceName="chk_SpecEptE" Text="Specific Endpoint" Enabled="false">
                                                                                </dx:ASPxCheckBox>
                                                                                 <dx:ASPxCheckBox ID="chk_RmNameE" runat="server" ClientInstanceName="chk_RmNameE" Text="Room Name">
                                                                                    <ClientSideEvents CheckedChanged="function(s,e){OnShowButtonClick('assignmentPopup');}" />
                                                                                </dx:ASPxCheckBox>
                                                                                <dx:ASPxCheckBox ID="chk_SpecRoomE" runat="server" ClientInstanceName="chk_SpecRoomE" Text="Specific Room" Enabled="false" >
                                                                                </dx:ASPxCheckBox>
                                                                                <dx:ASPxCheckBox ID="chk_ConfE" runat="server" ClientInstanceName="chk_ConfE" Text="Conferences">
                                                                                    <ClientSideEvents CheckedChanged="function(s, e) {fnBehaviorAllChecked();}" />
                                                                                </dx:ASPxCheckBox>
                                                                            </td> 
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>  
                                                            <tr> 
                                                                <td colspan="3">
                                                                    <table width="100%" cellpadding="0" cellspacing="0" id="tblMCU" style="display:none;">
                                                                        <tr valign="top">
                                                                            <td width="50%" align="left" >   
                                                                                <dx:ASPxLabel ID="ASPxLabel20" runat="server"  Text="  MCU:"
                                                                                    ForeColor="Black">
                                                                                </dx:ASPxLabel>  
                                                                            </td>
                                                                             <td valign="top" align="right"><%--FB 2886--%>
                                                                                <asp:Button ID="btnMClear" runat="server" CssClass="altShortBlueButtonFormat"  UseSubmitBehavior="false"
                                                                                    OnClientClick="javascript:return fnClear();" Text="Clear" />
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td width="50%" align="left" colspan="2">                                                                       
                                                                                <dx:ASPxCheckBox ID="chk_OrgM" runat="server" ClientInstanceName="chk_OrgM" Text="Organization" Font-Size="5px">
                                                                                    <ClientSideEvents CheckedChanged="function(s,e){OnShowButtonClick('popupM');}" />
                                                                                </dx:ASPxCheckBox>
                                                                                <dx:ASPxCheckBox ID="chk_DeptM" runat="server" Visible="false" ClientInstanceName="chk_DeptM" Text="Department">
                                                                                    <%--<ClientSideEvents CheckedChanged="function(s,e){OnShowButtonClick('dptPopupChk');}" />--%>
                                                                                </dx:ASPxCheckBox>
                                                                                <dx:ASPxCheckBox ID="chk_MCUNameM" runat="server" ClientInstanceName="chk_MCUNameM" Text="MCU Name">
                                                                                </dx:ASPxCheckBox>
                                                                                <dx:ASPxCheckBox ID="chk_SpecMCUM" runat="server" ClientInstanceName="chk_SpecMCUM" Text="Specific MCU">
                                                                                    <ClientSideEvents CheckedChanged="function(s,e){OnShowButtonClick('assignmentPopup');BindMCU();}" /> <%--FB 2593--%>
                                                                                </dx:ASPxCheckBox>
                                                                                <dx:ASPxCheckBox ID="chk_ENameM" runat="server" ClientInstanceName="chk_ENameM" Text="Endpoint Name">
                                                                                    <ClientSideEvents CheckedChanged="function(s,e){OnShowButtonClick('assignmentPopup');}" />
                                                                                </dx:ASPxCheckBox>
                                                                                <dx:ASPxCheckBox ID="chk_SpecEptM" runat="server" ClientInstanceName="chk_SpecEptM" Text="Specific Endpoint">
                                                                                    <ClientSideEvents CheckedChanged="function(s,e){OnShowButtonClick('assignmentPopup');BindEndpointList();}" /> 
                                                                                </dx:ASPxCheckBox>
                                                                                 <dx:ASPxCheckBox ID="chk_RmNameM" runat="server" ClientInstanceName="chk_RmNameM" Text="Room Name">
                                                                                    <ClientSideEvents CheckedChanged="function(s,e){OnShowButtonClick('assignmentPopup');}" />
                                                                                </dx:ASPxCheckBox>
                                                                                <dx:ASPxCheckBox ID="chk_SpecRoomM" runat="server" ClientInstanceName="chk_SpecRoomM" Text="Specific Room">
                                                                                    <ClientSideEvents CheckedChanged="function(s,e){OnShowButtonClick('assignmentPopup');BindRoomList();}" />
                                                                                </dx:ASPxCheckBox>
                                                                                <dx:ASPxCheckBox ID="chk_ConfM" runat="server" ClientInstanceName="chk_ConfM" Text="Conferences">
                                                                                    <ClientSideEvents CheckedChanged="function(s, e) {fnBehaviorAllChecked();}" />
                                                                                </dx:ASPxCheckBox>
                                                                            </td>
                                                                           
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>      
                                                            <tr> <%--FB 2593--%>
                                                                <td colspan="3">
                                                                    <table width="100%" cellpadding="0" cellspacing="0" id="tblCDR" style="display:none;">
                                                                        <tr valign="top">
                                                                            <td width="50%" align="left" >   
                                                                                <dx:ASPxLabel ID="lblCDR" runat="server"  Text="  CDR:"
                                                                                    ForeColor="#003366">
                                                                                </dx:ASPxLabel>  
                                                                            </td>
                                                                             <td valign="top" align="right"><%--FB 2886--%>
                                                                                <asp:Button ID="btnCDRClear" runat="server" CssClass="altShortBlueButtonFormat"  UseSubmitBehavior="false"
                                                                                    OnClientClick="javascript:return fnClear();" Text="Clear" />
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td width="50%" align="left" colspan="2">                                                                       
                                                                                <dx:ASPxRadioButton ID="rbConferenceCDR" runat="server" Text="Conference CDR" GroupName="CDRType" Checked="True" TextWrap="False" ClientInstanceName="rbConferenceCDR">                                            
                                                                                    <ClientSideEvents CheckedChanged="function(s, e){SavedRptListPopup.Hide();}" />
                                                                                </dx:ASPxRadioButton>  
                                                                                <dx:ASPxRadioButton ID="rbVMRCDR" runat="server" Text="VMR CDR" GroupName="CDRType" TextWrap="False" ClientInstanceName="rbVMRCDR">                                            
                                                                                    <ClientSideEvents CheckedChanged="function(s, e){SavedRptListPopup.Hide();document.getElementById('hdnRptSave').value='1';}" /> <%--FB 2593--%>
                                                                                </dx:ASPxRadioButton>  
                                                                            </td>
                                                                            <td valign="right" align="center" style="display:none";>
                                                                                <dx:ASPxButton ID="btnCDRPoll" ClientInstanceName="btnCDRPoll" runat="server" CssFilePath="../en/App_Themes/Glass/{0}/styles.css"
                                                                                    CssPostfix="Glass" SpriteCssFilePath="../en/App_Themes/Glass/{0}/sprite.css"
                                                                                    Text="Poll" Width="52px" OnClick="PollReport" >
                                                                                  <%--  <ClientSideEvents Click="function(s, e) {e.processOnServer = fnSave();}" />--%>
                                                                                </dx:ASPxButton> 
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td height="25px"></td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>   
                                                            <tr>
                                                                <td align="left" colspan="3">
                                                                    <dx:ASPxTextBox ID="txtSaveReport" ClientInstanceName="txtSaveReport" runat="server" Width="180px" Text="Report Name...">
                                                                        <ClientSideEvents GotFocus="OntextFocus" LostFocus="OntextunFocus" />
                                                                    </dx:ASPxTextBox>                                                                   
                                                                </td>
                                                            </tr>                                                       
                                                            <tr valign="middle">
                                                                <td align="center"><%--FB 2886--%>
                                                                <asp:Button ID="btnClose" runat="server" CssClass="altBlueButtonFormat" UseSubmitBehavior="false"
                                                                    OnClientClick="javascript:return fnSubmit('1');" Text="Close" />                                                                     
                                                                </td>
                                                                <td valign="middle" align="center">
                                                                    <asp:Button ID="btnSave" runat="server" CssClass="altBlueButtonFormat"  OnClick="SaveReport" 
                                                                    OnClientClick="javascript:return fnSave();" Text="Save" />                                                                     
                                                                </td>
                                                                <td align="center">
                                                                    <asp:Button ID="cmdConfOk" runat="server" CssClass="altBlueButtonFormat" UseSubmitBehavior="false"
                                                                    OnClientClick="javascript:return fnSubmit('1');" Text="Ok" /> 
                                                                </td>
                                                            </tr>
                                                        </table>
                                                        
                                                    </dx:PopupControlContentControl>
                                                </ContentCollection>
                                                 <CloseButtonImage Height="14px" Width="14px" />
                                            </dx:ASPxPopupControl>
                                            <%--FB 2886--%>
                                            <dx:ASPxPopupControl ID="pcPopup" runat="server" ClientInstanceName="popup" EncodeHtml="false"
                                                HeaderStyle-CssClass = "altMedium0BlueButtonFormat" HeaderStyle-ForeColor="White" Width="150px" 
                                                HeaderText="Organization" CloseAction="CloseButton" >
                                                <ContentCollection>
                                                    <dx:PopupControlContentControl>
                                                        <dx:ASPxListBox ID="OrgList" runat="server"  Width="100%" ClientInstanceName="OrgList">
                                                            <ClientSideEvents SelectedIndexChanged="function(s,e){OnShowButtonClick('dptPopup');}" />
                                                            <ItemStyle><Paddings Padding="2px" /> </ItemStyle>
                                                         </dx:ASPxListBox>      
                                                         <dx:ASPxListBox ID="TempList" runat="server" Height="0px" Width="100%" Border-BorderStyle=None ClientInstanceName="TempList" > </dx:ASPxListBox>                                                 
                                                    </dx:PopupControlContentControl>
                                                </ContentCollection>
                                                 <ContentStyle> 
                                                    <Paddings PaddingBottom="8px" />
                                                </ContentStyle>
                                            </dx:ASPxPopupControl><%--FB 2886--%>
                                            <dx:ASPxPopupControl ID="dptPopup" runat="server" ClientInstanceName="dptPopup"
                                                EncodeHtml="false" HeaderText="Department" Width="150px" CloseAction="CloseButton" HeaderStyle-CssClass = "altMedium0BlueButtonFormat" HeaderStyle-ForeColor="White" >
                                                <ContentCollection>
                                                    <dx:PopupControlContentControl>
                                                         <dx:ASPxListBox ID="lstDepartment" SelectionMode="Multiple"  runat="server"  Width="100%"  ClientInstanceName="lstDepartment">
                                                            <ClientSideEvents SelectedIndexChanged="function(s,e){OnShowButtonClick('assignmentPopup');}" />
                                                            <ItemStyle><Paddings Padding="2px" /> </ItemStyle>
                                                         </dx:ASPxListBox>                                                          
                                                    </dx:PopupControlContentControl>
                                                </ContentCollection>
                                                 <ContentStyle> 
                                                    <Paddings PaddingBottom="8px" />
                                                </ContentStyle>
                                            </dx:ASPxPopupControl><%--FB 2886--%>
                                            <dx:ASPxPopupControl ID="assignmentPopup" runat="server" ClientInstanceName="assignmentPopup"
                                                EncodeHtml="false" HeaderText="Assignments" Width="150px" CloseAction="CloseButton" HeaderStyle-CssClass = "altMedium0BlueButtonFormat" HeaderStyle-ForeColor="White">
                                                <ContentCollection>
                                                    <dx:PopupControlContentControl>
                                                        <dx:ASPxMenu ID="ASPxMenu1" runat="server" EncodeHtml="False" Orientation="Vertical"
                                                            Width="100%">
                                                            <ItemStyle HorizontalAlign="Center" Font-Size="11px" >
                                                            <Paddings Padding="0px" /> </ItemStyle>
                                                            <Items>
                                                                <dx:MenuItem Text="Rooms">
                                                                </dx:MenuItem>
                                                            </Items>
                                                        </dx:ASPxMenu>
                                                        <dx:ASPxListBox ID="lstRmApprover" runat="server" SelectionMode="Multiple" Height="100px" Width="98%" ClientInstanceName="lstRmApprover" Rows="5" >
                                                            <ItemStyle><Paddings Padding="2px" /> </ItemStyle>
                                                            <Items>
                                                              <dx:ListEditItem Text="Asst.-in-Charge" Value="1" />
                                                              <dx:ListEditItem Text="Primary Approver" Value="2" />
                                                              <dx:ListEditItem Text="Second.Approver I" Value="3" />
                                                              <dx:ListEditItem Text="Second.Approver II" Value="4" />
                                                              <dx:ListEditItem Text="All" Value="5" />
                                                            </Items>
                                                         </dx:ASPxListBox>                                                         
                                                        <dx:ASPxMenu ID="ASPxMenu2" runat="server" EncodeHtml="False" Orientation="Vertical"
                                                            Width="100%">
                                                            <ItemStyle HorizontalAlign="Center" Font-Size="11px" >
                                                            <Paddings Padding="0px" /> </ItemStyle>
                                                            <Items>
                                                                <dx:MenuItem Text="MCU">
                                                                </dx:MenuItem>
                                                            </Items>
                                                        </dx:ASPxMenu>
                                                        <dx:ASPxListBox ID="lstMCUApprover" runat="server" SelectionMode="Multiple" Height="100px" Width="98%" ClientInstanceName="lstMCUApprover" Rows="5" >
                                                            <ItemStyle><Paddings Padding="2px" /> </ItemStyle>
                                                            <Items>
                                                              <dx:ListEditItem Text="Administrator" Value="1" />
                                                              <dx:ListEditItem Text="Primary Approver" Value="2" />
                                                              <dx:ListEditItem Text="Second.Approver I" Value="3" />
                                                              <dx:ListEditItem Text="Second.Approver II" Value="4" />
                                                              <dx:ListEditItem Text="All" Value="5" />
                                                            </Items>
                                                         </dx:ASPxListBox>                                                         
                                                        <dx:ASPxMenu ID="ASPxMenu3" runat="server" EncodeHtml="False" Orientation="Vertical"
                                                            Width="100%">
                                                            <ItemStyle HorizontalAlign="Center" Font-Size="11px" >
                                                            <Paddings Padding="0px" /> </ItemStyle>
                                                            <Items>
                                                                <dx:MenuItem Text="Work Orders">
                                                                </dx:MenuItem>
                                                            </Items>
                                                        </dx:ASPxMenu>
                                                        <dx:ASPxListBox ID="lstWOAdmin" runat="server" SelectionMode="Multiple" Height="100px" Width="98%" ClientInstanceName="lstWOAdmin" Rows="5" >
                                                            <ItemStyle><Paddings Padding="2px" /> </ItemStyle>
                                                            <Items>
                                                              <dx:ListEditItem Text="Person-in-Charge" Value="1" />
                                                              <dx:ListEditItem Text="Staff" Value="2" />
                                                              <dx:ListEditItem Text="All" Value="3" />                                                             
                                                            </Items>
                                                         </dx:ASPxListBox>                                                         
                                                    </dx:PopupControlContentControl>
                                                </ContentCollection>
                                                 <ContentStyle> 
                                                    <Paddings PaddingBottom="5px" />
                                                </ContentStyle>
                                            </dx:ASPxPopupControl><%--FB 2886--%>
                                            <dx:ASPxPopupControl ID="resourcePopup"  AllowDragging ="true" Height="250px" runat="server" ClientInstanceName="resourcePopup"
                                                EncodeHtml="false" HeaderText="Resources" Width="150px" CloseAction="CloseButton" 
                                                HeaderStyle-CssClass = "altMedium0BlueButtonFormat" HeaderStyle-ForeColor="White" >
                                                <ContentCollection>
                                                    <dx:PopupControlContentControl BackColor="Red">
                                                        <dx:ASPxMenu ID="ASPxMenu10" runat="server" EncodeHtml="False" Orientation="Vertical"
                                                            Width="100%">
                                                            <ItemStyle HorizontalAlign="Center" Font-Size="11px" >
                                                            <Paddings Padding="0px" /> </ItemStyle>
                                                            <Items>
                                                                <dx:MenuItem Text="Attendees" >
                                                                </dx:MenuItem>
                                                            </Items>
                                                        </dx:ASPxMenu>
                                                        <dx:ASPxListBox ID="lstAttendee" runat="server" SelectionMode="Multiple" Height="60px" Width="100%" ClientInstanceName="lstAttendee" Rows="3" >
                                                            <ItemStyle><Paddings Padding="2px" /> </ItemStyle>
                                                            <Items>
                                                              <dx:ListEditItem Text="Staff" Value="1" />
                                                              <dx:ListEditItem Text="Guests" Value="2" />
                                                              <dx:ListEditItem Text="All" Value="3" />
                                                            </Items>
                                                         </dx:ASPxListBox>                                                        
                                                        <dx:ASPxMenu ID="ASPxMenu4" runat="server" EncodeHtml="False" Orientation="Vertical"
                                                            Width="100%">
                                                            <ItemStyle HorizontalAlign="Center" Font-Size="11px" >
                                                            <Paddings Padding="0px" /> </ItemStyle>
                                                            <Items>
                                                                <dx:MenuItem Text="Participants">
                                                                </dx:MenuItem>
                                                            </Items>
                                                        </dx:ASPxMenu>
                                                        <dx:ASPxListBox ID="lstRmType" runat="server" SelectionMode="Multiple" Height="60px" Width="100%" ClientInstanceName="lstRmType" Rows="3" >
                                                            <ItemStyle><Paddings Padding="2px" /> </ItemStyle>
                                                            <Items>
                                                              <dx:ListEditItem Text="Internal" Value="1" />
                                                              <dx:ListEditItem Text="External" Value="2" />
                                                              <dx:ListEditItem Text="All" Value="3" />
                                                            </Items>
                                                         </dx:ASPxListBox>                                                           
                                                        <dx:ASPxMenu ID="ASPxMenu5" runat="server" EncodeHtml="False" Orientation="Vertical"
                                                            Width="100%">
                                                            <ItemStyle HorizontalAlign="Center" Font-Size="11px" >
                                                            <Paddings Padding="0px" /> </ItemStyle>
                                                            <Items>
                                                                <dx:MenuItem Text="End Points">
                                                                </dx:MenuItem>
                                                            </Items>
                                                        </dx:ASPxMenu>
                                                        <dx:ASPxListBox ID="lstEPYes" runat="server" Height="40px" Width="100%" ClientInstanceName="lstEPYes" Rows="2" >
                                                            <ItemStyle><Paddings Padding="2px" /> </ItemStyle>
                                                            <Items>
                                                              <dx:ListEditItem Text="Yes" Value="1" />
                                                              <dx:ListEditItem Text="No" Value="0" />
                                                            </Items>
                                                         </dx:ASPxListBox>                                                           
                                                        <dx:ASPxMenu ID="ASPxMenu6" runat="server" EncodeHtml="False" Orientation="Vertical"
                                                            Width="100%">
                                                            <ItemStyle HorizontalAlign="Center" Font-Size="11px" >
                                                            <Paddings Padding="0px" /> </ItemStyle>
                                                            <Items>
                                                                <dx:MenuItem Text="MCU">
                                                                </dx:MenuItem>
                                                            </Items>
                                                        </dx:ASPxMenu>
                                                        <dx:ASPxListBox ID="lstMCUYes" runat="server" Height="40px" Width="100%" ClientInstanceName="lstMCUYes" Rows="2" >
                                                            <ItemStyle><Paddings Padding="2px" /> </ItemStyle>
                                                            <Items>
                                                              <dx:ListEditItem Text="Yes" Value="1" />
                                                              <dx:ListEditItem Text="No" Value="0" />
                                                            </Items>
                                                         </dx:ASPxListBox>
                                                        <dx:ASPxMenu ID="ASPxMenu7" runat="server" EncodeHtml="False" Orientation="Vertical"
                                                            Width="100%">
                                                            <ItemStyle HorizontalAlign="Center" Font-Size="11px" >
                                                            <Paddings Padding="0px" /> </ItemStyle>
                                                            <Items>
                                                                <dx:MenuItem Text="Conference Speed">
                                                                </dx:MenuItem>
                                                            </Items>
                                                        </dx:ASPxMenu>
                                                       <dx:ASPxListBox ID="lstConfSpeedYes" runat="server" Height="40px" Width="100%" ClientInstanceName="lstConfSpeedYes" Rows="2" >
                                                            <ItemStyle><Paddings Padding="2px" /> </ItemStyle>
                                                            <Items>
                                                              <dx:ListEditItem Text="Yes" Value="1" />
                                                              <dx:ListEditItem Text="No" Value="0" />
                                                            </Items>
                                                         </dx:ASPxListBox>
                                                        <dx:ASPxMenu ID="ASPxMenu8" runat="server" EncodeHtml="False" Orientation="Vertical"
                                                            Width="100%">
                                                            <ItemStyle HorizontalAlign="Center" Font-Size="11px" >
                                                            <Paddings Padding="0px" /> </ItemStyle>
                                                            <Items>
                                                                <dx:MenuItem Text="Conference Protocol">
                                                                </dx:MenuItem>
                                                            </Items>
                                                        </dx:ASPxMenu>
                                                        <dx:ASPxListBox ID="lstConfProYes" runat="server" Height="40px" Width="100%" ClientInstanceName="lstConfProYes" Rows="2" >
                                                            <ItemStyle><Paddings Padding="2px" /> </ItemStyle>
                                                            <Items>
                                                              <dx:ListEditItem Text="Yes" Value="1" />
                                                              <dx:ListEditItem Text="No" Value="0" />
                                                            </Items>
                                                         </dx:ASPxListBox>
                                                         <dx:ASPxMenu ID="ASPxMenu9" runat="server" EncodeHtml="False" Orientation="Vertical"
                                                            Width="100%">
                                                            <ItemStyle HorizontalAlign="Center" Font-Size="11px" >
                                                            <Paddings Padding="0px" /> </ItemStyle>
                                                            <Items>
                                                                <dx:MenuItem Text="Work Orders">
                                                                </dx:MenuItem>
                                                            </Items>
                                                        </dx:ASPxMenu>
                                                        <dx:ASPxListBox ID="lstWOYes" runat="server" Height="40px" Width="100%" ClientInstanceName="lstWOYes" Rows="2" >
                                                            <ItemStyle><Paddings Padding="2px" /> </ItemStyle>
                                                            <Items>
                                                              <dx:ListEditItem Text="Yes" Value="1" />
                                                              <dx:ListEditItem Text="No" Value="0" />
                                                            </Items>
                                                         </dx:ASPxListBox>
                                                    </dx:PopupControlContentControl>
                                                </ContentCollection>
                                                <ContentStyle> 
                                                    <Paddings PaddingBottom="1px" />
                                                </ContentStyle>
                                            </dx:ASPxPopupControl><%--FB 2886--%>
                                            <dx:ASPxPopupControl ID="confPopup" runat="server" ClientInstanceName="confPopup"
                                                EncodeHtml="false" HeaderText="Conferences" Width="150px" CloseAction="CloseButton"
                                                HeaderStyle-CssClass = "altMedium0BlueButtonFormat" HeaderStyle-ForeColor="White">
                                                <ContentCollection>
                                                    <dx:PopupControlContentControl>
                                                        <dx:ASPxMenu ID="ASPxMenu11" runat="server" EncodeHtml="False" Orientation="Vertical"
                                                            Width="100%">
                                                            <ItemStyle HorizontalAlign="Center" Font-Size="11px" >
                                                            <Paddings Padding="0px" /> </ItemStyle>
                                                            <Items>
                                                                <dx:MenuItem Text="Type">
                                                                </dx:MenuItem>
                                                            </Items>
                                                        </dx:ASPxMenu>
                                                        <dx:ASPxListBox ID="lstConfType" runat="server" SelectionMode="Multiple" Height="100px" Width="100%" ClientInstanceName="lstConfType" Rows="4" >
                                                            <ItemStyle><Paddings Padding="2px" /> </ItemStyle>
                                                            <Items>
                                                              <dx:ListEditItem Text="Audio/Video" Value="2" />
                                                              <dx:ListEditItem Text="Audio Only" Value="6" />
                                                              <dx:ListEditItem Text="Room Only" Value="7" />
                                                              <dx:ListEditItem Text="All" Value="4" />
                                                            </Items>
                                                         </dx:ASPxListBox>                                                         
                                                    </dx:PopupControlContentControl>
                                                </ContentCollection>
                                                 <ContentStyle> 
                                                    <Paddings PaddingBottom="5px" />
                                                </ContentStyle>
                                            </dx:ASPxPopupControl><%--FB 2886--%>
                                            <dx:ASPxPopupControl ID="tierPopup" runat="server" ClientInstanceName="tierPopup"
                                                EncodeHtml="false" HeaderText="Tiers" Width="150px" CloseAction="CloseButton"
                                                HeaderStyle-CssClass = "altMedium0BlueButtonFormat" HeaderStyle-ForeColor="White">
                                                <ContentCollection>
                                                    <dx:PopupControlContentControl>                                                       
                                                        <dx:ASPxListBox ID="lstTiers" runat="server" SelectionMode="Multiple" Height="100px" Width="100%" ClientInstanceName="lstTiers" Rows="4" >
                                                            <ItemStyle><Paddings Padding="2px" /> </ItemStyle>
                                                            <Items>
                                                              <dx:ListEditItem Text="Tier 1" Value="1" />
                                                              <dx:ListEditItem Text="Tier 2" Value="2" />
                                                              <dx:ListEditItem Text="All" Value="3" />
                                                            </Items>
                                                         </dx:ASPxListBox>                                                         
                                                    </dx:PopupControlContentControl>
                                                </ContentCollection>
                                                 <ContentStyle> 
                                                    <Paddings PaddingBottom="5px" />
                                                </ContentStyle>
                                            </dx:ASPxPopupControl><%--FB 2886--%>
                                            <dx:ASPxPopupControl ID="rmInfoPopup" runat="server" ClientInstanceName="rmInfoPopup"
                                                EncodeHtml="false" HeaderText="Room Information" Width="150px" CloseAction="CloseButton"
                                                HeaderStyle-CssClass = "altMedium0BlueButtonFormat" HeaderStyle-ForeColor="White">
                                                <ContentCollection>
                                                    <dx:PopupControlContentControl>                                                       
                                                        <dx:ASPxListBox ID="lstRmInfo" runat="server" SelectionMode="Multiple" Height="100px" Width="100%" ClientInstanceName="lstRmInfo" Rows="14" >
                                                            <ItemStyle><Paddings Padding="2px" /> </ItemStyle>
                                                            <Items>
                                                              <dx:ListEditItem Text="Phone Number" Value="1" />
                                                              <dx:ListEditItem Text="Capacity" Value="2" />
                                                              <dx:ListEditItem Text="Address 1" Value="3" />
                                                              <dx:ListEditItem Text="Address 2" Value="4" />
                                                              <dx:ListEditItem Text="Floor" Value="5" />
                                                              <dx:ListEditItem Text="Room" Value="6" />
                                                              <dx:ListEditItem Text="City" Value="7" />
                                                              <dx:ListEditItem Text="State/Province" Value="8" />
                                                              <dx:ListEditItem Text="Country" Value="9" />
                                                              <dx:ListEditItem Text="Zip/Country Code" Value="10" />
                                                              <dx:ListEditItem Text="Time Zone" Value="11" />
                                                              <dx:ListEditItem Text="Primary Approver" Value="12" />
                                                              <dx:ListEditItem Text="Secondary Approver I" Value="13" />
                                                              <dx:ListEditItem Text="Secondary Approver II" Value="14" />
                                                              <dx:ListEditItem Text="All" Value="15" />
                                                            </Items>
                                                         </dx:ASPxListBox>                                                         
                                                    </dx:PopupControlContentControl>
                                                    
                                                </ContentCollection>
                                                 <ContentStyle> 
                                                    <Paddings PaddingBottom="5px" />
                                                </ContentStyle>
                                            </dx:ASPxPopupControl><%--FB 2886--%>
                                            <dx:ASPxPopupControl ID="rmAssetPopup" runat="server" ClientInstanceName="rmAssetPopup"
                                                EncodeHtml="false" HeaderText="Room Assets" Width="150px" CloseAction="CloseButton"
                                                HeaderStyle-CssClass = "altMedium0BlueButtonFormat" HeaderStyle-ForeColor="White">
                                                <ContentCollection>
                                                    <dx:PopupControlContentControl>                                                       
                                                        <dx:ASPxListBox ID="lstRmAsset" runat="server" SelectionMode="Multiple" Height="100px" Width="100%" ClientInstanceName="lstRmAsset" Rows="3" >
                                                            <ItemStyle><Paddings Padding="2px" /> </ItemStyle>
                                                            <Items>
                                                              <dx:ListEditItem Text="Projector" Value="1" />
                                                              <dx:ListEditItem Text="Catering" Value="2" />
                                                              <dx:ListEditItem Text="All" Value="3" />
                                                            </Items>
                                                         </dx:ASPxListBox>                                                         
                                                    </dx:PopupControlContentControl>
                                                </ContentCollection>
                                                 <ContentStyle> 
                                                    <Paddings PaddingBottom="5px" />
                                                </ContentStyle>
                                            </dx:ASPxPopupControl><%--FB 2886--%>
                                            <dx:ASPxPopupControl ID="confStatusPopup" runat="server" ClientInstanceName="confStatusPopup"
                                                EncodeHtml="false" HeaderText="Conference Status" Width="150px" CloseAction="CloseButton"
                                                HeaderStyle-CssClass = "altMedium0BlueButtonFormat" HeaderStyle-ForeColor="White">
                                                <ContentCollection>
                                                    <dx:PopupControlContentControl>                                                       
                                                        <dx:ASPxListBox ID="lstConfStatus" runat="server" SelectionMode="Multiple" Height="100px" Width="100%" ClientInstanceName="lstConfStatus" Rows="7" >
                                                            <ItemStyle><Paddings Padding="2px" /> </ItemStyle>
                                                            <Items>
                                                             <dx:ListEditItem Text="Scheduled" Value="0" />
                                                              <dx:ListEditItem Text="Pending" Value="1" />
                                                              <dx:ListEditItem Text="Completed" Value="7" />
                                                              <dx:ListEditItem Text="Deleted" Value="9" />
                                                              <dx:ListEditItem Text="Terminated" Value="3" />
                                                              <dx:ListEditItem Text="Immediate" Value="15" />
                                                              <dx:ListEditItem Text="All" Value="16" />                                                              
                                                            </Items>
                                                            <ClientSideEvents SelectedIndexChanged="function(s,e){OnShowButtonClick('confOccurencePopup');}" />
                                                         </dx:ASPxListBox>                                                         
                                                    </dx:PopupControlContentControl>
                                                </ContentCollection>
                                                 <ContentStyle> 
                                                    <Paddings PaddingBottom="5px" />
                                                </ContentStyle>
                                            </dx:ASPxPopupControl><%--FB 2886--%>
                                            <dx:ASPxPopupControl ID="confOccurencePopup" runat="server" ClientInstanceName="confOccurencePopup"
                                                EncodeHtml="false" HeaderText="Occurence" Width="150px" CloseAction="CloseButton"
                                                HeaderStyle-CssClass = "altMedium0BlueButtonFormat" HeaderStyle-ForeColor="White">
                                                <ContentCollection>
                                                    <dx:PopupControlContentControl>                                                       
                                                        <dx:ASPxListBox ID="lstConfOccurence" runat="server" SelectionMode="Multiple" Height="100px" Width="100%" ClientInstanceName="lstConfOccurence" Rows="7" >
                                                            <ItemStyle><Paddings Padding="2px" /> </ItemStyle>
                                                            <Items>
                                                              <dx:ListEditItem Text="Single" Value="0" />
                                                              <dx:ListEditItem Text="Recuring" Value="1" />
                                                              <dx:ListEditItem Text="All" Value="2" />
                                                            </Items>
                                                            <ClientSideEvents SelectedIndexChanged="function(s,e){OnShowButtonClick('confDetailsPopup');}" />
                                                         </dx:ASPxListBox>                                                         
                                                    </dx:PopupControlContentControl>
                                                </ContentCollection>
                                                 <ContentStyle> 
                                                    <Paddings PaddingBottom="5px" />
                                                </ContentStyle>
                                            </dx:ASPxPopupControl><%--FB 2886--%>
                                            <dx:ASPxPopupControl ID="confDetailsPopup" runat="server" ClientInstanceName="confDetailsPopup"
                                                EncodeHtml="false" HeaderText="Conference Details" Width="150px" CloseAction="CloseButton"
                                                HeaderStyle-CssClass = "altMedium0BlueButtonFormat" HeaderStyle-ForeColor="White">
                                                <ContentCollection>
                                                    <dx:PopupControlContentControl>                                                       
                                                        <dx:ASPxListBox ID="lstConfDetails" runat="server" SelectionMode="Multiple" Height="100px" Width="100%" ClientInstanceName="lstConfDetails" Rows="7" >
                                                            <ClientSideEvents SelectedIndexChanged="function(s,e){ShowConfMenu(s);}" />
                                                            <ItemStyle><Paddings Padding="2px" /> </ItemStyle>
                                                            <Items>
                                                              <dx:ListEditItem Text="Conference ID" Value="1" />
                                                              <dx:ListEditItem Text="Conference Title" Value="2" />
                                                              <dx:ListEditItem Text="Endpoints" Value="3" />
                                                              <dx:ListEditItem Text="MCU" Value="5" />
                                                              <dx:ListEditItem Text="Work Orders" Value="6" />
                                                              <dx:ListEditItem Text="All" Value="7" />
                                                            </Items>
                                                         </dx:ASPxListBox>                                                         
                                                    </dx:PopupControlContentControl>
                                                </ContentCollection>
                                                 <ContentStyle> 
                                                    <Paddings PaddingBottom="5px" />
                                                </ContentStyle>
                                            </dx:ASPxPopupControl><%--FB 2886--%>
                                            <dx:ASPxPopupControl ID="eptDetailsPopup" runat="server" ClientInstanceName="eptDetailsPopup"
                                                EncodeHtml="false" Width="150px" HeaderText="Endpoint" CloseAction="CloseButton" 
                                                HeaderStyle-CssClass = "altMedium0BlueButtonFormat" HeaderStyle-ForeColor="White">
                                                <ContentCollection>
                                                    <dx:PopupControlContentControl>                                                       
                                                        <dx:ASPxListBox ID="lstEptDetails" runat="server" SelectionMode="Multiple" Height="100px" Width="100%" ClientInstanceName="lstEptDetails" Rows="3" >
                                                            <ItemStyle><Paddings Padding="2px" /> </ItemStyle>
                                                            <Items>
                                                              <dx:ListEditItem Text="Name" Value="1" />
                                                              <dx:ListEditItem Text="Address Type" Value="2" />
                                                              <dx:ListEditItem Text="Address" Value="3" />
                                                              <dx:ListEditItem Text="Dialing Option" Value="4" />
                                                              <dx:ListEditItem Text="All" Value="5" />
                                                            </Items>
                                                         </dx:ASPxListBox>                                                         
                                                    </dx:PopupControlContentControl>
                                                </ContentCollection>
                                                 <ContentStyle> 
                                                    <Paddings PaddingBottom="5px" />
                                                </ContentStyle>
                                            </dx:ASPxPopupControl><%--FB 2886--%>
                                            <dx:ASPxPopupControl ID="woDetailsPopup" runat="server" ClientInstanceName="woDetailsPopup"
                                                EncodeHtml="false" Width="150px" HeaderText="Work Order"  CloseAction="CloseButton"
                                                HeaderStyle-CssClass = "altMedium0BlueButtonFormat" HeaderStyle-ForeColor="White">
                                                <ContentCollection>
                                                    <dx:PopupControlContentControl>                                                       
                                                        <dx:ASPxListBox ID="lstWODetails" runat="server" SelectionMode="Multiple" Height="100px" Width="100%" ClientInstanceName="lstWODetails" Rows="3" >
                                                            <ItemStyle><Paddings Padding="2px" /> </ItemStyle>
                                                            <Items>
                                                              <dx:ListEditItem Text="Inventory" Value="1" />
                                                              <dx:ListEditItem Text="Catering" Value="2" />
                                                              <dx:ListEditItem Text="Facility" Value="3" /> <%-- FB 2570 --%>
                                                              <dx:ListEditItem Text="All" Value="4" />
                                                            </Items>
                                                         </dx:ASPxListBox>                                                         
                                                    </dx:PopupControlContentControl>
                                                </ContentCollection>
                                                 <ContentStyle> 
                                                    <Paddings PaddingBottom="5px" />
                                                </ContentStyle>
                                            </dx:ASPxPopupControl><%--FB 2886--%>
                                            <dx:ASPxPopupControl ID="mcuDetailsPopup" runat="server" ClientInstanceName="mcuDetailsPopup"
                                                EncodeHtml="false" Width="150px" HeaderText="MCU" CloseAction="CloseButton" 
                                                HeaderStyle-CssClass = "altMedium0BlueButtonFormat" HeaderStyle-ForeColor="White">
                                                <ContentCollection>
                                                    <dx:PopupControlContentControl>                                                       
                                                        <dx:ASPxListBox ID="lstMCUDetails" runat="server" SelectionMode="Multiple" Height="100px" Width="100%" ClientInstanceName="lstMCUDetails" Rows="7" >
                                                            <ItemStyle><Paddings Padding="2px" /> </ItemStyle>
                                                            <Items>
                                                              <dx:ListEditItem Text="Used in Conference" Value="1" />
                                                              <dx:ListEditItem Text="Default" Value="2" />
                                                              <dx:ListEditItem Text="Address" Value="3" />
                                                              <dx:ListEditItem Text="All" Value="4" />
                                                            </Items>
                                                         </dx:ASPxListBox>                                                         
                                                    </dx:PopupControlContentControl>
                                                </ContentCollection>
                                                 <ContentStyle> 
                                                    <Paddings PaddingBottom="5px" />
                                                </ContentStyle>
                                            </dx:ASPxPopupControl><%--FB 2886--%>
                                            <dx:ASPxPopupControl ID="eptPopup" runat="server" ClientInstanceName="eptPopup"
                                                EncodeHtml="false" HeaderText="Endpoint Details" Width="150px" CloseAction="CloseButton"
                                                HeaderStyle-CssClass = "altMedium0BlueButtonFormat" HeaderStyle-ForeColor="White">
                                                <ContentCollection>
                                                    <dx:PopupControlContentControl>                                                       
                                                        <dx:ASPxListBox ID="lstEptDetailsM" runat="server" SelectionMode="Multiple" Height="100px" Width="100%" ClientInstanceName="lstEptDetailsM" Rows="16" >
                                                            <ItemStyle><Paddings Padding="2px" /> </ItemStyle>
                                                            <Items>
                                                              <dx:ListEditItem Text="Default Profile Name" Value="1" />
                                                              <dx:ListEditItem Text="Number of Profiles" Value="2" />
                                                              <dx:ListEditItem Text="Address Type" Value="3" />
                                                              <dx:ListEditItem Text="Address" Value="4" />
                                                              <dx:ListEditItem Text="Model" Value="5" />
                                                              <dx:ListEditItem Text="Preferred Bandwith" Value="6" />
                                                              <dx:ListEditItem Text="MCU Assigment" Value="7" />
                                                              <dx:ListEditItem Text="Preferred Dialing Option" Value="8" />
                                                              <dx:ListEditItem Text="Default Protocol" Value="9" />
                                                              <dx:ListEditItem Text="Web Access URL" Value="10" />
                                                              <dx:ListEditItem Text="Network Location" Value="11" />
                                                              <dx:ListEditItem Text="Telnet Enabled" Value="12" />
                                                              <dx:ListEditItem Text="Email ID" Value="13" />
                                                              <dx:ListEditItem Text="API Port" Value="14" />
                                                              <dx:ListEditItem Text="iCal Invite" Value="15" />
                                                              <dx:ListEditItem Text="All" Value="16" />
                                                            </Items>
                                                         </dx:ASPxListBox>                                                         
                                                    </dx:PopupControlContentControl>
                                                </ContentCollection>
                                                 <ContentStyle> 
                                                    <Paddings PaddingBottom="5px" />
                                                </ContentStyle>
                                            </dx:ASPxPopupControl><%--FB 2593--%><%--FB 2886--%>
                                            <dx:ASPxPopupControl ID="mcuPopup" runat="server" ClientInstanceName="mcuPopup"
                                                EncodeHtml="false" HeaderText="MCU" Width="150px" CloseAction="CloseButton" 
                                                HeaderStyle-CssClass = "altMedium0BlueButtonFormat" HeaderStyle-ForeColor="White">
                                                <ContentCollection>
                                                    <dx:PopupControlContentControl>                                                       
                                                        <table width="100%">
                                                            <tr>
                                                                <td>
                                                                    <dx:ASPxListBox ID="lstMCU" runat="server" SelectionMode="Multiple" Height="100px" Width="100%" ClientInstanceName="lstMCU" Rows="10" >
                                                                        <ClientSideEvents SelectedIndexChanged="function(s,e){BindEptFromMCU();}" />
                                                                        <ItemStyle><Paddings Padding="2px" /> </ItemStyle>
                                                                     </dx:ASPxListBox>         
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    (*) - Public MCU                                                                
                                                                </td>
                                                            </tr>
                                                        </table>                                                                                                                                                                  
                                                    </dx:PopupControlContentControl>
                                                </ContentCollection>
                                                 <ContentStyle> 
                                                    <Paddings PaddingBottom="5px" />
                                                </ContentStyle>
                                            </dx:ASPxPopupControl><%--FB 2886--%>
                                            <dx:ASPxPopupControl ID="endPtPopup" runat="server" ClientInstanceName="endPtPopup"
                                                EncodeHtml="false" HeaderText="Endpoint" Width="150px" CloseAction="CloseButton" 
                                                HeaderStyle-CssClass = "altMedium0BlueButtonFormat" HeaderStyle-ForeColor="White">
                                                <ContentCollection>
                                                    <dx:PopupControlContentControl>                                                       
                                                        <dx:ASPxListBox ID="lstendPt" runat="server" SelectionMode="Multiple" Height="100px" Width="100%" ClientInstanceName="lstendPt" Rows="10" >
                                                            <ItemStyle><Paddings Padding="2px" /> </ItemStyle>
                                                         </dx:ASPxListBox>                                                         
                                                    </dx:PopupControlContentControl>
                                                </ContentCollection>
                                                 <ContentStyle> 
                                                    <Paddings PaddingBottom="5px" />
                                                </ContentStyle>
                                            </dx:ASPxPopupControl><%--FB 2886--%>
                                            <dx:ASPxPopupControl ID="rmPopup" runat="server" ClientInstanceName="rmPopup"
                                                EncodeHtml="false" HeaderText="Rooms" Width="150px" CloseAction="CloseButton" 
                                                HeaderStyle-CssClass = "altMedium0BlueButtonFormat" HeaderStyle-ForeColor="White">
                                                <ContentCollection>
                                                    <dx:PopupControlContentControl>                                                       
                                                        <dx:ASPxListBox ID="lstRm" runat="server" SelectionMode="Multiple" Height="100px" Width="100%" ClientInstanceName="lstRm" Rows="10" >
                                                            <ItemStyle><Paddings Padding="2px" /> </ItemStyle>
                                                         </dx:ASPxListBox>                                                         
                                                    </dx:PopupControlContentControl>
                                                </ContentCollection>
                                                 <ContentStyle> 
                                                    <Paddings PaddingBottom="5px" />
                                                </ContentStyle>
                                            </dx:ASPxPopupControl>
                                        </td>
                                    </tr>
                                    <tr style="display: none; background-color: red;">
                                        <td>
                                            <input align="middle" type="button" runat="server" style="width: 100px; height: 21px;
                                                display: none;" id="ClosePUp" value=" Close " class="altButtonFormat" />
                                        </td>
                                    </tr>
                                </table>
                            </asp:Panel>
                        </td>
                    </tr>                    
                    <tr valign="top" id="trSwt" runat="server" style="display: none">
                        <td align="right" valign="top">
                            <a id="ChgOrg" runat="server" href="#" class="blueblodtext">Switch Organization
                            </a>
                        </td>
                    </tr>
                    <tr valign="top">
                        <td> <%--FB 2886--%>
                            <dx:ASPxPanel ID="ASPxPanel1" runat="server" Width="100px" >
                                <PanelCollection>
                                    <dx:PanelContent ID="PanelContent1" runat="server">
                                        <dx:ASPxImage ID="ASPxImage1" runat="server" ImageUrl="../Image/category.png">
                                        </dx:ASPxImage>
                                        <dx:ASPxLabel ID="ASPxLabel3" runat="server" Text="Categories" >
                                        </dx:ASPxLabel>
                                    </dx:PanelContent>
                                </PanelCollection>
                            </dx:ASPxPanel>
                        </td>
                    </tr>
                    <%--Menu Start--%>
                    <tr>
                        <td><%--FB 2808--%><%--FB 2886--%>
                         <dx:ASPxPopupControl ID="SavedRptListPopup" runat="server" AllowDragging="false" AllowResize="false"
                            CloseAction="CloseButton" HeaderStyle-CssClass = "altLongSmallButtonFormat" HeaderStyle-ForeColor="White"
                            EnableViewState="False"  PopupHorizontalAlign="Center"
                            PopupVerticalAlign="Middle" Width="150px"
                            HeaderText="Report List" ClientInstanceName="SavedRptListPopup" EnableHierarchyRecreation="True">
                                <ContentCollection>
                                    <dx:PopupControlContentControl>
                                         <dx:ASPxListBox ID="lstReportList" runat="server" Height="80px" Width="100%"
                                          ClientInstanceName="lstReportList" SelectionMode="CheckColumn"  >
                                            <ItemStyle><Paddings Padding="2px" /> </ItemStyle>
                                            <ClientSideEvents SelectedIndexChanged="function(s,e){fnSelect();}" />
                                         </dx:ASPxListBox>
                                         <table width="80%" align="center">
                                            <tr>
                                                <td><%--FB 2886--%>
                                                    <asp:Button ID="btnRptDelete" runat="server" CssClass="altBlueButtonFormat"  OnClick="SaveReport"  UseSubmitBehavior="false"
                                                                    OnClientClick="javascript:if(!fnDelete()) return false;" Text="Delete" />                                                                     
                                                </td>
                                                <td>
                                                    <asp:Button ID="btnRptView" runat="server" CssClass="altBlueButtonFormat"  OnClick="btnOk_Click"  UseSubmitBehavior="false"
                                                                    OnClientClick="javascript:return SavedReportSelection();" Text="View" />                                                                     
                                                </td>
                                            </tr>
                                        </table>           
                                    </dx:PopupControlContentControl>
                                </ContentCollection>
                                 <ContentStyle> 
                                    <Paddings PaddingBottom="8px" />
                                </ContentStyle>
                            </dx:ASPxPopupControl>
                        </td>
                    </tr>
                    <tr valign="top">
                        <td><%--FB 2886--%>
                            <dx:ASPxMenu ID="MenuConference" runat="server" AutoSeparators="RootOnly" 
                                CssClass="altMedium0BlueButtonFormat" ItemSpacing="0px" ClientInstanceName="MenuConference"
                                SeparatorHeight="100%" SeparatorWidth="2px" Width="100%" ItemStyle-ForeColor="White" ItemStyle-Font-Bold = "true" >
                                <ClientSideEvents ItemClick="function(s, e){fnShow('1');}" />
                                <RootItemSubMenuOffset FirstItemX="1" LastItemX="1" X="1" />
                                <Items>
                                    <dx:MenuItem Text="Conferences" Name="conf_1">
                                    </dx:MenuItem>
                                </Items>
                                <SeparatorBackgroundImage ImageUrl="../en/App_Themes/Plastic Blue/Web/mSeparatorHor.gif"
                                    VerticalPosition="Top" />
                                <ItemSubMenuOffset FirstItemY="-1" LastItemY="-1" Y="-1" />
                                <SubMenuStyle GutterWidth="0px" />
                            </dx:ASPxMenu>
                        </td>
                    </tr>
                    <tr >
                        <td><%--FB 2886--%>
                            <dx:ASPxMenu ID="MenuUsers" runat="server" AutoSeparators="RootOnly" CssClass="altMedium0BlueButtonFormat" ItemSpacing="0px"
                                SeparatorHeight="100%" SeparatorWidth="2px" Width="100%" ItemStyle-ForeColor="White" ItemStyle-Font-Bold = "true">
                                <ClientSideEvents ItemClick="function(s, e){fnShow('2');}" />
                                <RootItemSubMenuOffset FirstItemX="1" LastItemX="1" X="1" />
                                <Items>
                                    <dx:MenuItem Text="Users" Name="Users_1">
                                    </dx:MenuItem>
                                </Items>
                                <SeparatorBackgroundImage ImageUrl="../en/App_Themes/Plastic Blue/Web/mSeparatorHor.gif"
                                    VerticalPosition="Top" />
                                <ItemSubMenuOffset FirstItemY="-1" LastItemY="-1" Y="-1" />
                                <SubMenuStyle GutterWidth="0px" />
                            </dx:ASPxMenu>
                        </td>
                    </tr>
                    <tr >
                        <td><%--FB 2886--%>
                            <dx:ASPxMenu ID="MenuRooms" runat="server" AutoSeparators="RootOnly" CssClass="altMedium0BlueButtonFormat" ItemSpacing="0px"
                                SeparatorHeight="100%" SeparatorWidth="2px" Width="100%" ItemStyle-ForeColor="White" ItemStyle-Font-Bold = "true">
                                <ClientSideEvents ItemClick="function(s, e){fnShow('3');}" />
                                <RootItemSubMenuOffset FirstItemX="1" LastItemX="1" X="1" />
                                <Items>
                                    <dx:MenuItem Text="Rooms" Name="Rooms_1">
                                    </dx:MenuItem>
                                </Items>
                                <SeparatorBackgroundImage ImageUrl="../en/App_Themes/Plastic Blue/Web/mSeparatorHor.gif"
                                    VerticalPosition="Top" />
                                <ItemSubMenuOffset FirstItemY="-1" LastItemY="-1" Y="-1" />
                                <SubMenuStyle GutterWidth="0px" />
                            </dx:ASPxMenu>
                        </td>
                    </tr>
                    <tr>
                        <td><%--FB 2886--%>
                            <dx:ASPxMenu ID="MenuEndpt" runat="server" AutoSeparators="RootOnly" CssClass="altMedium0BlueButtonFormat" ItemSpacing="0px"
                                SeparatorHeight="100%" SeparatorWidth="2px" Width="100%" ItemStyle-ForeColor="White" ItemStyle-Font-Bold = "true">
                                <ClientSideEvents ItemClick="function(s, e){fnShow('4');}" />
                                <RootItemSubMenuOffset FirstItemX="1" LastItemX="1" X="1" />
                                <Items>
                                    <dx:MenuItem Text="Endpoints" Name="Ept_1">
                                    </dx:MenuItem>
                                </Items>
                                <SeparatorBackgroundImage ImageUrl="../en/App_Themes/Plastic Blue/Web/mSeparatorHor.gif"
                                    VerticalPosition="Top" />
                                <ItemSubMenuOffset FirstItemY="-1" LastItemY="-1" Y="-1" />
                                <SubMenuStyle GutterWidth="0px" />
                            </dx:ASPxMenu>
                        </td>
                    </tr>
                    <tr>
                        <td><%--FB 2886--%>
                            <dx:ASPxMenu ID="MenuMCU" runat="server" AutoSeparators="RootOnly" CssClass="altMedium0BlueButtonFormat" ItemSpacing="0px"
                                SeparatorHeight="100%" SeparatorWidth="2px" Width="100%" ItemStyle-ForeColor="White" ItemStyle-Font-Bold = "true">
                                <ClientSideEvents ItemClick="function(s, e){fnShow('5');}" />
                                <RootItemSubMenuOffset FirstItemX="1" LastItemX="1" X="1" />
                                <Items>
                                    <dx:MenuItem Text="MCU" Name="Mcu_1">
                                    </dx:MenuItem>
                                </Items>
                                <SeparatorBackgroundImage ImageUrl="../en/App_Themes/Plastic Blue/Web/mSeparatorHor.gif"
                                    VerticalPosition="Top" />
                                <ItemSubMenuOffset FirstItemY="-1" LastItemY="-1" Y="-1" />
                                <SubMenuStyle GutterWidth="0px" />
                            </dx:ASPxMenu>
                        </td>
                    </tr>
                    <tr> <%--FB 2593--%>
                        <td><%--FB 2886--%>
                            <dx:ASPxMenu ID="MenuCDR" runat="server" AutoSeparators="RootOnly" CssClass="altMedium0BlueButtonFormat" ItemSpacing="0px"
                                SeparatorHeight="100%" SeparatorWidth="2px" Width="100%" ItemStyle-ForeColor="White" ItemStyle-Font-Bold = "true">
                                <ClientSideEvents ItemClick="function(s, e){fnShow('6');}" />
                                <RootItemSubMenuOffset FirstItemX="1" LastItemX="1" X="1" />
                                <Items>
                                    <dx:MenuItem Text="CDR" Name="CDR_1">
                                    </dx:MenuItem>
                                </Items>
                                <SeparatorBackgroundImage ImageUrl="../en/App_Themes/Plastic Blue/Web/mSeparatorHor.gif"
                                    VerticalPosition="Top" />
                                <ItemSubMenuOffset FirstItemY="-1" LastItemY="-1" Y="-1" />
                                <SubMenuStyle GutterWidth="0px" />
                            </dx:ASPxMenu>
                        </td>
                    </tr>
                    <tr>
                        <td style="height: 350px;">
                        </td>
                    </tr>
                    <%--Menu End--%>
                </table>
            </td>
            <td width="8000px" valign="top"> <%--FB 2050 FB 2886--%> 
                <table width="100%" align="center" border="0" >
                    <tr runat="server" id="rptImgRow">
                        <td align="center">
                            <img src="../Image/ReportsII.png" alt="" runat="server" id="imgRpt" width="50" height="50" />
                        </td>
                    </tr>
                    <tr valign="top" runat="server" id="trDetails">
                        <td>
                            <dx:ASPxPopupControl ID="MoreInfoPopup" runat="server" ClientInstanceName="MoreInfoPopup" Width="150px" >
                                <ContentCollection>
                                    <dx:PopupControlContentControl>   
                                        <dx:ASPxMemo ID="txtMemo" runat="server" Height="91px" Width="170px" ClientInstanceName="memo" Border-BorderStyle="None">
                                            <ClientSideEvents Init="function(s, e) {s.GetInputElement().style.overflowY='hidden';}" />
                                        </dx:ASPxMemo>                                        
                                    </dx:PopupControlContentControl>
                                </ContentCollection>
                                 <ContentStyle> 
                                    <Paddings PaddingBottom="5px" />
                                </ContentStyle>
                            </dx:ASPxPopupControl>
                            <%--Main Grid--%> <%--FB 2886--%>
                            <div id="MainDiv" style="width:1000px;overflow-y:auto;overflow-x:auto; word-break:break-all;HEIGHT: 540px;" >
                            <dx:ASPxGridView ID="MainGrid" ClientInstanceName="MainGrid" runat="server" Width="100%" EnableCallBacks="true" 
                              OnHtmlRowCreated="MainGrid_HtmlRowCreated" AutoGenerateColumns ="false" 
                             OnDetailRowGetButtonVisibility="masterGrid_DetailRowGetButtonVisibility" OnDataBound="MainDetailsGrid_DataBound"> <%--FB 2593--%>
                             <ClientSideEvents RowExpanding ="function(s,e){fnUpdateState(e.visibleIndex,'X');}" RowCollapsing ="function(s,e){fnUpdateState(e.visibleIndex,'R');}" ColumnMoving ="function(s,e){fnUpdateState(e.visibleIndex,'M');}" DetailRowExpanding="function(s,e){fnUpdateState(e.visibleIndex,'E');}" DetailRowCollapsing="function(s,e){fnUpdateState(e.visibleIndex,'C');}"/> <%--FB 2965--%>
                                <Templates>
                                    <DetailRow>
                                        <div style="width:100%;overflow-y:auto;overflow-x:auto; word-break:break-all;" runat="server" id="DetailsDiv" >
                                            <dx:ASPxGridView ID="DetailGrid"  ClientInstanceName="DetailGrid" runat="server" Width="100%" 
                                            OnPageIndexChanged="detailGrid_DataSelect" OnInit="detailGrid_DataSelect" 
                                            OnDetailRowGetButtonVisibility="detailsGrid_DetailRowGetButtonVisibility" OnDataBound="DetailsGrid_DataBound">
                             				<ClientSideEvents  DetailRowExpanding="function(s,e){fnUpdateState(e.visibleIndex,'E');}" DetailRowCollapsing="function(s,e){fnUpdateState(e.visibleIndex,'C');}"/><%--FB 2965--%>
                                                <Settings ShowFooter="True"  />    
                                                <SettingsPager Mode="ShowAllRecords"/>
                                                <SettingsDetail IsDetailGrid="True" ShowDetailRow="True"  ExportMode="All" />  
                                                <Templates>
                                                    <DetailRow>
                                                        <dx:ASPxGridView ID="SubDetailGrid"  ClientInstanceName="SubDetailGrid" runat="server" Width="100%" 
                                                           OnPageIndexChanged="subDetailGrid_DataSelect" OnInit="subDetailGrid_DataSelect" OnDataBound="subDetailGrid_DataBound">
                                                            <SettingsPager Mode="ShowAllRecords"/>  
                                                            <SettingsDetail IsDetailGrid="True" ShowDetailRow="True"  ExportMode="All" />  
                             								<ClientSideEvents DetailRowExpanding="function(s,e){fnUpdateState(e.visibleIndex,'E');}" DetailRowCollapsing="function(s,e){fnUpdateState(e.visibleIndex,'C');}"/> <%--FB 2965--%>
                                                            <Templates>
                                                                <DetailRow>
                                                                    <dx:ASPxGridView ID="ChildDetailGrid"  ClientInstanceName="ChildDetailGrid" runat="server" Width="100%" 
                                                                       OnPageIndexChanged="ChildDetailGrid_DataSelect" OnInit="ChildDetailGrid_DataSelect" OnDataBound="childDetailGrid_DataBound">
                                                                        <SettingsPager Mode="ShowAllRecords"/>  
                                                                    </dx:ASPxGridView>
                                                                </DetailRow>
                                                            </Templates>  
                                                        </dx:ASPxGridView>
                                                    </DetailRow>
                                                </Templates>
                                            </dx:ASPxGridView>
                                        </div>
                                    </DetailRow>
                                </Templates>
                                <SettingsDetail ShowDetailRow="true" ExportMode="All" />
                                <Styles> <%--FB 2886--%>
                                    <Header ImageSpacing="9px" SortingImageSpacing="9px" CssClass="tableHeader" Font-Size="9pt"></Header>
                                    <Cell    Font-Size="9pt"></Cell>                                 
                                </Styles>
                                <Images ImageFolder="../en/App_Themes/Plastic Blue/{0}/">
                                    <CollapsedButton Height="10px" Url="../en/App_Themes/Plastic Blue/GridView/gvCollapsedButton.png" Width="9px" />
                                    <ExpandedButton Height="9px" Url="../en/App_Themes/Plastic Blue/GridView/gvExpandedButton.png" Width="9px" />
                                    <HeaderFilter Height="14px" Url="../en/App_Themes/Plastic Blue/GridView/gvHeaderFilter.png" Width="14px" /> <%--FB 2886--%>
                                    <HeaderActiveFilter Height="11px" Url="../en/App_Themes/Plastic Blue/GridView/gvHeaderFilterActive.png" Width="11px" />
                                    <HeaderSortDown Height="11px" Url="../en/App_Themes/Plastic Blue/GridView/gvHeaderSortDown.png" Width="11px" />
                                    <HeaderSortUp Height="11px" Url="../en/App_Themes/Plastic Blue/GridView/gvHeaderSortUp.png" Width="11px" />
                                    <FilterRowButton Height="13px" Width="13px" />
                                    <CustomizationWindowClose Height="14px" Width="14px" />
                                    <PopupEditFormWindowClose Height="14px" Width="14px" />
                                    <FilterBuilderClose Height="14px" Width="14px" />
                                </Images>  
                                <Settings ShowFilterRow="True" ShowGroupPanel="True" ShowHeaderFilterButton="True" ShowTitlePanel="True" 
                                ShowHeaderFilterBlankItems="False" /> <%-- FB 2880 --%>
                                <SettingsPager ShowDefaultImages="False" Mode="ShowPager" AlwaysShowPager="true" Position="Top">
                                    <AllButton Text="All"></AllButton>
                                    <NextPageButton Text="Next &gt;"></NextPageButton>
                                    <PrevPageButton Text="&lt; Prev"></PrevPageButton>
                                </SettingsPager>
                            </dx:ASPxGridView>
                            </div>
                            <dx:ASPxGridViewExporter ID="gridExport" runat="server" GridViewID="MainGrid"></dx:ASPxGridViewExporter>
                         </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    </form>
</body>
</html>
<%--code added for Soft Edge button--%>

<script type="text/javascript">
    var mainDiv = document.getElementById("MainDiv");
    if(mainDiv)
    {  //Difference 180
    
        if (window.screen.width <= 1024)
            mainDiv.style.width = "845px";
        else
            mainDiv.style.width = "1184px";        
    }
    //FB 2886
    var btnPreview = document.getElementById("btnPreview");
    if (rbSaved.GetChecked())
         btnPreview.style.display ='None'; 

    //FB 2593
    function fnDefaultOrgSelection()
    {
        var args = fnDefaultOrgSelection.arguments;
        
        if(document.getElementById("hdnAdmin"))
        {
            if(document.getElementById("hdnAdmin").value == "U")
            {
                if(args[0] == "1")
                {                
                    chk_Org.SetChecked (true);
                    chk_Org.SetEnabled(false);
                    OnShowButtonClick();
                    OnShowButtonClick('dptPopup'); //FB 2882
                }
                else if(args[0] == "2")
                {
                    chk_OrgU.SetChecked (true);
                    chk_OrgU.SetEnabled(false);
                    OnShowButtonClick('popupU');
                    OnShowButtonClick('dptPopup'); //FB 2882
                }
                else if(args[0] == "3")
                {
                    chk_OrgR.SetChecked (true);
                    chk_OrgR.SetEnabled(false);
                    OnShowButtonClick('popupR');
                    OnShowButtonClick('dptPopup'); //FB 2882
                }
                else if(args[0] == "4")
                {
                    chk_OrgE.SetChecked (true);
                    chk_OrgE.SetEnabled(false);
                    OnShowButtonClick('popupE');
                }
                else if(args[0] == "5")
                {
                    chk_OrgM.SetChecked (true);
                    chk_OrgM.SetEnabled(false);
                    OnShowButtonClick('popupM');
                }
                else if(args[0] == "6")  //FB 2882
                {
                    OnShowButtonClick('CDR');
                    BindMCU();
                }
            }   
        }
    } 
   //FB 2965
   function fnUpdateState(id,status)   
    {
        var hdngrop = document.getElementById("hdnGroup");
        if(status == "E" || status == "C")
            hdngrop.value = "Y";
        else if(status == "M" || status == "R" || status == "X")        
            hdngrop.value = "";
        else
            hdngrop.value = "";
    }
    //ZD 100176 start
    function DataLoading(val) {
        if (val == "1")
            document.getElementById("dataLoadingDIV").innerHTML = "<img border='0' src='image/wait1.gif'>";
        else
            document.getElementById("dataLoadingDIV").innerHTML = "";
    }
    //ZD 100176 End  
</script>

<!-- FB 2885 Starts -->
<% if (Session["isExpressUser"].ToString() == "1"){%>
<!-- #INCLUDE FILE="inc/mainbottomNETExp.aspx" -->
<%}else{%>
<!-- #INCLUDE FILE="inc/mainbottomNET.aspx" -->
<%}%>
<!-- FB 2885 Ends -->
