﻿<%--ZD 100147 start--%>
<%--/* Copyright (C) 2013 myVRM - All Rights Reserved	
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.	
*
* You should have received a copy of the myVRM license with	
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/--%><%--ZD 100147 End--%>
<%@ Page Language="C#" AutoEventWireup="true" MaintainScrollPositionOnPostback="true" Inherits="ns_MonitorMCU" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<!-- #INCLUDE FILE="inc/maintopNET.aspx" -->

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
<meta http-equiv="Cache-control" content="no-cache"/>
    <title>Call Monitor</title>    
    <script type="text/javascript">
       function showNestedGridView(obj) {
           var nestedGridView = document.getElementById(obj);
           var imageID = document.getElementById('img' + obj);
           
           var gridState = document.getElementById("hdnGridState"); 

           if (nestedGridView.style.display == "none") {
               gridState.value += nestedGridView.id + ","; 
               nestedGridView.style.display = "inline";
               imageID.src = "image/loc/nolines_minus.gif";
           } else {
               gridState.value = gridState.value.replace(nestedGridView.id + ",",""); 
               nestedGridView.style.display = "none";
               imageID.src = "image/loc/nolines_plus.gif";
           }
           
       }
       
       
       function showNestedGridView2(obj) {
           var nestedGridView = document.getElementById(obj);
           var imageID = document.getElementById('img' + obj);
           
           var gridState = document.getElementById("hdnGridState"); 
           

           if (nestedGridView.style.display == "none") {
               gridState.value += nestedGridView.id + ","; 
               nestedGridView.style.display = "inline";
               imageID.src = "image/loc/nolines_minus.gif";
           } else {
               gridState.value = gridState.value.replace(nestedGridView.id + ",",""); 
               nestedGridView.style.display = "none";
               imageID.src = "image/loc/nolines_plus.gif";
           }
       }
       
       function goToCal()
       {
       if(document.getElementById("lstCalendar") != null)
       {
		           if (document.getElementById("lstCalendar").value == "4"){
                           window.location.href = "MonitorMCU.aspx";
                   } 
		           if (document.getElementById("lstCalendar").value == "5"){
                   window.location.href = "point2point.aspx";
                   }
               }
           }
           function DataLoading(val) {
               //alert(val);
               if (val == "1")
                   document.getElementById("dataLoadingDIV").innerHTML = "<img border='0' src='image/wait1.gif'>";
               else
                   document.getElementById("dataLoadingDIV").innerHTML = "";
           }    

    </script>    
    <style type="text/css">
       .hidden
    {
        display: none;
    }    
    </style>
    
    <link href="css/MonitorMCU.css" type="text/css" rel="stylesheet" />    
    
</head>
<script type="text/javascript" src="script/CallMonitorJquery/jquery.1.4.2.js" ></script>
<script type="text/javascript" src="script/CallMonitorJquery/jquery.bpopup-0.7.0.min.js"></script>
<script type="text/javascript" src="script/CallMonitorJquery/MonitorMCU.js"></script>
<script type="text/javascript" src="script/CallMonitorJquery/json2.js" ></script>
<body>
    <div id="successbox" style="text-align:center; font-family:Verdana;" class="lblMessage"></div><br />
    <div id="errormsgbox" style="text-align: center; font-family:Verdana; font-weight:bold; color: Red;"></div><br />
    <form id="form1" runat="server">
    <input type="hidden" id="hdnGridState" runat="server" />
    <input type="hidden" id="communStatus" value="0" />
    <input type="hidden" id="TotalMcuCount" value="0" runat="server"  />
    <input type="hidden" id="msgData" value="" />
    <input type="hidden" id="MuteAllIdArray" value="" /> <%--// FB 2652--%>
    <input type="hidden" id="MuteAllterminalTypeArray" value="" /> <%--// FB 2652--%>
    <input type="hidden" id="CurrentConfSupportingRowID" value="" />  <%--// FB 2652--%>
    <div>
        <%--FB 2646 Starts--%> 
        <%--FB 2984 Starts--%>
        <table align="center">
        <tr>
            <td>
                <table align="right" border="0" width="140"> <%--FB 2646 Starts--%>
                <tr>        
                    <td align="right">
                        <h3>Call Monitor</h3>
                    </td>
                </tr>
                </table>
            </td>
            <td>
                <table width="230">
                <tr>
                    <td align="left" style="width:200">
                        <select id="lstCalendar" name="lstCalendar" class="altText"  size="1" onchange="goToCal();javascript:DataLoading('1');" runat="server"> <%--FB 2058--%>
                            <option value="4">Call Monitor</option>
          		            <option value="5">Call Monitor (P2P)</option>               
          		        </select> 
                    </td>
                </tr>
                </table>
             </td>
             <td>
                <table>
                    <tr>
                        <td  style="width:100" nowrap="nowrap"> 
                            <asp:Label ID="lblchksilo" Text="Show All Silos" runat="server" CssClass="blackblodtext"/>
                        </td>
                        <td style="width:50">
                            <asp:CheckBox ID="chkAllSilo" runat="server" AutoPostBack="true"/> 
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        </table> 
        <div id="dataLoadingDIV" name="dataLoadingDIV" align="center"></div> <%--FB 2058--%>
            <%--FB 2984 Ends--%>
            <%--FB 2646 Ends--%>
            
        <br />
        <div style="padding-left:400px"><asp:Label ID="lblError" runat="server" CssClass="lblError"></asp:Label></div><br /> <%--FB 2653--%>
        <asp:GridView ID="grdGrand" runat="server" AutoGenerateColumns="false" OnRowDataBound="bindConference"
            HorizontalAlign="Center" HeaderStyle-BackColor="#000033" HeaderStyle-ForeColor="White"
            GridLines="None" CellPadding="1" ShowHeader="false" BackColor="#F2F4F8">
            <EmptyDataTemplate><font style="color:Black;" ><span class="lblError">No Records Found..</span></font></EmptyDataTemplate> 
            <Columns>
                <asp:TemplateField ItemStyle-BackColor="#3075AE" ItemStyle-ForeColor="#FFFFFF" ItemStyle-Width="8%">
                    <ItemTemplate>
                        <a href="javascript:showNestedGridView('divid-<%# Eval("nameid") %>');"><%--FB 2956--%>
                            <img id="imgdivid-<%# Eval("nameid") %>" alt="Click to show/hide details" border="0" src="image/loc/nolines_plus.gif" /><%--FB 2956--%>
                            <asp:Image ID="Image1" runat="server" ImageUrl="~/image/MonitorMCU/mcu.gif" Width="25px" />
                            <asp:Label ID="lblConfMultiInfo" Visible="false" Text='<%# Eval("confMultiInfo") %>' runat="server"></asp:Label>
                            <asp:Label ID="lblPartyMultiInfo" Visible="false" Text='<%# Eval("partyMultiInfo") %>' runat="server"></asp:Label>
                            <asp:Label ID="lblMcuIP" Visible="false" Text='<%# Eval("ip") %>' runat="server"></asp:Label>                            
                            <input type="hidden" id="confTotalCount<%# Container.DataItemIndex +1 %>" value="<%# Eval("confCount") %>" />
                            
                        </a>                                                
                    </ItemTemplate>
                </asp:TemplateField>
                <%--FB 2501 Dec10 Start--%>
                <asp:TemplateField ItemStyle-BackColor="#3075AE" ItemStyle-ForeColor="#FFFFFF" ItemStyle-Width="3%">
                    <ItemTemplate>
                        <span>Name: </span>
                    </ItemTemplate>
                </asp:TemplateField>                 
                <asp:BoundField DataField="name" HeaderText="Name" ItemStyle-BackColor="#3075AE" ItemStyle-ForeColor="#FFFFFF" ItemStyle-Width="15%" ></asp:BoundField> <%--FB 2501 Dec7--%>
                <asp:TemplateField ItemStyle-BackColor="#3075AE" ItemStyle-ForeColor="#FFFFFF" ItemStyle-Width="3%"> <%--FB 2646 Starts--%>
                    <ItemTemplate>
                        <span>Silo: </span>
                    </ItemTemplate>
                </asp:TemplateField>                 
                <asp:BoundField DataField="siloName" HeaderText="Silo" ItemStyle-BackColor="#3075AE" ItemStyle-ForeColor="#FFFFFF" ItemStyle-Width="15%" ></asp:BoundField> <%--FB 2646 Ends--%>
                 <asp:TemplateField ItemStyle-BackColor="#3075AE" ItemStyle-ForeColor="#FFFFFF" ItemStyle-Width="5%">
                    <ItemTemplate>
                        <span>Address: </span>
                    </ItemTemplate>
                </asp:TemplateField> 
                <asp:BoundField DataField="ip" HeaderText="IP"  ItemStyle-BackColor="#3075AE" ItemStyle-ForeColor="#FFFFFF" ItemStyle-Width="12%"></asp:BoundField>                
                 <asp:TemplateField ItemStyle-BackColor="#3075AE" ItemStyle-ForeColor="#FFFFFF" ItemStyle-Width="3%">
                    <ItemTemplate>
                        <span>Type: </span>
                    </ItemTemplate>
                </asp:TemplateField> 
                <asp:BoundField DataField="type" HeaderText="Type" ItemStyle-BackColor="#3075AE" ItemStyle-ForeColor="#FFFFFF" ItemStyle-Width="17%"></asp:BoundField>                
                <asp:TemplateField ItemStyle-BackColor="#3075AE" ItemStyle-ForeColor="#FFFFFF" ItemStyle-Width="3%"> <%--FB 2501 Dec7--%>
                    <ItemTemplate>
                        <span>Ports: </span>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField ItemStyle-BackColor="#3075AE" ItemStyle-ForeColor="#FFFFFF" ItemStyle-Width="5%"> <%--FB 2501 Dec7--%>
                    <ItemTemplate>
                        <asp:Label ID="MCUTotalPort"  Text='<%# Eval("MCUTotalPort") %>' ToolTip="Available/Total" runat="server"></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <%--FB 2501 Dec6 End--%>
                <%--FB 2501 Dec10 Start--%>
                <asp:BoundField DataField="MCUStatus" HeaderText="MCUStatus" ItemStyle-BackColor="#3075AE" ItemStyle-ForeColor="#FFFFFF" ItemStyle-Width="17%"></asp:BoundField>                
                
                
                <asp:TemplateField ItemStyle-BackColor="#3075AE" ItemStyle-ForeColor="#FFFFFF" ItemStyle-Width="3%">
                    <ItemTemplate>
                        <img src="../image/MonitorMCU/favourite_<%# Eval("favorite") %>.gif" title="Favorite" class="setfavorite" id="<%# Eval("mcuID") %>" alt="" width="25px" />                        
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField ItemStyle-BackColor="#3075AE" ItemStyle-ForeColor="#FFFFFF">
                    <ItemTemplate>
                        <tr>
                            <td colspan="100%">
                                <div id="divid-<%# Eval("nameid") %>" style="display: none; position: relative;">
                                    <asp:GridView ID="grdParent" CellPadding="1" runat="server" AutoGenerateColumns="false" OnRowDataBound="bindParticipant" Width="97%" HeaderStyle-BackColor="#330000" HeaderStyle-ForeColor="white" HorizontalAlign="Right"
                                        GridLines="None" ShowHeader="false">                                        
                                            <EmptyDataTemplate><font style="color:Black;" ><span class="lblError"> No Conferences Found..</span></font></EmptyDataTemplate> 
                                        <Columns>
                                            <asp:TemplateField ItemStyle-BackColor="#D4D7DA" ItemStyle-ForeColor="#000000" ItemStyle-Width="6%">
                                                <ItemTemplate>
                                                    <a href="javascript:showNestedGridView2('divid-<%# Eval("xconfName") %>');">
                                                        <img id="imgdivid-<%# Eval("xconfName") %>" alt="Click to show/hide details" border="0" src="image/loc/nolines_plus.gif" />
                                                    </a>
                                                    <asp:Image ID="Image2" runat="server" ImageUrl="~/image/MonitorMCU/confIcon.gif" Width="25px" />
                                                    <input type="hidden" id="hdnlevel<%# Eval("xparentId") %>" />
                                                    <span id="hdnconfid<%# Eval("xparentId") %>"  value="<%# Eval("confId") %>" />
                                                    <input type="hidden" id="McuBridgeID<%# Eval("xparentId") %>" value="<%# Eval("confMcuBridgeID") %>" />                                                                                                       
                                                    <asp:Label ID="mcubridgeID" runat="server" Visible="false" Text='<%# Bind("confMcuBridgeID") %>'></asp:Label>  
                                                    <asp:Label ID="lblxparentid" runat="server" Visible="false" Text='<%# Bind("xparentId") %>'></asp:Label>  
                                                    <asp:Label ID="lblxmergestr" runat="server" Visible="false" Text='<%# Bind("xmergeString") %>'></asp:Label>                                                                                                                                                                                                             
                                                    <asp:Label ID="lblPartyMultiInfo2" Visible="false" Text='<%# Eval("partyMultiInfo2") %>' runat="server"></asp:Label>
                                                    <asp:Label ID="CamVisible" Visible="false" Text='<%# Eval("confcameraStatus") %>' runat="server"></asp:Label> 
                                                </ItemTemplate>
                                            </asp:TemplateField>                                            
                                            <asp:BoundField DataField="xparentId" Visible="false"  ItemStyle-BackColor="#D4D7DA" ItemStyle-ForeColor="#000000">
                                            </asp:BoundField>                                            
                                            <asp:BoundField DataField="xmergeString" Visible="false" ItemStyle-BackColor="#D4D7DA" ItemStyle-ForeColor="#000000">
                                            </asp:BoundField>
                                            <asp:BoundField DataField="confName" HeaderText="ConfName"  ItemStyle-BackColor="#D4D7DA" ItemStyle-ForeColor="#000000" ItemStyle-Width="15%"></asp:BoundField>
                                            <%--<asp:BoundField DataField="confId" HeaderText="confId" ItemStyle-BackColor="#D4D7DA" ItemStyle-ForeColor="#000000"></asp:BoundField> --%>
                                            <asp:BoundField DataField="confDate" HeaderText="confDate" ItemStyle-BackColor="#D4D7DA" ItemStyle-ForeColor="#000000" ItemStyle-Width="15%"></asp:BoundField>                                            
                                            <%--<asp:BoundField DataField="duration" HeaderText="duration" ItemStyle-BackColor="#D4D7DA" ItemStyle-ForeColor="#000000" ItemStyle-Width="7%"></asp:BoundField>--%> 
                                            <asp:TemplateField ItemStyle-BackColor="#D4D7DA" ItemStyle-ForeColor="#000000" ItemStyle-Width="10%">
                                                <ItemTemplate> <%--FB 2582 --%>
                                            <label style="color:Blue" id="coundowntime<%# Eval("xconfMcuIndex") %><%# Container.DataItemIndex +1 %>" onload="javascript:MessageBox()" visible="true" value="0" ></label> <%--Tamil1--%>
                                            </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField ItemStyle-BackColor="#D4D7DA" ItemStyle-ForeColor="#000000" ItemStyle-Width="57%" ItemStyle-HorizontalAlign="Right" >
                                                <ItemTemplate>
                                                  <input type="hidden" id="userID" value="<%=Session["UserID"]%>" />
                                                    <input type="hidden" id="conUserId<%# Eval("xparentId") %>" value="<%=Session["UserID"]%>" />
                                                    <input type="hidden" id="conID<%# Eval("xparentId") %>" value="<%# Eval("confId") %>" />
                                                    <input type="hidden" id="conActualStatus<%# Eval("xparentId") %>" value="<%# Eval("confActualStatus") %>" />
                                                    <input type="hidden" id="conUniqID<%# Eval("xparentId") %>" value="<%# Eval("mcuAlias") %>" />
                                                    <input type="hidden" id="conName<%# Eval("xparentId") %>" value="<%# Eval("confName") %>" />
                                                    <input type="hidden" id="conType<%# Eval("xparentId") %>" value="<%# Eval("confType") %>" />                                                    
                                                    <input type="hidden" id="conStatus<%# Eval("xparentId") %>" value="<%# Eval("confStatus") %>" />
                                                    <input type="hidden" id="conStartMode<%# Eval("xparentId") %>" value="<%# Eval("connectingStatus") %>" />
                                                    <input type="hidden" id="conLastRun<%# Eval("xparentId") %>" value="<%# Eval("confLastRun") %>" />
                                                    <input type="hidden" id="hdnProfileID" value="<%=Session["ProfileID"]%>" />
                                                    <input type="hidden" id="hdnMcuIP<%# Eval("xparentId") %>" value="<%# Eval("McuIP")%>" /> 
                                                    <input type="hidden" id="hdntime<%# Eval("xconfMcuIndex") %><%# Container.DataItemIndex +1 %>" value="<%# Eval("ConfFinishingTime")%>" /> 
                                                    <input type="hidden" id="hdnSetlayoutVal<%# Eval("xparentId") %>" value="<%# Eval("CurSetlayoutValue") %>" />     
                                                    <input type="hidden" id="conforgID<%# Eval("xparentId") %>" value='<%# Eval("conforgID") %>' /><%--FB 2646--%>
													<input type="hidden" id="PartyListCount<%# Eval("xparentId") %>" value="<%# Eval("partCount") %>" />    <%--FB 2652--%> 
                                                    
                                                    <img src="../image/MonitorMCU/type_<%# Eval("ConftypeStatus") %>.gif" title="Audio and Video"  id="Av<%# Eval("xparentId") %>" alt="" width="25px" /> <%--FB 2441 II--%>
                                                    <%-- FB 2652 Starts--%>
                                                    <img src="../image/MonitorMCU/MuteAll_0.png" class="MuteAll" style="cursor:pointer;visibility:<%# (Eval("confMuteAllExcept").ToString() == "1")?"visible":"hidden" %>" " title="Mute all except"   id="<%# Eval("xparentId") %>"   alt="" width="25px" height="25px" /> 
                                                    <img src="../image/MonitorMCU/MuteAll_1.png" class="UnMuteAll" title="Unmute all except" id="<%# Eval("xparentId") %>"  style="cursor:pointer;visibility:<%# (Eval("confUnmuteAll").ToString() == "1")?"visible":"hidden" %>"  alt="" width="25px" height="25px" />  
                                                    <%--FB 2652 Ends--%>
                                                    <img src="../image/MonitorMCU/call_1.gif" class="GridEvents" style="cursor:pointer;" title="Connect All" id="call<%# Eval("xparentId") %>" alt="" width="25px" />                                                    
                                                    <img src="../image/MonitorMCU/call_0.gif" class="GridEvents" style="cursor:pointer;" title="Disconnect All" id="call<%# Eval("xparentId") %>" alt="" width="25px" />
                                                    <img src="../image/MonitorMCU/addUser.gif" class="addUser" style="cursor:pointer; visibility:<%# (Eval("ConflockStatus").ToString() != "1")?"visible":"hidden" %>" title="Add Party" id="addUser<%# Eval("xparentId") %>" alt="" width="25px" />                                                    
                                                    <img src="../image/MonitorMCU/lock_<%# Eval("ConflockStatus") %>.gif" style="cursor:pointer; visibility:<%# (Eval("ConfLockVisibleStatus2").ToString() == "1")?"visible":"hidden" %>" title="Lock status" class="GridEvents" id="lock<%# Eval("xparentId") %>" alt="" width="25px" />                                                                                                        
                                                    <img src="../image/MonitorMCU/message_1.gif" title="Send Message" alt="" style="cursor:pointer; visibility:<%# (Eval("ConfMessageVisibleStatus2").ToString() == "1")?"visible":"hidden" %>" class="GridEvents" id="message<%# Eval("xparentId") %>" width="25px" />
                                                    <img src="../image/MonitorMCU/audioTx_<%# Eval("ConfAudioTxStatus") %>.gif" style="cursor:pointer; visibility:<%# (Eval("ConfAudioTXVisibleStatus2").ToString() == "1")?"visible":"hidden" %>" class="GridEvents" title="Mute receive audioTx" id="audioTx<%# Eval("xparentId") %>" alt="" width="25px" />
                                                    <img src="../image/MonitorMCU/audioRx_<%# Eval("ConfAudioRxStatus") %>.gif" class="GridEvents" style="cursor:pointer; visibility:<%# (Eval("ConfAudioRXVisibleStatus2").ToString() == "1")?"visible":"hidden" %>" title="Mute receive audioRx" id="audioRx<%# Eval("xparentId") %>" alt="" width="25px" />
                                                    <img src="../image/MonitorMCU/videoTx_<%# Eval("ConfVideoTxStatus") %>.gif" title="Mute VideoTx" class="GridEvents" style="cursor:pointer; visibility:<%# (Eval("ConfVideoTXVisibleStatus2").ToString() == "1")?"visible":"hidden" %>" id="videoTx<%# Eval("xparentId") %>" alt="" width="25px" />
                                                    <img src="../image/MonitorMCU/videoRx_<%# Eval("ConfVideoRxStatus") %>.gif" title="Mute VideoRx" class="GridEvents" style="cursor:pointer; visibility:<%# (Eval("ConfVideoRXVisibleStatus2").ToString() == "1")?"visible":"hidden" %>" id="videoRx<%# Eval("xparentId") %>" alt="" width="25px" />
                                                    <img src="../image/MonitorMCU/time_1.gif" title="Extend Time" class="GridEvents" style="cursor:pointer;" id="time<%# Eval("xparentId") %>" alt="" width="25px" />
                                                    <img src="../image/MonitorMCU/delete_1.gif" title="Terminate" class="GridEvents" style="cursor:pointer;" id="delete<%# Eval("xparentId") %>" alt="" width="25px" />
                                                    <img src="../image/MonitorMCU/layout_0.gif" style="visibility:<%# (Eval("ConfLayoutVisibleStatus2").ToString() == "1")?"visible":"hidden" %>" title="Layout" class="layout" id="<%# Eval("xparentId") %>" alt="" width="25px" />
                                                    <%--<img src="../image/MonitorMCU/camera_0.gif" style="visibility:<%# (Eval("ConfCameraVisibleStatus2").ToString() == "1")?"visible":"hidden" %>" title="Camera" class="GridEvents" id="camera<%# Eval("xparentId") %>" alt="" width="25px" />--%> 
                                                    <img src="../image/MonitorMCU/packet_<%# Eval("confpacketlossStatus") %>.gif" style="visibility:<%# (Eval("ConfPacketlossVisibleStatus2").ToString() == "1")?"visible":"hidden" %>" class="GridEvents" id="packet<%# Eval("xparentId") %>" alt="" width="25px" />
                                                    <img src="../image/MonitorMCU/record_<%# Eval("confrecordStatus") %>.gif" style="visibility:<%# (Eval("ConfRecordVisibleStatus2").ToString() == "1")?"visible":"hidden" %>" class="GridEvents" id="record<%# Eval("xparentId") %>" alt="" width="25px" title="Recording" />
                                                    <img src="../image/MonitorMCU/eventLog_1.gif" title="Event Logs" class="EventLog" style="cursor: pointer;" id="eventLog<%# Eval("xparentId") %>" alt="" width="25px" /> <%--FB 2501 Dec7--%>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField ItemStyle-BackColor="#D4D7DA" ItemStyle-ForeColor="#000000">
                                                <ItemTemplate>
                                                    <tr>
                                                        <td colspan="100%" id="Grand_<%# Container.DataItemIndex +1 %>_child_<%# Container.DataItemIndex +1 %>">
                                                            <div id="divid-<%# Eval("xconfName") %>" style="display: none; position: relative;">
                                                                <table width="100%" style="border-collapse:collapse"><tr><td>
                                                                <asp:GridView ID="grdChild1" runat="server" Width="97%" HorizontalAlign="Right" AutoGenerateColumns="false"
                                                                    GridLines="None" ShowHeader="false" >
                                                                    <Columns>                                                    
                                                                        <asp:TemplateField ItemStyle-HorizontalAlign="Center">
                                                                            <ItemTemplate>
                                                                             <tr> 
                                                                                    <td rowspan="100%">
                                                                                        <img src="<%# (Eval("imgUrl").ToString() != "") ? Eval("imgUrl").ToString() : "../image/MonitorMCU/blank.jpg" %>" width="75px" id="streamID" alt="Screen Shot" />
                                                                                    </td>                                                                                    
                                                                                </tr>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                    </Columns>
                                                                </asp:GridView>
                                                                </td></tr><tr><td>
                                                                <asp:GridView ID="grdChild2" CellPadding="1" runat="server" AutoGenerateColumns="false" Width="97%"
                                                                    HeaderStyle-BackColor="#003333" HeaderStyle-ForeColor="white" HorizontalAlign="Right"
                                                                    GridLines="None" ShowHeader="false">
                                                                    <Columns>
                                                                        <asp:TemplateField ItemStyle-BackColor="#AED2F2" ItemStyle-ForeColor="#000000" ItemStyle-Width="15%">
                                                                            <ItemTemplate>
                                                                                <asp:ImageButton ID="participant" ToolTip="participant" runat="server" ImageUrl="~/image/MonitorMCU/participant.gif" Width="25px" />
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>                                                                        
                                                                        <asp:BoundField DataField="participantName" HeaderText="Name" ItemStyle-BackColor="#AED2F2" ItemStyle-ForeColor="#000000" ItemStyle-Width="20%"></asp:BoundField>
                                                                        <asp:BoundField DataField="partAddr" HeaderText="partAddr" ItemStyle-BackColor="#AED2F2" ItemStyle-ForeColor="#000000" ItemStyle-Width="5%"></asp:BoundField>                                                                                                                                               
                                                                        <asp:TemplateField ItemStyle-BackColor="#AED2F2" ItemStyle-ForeColor="#000000" ItemStyle-Width="60%" ItemStyle-HorizontalAlign="Right" >
                                                                            <ItemTemplate>                                                                                                                                                           
																				<input type="hidden" id="participantName<%# Eval("xchildId") %>" value="<%# Eval("participantName")%>" /> <%--FB 2652--%>
                                                                                <input type="hidden" id="partUserId<%# Eval("xchildId") %>" value="<%=Session["UserID"]%>" />
                                                                                <input type="hidden" id="partEndpointID<%# Eval("xchildId") %>" value="<%# Eval("conPartyId") %>" />
                                                                                <input type="hidden" id="partTerminalType<%# Eval("xchildId") %>" value="<%# Eval("conTerminalType") %>" />
                                                                                <input type="hidden" id="confId<%# Eval("xchildId") %>" value="<%# Eval("mergStr") %>" />  
                                                                                <input type="hidden" id="mID<%# Eval("xchildId") %>" value="<%# Eval("bridID") %>" />  
                                                                                <input type="hidden" id="hdnSetlayoutVal<%# Eval("xchildId") %>" value="<%# Eval("LayoutValue") %>" />  
                                                                                
                                                                                <img src="../image/MonitorMCU/TerminalStaus_<%# Eval("partyCallStatusinfo") %>.gif" style="padding-right:30px;" title="" class="GridEvents" alt="" width="25px" />                                                                              
                                                                                <img src="../image/MonitorMCU/call_<%# Eval("partyCallStatus") %>.gif" style="cursor:pointer;" title="" class="GridEvents" id="call<%# Eval("xchildId") %>" alt="" width="25px" />                                                                                                                                                                
                                                                                <img src="../image/MonitorMCU/message_1.gif" title="Send Message" style="cursor:pointer; visibility:<%# (Eval("partymessage3").ToString() == "1" && Eval("partyCallStatus").ToString() != "1")?"visible":"hidden" %>" class="GridEvents" id="message<%# Eval("xchildId") %>" alt="" width="25px" />
                                                                                <img src="../image/MonitorMCU/audioTx_<%# Eval("partyAudioTxStatus") %>.gif" style="cursor:pointer; visibility:<%# (Eval("partyAudioTx3").ToString() == "1" && Eval("partyCallStatus").ToString() != "1")?"visible":"hidden" %>" class="GridEvents" title="Mute receive audioTx" id="audioTx<%# Eval("xchildId") %>" alt="" width="25px" />
                                                                                <img src="../image/MonitorMCU/audioRx_<%# Eval("partyAudioRxStatus") %>.gif" class="GridEvents" style="cursor:pointer; visibility:<%# (Eval("partyAudioRx3").ToString() == "1" && Eval("partyCallStatus").ToString() != "1")?"visible":"hidden" %>" title="Mute receive audioRx" id="audioRx<%# Eval("xchildId") %>" alt="" width="25px" />
                                                                                <img src="../image/MonitorMCU/videoTx_<%# Eval("partyVideoTxStatus") %>.gif" title="Mute VideoTx" class="GridEvents" style="cursor:pointer; visibility:<%# (Eval("partyVideoTx3").ToString() == "1" && Eval("partyCallStatus").ToString() != "1")?"visible":"hidden" %>" id="videoTx<%# Eval("xchildId") %>" alt="" width="25px" />
                                                                                <img src="../image/MonitorMCU/videoRx_<%# Eval("partyVideoRxStatus") %>.gif" title="Mute VideoRx" class="GridEvents" style="cursor:pointer; visibility:<%# (Eval("partyVideoRx3").ToString() == "1" && Eval("partyCallStatus").ToString() != "1")?"visible":"hidden" %>" id="videoRx<%# Eval("xchildId") %>" alt="" width="25px" />
                                                                                <img src="../image/MonitorMCU/meter.gif" title="Packet Info" class="bandwidthmeter" style="cursor:pointer; visibility:<%# (Eval("partypacketloss3").ToString() == "1" && Eval("partyCallStatus").ToString() != "1")?"visible":"hidden" %>" id="meter<%# Eval("xchildId") %>" alt="" width="25px" />
                                                                                <img src="../image/MonitorMCU/delete_1.gif" title="Remove" class="GridEvents" style="cursor:pointer;" id="delete<%# Eval("xchildId") %>" alt="" width="25px" />
                                                                                <img src="../image/MonitorMCU/layout_0.gif" style="visibility:<%# (Eval("partyLayout3").ToString() == "1" && Eval("partyCallStatus").ToString() != "1")?"visible":"hidden" %>" title="Layout" class="layout" id="<%# Eval("xchildId") %>" alt="" width="25px" />
                                                                                <img src="../image/MonitorMCU/camera_0.gif" style="visibility:<%# (Eval("partycamera3").ToString() == "1" && Eval("partyCallStatus").ToString() != "1")?"visible":"hidden" %>" title="Camera" class="GridEvents" id="camera<%# Eval("xchildId") %>" alt="" width="25px" /> 
                                                                                <img src="../image/MonitorMCU/record_<%# Eval("partyrecordStatus") %>.gif" style="visibility:<%# (Eval("partyrecord3").ToString() == "1" && Eval("partyCallStatus").ToString() != "1")?"visible":"hidden" %>" class="GridEvents" id="record<%# Eval("xchildId") %>" alt="" width="25px" title="Recording" />                                                                                
                                                                                <img src="../image/MonitorMCU/LectureMode_<%# Eval("partylecturemode") %>.gif" style="visibility:<%# (Eval("partylecturemode3").ToString() == "1" && Eval("partyCallStatus").ToString() != "1")?"visible":"hidden" %>" title="Lecture Mode" class="GridEvents" alt="" width="25px" id="LectureMode<%# Eval("xchildId") %>" />  <%--FB 2553-RMX--%>
                                                                                <img src="../image/MonitorMCU/LeaderParty_<%# Eval("partychairperson") %>.gif" style="visibility:<%# (Eval("partyLeader").ToString() == "1" && Eval("partyCallStatus").ToString() != "1")?"visible":"hidden" %>" title="Leader Party" class="GridEvents" alt="" width="25px" id="LeaderParty<%# Eval("xchildId") %>" />  <%--FB 2553-RMX--%>                      
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                    </Columns>
                                                                </asp:GridView>
                                                                </td></tr></table>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                    </asp:GridView>
                                </div>
                            </td>
                        </tr>
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
        </asp:GridView>
    </div>
    <div style="display: none">
        <asp:Button ID="btnRefreshPage" runat="server" /></div>        
    </form>
    <input type="hidden" id="msgPopupIdentificationID" />
    <input type="hidden" id="AddUserWindowRedirect" value="" />
    <br />
    <br />
    <br />
    <br />
    <br /> 
    
    <%--Message and Duration Popup window--%>  
    <div id="popmsg" title="My VRM" style="left: 425px; position: absolute; background-color: White;
        top: 420px; z-index: 9999; width: 350px; height:320px; display: none;">
        <table border="0" cellpadding="0" cellspacing="0" style="background-color:" width="100%" style="height:320px;" id="tblpopup">
            <tr style="height: 25px;" id="smsg">
                <td style="background-color: #3075AE;" colspan="2">
                    <b style="font-size: small; margin-left: 10px; color: White; font-family: Verdana;
                        font-style: normal;">Send Message</b>
                </td>
            </tr>
            <tr style="height: 25px;" id="eTime">
                <td style="background-color: #3075AE;" colspan="2">
                    <b style="font-size: small; margin-left: 10px; color: White; font-family: Verdana;
                        font-style: normal;">Extend Conference Time</b> <%--Tamil1--%>
                </td>
            </tr>
             <tr style="height: 25px;" id="eCamera">
                <td style="background-color: #3075AE;" colspan="2">
                    <b style="font-size: small; margin-left: 10px; color: White; font-family: Verdana;
                        font-style: normal;">Camera Direction</b>
                </td>
            </tr>
            <tr class="pMsg">
                <td style="padding-left: 10px;">                    
                    <label>Comments:</label>
                </td>
                <td align="left">
                    <span id="username_warning" class="fontdisplay"></span><br />
                    <textarea id="popuptxt" class="altText" rows="5" cols="10"></textarea>
                </td>
            </tr>
            <tr class="pMsgDuration" >
                <td style="padding-left: 10px;">
                    <label>Duration:</label>
                </td>
                <td align="left">                
                    <input type="text" id="msgMinutes" class="altText" style="width:100px;"/>                    
                    <label>mins</label>                    
                </td>
            </tr> <%--FB 2981--%>
            <tr class="pSendMsgDuration" >
                <td style="padding-left: 10px;">
                    <label>Duration:</label>
                </td>
                <td align="left">                
                    <input type="text" id="msgSec" class="altText" style="width:100px;"/>                    
                    <label>Secs</label>                    
                </td>
            </tr>
            <tr style="height: 40px;" class="pMsg">
                <td style="padding-left: 10px;">
                <label>Direction:</label>                    
                </td>
                <td align="left">
                    <select id="direction" class="altSelectFormat" style="width:100px;">
                        <option value="1" selected="selected">Top</option>
                        <option value="2">Middle</option>
                        <option value="3">Bottom</option>
                    </select>
                </td>
            </tr>
            <tr class="pCamera">
                <td style="padding-left: 10px;">                    
                    <label>Direction:</label>                    
                </td>
                <td align="left">
                    <select id="camDirection" class="altSelectFormat" style="width:100px;" >
                        <option value="1" selected="selected">Up</option>
                        <option value="2">Down</option>
                        <option value="3">Left</option>
                        <option value="4">Right</option>
                        <option value="5">ZoomIn</option>
                        <option value="6">ZoomOut</option>
                        <option value="7">FocusIn</option>
                        <option value="8">FoucusOut</option>
                    </select>
                </td>
            </tr>
            <tr style="height: 40px;">
                <td align="center" colspan="2">
                    <div id="popupstatus" style="display: none;">
                    </div>                    
                    <button id="btnpopupcancel" class="altMedium0BlueButtonFormat">Cancel</button> &nbsp;&nbsp;
                    <button id="btnpopupSubmit" class="altMedium0BlueButtonFormat">Send</button>
                </td>
            </tr>
            <tr>
                <td height="20px;">
                    
                </td>
            </tr>
        </table>
    </div>  
    <%--Bandwidth Information Popup Window--%>
    <div id="BandWidth" title="My VRM" style="left: 425px; position: absolute; background-color: White;
        top: 420px; z-index: 9999; width: 350px; height: 400px; display: none;">        
                <table style='height: 370px;' border='0' cellpadding='0' cellspacing='0' width='100%' id='Table1'>
                <tr style='height: 25px;'><td style='background-color: #3075AE;' colspan='2' align='center'><b style='font-size: small; margin-left: 10px; color: White; font-family: Verdana; font-style: normal;'>Packet Details</b></td></tr>                
                <tr style='height: 40px;'><td style='padding-left: 10px; width:50%;'><b class='fontdisplay'>AudioPacketReceived </b></td><td align='left'><b> : </b><span class='fontdisplay' id="AudioPacketReceived"></span></td></tr>
                <tr style='height: 40px;'><td style='padding-left: 10px; width:50%;'><b class='fontdisplay'>AudioPacketError </b></td><td align='left'><b> : </b><span class='fontdisplay' id="AudioPacketError"></span></td></tr>
                <tr style='height: 40px;'><td style='padding-left: 10px; width:50%;'><b class='fontdisplay'>AudioPacketMissing </b></td><td align='left'><b> : </b><span class='fontdisplay' id="AudioPacketMissing"></span></td></tr>
                <tr style='height: 40px;'><td style='padding-left: 10px; width:50%;'><b class='fontdisplay'>VideoPacketError </b></td><td align='left'><b> : </b><span class='fontdisplay' id="VideoPacketError" ></span></td></tr>
                <tr style='height: 40px;'><td style='padding-left: 10px; width:50%;'><b class='fontdisplay'>VideoPacketReceived </b></td><td align='left'><b> : </b><span class='fontdisplay' id="VideoPacketReceived"></span></td></tr>
                <tr style='height: 40px;'><td style='padding-left: 10px; width:50%;'><b class='fontdisplay'>VideoPacketMissing </b></td><td align='left'><b> : </b><span class='fontdisplay' id="VideoPacketMissing"></span></td></tr>                
                <tr style="height: 40px;"><td align="center" colspan="2"><button id="Cancelbandwidth" class="altMedium0BlueButtonFormat">Close</button></td></tr>
                </table>
        </div>
    <%--Add User Popup with Jquery Wizard--%>
    <div id="PopupAddUser" class="rounded-corners" style="left: 425px; position: absolute;
    background-color: White; top: 420px; z-index: 9999; height: 450px; overflow:hidden; border: 0px;
    width: 700px; display: none;">
    <table border="0" cellpadding="0" cellspacing="0" width="100%">
        <tr style="height: 25px;">
            <td style="background-color: #3075AE;">
                <b style="font-size: small; padding-left: 15px; color: White; font-family: Verdana;
                    font-style: normal;">MonitorMCU</b>
            </td>
        </tr>    
    </table>        
    <input type="hidden" id="Hidden1" value="" />
    <iframe src="" id="addListUser" name="addListUser" style="height: 530px; border: 0px; overflow:hidden; width: 700px; overflow: hidden;"></iframe>
</div>
    <%--Set Layout Popup window--%>
    <div id="PopupSetLayout" class="rounded-corners" style="width:80%; background-color: White;
    top: 320px; z-index: 9999; min-height: 200px; border: 0px; display: none;">
    <table border="0" cellpadding="0" cellspacing="0" width="100%"  class="setLayoutContentTable">
        <tr style="height: 25px;">
            <td style="background-color: #3075AE;">
                <b style="font-size: small; padding-left: 15px; color: White; font-family: Verdana;
                    font-style: normal;">MonitorMCU</b>
            </td>
        </tr>
        <tr style="height:30px;">
            <td>
            </td>
        </tr>       
        <tr id="polycomID">
            <td>
                <table border="0" width="100%" cellpadding="0"> 
                    <tr>
                        <td colspan="4">
                        </td>
                        <td style="padding-right:20px;" colspan="2">
                            <b class="fontdisplay">Current Layout :</b>
                        </td>
                        <td>
                            <img src="" style="width:70px; height:70px;"  alt="" id="CurrentLayout" />
                        </td>
                        <td colspan="2">
                            <b class="fontdisplay">Selected Layout :</b>
                        </td>                        
                        <td colspan="6">
                            <img src="" style="width:70px; height:70px;"  alt="" id="CurSelectedLayout" />
                        </td>
                    </tr>                     
                    <tr>            
                                <% string j; for (int i = 1; i <= 63; i++)
                           {
                               j = (i > 9) ? i.ToString() : "0" + i.ToString(); %>                                                       
                               
                        <td style="width:72px; height:72px;" class="LayoutColour" align="center">
                            <img src="image/displaylayout/<%= j %>.gif" style="width:50px; cursor:pointer; height:50px;" id="<%= j %>" alt="" class="SelectLayout" />
                        </td>                       
                                           
                        <% if ((i % 15) == 0) { %> 
                           </tr><tr> 
                           <% } } %>                           
                           </tr>                           
                </table>
            </td>
        </tr>        
        <tr style="height:50px;">
            <td align="center">
                <button class="altMedium0BlueButtonFormat" id="SetLayoutCancel">Cancel</button>    
                <button class="altMedium0BlueButtonFormat" id="SetLayoutSubmit">Submit</button> 
            </td>
        </tr>
    </table>
</div>
    <%--loader gif animation--%>
    <div id="progressdiv">
        <div id="progressdivwindow" >        
                <table border='0' id="proceeimgid" cellpadding='0' cellspacing='0' style="padding-left:600px; display:none;">
                <tr style='height: 25px;'>
                    <td align="center">
                        <img src="" alt="" />
                    </td>                    
                    </tr>
                </table>
        </div> 
    </div>  
    
    <%--Event log FB 2501 Dec7 Start--%>
    
    <div id="diveventlog" class="rounded-corners" style="left: 425px; position: absolute;
        background-color: White; top: 420px; min-height:200px; z-index: 9999; overflow: hidden;
        border: 0px; width: 700px; display: none;">
        <table align="center" border="0" cellpadding="0"  cellspacing="0" width="100%" id="Table2">
            <tr style='height: 25px;'>
                <td colspan="3" style='background-color: #3075AE;' align='center'>
                    <b style='font-size: small; margin-left: 10px; color: White; font-family: Verdana;
                        font-style: normal;'>Event Logs</b>
                </td>
             </tr>                        
             <tr>
                <td colspan="3" height="20px;"></td>
             </tr>
            <tr>
                <td colspan="3" align="center">
                    <div id="EventLogHtmlContent"></div>
                </td>
            </tr>           
            <tr style="height: 40px;">
                <td align="center" colspan="3">
                    <button id="btnCancelEventLog" class="altMedium0BlueButtonFormat">
                        Close</button>
                </td>
            </tr>
        </table>        
    </div>
    
    <%--Event log FB 2501 Dec7 End--%>
    
    
    <%--FB 2652 Starts--%>
    <div id="PopupMuteAll" class="rounded-corners" style="left: 425px; position: absolute;
        background-color: White; top: 420px; min-height:200px; z-index: 9999; overflow: hidden;
        border: 0px; width: 400px; display: none;">
        <table align="center" border="0" cellpadding="0"  cellspacing="0" width="100%" id="Table3">
            <tr style='height: 25px;'>
                <td colspan="3" style='background-color: #3075AE;' align='center'>
                    <b style='font-size: small; margin-left: 10px; color: White; font-family: Verdana;
                        font-style: normal;'>Mute All Except</b>
                </td>
             </tr>                        
             <tr>
                <td colspan="3" height="20px;"  ></td>
             </tr>
            <tr>
                <td colspan="3" align="center">
                    <div id="divMuteAll"></div>
                </td>
            </tr>           
            <tr style="height: 40px;">
            <td align="right" style="padding-right:10px">
                    <button id="btnMuteAllSubmit" class="altMedium0BlueButtonFormat">
                        Submit</button>
                </td>
                <td align="left" colspan="2">
                    <button id="btnMuteAllClose" class="altMedium0BlueButtonFormat">
                        Close</button>
                </td>
            </tr>
        </table>        
    </div>
    <%--FB 2652 Ends--%>
								
</body>
</html>
<%--Java SCript for refresh the page every 15 sec--%>
<script type="text/javascript">
    function refPage() {            
if(document.getElementById("communStatus").value == "0")
document.getElementById("btnRefreshPage").click();
//alert(document.getElementById("communStatus").value);
//document.getElementById("communStatus").value = "0"
setTimeout("refPage()", 5000);
}

function fnUpdateGridState()
{
var gridState = document.getElementById("hdnGridState");
var ids = gridState.value.split(",");
var i = 0;
for(i = 0; i < ids.length-1; i++)
{
    if (document.getElementById(ids[i]) != null)
       document.getElementById(ids[i]).style.display = 'inline';
    if (document.getElementById("img" + ids[i]) != null)
       document.getElementById("img" + ids[i]).src = 'image/loc/nolines_minus.gif';
}
setTimeout("refPage()", 15000);
}
fnUpdateGridState();


document.getElementById("grdGrand").width = (screen.width - 25) + "px";

    
</script>
<!-- #INCLUDE FILE="inc/mainbottomNET.aspx" -->





