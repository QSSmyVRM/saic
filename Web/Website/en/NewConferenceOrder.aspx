<%--ZD 100147 start--%>
<%--/* Copyright (C) 2013 myVRM - All Rights Reserved	
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.		
*
* You should have received a copy of the myVRM license with	
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/--%><%--ZD 100147 End--%>
<%@ Page Language="C#" CodeFile="NewConferenceOrder.aspx.cs" Inherits="ns_NewConferenceOrder.NewConferenceOrder" viewStateEncryptionMode="Always" SmartNavigation="false"%>

<%@ Register Assembly="MetaBuilders.WebControls.ComboBox" Namespace="MetaBuilders.WebControls"
    TagPrefix="mbcbb" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<script type="text/javascript">
  var servertoday = new Date();
</script>
<script type="text/javascript" src="inc/functions.js"></script>
<%--FB 1861--%>
  <%--<script type="text/javascript" src="script/cal.js"></script>--%>
<script type="text/javascript" src="script/cal-flat.js"></script>
<script type="text/javascript" src="lang/calendar-en.js"></script>
<script type="text/javascript" src="script/calendar-setup.js"></script>
<script type="text/javascript" src="script/calendar-flat-setup.js"></script>
<link rel="stylesheet" type="text/css" media="all" href="css/calendar-blue.css" /> <%--FB 1861--%>  <%--FB 1982--%>

<script runat="server">

</script>
<script language="javascript">
  function ShowHideRow(rType, obj)
  {
    var tempRow = document.getElementById("tr_" + rType);
    //var tempLbl = document.getElementById("lbl_" + rType);
    //alert(obj.src);
    if (obj.src.indexOf("minus") > 0)
    {
        obj.src = obj.src.replace("minus", "plus");
        tempRow.style.display = "none";
        //tempLbl.style.display = "";
    }
    else
    {
        obj.src = obj.src.replace("plus", "minus");
        tempRow.style.display = "";
        //tempLbl.style.display = "none";
    }
  }
  
function OpenItemsList()
{
    url = "ItemsList.aspx";
	if (!window.winrtc) {	// has not yet been defined
		winrtc = window.open(url, "", "width=700,height=300,top=0,left=0,resizable=yes,scrollbars=yes,status=no");
		winrtc.focus();
	} else // has been defined
	    if (!winrtc.closed) {     // still open
	    	winrtc.close();
	        winrtc = window.open(url, "", "width=700,height=300,top=0,left=0,resizable=yes,scrollbars=yes,status=no");
			winrtc.focus();
		} else {
	        winrtc = window.open(url, "", "width=700,height=300,top=0,left=0,resizable=yes,scrollbars=yes,status=no");
	        winrtc.focus();
		}
}

function getYourOwnEmailList (i)
{
	url = "dispatcher/conferencedispatcher.asp?frm=approverNET&frmname=frmMainsuperadministrator&no=" + i + "&cmd=GetEmailList&emailListPage=1&wintype=pop";
	if (!window.winrtc) {	// has not yet been defined
		winrtc = window.open(url, "", "width=700,height=300,top=0,left=0,resizable=yes,scrollbars=yes,status=no");
		winrtc.focus();
	} else // has been defined
	    if (!winrtc.closed) {     // still open
	    	winrtc.close();
	        winrtc = window.open(url, "", "width=700,height=300,top=0,left=0,resizable=yes,scrollbars=yes,status=no");
			winrtc.focus();
		} else {
	        winrtc = window.open(url, "", "width=700,height=300,top=0,left=0,resizable=yes,scrollbars=yes,status=no");
	        winrtc.focus();
		}
}

function btnSubmit_Click()
{
    document.frmSubmit.action = "NewConferenceOrder.aspx?cmd=1";
    document.frmSubmit.submit();
}

function CancelEditWorkOrder()
{
    document.frmSubmit.action = "NewConferenceOrder.aspx?cmd=2";
    document.frmSubmit.submit();
}

function DeleteItem(rownum)
{
    if (confirm("Are you sure you want to delete this item from AV Set?") )
    {
        //alert(document.getElementById("selItems").value + " : " + document.getElementById("itemName" + rownum).value + ":" + document.getElementById("itemImg" + rownum).value + ",");
        document.getElementById("selItems").value = document.getElementById("selItems").value.replace(document.getElementById("itemName" + rownum).value + ":" + document.getElementById("itemImg" + rownum).value + ",", "");
        document.getElementById("itemName" + rownum).value = "Deleted";
        document.getElementById("itemQuantity" + rownum).value = "2";
        document.getElementById("itemRow" + rownum).style.display = "none";
        document.getElementById("itemDeleted" + rownum).checked = "true";
    }
}

function viewconf(cid)
{
	url = "dispatcher/conferencedispatcher.asp?cmd=ViewConference&cid=" + cid;
	confdetail = window.open(url, "viewconference", "width=1,height=1,resizable=yes,scrollbars=yes,status=no");
	confdetail.focus();
}

</script>
<!-- #INCLUDE FILE="inc/maintopNET.aspx" --> 
<html>
<body>

    <form id="frmSubmit" runat="server" method="post" onsubmit="return true">
    <div>
        <table width="100%">
            <tr>
                <td align="center" colspan="3">
                    <h3>
                        Conference Work Orders</h3>
                </td>
            </tr>
            <tr>
                <td align="center" colspan="3">
                    <asp:Label ID="errLabel" runat="server" Font-Size="Small" ForeColor="Red" Visible="False" Font-Bold="True"></asp:Label></td>
            </tr>
            <tr>
                <td align="center" colspan="3">
                    <table border="0" width="90%">
                        <tr>
                            <td align="right" style="font-weight: bold; font-size: small; color: black; font-family: Verdana;
                                height: 19px" width="150">
                                Title:</td>
                            <td align="left" colspan="4" style="font-weight: bold; font-size: small; color: black;
                                font-family: Verdana; height: 19px">
                                <asp:Label ID="lblConfName" runat="server" Font-Bold="True" Font-Names="Verdana"
                                    Font-Size="Medium" ForeColor="Green"></asp:Label>
                                <asp:Label ID="lblConfID" runat="server" Visible="False"></asp:Label></td>
                            <td align="left" colspan="1" rowspan="4" style="font-weight: bold; font-size: medium;
                                color: green; font-family: Verdana" valign="top" width="200">
                            </td>
                        </tr>
                        <tr>
                            <td align="right" style="font-weight: bold; font-size: small; color: black; font-family: Verdana;
                                height: 19px" width="150">
                                Host:</td>
                            <td align="left" style="font-weight: bold; font-size: small; color: black; font-family: Verdana;
                                height: 19px" width="300">
                                <asp:Label ID="lblConfHost" runat="server" Font-Bold="False"></asp:Label></td>
                            <td align="right" style="font-weight: bold; font-size: small; color: black; font-family: Verdana;
                                height: 19px" width="50">
                                Password:</td>
                            <td align="left" colspan="2" style="height: 21px" width="200">
                                <asp:Label ID="lblPassword" runat="server" Font-Names="Verdana" Font-Size="Small"></asp:Label>&nbsp;</td>
                        </tr>
                        <tr>
                            <td align="right" style="font-weight: bold; font-size: small; color: black; font-family: Verdana;
                                height: 21px" valign="top" width="150">
                                Date:</td>
                            <td align="left" style="font-weight: normal; font-size: small; color: black; font-family: Verdana;
                                height: 21px" valign="top" width="300">
                                <asp:Label ID="lblConfDate" runat="server" Font-Bold="False"></asp:Label>,
                                <asp:Label ID="lblConfTime" runat="server" Font-Bold="False"></asp:Label></td>
                            <td align="right" style="font-weight: bold; font-size: small; color: black; font-family: Verdana;
                                height: 21px" valign="top" width="50">
                                Duration:</td>
                            <td align="left" colspan="2" style="height: 21px" valign="top" width="200">
                                <asp:Label ID="lblConfDuration" runat="server" Font-Bold="False" Font-Names="Verdana"
                                    Font-Size="Small"></asp:Label></td>
                        </tr>
                        <tr>
                            <td align="right" style="font-weight: bold; font-size: small; color: black; font-family: Verdana;
                                height: 21px" valign="top" width="150">
                                Public:</td>
                            <td align="left" colspan="4" style="font-weight: bold; font-size: small; color: black;
                                font-family: Verdana; height: 21px" valign="top">
                                <asp:Label ID="lblPublic" runat="server" Font-Bold="False"></asp:Label>
                                <asp:Label ID="lblRegistration" runat="server" Font-Bold="False"></asp:Label></td>
                        </tr>
                        <tr>
                            <td align="right" style="font-weight: bold; font-size: small; color: black; font-family: Verdana;
                                height: 21px" valign="top" width="150">
                                Files:</td>
                            <td align="left" colspan="5" style="font-weight: bold; font-size: small; color: black;
                                font-family: Verdana; height: 21px" valign="top">
                                <asp:Label ID="lblFiles" runat="server" Font-Bold="False"></asp:Label></td>
                        </tr>
                        <tr>
                            <td align="right" style="font-weight: bold; font-size: small; color: black; font-family: Verdana;
                                height: 21px" valign="top" width="150">
                                Description:</td>
                            <td align="left" colspan="5" style="font-weight: bold; font-size: small; color: black;
                                font-family: Verdana; height: 21px" valign="top">
                                <asp:Label ID="lblDescription" runat="server" Font-Bold="False"></asp:Label>&nbsp;</td>
                        </tr>
                        <tr>
                            <td align="left" colspan="6" style="font-weight: bold; font-size: medium; color: green;
                                font-family: Verdana; height: 21px" valign="top">
                                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                    <tr>
                                        <td align="left" colspan="6" style="font-weight: bold; font-size: medium; color: green;
                                            font-family: Verdana; height: 20px" valign="top">
                                            <img id="img_LOC" border="0" onclick="ShowHideRow('LOC', this)" src="image/loc/nolines_minus.gif" />Locations
                                            <asp:Label ID="lblLocCount" runat="server" Font-Bold="True" Font-Names="Verdana"
                                                Font-Size="Small" ForeColor="#404040"></asp:Label></td>
                                    </tr>
                                    <tr id="tr_LOC">
                                        <td align="right" style="font-weight: bold; font-size: small; color: black; font-family: Verdana;
                                            height: 21px" valign="top" width="150">
                                            &nbsp;</td>
                                        <td align="left" colspan="5" style="font-weight: bold; font-size: small; color: black;
                                            font-family: Verdana; height: 21px" valign="top">
                                            <asp:Label ID="lblLocation" runat="server" Font-Bold="False"></asp:Label></td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td align="center" colspan="3">
                <asp:Table ID="EditMainTable" runat="server" BorderColor="Blue" BorderStyle="Double"
                        BorderWidth="1px" CellPadding="0" CellSpacing="0" Width="90%" EnableViewState="true">
                    <asp:TableRow runat="server" BackColor="#FFE0C0" BorderColor="Blue" BorderStyle="Solid"
                            BorderWidth="1px" Font-Bold="True" Font-Names="Verdana" Font-Size="Medium" ForeColor="Blue"
                            Height="30px" HorizontalAlign="Center" VerticalAlign="Middle" EnableViewState="true">
                        <asp:TableCell runat="server" ForeColor="Blue">Name</asp:TableCell>
                        <asp:TableCell runat="server" ForeColor="Blue">Room</asp:TableCell>
                        <asp:TableCell runat="server" ForeColor="Blue">A/V Set</asp:TableCell>
                        <asp:TableCell runat="server" ForeColor="Blue">Status</asp:TableCell>
                        <asp:TableCell runat="server" ForeColor="Blue">Notify</asp:TableCell>
                        <asp:TableCell runat="server" ForeColor="Blue">Reminder</asp:TableCell>
                   </asp:TableRow>
                    <asp:TableRow BackColor="LightGray" runat="server">
                        <asp:TableCell runat="server">
                            <asp:TextBox ID="txtWorkOrderName" runat="server" CssClass="altText" ></asp:TextBox>
                            <asp:TextBox ID="txtWorkOrderID" runat="server" CssClass="altText" Visible="False"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="ValidateWOName" ControlToValidate="txtWorkOrderName" runat="server" ErrorMessage="Required" Display="Dynamic" SetFocusOnError="True"></asp:RequiredFieldValidator>
                        </asp:TableCell>
                        <asp:TableCell runat="server"><asp:DropDownList ID="lstRooms" runat="server" CssClass="altSelectFormat" OnSelectedIndexChanged="selRooms_SelectedIndexChanged" AutoPostBack="True"></asp:DropDownList></asp:TableCell>
                        <asp:TableCell runat="server"><asp:DropDownList ID="lstAVSet" runat="server" CssClass="altSelectFormat" OnSelectedIndexChanged="selAVSet_SelectedIndexChanged" AutoPostBack="True"></asp:DropDownList></asp:TableCell>
                        <asp:TableCell runat="server">
                            <asp:DropDownList runat="server" ID="lstStatus">
                                <asp:ListItem Text="Pending" Value="0"></asp:ListItem>
                                <asp:ListItem Text="Completed" Value="1"></asp:ListItem>
                            </asp:DropDownList>
                        </asp:TableCell>                        
                        <asp:TableCell runat="server"><asp:CheckBox ID="chkNotify" runat="server" /></asp:TableCell>
                        <asp:TableCell runat="server"><asp:CheckBox ID="chkReminder" runat="server" /></asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow ID="TableRow1" runat="server" BackColor="#FFE0C0" BorderColor="Blue" BorderStyle="Solid"
                            BorderWidth="1px" Font-Bold="True" Font-Names="Verdana" Font-Size="Medium" ForeColor="Blue"
                            Height="30px" HorizontalAlign="Center" VerticalAlign="Middle">
                        <asp:TableCell runat="server" ForeColor="Blue">Admin Incharge</asp:TableCell>
                        <asp:TableCell runat="server" ForeColor="Blue" ColumnSpan="3">Completed by</asp:TableCell>
                        <asp:TableCell runat="server" ForeColor="Blue" ColumnSpan="2">Comments</asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow BackColor="LightGray" runat="server">
                        <asp:TableCell runat="server"><asp:TextBox ID="txtApprover1" runat="server" CssClass="altText" ></asp:TextBox>&nbsp;<img ID="ImageButton1" src="image/edit.gif" OnClick="javascript:getYourOwnEmailList(0)" style="cursor:pointer;" title="myVRM Address Book" /> <%--FB 2798 --%><asp:TextBox ID="hdnApprover1" runat="server" Width="0px" BackColor="Transparent" BorderColor="White" BorderStyle="None"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ControlToValidate="txtApprover1" runat="server" ErrorMessage="Required" Display="Dynamic" SetFocusOnError="True"></asp:RequiredFieldValidator>
                        </asp:TableCell>
                        <asp:TableCell ColumnSpan="3" runat="server">
                            <asp:TextBox ID="txtCompletedBy" runat="server" CssClass="altText" ></asp:TextBox><img ID="cal_trigger" Height="20px" src="image/calendar.gif" Onclick="javascript:showCalendar('<%=txtCompletedBy.ClientID %>', 'cal_trigger', 1, '%m/%d/%Y');" Width="20px" />
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" ControlToValidate="txtCompletedBy" runat="server" ErrorMessage="Required" Display="Dynamic" SetFocusOnError="True"></asp:RequiredFieldValidator>
                            <mbcbb:combobox id="completedByTime" runat="server" BackColor="White" CssClass="altSelectFormat" Rows="10" CausesValidation="True" Width="70px">
                                <asp:ListItem Value="01:00 AM" Selected="True"></asp:ListItem>
                                <asp:ListItem Value="02:00 AM"></asp:ListItem>
                                <asp:ListItem Value="03:00 AM"></asp:ListItem>
                                <asp:ListItem Value="04:00 AM"></asp:ListItem>
                                <asp:ListItem Value="05:00 AM"></asp:ListItem>
                                <asp:ListItem Value="06:00 AM"></asp:ListItem>
                                <asp:ListItem Value="07:00 AM"></asp:ListItem>
                                <asp:ListItem Value="08:00 AM"></asp:ListItem>
                                <asp:ListItem Value="09:00 AM"></asp:ListItem>
                                <asp:ListItem Value="10:00 AM"></asp:ListItem>
                                <asp:ListItem Value="11:00 AM"></asp:ListItem>
                                <asp:ListItem Value="12:00 AM"></asp:ListItem>
                                <asp:ListItem Value="01:00 PM" Selected="True"></asp:ListItem>
                                <asp:ListItem Value="02:00 PM"></asp:ListItem>
                                <asp:ListItem Value="03:00 PM"></asp:ListItem>
                                <asp:ListItem Value="04:00 PM"></asp:ListItem>
                                <asp:ListItem Value="05:00 PM"></asp:ListItem>
                                <asp:ListItem Value="06:00 PM"></asp:ListItem>
                                <asp:ListItem Value="07:00 PM"></asp:ListItem>
                                <asp:ListItem Value="08:00 PM"></asp:ListItem>
                                <asp:ListItem Value="09:00 PM"></asp:ListItem>
                                <asp:ListItem Value="10:00 PM"></asp:ListItem>
                                <asp:ListItem Value="11:00 PM"></asp:ListItem>
                                <asp:ListItem Value="12:00 PM"></asp:ListItem>
                            </mbcbb:combobox>
                            <asp:RequiredFieldValidator ID="reqTime" runat="server" ControlToValidate="completedByTime" Display="Dynamic" ErrorMessage="Required"></asp:RequiredFieldValidator>
                            <asp:RegularExpressionValidator ID="regTime" runat="server" ControlToValidate="completedByTime" Display="Dynamic" ErrorMessage="Invalid format" ValidationExpression="\d{2}:\d{2} \w{2}"></asp:RegularExpressionValidator>&nbsp;
                        </asp:TableCell>
                        <asp:TableCell ColumnSpan="2" runat="server"><asp:TextBox ID="txtComments" runat="server" TextMode="MultiLine" Rows="2" CssClass="altText"></asp:TextBox></asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow BackColor="LightGray" runat="server">
                        <asp:TableCell ID="TableCell2" ColumnSpan="6" runat="server" HorizontalAlign="Right">&nbsp;
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow BackColor="LightGray" runat="server">
                        <asp:TableCell ColumnSpan="6" runat="server">
                             <asp:DataGrid runat="server" ID="itemsGrid" AllowSorting="true" BorderColor="Blue" CellPadding="4" Font-Bold="False" Font-Names="Verdana" Font-Size="Small" ForeColor="#333333" GridLines="None" Width="90%" BorderStyle="Solid" BorderWidth="1px" AutoGenerateColumns="true" EnableViewState="true">
                                    <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />

                                    <EditItemStyle BackColor="#999999" />
                                    <SelectedItemStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
                                    <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
                                    <AlternatingItemStyle BackColor="LightGray" ForeColor="#284775" />
                                    <ItemStyle BackColor="#F7F6F3" ForeColor="#333333" />
                                    <HeaderStyle BackColor="#FFE0C0" BorderColor="Blue" Font-Bold="True" Font-Names="Verdana"
                                        Font-Size="Medium" Font-Underline="True" ForeColor="Blue" Height="30px" />
                                </asp:DataGrid>
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow BackColor="LightGray" runat="server">
                        <asp:TableCell ID="TableCell1" ColumnSpan="6" runat="server" HorizontalAlign="Right"> 
                            <asp:Button runat=server ID="btnCancel" OnClick="btnCancel_Click" UseSubmitBehavior="false" CssClass="altShortBlueButtonFormat" Text="Cancel" />&nbsp;
                            <asp:Button runat=server ID="btnSubmit" OnClick="btnSubmit_Click" CssClass="altShortBlueButtonFormat" Text="Submit" Visible="false" />
                        </asp:TableCell>
                    </asp:TableRow>
                </asp:Table>
                </td>
            </tr>
        </table>
    </div>

<img src="keepalive.asp" name="myPic" width="1px" height="1px">&nbsp;</form>
    <script language="javascript">
    if (typeof(reqTime) != "undefined")
        reqTime.controltovalidate = "completedByTime_Text";
    if (typeof(regTime) != "undefined")
        regTime.controltovalidate = "completedByTime_Text";
    </script>
</body></html>
<%--code added for Soft Edge button--%>
<script type="text/javascript" src="inc/softedge.js"></script>
    <!-- #INCLUDE FILE="inc/mainbottomNET.aspx" -->
