<%--ZD 100147 start--%>
<%--/* Copyright (C) 2013 myVRM - All Rights Reserved	
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.		
*
* You should have received a copy of the myVRM license with	
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/--%><%--ZD 100147 End--%>
<%@ Page Language="C#" AutoEventWireup="true" Inherits="OrganisationSettings" %>

<%@ Register TagPrefix="cc1" Namespace="myVRMWebControls" Assembly="myVRMWebControls" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajax" %>
<%@ Register Assembly="MetaBuilders.WebControls.ComboBox" Namespace="MetaBuilders.WebControls"
    TagPrefix="mbcbb" %>
<%--added for FB 1710 start--%>
<%@ Register Assembly="DevExpress.SpellChecker.v10.2.Core, Version=10.2.3.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.XtraSpellChecker" TagPrefix="dxXSC" %>
<%@ Register Assembly="DevExpress.Web.ASPxSpellChecker.v10.2, Version=10.2.3.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxSpellChecker" TagPrefix="dxSC" %>
<%@ Register Assembly="DevExpress.Web.ASPxHtmlEditor.v10.2, Version=10.2.3.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxHtmlEditor" TagPrefix="dxHE" %>
<%@ Register Assembly="DevExpress.Web.ASPxEditors.v10.2, Version=10.2.3.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dxE" %>
<%@ Register Assembly="DevExpress.Web.v10.2, Version=10.2.3.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxPanel" TagPrefix="dxP" %>
<%@ Register Assembly="DevExpress.Web.v10.2, Version=10.2.3.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxRoundPanel" TagPrefix="dxRP" %>
    <%@ Register Assembly="DevExpress.Web.v10.2, Version=10.2.3.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGlobalEvents" TagPrefix="dx" %>
<%--added for FB 1710 end--%>
<%--FB 2779 Starts--%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<meta http-equiv="X-UA-Compatible" content="IE=8" />
<!-- #INCLUDE FILE="inc/maintopNET.aspx" -->
<%--FB 2779 Ends--%>

<script type="text/javascript" language="JavaScript" src="inc\functions.js">
</script>

<script type="text/javascript" src="script/calview.js"></script>

<script type="text/javascript" language="javascript" src='script/lib.js'></script>

<script type="text/javascript" src="inc/functions.js"></script>

<script language="JavaScript">
<!--
//FB Case 807 starts here
function deleteApprover(id)
{
	eval("document.getElementById('hdnApprover" + (id+1) + "')").value = "";
	eval("document.getElementById('txtApprover" + (id+1) + "')").value = "";
}
//FB 2599 Start
function PreservePassword()// FB 2262
{
    document.getElementById("hdnVidyoPassword").value = document.getElementById("txtvidyoPassword1").value; //FB 2363
}
//FB 2599 End

function getYourOwnEmailList (i)
{
    if (i == -2)//Login Management
    {
//        url = "dispatcher/conferencedispatcher.asp?frm=roomassist&frmname=frmMainroom&cmd=GetEmailList&emailListPage=1&wintype=pop";
      if(queryField("sb") > 0 )
            url = "emaillist2.aspx?t=e&frm=approverNET&wintype=ifr&fn=frmMainsuperadministrator&n=";
            else
            url = "emaillist2main.aspx?t=e&frm=approverNET&fn=frmMainsuperadministrator&n=";
    }
    else
    {
//        url = "dispatcher/conferencedispatcher.asp?frm=approver&frmname=frmMainroom&no=" + i + "&cmd=GetEmailList&emailListPage=1&wintype=pop";
        url = "emaillist2main.aspx?t=e&frm=approverNET&fn=frmMainsuperadministrator&n=" + i;
	}
	//url = "dispatcher/conferencedispatcher.asp?frm=approverNET&frmname=frmMainsuperadministrator&no=" + i + "&cmd=GetEmailList&emailListPage=1&wintype=pop";
	if (!window.winrtc) {	// has not yet been defined
	    winrtc = window.open(url, "", "width=920,height=470,top=0,left=0,resizable=yes,scrollbars=yes,status=no"); //FB 2735
		winrtc.focus();
	} else // has been defined
	    if (!winrtc.closed) {     // still open
	    	winrtc.close();
	    	winrtc = window.open(url, "", "width=920,height=470,top=0,left=0,resizable=yes,scrollbars=yes,status=no"); //FB 2735
			winrtc.focus();
		} else {
		winrtc = window.open(url, "", "width=920,height=470,top=0,left=0,resizable=yes,scrollbars=yes,status=no"); //FB 2735
	        winrtc.focus();
		}
				
}


 //code added for custom attribute fixes

function OpenEntityCode()
{
    //window.location.replace("ViewCustomAttributes.aspx"); //FB 2565
    DataLoading(1); // ZD 100176
    window.location = 'ViewCustomAttributes.aspx';
}
// Added for FB 1758
function fnReset()
    {
        document.getElementById("txtTestEmailId").innerText = "";
        document.getElementById("reqvalid").innerText = "";
        document.getElementById("regTestemail").innerText = "";
        document.getElementById("reqvalid2").innerText = "";
        DataLoading(1); //ZD 100176
    }
//FB 1849    
function fnChangeOrganization()
{
    var btnchng = document.getElementById("BtnChangeOrganization");
    var drporg = document.getElementById("DrpOrganization");
    var cnfrm = confirm("Hereafter all the transactions performed in system will be for the selected organization.Do you wish to continue");
            
    if(cnfrm)
        return true;
    else
        return false;
   
}
//FB 2052
function OpenDayColor()
{
    //window.location.replace("HolidayDetails.aspx"); //FB 2565
    DataLoading(1); // ZD 100176
    window.location='HolidayDetails.aspx';
}
//-->
//FB 2343
function WorkingDayDetails()
{
    //window.location.replace("WorkingDays.aspx"); //FB 2565
    DataLoading(1); // ZD 100176
    window.location = 'WorkingDays.aspx';
}
//FB 2486
function OpenManageMsg()
{
    //window.location.replace("ManageMessages.aspx"); //FB 2565
    DataLoading(1); // ZD 100176
    window.location = 'ManageMessages.aspx';
}
//FB 2410
function OpenBatchReport()
{
    DataLoading(1); // ZD 100176
    window.location = 'ManageBatchReport.aspx';
}

//FB 3054 Starts
function PasswordChange(par) {
    document.getElementById("hdnPasschange").value = true;
    if (par == 1)
        document.getElementById("hdnPW1Visit").value = true;
    else
        document.getElementById("hdnPW2Visit").value = true;
}
function fnTextFocus(xid, par) {
    // ZD 100263 Starts
    var obj1 = document.getElementById("txtvidyoPassword1");
    var obj2 = document.getElementById("txtvidyoPassword2");
    if (obj1.value == "" && obj2.value == "") {
        document.getElementById("txtvidyoPassword1").style.backgroundImage = "";
        document.getElementById("txtvidyoPassword2").style.backgroundImage = "";
        document.getElementById("txtvidyoPassword1").value = "";
        document.getElementById("txtvidyoPassword2").value = "";
    }
    return false;
    // ZD 100263 Ends
    var obj = document.getElementById(xid);
    if (par == 1) {
        if (document.getElementById("hdnPW2Visit") != null) {
            if (document.getElementById("hdnPW2Visit").value == "false") {
                document.getElementById("txtvidyoPassword1").value = "";
                document.getElementById("hdnVidyoPassword").value = "";
                document.getElementById("txtvidyoPassword2").value = "";
            } else {
            document.getElementById("txtvidyoPassword1").value = "";
            document.getElementById("hdnVidyoPassword").value = "";
            }
        }
        else {
            document.getElementById("txtvidyoPassword1").value = "";
            document.getElementById("hdnVidyoPassword").value = "";
            document.getElementById("txtvidyoPassword2").value = "";
        }
    }
    else {
        if (document.getElementById("hdnPW1Visit") != null) {
            if (document.getElementById("hdnPW1Visit").value == "false") {
                document.getElementById("txtvidyoPassword1").value = "";
                document.getElementById("hdnVidyoPassword").value = "";
                document.getElementById("txtvidyoPassword2").value = "";
            }
            else {
                document.getElementById("txtvidyoPassword2").value = "";
                document.getElementById("hdnVidyoPassword").value = "";
            }

        }
        else {
            document.getElementById("txtvidyoPassword2").value = "";
            document.getElementById("hdnVidyoPassword").value = "";
        }
    }

    if (document.getElementById("CompareValidator2") != null) {
        ValidatorEnable(document.getElementById('CompareValidator2'), false);
        ValidatorEnable(document.getElementById('CompareValidator2'), true);
    }
}
//FB 3054 Ends
//ZD 100176 start
function DataLoading(val) {
    if (val == "1")
        document.getElementById("dataLoadingDIV").innerHTML = "<img border='0' src='image/wait1.gif'>";
    else
        document.getElementById("dataLoadingDIV").innerHTML = "";
}
//ZD 100176 End
</script>

<script type="text/javascript" src="script\approverdetails.js">

</script>

<html>
<body>
   <div id="topDiv1" style="width:1600px; height:1400px; z-index: 10000px; position:absolute; overlap: left:0px; top:0px; background-color:White;";></div>  <%--FB 2738--%>
    <%--UI Changes for FB 1849--%>
    <form name="frmOrgSettings" id="frmOrgSettings" method="Post" action="OrganisationSettings.aspx"
    language="JavaScript" runat="server">
    <%--FB Icon--%>
   <%-- <asp:ScriptManager ID="CalendarScriptManager" runat="server" AsyncPostBackTimeout="600">
    </asp:ScriptManager>--%>
    <%--FB 1849--%>
    <center>
        <input type="hidden" id="helpPage" value="92">
        <input type="hidden" id="hdnVidyoPassword" runat="server" /> <%--FB 2262 //FB 2599--%>
        <input type="hidden" id="hdnMailServer" runat="server" />
        <input type="hidden" id="hdnLDAPPassword" runat="server" />
        <%--<input type="hidden" id="hdnFooterMsg" runat="server" />--%><%--FB 2681--%><%--FB 3011--%>
        <input type="hidden" id="hdnPasschange" runat="server" /> <%--FB 3054--%>
          <input type="hidden" id="hdnPW1Visit" value="false" runat="server"/> <%--FB 3054--%>
       <input type="hidden" id="hdnPW2Visit" value="false" runat="server"/> <%--FB 3054--%>
        <h3>
            Organization Settings</h3>
        <asp:Label ID="errLabel" runat="server" Text="" CssClass="lblError"></asp:Label><br />
    </center>
    <div id="dataLoadingDIV" align="center"></div> <%--ZD 100176--%>
    <%--FB 1710 Alignment change start--%>
    <table width="100%" border="0">
    <tr><%--FB 2681--%>
        <td>
            <%--FB 1849--%>
            <%--FB 1982--%>
            <table width="100%" border="0">
                <tr>
                    <td>
                        <table width="100%">
                            <%--FB 1849--%>
                            <tr>
                                <td>
                                    <table width="100%" border="0">
                                        <tr valign="top" id="trSwt" runat="server">
                                            <td colspan="2" align="right" valign="top" style="display: none">
                                                <a id="ChgOrg" runat="server" href="#" class="blueblodtext">Switch Organization
                                                </a>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td width="40%" valign="top">
                                                <table width="100%">
                                                    <%--FB 1982--%>
                                                    <%--TD Width Updated for FB 2050--%>
                                                    <tr>
                                                        <td align="left" class="subtitleblueblodtext" colspan="5" style="height: 21; font-weight: bold">
                                                            Organization Resources
                                                        </td>
                                                        <%--CSS Project--%>
                                                    </tr>
                                                    <tr><%--FB 2579 Start--%>
                                                        <td align="right" height="21" style="font-weight: bold; width: 5%">
                                                        </td>
                                                        <td style="width: 45%" align="left" valign="top" class="blackblodtext">
                                                            Video Rooms
                                                        </td>
                                                        <!-- FB 2050 -->
                                                        <td style="width: 50%" align="left" valign="top" colspan="3">
                                                            <!-- FB 2050 -->
                                                            <asp:Label ID="LblActRooms" runat="server" ForeColor="DarkGray" BorderColor="#cccccc"
                                                                BorderStyle="Solid" BorderWidth="1px" Width="40px" Style="text-align: right"></asp:Label>
                                                        </td>
                                                    </tr>
                                                     <tr>
                                                        <td align="right" height="21" style="font-weight: bold">
                                                        </td>
                                                        <td align="left" valign="top" class="blackblodtext">
                                                            EndPoints
                                                        </td>
                                                        <td align="left" class="altblackblodttext" valign="top" colspan="3">
                                                            <asp:Label ID="LblEpts" runat="server" ForeColor="DarkGray" BorderColor="#cccccc"
                                                                BorderStyle="Solid" BorderWidth="1px" Width="40px" Style="text-align: right"></asp:Label>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="right" height="21" style="font-weight: bold">
                                                        </td>
                                                        <td align="left" valign="top" class="blackblodtext">
                                                            Non-Video Rooms
                                                        </td>
                                                        <td align="left" valign="top" colspan="3">
                                                            <asp:Label ID="LblNonVidRooms" runat="server" ForeColor="DarkGray" BorderColor="#cccccc"
                                                                BorderStyle="Solid" BorderWidth="1px" Width="40px" Style="text-align: right"></asp:Label>
                                                        </td>
                                                    </tr>
                                                    <%--FB 2586 START--%>
                                                     <tr>
                                                        <td align="left" height="21" style="font-weight: bold">
                                                        </td>
                                                        <td align="left" valign="top" class="blackblodtext">
                                                            VMR Rooms
                                                        </td>
                                                        <td align="left" valign="top" colspan="3">
                                                            <asp:Label ID="LblVMRRooms" runat="server" ForeColor="DarkGray" BorderColor="#cccccc"
                                                                BorderStyle="Solid" BorderWidth="1px" Width="40px" Style="text-align: right"></asp:Label>
                                                        </td>
                                                    </tr>
                                                    <%--FB 2586 END--%>
                                                    <tr>
                                                        <td align="right" height="21" style="font-weight: bold">
                                                        </td>
                                                        <td align="left" valign="top" class="blackblodtext">
                                                            Guest Rooms
                                                        </td>
                                                        <td align="left" class="altblackblodttext" valign="top" colspan="3">
                                                            <asp:Label ID="LblGstRooms" runat="server" ForeColor="DarkGray" BorderColor="#cccccc"
                                                                BorderStyle="Solid" BorderWidth="1px" Width="40px" Style="text-align: right"></asp:Label>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="right" height="21" style="font-weight: bold">
                                                        </td>
                                                        <td align="left" valign="top" class="blackblodtext">
                                                            Standard MCU <%--FB 2486--%>
                                                        </td>
                                                        <td align="left" class="altblackblodttext" valign="top" colspan="3">
                                                            <asp:Label ID="LblActMCU" runat="server" ForeColor="DarkGray" BorderColor="#cccccc"
                                                                BorderStyle="Solid" BorderWidth="1px" Width="40px" Style="text-align: right"></asp:Label>
                                                        </td>
                                                        </tr>
                                                        <%--FB 2486 Start--%>
                                                        <tr>
                                                        <td align="right" height="21" style="font-weight: bold">
                                                        </td>
                                                        <td align="left" valign="top" class="blackblodtext">
                                                            Enhanced MCU
                                                        </td>
                                                        <td align="left" class="altblackblodttext" valign="top" colspan="3">
                                                            <asp:Label ID="LblEnchancedMCU" runat="server" ForeColor="DarkGray" BorderColor="#cccccc"
                                                                BorderStyle="Solid" BorderWidth="1px" Width="40px" Style="text-align: right"></asp:Label>
                                                        </td>
                                                        </tr>
                                                        <%--FB 2486 End--%>
													<%--FB 2694 START--%>
                                                     <tr>
                                                        <td align="left" height="21" style="font-weight: bold">
                                                        </td>
                                                        <td align="left" valign="top" class="blackblodtext">
                                                            Hotdesking Video Rooms
                                                        </td>
                                                        <td align="left" valign="top" colspan="3">
                                                            <asp:Label ID="LblHDVRooms" runat="server" ForeColor="DarkGray" BorderColor="#cccccc"
                                                                BorderStyle="Solid" BorderWidth="1px" Width="40px" Style="text-align: right"></asp:Label>
                                                        </td>
                                                    </tr>
                                                     <tr>
                                                        <td align="left" height="21" style="font-weight: bold">
                                                        </td>
                                                        <td align="left" valign="top" class="blackblodtext">
                                                            Hotdesking Non-Video Rooms
                                                        </td>
                                                        <td align="left" valign="top" colspan="3">
                                                            <asp:Label ID="LblHDNVRooms" runat="server" ForeColor="DarkGray" BorderColor="#cccccc"
                                                                BorderStyle="Solid" BorderWidth="1px" Width="40px" Style="text-align: right"></asp:Label>
                                                        </td>
                                                    </tr>
                                                    <%--FB 2694 END--%>
                                                    <tr>
                                                        <td align="right" height="21" style="font-weight: bold">
                                                        </td>
                                                        <td align="left" valign="top" class="blackblodtext">
                                                            Users
                                                        </td>
                                                        <td align="left" class="altblackblodttext" valign="top" colspan="3">
                                                            <asp:Label ID="LblActUsers" runat="server" ForeColor="DarkGray" BorderColor="#cccccc"
                                                                BorderStyle="Solid" BorderWidth="1px" Width="40px" Style="text-align: right"></asp:Label>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="right" height="21" style="font-weight: bold">
                                                        </td>
                                                        <td align="left" valign="top" class="blackblodtext">
                                                            Outlook Users
                                                        </td>
                                                        <%--FB 2098--%>
                                                        <td align="left" class="altblackblodttext" valign="top" colspan="3">
                                                            <asp:Label ID="LblExchUser" runat="server" ForeColor="DarkGray" BorderColor="#cccccc"
                                                                BorderStyle="Solid" BorderWidth="1px" Width="40px" Style="text-align: right"></asp:Label>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="right" height="21" style="font-weight: bold">
                                                        </td>
                                                        <td align="left" valign="top" class="blackblodtext">
                                                            Notes Users
                                                        </td>
                                                        <%--FB 2098--%>
                                                        <td align="left" class="altblackblodttext" valign="top" colspan="3">
                                                            <asp:Label ID="LblDuser" runat="server" ForeColor="DarkGray" BorderColor="#cccccc"
                                                                BorderStyle="Solid" BorderWidth="1px" Width="40px" Style="text-align: right"></asp:Label>
                                                        </td>
                                                    </tr>
                                                    
                                                    <tr>
                                                        <%--FB 1979--%>
                                                        <td align="right" height="21" style="font-weight: bold">
                                                        </td>
                                                        <td align="left" valign="top" class="blackblodtext">
                                                            Mobile Users
                                                        </td>
                                                        <td align="left" class="altblackblodttext" valign="top" colspan="3">
                                                            <asp:Label ID="LblMobUser" runat="server" ForeColor="DarkGray" BorderColor="#cccccc"
                                                                BorderStyle="Solid" BorderWidth="1px" Width="40px" Style="text-align: right"></asp:Label>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <%--FB 2693--%>
                                                        <td align="right" height="21" style="font-weight: bold">
                                                        </td>
                                                        <td align="left" valign="top" class="blackblodtext">
                                                            PC Users
                                                        </td>
                                                        <td align="left" class="altblackblodttext" valign="top" colspan="3">
                                                            <asp:Label ID="LblPCUser" runat="server" ForeColor="DarkGray" BorderColor="#cccccc"
                                                                BorderStyle="Solid" BorderWidth="1px" Width="40px" Style="text-align: right"></asp:Label>
                                                        </td>
                                                    </tr>
                                                    <%--FB 2426 Start--%>
                                                    
                                                    <tr>
                                                        <td align="right" height="21" style="font-weight: bold">
                                                        </td>
                                                        <td align="left" valign="top" class="blackblodtext">
                                                            Guest Rooms Per User
                                                        </td>
                                                        <td align="left" class="altblackblodttext" valign="top" colspan="3">
                                                            <asp:Label ID="LblGstRoomsPerUser" runat="server" ForeColor="DarkGray" BorderColor="#cccccc"
                                                                BorderStyle="Solid" BorderWidth="1px" Width="40px" Style="text-align: right"></asp:Label>
                                                        </td>
                                                    </tr>
                                                    
                                                    <%--FB 2426 End--%>
                                                    <tr>
                                                        <td align="right" height="21" style="font-weight: bold">
                                                        </td>
                                                        <td align="left" valign="top" class="blackblodtext">
                                                            Catering Module<!-- FB 2570 -->
                                                        </td>
                                                        <td align="left" class="altblackblodttext" valign="top" colspan="3">
                                                            <asp:Label ID="LblFdMod" runat="server" Text="Disabled"></asp:Label>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="right" height="21" style="font-weight: bold">
                                                        </td>
                                                        <td align="left" valign="top" class="blackblodtext">
                                                            Facility Services Module <%-- FB 2570 --%>
                                                        </td>
                                                        <td align="left" class="altblackblodttext" valign="top" colspan="3">
                                                            <asp:Label ID="LblRsMod" runat="server" Text="Disabled"></asp:Label>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="right" height="21" style="font-weight: bold">
                                                        </td>
                                                        <td align="left" valign="top" class="blackblodtext">
                                                            Audiovisual Module <%-- FB 2570 --%>
                                                        </td>
                                                        <td align="left" class="altblackblodttext" valign="top" colspan="3">
                                                            <asp:Label ID="LblFacility" runat="server" Text="Disabled"></asp:Label>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="right" height="21" style="font-weight: bold">
                                                        </td>
                                                        <td align="left" valign="top" class="blackblodtext">
                                                            API Module
                                                        </td>
                                                        <td align="left" class="altblackblodttext" valign="top" colspan="3">
                                                            <asp:Label ID="LblApi" runat="server" Text="Disabled"></asp:Label>
                                                        </td>
                                                    </tr>
                                                     <tr>
                                                        <td align="right" height="21" style="font-weight: bold">
                                                        </td>
                                                        <td align="left" valign="top" class="blackblodtext">
                                                            Blue Jeans
                                                        </td>
                                                        <td align="left" class="altblackblodttext" valign="top" colspan="3">
                                                            <asp:Label ID="LblBJ" runat="server" Text="Disabled"></asp:Label>
                                                        </td>
                                                    </tr>
                                                     <tr>
                                                        <td align="right" height="21" style="font-weight: bold">
                                                        </td>
                                                        <td align="left" valign="top" class="blackblodtext">
                                                            Jabber
                                                        </td>
                                                        <td align="left" class="altblackblodttext" valign="top" colspan="3">
                                                            <asp:Label ID="LblJabber" runat="server" Text="Disabled"></asp:Label>
                                                        </td>
                                                    </tr>
                                                     <tr>
                                                        <td align="right" height="21" style="font-weight: bold">
                                                        </td>
                                                        <td align="left" valign="top" class="blackblodtext">
                                                            Lync
                                                        </td>
                                                        <td align="left" class="altblackblodttext" valign="top" colspan="3">
                                                            <asp:Label ID="LblLync" runat="server" Text="Disabled"></asp:Label>
                                                        </td>
                                                    </tr>
                                                     <tr>
                                                        <td align="right" height="21" style="font-weight: bold">
                                                        </td>
                                                        <td align="left" valign="top" class="blackblodtext">
                                                            Vidtel
                                                        </td>
                                                        <td align="left" class="altblackblodttext" valign="top" colspan="3">
                                                            <asp:Label ID="LblVidtel" runat="server" Text="Disabled"></asp:Label>
                                                        </td>
                                                    </tr>
                                                    <%--FB 2593 Start--%>
                                                    <tr>
                                                        <td align="right" height="21" style="font-weight: bold">
                                                        </td>
                                                        <td align="left" valign="top" class="blackblodtext">
                                                            Adv Rep Mod
                                                        </td>
                                                        <td align="left" class="altblackblodttext" valign="top" colspan="3">
                                                            <asp:Label ID="LblAdvReport" runat="server" Text="Disabled"></asp:Label>
                                                        </td>
                                                    </tr>
                                                    <%--FB 2593 End--%>
                                                    <%--FB 2347--%>
                                                    <%--<tr>
                                                        <td align="right" height="21" style="font-weight: bold">
                                                        </td>
                                                        <td align="left" valign="top" class="blackblodtext">
                                                            PC Module
                                                        </td>
                                                        <td align="left" class="altblackblodttext" valign="top" colspan="3">
                                                            <asp:Label ID="LblPC" runat="server" Text="Disabled"></asp:Label>
                                                        </td>
                                                    </tr>--%>
                                                     <%--FB 2262 //FB 2599 FB 2645 Starts--%>
                                                     <%if (Session["Cloud"] != null && Session["Cloud"].ToString().Equals("1"))
                                                       {%> 
                                                    <tr>
                                                        <td align="right" height="21" style="font-weight: bold">
                                                        </td>
                                                        <td align="left" valign="top" class="blackblodtext"><%--FB 2834--%>
                                                            Vidyo
                                                        </td>
                                                        <td align="left" class="altblackblodttext" valign="top" colspan="3">
                                                            <asp:Label ID="LblCloud" runat="server" Text="Disabled"></asp:Label>
                                                        </td>
                                                    </tr>
                                                    <%} %>
                                                    <%--FB 2262 //FB 2599 Ends--%>
                                                    <%--FB 2594 FB 2645 Starts--%>
                                                    <% if (Session["EnablePublicRooms"] != null && Session["EnablePublicRooms"].ToString().Equals("1"))
                                                       { %>
                                                    <tr>
                                                        <td align="right" height="21" style="font-weight: bold">
                                                        </td>
                                                        <td align="left" valign="top" class="blackblodtext">
                                                            Public Room Service
                                                        </td>
                                                        <td align="left" class="altblackblodttext" valign="top" colspan="3">
                                                            <asp:Label ID="lblPublicRoom" runat="server" Text="Disabled"></asp:Label>
                                                        </td>
                                                    </tr>
                                                    <% } %>
                                                    <%--FB 2594 FB 2645 Ends--%>
													<%--FB 2579 End--%>
                                                    <tr>
                                                        <td align="left" class="subtitleblueblodtext" colspan="5">
                                                            System Approvers
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="right" height="21" style="font-weight: bold">
                                                        </td>
                                                        <td align="right" style="font-weight: bold" class="blackblodtext">
                                                        </td>
                                                        <%--FB 1982 --%>
                                                        <td align="left" style="font-weight: bold; width: 35%" class="style3">
                                                            Approver Name
                                                        </td>
                                                        <td id="tdlblAction" height="21" style="font-weight: bold; width: 10%" class="blackblodtext" runat="server"> <%--FB 2594--%>
                                                            Action
                                                        </td>
                                                        <%--FB 1982 --%>
                                                        <td style="width: 5%">
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="center" height="21" style="font-weight: bold">
                                                        </td>
                                                        <td align="left" height="21" style="font-weight: bold" class="blackblodtext">
                                                            <%--FB 1982--%>
                                                            Primary Approver
                                                        </td>
                                                        <td style="text-align: left" class="style4">
                                                            <asp:TextBox ID="txtApprover1" runat="server" CssClass="altText" Enabled="False"
                                                                Style="width: 95%"></asp:TextBox>
                                                        </td>
                                                        <%--FB 1982 --%>
                                                        <td align="left" id="tdAction" runat="server"> <%--FB 2594--%>
                                                            <a href="javascript: getYourOwnEmailList(0);" onmouseover="window.status='';return true;">
                                                                <img id="Img1" border="0" src="image/edit.gif" alt="" style="cursor:pointer;" title="myVRM Address Book" /></a> <%--FB 2798--%>
                                                            <%--FB 1982--%>
                                                            <a href="javascript: deleteApprover(0);" onmouseover="window.status='';return true;">
                                                                <img border="0" src="image/btn_delete.gif" alt="delete" alt="" width="16" height="16" style="cursor:pointer;" title="Delete"></a> <%--FB 2798--%>
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="hdnApprover1" runat="server" Width="0px" BackColor="White" BorderColor="White"
                                                                BorderStyle="None" Style="display: none"></asp:TextBox>
                                                            <%--FB 1982 --%>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="left">
                                                        </td>
                                                        <td align="left" style="font-weight: bold" class="blackblodtext">
                                                            Secondary Approver 1 <%--FB 3033--%>
                                                        </td>
                                                        <td style="text-align: left;" class="blackblodtext">
                                                            <asp:TextBox ID="txtApprover2" runat="server" CssClass="altText" Enabled="False"
                                                                Style="width: 95%"></asp:TextBox>
                                                        </td>
                                                        <%--FB 1982 --%>
                                                        <td align="left" id="tdAction2" runat="server"> <%--FB 2594--%>
                                                            <a href="javascript: getYourOwnEmailList(1);" onmouseover="window.status='';return true;">
                                                                <img id="Img2" border="0" src="image/edit.gif" style="cursor:pointer;" title="myVRM Address Book"/></a> <%--FB 2798--%>
                                                            <%--FB 1982--%>
                                                            <a href="javascript: deleteApprover(1);" onmouseover="window.status='';return true;">
                                                                <img border="0" src="image/btn_delete.gif" alt="delete" width="16" height="16" style="cursor:pointer;" title="Delete"></a> <%--FB 2798--%>
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="hdnApprover2" runat="server" Width="0px" BackColor="White" BorderColor="White"
                                                                BorderStyle="None" Style="display: none"></asp:TextBox>
                                                        </td>
                                                        <%--FB 1982 --%>
                                                    </tr>
                                                    <tr>
                                                        <td align="left" height="21%" style="font-weight: bold">
                                                        </td>
                                                        <td align="left" height="21" style="font-weight: bold" class="blackblodtext">
                                                            Secondary Approver 2 <%--FB 3033--%>
                                                        </td>
                                                        <td style="text-align: left;" class="style5">
                                                            <asp:TextBox ID="txtApprover3" runat="server" CssClass="altText" Enabled="False"
                                                                Style="width: 95%"></asp:TextBox>
                                                        </td>
                                                        <%--FB 1982 --%>
                                                        <td align="left" id="tdAction3" runat="server"> <%--FB 2594--%>
                                                            <a href="javascript: getYourOwnEmailList(2);" onmouseover="window.status='';return true;">
                                                                <img id="Img3" border="0" src="image/edit.gif"  style="cursor:pointer;" title="myVRM Address Book"/></a> <%--FB 2798--%>
                                                            <%--FB 1982--%>
                                                            <a href="javascript: deleteApprover(2);" onmouseover="window.status='';return true;">
                                                                <img border="0" src="image/btn_delete.gif" alt="delete" width="16" height="16" style="cursor:pointer;" title="Delete"></a> <%--FB 2798--%>
                                                        </td>
                                                        <td style="height: 21px;">
                                                            <asp:TextBox ID="hdnApprover3" runat="server" Width="0px" BackColor="White" BorderColor="White"
                                                                BorderStyle="None" Style="display: none"></asp:TextBox>
                                                        </td>
                                                        <%--FB 1982 --%>
                                                    </tr>
                                                    
                                                    <tr>
                                                        <td align="left" class="subtitleblueblodtext" colspan="5">
                                                            Room Usage Report Settings
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                         <td align="right" height="21" style="font-weight: bold">
                                                        </td>
                                                        <td align="left" class="blackblodtext">
                                                            Accounting Working Days
                                                        </td>
                                                        <td style="height: 21px;" colspan="3">
                                                            <input type="button" name="btnManageDayColor" value="Configure" class="altMedium0BlueButtonFormat"
                                                                onclick="javascript:WorkingDayDetails();" />&nbsp;
                                                        </td>                                                      
                                                    </tr>
                                                    <tr>
                                                         <td align="right" height="21" style="font-weight: bold">
                                                        </td>
                                                        <td align="left" class="blackblodtext">
                                                            Number of Accounting
                                                            Working Hours
                                                        </td>
                                                        <td style="height: 21px;" colspan="3">
                                                            <asp:DropDownList ID="lstWorkingHours" runat="server" CssClass="altSelectFormat"
                                                                Style="width: 30%;">
                                                                <asp:ListItem Value="1">1</asp:ListItem>
                                                                <asp:ListItem Value="2">2</asp:ListItem>
                                                                <asp:ListItem Value="3">3</asp:ListItem>
                                                                <asp:ListItem Value="4">4</asp:ListItem>
                                                                <asp:ListItem Value="5">5</asp:ListItem>
                                                                <asp:ListItem Value="6">6</asp:ListItem>
                                                                <asp:ListItem Value="7">7</asp:ListItem>
                                                                <asp:ListItem Selected="True" Value="8">8</asp:ListItem>
                                                                <asp:ListItem Value="9">9</asp:ListItem>
                                                                <asp:ListItem Value="10">10</asp:ListItem>
                                                                <asp:ListItem Value="11">11</asp:ListItem>
                                                                <asp:ListItem Value="12">12</asp:ListItem>
                                                                <asp:ListItem Value="13">13</asp:ListItem>
                                                                <asp:ListItem Value="14">14</asp:ListItem>
                                                                <asp:ListItem Value="15">15</asp:ListItem>
                                                                <asp:ListItem Value="16">16</asp:ListItem>
                                                                <asp:ListItem Value="17">17</asp:ListItem>
                                                                <asp:ListItem Value="18">18</asp:ListItem>
                                                                <asp:ListItem Value="19">19</asp:ListItem>
                                                                <asp:ListItem Value="20">20</asp:ListItem>
                                                                <asp:ListItem Value="21">21</asp:ListItem>
                                                                <asp:ListItem Value="22">22</asp:ListItem>
                                                                <asp:ListItem Value="23">23</asp:ListItem>
                                                                <asp:ListItem Value="24">24</asp:ListItem>
                                                            </asp:DropDownList>
                                                        </td>
                                                    </tr>
                                                   <%-- FB 2501 EM7 Starts--%>
                                                   <%-- FB 2598 EnableEM7 Starts tr-id --%>
                                                    <tr id="trEM7OrgSetting" runat="server">
                                                    
                                                        <td align="left" class="subtitleblueblodtext" colspan="5">
                                                           EM7 Organization Settings
                                                         </td>
                                                    </tr>
                                                    <tr id="trEM7Organization" runat="server">
                                                     <td align="right" height="10px" style="font-weight: bold">
                                                        </td>
                                                        <td align="left" class="blackblodtext">
                                                            EM7 Organization
                                                        </td>
                                                        <td>
                                                                <asp:DropDownList ID="lstEM7Orgsilo" runat="server" CssClass="altSelectFormat" Style="width: 95%">
                                                                <asp:ListItem Value="-1">No Items</asp:ListItem>
                                                                </asp:DropDownList>
                                                        </td>
                                                    </tr>
                                                    <%-- FB 2598 EnableEM7 tr-id  Ends--%>
                                                   <%-- FB 2501 EM7 Ends--%>
                                                </table>
                                            </td>
                                            <td width="40%" valign="top">
                                                <%--FB 1982--%>
                                                <table width="90%" border="0" cellpadding="2" cellspacing="0">
                                                    <%--FB 1982--%>
                                                    <tr>
                                                        <td align="left" class="subtitleblueblodtext" colspan="5" style="height: 21; font-weight: bold">
                                                            User Interface Settings
                                                            <dx:ASPxGlobalEvents ID="ASPxGlobalEvents1" runat="server">
                                                                <ClientSideEvents
                                                            ControlsInitialized="function(s, e) {
                                                                    window.setTimeout(function(){
                                                                            var input1 = document.getElementById('Button1');
                                                                            input1.focus();
                                                                        },100);
                                                                }"
                                                            />
                                                            </dx:ASPxGlobalEvents>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td width="2%">
                                                        </td>
                                                        <td style="font-weight: bold; width: 15%" class="blackblodtext" colspan="2">
                                                            UI Design Settings &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                        </td>
                                                        <td align="left" width="20%">
                                                            <asp:Button  ID="Button1" runat="server" CssClass="altLongBlueButtonFormat" OnClick="btnChangeUIDesign_Click" OnClientClick="javascript:DataLoading(1)"
                                                                Text="Change UI Design" /> <%--FB 2719 FB 2739--%> <%--ZD 100176--%>
                                                        </td>
                                                        <td width="1%">
                                                        </td>
                                                    </tr>
                                                    <tr style="display:none"><%--FB 2786--%>
                                                        <td width="2%">
                                                        </td>
                                                        <td style="font-weight: bold" class="blackblodtext" colspan="2">
                                                            UI Text Settings &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                        </td>
                                                        <td align="left">
                                                            <input type="button" id="Button2" disabled="disabled" value="Change UI Text" class="altLongBlueButtonFormat"
                                                                onclick="fnTransferPage()" /> <%--FB 2719--%>
                                                        </td>
                                                        <td width="1%">
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <%--FB 2154--%>
                                                        <td align="right" height="21" style="font-weight: bold" width="2%">
                                                        </td>
                                                        <td colspan="2" valign="top" style="font-weight: bold" class="blackblodtext">
                                                            Email Domain
                                                        </td>
                                                        <td valign="top">
                                                            <asp:Button ID="btnEmailDomain" runat="server" Text="Manage Email Domain" OnClick="EditEmaiDomain" OnClientClick="javascript:DataLoading(1)"
                                                                class="altLongBlueButtonFormat" /><%--ZD 100176--%>
                                                        </td>
                                                    </tr>
                                                    <tr style="height: 10px">
                                                        <td colspan="5">
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="width: 15%" align="left" class="subtitleblueblodtext" valign="top" colspan="5">
                                                            Billing Options
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td width="2%">
                                                        </td>
                                                        <td align="left" height="21" style="font-weight: bold; width: 32%" class="blackblodtext"
                                                            colspan="2">
                                                            Billing Scheme
                                                        </td>
                                                        <td style="height: 21px;">
                                                            <asp:DropDownList ID="lstBillingScheme" runat="server" CssClass="altSelectFormat">
                                                            </asp:DropDownList>
                                                        </td>
                                                        <td width="1%">
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td width="2%">
                                                        </td>
                                                        <td align="left" height="21" style="font-weight: bold; width: 30%" class="blackblodtext"
                                                            colspan="2">
                                                            Allow Over Allocation
                                                        </td>
                                                        <td style="height: 21px;">
                                                            <%--FB 2565 Starts--%>
                                                            <asp:DropDownList ID="drpAlloverAllocation" runat="server" Style="width: 30%;" CssClass="altSelectFormat">
                                                                <asp:ListItem Value="0">No</asp:ListItem>
                                                                <asp:ListItem Value="1">Yes</asp:ListItem>
                                                            </asp:DropDownList>
                                                            <%--<asp:CheckBox ID="chkAllowOver" runat="server" />--%>
                                                            <%--FB 2565 Ends--%>
                                                        </td>
                                                        <td width="1%">
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td width="2%">
                                                        </td>
                                                        <td align="left" height="21" style="font-weight: bold; width: 45%" class="blackblodtext"
                                                            colspan="2">
                                                            <%if (Application["Client"].ToString().ToUpper() == "MOJ"){%>Allow Billing on Point-<br />
                                                            to-point Hearing<%}else{ %>Allow Billing on Point-to-point Conferences<%}%><%--added for FB 1428 Start--%>
                                                        </td>
                                                        <td style="height: 21px;">
                                                            <%--FB 2565 Starts--%>
                                                                <asp:DropDownList ID="drpAllowP2PConf" runat="server" Style="width: 30%;" CssClass="altSelectFormat">
                                                                <asp:ListItem Value="0">No</asp:ListItem>
                                                                <asp:ListItem Value="1">Yes</asp:ListItem>
                                                            </asp:DropDownList>
                                                            <%--<asp:CheckBox ID="chkP2P" runat="server" />--%>
                                                            <%--FB 2565 Ends--%>
                                                        </td>
                                                        <td width="1%">
                                                        </td>
                                                    </tr>
                                                    <%--FB 2045 Start--%>
                                                    <tr>
                                                        <td width="2%">
                                                        </td>
                                                        <td align="left" height="21" style="font-weight: bold; width: 30%" class="blackblodtext"
                                                            colspan="2">
                                                            Entity Code
                                                        </td>
                                                        <td style="height: 21px;">
                                                            <asp:Button ID="btnEntityCode" runat="server" Text="Manage Entity Code" class="altLongBlueButtonFormat" OnClientClick="javascript:DataLoading(1)"
                                                                OnClick="bntEntityCode" />
                                                        </td>
                                                        <td width="1%">
                                                        </td>
                                                    </tr>
                                                    <%--FB 2045 End--%>
                                                    <%--FB 1830 starts--%>
                                                    <tr>
                                                        <td style="width: 15%" align="left" class="subtitleblueblodtext" valign="top" colspan="5">
                                                            Language Settings
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td width="2%">
                                                        </td>
                                                        <td align="left" height="21" style="font-weight: bold; width: 32%" class="blackblodtext"
                                                            colspan="2">
                                                            Preferred Language
                                                        </td>
                                                        <td style="height: 21px;">
                                                            <asp:DropDownList ID="drporglang" runat="server" CssClass="altSelectFormat" DataTextField="name"
                                                                DataValueField="ID">
                                                            </asp:DropDownList>
                                                        </td>
                                                        <td width="1%">
                                                        </td>
                                                    </tr>
                                                    <%--FB 1830 ends--%>
                                                    <tr>
                                                        <td align="left" class="subtitleblueblodtext" colspan="5">
                                                            Custom Option
                                                        </td>
                                                    </tr>
                                                    <%if(!(Application["Client"].ToString().ToUpper().Equals(ns_MyVRMNet.vrmClient.MOJ))){%>
                                                    <%--Added for FB 1425 MOJ--%>
                                                    <tr>
                                                        <td width="2%">
                                                        </td>
                                                        <td align="left" height="21" style="font-weight: bold; width: 32%;" class="blackblodtext"
                                                            colspan="2">
                                                            Enable Custom Options
                                                        </td>
                                                        <td style="height: 21px;">
                                                            <asp:DropDownList ID="CustomAttributeDrop" runat="server" Style="width: 30%;" CssClass="altSelectFormat">
                                                                <asp:ListItem Value="0">No</asp:ListItem>
                                                                <asp:ListItem Selected="True" Value="1">Yes</asp:ListItem>
                                                            </asp:DropDownList>
                                                        </td>
                                                        <td width="1%">
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="3">
                                                        </td>
                                                        <td style="text-align: left" width="12%" colspan="2">
                                                            <br />
                                                            <input type="button" name="btnCustomAttribute" value="Manage Custom Options" class="altLongBlueButtonFormat"
                                                                onclick="javascript:OpenEntityCode();" />&nbsp;
                                                        </td>
                                                    </tr>
                                                    <%--code added for custom attribute end --%>
                                                    <%} %><%--Added for FB 1425 MOJ--%>
                                                    <%-- FB 2486 Start--%>
                                                    <tr>
                                                        <td align="left" class="subtitleblueblodtext" colspan="3" >
                                                            Active Message Delivery
                                                        </td>
                                                        <td style="height: 21px;" valign="bottom">
                                                            <input type="button" name="btnManageMsg" value="Manage" class="altLongBlueButtonFormat"
                                                                onclick="javascript:OpenManageMsg();" />&nbsp;
                                                        </td>
                                                    </tr>
                                                    <%-- FB 2486 End--%>
                                                    <%--FB 2052--%>
                                                    <%if (!(Session["isSpecialRecur"].ToString().Equals("0"))){%>
                                                    <%--FB 2343 Start--%>
                                                    <tr>
                                                        <td align="left" class="subtitleblueblodtext" colspan="3">
                                                            Special Recurrence Day Color
                                                        </td>
                                                        <td style="height: 21px;" valign="bottom">
                                                            <input type="button" name="btnManageDayColor" value="Manage Day Color" class="altLongBlueButtonFormat"
                                                                onclick="javascript:OpenDayColor();" />&nbsp;
                                                        </td>
                                                    </tr>
                                                    <%} %>
                                                    <%--FB 2410--%>
                                                    <tr>
                                                        <td align="left" colspan="3" valign="top" class="subtitleblueblodtext">
                                                            Batch Report
                                                        </td>
                                                        <td style="height: 21px;" valign="bottom">
                                                            <input type="button" name="btnManageBatchRpt" runat="server" id="btnManageBatchRpt" value="Manage" 
                                                                 style="width:150pt" onclick="javascript:OpenBatchReport();" />&nbsp; <%--FB 2796--%>
                                                        </td>
                                                    </tr>
                                                    
                                                    <%--FB 2343--%>
                                                    <%--FB 2262 //FB 2599 Starts--%>
                                                    <tr id="trCloud" runat="server" visible="false">
                                                        <td align="left" class="subtitleblueblodtext" colspan="3"><%--FB 2834--%>
                                                            Vidyo
                                                        </td>
                                                        <td style="height: 21px;">
                                                        <asp:Button ID="btnCloudImport" runat="server" Text="Import" CssClass="altShortBlueButtonFormat" OnClick="CloudImport" />
                                                        </td>
                                                    </tr>
                                                    <%--FB 2262 //FB 2599 Ends--%>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td>
            <table width="100%" border="0">
                <tr style="display: none;">
                    <td align="left" class="subtitleblueblodtext" colspan="3">
                        User Interface Settings
                         
                    </td>
                </tr>
                <tr style="display: none;">
                    <%--Window Dressing--%>
                    <td align="left" height="21" style="font-weight: bold; width: auto" class="blackblodtext">
                        UI Design Settings
                    </td>
                    <td style="height: 21px; font-weight: bold" width="30%" colspan="3">
                        <asp:Button ID="btnChangeUIDesign" runat="server" CssClass="altLongBlueButtonFormat"
                            OnClick="btnChangeUIDesign_Click" Text="Change UI Design" />
                    </td>
                    <td align="right" style="height: 21px; width: 15%;">
                    </td>
                    <td style="height: 21px;" width="35%">
                    </td>
                </tr>
                <%-- Code Added for FB 1428--%>
                <tr style="display: none;">
                    <%--Window Dressing--%>
                    <td align="left" height="21" style="font-weight: bold" class="blackblodtext" style="width: 18%">
                        UI Text Settings
                    </td>
                    <td style="height: 21px; font-weight: bold" width="30%">
                        <input type="button" id="btnUITextChange" value="Change UI Text" class="altLongBlueButtonFormat"
                            onclick="fnTransferPage()" />
                    </td>
                    <td align="right" style="height: 21px; width: 15%;">
                    </td>
                    <td style="height: 21px;" width="35%">
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td>
            <%--Mail Logo and Footer message--%>
            <table width="100%" style="height: 21%" border="0">
                <%--FB 1982 --%>
                <%--TD Width Updated for FB 2050--%>
                <tr>
                    <td width="100%" align="left" style="font-weight: bold" valign="top" class="subtitleblueblodtext"
                        colspan="5">
                        Mail Settings
                    </td>
                    <%--FB 1982 --%>
                </tr>
                <tr>
                    <td style="width: 3%">
                    </td>
                    <td style="font-weight: bold; width: 22%" align="left" class="blackblodtext">
                        Mail Logo
                    </td>
                    <%--FB 1982--%>
                    <td style="width: 75%" colspan="5" valign="top" align="left">
                        <%--FB 1982 --%>
                        <table style="width: 100%" border="0">
                            <tr>
                                <td> <%--FB 2909 Start--%>
                                    <div>
                                     <input type="text" class="file_input_textbox" readonly="readonly" value='No file selected' />
                                      <div class="file_input_div"><input type="button" value="Browse" class="file_input_button"  />
                                       <input type="file"  class="file_input_hidden" accept="image/*" id="fleMap1" contenteditable="false" enableviewstate="true" runat="server" onchange="getfilename(this)"/></div></div> <%--FB 3055-Filter in Upload Files--%>
                                       <asp:RegularExpressionValidator ID="regfleMap1" runat="server" Display="Dynamic" ValidationGroup="Submit1" ControlToValidate="fleMap1" CssClass="lblError" ErrorMessage="File type is invalid." ValidationExpression="^.*\.((j|J)(p|P)(e|E)?(g|G)|(g|G)(i|I)(f|F)|(p|P)(n|N)(g|G)|(b|B)(m|M)(p|P))$"></asp:RegularExpressionValidator>
                                    <%--<input type="file" id="fleMap1" contenteditable="false" enableviewstate="true" size="50"
                                        class="altText" runat="server" />--%><%--FB 2909 End--%>
                                    <asp:Button ID="btnUploadImages" OnClick="UploadMailLogoImages" runat="server" Text="Upload Images" style="margin-left:2px"
                                        CssClass="altLongBlueButtonFormat" ValidationGroup="Submit1" />  <%--FB 2909 End--%>
                                    <cc1:ImageControl ID="Map1ImageCtrl" Width="30" Height="30" Visible="false" runat="server">
                                    </cc1:ImageControl>
                                    <asp:Label ID="lblUploadMap1" Text="" Visible="false" runat="server"></asp:Label>
                                    <asp:Button ID="btnRemoveMap1" CssClass="altMedium0BlueButtonFormat" Text="Remove"
                                        Visible="false" runat="server" OnCommand="RemoveFile" CommandArgument="1" />
                                    <asp:Label ID="hdnUploadMap1" Text="" Visible="false" runat="server"></asp:Label>
                                    <input type="hidden" id="Map1ImageDt" name="Map1ImageDt" runat="server" />
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <%--code added for FB 1710 start--%>
                <tr>
                    <td align="left" colspan="5" height="21">
                    </td>
                </tr>
                
                <tr>
                    <td height="21%">
                    </td>
                    <td align="left" valign="top" class="blackblodtext">
                        Footer Message
                    </td>
                    <%--FB 1982 --%>
                    <td colspan="3" height="21" align="left" valign="top" class="blackblodtext">
                        <dxHE:ASPxHtmlEditor ID="dxHTMLEditor" runat="server" Height="200px" >
                            <SettingsImageUpload UploadImageFolder="~/image/maillogo/">
                                <ValidationSettings MaxFileSize="100000" MaxFileSizeErrorText="Footer Image attachment is greater than 100KB. File has not been uploaded" />
                            </SettingsImageUpload>
                        </dxHE:ASPxHtmlEditor>
                        <input type="file" id="fmMap" contenteditable="false" size="50" class="altText" runat="server"
                            visible="false" />
                        <input type="hidden" id="fmMapImage" name="Map1ImageDt" runat="server" height="21%"
                            style="display: none" /><%--FB 1982 --%>
                            
                        
                       
                    </td>
                </tr>
                <tr>
                    <td colspan="5" height="15">
                    </td>
                </tr>
                <%--code added for FB 1710 end--%>
                
                <%--FB 2659 Starts--%>
                <tr id="tdSubject" runat="server">
                <td style="width: 3%">
                    </td>
                <td style="font-weight: bold; width: 22%" align="left" class="blackblodtext">
                Default Appointment Subject
                </td>
                <td>
                <input type="text" id="txtSubject" runat="server" class="altText" style="width:500px;" />
                </td>
                </tr>
                <tr id="tdInvitation" runat="server">
                <td style="width: 3%">
                    </td>
                <td style="font-weight: bold; width: 22%" align="left" class="blackblodtext">
                Default Appointment Invitation
                </td>
                <td>
                <br />
                <textarea id="txtInvitaion" runat="server" rows="2" cols="20" class="altText" style="width:500px;height:100px;" ></textarea>
                </td>
                </tr>
                <%--FB 2659 End--%>
                
                <%--FB 1758 Starts--%>
                <tr>
                    <td colspan="5">
                    <br /><%--FB 2659--%>
                        <table colspan="1" width="100%" border="0" cellpadding="0" cellspacing="0">
                            <%--TD Width Updated for FB 2050--%>
                            <tr>
                                <td height="18%" style="width: 3%">
                                </td>
                                <%--//FB 1830 Language  --%>
                                <td align="left" style="width: 22%" class="blackblodtext">
                                    Test Email ID
                                </td>
                                <%--//FB 1830 Language--%>
                                <%--FB 1982--%>
                                <td colspan="3" align="left" class="style1">
                                    <asp:TextBox ID="txtTestEmailId" runat="server" CssClass="altText" Width="200px"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="reqvalid" runat="server" ControlToValidate="txtTestEmailId"
                                        ValidationGroup="TestEmail" ErrorMessage="Required." Display="dynamic"></asp:RequiredFieldValidator>
                                    <asp:RegularExpressionValidator ID="regTestemail" runat="server" ControlToValidate="txtTestEmailId"
                                        ErrorMessage="Invalid Email Address" Display="dynamic" ValidationExpression="^[a-zA-Z()^][()^\w\.-]*[a-zA-Z0-9()^]@[a-zA-Z0-9][\w\.-]*[a-zA-Z0-9]\.[a-zA-Z][a-zA-Z\.]*[a-zA-Z]$"></asp:RegularExpressionValidator>
                                    <asp:RegularExpressionValidator ID="reqvalid2" ControlToValidate="txtTestEmailId"
                                        Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<br>& < > ' + % \ / ; ? | = ! ` , [ ] { } : # $ ~ and &#34; are invalid characters."
                                        ValidationExpression="^(a-z|A-Z|0-9)*[^\\/<>+;?|!`,\[\]{}\x22;=:#$%&'~]*$"></asp:RegularExpressionValidator>
                                    <asp:Button ID="btntestmail" Text="Test Mail" ValidationGroup="TestEmail" CssClass="altMedium0BlueButtonFormat"
                                        runat="server" OnClick="TestEmailConnection" />
                                </td>
                            </tr>
                            <%--FB 1830 Starts--%>
                            <tr>
                                <td align="left" height="15" style="font-weight: bold">
                                    <%--//FB 1830 Language--%>
                                    <%--FB 1982--%>
                                </td>
                            </tr>
                            <tr>
                                <td align="left" height="21" style="font-weight: bold" style="width: 3%">
                                </td>
                                <%--FB 1982--%>
                                <td align="left" class="blackblodtext" style="width: 22%">
                                    Email Language
                                </td>
                                <%--FB 1982--%>
                                <td align="left" height="21" style="width: 28%">
                                    <%--FB 1860 start--%>
                                    <%--FB 1982 and FB 2050--%>
                                    <asp:Button ID="btnDefine" runat="server" Text="Customize" OnClick="DefineEmailLanguage" OnClientClick="javascript:DataLoading(1);"
                                        CssClass="altMedium0BlueButtonFormat" /><%--ZD 100176--%> 
                                    <%--FB 1982 FB 2104--%>
                                    <asp:TextBox ID="txtEmailLang" runat="server" ReadOnly="true" CssClass="altText"
                                        Visible="false"></asp:TextBox>&nbsp;<%--FB 1830 - DeleteEmailLang 2104--%>
                                    <asp:ImageButton ImageUrl="image/btn_delete.gif" runat="server" ID="delEmailLang"
                                        ToolTip="Delete Email Language" OnClick="DeleteEmailLangugage" OnClientClick="javascript:return fnDelEmailLan()" />
                                    <%--FB 1830 - DeleteEmailLang--%>
                                </td>
                                <td align="left" class="blackblodtext" style="width: 18%">
                                    <%-- FB 2050 --%>
                                    <asp:Label ID="LblBlockEmails" runat="server" Text="Block Emails" nowrap></asp:Label> &nbsp;
                                    <asp:CheckBox ID="ChkBlockEmails" runat="server" />
                                </td>
                                <td align="left" valign="top" width="29%" > 
                                    <asp:Button ID="BtnBlockEmails" Style="display: none;" runat="server" Text="Edit"
                                        OnClick="EditBlockEmails" class="altShortBlueButtonFormat" />
                                    <%--FB 1982--%>
                                    <%--FB 2164--%>
                                </td>
                            </tr>
                        </table>
                        <%--FB 1982--%>
                    </td>
                    <%--FB 1860 end--%>
                </tr>
            </table>
            <%--FB 1982 end--%>
            <%--FB 1830 Ends--%>
            <%--FB 1758 Ends--%>
            <%--Window Dressing end--%>
            <tr>
                <%--FB 2337--%>
                <td>
                    <table width="100%" style="height: 21%" border="0" cellpadding="0" cellspacing="0"> <%--FB 2555--%>
                        <tr>
                            <td align="left" height="21" style="font-weight: bold; width: 3%">
                            </td>
                            <td align="left" class="blackblodtext" style="width: 22%; display:none;"> <%--FB 2848--%>
                                End User License Agreement
                            </td> 
                            <td align="left" height="21" width="28%" style="display:none"> <%--FB 2555--%> <%--FB 2848--%>
                                <asp:Button ID="btnCustLicAgrmnt" runat="server" Text="Customize" CssClass="altMedium0BlueButtonFormat"
                                    OnClick="CustomizeLicenseAgreement" />
                            </td> 
                            <%--FB 2555 Starts--%>
                            <td align="left" class="blackblodtext" style="width: 21%;padding-left: 1px;">Email Date Format
                            </td><%--FB 2848--%>
                            <td align="left" height="21" style="width:71%;"><%--FB 2848--%>
                            <asp:DropDownList ID="drpEmailDateFormat" runat="server" CssClass="altSelectFormat" style="width: 20%"><%--FB 2848--%>
                                <asp:ListItem Value="0">User Preference</asp:ListItem>
                                <asp:ListItem Value="1">European (dd Mmm YYYY)</asp:ListItem></asp:DropDownList>
                            </td> 
                            <%--FB 2555 Ends--%>  
                        </tr>
                    </table>
                </td>
            </tr>
            <%if((Application["External"].ToString() != "")){%>
            <tr>
                <td>
                    <table width="100%" style="height: 21%" border="0">
                        <tr>
                            <td width="100%" align="left" style="font-weight: bold" valign="top" class="subtitleblueblodtext"
                                colspan="5">
                                External Scheduling Settings
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 3%">
                            </td>
                            <td class="blackblodtext" style="width: 22%">
                                Customer Name
                            </td>
                            <td style="width: 28%">
                                <asp:TextBox ID="txtCustomerName" runat="server" CssClass="altText" Width="200px"></asp:TextBox>
                                <asp:RegularExpressionValidator ID="RegularExpressionValidator12" ControlToValidate="txtCustomerName"
                                    ValidationGroup="submit" Display="dynamic" runat="server" SetFocusOnError="true"
                                    ErrorMessage="<br>& < > ' + % \ / ( ) ; ? | ^ = ! ` , [ ] { } : # $ @ ~ and &#34; are invalid characters."
                                    ValidationExpression="^(a-z|A-Z|0-9)*[^\\/<>+;?|!`,\[\]{}\x22;=^:@#$%&()']*$"></asp:RegularExpressionValidator>
                            </td>
                            <td class="blackblodtext" style="width: 18%">
                                Customer ID
                            </td>
                            <td>
                                <asp:TextBox ID="txtCustomerID" runat="server" CssClass="altText" Width="200px"></asp:TextBox>
                                <asp:RegularExpressionValidator ID="RegularExpressionValidator13" ControlToValidate="txtCustomerID"
                                    ValidationGroup="submit" Display="dynamic" runat="server" SetFocusOnError="true"
                                    ErrorMessage="<br>& < > ' + % \ / ( ) ; ? | ^ = ! ` , [ ] { } : # $ @ ~ and &#34; are invalid characters."
                                    ValidationExpression="^(a-z|A-Z|0-9)*[^\\/<>+;?|!`,\[\]{}\x22;=^:@#$%&()']*$"></asp:RegularExpressionValidator>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <%} %>
            <%--FB 2262 //FB 2599 start--%>
            <tr id="trCloudDetails" runat="server" visible="false">
                <td>
                    <table width="100%" style="height: 21%" border="0">
                        <tr>
                            <td width="100%" align="left" style="font-weight: bold" valign="top" class="subtitleblueblodtext"
                                colspan="5"><%--FB 2834--%>
                                Vidyo
                            </td>
                        </tr>     
                        <tr>
                            <td style="width: 3%"></td>
                            <td class="blackblodtext" style="width: 22%">
                                Vidyo URL
                            </td>
                            <td style="width: 28%">
                                <asp:TextBox ID="txtvidyoURL" runat="server" CssClass="altText" TextMode="MultiLine" MaxLength="150"
                                    Width="200px"></asp:TextBox>
                                <asp:RegularExpressionValidator ID="RegularExpressionValidator11" ControlToValidate="txtvidyoURL"
                                    ValidationGroup="submit" Display="dynamic" runat="server" SetFocusOnError="true"
                                    ErrorMessage="<br>& < > ' + % \ ( ) ; ? | ^ = ! ` , [ ] { } # $ @ ~ and &#34; are invalid characters."
                                    ValidationExpression="^(a-z|A-Z|0-9)*[^\\<>+;?|!`,\[\]{}\x22;=^@#$%&()']*$"></asp:RegularExpressionValidator>
                            </td>
                            <td class="blackblodtext" style="width: 18%">
                                Login
                            </td>
                            <td>
                                <asp:TextBox ID="txtvidyoLogin" runat="server" CssClass="altText" Rows="3" MaxLength="30"></asp:TextBox>
                                <asp:RegularExpressionValidator ID="RegularExpressionValidator16" ControlToValidate="txtvidyoLogin"
                                    ValidationGroup="submit" Display="dynamic" runat="server" SetFocusOnError="true"
                                    ErrorMessage="<br>& < > ' + % \ / ( ) ; ? | ^ = ! ` , [ ] { } : # $ @ ~ and &#34; are invalid characters."
                                    ValidationExpression="^(a-z|A-Z|0-9)*[^\\/<>+;?|!`,\[\]{}\x22;=^:@#$%&()']*$"></asp:RegularExpressionValidator>
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 3%"></td>
                            <td style="width: 22%" class="blackblodtext">
                                Password
                            </td>
                            <td style="width: 28%">
                                <asp:TextBox ID="txtvidyoPassword1" style="background-image:url('../image/pwdbg.png'); background-repeat:no-repeat" onchange="javascript:PreservePassword()" runat="server" MaxLength="30"
                                    CssClass="altText" TextMode="Password" onblur="PasswordChange(1)" onfocus=" fnTextFocus(this.id,1)"></asp:TextBox>
                                    <asp:CompareValidator ID="CompareValidator2"
                                    runat="server" ControlToValidate="txtvidyoPassword1" ControlToCompare="txtvidyoPassword2"
                                    ErrorMessage="Passwords do not match." Font-Names="Verdana" Font-Size="X-Small"
                                    Font-Bold="False" Display="Dynamic" />
                            </td>
                            <td style="width: 18%" class="blackblodtext">
                                Retype Password
                            </td>
                            <td style="height: 37px;" width="35%">
                                <asp:TextBox ID="txtvidyoPassword2" style="background-image:url('../image/pwdbg.png'); background-repeat:no-repeat" runat="server" onchange="javascript:PreservePassword()" MaxLength="30"
                                    CssClass="altText" TextMode="Password" onblur="PasswordChange(2)" onfocus=" fnTextFocus(this.id,2)"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 3%"></td>
                            <td class="blackblodtext" style="width: 22%">
                                Proxy Address
                            </td>
                            <td style="width: 28%">
                                <asp:TextBox ID="txtproxyAdd" runat="server" CssClass="altText"></asp:TextBox>
                                <asp:RegularExpressionValidator ID="regTimeoutVlaue" runat="server" ControlToValidate="txtproxyAdd" ValidationGroup="submit" Display="Dynamic" ErrorMessage="Invalid IP Address" ValidationExpression="^[^&<>+'dD][0-9'.]*$"></asp:RegularExpressionValidator>
                            </td>
                            <td class="blackblodtext" style="width: 18%">
                                Port
                            </td>
                            <td>
                                <asp:TextBox ID="txtvidyoPort" runat="server" CssClass="altText" Rows="3" ></asp:TextBox>
                                <asp:RegularExpressionValidator ID="RegularExpressionValidator1" ControlToValidate="txtvidyoPort"
                                    ValidationGroup="submit" Display="dynamic" runat="server" SetFocusOnError="true"
                                    ErrorMessage="<br>& < > ' + % \ / ( ) ; ? | ^ = ! ` , [ ] { } : # $ @ ~ and &#34; are invalid characters."
                                    ValidationExpression="^(a-z|A-Z|0-9)*[^\\/<>+;?|!`,\[\]{}\x22;=^:@#$%&()']*$"></asp:RegularExpressionValidator>
                            </td>
                        </tr>
                        <%--<tr>
                            <td style="width: 3%"></td>
                            <td class="blackblodtext" style="width: 22%">
                                Poll
                            </td>
                            <td style="width: 28%">
                            <%--<input type="button" id="btnvidyoPoll" runat="server" name="btnvidyoPoll" value="Now" class="altShortBlueButtonFormat" >--%>
                                <%--<asp:Button ID="btnvidyoPoll"  runat="server" Text="Now" class="altShortBlueButtonFormat" />
                            </td>
                            <td colspan="2" ></td>
                        </tr>--%>   
                    </table>
                </td>
            </tr>
            <%--FB 2262 //FB 2599 End--%>
            <%-- Commented this, because not deliver for this Phase II delivery
            <tr>
                <td>
                    <table width="100%" style="height: 21%" border="0">
                        <tr>
                            <td width="100%" align="left" style="font-weight: bold" valign="top" class="subtitleblueblodtext"
                                colspan="5">
                                <span class="subtitleblueblodtext">Mailing Error Report Settings</span>
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 3%">
                            </td>                            
                            <td class="blackblodtext" style="width: 22%">
                                Recipient's eMail 
                            </td>
                            <td style="width: 28%">
                                <asp:TextBox ID="txtUsrRptDestination" runat="server" CssClass="altText" Width="200px"></asp:TextBox>
                                <asp:RegularExpressionValidator ID="RegtxtUsrRptDestination" ControlToValidate="txtUsrRptDestination"
                                    ValidationGroup="submit" Display="dynamic" runat="server" SetFocusOnError="true"
                                    ErrorMessage="<br>Invalid email address." ValidationExpression="[a-zA-Z][\w\.-]*[a-zA-Z0-9]@[a-zA-Z0-9][\w\.-]*[a-zA-Z0-9]\.[a-zA-Z][a-zA-Z\.]*[a-zA-Z]$"></asp:RegularExpressionValidator>
                            </td>                        
                            <td class="blackblodtext" style="width: 18%">
                                Frequency
                            </td>
                            <td >
                                <asp:TextBox ID="lstUsrRptFrequencyCount" runat="server" CssClass="altText" Width="70px"></asp:TextBox>
                                (Mins)
                            </td>                            
                        </tr>                        
                    </table>
                </td>
            </tr>--%>
            <tr>
                <td align="center" colspan="5" style="font-weight: bold; font-size: small; color: black;
                    font-family: verdana;" height="21%">
                    <%--<input id="btnReset" type="reset" value="Reset" class="altMedium0BlueButtonFormat" runat="server"
                        onclick="javascript:fnReset();" />&nbsp;--%>
                        <asp:Button ID="btnReset" Text="Reset" CssClass="altMedium0BlueButtonFormat" runat="server" 
                             OnClientClick="javascript:fnReset();" />&nbsp; <%--ZD 100263--%>
                    <asp:Button runat="server" ID="btnSubmit"  OnClick="btnSubmit_Click" Width="100pt" OnClientClick="DataLoading(1)"
                        Text="Submit"></asp:Button><%-- FB 2796--%> <%--ZD 100176--%> 
                </td>
            </tr>
            <input align="center" type="hidden" name="formname" id="formname" value="frmMainsuperadministrator" />
            <img src="keepalive.asp" name="myPic" alt="" width="1" height="1" />
            <tr>
                <%--FB 1849 Start--%>
                <%--FB 2719 Starts--%>
                <%--<td colspan="5" align="center">
                    <ajax:ModalPopupExtender ID="RoomPopUp" runat="server" TargetControlID="ChgOrg" BackgroundCssClass="modalBackground"
                        PopupControlID="switchOrgPnl" DropShadow="false" Drag="true" CancelControlID="ClosePUp">
                    </ajax:ModalPopupExtender>
                    <asp:Panel ID="switchOrgPnl" runat="server" HorizontalAlign="Center" Width="30%"
                        CssClass="treeSelectedNode">
                        <table width="100%" align="center" border="0">
                            <tr>
                                <td align="center" class="blackblodtext">
                                    <span class="subtitleblueblodtext">Switch Organization</span><br />
                                    <p>
                                        All the transactions performed will be for the below selected organization after
                                        the switch.
                                    </p>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    &nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td align="center">
                                    <asp:DropDownList ID="DrpOrganization" DataTextField="OrganizationName" DataValueField="OrgId"
                                        runat="server" CssClass="altLong0SelectFormat" Width="200px">
                                    </asp:DropDownList>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    &nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td align="center">
                                    <asp:Button runat="server" ID="BtnChangeOrganization" CssClass="altShortBlueButtonFormat"
                                        Text=" Submit " OnClientClick="javascript:return fnChangeOrganization();" OnClick="btnChgOrg_Click">
                                    </asp:Button>
                                    <input align="middle" type="button" runat="server" style="width: 100px; height: 21px"
                                        id="ClosePUp" value=" Close " class="altButtonFormat" onclick="javascript:fnClearOrg();" />
                                </td>
                            </tr>
                        </table>
                    </asp:Panel>
                </td>--%>
                <%--FB 2719 Ends--%>
            </tr>
            <%--FB 1849 End--%>
        </td>
    </tr>
    <%--FB 1982--%>
    </table>
    </td>
    </tr>
    </table>

    <script language="javascript" type="text/javascript">

        //FB 3011
//        function fnFooterMsg() //FB 2681
//        {
//            if(FooterMsg.GetHtml() != null)
//                document.getElementById("hdnFooterMsg").value = FooterMsg.GetHtml();            
//        }

// FB 1710 Alignment change  Ends
//Code added fro FB 1428 - CSS Project
    function fnTransferPage()
    {
        //window.location.replace("UITextChange.aspx"); //FB 2565
        window.location = 'UITextChange.aspx';
    }
//FB 1830 - DeleteEmailLang start
  function fnDelEmailLan()
  {
    if (document.getElementById("txtEmailLang").value == "")
        return false;
    else
        return true;
  }
//FB 1830 - DeleteEmailLang end
//FB 1849
    function fnClearOrg()
    {
        var obj1 = document.getElementById('DrpOrganization');
        if(obj1)
        {
            obj1.value = '<%=orgId%>';
        }        
    }
    
    </script>

    </form>
</body>
</html>
<%--code added for Soft Edge button--%>

<script type="text/javascript" src="inc/softedge.js"></script>

<!-- #INCLUDE FILE="inc/mainbottomNET.aspx" -->
<!-- FB 2050 Start -->

<script type="text/javascript">

function scrollWindow()
{
//    document.getElementById("Button1").focus();
    //    refreshStyle();
    //FB 2738 Starts
    document.getElementById("topDiv1").style.display = 'none';
    scrollTo(0, 0);
    //FB 2738 Ends
    
    
}

function refreshImage()
{
    setTimeout("scrollWindow()",500); // FB 2738
   
  var obj = document.getElementById("mainTop");
  if(obj != null)
  {
      var src = obj.src;
      var pos = src.indexOf('?');
      if (pos >= 0) {
         src = src.substr(0, pos);
      }
      var date = new Date();
      obj.src = src + '?v=' + date.getTime();
      
      if(obj.width > 804)
      obj.setAttribute('width','804');
  }
  //refreshStyle();
  setMarqueeWidth();
  return false;
}

function refreshStyle()
{
	var i,a,s;
	a=document.getElementsByTagName('link');
	for(i=0;i<a.length;i++) {
		s=a[i];
		if(s.rel.toLowerCase().indexOf('stylesheet')>=0&&s.href) {
			var h=s.href.replace(/(&|\\?)forceReload=d /,'');
			s.href=h+(h.indexOf('?')>=0?'&':'?')+'forceReload='+(new Date().valueOf());
		}
	}
}

function setMarqueeWidth()
{
    var screenWidth = screen.width - 25;
    if(document.getElementById('martickerDiv')!=null)
        document.getElementById('martickerDiv').style.width = screenWidth + 'px';
        
    if(document.getElementById('marticDiv')!=null)
        document.getElementById('marticDiv').style.width = screenWidth + 'px';
    
    if(document.getElementById('marticker2Div')!=null)
        document.getElementById('marticker2Div').style.width = (screenWidth-15) + 'px';
    
    if(document.getElementById('martic2Div')!=null)
        document.getElementById('martic2Div').style.width = (screenWidth-15) + 'px';
}

window.onload = refreshImage;


</script>

<!-- FB 2050 End -->
