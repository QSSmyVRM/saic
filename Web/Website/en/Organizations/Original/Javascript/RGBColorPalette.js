/*ZD 100147 Start*/
/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
/*ZD 100147 End*/

// JScript source code

var ggWinCal
var gReturnItem;
var CSSPath='<%=Session["OrgCSSPath"]%>';

function show_RGBPalette()
{
	var args = show_RGBPalette.arguments;

	var w = 630, h = 440; // default sizes
	if (window.screen) {
		w = window.screen.availWidth * 40 / 100;
		h = window.screen.availHeight * 44 / 100;
	}
	//alert("show_RGBPalette");
	//var vWinCal = window.open("","",'resizable=yes,scrollbars=no,toolbar=no, width='+w+',height='+h+',top='+args[1]+',left='+args[2]);
	var vWinCal = window.open("","",'resizable=yes,scrollbars=no,toolbar=no, width='+w+',height='+h);
/*
	var pal_top =args[1] + 60;
	
    var pal_left = args[2] + 10;
    
    var pal_width = 350;
    
    var pal_height = 200;
	
	var vWinCal = window.open("", "", 
		'toolbar=no,location=no,directories=no,titlebar=no,status=no,menubar=no,scrollbars=no,resizable=0,dependent=no,width='+pal_width+',height='+pal_height+',top='+pal_top+',left='+pal_left);
*/		
	vWinCal.opener = self;
	
	
	this.gReturnItem = "document.getElementById('"+ args[0]+ "')";
	
	vWinCal.document.open();
	vWinCal.document.write(fnCreateWebPalette());
	vWinCal.document.close();	
	
}

function fnCreateHTMLHead()
{
	
	// Organization CSS Module

    CSSPath='Organizations/Original/Styles/main.css';
       
	var htmlString = '<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">';
	htmlString += '<html><head><title>Web Color Palette</title><script language="javascript">';
	
	htmlString += 'function fnOnWebPaletteClick(e) ';
	//htmlString += '{ var gReturnItem = "'+ gReturnItem + '"; var e = window.event.srcElement; ';
	htmlString += '{ var targ; if (!e) var e = window.event; ';
	htmlString += ' if (e.target) targ = e.target; ';
	htmlString += ' else if (e.srcElement) targ = e.srcElement;';
	htmlString += ' if (targ.nodeType == 3) targ = targ.parentNode;';
	htmlString += ' var selectedcolor = document.getElementById("selcolor");'; 
	htmlString += ' var selectedcolorcode = document.getElementById("selcolorcode");'; 
	htmlString += ' var table = document.getElementById("webPalette");';
	htmlString += ' if(targ.tagName == "TD") ';
	htmlString += ' { for(i=0;i<table.rows.length;i++) ' ;
	htmlString += ' { for(j=0;j<table.rows[i].cells.length;j++) ' ;
	htmlString += ' { table.rows[i].cells[j].style.borderColor = "white" }';
	htmlString += ' } ';
	htmlString += ' targ.style.borderColor = "black"; ';
	htmlString += ' selectedcolor.style.backgroundColor = targ.title;';
	htmlString += ' selectedcolorcode.value = targ.title;'; 
	htmlString += ' } }';
	htmlString += 'function fnClose() {window.close();}';
	htmlString += 'function fnConfirm()';
	htmlString += ' {';
	htmlString += ' var selectedcolorcode = document.getElementById("selcolorcode");'; 
	htmlString += ' if((selectedcolorcode.value == "") || (selectedcolorcode.value ==undefined))';
	htmlString += ' {alert("Please choose a color.");} ';
	htmlString += ' else{ ';
	htmlString += ' self.opener.'+ gReturnItem +'.value=selectedcolorcode.value; ';
	htmlString += ' window.close();';
	htmlString += ' }';
	htmlString += ' }';
	htmlString += '</script>';
	// Organization CSS Module
	var sessionThemeType = window.parent.document.getElementById("sessionThemeType").value; // FB 2815
	htmlString += '<LINK title="Expedite base styles" href="Organizations/Original/Styles/main' + sessionThemeType + '.css" type="text/css" rel="stylesheet">'; //FB 2815
	htmlString += '</head>';

	return htmlString;
}

function fnCreateWebPalette()
{

	var htmlString = fnCreateHTMLHead();

	htmlString += '<body bottomMargin="0" leftMargin="0" background="" topMargin="0" rightMargin="0" ><center><table bgcolor=#1556B0 width=100% border=0><tr><td style=color:white;><b>Web Color Palette</b></td></tr></table><div id=pal style="overflow-y:auto; height:300px;">';
	htmlString += '<table width="100%" cellpadding="0" cellspacing="0"><tr><td width="60%" align=left>' ;
	htmlString += '<table style="left:5px;border-collapse:collapse;" cellspacing=1 height=300px bgcolor=scrollbar id=webPalette width=100%>';
	
	var colors = new Array('00', '33', '66', '99', 'cc', 'ff')
	var clr1 = 'ff'
	var clr2 = 'ff'
	var clr3 = 'ff'
	
	//table rows
	for(var i=0;i<18;i++)
	{
		htmlString += '<tr>'
		
		//R values
		if(i>=0 && i<=5)	clr1 = colors[5-i]
		if(i>=6 && i<=11)	clr1 = colors[i-6]
		if(i>=12 && i<=17)	clr1 = colors[17-i]
		
		//table cols
		for(var j=0;j<12;j++)
		{
			//G values
			if(j>=0 && j<=5)	clr2 = colors[j]
			if(j>=6 && j<=11)	clr2 = colors[5-(j-6)]
			
			//B values
			if(i>=0 && i<=5 && j>=0 && j<=5)	clr3 = "ff"
			if(i>=0 && i<=5 && j>=6 && j<=11)	clr3 = "66"
			if(i>=6 && i<=11 && j>=0 && j<=5)	clr3 = "cc"
			if(i>=6 && i<=11 && j>=6 && j<=11)	clr3 = "33"
			if(i>=12 && i<=17 && j>=0 && j<=5)	clr3 = "99"
			if(i>=12 && i<=17 && j>=6 && j<=11)	clr3 = "00"
			
			var color = "#" + clr1 + clr2 + clr3
			
			htmlString += '<td onclick="fnOnWebPaletteClick(event)" title=' + color + ' class="webitem" style="background-color:'+ color + '" ></td>'
		}
		htmlString += '</tr>'
	}

	htmlString += '</table></td>';
	htmlString += '<td width="40%" align="right">';
	//htmlString += '<center>';
	htmlString += '<table width="100%" cellpadding="3" cellspacing="0" border="0">';
	htmlString += '<tr>';
	htmlString += '<td align="left" style="FONT-SIZE: 8pt; COLOR: black; FONT-FAMILY: Verdana">';
	htmlString += '<b>Selected Color</b> &nbsp;';
	htmlString += '</td>';
	htmlString += '</tr>';
	htmlString += '<tr>';
	htmlString += '<td align="left"><input type="text" id="selcolor" style="width:90px;border-style:ridge;" readonly></td><br>';
	htmlString += '</tr>';
	htmlString += '<tr>';
	htmlString += '<td align="left" style="FONT-SIZE: 8pt; COLOR: black; FONT-FAMILY: Verdana"><b>Color Code</b> &nbsp;</td>';
	htmlString += '</tr>';
	htmlString += '<tr>';
	htmlString += '<td align="left"><input type="text" id="selcolorcode" style="FONT-SIZE: 10pt; COLOR: black; FONT-FAMILY: Verdana;border-style:ridge;width:90px"></td>'; 
	htmlString += '</tr>';
	htmlString += '<tr>';
	htmlString += '<td align="center" nowrap><br/>';
	htmlString += '<input type="button" name="btnok" value="Ok" onclick="fnConfirm();" class="altShort2BlueButtonFormat" > &nbsp; &nbsp; &nbsp;';
	htmlString += '<input type="button" name="btnclose" value="Close" onclick="fnClose();" class="altShort2BlueButtonFormat">';
	htmlString += '</td>';
	htmlString += '</tr>';
	htmlString += '</table>';
	htmlString += '</td></tr></table></div></center></body></html>';
	return htmlString;
	
}							
			
