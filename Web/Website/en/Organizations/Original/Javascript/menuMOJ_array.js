/*ZD 100147 Start*/
/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
/*ZD 100147 End*/
menunum=0;
menus=new Array();
_d=document; 



function addmenu()
{
	menunum++;
	menus[menunum]=menu;
}

function dumpmenus()
{
	mt = "<script language=javascript>";
	for (a=1; a<menus.length; a++) {
		mt += " menu" + a + "=menus[" + a + "];"
	}
	mt += "<\/script>";
	_d.write(mt)
}


if((navigator.appVersion.indexOf("MSIE 7.0")>0) || (navigator.appVersion.indexOf("MSIE 6.0")>0) || (navigator.appVersion.indexOf("MSIE 8.0")>0))
{
	effect = "Fade(duration=0.2); Alpha(style=0,opacity=88); Shadow(color='#777777', Direction=135, Strength=5)"
}
else
{
	effect = "Shadow(color='#777777', Direction=135, Strength=5)" 
}

//effect = ""			// 2785.2

timegap=500;		followspeed=5;		followrate=40;
suboffset_top=10;	suboffset_left=10;	

style1=[
"white",			// Mouse Off Font 
"Purple",			// Mouse Off Background 
"Purple",			// Mouse On Font 
"#FFF7CE",			// Mouse On Background 
"lightblue",		// Menu Border 
12,					// Font Size in pixels
"normal",			// Font Style 
"bold",				// Font Weight
"Verdana, Arial",	// Font Name
4,					// Menu Item Padding
,					// Sub Menu Image 
,					// Border & Separator bar
"66ffff",			// High 
"000099",			// Low 
"Purple",			// Current Page Item Font Color
"pink",				// Current Page Item Background Color
,					// Top Bar image 
"ffffff",			// Menu Header Font Color 
"000099",			// Menu Header Background Color
]

style1_2={0}

style2={1}

if((navigator.appVersion.indexOf("MSIE 7.0")>0) || (navigator.appVersion.indexOf("MSIE 6.0")>0) || (navigator.appVersion.indexOf("MSIE 8.0")>0))
{
    menu_0 = ["mainmenu",72,3,117,,,style1,1,"center",effect,,1,,,,,,,,,,]			// 24
    menu_0_2 = ["mainmenu",72,3,117,,,style1_2,1,"center",effect,,1,,,,,,,,,,]		// 24
    menu_1 = ["Lobby&nbsp;","SettingSelect2.aspx",,"Go to the lobby page",0]
    menu_2 = ["Calendar&nbsp;","dispatcher/admindispatcher.asp?cmd=ManageConfRoom&f=v&hf=",,"Go to Room Calendar",0]
    //menu_3 = ["List&nbsp;","preaccount.asp?t=1",,"Ongoing Conferences",0]
    menu_3 = ["List&nbsp;","aspToaspNet.asp?tp=ConferenceList.aspx&t=2",,"Ongoing Conferences",0]
    //menu_4 = ["Search&nbsp;","dispatcher/gendispatcher.asp?cmd=GetSearchConference",,"Search Conference",0]
    menu_4 = ["Search&nbsp;","aspToaspNet.asp?tp=SearchConferenceInputParameters.aspx",,"Search Conference",0]
    //menu_5 = ["New Conference","aspToaspNet.asp?tp=ConferenceSetup.aspx&t=n",,"Create a new conference",0]
    menu_5 = ["New Conference","show-menu=conference",,"Create a new conference",0]
    menu_6 = ["Tools","show-menu=manager",,"Administrative Tools",0]

    menu = new Array();
    menu = (if_str=="2") ? menu.concat(menu_0_2) : menu.concat(menu_0);

    for (i=1; i<=mmm_num; i++) {
	    menu = ( mmm_int & (1 << (mmm_num-i)) ) ? ( menu.concat(eval("menu_" + i)) ) : menu;
    }
    addmenu();
	submenu1_0   = ["conference",96,,180,1,,style2,,"left",effect,,,,,,,,,,,,]
	submenu1_0_2 = ["conference",96,,180,1,,style2_2,,"left",effect,,,,,,,,,,,,]
    submenu1_1 = ["Future","ConferenceSetup.aspx?t=n&op=1",,"Create a Future Conference",0]
	submenu1_2 = ["Immediate","ConferenceSetup.aspx?t=n&op=2",,"Create an Immediate Conference",0]
	submenu1_3 = ["From Template","ManageTemplate.aspx",,"Create a conference from Template",0]

    menu = new Array();
    //menu = menu.concat( eval("submenu1_0"));
    menu = menu.concat( eval("submenu1_0_2"));
    for (i=1; i<=3; i++) 
        menu = menu.concat(eval("submenu1_" + i));

    addmenu();
	
	submenu2_0   = ["manager",96,,180,1,,style2,,"left",effect,,,,,,,,,,,,]
	submenu2_0_2 = ["manager",96,,180,1,,style2_2,,"left",effect,,,,,,,,,,,,]
    submenu2_1 = []
	//submenu2_2 = ["My Templates","dispatcher/conferencedispatcher.asp?cmd=GetTemplateList&frm=manage&sb=1",,"manage template for setting conference",0]
	submenu2_2 = ["My Templates","ManageTemplate.aspx",,"Manage Conference Templates",0]
    submenu2_3 = ["My Preferences","ManageUserProfile.aspx",,"Edit preferences",0]
	submenu2_4 = ["My Reports","asptoaspnet.asp?tp=ConferenceReportList.aspx&t=",,"My Reports",0]
	submenu2_5  = ["<hr>Hardware","show-menu=mcu",,"Manage Hardware",0]
	submenu2_5_ = ["Hardware","",,,0]
	submenu2_6  = ["Locations","show-menu=loc",,"Manage locations",0]
	submenu2_6_ = ["Locations","",,,0]
	submenu2_7  = []
	submenu2_7_ = ["Reports","",,,0]
	
    submenu2_8  = ["Site Options","aspToaspnet.asp?tp=mainadministrator.aspx",,"Edit System Settings",0]
	submenu2_9  = ["Site Settings","aspToaspnet.asp?tp=SuperAdministrator.aspx",,"Advanced system settings",0]
	submenu2_10  = ["Users<hr>","show-menu=usr",,"Manage users",0]
	submenu2_10_ = ["Users","",,,0]
	//submenu3_15 = ["<hr>","",,"",0]
  	submenu2_11 = ["Audio/Visual","show-menu=av",,,0]
    submenu2_11_ = ["Audio/Visual","",,,0]
	submenu2_12  = ["Caterer","show-menu=catr",,,0]
	submenu2_12_ = ["Caterer","",,,0]
	submenu2_13  = ["Housekeeping","show-menu=hk",,,0]
	submenu2_13_ = ["Housekeeping","",,,0]
	    cur_munu_no = 1;	cur_munu_str = "2_";
	    if ( mms_int[cur_munu_no] ) {
		    menu = new Array();
		    menu = menu.concat( eval("submenu" + cur_munu_str + "0" + ((if_str=="2") ? "_2" : "")) );
		    for (i=1; i<=mms_num[cur_munu_no]-3; i++) {
			    menu = ( mms_int[cur_munu_no] & (1 << (mms_num[cur_munu_no]-i)) ) ? ( menu.concat(eval("submenu" + cur_munu_str + i)) ) : menu;
		    }
		    for (i=mms_num[cur_munu_no]-2; i<=mms_num[cur_munu_no]; i++) {
			    if (rf_int & 1)
			        menu = ( mms_int[cur_munu_no] & (1 << (mms_num[cur_munu_no]-i)) ) ? ( menu.concat(eval("submenu" + cur_munu_str + i)) ) : menu;
	    		rf_int = rf_int/2;
		    }
		    addmenu();
	    }

		submenu2_10_0 = ["usr",,,200,1,"",style2,,"left",effect,,,,,,,,,,,,]
		submenu2_10_0_2 = ["usr",,,200,1,"",style2_2,,"left",effect,,,,,,,,,,,,]
//		if (al_int=="2")
			submenu2_10_1 = ["Active Users","ManageUser.aspx?t=1",,"Edit, search or create a new user",0]
//		else
//			submenu2_10_1 = ["Edit User","ManageUser.aspx?t=1",,"Edit, search or create a new user",0]
		submenu2_10_2 = ["Bulk Tool","dispatcher/admindispatcher.asp?cmd=GetAllocation",,"Manage Bulk User",0]
		submenu2_10_3 = ["Departments","aspToaspNet.asp?tp=ManageDepartment.aspx",,"Manage Department",0]
		submenu2_10_4 = ["Guests","ManageUser.aspx?t=2",,"Edit, search or create a new guest",0]
		if (sso_int)
			submenu2_10_5 = ["Restore User","ManageUser.aspx?t=3",,"Delete or Replaced a removed user",0]
		else
			submenu2_10_5 = ["Inactive Users","ManageUser.aspx?t=3",,"Delete or Replaced a removed user",0]
		submenu2_10_6 = ["LDAP Directory Import","aspToaspNet.asp?tp=LDAPImport.aspx",,"Setup Multiple Users",0]
		submenu2_10_7 = ["Roles","dispatcher/admindispatcher.asp?cmd=GetUserRoles",,"Edit, search or create user roles",0]
		submenu2_10_8 = ["Templates","aspToaspNet.asp?tp=ManageUserTemplatesList.aspx",,"Edit, search or create user templates",0]

		cur_munu_no = 5;	cur_munu_str = "2_10_";
		if ( mms_int[cur_munu_no] ) {
			menu = new Array();
			menu = menu.concat( eval("submenu" + cur_munu_str + "0" + ((if_str=="2") ? "_2" : "")) );
			for (i=1; i<=mms_num[cur_munu_no]; i++) {
				menu = ( mms_int[cur_munu_no] & (1 << (mms_num[cur_munu_no]-i)) ) ? ( menu.concat(eval("submenu" + cur_munu_str + i)) ) : menu;
			}
			addmenu();
		}


		submenu2_6_0 = ["loc",,,210,1,"",style2,,"left",effect,,,,,,,,,,,,]
		submenu2_6_0_2 = ["loc",,,210,1,"",style2_2,,"left",effect,,,,,,,,,,,,]
		submenu2_6_1 = ["Rooms","dispatcher/admindispatcher.asp?cmd=ManageConfRoom",,"Edit or set up conference room",0]
		submenu2_6_2 = ["Tiers","aspToaspNet.asp?tp=ManageTiers.aspx",,"Manage Tiers",0]

		cur_munu_no = 3;	cur_munu_str = "2_6_";
		if ( mms_int[cur_munu_no] ) {
			menu = new Array();
			menu = menu.concat( eval("submenu" + cur_munu_str + "0" + ((if_str=="2") ? "_2" : "")) );
			for (i=1; i<=mms_num[cur_munu_no]; i++) {
				menu = ( mms_int[cur_munu_no] & (1 << (mms_num[cur_munu_no]-i)) ) ? ( menu.concat(eval("submenu" + cur_munu_str + i)) ) : menu;
			}
			addmenu();
		}


		submenu2_5_0 = ["mcu",,,210,1,"",style2,,"left",effect,,,,,,,,,,,,]
		submenu2_5_0_2 = ["mcu",,,210,1,"",style2_2,,"left",effect,,,,,,,,,,,,]
		submenu2_5_1 = ["Endpoints","aspToaspnet.asp?tp=EndpointList.aspx",,"Setup Bridge",0]
		submenu2_5_2 = ["Event Logs","aspToaspnet.asp?tp=hardwareissuelog.aspx",,"View hardware problem log",0]
		submenu2_5_3 = ["MCUs","aspToaspnet.asp?tp=ManageBridge.aspx",,"Edit Bridge Management",0]

		cur_munu_no = 2;	cur_munu_str = "2_5_";
		if ( mms_int[cur_munu_no] ) {
			menu = new Array();
			menu = menu.concat( eval("submenu" + cur_munu_str + "0" + ((if_str=="2") ? "_2" : "")) );
			for (i=1; i<=mms_num[cur_munu_no]; i++) {
				menu = ( mms_int[cur_munu_no] & (1 << (mms_num[cur_munu_no]-i)) ) ? ( menu.concat(eval("submenu" + cur_munu_str + i)) ) : menu;
			}
			addmenu();
		}


		submenu2_7_0 = ["rpt",,,220,1,"",style2,,"left",effect,,,,,,,,,,,,]
		submenu2_7_0_2 = ["rpt",,,220,1,"",style2_2,,"left",effect,,,,,,,,,,,,]
		submenu2_7_1 = ["Conference Reports","asptoaspnet.asp?tp=ConferenceReportList.aspx&t=",,"Show conference report",0]
		submenu2_7_2 = ["Room Reports","dispatcher/admindispatcher.asp?cmd=RoomReport",,"Show room report",0]
		submenu2_7_3 = ["User Reports","dispatcher/admindispatcher.asp?cmd=UserReport",,"Show user report",0]

		cur_munu_no = 4;	cur_munu_str = "2_7_";
		if ( mms_int[cur_munu_no] ) {
			menu = new Array();
			menu = menu.concat( eval("submenu" + cur_munu_str + "0" + ((if_str=="2") ? "_2" : "")) );
			for (i=1; i<=mms_num[cur_munu_no]; i++) {
				menu = ( mms_int[cur_munu_no] & (1 << (mms_num[cur_munu_no]-i)) ) ? ( menu.concat(eval("submenu" + cur_munu_str + i)) ) : menu;
			}
			addmenu();
		}
		
	
		submenu2_11_0 = ["av",,,220,1,"",style2,,"left",effect,,,,,,,,,,,,]
		submenu2_11_0_2 = ["av",,,220,1,"",style2_2,,"left",effect,,,,,,,,,,,,]
		submenu2_11_1 = ["Inventory Sets","asptoaspnet.asp?tp=InventoryManagement.aspx&t=1",,"",0]
		submenu2_11_2 = ["Work Orders","asptoaspnet.asp?tp=ConferenceOrders.aspx&t=1",,"",0]

		cur_munu_no = 6;	cur_munu_str = "2_11_";
		if ( mms_int[cur_munu_no] ) {
			menu = new Array();
			menu = menu.concat( eval("submenu" + cur_munu_str + "0" + ((if_str=="2") ? "_2" : "")) );
			for (i=1; i<=mms_num[cur_munu_no]; i++) {
				menu = ( mms_int[cur_munu_no] & (1 << (mms_num[cur_munu_no]-i)) ) ? ( menu.concat(eval("submenu" + cur_munu_str + i)) ) : menu;
			}
			addmenu();
		}
		
		submenu2_12_0 = ["catr",,,220,1,"",style2,,"left",effect,,,,,,,,,,,,]
		submenu2_12_0_2 = ["catr",,,220,1,"",style2_2,,"left",effect,,,,,,,,,,,,]
		submenu2_12_1 = ["Catering Provider","asptoaspnet.asp?tp=InventoryManagement.aspx&t=2",,"",0]
        submenu2_12_2 = ["Work Orders","asptoaspnet.asp?tp=ConferenceOrders.aspx&t=2",,"",0]
		
		cur_munu_no = 7;	cur_munu_str = "2_12_";
		if ( mms_int[cur_munu_no] ) {
			menu = new Array();
			menu = menu.concat( eval("submenu" + cur_munu_str + "0" + ((if_str=="2") ? "_2" : "")) );
			for (i=1; i<=mms_num[cur_munu_no]; i++) {
				menu = ( mms_int[cur_munu_no] & (1 << (mms_num[cur_munu_no]-i)) ) ? ( menu.concat(eval("submenu" + cur_munu_str + i)) ) : menu;
			}
			addmenu();
		}

		submenu2_13_0 = ["hk",,,220,1,"",style2,,"left",effect,,,,,,,,,,,,]
		submenu2_13_0_2 = ["hk",,,220,1,"",style2_2,,"left",effect,,,,,,,,,,,,]
		submenu2_13_1 = ["Housekeeping Groups","asptoaspnet.asp?tp=InventoryManagement.aspx&t=3",,"",0]
		submenu2_13_2 = ["Work Orders","asptoaspnet.asp?tp=ConferenceOrders.aspx&t=3",,"",0]

		cur_munu_no = 8;	cur_munu_str = "2_13_";
		if ( mms_int[cur_munu_no] ) {
			menu = new Array();
			menu = menu.concat( eval("submenu" + cur_munu_str + "0" + ((if_str=="2") ? "_2" : "")) );
			for (i=1; i<=mms_num[cur_munu_no]; i++) {
				menu = ( mms_int[cur_munu_no] & (1 << (mms_num[cur_munu_no]-i)) ) ? ( menu.concat(eval("submenu" + cur_munu_str + i)) ) : menu;
			}
			addmenu();
		}
}
else
{
    menu_0 = ["mainmenu",72,3,117,,,style1,1,"center",effect,,1,,,,,,,,,,,]			// 24
    menu_0_2 = ["mainmenu",72,3,117,,,style1_2,1,"center",effect,,1,,,,,,,,,,,]		// 24
    menu_1 = ["Lobby&nbsp;","SettingSelect2.aspx",,"Go to the lobby page",0]
    menu_2 = ["Calendar&nbsp;","dispatcher/admindispatcher.asp?cmd=ManageConfRoom&f=v&hf=",,"Go to Room Calendar",0]
    menu_3 = ["List&nbsp;","aspToaspNet.asp?tp=ConferenceList.aspx&t=2",,"Ongoing Conferences",0]
    menu_4 = ["Search&nbsp;","aspToaspNet.asp?tp=SearchConferenceInputParameters.aspx",,"Search Conference",0]
    //menu_5 = ["New Conference","aspToaspNet.asp?tp=ConferenceSetup.aspx&t=n",,"Create a new conference",0]
    menu_5 = ["New Conference","show-menu=conference",,"Create a new conference",0]
    menu_6 = ["Tools","show-menu=manager",,"Administrative Tools",0]

    menu = new Array();
    menu = (if_str=="2") ? menu.concat(menu_0_2) : menu.concat(menu_0);

    for (i=1; i<=mmm_num; i++) {
	    menu = ( mmm_int & (1 << (mmm_num-i)) ) ? ( menu.concat(eval("menu_" + i)) ) : menu;
    }
    addmenu();
	
	submenu1_0   = ["conference",96,,180,1,,style2,,"left",effect,,,,,,,,,,,,,]
	submenu1_0_2 = ["conference",96,,180,1,,style2_2,,"left",effect,,,,,,,,,,,,,]
    submenu1_1 = ["Future","ConferenceSetup.aspx?t=n&op=1",,"Create a Future Conference",0]
	submenu1_2 = ["Immediate","ConferenceSetup.aspx?t=n&op=2",,"Create an Immediate Conference",0]
	submenu1_3 = ["From Template","ManageTemplate.aspx",,"Create a conference from Template",0]

    menu = new Array();
    //menu = menu.concat( eval("submenu1_0"));
    menu = menu.concat( eval("submenu1_0_2"));
    for (i=1; i<=3; i++) 
        menu = menu.concat(eval("submenu1_" + i));

    addmenu();
	submenu2_0   = ["manager",96,,180,1,,style2,,"left",effect,,,,,,,,,,,,,]
	submenu2_0_2 = ["manager",96,,180,1,,style2_2,,"left",effect,,,,,,,,,,,,,]
    submenu2_1 = []
	//submenu2_2 = ["My Templates","dispatcher/conferencedispatcher.asp?cmd=GetTemplateList&frm=manage&sb=1",,"manage template for setting conference",0]
	submenu2_2 = ["My Templates","ManageTemplate.aspx",,"Manage Conference Templates",0]
	submenu2_3 = ["My Preferences","ManageUserProfile.aspx",,"Edit preferences",0]
	submenu2_4 = ["My Reports","asptoaspnet.asp?tp=ConferenceReportList.aspx&t=",,"My Reports",0]
	submenu2_5  = ["<hr>Hardware","show-menu=mcu",,"Manage Hardware",0]
	submenu2_5_ = ["Hardware","",,,0]
	submenu2_6  = ["Locations","show-menu=loc",,"Manage locations",0]
	submenu2_6_ = ["Locations","",,,0]
	submenu2_7  = []
	submenu2_7_ = ["Reports","",,,0]
	
    submenu2_8  = ["Site Options","aspToaspnet.asp?tp=mainadministrator.aspx",,"Edit System Settings",0]
	submenu2_9  = ["Site Settings","aspToaspnet.asp?tp=SuperAdministrator.aspx",,"Advanced system settings",0]
	submenu2_10  = ["Users<hr>","show-menu=usr",,"Manage users",0]
	submenu2_10_ = ["Users","",,,0]
	//submenu3_15 = ["<hr>","",,"",0]
  	submenu2_11 = ["Audio/Visual","show-menu=av",,,0]
    submenu2_11_ = ["Audio/Visual","",,,0]
	submenu2_12  = ["Caterer","show-menu=catr",,,0]
	submenu2_12_ = ["Caterer","",,,0]
	submenu2_13  = ["Housekeeping","show-menu=hk",,,0]
	submenu2_13_ = ["Housekeeping","",,,0]
	    cur_munu_no = 1;	cur_munu_str = "2_";
	    if ( mms_int[cur_munu_no] ) {
		    menu = new Array();
		    menu = menu.concat( eval("submenu" + cur_munu_str + "0" + ((if_str=="2") ? "_2" : "")) );
		    for (i=1; i<=mms_num[cur_munu_no]-3; i++) {
			    menu = ( mms_int[cur_munu_no] & (1 << (mms_num[cur_munu_no]-i)) ) ? ( menu.concat(eval("submenu" + cur_munu_str + i)) ) : menu;
		    }
		    for (i=mms_num[cur_munu_no]-2; i<=mms_num[cur_munu_no]; i++) {
			    if (rf_int & 1)
			        menu = ( mms_int[cur_munu_no] & (1 << (mms_num[cur_munu_no]-i)) ) ? ( menu.concat(eval("submenu" + cur_munu_str + i)) ) : menu;
	    		rf_int = rf_int/2;
		    }
		    addmenu();
	    }

		submenu2_10_0 = ["usr",,,200,1,"",style2,,"left",effect,,,,,,,,,,,,,]
		submenu2_10_0_2 = ["usr",,,200,1,"",style2_2,,"left",effect,,,,,,,,,,,,,]
//		if (al_int=="2")
			submenu2_10_1 = ["Active Users","ManageUser.aspx?t=1",,"Edit, search or create a new user",0]
//		else
//			submenu2_10_1 = ["Edit User","ManageUser.aspx?t=1",,"Edit, search or create a new user",0]
		submenu2_10_2 = ["Bulk Tool","dispatcher/admindispatcher.asp?cmd=GetAllocation",,"Manage Bulk User",0]
		submenu2_10_3 = ["Departments","aspToaspNet.asp?tp=ManageDepartment.aspx",,"Manage Department",0]
		submenu2_10_4 = ["Guests","ManageUser.aspx?t=2",,"Edit, search or create a new guest",0]
		if (sso_int)
			submenu2_10_5 = ["Restore User","ManageUser.aspx?t=3",,"Delete or Replaced a removed user",0]
		else
			submenu2_10_5 = ["Inactive Users","ManageUser.aspx?t=3",,"Delete or Replaced a removed user",0]
		submenu2_10_6 = ["LDAP Directory Import","aspToaspNet.asp?tp=LDAPImport.aspx",,"Setup Multiple Users",0]
		submenu2_10_7 = ["Roles","dispatcher/admindispatcher.asp?cmd=GetUserRoles",,"Edit, search or create user roles",0]
		submenu2_10_8 = ["Templates","aspToaspNet.asp?tp=ManageUserTemplatesList.aspx",,"Edit, search or create user templates",0]

		cur_munu_no = 5;	cur_munu_str = "2_10_";
		if ( mms_int[cur_munu_no] ) {
			menu = new Array();
			menu = menu.concat( eval("submenu" + cur_munu_str + "0" + ((if_str=="2") ? "_2" : "")) );
			for (i=1; i<=mms_num[cur_munu_no]; i++) {
				menu = ( mms_int[cur_munu_no] & (1 << (mms_num[cur_munu_no]-i)) ) ? ( menu.concat(eval("submenu" + cur_munu_str + i)) ) : menu;
			}
			addmenu();
		}


		submenu2_6_0 = ["loc",,,210,1,"",style2,,"left",effect,,,,,,,,,,,,,]
		submenu2_6_0_2 = ["loc",,,210,1,"",style2_2,,"left",effect,,,,,,,,,,,,,]
		submenu2_6_1 = ["Rooms","dispatcher/admindispatcher.asp?cmd=ManageConfRoom",,"Edit or set up conference room",0]
		submenu2_6_2 = ["Tiers","aspToaspNet.asp?tp=ManageTiers.aspx",,"Manage room location",0]

		cur_munu_no = 3;	cur_munu_str = "2_6_";
		if ( mms_int[cur_munu_no] ) {
			menu = new Array();
			menu = menu.concat( eval("submenu" + cur_munu_str + "0" + ((if_str=="2") ? "_2" : "")) );
			for (i=1; i<=mms_num[cur_munu_no]; i++) {
				menu = ( mms_int[cur_munu_no] & (1 << (mms_num[cur_munu_no]-i)) ) ? ( menu.concat(eval("submenu" + cur_munu_str + i)) ) : menu;
			}
			addmenu();
		}


		submenu2_5_0 = ["mcu",,,210,1,"",style2,,"left",effect,,,,,,,,,,,,,]
		submenu2_5_0_2 = ["mcu",,,210,1,"",style2_2,,"left",effect,,,,,,,,,,,,,]
		submenu2_5_1 = ["Endpoints","aspToaspnet.asp?tp=EndpointList.aspx",,"Setup Bridge",0]
		submenu2_5_2 = ["Event Logs","aspToaspnet.asp?tp=hardwareissuelog.aspx",,"View hardware problem log",0]
		submenu2_5_3 = ["MCUs","aspToaspnet.asp?tp=ManageBridge.aspx",,"Edit Bridge Management",0]

		cur_munu_no = 2;	cur_munu_str = "2_5_";
		if ( mms_int[cur_munu_no] ) {
			menu = new Array();
			menu = menu.concat( eval("submenu" + cur_munu_str + "0" + ((if_str=="2") ? "_2" : "")) );
			for (i=1; i<=mms_num[cur_munu_no]; i++) {
				menu = ( mms_int[cur_munu_no] & (1 << (mms_num[cur_munu_no]-i)) ) ? ( menu.concat(eval("submenu" + cur_munu_str + i)) ) : menu;
			}
			addmenu();
		}


		submenu2_7_0 = ["rpt",,,220,1,"",style2,,"left",effect,,,,,,,,,,,,,]
		submenu2_7_0_2 = ["rpt",,,220,1,"",style2_2,,"left",effect,,,,,,,,,,,,,]
		submenu2_7_1 = ["Conference Reports","asptoaspnet.asp?tp=ConferenceReportList.aspx&t=",,"Show conference report",0]
		submenu2_7_2 = ["Room Reports","dispatcher/admindispatcher.asp?cmd=RoomReport",,"Show room report",0]
		submenu2_7_3 = ["User Reports","dispatcher/admindispatcher.asp?cmd=UserReport",,"Show user report",0]

		cur_munu_no = 4;	cur_munu_str = "2_7_";
		if ( mms_int[cur_munu_no] ) {
			menu = new Array();
			menu = menu.concat( eval("submenu" + cur_munu_str + "0" + ((if_str=="2") ? "_2" : "")) );
			for (i=1; i<=mms_num[cur_munu_no]; i++) {
				menu = ( mms_int[cur_munu_no] & (1 << (mms_num[cur_munu_no]-i)) ) ? ( menu.concat(eval("submenu" + cur_munu_str + i)) ) : menu;
			}
			addmenu();
		}
		
	
		submenu2_11_0 = ["av",,,220,1,"",style2,,"left",effect,,,,,,,,,,,,,]
		submenu2_11_0_2 = ["av",,,220,1,"",style2_2,,"left",effect,,,,,,,,,,,,,]
		submenu2_11_1 = ["Inventory Sets","asptoaspnet.asp?tp=InventoryManagement.aspx&t=1",,"",0]
		submenu2_11_2 = ["Work Orders","asptoaspnet.asp?tp=ConferenceOrders.aspx&t=1",,"",0]

		cur_munu_no = 6;	cur_munu_str = "2_11_";
		if ( mms_int[cur_munu_no] ) {
			menu = new Array();
			menu = menu.concat( eval("submenu" + cur_munu_str + "0" + ((if_str=="2") ? "_2" : "")) );
			for (i=1; i<=mms_num[cur_munu_no]; i++) {
				menu = ( mms_int[cur_munu_no] & (1 << (mms_num[cur_munu_no]-i)) ) ? ( menu.concat(eval("submenu" + cur_munu_str + i)) ) : menu;
			}
			addmenu();
		}
		
		submenu2_12_0 = ["catr",,,220,1,"",style2,,"left",effect,,,,,,,,,,,,,]
		submenu2_12_0_2 = ["catr",,,220,1,"",style2_2,,"left",effect,,,,,,,,,,,,,]
		submenu2_12_1 = ["Catering Provider","asptoaspnet.asp?tp=InventoryManagement.aspx&t=2",,"",0]
        submenu2_12_2 = ["Work Orders","asptoaspnet.asp?tp=ConferenceOrders.aspx&t=2",,"",0]
		
		cur_munu_no = 7;	cur_munu_str = "2_12_";
		if ( mms_int[cur_munu_no] ) {
			menu = new Array();
			menu = menu.concat( eval("submenu" + cur_munu_str + "0" + ((if_str=="2") ? "_2" : "")) );
			for (i=1; i<=mms_num[cur_munu_no]; i++) {
				menu = ( mms_int[cur_munu_no] & (1 << (mms_num[cur_munu_no]-i)) ) ? ( menu.concat(eval("submenu" + cur_munu_str + i)) ) : menu;
			}
			addmenu();
		}

		submenu2_13_0 = ["hk",,,220,1,"",style2,,"left",effect,,,,,,,,,,,,,]
		submenu2_13_0_2 = ["hk",,,220,1,"",style2_2,,"left",effect,,,,,,,,,,,,,]
		submenu2_13_1 = ["Housekeeping Groups","asptoaspnet.asp?tp=InventoryManagement.aspx&t=3",,"",0]
		submenu2_13_2 = ["Work Orders","asptoaspnet.asp?tp=ConferenceOrders.aspx&t=3",,"",0]

		cur_munu_no = 8;	cur_munu_str = "2_13_";
		if ( mms_int[cur_munu_no] ) {
			menu = new Array();
			menu = menu.concat( eval("submenu" + cur_munu_str + "0" + ((if_str=="2") ? "_2" : "")) );
			for (i=1; i<=mms_num[cur_munu_no]; i++) {
				menu = ( mms_int[cur_munu_no] & (1 << (mms_num[cur_munu_no]-i)) ) ? ( menu.concat(eval("submenu" + cur_munu_str + i)) ) : menu;
			}
			addmenu();
		}
}

dumpmenus()
