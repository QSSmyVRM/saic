/*ZD 100147 Start*/
/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
/*ZD 100147 End*/
menunum = 0;
menus = new Array();
_d = document;


// FB 3055 Starts
var arr = window.location.pathname.split("aspx")[0].split("/");
var page = arr[arr.length - 1] + "aspx";
page = page.toLowerCase();
if ("emailcustomization.aspx,viewblockedmails.aspx,editblockemail.aspx,conferenceorders.aspx,calendarworkorder.aspx,inventorymanagement.aspx,editinventory.aspx,editconferenceorder.aspx".indexOf(page) > -1) {
    var ar = window.location.href.split("/");
    var pg = ar[ar.length - 1];
    page = pg.toLowerCase();
}
//prompt("",page);

var refPage = page;
var valid = false;
var valid2 = false;


var isExpressProfile = false;
if (document.getElementById('isExpressUser').value == "1" && document.getElementById('isExpressUserAdv').value == "0" && document.getElementById('isExpressManage').value == "0")
    isExpressProfile = true;

var isExpressUsrAdv = false;
if (document.getElementById('isExpressUserAdv').value == "1")
    isExpressUsrAdv = true;

var isExpressUsrManage = false;
if (document.getElementById('isExpressManage').value == "1" && document.getElementById('isExpressUserAdv').value == "0")
    isExpressUsrManage = true;

// Page names included in menu follows
var mainPgs = "expressconference.aspx";
if (mainPgs.indexOf(page) > -1)
    var mainPage = page;
var mainPgs = "conferencelist.aspx";
if (mainPgs.indexOf(page) > -1 && isExpressProfile == false)
    var mainPage = page;

// Pages based on session variable
var sessPages = "masterchildreport.aspx";
var advRep = document.getElementById('advRep').value;

var sessPages2 = "monitormcu.aspx,point2point.aspx";
var calMon = document.getElementById('callMon').value;

var userLevel = document.getElementById('userAdminLevel').value;

if (sessPages.indexOf(page) > -1 && advRep == "1")
    valid2 = true;
//if (sessPages2.indexOf(page) > -1 && calMon == "1" )// && (userLevel == "2" || userLevel == "3")) //FB 2996
  //  valid = true;

if ("conferencelist.aspx".indexOf(page) > -1) {
    var arr = window.location.href.split("/");
    var pag = arr[arr.length - 1];
    page = pag.toLowerCase();
}
refPage = page;

if (userLevel == "0")
    if (page.indexOf("conferencelist.aspx?t=7") > -1)
    window.location.assign("thankyou.aspx");

if (page.indexOf("conferencelist.aspx?t=1&frm=1") > -1)
    valid = true;
else if (page.indexOf("hdconferencelist.aspx") > -1)
    page = "hdconferencelist.aspx";
else if (page.indexOf("conferencelist.aspx") > -1)
    page = "conferencelist.aspx";


if (isExpressProfile || isExpressUsrManage) {
    if ("conferencelist.aspx".indexOf(page) > -1) {
        var arr = window.location.href.split("/");
        var pag = arr[arr.length - 1];
        page = pag.toLowerCase();
    }
    refPage = page;
    if (page.indexOf("conferencelist.aspx?t=3") > -1 || page.indexOf("conferencelist.aspx?m=1&t=3") > -1)//FB 3055
        valid = true;
}
if (isExpressUsrAdv) {
    if ("conferencelist.aspx".indexOf(page) > -1) {
        valid = true;
        var arr = window.location.href.split("/");
        var pag = arr[arr.length - 1];
        page = pag.toLowerCase();
    }
    refPage = page;
    if (page.indexOf("conferencelist.aspx?t=7") > -1)
        window.location.assign("thankyou.aspx");
}
if ("manageuser.aspx".indexOf(page) > -1) {
    var arr = window.location.href.split("/");
    var pag = arr[arr.length - 1];
    page = pag.toLowerCase();

    if (page.indexOf("manageuser.aspx?t=1") > -1)
        page = "manageuser.aspx?t=1";
    else if (page.indexOf("manageuser.aspx?t=2") > -1)
        page = "manageuser.aspx?t=2";
    else if (page.indexOf("manageuser.aspx?t=3") > -1)
        page = "manageuser.aspx?t=3";
    else
        page = "manageuser.aspx";

    refPage = page;

}


if ("manageconference.aspx".indexOf(page) > -1) {
    var arr = window.location.href.split("/");
    var pag = arr[arr.length - 1];
    page = pag.toLowerCase();
    if (page.indexOf("manageconference.aspx?t=hf") > -1)
        valid = true;
    else
        page = "manageconference.aspx";
}


// Pages depend on menu, including 1st occurance of multi dependent pages
var pgs = "searchconferenceinputparameters.aspx,hdconferencelist.aspx";
if (pgs.indexOf(page) > -1)
    refPage = "settingselect2.aspx"; // 1

pgs = "monitormcu.aspx,point2point.aspx";//FB 2996
if (pgs.indexOf(page) > -1)
    refPage = "monitormcu.aspx";
    
pgs = "conferencelist.aspx,roomcalendar.aspx";
if (pgs.indexOf(page) > -1)
    refPage = "personalcalendar.aspx"; // 2

pgs = "showerror.aspx,dataimport.aspx,searchconferenceinputparameters.aspx,manageuserprofile.aspx,responseconference.aspx,lobbymanagement.aspx,emailcustomization.aspx?tp=u,emailcustomization.aspx?m=1&tp=u"; // FB 3055 //ZD 100170
if (pgs.indexOf(page) > -1)
    valid = true;


pgs = "addnewendpoint.aspx,addterminalendpoint.aspx,expressconference.aspx,dashboard.aspx,manageconference.aspx";
if (pgs.indexOf(page) > -1)
    refPage = "conferencesetup.aspx"; // 3

if (isExpressProfile) {
    if (page.indexOf("lobbymanagement.aspx") > -1 || page.indexOf("emailcustomization.aspx") > -1)
        valid = false;
}

pgs = "viewblockedmails.aspx?tp=u,viewblockedmails.aspx?m=1&tp=u";
if (pgs.indexOf(page) > -1 && userLevel != "0")
    refPage = "manageuserprofile.aspx"; // 4


pgs = "editinventory.aspx?tp=au,emailcustomization.aspx?tp=au,emailcustomization.aspx?m=1&tp=au, LobbyManagement.aspx?p=1&t=au";
if (pgs.indexOf(page) > -1 && userLevel != "3")
    refPage = "manageuser.aspx?t=1";

pgs = "managereports.aspx,userreport.aspx,utilizationreport.aspx,reportdetails.aspx";
if (pgs.indexOf(page) > -1)
    refPage = "graphicalreport.aspx"; // 5

pgs = "confirmtemplate.aspx,managetemplate2.aspx";
if (pgs.indexOf(page) > -1)
    refPage = "managetemplate.aspx"; // 6

pgs = "managegroup2.aspx";
if (pgs.indexOf(page) > -1)
    refPage = "managegroup.aspx"; // 7

pgs = "editendpoint.aspx";
if (pgs.indexOf(page) > -1)
    refPage = "endpointlist.aspx"; // 8

pgs = "bridgedetails.aspx,mcuresourcereport.aspx";
if (pgs.indexOf(page) > -1)
    refPage = "managebridge.aspx"; // 10

pgs = "audioaddonbridge.aspx";
if (pgs.indexOf(page) > -1)
    refPage = "manageaudioaddonbridges.aspx"; // 11

pgs = "manageroomprofile.aspx,managevirtualmeetingroom.aspx";
if (pgs.indexOf(page) > -1)
    refPage = "manageroom.aspx"; // 13

pgs = "managetier2.aspx";
if (pgs.indexOf(page) > -1)
    refPage = "managetiers.aspx"; // 14

pgs = "usermenucontroller.aspx";
if (pgs.indexOf(page) > -1)
    refPage = "manageuserroles.aspx"; // 21

pgs = "editusertemplate.aspx";
if (pgs.indexOf(page) > -1)
    refPage = "manageusertemplateslist.aspx"; // 22

pgs = "viewcustomattributes.aspx,uisettings.aspx,editentityoption.aspx,editholidaydetails.aspx,holidaydetails.aspx,managebatchreport.aspx,managecustomattribute.aspx,manageemaildomain.aspx,manageentitycode.aspx,managemessages.aspx,workingdays.aspx";
if (pgs.indexOf(page) > -1)
    refPage = "organisationsettings.aspx"; // 24

pgs = "manageorganization.aspx,eseventreport.aspx,manageorganizationprofile.aspx,userhistoryreport.aspx";
if (pgs.indexOf(page) > -1)
    refPage = "superadministrator.aspx"; // 31

if (page.indexOf("editconferenceorder.aspx") > -1 || page.indexOf("viewworkorderdetails.aspx") > -1)
    refPage = "conferencesetup.aspx"; // ZD 100263

if (page.indexOf("loglist.aspx") > -1) //ZD 100263
    refPage = "eventlog.aspx";    

// Pages including 2nd occurance of multi dependent page
var refPage2;
pgs = "viewblockedmails.aspx?tp=au,emailcustomization.aspx?tp=o,emailcustomization.aspx?m=1&tp=o,viewblockedmails.aspx?tp=o,viewblockedmails.aspx?m=1&tp=o";
if (pgs.indexOf(page) > -1)
    refPage2 = "organisationsettings.aspx";

if (page.indexOf("editblockemail.aspx") > -1) {
    pgs = "&tp=u";
    if (page.indexOf(pgs) > -1 && userLevel != "0")
        refPage2 = "manageuserprofile.aspx";
    pgs = "&tp=o";
    if (page.indexOf(pgs) > -1)
        refPage2 = "organisationsettings.aspx";
    pgs = "&tp=au";
    if (page.indexOf(pgs) > -1)
        refPage2 = "organisationsettings.aspx";
}

if (page.indexOf("conferenceorders.aspx") > -1 || page.indexOf("calendarworkorder.aspx") > -1 || page.indexOf("editconferenceorder.aspx") > -1) {
    pgs = "t=1";
    if (page.indexOf(pgs) > -1)
        refPage2 = "conferenceorders.aspx?t=1";
    pgs = "t=2";
    if (page.indexOf(pgs) > -1)
        refPage2 = "conferenceorders.aspx?t=2";
    pgs = "t=3";
    if (page.indexOf(pgs) > -1)
        refPage2 = "conferenceorders.aspx?t=3";
}

if (page.indexOf("inventorymanagement.aspx") > -1 || page.indexOf("editinventory.aspx") > -1) {
    pgs = "t=1";
    if (page.indexOf(pgs) > -1)
        refPage2 = "inventorymanagement.aspx?t=1";
    pgs = "t=2";
    if (page.indexOf(pgs) > -1)
        refPage2 = "inventorymanagement.aspx?t=2";
    pgs = "t=3";
    if (page.indexOf(pgs) > -1)
        refPage2 = "inventorymanagement.aspx?t=3";
}



function addmenu() {
    var str = menu.toString();
    var str = str.toLowerCase();
    if (str.indexOf("graphicalreport.aspx") > -1 && valid2 == true)
        valid = true;
    if (str.indexOf(refPage) > -1)
        valid = true;
    if (str.indexOf(mainPage) > -1)
        valid = true;
    if (str.indexOf(refPage2) > -1)
        valid = true;
    menunum++;
    menus[menunum] = menu;
}

function dumpmenus() {

    if (valid == false) {
        //window.stop(); // Not supportnig for IE browsers.
        window.location.assign("thankyou.aspx");
    }

    mt = "<script language=javascript>";
    for (a = 1; a < menus.length; a++) {
        mt += " menu" + a + "=menus[" + a + "];"
    }
    mt += "</script>";
    _d.write(mt)
}
// FB 3055 Ends


if (navigator.appVersion.indexOf("MSIE") > 0) {
    effect = "Fade(duration=0.2); Alpha(style=0,opacity=88); Shadow(color='#777777', Direction=135, Strength=5)"
}
else {
    effect = "Shadow(color='#777777', Direction=135, Strength=5)"
}

effect = ""			// 2785.2

timegap = 500; followspeed = 5; followrate = 40;
suboffset_top = 10; suboffset_left = 10;
style1 = ["white", "Purple", "Purple", "#FFF7CE", "lightblue", 12, "normal", "bold", "Arial, Verdana", 4, , , "66ffff", "000099", "Purple", "white", , "ffffff", "000099"]

style1_2 = ["#041433", "#ffffff", "#046380", "#E0E0E0", "lightblue", 12, "normal", "bold", "Arial, Verdana", 4, "", "", "#66ffff", "#000099", "#041433", "", "", "", "#000099", ] // FB 2791 //ZD 100156

style2 = ["black", "#FFF7CE", "#046380", "#FFF7CE", "lightblue", 120, "normal", "bold", "Arial, Verdana", 4, "image/menuarrow.gif", , "#66ffff", "#000099", "#046380", "", , "", "000099"] // FB 2791

style2_2 = ["#5E5D5E", "#E0E0E0", "#ffffff", "#7c7c7c", "transparent", 12, "normal", "bold", "Arial, Verdana", 10, "", "", "66ffff", "000099", "#041433", "#ffffff", "", "#046380", "#000099", ] // FB 2791
style2_3 = ["#5E5D5E", "#C2C2C2", "#ffffff", "#7c7c7c", "transparent", 12, "normal", "bold", "Arial, Verdana", 10, "", "", "66ffff", "000099", "#041433", "#ffffff", "", "#046380", "#000099", ] // FB 2791

var menu1Val, menu5Val;

menu1Val = ["<div id='menuHome' ></div>", "SettingSelect2.aspx", , "Go to the Home page", 0]; // FB 2719
if (defaultConfTempJS == "0")//FB 1755
    menu5Val = ["<div id='menuConf' ></div>", "ConferenceSetup.aspx?t=n&op=1", , "Create a New Conference", 0]; // FB 2719
else
    menu5Val = ["<div id='menuConf' ></div>", "ConferenceSetup.aspx?t=t", , "Create a New Conference", 0]; // FB 2719

if (navigator.appVersion.indexOf("MSIE") > 0) {

    menu_0 = ["mainmenu", 5, 250, 90, , , style1, 1, "center", effect, , 1, , , , , , , , , , ]			// 24 FB 2719
    menu_0_2 = ["mainmenu", 5, 250, 90, , , style1_2, 1, "center", effect, , 1, , , , , , , , , , ]		// 24//FB 1565 FB 2719
    if (isExpressUser != null)//FB 1779 FB 2827
        if (isExpressUser == 1)
        menu_0_2 = ["mainmenu", 5, 250, 90, , , style1_2, 1, "center", effect, , 1, , , , , , , , , , ]

    menu_1 = menu1Val
    menu_2 = ["<div id='menuCal' ></div>", "show-menu=conferences", , "Conferences", 0] // FB 2719
    menu_3 = menu5Val;
    menu_4 = ["<div id='menuSet' ></div>", "show-menu=settings", , "My Settings", 0] // FB 2719
    menu_5 = ["<div id='menuAdmin' ></div>", "show-menu=organization", , "Organization", 0] // FB 2719
    menu_6 = ["<div id='menuSite' ></div>", "show-menu=site", , "Site", 0] // FB 2719
    menu_7 = ["<div id='menuCall' ></div>", "ExpressConference.aspx?t=n", , "Schedule a Call", 0] //FB 1779 FB 2827
    menu_8 = ["<div id='menuReservation' ></div>", "ConferenceList.aspx?t=3", , "View Reservations", 0] //FB 1779 FB 2827

    menu = new Array();
    menu = (if_str == "2") ? menu.concat(menu_0_2) : menu.concat(menu_0);

    for (i = 1; i <= mmm_num; i++) {
        menu = (mmm_int & (1 << (mmm_num - i))) ? (menu.concat(eval("menu_" + i))) : menu;
    }
    addmenu();

    submenu1_0 = ["conferences", 116, , 100, 1, , style2, , "left", effect, , , , , , , , , , , , ]
    submenu1_0_2 = ["conferences", 116, , 100, 1, , style2_2, , "left", effect, , , , , , , , , , , , ]
    submenu1_1 = ["Calendar", "personalCalendar.aspx?v=1&r=1&hf=&m=&pub=&d=&comp=&f=v", , "Personal Calendar", 0]
    submenu1_2 = ["Call Monitor", "MonitorMCU.aspx", , "Call Monitor", 0]

    submenu2_0 = ["settings", 116, , 100, 1, , style2, , "left", effect, , , , , , , , , , , , ] // FB 2719 FB 2857 FB 2890
    submenu2_0_2 = ["settings", 116, , 100, 1, , style2_2, , "left", effect, , , , , , , , , , , , ] // FB 2719 FB 2857 FB 2890
    submenu2_1 = ["Preferences", "ManageUserProfile.aspx", , "Edit preferences", 0]
    submenu2_2 = ["Reports", "GraphicalReport.aspx", , "Reports", 0] //FB 2593 //FB 2885
    submenu2_3 = ["Templates", "ManageTemplate.aspx", , "Manage Conference Templates", 0]
    submenu2_4 = ["Groups", "ManageGroup.aspx", , "manage group", 0]


    submenu3_0 = ["organization", 116, , 120, 1, , style2, , "left", effect, , , , , , , , , , , , ] // FB 2719 FB 2857 FB 2890
    submenu3_0_2 = ["organization", 116, , 120, 1, , style2_2, , "left", effect, , , , , , , , , , , , ] // FB 2719 FB 2857 FB 2890

    submenu3_1 = ["Hardware", "show-menu=mcu", , "Manage Hardware", 0]
    submenu3_1_ = ["Hardware", "", , , 0]
    submenu3_2 = ["Locations", "show-menu=loc", , "Manage locations", 0]
    submenu3_2_ = ["Locations", "", , , 0]
    submenu3_3 = ["Users", "show-menu=usr", , "Manage users", 0]
    submenu3_4 = ["Options", "mainadministrator.aspx", , "Edit System Settings", 0]
    submenu3_5 = ["Settings ", "OrganisationSettings.aspx", , "Edit Organization Settings", 0] // FB 2719
    submenu3_6 = ["Audiovisual", "show-menu=av", , , 0]
    submenu3_6_ = ["Audiovisual", "", , , 0] // FB 2570
    submenu3_7 = ["Catering", "show-menu=catr", , , 0]
    submenu3_7_ = ["Catering", "", , , 0]
    submenu3_8 = ["Facility", "show-menu=hk", , , 0]
    submenu3_8_ = ["Facility", "", , , 0] // FB 2570 // FB 2593 End

    submenu4_0 = ["site", 116, , 100, 1, , style2, , "left", effect, , , , , , , , , , , , ] // FB 2719 FB 2857 FB 2890
    submenu4_0_2 = ["site", 116, , 100, 1, , style2_2, , "left", effect, , , , , , , , , , , , ] // FB 2719 FB 2857 FB 2890
    submenu4_1 = ["Settings", "SuperAdministrator.aspx", , "Edit system settings", 0]

    submenu3_1_0 = ["mcu", , , 100, 1, "", style2, , "left", effect, , , , , , , , , , , , ] // FB 2719
    submenu3_1_0_2 = ["mcu", , , 100, 1, "", style2_3, , "left", effect, , , , , , , , , , , , ] // FB 2719
    submenu3_1_1 = ["Endpoints", "EndpointList.aspx?t=", , "Setup Bridge", 0]
    submenu3_1_2 = ["Diagnostics", "EventLog.aspx", , "View hardware problem log", 0]
    submenu3_1_3 = ["MCUs", "ManageBridge.aspx", , "Edit Bridge Management", 0]
    submenu3_1_4 = ["Audio Bridges", "ManageAudioAddOnBridges.aspx", , "Audio Bridge Management", 0] //FB 2023
    submenu3_1_5 = ["EM7", "EM7Dashboard.aspx", , "EM7", 0] //FB 2633

    submenu3_2_0 = ["loc", , , 100, 1, "", style2, , "left", effect, , , , , , , , , , , , ] // FB 2719
    submenu3_2_0_2 = ["loc", , , 100, 1, "", style2_3, , "left", effect, , , , , , , , , , , , ] // FB 2719
    submenu3_2_1 = ["Rooms", "manageroom.aspx?hf=&m=&pub=&d=&comp=&f=&frm=", , "Edit or set up conference room", 0]
    submenu3_2_2 = ["Tiers", "ManageTiers.aspx", , "Manage Tiers", 0]


    submenu3_3_0 = ["usr", , , 150, 1, "", style2, , "left", effect, , , , , , , , , , , , ] // FB 2719
    submenu3_3_0_2 = ["usr", , , 150, 1, "", style2_3, , "left", effect, , , , , , , , , , , , ] // FB 2719
    submenu3_3_1 = ["Active Users", "ManageUser.aspx?t=1", , "Edit, search or create a new user", 0]
    submenu3_3_2 = ["Bulk Tool", "allocation.aspx", , "Manage Bulk User", 0]
    submenu3_3_3 = ["Departments", "ManageDepartment.aspx", , "Manage Department", 0]
    submenu3_3_4 = ["Guests", "ManageUser.aspx?t=2", , "Edit, search or create a new guest", 0]
    if (sso_int)
        submenu3_3_5 = ["Restore User", "ManageUser.aspx?t=3", , "Delete or Replaced a removed user", 0]
    else
        submenu3_3_5 = ["Inactive Users", "ManageUser.aspx?t=3", , "Delete or Replaced a removed user", 0]
    submenu3_3_6 = ["LDAP Directory Import", "LDAPImport.aspx", , "Setup Multiple Users", 0]
    submenu3_3_7 = ["Roles", "manageuserroles.aspx", , "Edit, search or create user roles", 0]
    submenu3_3_8 = ["Templates", "ManageUserTemplatesList.aspx", , "Edit, search or create user templates", 0]

    submenu3_6_0 = ["av", , , 150, 1, "", style2, , "left", effect, , , , , , , , , , , , ] // FB 2719
    submenu3_6_0_2 = ["av", , , 150, 1, "", style2_3, , "left", effect, , , , , , , , , , , , ] // FB 2719
    submenu3_6_1 = ["Audiovisual Inventories", "InventoryManagement.aspx?t=1", , "", 0] // FB 2570
    submenu3_6_2 = ["Work Orders", "ConferenceOrders.aspx?t=1", , "", 0]


    submenu3_7_0 = ["catr", , , 150, 1, "", style2, , "left", effect, , , , , , , , , , , , ] // FB 2719
    submenu3_7_0_2 = ["catr", , , 150, 1, "", style2_3, , "left", effect, , , , , , , , , , , , ] // FB 2719
    submenu3_7_1 = ["Catering Menus", "InventoryManagement.aspx?t=2", , "", 0] // FB 2570
    submenu3_7_2 = ["Work Orders", "ConferenceOrders.aspx?t=2", , "", 0]

    submenu3_8_0 = ["hk", , , 150, 1, "", style2, , "left", effect, , , , , , , , , , , , ] // FB 2719
    submenu3_8_0_2 = ["hk", , , 150, 1, "", style2_3, , "left", effect, , , , , , , , , , , , ] // FB 2719
    submenu3_8_1 = ["Facility Services", "InventoryManagement.aspx?t=3", , "", 0] // FB 2570
    submenu3_8_2 = ["Work Orders", "ConferenceOrders.aspx?t=3", , "", 0]
    //FB 2885 Ends

    cur_munu_no = 1; cur_munu_str = "1_";
    if (mms_int[cur_munu_no]) {
        menu = new Array();
        menu = menu.concat(eval("submenu" + cur_munu_str + "0" + ((if_str == "2") ? "_2" : "")));
        for (i = 1; i <= mms_num[cur_munu_no]; i++) {
            if (EnableCallMonitor == 0 && i == 2) continue; 
            menu = (mms_int[cur_munu_no] & (1 << (mms_num[cur_munu_no] - i))) ? (menu.concat(eval("submenu" + cur_munu_str + i))) : menu;
        }
        addmenu();
    }

    cur_munu_no = 2; cur_munu_str = "2_";
    if (mms_int[cur_munu_no]) {
        menu = new Array();
        menu = menu.concat(eval("submenu" + cur_munu_str + "0" + ((if_str == "2") ? "_2" : "")));
        for (i = 1; i <= mms_num[cur_munu_no]; i++) {
            menu = (mms_int[cur_munu_no] & (1 << (mms_num[cur_munu_no] - i))) ? (menu.concat(eval("submenu" + cur_munu_str + i))) : menu;
        }
        addmenu();
    }
    cur_munu_no = 3; cur_munu_str = "3_";
    if (mms_int[cur_munu_no]) {
        '<%Session["SettingsMenu"] = "1";%>'
        menu = new Array();
        menu = menu.concat(eval("submenu" + cur_munu_str + "0" + ((if_str == "2") ? "_2" : "")));
        for (i = 1; i <= mms_num[cur_munu_no] - 3; i++) {
            menu = (mms_int[cur_munu_no] & (1 << (mms_num[cur_munu_no] - i))) ? (menu.concat(eval("submenu" + cur_munu_str + i))) : menu;
        }
        for (i = mms_num[cur_munu_no] - 2; i <= mms_num[cur_munu_no]; i++) {
            if (rf_int & 1)
                menu = (mms_int[cur_munu_no] & (1 << (mms_num[cur_munu_no] - i))) ? (menu.concat(eval("submenu" + cur_munu_str + i))) : menu;
            rf_int = rf_int / 2;
        }
        addmenu();
    }

    cur_munu_no = 4; cur_munu_str = "3_1_";
    if (mms_int[cur_munu_no]) {
        menu = new Array();
        menu = menu.concat(eval("submenu" + cur_munu_str + "0" + ((if_str == "2") ? "_2" : "")));
        for (i = 1; i <= mms_num[cur_munu_no]; i++) {
            if (EnableAudioBridge == 0 && i == 4) continue; //FB 2023
            if (EnableEM7Opt == 0 && i == 5) continue; // FB 2633
            menu = (mms_int[cur_munu_no] & (1 << (mms_num[cur_munu_no] - i))) ? (menu.concat(eval("submenu" + cur_munu_str + i))) : menu;
        }
        addmenu();
    }
    cur_munu_no = 5; cur_munu_str = "3_2_";
    if (mms_int[cur_munu_no]) {
        menu = new Array();
        menu = menu.concat(eval("submenu" + cur_munu_str + "0" + ((if_str == "2") ? "_2" : "")));
        for (i = 1; i <= mms_num[cur_munu_no]; i++) {
            menu = (mms_int[cur_munu_no] & (1 << (mms_num[cur_munu_no] - i))) ? (menu.concat(eval("submenu" + cur_munu_str + i))) : menu;
        }
        addmenu();
    }
    cur_munu_no = 6; cur_munu_str = "3_3_";
    if (mms_int[cur_munu_no]) {
        menu = new Array();
        menu = menu.concat(eval("submenu" + cur_munu_str + "0" + ((if_str == "2") ? "_2" : "")));
        for (i = 1; i <= mms_num[cur_munu_no]; i++) {
            menu = (mms_int[cur_munu_no] & (1 << (mms_num[cur_munu_no] - i))) ? (menu.concat(eval("submenu" + cur_munu_str + i))) : menu;
        }
        addmenu();
    }

    cur_munu_no = 7; cur_munu_str = "3_6_"; //FB 2593 //FB 2885
    if (mms_int[cur_munu_no]) {
        menu = new Array();
        menu = menu.concat(eval("submenu" + cur_munu_str + "0" + ((if_str == "2") ? "_2" : "")));
        for (i = 1; i <= mms_num[cur_munu_no]; i++) {
            menu = (mms_int[cur_munu_no] & (1 << (mms_num[cur_munu_no] - i))) ? (menu.concat(eval("submenu" + cur_munu_str + i))) : menu;
        }
        addmenu();
    }

    cur_munu_no = 8; cur_munu_str = "3_7_"; //FB 2593 //FB 2885 
    if (mms_int[cur_munu_no]) {
        menu = new Array();
        menu = menu.concat(eval("submenu" + cur_munu_str + "0" + ((if_str == "2") ? "_2" : "")));
        for (i = 1; i <= mms_num[cur_munu_no]; i++) {
            menu = (mms_int[cur_munu_no] & (1 << (mms_num[cur_munu_no] - i))) ? (menu.concat(eval("submenu" + cur_munu_str + i))) : menu;
        }
        addmenu();
    }

    cur_munu_no = 9; cur_munu_str = "3_8_"; //FB 2593 //FB 2885
    if (mms_int[cur_munu_no]) {
        menu = new Array();
        menu = menu.concat(eval("submenu" + cur_munu_str + "0" + ((if_str == "2") ? "_2" : "")));
        for (i = 1; i <= mms_num[cur_munu_no]; i++) {
            menu = (mms_int[cur_munu_no] & (1 << (mms_num[cur_munu_no] - i))) ? (menu.concat(eval("submenu" + cur_munu_str + i))) : menu;
        }
        addmenu();
    }

    cur_munu_no = 10; cur_munu_str = "4_";
    if (mms_int[cur_munu_no]) {
        menu = new Array();
        menu = menu.concat(eval("submenu" + cur_munu_str + "0" + ((if_str == "2") ? "_2" : "")));
        for (i = 1; i <= mms_num[cur_munu_no]; i++) {
            menu = (mms_int[cur_munu_no] & (1 << (mms_num[cur_munu_no] - i))) ? (menu.concat(eval("submenu" + cur_munu_str + i))) : menu;
        }
        addmenu();
    }
}
else {
    menu_0 = ["mainmenu", 5, 250, 110, , , style1, 1, "center", effect, , 1, , , , , , , , , , , ]			// 24 FB 2719
    menu_0_2 = ["mainmenu", 5, 250, 110, , , style1_2, 1, "center", effect, , 1, , , , , , , , , , , ]		// 24 // FB 2050 FB 2719
    menu_1 = menu1Val
    menu_2 = ["<div id='menuCal' ></div>", "show-menu=conferences", , "Conferences", 0] // FB 2719
    menu_3 = menu5Val;
    menu_4 = ["<div id='menuSet' ></div>", "show-menu=settings", , "My Settings", 0] // FB 2719
    menu_5 = ["<div id='menuAdmin' ></div>", "show-menu=organization", , "Organization", 0] // FB 2719
    menu_6 = ["<div id='menuSite' ></div>", "show-menu=site", , "Site", 0] // FB 2719
    menu_7 = ["<div id='menuCall' ></div>", "ExpressConference.aspx?t=n", , "Schedule a Call", 0] //FB 1779 FB 2827
    menu_8 = ["<div id='menuReservation' ></div>", "ConferenceList.aspx?t=3", , "View Reservations", 0] //FB 1779 FB 2827

    menu = new Array();
    menu = (if_str == "2") ? menu.concat(menu_0_2) : menu.concat(menu_0);

    for (i = 1; i <= mmm_num; i++) {
        menu = (mmm_int & (1 << (mmm_num - i))) ? (menu.concat(eval("menu_" + i))) : menu;
    }
    addmenu();

    submenu1_0 = ["conferences", 116, , 100, 1, , style2, , "left", effect, , , , , , , , , , , , , ]
    submenu1_0_2 = ["conferences", 116, , 100, 1, , style2_2, , "left", effect, , , , , , , , , , , , , ]
    submenu1_1 = ["Calendar", "personalCalendar.aspx?v=1&r=1&hf=&m=&pub=&d=&comp=&f=v", , "Personal Calendar", 0]
    submenu1_2 = ["Call Monitor", "MonitorMCU.aspx", , "Call Monitor", 0]

    submenu2_0 = ["settings", 116, , 100, 1, , style2, , "left", effect, , , , , , , , , , , , , ] // FB 2719 FB 2857 FB 2890
    submenu2_0_2 = ["settings", 116, , 100, 1, , style2_2, , "left", effect, , , , , , , , , , , , , ] // FB 2719 FB 2857 FB 2890
    submenu2_1 = ["Preferences", "ManageUserProfile.aspx", , "Edit preferences", 0]
    submenu2_2 = ["Reports", "GraphicalReport.aspx", , "Reports", 0] //FB 2593 //FB 2885
    submenu2_3 = ["Templates", "ManageTemplate.aspx", , "Manage Conference Templates", 0]
    submenu2_4 = ["Groups", "ManageGroup.aspx", , "manage group", 0]


    submenu3_0 = ["organization", 116, , 120, 1, , style2, , "left", effect, , , , , , , , , , , , , ] // FB 2719 FB 2857 FB 2890
    submenu3_0_2 = ["organization", 116, , 120, 1, , style2_2, , "left", effect, , , , , , , , , , , , , ] // FB 2719 FB 2857 FB 2890

    submenu3_1 = ["Hardware", "show-menu=mcu", , "Manage Hardware", 0]
    submenu3_1_ = ["Hardware", "", , , 0]
    submenu3_2 = ["Locations", "show-menu=loc", , "Manage locations", 0]
    submenu3_2_ = ["Locations", "", , , 0]
    submenu3_3 = ["Users", "show-menu=usr", , "Manage users", 0]
    submenu3_3_ = ["Users", "", , , 0]
    submenu3_4 = ["Options", "mainadministrator.aspx", , "Edit System Settings", 0]
    submenu3_5 = ["Settings ", "OrganisationSettings.aspx", , "Edit Organization Settings", 0]// FB 2719
    submenu3_6 = ["Audiovisual", "show-menu=av", , , 0]
    submenu3_6_ = ["Audiovisual", "", , , 0] // FB 2570
    submenu3_7 = ["Catering", "show-menu=catr", , , 0]
    submenu3_7_ = ["Catering", "", , , 0]
    submenu3_8 = ["Facility", "show-menu=hk", , , 0]
    submenu3_8_ = ["Facility", "", , , 0] // FB 2570 // FB 2593 End

    submenu4_0 = ["site", 116, , 100, 1, , style2, , "left", effect, , , , , , , , , , , , , ] // FB 2719 FB 2857 FB 2890
    submenu4_0_2 = ["site", 116, , 100, 1, , style2_2, , "left", effect, , , , , , , , , , , , , ] // FB 2719 FB 2857 FB 2890
    submenu4_1 = ["Settings", "SuperAdministrator.aspx", , "Edit system settings", 0]

    submenu3_1_0 = ["mcu", , , 100, 1, "", style2, , "left", effect, , , , , , , , , , , , , ] // FB 2719
    submenu3_1_0_2 = ["mcu", , , 100, 1, "", style2_3, , "left", effect, , , , , , , , , , , , , ] // FB 2719
    submenu3_1_1 = ["Endpoints", "EndpointList.aspx?t=", , "Setup Bridge", 0]
    submenu3_1_2 = ["Diagnostics", "EventLog.aspx", , "View hardware problem log", 0]
    submenu3_1_3 = ["MCUs", "ManageBridge.aspx", , "Edit Bridge Management", 0]
    submenu3_1_4 = ["Audio Bridges", "ManageAudioAddOnBridges.aspx", , "Audio Bridge Management", 0] //FB 2023
    submenu3_1_5 = ["EM7", "EM7Dashboard.aspx", , "EM7", 0] //FB 2633

    submenu3_2_0 = ["loc", , , 100, 1, "", style2, , "left", effect, , , , , , , , , , , , , ] // FB 2719
    submenu3_2_0_2 = ["loc", , , 100, 1, "", style2_3, , "left", effect, , , , , , , , , , , , , ] // FB 2719
    submenu3_2_1 = ["Rooms", "manageroom.aspx?hf=&m=&pub=&d=&comp=&f=&frm=", , "Edit or set up conference room", 0]
    submenu3_2_2 = ["Tiers", "ManageTiers.aspx", , "Manage Tiers", 0]


    submenu3_3_0 = ["usr", , , 150, 1, "", style2, , "left", effect, , , , , , , , , , , , , ] // FB 2719
    submenu3_3_0_2 = ["usr", , , 150, 1, "", style2_3, , "left", effect, , , , , , , , , , , , , ] // FB 2719
    submenu3_3_1 = ["Active Users", "ManageUser.aspx?t=1", , "Edit, search or create a new user", 0]
    submenu3_3_2 = ["Bulk Tool", "allocation.aspx", , "Manage Bulk User", 0]
    submenu3_3_3 = ["Departments", "ManageDepartment.aspx", , "Manage Department", 0]
    submenu3_3_4 = ["Guests", "ManageUser.aspx?t=2", , "Edit, search or create a new guest", 0]
    if (sso_int)
        submenu3_3_5 = ["Restore User", "ManageUser.aspx?t=3", , "Delete or Replaced a removed user", 0]
    else
        submenu3_3_5 = ["Inactive Users", "ManageUser.aspx?t=3", , "Delete or Replaced a removed user", 0]
    submenu3_3_6 = ["LDAP Directory Import", "LDAPImport.aspx", , "Setup Multiple Users", 0]
    submenu3_3_7 = ["Roles", "manageuserroles.aspx", , "Edit, search or create user roles", 0]
    submenu3_3_8 = ["Templates", "ManageUserTemplatesList.aspx", , "Edit, search or create user templates", 0]

    submenu3_6_0 = ["av", , , 150, 1, "", style2, , "left", effect, , , , , , , , , , , , , ] // FB 2719
    submenu3_6_0_2 = ["av", , , 150, 1, "", style2_3, , "left", effect, , , , , , , , , , , , , ] // FB 2719
    submenu3_6_1 = ["Audiovisual Inventories", "InventoryManagement.aspx?t=1", , "", 0] // FB 2570
    submenu3_6_2 = ["Work Orders", "ConferenceOrders.aspx?t=1", , "", 0]

    submenu3_7_0 = ["catr", , , 150, 1, "", style2, , "left", effect, , , , , , , , , , , , , ] // FB 2719
    submenu3_7_0_2 = ["catr", , , 150, 1, "", style2_3, , "left", effect, , , , , , , , , , , , , ] // FB 2719
    submenu3_7_1 = ["Catering Menus", "InventoryManagement.aspx?t=2", , "", 0] // FB 2570
    submenu3_7_2 = ["Work Orders", "ConferenceOrders.aspx?t=2", , "", 0]

    submenu3_8_0 = ["hk", , , 150, 1, "", style2, , "left", effect, , , , , , , , , , , , , ] // FB 2719
    submenu3_8_0_2 = ["hk", , , 150, 1, "", style2_3, , "left", effect, , , , , , , , , , , , , ] // FB 2719
    submenu3_8_1 = ["Facility Services", "InventoryManagement.aspx?t=3", , "", 0] // FB 2570
    submenu3_8_2 = ["Work Orders", "ConferenceOrders.aspx?t=3", , "", 0]

    cur_munu_no = 1; cur_munu_str = "1_";
    if (mms_int[cur_munu_no]) {
        menu = new Array();
        menu = menu.concat(eval("submenu" + cur_munu_str + "0" + ((if_str == "2") ? "_2" : "")));
        for (i = 1; i <= mms_num[cur_munu_no]; i++) {
            if (EnableCallMonitor == 0 && i == 2) continue; 
            menu = (mms_int[cur_munu_no] & (1 << (mms_num[cur_munu_no] - i))) ? (menu.concat(eval("submenu" + cur_munu_str + i))) : menu;
        }
        addmenu();
    }

    cur_munu_no = 2; cur_munu_str = "2_";
    if (mms_int[cur_munu_no]) {
        menu = new Array();
        menu = menu.concat(eval("submenu" + cur_munu_str + "0" + ((if_str == "2") ? "_2" : "")));
        for (i = 1; i <= mms_num[cur_munu_no]; i++) {
            menu = (mms_int[cur_munu_no] & (1 << (mms_num[cur_munu_no] - i))) ? (menu.concat(eval("submenu" + cur_munu_str + i))) : menu;
        }
        addmenu();
    }

    cur_munu_no = 3; cur_munu_str = "3_";
    if (mms_int[cur_munu_no]) {
        '<%Session["SettingsMenu"] = "1";%>'
        menu = new Array();
        menu = menu.concat(eval("submenu" + cur_munu_str + "0" + ((if_str == "2") ? "_2" : "")));
        for (i = 1; i <= mms_num[cur_munu_no] - 3; i++) {
            menu = (mms_int[cur_munu_no] & (1 << (mms_num[cur_munu_no] - i))) ? (menu.concat(eval("submenu" + cur_munu_str + i))) : menu;
        }
        for (i = mms_num[cur_munu_no] - 2; i <= mms_num[cur_munu_no]; i++) {
            if (rf_int & 1)
                menu = (mms_int[cur_munu_no] & (1 << (mms_num[cur_munu_no] - i))) ? (menu.concat(eval("submenu" + cur_munu_str + i))) : menu;
            rf_int = rf_int / 2;
        }
        addmenu();
    }

    cur_munu_no = 4; cur_munu_str = "3_1_";
    if (mms_int[cur_munu_no]) {
        menu = new Array();
        menu = menu.concat(eval("submenu" + cur_munu_str + "0" + ((if_str == "2") ? "_2" : "")));
        for (i = 1; i <= mms_num[cur_munu_no]; i++) {
            if (EnableAudioBridge == 0 && i == 4) continue; //FB 2023
            if (EnableEM7Opt == 0 && i == 5) continue; // FB 2633
            menu = (mms_int[cur_munu_no] & (1 << (mms_num[cur_munu_no] - i))) ? (menu.concat(eval("submenu" + cur_munu_str + i))) : menu;
        }
        addmenu();
    }
    cur_munu_no = 5; cur_munu_str = "3_2_";
    if (mms_int[cur_munu_no]) {
        menu = new Array();
        menu = menu.concat(eval("submenu" + cur_munu_str + "0" + ((if_str == "2") ? "_2" : "")));
        for (i = 1; i <= mms_num[cur_munu_no]; i++) {
            menu = (mms_int[cur_munu_no] & (1 << (mms_num[cur_munu_no] - i))) ? (menu.concat(eval("submenu" + cur_munu_str + i))) : menu;
        }
        addmenu();
    }
    cur_munu_no = 6; cur_munu_str = "3_3_";
    if (mms_int[cur_munu_no]) {
        menu = new Array();
        menu = menu.concat(eval("submenu" + cur_munu_str + "0" + ((if_str == "2") ? "_2" : "")));
        for (i = 1; i <= mms_num[cur_munu_no]; i++) {
            menu = (mms_int[cur_munu_no] & (1 << (mms_num[cur_munu_no] - i))) ? (menu.concat(eval("submenu" + cur_munu_str + i))) : menu;
        }
        addmenu();
    }

    cur_munu_no = 7; cur_munu_str = "3_6_"; //FB 2593 //FB 2885
    if (mms_int[cur_munu_no]) {
        menu = new Array();
        menu = menu.concat(eval("submenu" + cur_munu_str + "0" + ((if_str == "2") ? "_2" : "")));
        for (i = 1; i <= mms_num[cur_munu_no]; i++) {
            menu = (mms_int[cur_munu_no] & (1 << (mms_num[cur_munu_no] - i))) ? (menu.concat(eval("submenu" + cur_munu_str + i))) : menu;
        }
        addmenu();
    }

    cur_munu_no = 8; cur_munu_str = "3_7_"; //FB 2593 //FB 2885
    if (mms_int[cur_munu_no]) {
        menu = new Array();
        menu = menu.concat(eval("submenu" + cur_munu_str + "0" + ((if_str == "2") ? "_2" : "")));
        for (i = 1; i <= mms_num[cur_munu_no]; i++) {
            menu = (mms_int[cur_munu_no] & (1 << (mms_num[cur_munu_no] - i))) ? (menu.concat(eval("submenu" + cur_munu_str + i))) : menu;
        }
        addmenu();
    }

    cur_munu_no = 9; cur_munu_str = "3_8_"; //FB 2593 //FB 2885
    if (mms_int[cur_munu_no]) {
        menu = new Array();
        menu = menu.concat(eval("submenu" + cur_munu_str + "0" + ((if_str == "2") ? "_2" : "")));
        for (i = 1; i <= mms_num[cur_munu_no]; i++) {
            menu = (mms_int[cur_munu_no] & (1 << (mms_num[cur_munu_no] - i))) ? (menu.concat(eval("submenu" + cur_munu_str + i))) : menu;
        }
        addmenu();
    }

    cur_munu_no = 10; cur_munu_str = "4_";
    if (mms_int[cur_munu_no]) {
        menu = new Array();
        menu = menu.concat(eval("submenu" + cur_munu_str + "0" + ((if_str == "2") ? "_2" : "")));
        for (i = 1; i <= mms_num[cur_munu_no]; i++) {
            menu = (mms_int[cur_munu_no] & (1 << (mms_num[cur_munu_no] - i))) ? (menu.concat(eval("submenu" + cur_munu_str + i))) : menu;
        }
        addmenu();
    }
}
dumpmenus()

