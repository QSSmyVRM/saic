/*ZD 100147 Start*/
/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
/*ZD 100147 End*/
menunum = 0;
menus=new Array();
_d=document; 



function addmenu()
{
	menunum++;
	menus[menunum]=menu;
}

function dumpmenus()
{
	mt = "<script language=javascript>";
	for (a=1; a<menus.length; a++) {
		mt += " menu" + a + "=menus[" + a + "];"
	}
	mt += "</script>";
	_d.write(mt)
}


if (navigator.appVersion.indexOf("MSIE") > 0) // FB 2815
{
	effect = "Fade(duration=0.2); Alpha(style=0,opacity=88); Shadow(color='#777777', Direction=135, Strength=5)"
}
else
{
	effect = "Shadow(color='#777777', Direction=135, Strength=5)" 
}

effect = ""			// 2785.2

timegap=500;		followspeed=5;		followrate=40;
suboffset_top=10;	suboffset_left=10;	

/*style1=[
"white",			// Mouse Off Font 
"Purple",			// Mouse Off Background 
"Purple",			// Mouse On Font 
"#FFF7CE",			// Mouse On Background 
"lightblue",		// Menu Border 
12,					// Font Size in pixels
"normal",			// Font Style 
"bold",				// Font Weight
"Verdana, Arial",	// Font Name
4,					// Menu Item Padding
,					// Sub Menu Image 
,					// Border & Separator bar
"66ffff",			// High 
"000099",			// Low 
"Purple",			// Current Page Item Font Color
"white",				// Current Page Item Background Color
,					// Top Bar image 
"ffffff",			// Menu Header Font Color 
"000099",			// Menu Header Background Color
]

style1_2=[
"#ff6347",			// Mouse Off Font 
"",					// Mouse Off Background 
"#3399FF",			// Mouse On Font 
"#FFF7CE",			// Mouse On Background 
"lightblue",		// Menu Border 
12,					// Font Size in pixels
"normal",			// Font Style 
"bold",				// Font Weight
"Verdana, Arial",	// Font Name
4,					// Menu Item Padding
,					// Sub Menu Image 
,					// Border & Separator bar
"66ffff",			// High 
"000099",			// Low 
"Purple",			// Current Page Item Font Color
"white",				// Current Page Item Background Color
,					// Top Bar image 
"ffffff",			// Menu Header Font Color 
"000099",			// Menu Header Background Color
]

style2=[
"black",			// Mouse Off Font 
"#FFF7CE",			// Mouse Off Background 
"Purple",			// Mouse On Font 
"#FFF7CE",			// Mouse On Background 
"lightblue",		// Menu Border 
12,					// Font Size in pixels
"normal",			// Font Style 
"bold",				// Font Weight 
"Verdana, Arial",	// Font Name
4,					// Menu Item Padding
"image/menuarrow.gif",		// Sub Menu Image 
,					// Border & Separator bar
"66ffff",			// High 
"000099",			// Low 
"Purple",			// Current Page Item Font 
"white",				// Current Page Item Background 
,					// Top Bar image
"ffffff",			// Menu Header Font 
"000099",			// Menu Header Background 
]

style2_2=[
"white",			// Mouse Off Font 
"#9acd32",			// Mouse Off Background 
"#3399FF",			// Mouse On Font 
"#FFF7CE",			// Mouse On Background 
"lightblue",		// Menu Border 
12,					// Font Size in pixels
"normal",			// Font Style 
"bold",				// Font Weight 
"Verdana, Arial",	// Font Name
4,					// Menu Item Padding
"image/menuarrow.gif",		// Sub Menu Image 
,					// Border & Separator bar
"66ffff",			// High 
"000099",			// Low 
"Purple",			// Current Page Item Font 
"white",				// Current Page Item Background 
,					// Top Bar image
"ffffff",			// Menu Header Font 
"000099",			// Menu Header Background 
]
*/
style1=["white","Purple","Purple","#FFF7CE","lightblue",12,"normal","bold","Arial, Verdana",4,,,"66ffff","000099","Purple","white",,"ffffff","000099"]

//style1_2=["#ff6347","","#3399FF","#FFF7CE","lightblue",12,"normal","bold","Verdana, Arial",4,,,"66ffff","000099","Purple","white",,"ffffff","000099",]

style1_2 = ["#041433", "#ffffff", "#046380", "#E0E0E0", "lightblue", 12, "normal", "bold", "Arial, Verdana", 4, "", "", "#66ffff", "#000099", "#041433", "#ffffff", "", "#ffffff", "#000099", ] // FB 2719

style2=["black","#FFF7CE","#046380","#FFF7CE","lightblue",120,"normal","bold","Arial, Verdana",4,"image/menuarrow.gif",,"#66ffff","#000099","#046380","white",,"ffffff","000099"]

//style2_2=["white","#9acd32","#3399FF","#FFF7CE","lightblue",12,"normal","bold","Verdana, Arial",4,"image/menuarrow.gif",,"66ffff","000099","Purple","white",,"ffffff","000099",]

style2_2 = ["#5E5D5E", "#E0E0E0", "#ffffff", "#7c7c7c", "transparent", 12, "normal", "bold", "Arial, Verdana", 10, "", "", "66ffff", "000099", "#041433", "#ffffff", "", "#046380", "#000099", ] // FB 2719
style2_3 = ["#5E5D5E", "#C2C2C2", "#ffffff", "#7c7c7c", "transparent", 12, "normal", "bold", "Arial, Verdana", 10, "", "", "66ffff", "000099", "#041433", "#ffffff", "", "#046380", "#000099", ] // FB 2719

var menu1Val, menu5Val;

menu1Val = ["<div id='menuHomeRed' ></div>", "SettingSelect2.aspx", , "Go to the Home page", 0]; // FB 2719
if(defaultConfTempJS == "0")//FB 1755
    menu5Val = ["<div id='menuConfRed' ></div>", "ConferenceSetup.aspx?t=n&op=1", , "Create a New Conference", 0]; // FB 2719
else
    menu5Val = ["<div id='menuConfRed' ></div>", "ConferenceSetup.aspx?t=t", , "Create a New Conference", 0]; // FB 2719

if (navigator.appVersion.indexOf("MSIE") > 0) // FB 2815
{
    menu_0 = ["mainmenu",5,250,90,,,style1,1,"center",effect,,1,,,,,,,,,,]			// 24 FB 2719
    menu_0_2 = ["mainmenu",5,250,90,,,style1_2,1,"center",effect,,1,,,,,,,,,,]		// 24//FB 1565 FB 2719
    if (isExpressUser != null)//FB 1779
        if (isExpressUser == 1)
            menu_0_2 = ["mainmenu", 72, 3, 110, , , style1_2, 1, "center", effect, , 1, , , , , , , , , , ]
            
    menu_1 = menu1Val //["Lobby&nbsp;","SettingSelect2.aspx",,"Go to the lobby page",0]
//    menu_2 = ["Calendar&nbsp;","dispatcher/admindispatcher.asp?cmd=ManageConfRoom&f=v&hf=",,"Go to Room Calendar",0]
    menu_2 = ["<div id='menuCalRed' ></div>", "personalCalendar.aspx?v=1&r=1&hf=&m=&pub=&d=&comp=&f=v", , "Go to Room Calendar", 0] // FB 2719
    //menu_3 = ["List&nbsp;","ConferenceList.aspx?t=2",,"Ongoing Conferences",0]
    menu_3 = menu5Val;
    //menu_4 = ["Search&nbsp;","dispatcher/gendispatcher.asp?cmd=GetSearchConference",,"Search Conference",0]
    menu_4 = ["<div id='menuSetRed' ></div>", "show-menu=settings", , "My Settings", 0] // FB 2719
    //menu_5 = ["New Conference","aspToaspNet.asp?tp=ConferenceSetup.aspx&t=n",,"Create a new conference",0]
    //menu_5 = menu5Val //["New Conference","show-menu=conference",,"Create a new conference",0]
    menu_5 = ["<div id='menuAdminRed' ></div>", "show-menu=organization", , "Organization", 0] // FB 2719
    menu_6 = ["<div id='menuSiteRed' ></div>", "show-menu=site", , "Site", 0] // FB 2719
    menu_7 = ["Schedule a<br/> Call ", "ExpressConference.aspx?t=n", , "Schedule a Call", 0] //FB 1779
    menu_8 = ["View / Edit<br/> Reservations", "ConferenceList.aspx?t=3", , "View Reservations", 0] //FB 1779

    menu = new Array();
    menu = (if_str=="2") ? menu.concat(menu_0_2) : menu.concat(menu_0);

    for (i=1; i<=mmm_num; i++) {
	    menu = ( mmm_int & (1 << (mmm_num-i)) ) ? ( menu.concat(eval("menu_" + i)) ) : menu;
    }
    addmenu();
	submenu1_0   = ["conference",94,,180,1,,style2,,"left",effect,,,,,,,,,,,,]
	submenu1_0_2 = ["conference",94,,180,1,,style2_2,,"left",effect,,,,,,,,,,,,]
    submenu1_1 = ["Future","ConferenceSetup.aspx?t=n&op=1",,"Create a Future Conference",0]
	submenu1_2 = ["Immediate","ConferenceSetup.aspx?t=n&op=2",,"Create an Immediate Conference",0]
	submenu1_3 = ["From Template","ManageTemplate.aspx",,"Create a conference from Template",0]

//    menu = new Array();
    //menu = menu.concat( eval("submenu1_0"));
//    menu = menu.concat( eval("submenu1_0_2"));
//    for (i=1; i<=3; i++) 
//        menu = menu.concat(eval("submenu1_" + i));

//    addmenu();
	submenu2_0   = ["settings",,,100,1,,style2,,"left",effect,,,,,,,,,,,,] // FB 2719
	submenu2_0_2 = ["settings",,,100,1,,style2_2,,"left",effect,,,,,,,,,,,,] // FB 2719
	submenu2_1 = ["Preferences","ManageUserProfile.aspx",,"Edit preferences",0]
	//submenu2_2 = ["Reports","GraphicalReport.aspx",,"Reports",0] //FB 2593
	submenu2_2 = ["Templates","ManageTemplate.aspx",,"Manage Conference Templates",0]
    submenu2_3 = ["Groups","ManageGroup.aspx",,"manage group",0]
	

    
    submenu3_0   = ["organization",,,120,1,,style2,,"left",effect,,,,,,,,,,,,] // FB 2719
	submenu3_0_2 = ["organization",,,120,1,,style2_2,,"left",effect,,,,,,,,,,,,] // FB 2719
	
	submenu3_1  = ["Hardware","show-menu=mcu",,"Manage Hardware",0]
	submenu3_1_ = ["Hardware","",,,0]
	submenu3_2  = ["Locations","show-menu=loc",,"Manage locations",0]
	submenu3_2_ = ["Locations","",,,0]
	submenu3_3  = ["Users","show-menu=usr",,"Manage users",0]
	submenu3_3_ = ["Users","",,,0]
    submenu3_4  = ["Options","mainadministrator.aspx",,"Edit System Settings",0]
    submenu3_5  = ["Settings ","OrganisationSettings.aspx",,"Edit Organization Settings",0] // FB 2719
    submenu3_6 = ["Reports ", "ReportDetails.aspx", , "Manage Reports", 0]// FB 2593 Starts
    submenu3_7 = ["Audiovisual", "show-menu=av", , , 0]
    submenu3_7_ = ["Audiovisual", "", , , 0] // FB 2570
    submenu3_8 = ["Catering", "show-menu=catr", , , 0]
    submenu3_8_ = ["Catering", "", , , 0]
    submenu3_9 = ["Facility", "show-menu=hk", , , 0]
    submenu3_9_ = ["Facility", "", , , 0] // FB 2570 // FB 2593 End    
    
	submenu4_0   = ["site",,,100,1,,style2,,"left",effect,,,,,,,,,,,,] // FB 2719
	submenu4_0_2 = ["site",,,100,1,,style2_2,,"left",effect,,,,,,,,,,,,] // FB 2719
	submenu4_1  = ["Settings","SuperAdministrator.aspx",,"Edit system settings",0]
	
        submenu3_1_0 = ["mcu",,,100,1,"",style2,,"left",effect,,,,,,,,,,,,] // FB 2719
		submenu3_1_0_2 = ["mcu",,,100,1,"",style2_3,,"left",effect,,,,,,,,,,,,] // FB 2719
		submenu3_1_1 = ["Endpoints","EndpointList.aspx?t=",,"Setup Bridge",0]
		submenu3_1_2 = ["Diagnostics","EventLog.aspx",,"View hardware problem log",0]
		submenu3_1_3 = ["MCUs","ManageBridge.aspx",,"Edit Bridge Management",0]
		submenu3_1_4 = ["Audio Bridges", "ManageAudioAddOnBridges.aspx", , "Audio Bridge Management", 0] //FB 2023
		submenu3_1_5 = ["EM7", "EM7Dashboard.aspx", , "EM7", 0] //FB 2633
		
		submenu3_2_0 = ["loc",,,100,1,"",style2,,"left",effect,,,,,,,,,,,,] // FB 2719
		submenu3_2_0_2 = ["loc",,,100,1,"",style2_3,,"left",effect,,,,,,,,,,,,] // FB 2719
//		submenu3_2_1 = ["Rooms","dispatcher/admindispatcher.asp?cmd=ManageConfRoom",,"Edit or set up conference room",0]
        submenu3_2_1 = ["Rooms","manageroom.aspx?hf=&m=&pub=&d=&comp=&f=&frm=",,"Edit or set up conference room",0]
		submenu3_2_2 = ["Tiers","ManageTiers.aspx",,"Manage Tiers",0]


		submenu3_3_0 = ["usr", , , 150, 1, "", style2, , "left", effect, , , , , , , , , , , , ] // FB 2719
		submenu3_3_0_2 = ["usr", , , 150, 1, "", style2_3, , "left", effect, , , , , , , , , , , , ] // FB 2719
		submenu3_3_1 = ["Active Users","ManageUser.aspx?t=1",,"Edit, search or create a new user",0]
//		submenu3_3_2 = ["Bulk Tool","dispatcher/admindispatcher.asp?cmd=GetAllocation",,"Manage Bulk User",0]
        submenu3_3_2 = ["Bulk Tool","allocation.aspx",,"Manage Bulk User",0]
		submenu3_3_3 = ["Departments","ManageDepartment.aspx",,"Manage Department",0]
		submenu3_3_4 = ["Guests","ManageUser.aspx?t=2",,"Edit, search or create a new guest",0]
		if (sso_int)
			submenu3_3_5 = ["Restore User","ManageUser.aspx?t=3",,"Delete or Replaced a removed user",0]
		else
			submenu3_3_5 = ["Inactive Users","ManageUser.aspx?t=3",,"Delete or Replaced a removed user",0]
//		submenu3_3_6 = ["LDAP Directory Import","aspToaspNet.asp?tp=LDAPImport.aspx",,"Setup Multiple Users",0]
        submenu3_3_6 = ["LDAP Directory Import","LDAPImport.aspx",,"Setup Multiple Users",0]
//		submenu3_3_7 = ["Roles","dispatcher/admindispatcher.asp?cmd=GetUserRoles",,"Edit, search or create user roles",0]
        submenu3_3_7 = ["Roles","manageuserroles.aspx",,"Edit, search or create user roles",0]
		submenu3_3_8 = ["Templates","ManageUserTemplatesList.aspx",,"Edit, search or create user templates",0]

		// FB 2593 Starts
		submenu3_7_0 = ["av", , , 150, 1, "", style2, , "left", effect, , , , , , , , , , , , ] // FB 2719
		submenu3_7_0_2 = ["av", , , 150, 1, "", style2_3, , "left", effect, , , , , , , , , , , , ] // FB 2719
		submenu3_7_1 = ["Audiovisual Inventories","InventoryManagement.aspx?t=1",,"",0] // FB 2570
		submenu3_7_2 = ["Work Orders","ConferenceOrders.aspx?t=1",,"",0]


		submenu3_8_0 = ["catr", , , 150, 1, "", style2, , "left", effect, , , , , , , , , , , , ] // FB 2719
		submenu3_8_0_2 = ["catr", , , 150, 1, "", style2_3, , "left", effect, , , , , , , , , , , , ] // FB 2719
		submenu3_8_1 = ["Catering Menus", "InventoryManagement.aspx?t=2", , "", 0] // FB 2570
		submenu3_8_2 = ["Work Orders", "ConferenceOrders.aspx?t=2", , "", 0]

		submenu3_9_0 = ["hk",,,150,1,"",style2,,"left",effect,,,,,,,,,,,,] // FB 2719
		submenu3_9_0_2 = ["hk",,,150,1,"",style2_3,,"left",effect,,,,,,,,,,,,] // FB 2719
		submenu3_9_1 = ["Facility Services","InventoryManagement.aspx?t=3",,"",0] // FB 2570
		submenu3_9_2 = ["Work Orders","ConferenceOrders.aspx?t=3",,"",0]
		// FB 2593 End

	    cur_munu_no = 1;	cur_munu_str = "2_";
        if ( mms_int[cur_munu_no] ) {
	        menu = new Array();
	        menu = menu.concat( eval("submenu" + cur_munu_str + "0" + ((if_str=="2") ? "_2" : "")) );
	        for (i=1; i<=mms_num[cur_munu_no]; i++) {
		        menu = ( mms_int[cur_munu_no] & (1 << (mms_num[cur_munu_no]-i)) ) ? ( menu.concat(eval("submenu" + cur_munu_str + i)) ) : menu;
	        }
	        addmenu();
        }
        cur_munu_no = 2;	cur_munu_str = "3_";
        if ( mms_int[cur_munu_no] ) {
	        menu = new Array();
	        menu = menu.concat( eval("submenu" + cur_munu_str + "0" + ((if_str=="2") ? "_2" : "")) );
	        for (i=1; i<=mms_num[cur_munu_no]-3; i++) {
		        menu = ( mms_int[cur_munu_no] & (1 << (mms_num[cur_munu_no]-i)) ) ? ( menu.concat(eval("submenu" + cur_munu_str + i)) ) : menu;
	        }
	      for (i=mms_num[cur_munu_no]-2; i<=mms_num[cur_munu_no]; i++) {
			    if (rf_int & 1)
			        menu = ( mms_int[cur_munu_no] & (1 << (mms_num[cur_munu_no]-i)) ) ? ( menu.concat(eval("submenu" + cur_munu_str + i)) ) : menu;
	    		rf_int = rf_int/2;
		    }
	        addmenu();
        }
   	
		cur_munu_no = 3;	cur_munu_str = "3_1_";
		if ( mms_int[cur_munu_no] ) {
			menu = new Array();
			menu = menu.concat( eval("submenu" + cur_munu_str + "0" + ((if_str=="2") ? "_2" : "")) );
			for (i=1; i<=mms_num[cur_munu_no]; i++) { if (EnableAudioBridge == 0 && i == 4) continue ; //FB 2023
				  if (EnableEM7Opt == 0 && i == 5) continue; // FB 2633
				menu = ( mms_int[cur_munu_no] & (1 << (mms_num[cur_munu_no]-i)) ) ? ( menu.concat(eval("submenu" + cur_munu_str + i)) ) : menu;
			}
			addmenu();
		}
		cur_munu_no = 4;	cur_munu_str = "3_2_";
		if ( mms_int[cur_munu_no] ) {
			menu = new Array();
			menu = menu.concat( eval("submenu" + cur_munu_str + "0" + ((if_str=="2") ? "_2" : "")) );
			for (i=1; i<=mms_num[cur_munu_no]; i++) {
				menu = ( mms_int[cur_munu_no] & (1 << (mms_num[cur_munu_no]-i)) ) ? ( menu.concat(eval("submenu" + cur_munu_str + i)) ) : menu;
			}
			addmenu();
		}
		cur_munu_no = 5;	cur_munu_str = "3_3_";
		if ( mms_int[cur_munu_no] ) {
			menu = new Array();
			menu = menu.concat( eval("submenu" + cur_munu_str + "0" + ((if_str=="2") ? "_2" : "")) );
			for (i=1; i<=mms_num[cur_munu_no]; i++) {
				menu = ( mms_int[cur_munu_no] & (1 << (mms_num[cur_munu_no]-i)) ) ? ( menu.concat(eval("submenu" + cur_munu_str + i)) ) : menu;
			}
			addmenu();
		}

		cur_munu_no = 6; cur_munu_str = "3_7_"; //FB 2593
		if ( mms_int[cur_munu_no] ) {
			menu = new Array();
			menu = menu.concat( eval("submenu" + cur_munu_str + "0" + ((if_str=="2") ? "_2" : "")) );
			for (i=1; i<=mms_num[cur_munu_no]; i++) {
				menu = ( mms_int[cur_munu_no] & (1 << (mms_num[cur_munu_no]-i)) ) ? ( menu.concat(eval("submenu" + cur_munu_str + i)) ) : menu;
			}
			addmenu();
		}

		cur_munu_no = 7; cur_munu_str = "3_8_"; //FB 2593
		if ( mms_int[cur_munu_no] ) {
			menu = new Array();
			menu = menu.concat( eval("submenu" + cur_munu_str + "0" + ((if_str=="2") ? "_2" : "")) );
			for (i=1; i<=mms_num[cur_munu_no]; i++) {
				menu = ( mms_int[cur_munu_no] & (1 << (mms_num[cur_munu_no]-i)) ) ? ( menu.concat(eval("submenu" + cur_munu_str + i)) ) : menu;
			}
			addmenu();
		}

		cur_munu_no = 8; cur_munu_str = "3_9_"; //FB 2593
		if ( mms_int[cur_munu_no] ) {
			menu = new Array();
			menu = menu.concat( eval("submenu" + cur_munu_str + "0" + ((if_str=="2") ? "_2" : "")) );
			for (i=1; i<=mms_num[cur_munu_no]; i++) {
				menu = ( mms_int[cur_munu_no] & (1 << (mms_num[cur_munu_no]-i)) ) ? ( menu.concat(eval("submenu" + cur_munu_str + i)) ) : menu;
			}
			addmenu();
		}
		
		cur_munu_no = 9;	cur_munu_str = "4_";
		if ( mms_int[cur_munu_no] ) {
			menu = new Array();
			menu = menu.concat( eval("submenu" + cur_munu_str + "0" + ((if_str=="2") ? "_2" : "")) );
			for (i=1; i<=mms_num[cur_munu_no]; i++) {
				menu = ( mms_int[cur_munu_no] & (1 << (mms_num[cur_munu_no]-i)) ) ? ( menu.concat(eval("submenu" + cur_munu_str + i)) ) : menu;
			}
			addmenu();
		}
}
else
{
     menu_0 = ["mainmenu",5,250,110,,,style1,1,"center",effect,,1,,,,,,,,,,,]			// 24 FB 2719
    menu_0_2 = ["mainmenu",5,250,110,,,style1_2,1,"center",effect,,1,,,,,,,,,,,]		// 24 // FB 2050 FB 2719
    menu_1 = menu1Val //["Lobby&nbsp;","SettingSelect2.aspx",,"Go to the lobby page",0]
//    menu_2 = ["Calendar&nbsp;","dispatcher/admindispatcher.asp?cmd=ManageConfRoom&f=v&hf=",,"Go to Room Calendar",0]
    menu_2 = ["<div id='menuCalRed' ></div>", "personalCalendar.aspx?v=1&r=1&hf=&m=&pub=&d=&comp=&f=v", , "Go to Room Calendar", 0] // FB 2719
    //menu_3 = ["List&nbsp;","ConferenceList.aspx?t=2",,"Ongoing Conferences",0]
    menu_3 = menu5Val;
    //menu_4 = ["Search&nbsp;","dispatcher/gendispatcher.asp?cmd=GetSearchConference",,"Search Conference",0]
    menu_4 = ["<div id='menuSetRed' ></div>", "show-menu=settings", , "My Settings", 0] // FB 2719
    //menu_5 = ["New Conference","aspToaspNet.asp?tp=ConferenceSetup.aspx&t=n",,"Create a new conference",0]
    //menu_5 = menu5Val //["New Conference","show-menu=conference",,"Create a new conference",0]
    menu_5 = ["<div id='menuAdminRed' ></div>", "show-menu=organization", , "Organization", 0] // FB 2719
    menu_6 = ["<div id='menuSiteRed' ></div>", "show-menu=site", , "Site", 0] // FB 2719
    menu_7 = ["Schedule a<br/> Call ", "ExpressConference.aspx?t=n", , "Schedule a Call", 0] //FB 1779
    menu_8 = ["View / Edit<br/> Reservations", "ConferenceList.aspx?t=3", , "View Reservations", 0] //FB 1779

    menu = new Array();
    menu = (if_str=="2") ? menu.concat(menu_0_2) : menu.concat(menu_0);

    for (i=1; i<=mmm_num; i++) {
	    menu = ( mmm_int & (1 << (mmm_num-i)) ) ? ( menu.concat(eval("menu_" + i)) ) : menu;
    }
    addmenu();
	submenu1_0   = ["conference",94,,180,1,,style2,,"left",effect,,,,,,,,,,,,,]
	submenu1_0_2 = ["conference",94,,180,1,,style2_2,,"left",effect,,,,,,,,,,,,,]
    submenu1_1 = ["Future","ConferenceSetup.aspx?t=n&op=1",,"Create a Future Conference",0]
	submenu1_2 = ["Immediate","ConferenceSetup.aspx?t=n&op=2",,"Create an Immediate Conference",0]
	submenu1_3 = ["From Template","ManageTemplate.aspx",,"Create a conference from Template",0]

//    menu = new Array();
    //menu = menu.concat( eval("submenu1_0"));
//    menu = menu.concat( eval("submenu1_0_2"));
//    for (i=1; i<=3; i++) 
//        menu = menu.concat(eval("submenu1_" + i));

//    addmenu();
	submenu2_0   = ["settings",,,100,1,,style2,,"left",effect,,,,,,,,,,,,,] // FB 2719
	submenu2_0_2 = ["settings",,,100,1,,style2_2,,"left",effect,,,,,,,,,,,,,] // FB 2719
	submenu2_1 = ["Preferences","ManageUserProfile.aspx",,"Edit preferences",0]
	//submenu2_2 = ["Reports","GraphicalReport.aspx",,"Reports",0] //FB 2593
	submenu2_2 = ["Templates","ManageTemplate.aspx",,"Manage Conference Templates",0]
    submenu2_3 = ["Groups","ManageGroup.aspx",,"manage group",0]
	

    
    submenu3_0   = ["organization",,,120,1,,style2,,"left",effect,,,,,,,,,,,,,] // FB 2719
	submenu3_0_2 = ["organization",,,120,1,,style2_2,,"left",effect,,,,,,,,,,,,,] // FB 2719
	
	submenu3_1  = ["Hardware","show-menu=mcu",,"Manage Hardware",0]
	submenu3_1_ = ["Hardware","",,,0]
	submenu3_2  = ["Locations","show-menu=loc",,"Manage locations",0]
	submenu3_2_ = ["Locations","",,,0]
	submenu3_3  = ["Users","show-menu=usr",,"Manage users",0]
	submenu3_3_ = ["Users","",,,0]
    submenu3_4  = ["Options","mainadministrator.aspx",,"Edit System Settings",0]
    submenu3_5  = ["Settings ","OrganisationSettings.aspx",,"Edit Organization Settings",0]// FB 2719
	submenu3_6  = ["Reports ","ReportDetails.aspx",,"Manage Reports",0]// FB 2593 Starts
  	submenu3_7  = ["Audiovisual","show-menu=av",,,0]
    submenu3_7_ = ["Audiovisual","",,,0] // FB 2570
	submenu3_8  = ["Catering","show-menu=catr",,,0]
	submenu3_8_ = ["Catering","",,,0]
	submenu3_9  = ["Facility","show-menu=hk",,,0]
	submenu3_9_ = ["Facility","",,,0] // FB 2570 // FB 2593 End
	
	submenu4_0   = ["site",,,100,1,,style2,,"left",effect,,,,,,,,,,,,,] // FB 2719
	submenu4_0_2 = ["site",,,100,1,,style2_2,,"left",effect,,,,,,,,,,,,,] // FB 2719
	submenu4_1  = ["Settings","SuperAdministrator.aspx",,"Edit system settings",0]
	
        submenu3_1_0 = ["mcu",,,100,1,"",style2,,"left",effect,,,,,,,,,,,,,] // FB 2719
		submenu3_1_0_2 = ["mcu",,,100,1,"",style2_3,,"left",effect,,,,,,,,,,,,,] // FB 2719
		submenu3_1_1 = ["Endpoints", "EndpointList.aspx?t=", , "Setup Bridge", 0]
		submenu3_1_2 = ["Diagnostics","EventLog.aspx",,"View hardware problem log",0]
		submenu3_1_3 = ["MCUs","ManageBridge.aspx",,"Edit Bridge Management",0]
		submenu3_1_4 = ["Audio Bridges", "ManageAudioAddOnBridges.aspx", , "Audio Bridge Management", 0] //FB 2023
		submenu3_1_5 = ["EM7", "EM7Dashboard.aspx", , "EM7", 0] //FB 2633

		submenu3_2_0 = ["loc",,,100,1,"",style2,,"left",effect,,,,,,,,,,,,,] // FB 2719
		submenu3_2_0_2 = ["loc",,,100,1,"",style2_3,,"left",effect,,,,,,,,,,,,,] // FB 2719
//		submenu3_2_1 = ["Rooms","dispatcher/admindispatcher.asp?cmd=ManageConfRoom",,"Edit or set up conference room",0]
        submenu3_2_1 = ["Rooms","manageroom.aspx?hf=&m=&pub=&d=&comp=&f=&frm=",,"Edit or set up conference room",0]
		submenu3_2_2 = ["Tiers","ManageTiers.aspx",,"Manage Tiers",0]

        
		submenu3_3_0 = ["usr",,,150,1,"",style2,,"left",effect,,,,,,,,,,,,,] // FB 2719
		submenu3_3_0_2 = ["usr",,,150,1,"",style2_3,,"left",effect,,,,,,,,,,,,,] // FB 2719
		submenu3_3_1 = ["Active Users","ManageUser.aspx?t=1",,"Edit, search or create a new user",0]
//		submenu3_3_2 = ["Bulk Tool","dispatcher/admindispatcher.asp?cmd=GetAllocation",,"Manage Bulk User",0]
        submenu3_3_2 = ["Bulk Tool","allocation.aspx",,"Manage Bulk User",0]
		submenu3_3_3 = ["Departments","ManageDepartment.aspx",,"Manage Department",0]
		submenu3_3_4 = ["Guests","ManageUser.aspx?t=2",,"Edit, search or create a new guest",0]
		if (sso_int)
			submenu3_3_5 = ["Restore User","ManageUser.aspx?t=3",,"Delete or Replaced a removed user",0]
		else
			submenu3_3_5 = ["Inactive Users","ManageUser.aspx?t=3",,"Delete or Replaced a removed user",0]
//		submenu3_3_6 = ["LDAP Directory Import","aspToaspNet.asp?tp=LDAPImport.aspx",,"Setup Multiple Users",0]
        submenu3_3_6 = ["LDAP Directory Import","LDAPImport.aspx",,"Setup Multiple Users",0]
//		submenu3_3_7 = ["Roles","dispatcher/admindispatcher.asp?cmd=GetUserRoles",,"Edit, search or create user roles",0]
        submenu3_3_7 = ["Roles","manageuserroles.aspx",,"Edit, search or create user roles",0]
		submenu3_3_8 = ["Templates","ManageUserTemplatesList.aspx",,"Edit, search or create user templates",0]
		// FB 2593 Starts
		submenu3_7_0 = ["av",,,150,1,"",style2,,"left",effect,,,,,,,,,,,,,] // FB 2719
		submenu3_7_0_2 = ["av",,,150,1,"",style2_3,,"left",effect,,,,,,,,,,,,,] // FB 2719
		submenu3_7_1 = ["Audiovisual Inventories","InventoryManagement.aspx?t=1",,"",0] // FB 2570
		submenu3_7_2 = ["Work Orders","ConferenceOrders.aspx?t=1",,"",0]

		
		submenu3_8_0 = ["catr",,,150,1,"",style2,,"left",effect,,,,,,,,,,,,,] // FB 2719
		submenu3_8_0_2 = ["catr",,,150,1,"",style2_3,,"left",effect,,,,,,,,,,,,,] // FB 2719
		submenu3_8_1 = ["Catering Menus","InventoryManagement.aspx?t=2",,"",0] // FB 2570
        submenu3_8_2 = ["Work Orders","ConferenceOrders.aspx?t=2",,"",0]

		submenu3_9_0 = ["hk",,,150,1,"",style2,,"left",effect,,,,,,,,,,,,,] // FB 2719
		submenu3_9_0_2 = ["hk",,,150,1,"",style2_3,,"left",effect,,,,,,,,,,,,,] // FB 2719
		submenu3_9_1 = ["Facility Services","InventoryManagement.aspx?t=3",,"",0] // FB 2570
		submenu3_9_2 = ["Work Orders","ConferenceOrders.aspx?t=3",,"",0]
		// FB 2593 End

	    cur_munu_no = 1;	cur_munu_str = "2_";
        if ( mms_int[cur_munu_no] ) {
	        menu = new Array();
	        menu = menu.concat( eval("submenu" + cur_munu_str + "0" + ((if_str=="2") ? "_2" : "")) );
	        for (i=1; i<=mms_num[cur_munu_no]; i++) {
		        menu = ( mms_int[cur_munu_no] & (1 << (mms_num[cur_munu_no]-i)) ) ? ( menu.concat(eval("submenu" + cur_munu_str + i)) ) : menu;
	        }
	        addmenu();
	    }
	    
        cur_munu_no = 2;	cur_munu_str = "3_";
        if ( mms_int[cur_munu_no] ) {
	        menu = new Array();
	        menu = menu.concat( eval("submenu" + cur_munu_str + "0" + ((if_str=="2") ? "_2" : "")) );
	        for (i=1; i<=mms_num[cur_munu_no]-3; i++) {
		        menu = ( mms_int[cur_munu_no] & (1 << (mms_num[cur_munu_no]-i)) ) ? ( menu.concat(eval("submenu" + cur_munu_str + i)) ) : menu;
	        }
	      for (i=mms_num[cur_munu_no]-2; i<=mms_num[cur_munu_no]; i++) {
			    if (rf_int & 1)
			        menu = ( mms_int[cur_munu_no] & (1 << (mms_num[cur_munu_no]-i)) ) ? ( menu.concat(eval("submenu" + cur_munu_str + i)) ) : menu;
	    		rf_int = rf_int/2;
		    }
	        addmenu();
        }
   	
		cur_munu_no = 3;	cur_munu_str = "3_1_";
		if ( mms_int[cur_munu_no] ) {
			menu = new Array();
			menu = menu.concat( eval("submenu" + cur_munu_str + "0" + ((if_str=="2") ? "_2" : "")) );
			for (i=1; i<=mms_num[cur_munu_no]; i++) { if (EnableAudioBridge == 0 && i == 4) continue ; //FB 2023
				 if (EnableEM7Opt == 0 && i == 5) continue; // FB 2633
				menu = ( mms_int[cur_munu_no] & (1 << (mms_num[cur_munu_no]-i)) ) ? ( menu.concat(eval("submenu" + cur_munu_str + i)) ) : menu;
			}
			addmenu();
		}
		cur_munu_no = 4;	cur_munu_str = "3_2_";
		if ( mms_int[cur_munu_no] ) {
			menu = new Array();
			menu = menu.concat( eval("submenu" + cur_munu_str + "0" + ((if_str=="2") ? "_2" : "")) );
			for (i=1; i<=mms_num[cur_munu_no]; i++) {
				menu = ( mms_int[cur_munu_no] & (1 << (mms_num[cur_munu_no]-i)) ) ? ( menu.concat(eval("submenu" + cur_munu_str + i)) ) : menu;
			}
			addmenu();
		}
		cur_munu_no = 5;	cur_munu_str = "3_3_";
		if ( mms_int[cur_munu_no] ) {
			menu = new Array();
			menu = menu.concat( eval("submenu" + cur_munu_str + "0" + ((if_str=="2") ? "_2" : "")) );
			for (i=1; i<=mms_num[cur_munu_no]; i++) {
				menu = ( mms_int[cur_munu_no] & (1 << (mms_num[cur_munu_no]-i)) ) ? ( menu.concat(eval("submenu" + cur_munu_str + i)) ) : menu;
			}
			addmenu();
		}

		cur_munu_no = 6;	cur_munu_str = "3_7_"; //FB 2593
		if ( mms_int[cur_munu_no] ) {
			menu = new Array();
			menu = menu.concat( eval("submenu" + cur_munu_str + "0" + ((if_str=="2") ? "_2" : "")) );
			for (i=1; i<=mms_num[cur_munu_no]; i++) {
				menu = ( mms_int[cur_munu_no] & (1 << (mms_num[cur_munu_no]-i)) ) ? ( menu.concat(eval("submenu" + cur_munu_str + i)) ) : menu;
			}
			addmenu();
		}
		
		cur_munu_no = 7;	cur_munu_str = "3_8_"; //FB 2593
		if ( mms_int[cur_munu_no] ) {
			menu = new Array();
			menu = menu.concat( eval("submenu" + cur_munu_str + "0" + ((if_str=="2") ? "_2" : "")) );
			for (i=1; i<=mms_num[cur_munu_no]; i++) {
				menu = ( mms_int[cur_munu_no] & (1 << (mms_num[cur_munu_no]-i)) ) ? ( menu.concat(eval("submenu" + cur_munu_str + i)) ) : menu;
			}
			addmenu();
		}

		cur_munu_no = 8;	cur_munu_str = "3_9_"; //FB 2593
		if ( mms_int[cur_munu_no] ) {
			menu = new Array();
			menu = menu.concat( eval("submenu" + cur_munu_str + "0" + ((if_str=="2") ? "_2" : "")) );
			for (i=1; i<=mms_num[cur_munu_no]; i++) {
				menu = ( mms_int[cur_munu_no] & (1 << (mms_num[cur_munu_no]-i)) ) ? ( menu.concat(eval("submenu" + cur_munu_str + i)) ) : menu;
			}
			addmenu();
		}
		
		cur_munu_no = 9;	cur_munu_str = "4_";
		if ( mms_int[cur_munu_no] ) {
			menu = new Array();
			menu = menu.concat( eval("submenu" + cur_munu_str + "0" + ((if_str=="2") ? "_2" : "")) );
			for (i=1; i<=mms_num[cur_munu_no]; i++) {
				menu = ( mms_int[cur_munu_no] & (1 << (mms_num[cur_munu_no]-i)) ) ? ( menu.concat(eval("submenu" + cur_munu_str + i)) ) : menu;
			}
			addmenu();
		}
}
dumpmenus()

