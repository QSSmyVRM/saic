/*ZD 100147 Start*/
/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
/*ZD 100147 End*/
menunum = 0;
menus=new Array();
_d=document; 



function addmenu()
{
	menunum++;
	menus[menunum]=menu;
}

function dumpmenus()
{
	mt = "<script language=javascript>";
	for (a=1; a<menus.length; a++) {
		mt += " menu" + a + "=menus[" + a + "];"
	}
	mt += "<\/script>";
	_d.write(mt)
}

if(navigator.appVersion.indexOf("MSIE 6.0")>0)
{
	effect = "Fade(duration=0.2); Alpha(style=0,opacity=88); Shadow(color='#ffffff', Direction=135, Strength=5)"
}
else
{
	effect = "Shadow(color='#777777', Direction=135, Strength=5)" 
}


timegap=500;		followspeed=5;		followrate=40;
suboffset_top=10;	suboffset_left=10;	

/*style1=[
"white",			// Mouse Off Font 
"Purple",			// Mouse Off Background 
"Purple",			// Mouse On Font 
"#FFF7CE",			// Mouse On Background 
"lightblue",		// Menu Border 
12,					// Font Size in pixels
"normal",			// Font Style 
"bold",				// Font Weight
"Verdana, Arial",	// Font Name
4,					// Menu Item Padding
,					// Sub Menu Image 
,					// Border & Separator bar
"66ffff",			// High 
"000099",			// Low 
"Purple",			// Current Page Item Font Color
"pink",				// Current Page Item Background Color
,					// Top Bar image 
"ffffff",			// Menu Header Font Color 
"000099",			// Menu Header Background Color
]*/

style1=["white","Purple","Purple","#FFF7CE","lightblue",12,"normal","bold","Verdana, Arial",4,,,"66ffff","000099","Purple","pink",,"ffffff","000099",]

//style1_2=["#ff6347","","#3399FF","#FFF7CE","lightblue",12,"normal","bold","Verdana, Arial",4,,,"66ffff","000099","Purple","pink",,"ffffff","000099",]

style1_2=["#ff6347","#ffffff","#3399FF","#FFF7CE","lightblue",12,"normal","bold","Verdana, Arial",4,"","","#66ffff","#000099","Purple","pink","","#ffffff","#000099",]

style2=["black","#FFF7CE","Purple","#FFF7CE","lightblue",12,"normal","bold","Verdana, Arial",4,"image/menuarrow.gif",,"66ffff","000099","Purple","pink",,"ffffff","000099",]

//style2_2=["white","#9acd32","#3399FF","#FFF7CE","lightblue",12,"normal","bold","Verdana, Arial",4,"image/menuarrow.gif",,"66ffff","000099","Purple","pink",,"ffffff","000099",]

style2_2=["white","#9acd32","#3399FF","#FFF7CE","lightblue",12,"normal","bold","Verdana, Arial",4,"image/menuarrow.gif","","66ffff","000099","Purple","pink","","#ffffff","#000099",]


if(navigator.appVersion.indexOf("MSIE 6.0")>0)
{

menu_0 = ["mainmenu",72,3,120,,,style1,1,"center",effect,,1,,,,,,,,,,]			// 24
menu_0_2 = ["mainmenu",72,3,120,,,style1_2,1,"center",effect,,1,,,,,,,,,,]		// 24
menu_1 = ["Lobby&nbsp;","",,"Go to the lobby page",0]
menu_2 = ["Account&nbsp;","show-menu=account",,"Back to the home page",0]
menu_3 = ["Conference","show-menu=conference",,"",0]
menu_4 = ["Utilities","show-menu=utility",,"",0]
menu_5 = ["Admin Tools","show-menu=manager",,"",0]

menu = new Array();
menu = (if_str=="2") ? menu.concat(menu_0_2) : menu.concat(menu_0);

for (i=1; i<=mmm_num; i++) {
	menu = ( mmm_int & (1 << (mmm_num-i)) ) ? ( menu.concat(eval("menu_" + i)) ) : menu;
}
addmenu();
	

	submenu2_0 = ["account",96,,210,1,"",style2,,"left",effect,,,,,,,,,,,,]
	submenu2_0_2 = ["account",96,,210,1,"",style2_2,,"left",effect,,,,,,,,,,,,]
	submenu2_1 = ["Search Conference","",,"search conference",0]
	submenu2_2 = ["My Immediate Conferences","",,"view all immediate conference",0]
	submenu2_3 = ["My Future Conferences","",,"view all future conference",0]
	submenu2_4 = ["Public Conferences","",,"view all public conference",0]
	submenu2_5 = ["My Pending Conferences","",,"view all pending conference",0]
	submenu2_6 = ["My Approval Conferences","",,"view all approval conference",0]
	submenu2_7 = ["All Room Conferences","",,"view all conference based on room",0]
	submenu2_7_ = []

	cur_munu_no = 2;	cur_munu_str = "2_";
	if ( mms_int[cur_munu_no] ) {
		menu = new Array();
		menu = menu.concat( eval("submenu" + cur_munu_str + "0" + ((if_str=="2") ? "_2" : "")) );
		for (i=1; i<=6; i++) {
			menu = ( mms_int[cur_munu_no] & (1 << (mms_num[cur_munu_no]-i)) ) ? ( menu.concat(eval("submenu" + cur_munu_str + i)) ) : menu;
		}
		i=7;
		menu = ( mms_int[cur_munu_no] & (1 << (mms_num[cur_munu_no]-i)) ) ? ( menu.concat(eval("submenu" + cur_munu_str + i + ( (rf_int & 1) ?  "" : "_" ) )) ) : menu;
		addmenu();
	}

	submenu3_0 = ["conference",96,,150,1,"",style2,,"left",effect,,,,,,,,,,,,]
	submenu3_0_2 = ["conference",96,,150,1,"",style2_2,,"left",effect,,,,,,,,,,,,]
	submenu3_1 = ["Create Conference","show-menu=setconf",,,0]
	submenu3_1_ = ["Create Conference","",,,0]
	submenu3_2 = ["Manage Conference","show-menu=magconf",,,0]
	submenu3_2_ = ["Manage Conference","",,,0]
	submenu3_3 = ["Access Conference","show-menu=acsconf",,,0]
	submenu3_3_ = ["Access Conference","",,,0]

	cur_munu_no = 3;	cur_munu_str = "3_";
	if ( mms_int[cur_munu_no] ) {
		submenuval = new Array(1,mms_int[4],mms_int[5],mms_int[6])

		menu = new Array();
		menu = menu.concat( eval("submenu" + cur_munu_str + "0" + ((if_str=="2") ? "_2" : "")) );
		for (i=1; i<=mms_num[cur_munu_no]; i++) {
			menu = ( mms_int[cur_munu_no] & (1 << (mms_num[cur_munu_no]-i)) ) ? ( menu.concat(eval("submenu" + cur_munu_str + i + ((submenuval[i] == 0) ? "_" : "") )) ) : menu;
		}
		addmenu();
	}

		submenu3_1_0 = ["setconf",,,220,1,"",style2,,"left",effect,,,,,,,,,,,,]
		submenu3_1_0_2 = ["setconf",,,220,1,"",style2_2,,"left",effect,,,,,,,,,,,,]
		submenu3_1_1 = ["Use Template","",,"Set up a conference using template",0]
		submenu3_1_2 = ["Room Conference","",,"Set up a conference using conference room",0]
		submenu3_1_3 = ["Immediate Video Conference","",,"Set up a immediate conference",0]
		submenu3_1_4 = ["Future Video Conference","",,"Set up a future conference",0]
		submenu3_1_5 = ["Point-to-Point Conference","",,"Set up a connect2 conference",0]
		submenu3_1_5_ = []
		submenu3_1_6 = ["Audio-only Conference","",,"Set up a connect2 conference",0]

		cur_munu_no = 4;	cur_munu_str = "3_1_";
		if ( mms_int[cur_munu_no] ) {
			menu = new Array();
			menu = menu.concat( eval("submenu" + cur_munu_str + "0" + ((if_str=="2") ? "_2" : "")) );
			for (i=1; i<=4; i++) {
				menu = ( mms_int[cur_munu_no] & (1 << (mms_num[cur_munu_no]-i)) ) ? ( menu.concat(eval("submenu" + cur_munu_str + i)) ) : menu;
			}
			i=5;
			menu = ( mms_int[cur_munu_no] & (1 << (mms_num[cur_munu_no]-i)) ) ? ( menu.concat(eval("submenu" + cur_munu_str + i + ( (p2p_int & 1) ?  "" : "_" ) )) ) : menu;
			i=6;
			menu = ( mms_int[cur_munu_no] & (1 << (mms_num[cur_munu_no]-i)) ) ? ( menu.concat(eval("submenu" + cur_munu_str + i)) ) : menu;
			
			addmenu();
		}

		submenu3_2_0 = ["magconf",,,170,1,"",style2,,"left",effect,,,,,,,,,,,,]
		submenu3_2_0_2 = ["magconf",,,170,1,"",style2_2,,"left",effect,,,,,,,,,,,,]
		submenu3_2_1 = ["Edit Conference","",,"Edit or copy a conference",0]
		submenu3_2_2 = ["Cancel Conference","",,"Delete a conference",0]

		cur_munu_no = 5;	cur_munu_str = "3_2_";
		if ( mms_int[cur_munu_no] ) {
			menu = new Array();
			menu = menu.concat( eval("submenu" + cur_munu_str + "0" + ((if_str=="2") ? "_2" : "")) );
			for (i=1; i<=mms_num[cur_munu_no]; i++) {
				menu = ( mms_int[cur_munu_no] & (1 << (mms_num[cur_munu_no]-i)) ) ? ( menu.concat(eval("submenu" + cur_munu_str + i)) ) : menu;
			}
			addmenu();
		}


		submenu3_3_0 = ["acsconf",,,170,1,"",style2,,"left",effect,,,,,,,,,,,,]
		submenu3_3_0_2 = ["acsconf",,,170,1,"",style2_2,,"left",effect,,,,,,,,,,,,]
		submenu3_3_1 = ["Confirm Reservation","",,"Confirm a future conference",0]
		submenu3_3_2 = ["Check-in Conference","",,"Login a ongoing conference",0]

		cur_munu_no = 6;	cur_munu_str = "3_3_";
		if ( mms_int[cur_munu_no] ) {
			menu = new Array();
			menu = menu.concat( eval("submenu" + cur_munu_str + "0" + ((if_str=="2") ? "_2" : "")) );
			for (i=1; i<=mms_num[cur_munu_no]; i++) {
				menu = ( mms_int[cur_munu_no] & (1 << (mms_num[cur_munu_no]-i)) ) ? ( menu.concat(eval("submenu" + cur_munu_str + i)) ) : menu;
			}
			addmenu();
		}
	

	submenu4_0 = ["utility",96,,180,1,"",style2,,"left",effect,,,,,,,,,,,,]
	submenu4_0_2 = ["utility",96,,180,1,"",style2_2,,"left",effect,,,,,,,,,,,,]
	submenu4_1 = ["Groups","",,"manage group",0]
	submenu4_2 = ["Templates","",,"manage template for setting conference",0]
	submenu4_3 = ["Template Preferences","",,"Select Instant Template Priorities",0]
	submenu4_4 = ["User Preferences","",,"Edit preferences",0]
	submenu4_5 = ["Reports","",,"Show account information",0]
		
	cur_munu_no = 7;	cur_munu_str = "4_";
	if ( mms_int[cur_munu_no] ) {
		submenuval = new Array(1,1,1,1,1,1)

		menu = new Array();
		menu = menu.concat( eval("submenu" + cur_munu_str + "0" + ((if_str=="2") ? "_2" : "")) );
		for (i=1; i<=mms_num[cur_munu_no]; i++) {
			menu = ( mms_int[cur_munu_no] & (1 << (mms_num[cur_munu_no]-i)) ) ? ( menu.concat(eval("submenu" + cur_munu_str + i + ((submenuval[i] == 0) ? "_" : "") )) ) : menu;
		}
		addmenu();
	}


	submenu5_0   = ["manager",96,,180,1,,style2,,"left",effect,,,,,,,,,,,,]
	submenu5_0_2 = ["manager",96,,180,1,,style2_2,,"left",effect,,,,,,,,,,,,]
	submenu5_1  = ["Users","show-menu=usr",,"Manage users",0]
	submenu5_1_ = ["Users","",,,0]
	submenu5_2  = ["Locations","show-menu=loc",,"Manage locations",0]
	submenu5_2_ = ["Locations","",,,0]
	submenu5_3  = ["Hardware","show-menu=mcu",,"Manage Hardware",0]
	submenu5_3_ = ["Hardware","",,,0]
	submenu5_4  = ["Settings","",,"Edit System Settings",0]
	submenu5_5  = ["Advanced Settings","",,"Edit advanced system settings",0]
	submenu5_6  = ["Reports","show-menu=rpt",,,0]
	submenu5_6_ = ["Reports","",,,0]
	submenu5_7 = ["Room Assistant","show-menu=rmas",,,0]
	submenu5_7_ = ["Room Assistant","",,,0]
	submenu5_7__ = []
	submenu5_8  = ["Caterer","show-menu=catr",,,0]
	submenu5_8_ = ["Caterer","",,,0]
	submenu5_8__ = []
	
	cur_munu_no = 8;	cur_munu_str = "5_";
	if ( mms_int[cur_munu_no] ) {
		submenuval = new Array(1, mms_int[9],mms_int[10],mms_int[11],1,1,mms_int[12],mms_int[13],mms_int[14])
		
		menu = new Array();
		menu = menu.concat( eval("submenu" + cur_munu_str + "0" + ((if_str=="2") ? "_2" : "")) );
		for (i=1; i<=6; i++) {
			menu = ( mms_int[cur_munu_no] & (1 << (mms_num[cur_munu_no]-i)) ) ? ( menu.concat(eval("submenu" + cur_munu_str + i + ((submenuval[i] == 0) ? "_" : "") )) ) : menu;
		}
		for (i=7; i<=8; i++) { 
			menu = ( mms_int[cur_munu_no] & (1 << (mms_num[cur_munu_no]-i)) ) ? ( menu.concat(eval("submenu" + cur_munu_str + i + ( ((rf_int>>(mms_num[cur_munu_no]-i)) & 1) ?  ((submenuval[i] == 0) ? "_" : "") : "__" ) )) ) : menu;
		}		

		addmenu();
	}


		submenu5_1_0 = ["usr",,,200,1,"",style2,,"left",effect,,,,,,,,,,,,]
		submenu5_1_0_2 = ["usr",,,200,1,"",style2_2,,"left",effect,,,,,,,,,,,,]
		if (al_int=="2")
			submenu5_1_1 = ["Active Users","",,"Edit, search or create a new user",0]
		else
			submenu5_1_1 = ["Edit User","",,"Edit, search or create a new user",0]
		submenu5_1_2 = ["Guests","",,"Edit, search or create a new guest",0]
		if (sso_int)
			submenu5_1_3 = ["Restore User","",,"Delete or Replaced a removed user",0]
		else
			submenu5_1_3 = ["Inactive Users","",,"Delete or Replaced a removed user",0]
		submenu5_1_4 = ["Active Directory Import","",,"Setup Multiple Users",0]
		submenu5_1_5 = ["Templates","",,"Edit, search or create user templates",0]
		submenu5_1_6 = ["Roles","",,"Edit, search or create user roles",0]
		submenu5_1_7 = ["Departments","",,"Manage Department",0]
		submenu5_1_8 = ["Bulk Tool","",,"Manage Bulk User",0]

		cur_munu_no = 9;	cur_munu_str = "5_1_";
		if ( mms_int[cur_munu_no] ) {
			menu = new Array();
			menu = menu.concat( eval("submenu" + cur_munu_str + "0" + ((if_str=="2") ? "_2" : "")) );
			for (i=1; i<=mms_num[cur_munu_no]; i++) {
				menu = ( mms_int[cur_munu_no] & (1 << (mms_num[cur_munu_no]-i)) ) ? ( menu.concat(eval("submenu" + cur_munu_str + i)) ) : menu;
			}
			addmenu();
		}


		submenu5_2_0 = ["loc",,,210,1,"",style2,,"left",effect,,,,,,,,,,,,]
		submenu5_2_0_2 = ["loc",,,210,1,"",style2_2,,"left",effect,,,,,,,,,,,,]
		submenu5_2_1 = ["Rooms","",,"Edit or set up conference room",0]
		submenu5_2_2 = ["Tiers","",,"Manage room location",0]

		cur_munu_no = 10;	cur_munu_str = "5_2_";
		if ( mms_int[cur_munu_no] ) {
			menu = new Array();
			menu = menu.concat( eval("submenu" + cur_munu_str + "0" + ((if_str=="2") ? "_2" : "")) );
			for (i=1; i<=mms_num[cur_munu_no]; i++) {
				menu = ( mms_int[cur_munu_no] & (1 << (mms_num[cur_munu_no]-i)) ) ? ( menu.concat(eval("submenu" + cur_munu_str + i)) ) : menu;
			}
			addmenu();
		}


		submenu5_3_0 = ["mcu",,,210,1,"",style2,,"left",effect,,,,,,,,,,,,]
		submenu5_3_0_2 = ["mcu",,,210,1,"",style2_2,,"left",effect,,,,,,,,,,,,]
		submenu5_3_1 = ["Endpoints","",,"Setup Bridge",0]
		submenu5_3_2 = ["MCU's","",,"Edit Bridge Management",0]
		submenu5_3_3 = ["Event Logs","",,"View hardware problem log",0]

		cur_munu_no = 11;	cur_munu_str = "5_3_";
		if ( mms_int[cur_munu_no] ) {
			menu = new Array();
			menu = menu.concat( eval("submenu" + cur_munu_str + "0" + ((if_str=="2") ? "_2" : "")) );
			for (i=1; i<=mms_num[cur_munu_no]; i++) {
				menu = ( mms_int[cur_munu_no] & (1 << (mms_num[cur_munu_no]-i)) ) ? ( menu.concat(eval("submenu" + cur_munu_str + i)) ) : menu;
			}
			addmenu();
		}


		submenu5_6_0 = ["rpt",,,220,1,"",style2,,"left",effect,,,,,,,,,,,,]
		submenu5_6_0_2 = ["rpt",,,220,1,"",style2_2,,"left",effect,,,,,,,,,,,,]
		submenu5_6_1 = ["User Reports","",,"Show user report",0]
		submenu5_6_2 = ["Room Reports","",,"Show room report",0]
		submenu5_6_3 = ["Conference Reports","",,"Show conference report",0]

		cur_munu_no = 12;	cur_munu_str = "5_6_";
		if ( mms_int[cur_munu_no] ) {
			menu = new Array();
			menu = menu.concat( eval("submenu" + cur_munu_str + "0" + ((if_str=="2") ? "_2" : "")) );
			for (i=1; i<=mms_num[cur_munu_no]; i++) {
				menu = ( mms_int[cur_munu_no] & (1 << (mms_num[cur_munu_no]-i)) ) ? ( menu.concat(eval("submenu" + cur_munu_str + i)) ) : menu;
			}
			addmenu();
		}
		
	
		submenu5_7_0 = ["rmas",,,220,1,"",style2,,"left",effect,,,,,,,,,,,,]
		submenu5_7_0_2 = ["rmas",,,220,1,"",style2_2,,"left",effect,,,,,,,,,,,,]
		submenu5_7_1 = ["Inventory","",,"",0]
		submenu5_7_2 = ["Conference Orders","",,"",0]
		submenu5_7_3 = ["Search Orders","",,"",0]

		cur_munu_no = 13;	cur_munu_str = "5_7_";
		if ( mms_int[cur_munu_no] ) {
			menu = new Array();
			menu = menu.concat( eval("submenu" + cur_munu_str + "0" + ((if_str=="2") ? "_2" : "")) );
			for (i=1; i<=mms_num[cur_munu_no]; i++) {
				menu = ( mms_int[cur_munu_no] & (1 << (mms_num[cur_munu_no]-i)) ) ? ( menu.concat(eval("submenu" + cur_munu_str + i)) ) : menu;
			}
			addmenu();
		}
		
		
		
		submenu5_9_0 = ["catr",,,220,1,"",style2,,"left",effect,,,,,,,,,,,,]
		submenu5_9_0_2 = ["catr",,,220,1,"",style2_2,,"left",effect,,,,,,,,,,,,]
		submenu5_9_1 = ["Menus","",,"",0]
		submenu5_9_2 = ["Conference Orders","",,"",0]
		submenu5_9_3 = ["Search Orders","",,"",0]
		cur_munu_no = 14;	cur_munu_str = "5_9_";
		if ( mms_int[cur_munu_no] ) {
			menu = new Array();
			menu = menu.concat( eval("submenu" + cur_munu_str + "0" + ((if_str=="2") ? "_2" : "")) );
			for (i=1; i<=mms_num[cur_munu_no]; i++) {
				menu = ( mms_int[cur_munu_no] & (1 << (mms_num[cur_munu_no]-i)) ) ? ( menu.concat(eval("submenu" + cur_munu_str + i)) ) : menu;
			}
			addmenu();
		}

}
else
{


menu_0 = ["mainmenu",72,3,120,,,style1,1,"center",effect,,1,,,,,,,,,,,]			// 24
menu_0_2 = ["mainmenu",72,3,120,,,style1_2,1,"center",effect,,1,,,,,,,,,,,]		// 24
menu_1 = ["Lobby&nbsp;","",,"Go to the lobby page",0]
menu_2 = ["Account&nbsp;","show-menu=account",,"Back to the home page",0]
menu_3 = ["Conference","show-menu=conference",,"",0]
menu_4 = ["Utilities","show-menu=utility",,"",0]
menu_5 = ["Admin Tools","show-menu=manager",,"",0]

menu = new Array();
menu = (if_str=="2") ? menu.concat(menu_0_2) : menu.concat(menu_0);

for (i=1; i<=mmm_num; i++) {
	menu = ( mmm_int & (1 << (mmm_num-i)) ) ? ( menu.concat(eval("menu_" + i)) ) : menu;
}
addmenu();
	

	submenu2_0 = ["account",96,,210,1,"",style2,,"left",effect,,,,,,,,,,,,,]
	submenu2_0_2 = ["account",96,,210,1,"",style2_2,,"left",effect,,,,,,,,,,,,,]
	submenu2_1 = ["Search Conference","",,"search conference",0]
	submenu2_2 = ["My Immediate Conferences","",,"view all immediate conference",0]
	submenu2_3 = ["My Future Conferences","",,"view all future conference",0]
	submenu2_4 = ["Public Conferences","",,"view all public conference",0]
	submenu2_5 = ["My Pending Conferences","",,"view all pending conference",0]
	submenu2_6 = ["My Approval Conferences","",,"view all approval conference",0]
	submenu2_7 = ["All Room Conferences","",,"view all conference based on room",0]
	submenu2_7_ = []

	cur_munu_no = 2;	cur_munu_str = "2_";
	if ( mms_int[cur_munu_no] ) {
		menu = new Array();
		menu = menu.concat( eval("submenu" + cur_munu_str + "0" + ((if_str=="2") ? "_2" : "")) );
		for (i=1; i<=6; i++) {
			menu = ( mms_int[cur_munu_no] & (1 << (mms_num[cur_munu_no]-i)) ) ? ( menu.concat(eval("submenu" + cur_munu_str + i)) ) : menu;
		}
		i=7;
		menu = ( mms_int[cur_munu_no] & (1 << (mms_num[cur_munu_no]-i)) ) ? ( menu.concat(eval("submenu" + cur_munu_str + i + ( (rf_int & 1) ?  "" : "_" ) )) ) : menu;
		addmenu();
	}

	submenu3_0 = ["conference",96,,150,1,"",style2,,"left",effect,,,,,,,,,,,,,]
	submenu3_0_2 = ["conference",96,,150,1,"",style2_2,,"left",effect,,,,,,,,,,,,,]
	submenu3_1 = ["Create Conference","show-menu=setconf",,,0]
	submenu3_1_ = ["Create Conference","",,,0]
	submenu3_2 = ["Manage Conference","show-menu=magconf",,,0]
	submenu3_2_ = ["Manage Conference","",,,0]
	submenu3_3 = ["Access Conference","show-menu=acsconf",,,0]
	submenu3_3_ = ["Access Conference","",,,0]

	cur_munu_no = 3;	cur_munu_str = "3_";
	if ( mms_int[cur_munu_no] ) {
		submenuval = new Array(1,mms_int[4],mms_int[5],mms_int[6])

		menu = new Array();
		menu = menu.concat( eval("submenu" + cur_munu_str + "0" + ((if_str=="2") ? "_2" : "")) );
		for (i=1; i<=mms_num[cur_munu_no]; i++) {
			menu = ( mms_int[cur_munu_no] & (1 << (mms_num[cur_munu_no]-i)) ) ? ( menu.concat(eval("submenu" + cur_munu_str + i + ((submenuval[i] == 0) ? "_" : "") )) ) : menu;
		}
		addmenu();
	}

		submenu3_1_0 = ["setconf",,,220,1,"",style2,,"left",effect,,,,,,,,,,,,,]
		submenu3_1_0_2 = ["setconf",,,220,1,"",style2_2,,"left",effect,,,,,,,,,,,,,]
		submenu3_1_1 = ["Use Template","",,"Set up a conference using template",0]
		submenu3_1_2 = ["Room Conference","",,"Set up a conference using conference room",0]
		submenu3_1_3 = ["Immediate Video Conference","",,"Set up a immediate conference",0]
		submenu3_1_4 = ["Future Video Conference","",,"Set up a future conference",0]
		submenu3_1_5 = ["Point-to-Point Conference","",,"Set up a connect2 conference",0]
		submenu3_1_5_ = []
		submenu3_1_6 = ["Audio-only Conference","",,"Set up a connect2 conference",0]

		cur_munu_no = 4;	cur_munu_str = "3_1_";
		if ( mms_int[cur_munu_no] ) {
			menu = new Array();
			menu = menu.concat( eval("submenu" + cur_munu_str + "0" + ((if_str=="2") ? "_2" : "")) );
			for (i=1; i<=4; i++) {
				menu = ( mms_int[cur_munu_no] & (1 << (mms_num[cur_munu_no]-i)) ) ? ( menu.concat(eval("submenu" + cur_munu_str + i)) ) : menu;
			}
			i=5;
			menu = ( mms_int[cur_munu_no] & (1 << (mms_num[cur_munu_no]-i)) ) ? ( menu.concat(eval("submenu" + cur_munu_str + i + ( (p2p_int & 1) ?  "" : "_" ) )) ) : menu;
			i=6;
			menu = ( mms_int[cur_munu_no] & (1 << (mms_num[cur_munu_no]-i)) ) ? ( menu.concat(eval("submenu" + cur_munu_str + i)) ) : menu;
			
			addmenu();
		}

		submenu3_2_0 = ["magconf",,,170,1,"",style2,,"left",effect,,,,,,,,,,,,,]
		submenu3_2_0_2 = ["magconf",,,170,1,"",style2_2,,"left",effect,,,,,,,,,,,,,]
		submenu3_2_1 = ["Edit Conference","",,"Edit or copy a conference",0]
		submenu3_2_2 = ["Cancel Conference","",,"Delete a conference",0]

		cur_munu_no = 5;	cur_munu_str = "3_2_";
		if ( mms_int[cur_munu_no] ) {
			menu = new Array();
			menu = menu.concat( eval("submenu" + cur_munu_str + "0" + ((if_str=="2") ? "_2" : "")) );
			for (i=1; i<=mms_num[cur_munu_no]; i++) {
				menu = ( mms_int[cur_munu_no] & (1 << (mms_num[cur_munu_no]-i)) ) ? ( menu.concat(eval("submenu" + cur_munu_str + i)) ) : menu;
			}
			addmenu();
		}


		submenu3_3_0 = ["acsconf",,,170,1,"",style2,,"left",effect,,,,,,,,,,,,,]
		submenu3_3_0_2 = ["acsconf",,,170,1,"",style2_2,,"left",effect,,,,,,,,,,,,,]
		submenu3_3_1 = ["Confirm Reservation","",,"Confirm a future conference",0]
		submenu3_3_2 = ["Check-in Conference","",,"Login a ongoing conference",0]

		cur_munu_no = 6;	cur_munu_str = "3_3_";
		if ( mms_int[cur_munu_no] ) {
			menu = new Array();
			menu = menu.concat( eval("submenu" + cur_munu_str + "0" + ((if_str=="2") ? "_2" : "")) );
			for (i=1; i<=mms_num[cur_munu_no]; i++) {
				menu = ( mms_int[cur_munu_no] & (1 << (mms_num[cur_munu_no]-i)) ) ? ( menu.concat(eval("submenu" + cur_munu_str + i)) ) : menu;
			}
			addmenu();
		}
	

	submenu4_0 = ["utility",96,,180,1,"",style2,,"left",effect,,,,,,,,,,,,,]
	submenu4_0_2 = ["utility",96,,180,1,"",style2_2,,"left",effect,,,,,,,,,,,,,]
	submenu4_1 = ["Groups","",,"manage group",0]
	submenu4_2 = ["Templates","",,"manage template for setting conference",0]
	submenu4_3 = ["Template Preferences","",,"Select Instant Template Priorities",0]
	submenu4_4 = ["User Preferences","",,"Edit preferences",0]
	submenu4_5 = ["Reports","",,"Show account information",0]
		
	cur_munu_no = 7;	cur_munu_str = "4_";
	if ( mms_int[cur_munu_no] ) {
		submenuval = new Array(1,1,1,1,1,1)

		menu = new Array();
		menu = menu.concat( eval("submenu" + cur_munu_str + "0" + ((if_str=="2") ? "_2" : "")) );
		for (i=1; i<=mms_num[cur_munu_no]; i++) {
			menu = ( mms_int[cur_munu_no] & (1 << (mms_num[cur_munu_no]-i)) ) ? ( menu.concat(eval("submenu" + cur_munu_str + i + ((submenuval[i] == 0) ? "_" : "") )) ) : menu;
		}
		addmenu();
	}


	submenu5_0   = ["manager",96,,180,1,,style2,,"left",effect,,,,,,,,,,,,,]
	submenu5_0_2 = ["manager",96,,180,1,,style2_2,,"left",effect,,,,,,,,,,,,,]
	submenu5_1  = ["Users","show-menu=usr",,"Manage users",0]
	submenu5_1_ = ["Users","",,,0]
	submenu5_2  = ["Locations","show-menu=loc",,"Manage locations",0]
	submenu5_2_ = ["Locations","",,,0]
	submenu5_3  = ["Hardware","show-menu=mcu",,"Manage Hardware",0]
	submenu5_3_ = ["Hardware","",,,0]
	submenu5_4  = ["Settings","",,"Edit System Settings",0]
	submenu5_5  = ["Advanced Settings","",,"Edit advanced system settings",0]
	submenu5_6  = ["Reports","show-menu=rpt",,,0]
	submenu5_6_ = ["Reports","",,,0]
	submenu5_7 = ["Room Assistant","show-menu=rmas",,,0]
	submenu5_7_ = ["Room Assistant","",,,0]
	submenu5_7__ = []
	submenu5_8  = ["Caterer","show-menu=catr",,,0]
	submenu5_8_ = ["Caterer","",,,0]
	submenu5_8__ = []

	cur_munu_no = 8;	cur_munu_str = "5_";
	if ( mms_int[cur_munu_no] ) {
		submenuval = new Array(1, mms_int[9],mms_int[10],mms_int[11],1,1,mms_int[12],mms_int[13],mms_int[14])

		menu = new Array();
		menu = menu.concat( eval("submenu" + cur_munu_str + "0" + ((if_str=="2") ? "_2" : "")) );
		for (i=1; i<=6; i++) {
			menu = ( mms_int[cur_munu_no] & (1 << (mms_num[cur_munu_no]-i)) ) ? ( menu.concat(eval("submenu" + cur_munu_str + i + ((submenuval[i] == 0) ? "_" : "") )) ) : menu;
		}

		for (i=7; i<=8; i++) { 
			menu = ( mms_int[cur_munu_no] & (1 << (mms_num[cur_munu_no]-i)) ) ? ( menu.concat(eval("submenu" + cur_munu_str + i + ( ((rf_int>>(mms_num[cur_munu_no]-i)) & 1) ?  ((submenuval[i] == 0) ? "_" : "") : "__" ) )) ) : menu;
		}		

		addmenu();
	}


		submenu5_1_0 = ["usr",,,200,1,"",style2,,"left",effect,,,,,,,,,,,,,]
		submenu5_1_0_2 = ["usr",,,200,1,"",style2_2,,"left",effect,,,,,,,,,,,,,]
		if (al_int=="2")
			submenu5_1_1 = ["Active Users","",,"Edit, search or create a new user",0]
		else
			submenu5_1_1 = ["Edit User","",,"Edit, search or create a new user",0]
		submenu5_1_2 = ["Guests","",,"Edit, search or create a new guest",0]
		if (sso_int)
			submenu5_1_3 = ["Restore User","",,"Delete or Replaced a removed user",0]
		else
			submenu5_1_3 = ["Inactive Users","",,"Delete or Replaced a removed user",0]
		submenu5_1_4 = ["Active Directory Import","",,"Setup Multiple Users",0]
		submenu5_1_5 = ["Templates","",,"Edit, search or create user templates",0]
		submenu5_1_6 = ["Roles","",,"Edit, search or create user roles",0]
		submenu5_1_7 = ["Departments","",,"Manage Department",0]
		submenu5_1_8 = ["Bulk Tool","",,"Manage Bulk User",0]

		cur_munu_no = 9;	cur_munu_str = "5_1_";
		if ( mms_int[cur_munu_no] ) {
			menu = new Array();
			menu = menu.concat( eval("submenu" + cur_munu_str + "0" + ((if_str=="2") ? "_2" : "")) );
			for (i=1; i<=mms_num[cur_munu_no]; i++) {
				menu = ( mms_int[cur_munu_no] & (1 << (mms_num[cur_munu_no]-i)) ) ? ( menu.concat(eval("submenu" + cur_munu_str + i)) ) : menu;
			}
			addmenu();
		}


		submenu5_2_0 = ["loc",,,210,1,"",style2,,"left",effect,,,,,,,,,,,,,]
		submenu5_2_0_2 = ["loc",,,210,1,"",style2_2,,"left",effect,,,,,,,,,,,,,]
		submenu5_2_1 = ["Rooms","",,"Edit or set up conference room",0]
		submenu5_2_2 = ["Tiers","",,"Manage room location",0]

		cur_munu_no = 10;	cur_munu_str = "5_2_";
		if ( mms_int[cur_munu_no] ) {
			menu = new Array();
			menu = menu.concat( eval("submenu" + cur_munu_str + "0" + ((if_str=="2") ? "_2" : "")) );
			for (i=1; i<=mms_num[cur_munu_no]; i++) {
				menu = ( mms_int[cur_munu_no] & (1 << (mms_num[cur_munu_no]-i)) ) ? ( menu.concat(eval("submenu" + cur_munu_str + i)) ) : menu;
			}
			addmenu();
		}


		submenu5_3_0 = ["mcu",,,210,1,"",style2,,"left",effect,,,,,,,,,,,,,]
		submenu5_3_0_2 = ["mcu",,,210,1,"",style2_2,,"left",effect,,,,,,,,,,,,,]
		submenu5_3_1 = ["Endpoints","",,"Setup Bridge",0]
		submenu5_3_2 = ["MCU's","",,"Edit Bridge Management",0]
		submenu5_3_3 = ["Event Logs","",,"View hardware problem log",0]

		cur_munu_no = 11;	cur_munu_str = "5_3_";
		if ( mms_int[cur_munu_no] ) {
			menu = new Array();
			menu = menu.concat( eval("submenu" + cur_munu_str + "0" + ((if_str=="2") ? "_2" : "")) );
			for (i=1; i<=mms_num[cur_munu_no]; i++) {
				menu = ( mms_int[cur_munu_no] & (1 << (mms_num[cur_munu_no]-i)) ) ? ( menu.concat(eval("submenu" + cur_munu_str + i)) ) : menu;
			}
			addmenu();
		}


		submenu5_6_0 = ["rpt",,,220,1,"",style2,,"left",effect,,,,,,,,,,,,,]
		submenu5_6_0_2 = ["rpt",,,220,1,"",style2_2,,"left",effect,,,,,,,,,,,,,]
		submenu5_6_1 = ["User Reports","",,"Show user report",0]
		submenu5_6_2 = ["Room Reports","",,"Show room report",0]
		submenu5_6_3 = ["Conference Reports","",,"Show conference report",0]

		cur_munu_no = 12;	cur_munu_str = "5_6_";
		if ( mms_int[cur_munu_no] ) {
			menu = new Array();
			menu = menu.concat( eval("submenu" + cur_munu_str + "0" + ((if_str=="2") ? "_2" : "")) );
			for (i=1; i<=mms_num[cur_munu_no]; i++) {
				menu = ( mms_int[cur_munu_no] & (1 << (mms_num[cur_munu_no]-i)) ) ? ( menu.concat(eval("submenu" + cur_munu_str + i)) ) : menu;
			}
			addmenu();
		}
		
	
		submenu5_7_0 = ["rmas",,,220,1,"",style2,,"left",effect,,,,,,,,,,,,,]
		submenu5_7_0_2 = ["rmas",,,220,1,"",style2_2,,"left",effect,,,,,,,,,,,,,]
		submenu5_7_1 = ["Inventory","",,"",0]
		submenu5_7_2 = ["Conference Orders","",,"",0]
		submenu5_7_3 = ["Search Orders","",,"",0]

		cur_munu_no = 13;	cur_munu_str = "5_7_";
		if ( mms_int[cur_munu_no] ) {
			menu = new Array();
			menu = menu.concat( eval("submenu" + cur_munu_str + "0" + ((if_str=="2") ? "_2" : "")) );
			for (i=1; i<=mms_num[cur_munu_no]; i++) {
				menu = ( mms_int[cur_munu_no] & (1 << (mms_num[cur_munu_no]-i)) ) ? ( menu.concat(eval("submenu" + cur_munu_str + i)) ) : menu;
			}
			addmenu();
		}
		
		
		
		submenu5_9_0 = ["catr",,,220,1,"",style2,,"left",effect,,,,,,,,,,,,,]
		submenu5_9_0_2 = ["catr",,,220,1,"",style2_2,,"left",effect,,,,,,,,,,,,,]
		submenu5_9_1 = ["Menus","",,"",0]
		submenu5_9_2 = ["Conference Orders","",,"",0]
		submenu5_9_3 = ["Search Orders","",,"",0]
		cur_munu_no = 14;	cur_munu_str = "5_9_";
		if ( mms_int[cur_munu_no] ) {
			menu = new Array();
			menu = menu.concat( eval("submenu" + cur_munu_str + "0" + ((if_str=="2") ? "_2" : "")) );
			for (i=1; i<=mms_num[cur_munu_no]; i++) {
				menu = ( mms_int[cur_munu_no] & (1 << (mms_num[cur_munu_no]-i)) ) ? ( menu.concat(eval("submenu" + cur_munu_str + i)) ) : menu;
			}
			addmenu();
		}

}



dumpmenus()
