//ZD 100147 Start
/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
//ZD 100147 End
using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.IO;
using System.IO.Compression;
using System.Net;
using System.Text;

public partial class en_OutLookTransfers : System.Web.UI.Page
{
    
    ns_Logger.Logger log;
    myVRMNet.NETFunctions obj;
    String ouXml = "";
    String outstr = "";
    String inXML = "";
    String comConfigPath = "C:\\VRMSchemas_v1.8.3\\COMConfig.xml";
    String myvrmser =  "C:\\VRMSchemas_v1.8.3\\";
    String Userid = "11";
    String Cmdid = "GetAllManageUser";

    #region Page Load
    /// <summary>
    ///  Page Load
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>

    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (obj == null)
                obj = new myVRMNet.NETFunctions();
            obj.AccessandURLConformityCheck("OutLookTransfers.aspx", Request.Url.AbsoluteUri.ToLower()); // ZD 100263

            if(Request.QueryString["u"] != null)
            {
                if(Request.QueryString["u"].ToString() != "")
                    Userid = Request.QueryString["u"].ToString();
            }

            if (Request.QueryString["cmd"] != null)
            {
                if (Request.QueryString["cmd"].ToString() != "")
                    Cmdid = Request.QueryString["cmd"].ToString();
            }

            inXML = "";

            if (Cmdid == "GetAllManageUser")
            {
                inXML = "<login><userID>" + Userid + "</userID><sortBy>2</sortBy><alphabet></alphabet><pageNo>1</pageNo></login>";
            }
            else if (Cmdid == "GetNewConference")
                inXML = "<login><userID>" + Userid + "</userID></login>";
            else if (Cmdid == "GetTemplateList")
            {
                inXML = "<login><userID>" + Userid + "</userID><sortBy></sortBy></login>";
            }
            else
            {
                inXML = SearchConference();
                Cmdid = "SearchAllConference";
            }


            ouXml = CallCOM();

            Compress();

            Response.Write(outstr);

        }
        catch (Exception ex )
        {
            log = new ns_Logger.Logger();
            log.Trace("TransferOutLook: " + ex.Message);
            
            
        }

    }

    #endregion

    #region Retrieve Data
    /// <summary>
    ///  Retrieve Data
    /// </summary>
    /// <returns></returns>

    public String CallCOM()
    {
           
        string outXML = "";
        try
        {
            obj = new myVRMNet.NETFunctions();

            if (Cmdid == "SearchAllConference" || Cmdid == "GetAllManageUser")
                outXML = obj.CallMyVRMServer(Cmdid, inXML, myvrmser); 
            else
                outXML = obj.CallCOM(Cmdid, inXML, comConfigPath);

            obj = null;           

        }
        catch (Exception ex)
        {
            log = new ns_Logger.Logger();
            log.Trace("TransferOutLook: " + ex.Message);
        }

        return outXML;

    }

    #endregion

    #region Search Conference
    /// <summary>
    ///  Search Conference
    /// </summary>
    /// <returns></returns>
    public String SearchConference()
    {
        String inputXML = "";
        String strCommand = "";
        Int32 intIndex = 0;
        try 
        {
            switch (Cmdid)
            {
                case "Ongoing": //Ongoing
                    strCommand = "0,6,5";
                    intIndex = 1;
                    break;

                case "Future":  //Future
                    strCommand = "0,6";
                    break;

                case "Public":  //Public
                    strCommand = "0,6";
                    intIndex = 3;
                    break;

                case "Pending":  //Pending
                    strCommand = "1,";
                    break;

                default : //Ongoing
                    strCommand = "0,6,5";
                    intIndex = 1;
                    break;

            }

            inputXML = "<SearchConference>";
            inputXML += "<UserID>" + Userid + "</UserID>";
            inputXML += "<ConferenceID></ConferenceID><ConferenceUniqueID></ConferenceUniqueID>";
            //Scheduled = 0, Pending = 1, Terminated = 3, Ongoing = 5, OnMCU = 6, Completed = 7, Deleted = 9, BLANK = ERROR   -->
            inputXML += "<StatusFilter>";
            string[] strTokenArray = strCommand.Split(',');
            foreach (string strToken in strTokenArray)
            {
                if (strToken.Length > 0)
                    inputXML += "<ConferenceStatus>" + strToken + "</ConferenceStatus>";
            }
            inputXML += "</StatusFilter>";
            inputXML += "<ConferenceName></ConferenceName>";

            //1:Ongoing, 2:Today, 3:This week, 4:This month, 5:Cutom, 6:Yesterday, 7:Tomorrow   -->

            inputXML += "<ConferenceSearchType>";
            if (intIndex == 1)
            {
                inputXML += "1";
            }

            if (intIndex == 2)
            {
                inputXML += "";
            }


            inputXML += "</ConferenceSearchType>";
            //<!--  In case of ConferenceSearchType 5, these from and to dates MUST be specified   
            inputXML += "<ConferenceHost></ConferenceHost><ConferenceParticipant></ConferenceParticipant>";
            //<!--  1:Public, 0:Private, NONE: Any  
            if (intIndex == 3)// Public 
                inputXML += "<Public>1</Public>";
            else
                inputXML += "<Public></Public>";
            //<!-- Use 0  for best results  
            inputXML += "<ApprovalPending>0</ApprovalPending>";
            inputXML += "<DateFrom></DateFrom>";
            inputXML += "<DateTo></DateTo>";
            inputXML += "<RecurrenceStyle>1</RecurrenceStyle>";
            inputXML += "<Location>";
            inputXML += "<SelectionType>1</SelectionType>";
            //<!--  0:None, 1:Any, 2:Selected 
            inputXML += "<SelectedRooms></SelectedRooms>";
            inputXML += "</Location>";
            //Page number will be any number based on what is returned in total pages, the first call would have 
            // a value 1
            inputXML += "<PageNo>1</PageNo>";
            //1:UniqueID, 2:Conference Name, 3:Conference Start Date/Time   
            inputXML += "<SortBy>3</SortBy>";
            inputXML += "</SearchConference>";
        }
        catch (Exception ex)
        {
            throw ex;
        }
        return inputXML;
    }

    #endregion

    #region Compress Stream
    /// <summary>
    ///  Compress Stream
    /// </summary>

    private void Compress()
    {
        // Will open the file to be compressed
        MemoryStream fsSource = null;
        StreamReader sr = null;
        // To hold the compressed file
        GZipStream gzCompressed = null;
        String ret = "";
        try
        {
            if (ouXml != "")
            {
                
                byte[] bufferWrite;
                byte[] bufferOut;
               
                bufferWrite = Encoding.Default.GetBytes(ouXml);
                using (fsSource = new MemoryStream())
                {
                    CompressionMode mode = CompressionMode.Compress;
                    
                    using (gzCompressed = new GZipStream(fsSource, mode, true))
                    {
                        
                        gzCompressed.Write(bufferWrite,0,bufferWrite.Length);
                    }
                    
                }

                bufferOut = fsSource.ToArray();
                fsSource.Close();
                gzCompressed.Close();
                gzCompressed = null;
                outstr = Encoding.Default.GetString(bufferOut);
               
            }

        }
        catch (Exception ex)
        {
            throw ex;
        }
        finally
        {
            // Close the streams
            if (fsSource != null)
            {
                fsSource.Close();
                fsSource = null;
            }
            if (gzCompressed != null)
            {
                gzCompressed.Close();
                gzCompressed = null;
            }
            if (sr != null)
            {
                sr.Close();
                sr = null;
            }
                        
        }



    }

    #endregion

}
