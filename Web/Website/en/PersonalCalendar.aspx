<%--ZD 100147 start--%>
<%--/* Copyright (C) 2013 myVRM - All Rights Reserved	
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.		
*
* You should have received a copy of the myVRM license with	
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/--%><%--ZD 100147 End--%>
<%@ Page Language="C#" Async="true" AutoEventWireup="true" EnableEventValidation="false" Inherits="PersonalCalendar" ValidateRequest="false" %><%-- ZD 100170 --%>

<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>
<%@ Register Assembly="DayPilot" Namespace="DayPilot.Web.Ui" TagPrefix="DayPilot" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajax" %>
<%@ Register Assembly="DevExpress.Web.v10.2, Version=10.2.3.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxTabControl" TagPrefix="dxtc"%>
<%@ Register Assembly="DevExpress.Web.v10.2, Version=10.2.3.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxClasses" tagprefix="dxw"%>
<%@ Register Assembly="MetaBuilders.WebControls.ComboBox" Namespace="MetaBuilders.WebControls" TagPrefix="mbcbb" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> <!-- FB 2050 -->
<meta http-equiv="X-UA-Compatible" content="IE=8" /> <!-- FB 2050 -->
<%
    if(Session["userID"] == null)
    {
        Response.Redirect("genlogin.aspx");

    }
    else
    {
        Application.Add("COM_ConfigPath", "C:\\VRMSchemas_v1.8.3\\ComConfig.xml");
        Application.Add("MyVRMServer_ConfigPath", "C:\\VRMSchemas_v1.8.3\\");
    }
%>

<% 
Boolean showCompanyLogo = false;
if(Request.QueryString["hf"] != null)
{
    if(Request.QueryString["hf"].ToString() == "1")
    {
	    showCompanyLogo = false;
%>
    <!-- #INCLUDE FILE="inc/maintop2.aspx" --> 
<%	    
      if(Session["browser"].ToString().Contains("Netscape") || Session["browser"].ToString().Contains("Firefox"))
      { 
%>
		<link rel="stylesheet" title="Expedite base styles" type="text/css" media="all" href="css/aqua/theme.css" title="Aqua" />
	<% }%>
<%
    }
    else 
    {
%>
    <!-- FB 2719 Starts -->
    <% if (Session["isExpressUser"].ToString() == "1"){%>
    <!-- #INCLUDE FILE="inc/maintopNETExp.aspx" -->
    <%}else{%>
    <!-- #INCLUDE FILE="inc/maintopNET.aspx" -->
    <%}%>
    <!-- FB 2719 Ends -->
<%    
         if(Session["browser"].ToString().Contains("Netscape") || Session["browser"].ToString().Contains("Firefox"))
        {
%>
    		<%--<link rel="stylesheet" title="Expedite base styles" type="text/css" media="all" href="css/aqua/theme.css" title="Aqua" />--%> <%--FB 1982--%>
     <% }%>
<% }

}%>

<%--FB 3055-URL Starts--%>
    <script>
    var ar = window.location.href.split("/");
    var pg = ar[ar.length - 1];
    var page = pg.toLowerCase();
    var cnt = 0;

    if (page != "personalcalendar.aspx?v=1&r=1&hf=&m=&pub=&d=&comp=&f=v")
        cnt++;
    if (page != "personalcalendar.aspx?type=d&v=1&r=1&hf=&d=")
        cnt++;
    if (page != "personalcalendar.aspx?v=1&r=1&hf=&d=")
        cnt++;
    
    if(cnt == 3)
        window.location.href = "thankyou.aspx"

    function fnTimeScroll() {

        if (document.getElementById('CalendarContainer_schDaypilot') != null) {
            document.getElementById('CalendarContainer_schDaypilot').childNodes[1].style.height = '300px';
            document.getElementById('CalendarContainer_schDaypilot').childNodes[1].style.overflowX = 'hidden';
            document.getElementById('CalendarContainer_schDaypilot').childNodes[1].style.overflowY = 'auto';
        }
        if (document.getElementById('CalendarContainer_schDaypilotweek') != null) {
            document.getElementById('CalendarContainer_schDaypilotweek').childNodes[1].style.height = '300px';            
            document.getElementById('CalendarContainer_schDaypilotweek').childNodes[1].style.overflowX = 'hidden';
            document.getElementById('CalendarContainer_schDaypilotweek').childNodes[1].style.overflowY = 'auto';

            var offset = document.getElementById("CalendarContainer_schDaypilotweek").childNodes[1].offsetHeight;
            var scroll = document.getElementById("CalendarContainer_schDaypilotweek").childNodes[1].scrollHeight;
            if(scroll > offset)
                document.getElementById('CalendarContainer_schDaypilotweek').childNodes[0].style.marginRight = '16px';                        
        }

        var StartHr;
        var n = 0;
        var d = new Date();
        var t = d.getHours();
        if (document.getElementById("lstStartHrs_Text") != null)
            StartHr = document.getElementById("lstStartHrs_Text").value.toLowerCase();

        if (StartHr.indexOf("z") > -1)
            n = parseInt(StartHr.substring(0, 2), 10);

        else if (StartHr.indexOf("am") > -1 || StartHr.indexOf("pm") > -1) {
            if (StartHr.indexOf("12") > -1 && StartHr.indexOf("am") > -1)
                n = 0;
            else
                n = parseInt(StartHr.substring(0, 2), 10);

            if (StartHr.indexOf("pm") > -1)
                n += 12;
        }
        else {
            n = parseInt(StartHr.substring(0, 2), 10);
        }

        var pos;

        if (document.getElementById("officehrDaily").checked == true)
            pos = (t * 80) - (n * 80);
        else
            pos = t * 80;

        pos -= 160;
        var dv1;
        var dv2;
        if (document.getElementById('CalendarContainer_schDaypilotweek').childNodes[1] != null)
            dv1 = document.getElementById('CalendarContainer_schDaypilotweek').childNodes[1];
        if (document.getElementById('CalendarContainer_schDaypilot').childNodes[1] != null)
            dv2 = document.getElementById('CalendarContainer_schDaypilot').childNodes[1];

        //if (pos > 80)
            //pos += 15;

        dv1.scrollTop = pos;
        dv2.scrollTop = pos;
        //alert("n: " + n + ", pos:" + pos);
    }
        
    </script>
<%--FB 3055-URL End--%>
    
<link rel="StyleSheet" href="css/divtable.css" type="text/css" />


<script type="text/javascript" src="script/errorList.js"></script>
<script type="text/javascript" language="JavaScript" src="inc/functions.js"></script>
<script type="text/javascript" src="extract.js"></script>
<script type="text/javascript" src="script\mousepos.js"></script>
<script type="text/javascript" src="script\showmsg.js"></script>

<%--FB 1861--%>
<%--<link rel="stylesheet" type="text/css" media="all" href="css/aqua/theme.css" title="Aqua" />--%>

<script type="text/javascript" src="script/mytreeNET.js"></script>
<script type="text/javascript">    // FB 2790
    var path = '<%=Session["OrgCSSPath"]%>';
    path = path.split('.')[0] + '<%=Session["ThemeType"]%>' + ".css"; // FB 2815
    document.write('<link rel="stylesheet" title="Expedite base styles" type="text/css" href="' + path + '?' + new Date().getTime() + '">');
</script>
<link rel="stylesheet" type="text/css" media="all" href="css/calendar-blue.css" /> <%--FB 1861--%> <%--FB 1982--%>

<script type="text/javascript">

  var servertoday = new Date(parseInt("<%=DateTime.Now.Year%>", 10), parseInt("<%=DateTime.Now.Month%>", 10)-1, parseInt("<%=DateTime.Now.Day%>", 10),
  parseInt("<%=DateTime.Now.Hour%>", 10), parseInt("<%=DateTime.Now.Minute%>", 10), parseInt("<%=DateTime.Now.Second%>", 10));
  
</script>
<script type="text/javascript" language="JavaScript" src="inc/disablerclick.js"></script>
<script type="text/javascript" src="script/cal-flat.js"></script>
<script type="text/javascript" src="lang/calendar-en.js"></script>
<script type="text/javascript" src="script/calendar-flat-setup.js"></script>
<script language="javascript1.1" src="script/calview.js"></script>
<html id="Html1" runat="server"  xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title>Personal Calendar</title>
</head>
<body>

  <form id="frmPersonalCalendar" runat="server" >
  
   <asp:ScriptManager ID="CalendarScriptManager" runat="server" AsyncPostBackTimeout="600">
                 </asp:ScriptManager>
  
    <input type="hidden" id="cmd" value="GetSettingsSelect" />
    <input type="hidden" id="settings2locstr" value="" />
    <input type="hidden" id="helpPage" value="79" />                       
    <input type="hidden" id="IsMonthChanged" runat="server" />
    <input type="hidden" id="IsWeekOverLap" runat="server" />
    <input type="hidden" id="IsWeekChanged" runat="server" />                        
    <input type="hidden" id="MonthNum"  />
    <input type="hidden" id="YrNum"  />
    <input type="hidden" id="Weeknum"  />
    <input type="hidden" runat="server" id="hdnStartTime" name="hdnStartTime" /><%--ZD 100157--%>
	<input type="hidden" runat="server" id="hdnEndTime" name="hdnEndTime"  /><%--ZD 100157--%>
  
<%
    if (Request.QueryString["hf"] != null)
    {
        if (Request.QueryString["hf"].ToString() == "1")
        {
%>
            <table width="100%" border="0"><tr><td align="right">
	            <input type="button" name="close" onfocus="this.blur()" id="close" value="Close" class="altShort2BlueButtonFormat" onclick="javascript:window.close();">

<%              
                if (Request.QueryString["pub"] != null)
                {
                    if (Request.QueryString["pub"].ToString() == "1")
                    {
%>
	                    <input type="button" name="login" id="login" value="Login" class="altShort2BlueButtonFormat" onclick="javascript:window.location.href='genlogin.aspx'">
                        </td></tr></table>
<%                  }
                    else{%> </td></tr></table><%}
                }
                else{%> </td></tr></table><%}
         }
    }   
%>  
 
     <table align="center"> <%--Added for FF--%>
       <tr>
        <td>
     <div id="footer" style="width:100%;">
      <table border="0"  width="100%" align="left"  style="vertical-align:bottom;">
          <tr>
              <td colspan="12" align="center">
                 <h3>Personal Calendar&nbsp;&nbsp; 
                    <%--<input type="button" name="RoomCal" onfocus="this.blur()"  value="Switch to Room Calendar" style="width:180px" class="altShortBlueButtonFormat" onclick="javascript:viewrmdy()" /></h3>--%>
                    <select id="lstCalendar" title="View" name="lstCalendar" class="altText" size="1" onChange="goToCal();javascript:DataLoading1('1');" runat="server"> <%--FB 2058--%>
                        <%--<option value="0">Select View</option>--%>
                        <option value="1">Personal Calendar View</option>
                        <option value="3">Room Calendar View</option>
                        <option value="2">List View</option>
                    </select><br />
              </td>
        </tr>
      </table>
     </div>
        </td> <%--Added for FF--%>
      </tr>
     </table> 
     
    <table width="100%" border="0" style="vertical-align:super;">
        <tr valign="top">
            <td id="ActionsTD" valign="top" align="right" style="width:15%" runat="server">
              <table><tr height="15PX"><td></td></td></tr></table>
                 <div align="center" id="othview">
                    <table border="0" width="100%" class="treeSelectedNode"> 
                         <tr id="trofficehrDaily" >
                            <td align="left">  <%--ZD 100157 Starts--%>
                             <table border="0" style="border-collapse:collapse; text-align:left" >
                            <tr>
                                <td nowrap="nowrap">
                         
                                <asp:CheckBox id="officehrDaily" Checked="true" onclick="javascript:chnghrs();ShowHrs();"   AutoPostBack="false" runat="server" tooltip="show office hour only" />
                                <label class="blackblodtext"  id="lblhour">Show Hours</label>
                                <label class="rmnamediv" for="officehr" id="lblOfficehrs" title="show office hours only" style="display:none;">Show Office Hours Only</label>
                            
                                </td><td  nowrap="nowrap">
                                &nbsp;&nbsp;&nbsp;&nbsp;
                            <mbcbb:combobox id="lstStartHrs" cssclass="altText" causesvalidation="true"
                        runat="server" enabled="true" rows="15" >
                    </mbcbb:combobox> 
                    <asp:RequiredFieldValidator ID="reqlstStartHrs" runat="server" ControlToValidate="lstStartHrs"
                        Display="Dynamic" ErrorMessage="Time is Required"></asp:RequiredFieldValidator>
                    <asp:RegularExpressionValidator ID="reglstStartHrs" runat="server" ControlToValidate="lstStartHrs"
                        Display="Dynamic" ErrorMessage="Invalid Time (HH:MM AM/PM)"
                        ValidationExpression="[0-1][0-9]:[0-5][0-9] ?[A|a|P|p][M|m]"></asp:RegularExpressionValidator>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2"></td>
                            </tr>
                            <tr>
                                <td></td>
                                <td style="text-align:right"  nowrap="nowrap">
                    <mbcbb:combobox id="lstEndHrs" cssclass="altText" causesvalidation="true"
                        runat="server" enabled="true" rows="15" >
                    </mbcbb:combobox> 
                    <asp:RequiredFieldValidator ID="reqlstEndHrs" runat="server" ControlToValidate="lstEndHrs"
                        Display="Dynamic" ErrorMessage="Time is Required"></asp:RequiredFieldValidator>
                    <asp:RegularExpressionValidator ID="reglstEndHrs" runat="server" ControlToValidate="lstEndHrs"
                        Display="Dynamic" ErrorMessage="Invalid Time (HH:MM AM/PM)"
                        ValidationExpression="[0-1][0-9]:[0-5][0-9] ?[A|a|P|p][M|m]"></asp:RegularExpressionValidator>
                         <%--ZD 100157 Ends--%>
                        
                                </td>
                            </tr>
                         </table>
                            </td>
                         </tr>
                         <tr id="trofficehrWeek" style="display:none">
                             <td align="left"  nowrap>
                                <asp:CheckBox id="officehrWeek" Checked="true" onclick="javascript:chnghrs();"   AutoPostBack="false" runat="server" tooltip="show office hour only" />
                                <label class="blackblodtext"  id="lblweek">Show Weekdays Only</label>
                                <label class="rmnamediv" for="officehr" id="Label1" title="show office hours only"  style="display:none;">Show Weekdays Only</label>
                             </td>
                         </tr>
                         <tr id="trofficehrMonth" style="display:none">
                            <td align="left"  nowrap>
                                <asp:CheckBox id="officehrMonth" Checked="true" onclick="javascript:chnghrs();"  AutoPostBack="false" runat="server" tooltip="show week days only" />
                                <label class="blackblodtext"  id="lblmonth">Show Weekdays Only</label>
                                <label class="rmnamediv" for="officehr" id="Label2" title="show office hours only" style="display:none;">Show Weekdays Only</label>
                            </td>
                         </tr>
                         <tr><%--FB 1800--%>
                              <td valign="top" align="left">
                                <asp:CheckBox ID="showDeletedConf" runat="server" AutoPostBack="false"  onclick="DeleteClick()" />                                
                                <span class="blackblodtext">Deleted Conferences</span>
                              </td>
                         </tr>
                         <tr>
                            <td class="tableHeader"  align="left">Calendar</td>
                         </tr>
                        <tr height="180" align="left"  valign="baseline">
                            <td> &nbsp;<div id="flatCalendarDisplay" align="center" ></div><div id="subtitlenamediv"></div></td>
                        </tr>
                    </table>
                 </div>
                 <table><tr height="15"><td>&nbsp;</td></tr></table>
                 <div align="center" id="conftypeDIV" style="z-index:1;width:100%;" class="treeSelectedNode">
                    <table border="0" width="100%">
                        <tr><td height="5" class="tableHeader">Filters</td></tr>
                        <tr valign="top">
                            <td align="left">
                                <asp:TreeView ID="treeRoomSelection"   runat="server" BorderColor="White" Height="100%" ShowCheckBoxes="All" valign="top"  
                                  ShowLines="True" Width="100%"  onclick="javascript:getTabCalendar(event)" BorderStyle="None">
                                    <NodeStyle CssClass="blacktext" />
                                    <RootNodeStyle CssClass="blacktext" />
                                    <ParentNodeStyle CssClass="blacktext" />
                                    <LeafNodeStyle CssClass="blacktext" />
                                    <Nodes>
                                        <asp:TreeNode SelectAction="None"   Value="ALL" Text="All">
                                            <asp:TreeNode  SelectAction="None" Value="OG"   Text="Ongoing Conferences"></asp:TreeNode>
                                            <asp:TreeNode  SelectAction="None" Value="FC" Text="Future Conferences "></asp:TreeNode>
                                            <asp:TreeNode  SelectAction="None" Value="PC" Text="Public Conferences "></asp:TreeNode>
                                            <asp:TreeNode SelectAction="None" Value="PNC" Text="Pending Conferences "></asp:TreeNode>
                                            <asp:TreeNode SelectAction="None" Value="AC" Text="Approval Conferences "></asp:TreeNode>
                                        </asp:TreeNode>
                                    </Nodes>
                                </asp:TreeView>
                            </td>
                        </tr>
                    </table>
                 </div>
              </td>
              <td  style="width:2%"></td> 
              <td style="width:83%" align="left">
                <asp:Label ID="errLabel" runat="server" Text="" CssClass="lblError"></asp:Label>
                <label id="labErrTime" class="lblError"  style="margin-left: 300px; display:none">End Time should be greater than Start Time.</label> <%--ZD 100157--%>
                <asp:UpdatePanel ID="PanelTab" runat="server" RenderMode="Inline" UpdateMode="Conditional">
                    <Triggers><asp:AsyncPostBackTrigger ControlID="btnDate" /></Triggers>
                    <ContentTemplate>
                        <input type="hidden" id="HdnMonthlyXml" runat="server" /> 
                        <div id="dataLoadingDIV" name="dataLoadingDIV" align="center"></div>    
                            <input type="hidden" id="HdnXml" runat="server" />
                            <input type="hidden" id="IsSettingsChange" runat="server" />
                            <dxtc:ASPxPageControl ClientInstanceName="CalendarContainer" ID="CalendarContainer" runat="server" ActiveTabIndex="0" Width="99%" 
                            > <%--ZD 100151--%>
                                <ClientSideEvents ActiveTabChanged="function(s,e){ChangeTabsIndex(s,e);}" />
                                <TabPages>
                                 <dxtc:TabPage Text="Daily View" Name="Daily">
                                    <ContentCollection>
                                        <dxw:ContentControl ID="ContentControl1" runat="server">
                                            <table width="100%" style="width: 100%; vertical-align: top; overflow: auto; height:420px">  <%-- ZD 100157--%>
                                               <tr valign="top" id="trlegend1" runat="server"> <%--FB 1985--%>
                                                    <td>
                                                        <table  width="100%">
                                                            <tr>
                                                                <td  id="TdAV1" runat="server" width="15" height="15" bgcolor="#BBB4FF"></td>
                                                                <td id="TdAV" runat="server"> <span id="spnAV"  runat="server">Audio/Video Conference</span></td>
                                                                <td width="15" height="15" bgcolor="#F16855"></td>
                                                                <td><span id="spnRmHrg"  runat="server">Room Conference</span></td>
                                                                <td id="TdP2p" runat="server" width="15" height="15" bgcolor="#EAA2D4"></td>
                                                                <td id="TdA" runat="server"><span id="spnAudCon"   runat="server">Audio-Only Conference</span></td>
                                                                <td id="TdA1" runat="server" width="15" height="15" bgcolor="#85EE99"></td>
                                                                <td id="TdP2p1" runat="server"><span id="spnPpConf"   runat="server">Point-to-Point Conference</span></td>
                                                                <td id="TdVMR" runat="server" width="15" height="15" bgcolor="#82CAFF"></td> <%--FB 2448--%>
                                                                <td id="Td27" runat="server"><span id="Span1"   runat="server">VMR Conference</span></td>
                                                                <td id="Td28" runat="server" width="15" height="15" bgcolor="#ffcc99"></td>
			                                                    <td id="Tdbuffer2" runat="server">Setup Time</td>
			                                                    <td id="Tdbuffer3" runat="server" width="15" height="15" bgcolor="#cccc99"></td>
			                                                    <td id="Tdbuffer4" runat="server">TearDown Time</td>
			                                                     <td id="Td21" runat="server" width="15" height="15" bgcolor= "#01DFD7"></td>
			                                                    <td id="Td22" runat="server">Deleted Conference</td>
			                                                    <td id="TdDailyHotdeskingColor" runat="server" width="15" height="15" bgcolor= "#cc0033"></td><%--FB 2694--%>	
			                                                    <td id="TdDailyHotdesking" runat="server">Hotdesking Conference</td>	
			                                                   
                                                            </tr>
                                                        </table>
                                                    </td>
                                               </tr>  
                                               <tr valign="top">
                                                    <td>
                                                        <div id="DayPilotCalendar" style="height:300px;"> <%--ZD 100151--%>
                                                            <DayPilot:DayPilotCalendar  ID="schDaypilot"  runat="server" Days="1"   Visible="false" 
                                                            DataStartField="start"   DataEndField="end"  DataTextField="confDetails" DataValueField="ConfID" 
                                                            DataTagFields="ConferenceType,CustomDescription,confName,ID,Heading,ConfTime,Hours,Minutes,Password,Location,ConfSupport,CustomOptions" 
                                                            HeaderFontSize="8pt" HeaderHeight="17" CellDuration="15"  OnBeforeEventRender="BeforeEventRenderhandler"  BubbleID="Details" ShowToolTip="false"    EventClickHandling="JavaScript"   EventClickJavaScript="ViewDetails(e);" EventFontSize="10" 
                                                            ClientObjectName="dps1" TimeRangeSelectedHandling="JavaScript" HeightSpec="Fixed" Height="300" TimeRangeSelectedJavaScript="getConfType(start)"  UseEventBoxes="Never" OnBeforeTimeHeaderRender="BeforeTimeHeaderRenderDaily" /> <%--FB 1673--%> <%-- FB 2588 --%>
                                                            <DayPilot:DayPilotBubble ID="Details" runat="server"  Visible="true"  Width="0" OnRenderContent="BubbleRenderhandler" ZIndex="999"></DayPilot:DayPilotBubble>    <%-- ZD 100157--%>
                                                        </div>
                                                    </td>
                                                </tr>
                                            </table>
                                        </dxw:ContentControl>   
                                    </ContentCollection>
                                  </dxtc:TabPage>
                                  <dxtc:TabPage Text="Weekly View" name="Weekly">
                                    <ContentCollection>
                                        <dxw:ContentControl ID="ContentControl2" runat="server">
                                            <table width="100%" style="width: 100%; vertical-align: top; overflow: auto; height: 420px"> <%-- ZD 100157--%>
                                                <tr valign="top" id="trlegend2" runat="server"><%--FB 1985--%>
                                                    <td>
                                                        <table width="100%">
                                                            <tr>
                                                                <td  id="Td1" runat="server" width="15" height="15" bgcolor="#BBB4FF"></td>
                                                                <td id="Td2" runat="server"><span id="spnAVW" runat="server">Audio/Video Conference</span></td>
                                                                <td width="15" height="15" bgcolor="#F16855"></td>
                                                                <td><span id="spnRmHrgW"  runat="server">Room Conference</span></td>
                                                                <td id="Td3" runat="server" width="15" height="15" bgcolor="#EAA2D4"></td>
                                                                <td id="Td4" runat="server"><span id="spnAudConW"   runat="server">Audio-Only Conference</span></td>
                                                                <td id="Td5" runat="server" width="15" height="15" bgcolor="#85EE99"></td>
                                                                <td id="Td6" runat="server"><span id="spnPpConfW"  runat="server">Point-to-Point Conference</span></td>
                                                                <td id="Td29" runat="server" width="15" height="15" bgcolor="#82CAFF"></td> <%--FB 2448--%>
                                                                <td id="Td30" runat="server"><span id="Span2"   runat="server">VMR Conference</span></td>
                                                                <td id="Td7" runat="server" width="15" height="15" bgcolor="#ffcc99"></td>
		                                                        <td id="Td8" runat="server">Setup Time</td> <%-- Organization Css Module --%>
		                                                        <td id="Td9" runat="server" width="15" height="15" bgcolor="#cccc99"></td>
		                                                        <td id="Td10" runat="server">TearDown Time</td> <%-- Organization Css Module --%>
			                                                     <td id="Td23" runat="server" width="15" height="15" bgcolor= "#01DFD7"></td>
			                                                    <td id="Td24" runat="server">Deleted Conference</td>
			                                                    <td id="TdWeeklyHotdeskingColor" runat="server" width="15" height="15" bgcolor= "#cc0033"></td><%--FB 2694--%>	
			                                                    <td id="TdWeeklyHotdesking" runat="server">Hotdesking Conference</td>		                                                    
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                                <tr valign="top">
                                                    <td>
                                                      <div id="Div1" align="left"  style="height:300px;" ><%--ZD 100151--%>
                                                         <DayPilot:DayPilotCalendar CssClass="tableHeader" ID="schDaypilotweek" runat="server" Days="7" Visible="false" 
                                                        DataStartField="start" DataEndField="end"  DataTextField="confDetails" DataValueField="ConfID" 
                                                        DataTagFields="ConferenceType,ConfID,CustomDescription,confName,ID,Heading,ConfTime,Hours,Minutes,Password,Location,ConfSupport,CustomOptions" 
                                                        HeaderFontSize="8pt" HeaderHeight="17" CellDuration="15"  OnBeforeEventRender="BeforeEventRenderhandler"  BubbleID="DetailsWeekly" ShowToolTip="false"    EventClickHandling="JavaScript" HeightSpec="Fixed" Height="300"  EventClickJavaScript="ViewDetails(e);" EventFontSize="10" CssClassPrefix="calendar_silver_"
                                                        ClientObjectName="dps1"  UseEventBoxes="Never" OnBeforeTimeHeaderRender="BeforeTimeHeaderRenderWeekly" /> <%-- FB 2588 --%> <%-- ZD 100157--%>
                                                        <DayPilot:DayPilotBubble ID="DetailsWeekly" runat="server"  Visible="true"  Width="0" OnRenderContent="BubbleRenderhandler" ZIndex="999"></DayPilot:DayPilotBubble>   
                                                      </div>
                                                    </td>
                                                </tr>
                                            </table>
                                        </dxw:ContentControl>    
                                    </ContentCollection>
                                 </dxtc:TabPage>
                                 <dxtc:TabPage Text="Monthly View" Name="Monthly">
                                    <ContentCollection>
                                        <dxw:ContentControl ID="ContentControl3" runat="server">
                                            <table width="100%"  style="width:100%;vertical-align:top;overflow:auto;height:550px">
                                                <tr valign="top" id="trlegend3" runat="server"><%--FB 1985--%>
                                                    <td>
                                                        <table width="100%">
                                                            <tr>
                                                                <td  id="Td11" runat="server" width="15" height="15" bgcolor="#BBB4FF"></td>
                                                                <td id="Td12" runat="server"><span id="spnAVM"  runat="server">Audio/Video Conference</span></td>
                                                                <td width="15" height="15" bgcolor="#F16855"></td>
                                                                <td><span id="spnRmHrgM"  runat="server">Room Conference</span></td>
                                                                <td id="Td13" runat="server" width="15" height="15" bgcolor="#EAA2D4"></td>
                                                                <td id="Td14" runat="server"><span id="spnAudConM"   runat="server">Audio-Only Conference</span></td>
                                                                <td id="Td15" runat="server" width="15" height="15" bgcolor="#85EE99"></td>
                                                                <td id="Td16" runat="server"><span id="spnPpConfM"   runat="server">Point-to-Point Conference</span></td>
                                                                <td id="Td31" runat="server" width="15" height="15" bgcolor="#82CAFF"></td> <%--FB 2448--%>
                                                                <td id="Td32" runat="server"><span id="Span3"   runat="server">VMR Conference</span></td>
                                                                <td id="Td17" runat="server" width="15" height="15" bgcolor="#ffcc99"></td>
			                                                    <td id="Td18" runat="server">Setup Time</td>
			                                                    <td id="Td19" runat="server" width="15" height="15" bgcolor="#cccc99"></td>
			                                                    <td id="Td20" runat="server">TearDown Time</td><%-- Organization Css Module --%>
			                                                     <td id="Td25" runat="server" width="15" height="15" bgcolor= "#01DFD7"></td>
			                                                    <td id="Td26" runat="server">Deleted Conference</td>
			                                                    <td id="TdMontlyHotdeskingColor" runat="server" width="15" height="15" bgcolor= "#cc0033"></td><%--FB 2694--%>	
			                                                    <td id="TdMontlyHotdesking" runat="server">Hotdesking Conference</td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                                <tr valign="top">
                                                    <td>
                                                       <div id="DayPilotCalendarMonth"  ><%--ZD 100151--%>
                                                         <DayPilot:DayPilotMonth CssClass="tableHeader" ID="schDaypilotMonth" runat="server"  Visible="false" 
                                                            DataStartField="start" DataEndField="end"  DataTextField="confDetails" DataValueField="ConfID" 
                                                            DataTagFields="ConferenceType,CustomDescription,confName,ID,Heading,ConfTime,Hours,Minutes,Password,Location,ConfSupport,CustomOptions"
                                                            HeaderFontSize="8pt" HeaderHeight="17"  WeekStarts="Monday"  OnBeforeEventRender="BeforeEventRenderhandler" 
                                                            EventClickHandling="JavaScript"   EventClickJavaScript="ViewDetails(e);" JavaScript="ViewDetails(e);" EventFontSize="10" 
                                                            ClientObjectName="dps1" Width="100%"  BubbleID="DetailsMonthly" ShowToolTip="false"  HeightSpec="Fixed" Height="300"   />  <%-- ZD 100157--%>
                                                            <DayPilot:DayPilotBubble ID="DetailsMonthly" runat="server"  Visible="true" Width="0" OnRenderContent="BubbleRenderhandler" ZIndex="999"></DayPilot:DayPilotBubble>   
                                                       </div>
                                                    </td>
                                                </tr>
                                            </table>
                                    </dxw:ContentControl>
                                    </ContentCollection>
                                 </dxtc:TabPage>
                             </TabPages>
                        </dxtc:ASPxPageControl>
                    </div>
                    <asp:Button ID="btnDate" style="display:none" OnClick="ChangeCalendarDate" OnClientClick="DataLoading('1');" runat="server"/>
                    <asp:Button ID="btnWithinMonth" style="display:none" OnClick="changeDate" OnClientClick="DataLoading('1');" runat="server"/>
                    <asp:Button ID="btnbsndhrs" style="display:none" OnClick="changeDate" OnClientClick="DataLoading('1');" runat="server"/>
                    
                </ContentTemplate>
             </asp:UpdatePanel>
           </td>
        </tr>
     </table>
    <asp:TextBox ID="txtType" Visible="false" runat="server"></asp:TextBox>  
    <input type="hidden" ID="txtSelectedDate" runat="server" />
    <input type="hidden" ID="txtSelectedDate1" runat="server" />
    
    <asp:UpdatePanel ID="UpdateXmls" runat="server">
<Triggers>
<asp:AsyncPostBackTrigger ControlID="xmlsAsync" EventName="Click" />
</Triggers>
<ContentTemplate>
<asp:Button ID="xmlsAsync" runat="server" style="display:none;"
onclick="LoadXmlsAsync" />
</ContentTemplate>
</asp:UpdatePanel>
    
  </form>
  </body>
  </html>  
  
<script language="javascript">

//FB 2598 Starts
//"<%=Session["emailClient"] %>"

    function removeOption()
    {
        if("<%=Session["EnableCallmonitor"]%>" == "0")
        {
        
        var x=document.getElementById("lstCalendar");
        x.remove(4);
        x.remove(3);
        }
       
    }

//FB 2598 Ends

var asyncReqCompleted = "1";    
//setTimeout(function(){document.getElementById ("btnLdAsync").click();},500);

function DeleteClick()
{
    document.getElementById ("btnDate").click();
    
}


//loadAsync();
function loadAsync()
{

// if("<%=Session["CalendarMonthly"]%>" == "") //FB 1888
    document.getElementById ("xmlsAsync").click();
        
}

function ChangeTabsIndex(s,e)
{
    var activetab = s.GetActiveTab();
   var tabname;
   tabname = activetab.name;
   var hour = document.getElementById('trofficehrDaily');
   var week = document.getElementById('trofficehrWeek');
   var month = document.getElementById('trofficehrMonth');
   var hdnmonth = document.getElementById('HdnMonthlyXml');
   
   if(tabname == "Daily")
   {
   hour.style.display = 'block';
   week.style.display = 'none';
   month.style.display = 'none';
   //if(document.getElementById("IsWeekOverLap").value == "Y")
       document.getElementById ("btnDate").click();
   }
   else if(tabname == "Weekly")
   {
    hour.style.display = 'none';
   week.style.display = 'block';
   month.style.display = 'none';
   //if(document.getElementById("IsMonthChanged").value == "Y"|| document.getElementById("IsWeekOverLap").value == "Y" || hdnmonth.value == "") //ZD 100151
       document.getElementById ("btnDate").click();
  
  
   }
   else if(tabname== "Monthly")
   {
    hour.style.display = 'none';
   week.style.display = 'none';
   month.style.display = 'block';
   //if(hdnmonth.value == "" || document.getElementById("IsMonthChanged").value == "Y" || document.getElementById("IsWeekOverLap").value == "Y") //ZD 100151
       document.getElementById ("btnDate").click();
   }
   if(fnTimeScroll()) //ZD 100157
    fnTimeScroll();
   
   } 
   
   function chnghrs()
   {
     if (fncheckTime() == false) //ZD 100157
            return false;
    var hourbtn = document.getElementById('btnbsndhrs');
    if(hourbtn)
        hourbtn.click();
   } 

function DataLoading(val)
{   
    
    if (val=="1")
    {
        document.getElementById("dataLoadingDIV").innerHTML="<b><font color='#FF00FF' size='2'>Data loading ...</font></b>&nbsp;&nbsp;&nbsp;&nbsp;<img border='0' src='image/wait1.gif' >"; // FB 2742
        //document.body.style.cursor = "wait";
        document.body.style.cursor = "normal";//FB 2803
        
        
    
        
       }
    else
    {
        document.getElementById("dataLoadingDIV").innerHTML="";
        document.body.style.cursor = "normal";
     } 
                  
}

function DataLoading1(val)
 {
    //alert(val);
    if (val == "1")
     document.getElementById("dataLoadingDIV").innerHTML = "<img border='0' src='image/wait1.gif'>";
    else
     document.getElementById("dataLoadingDIV").innerHTML = "";
}   
 var prm = Sys.WebForms.PageRequestManager.getInstance();
    prm.add_initializeRequest(InitializeRequestHandler);
    prm.add_endRequest(EndRequestHandler);        

    var pbQueue = new Array();
    var argsQueue = new Array();       

    function InitializeRequestHandler(sender, args) {
        if (prm.get_isInAsyncPostBack()) {
            args.set_cancel(true);
            pbQueue.push(args.get_postBackElement().id);
            argsQueue.push(document.forms[0].__EVENTARGUMENT.value);
        }
        
        DataLoading("1");
    }       

    function EndRequestHandler(sender, args) {
        if (pbQueue.length > 0) {
            __doPostBack(pbQueue.shift(), argsQueue.shift());
        }
        else
            DataLoading("0");
    }
var myPopup;
var IE4 = (document.all) ? true : false;


var	dayname = new Array();
var pageleft = 10;
var totaltop = 190, topleft = 245, totalh = 960, calendarh = 180, totalw = getwinwid() - topleft - pageleft, yaxlwid = 65, lineh = 15, splitwid=1;


var allowedrmnum = 0,	isOhOn = false;
var strConfs = "", startDate; 
var aryRooms="", selConfTypeCode;

var isWdOn = false, confmonthfirst = "";

function getwinwid() 
{
	var myWidth = 0, myHeight = 0;
	if( typeof( window.innerWidth ) == 'number' ) {
		//Non-IE
		myWidth = window.innerWidth;
		myHeight = window.innerHeight;
	} else if( document.documentElement && ( document.documentElement.clientWidth || document.documentElement.clientHeight ) ) {
		//IE 6+ in 'standards compliant mode'
		myWidth = document.documentElement.clientWidth;
		myHeight = document.documentElement.clientHeight;
	} else if( document.body && ( document.body.clientWidth || document.body.clientHeight ) ) {
		//IE 4 compatible
		myWidth = document.body.clientWidth;
		myHeight = document.body.clientHeight;
	}
	  
	return (myWidth);
}


function getConfType(start)
{
  if( isExpressUser == 1 ) //FB 1779
   {
      //alert("You cannot schedule a conference from a popup calendar.");//FB 1779 - Commented
      return false; 
      
   }
  var time = start.toString(); //Edited for FF 
  var conftime = new Date(time);
  
  var hour,min;
  
  var splttim = conftime.toTimeString().split(':');
  
  hour = splttim[0];
  min = splttim[1];
  
  if(timeFormat == "1")
  {
  
      hour = conftime.getHours();
      min = conftime.getMinutes();
      
      
      if(eval(hour) < 10)
        hour = "0"+hour
        
      if(eval(min) < 10)
        min = "0"+min
        
      //min = min +" "+conftime.toLocaleTimeString().toString().split(" ")[1]
      min = min +" "+ conftime.format("tt"); //FB 2108
  }
  
  
    
  if (queryField("hf") == "1")
    {
        alert("You cannot schedule a conference from a popup calendar.");
        return false;
    }
	
    else {
     
        var mmm_numc, mm_strc, mm_aryc,mmm_strc,mmm_intc,menu;
        
        var menuset = "0";
        var time ;
        
        mm_strc = "<%=Session["sMenuMask"].ToString() %>";;
        mm_aryc = mm_strc.split("-");
        mmm_strc = mm_aryc[0];
        mmm_aryc = mmm_strc.split("*");
        mmm_numc = parseInt(mmm_aryc[0], 10);
        mmm_intc = parseInt(mmm_aryc[1], 10);
        for (i=1; i<=mmm_numc; i++) {
        menu = ( mmm_intc & (1 << (mmm_numc-i)) ) ? ("menu_" + i) : "";
        //FB 1929
        if(menu == "menu_3")
            menuset = "1";
	    
        }
        //FB 1929
        if(menuset > 0)
        {         
//        if(confirm("Are you sure you want to create a new conference beginning " + confdate + " @ " + hour+":"+min  + "?"))
//        {
            
               url = "ConferenceSetup.aspx?t=n&sd=" + confdate + "&st=" + hour+":"+min  ; 
            
	        
            DataLoading("1");
            window.location.href=url;
//        }
       }
    }
}

function ViewDetails(e)
{
    
  url = "ManageConference.aspx?confid="+e.value()+"&t=hf";
  myPopup = window.open(url, "viewconference", "width=1,height=1,resizable=yes,scrollbars=yes,status=no");
  myPopup.focus();
  
}  


function initsubtitle ()
{
    var dd, m = "";
    var monthNew,yrNew
    
    dd = new Date(confdate);
                     
    monthNew = (dd.getMonth() + 1);
    yrNew = dd.getFullYear();
    
    document.getElementById ("IsWeekOverLap").value = "";
    document.getElementById ("IsMonthChanged").value = "";
    
    if(WeekOverlap(confdate))
           document.getElementById ("IsWeekOverLap").value = "Y";
    if(document.getElementById ("MonthNum").value != monthNew || document.getElementById ("YrNum").value != yrNew )
    {
        document.getElementById ("YrNum").value = yrNew ;
        document.getElementById ("MonthNum").value = monthNew;
        document.getElementById ("IsMonthChanged").value = "Y";
    }
    else
    {
    
        document.getElementById ("IsMonthChanged").value = "";
        
    }
    
}
function datechg() {
    
   var hdndmonth = document.getElementById('HdnMonthlyXml');

    DataLoading("1");



    document.getElementById("txtSelectedDate").value=confdate;
    initsubtitle();
    
    if(hdndmonth.value != "" && document.getElementById ("IsMonthChanged").value == "")
    {
       document.getElementById ("btnWithinMonth").click();
       
    }
    else
        document.getElementById ("btnDate").click();
    
}


 document.getElementById ("btnDate").style.display = "none";
            
var dtFormatType, convDate ,calendarTyp ,timeFormat;
dtFormatType = '<%=dtFormatType%>';
calendarTyp = '<%=CalendarType%>';
timeFormat = '<%=timeFormat%>';
var confdate,confweekmon,curwkDate
var curdate = new Date();

//datechg("1");

if (document.getElementById("txtSelectedDate").value == "") {
	confdate = (curdate.getMonth() + 1) + "/" + curdate.getDate() + "/" + curdate.getFullYear();
	document.getElementById ("MonthNum").value = (curdate.getMonth() + 1); 
	document.getElementById ("YrNum").value = curdate.getFullYear();
	document.getElementById ("IsMonthChanged").value = "";
	confweekmon = addDates(curdate.getFullYear(), curdate.getMonth() + 1, curdate.getDate(), (curdate.getDay() == 0) ? -6 : (1-curdate.getDay()) );
	document.getElementById("<%=txtSelectedDate.ClientID %>").value=confdate;
	showFlatCalendar(0, '%m/%d/%Y');
} else {
	confdate = document.getElementById("txtSelectedDate").value;
	curwkDate = new Date(confdate);
	confweekmon = addDates(curwkDate.getFullYear(), curwkDate.getMonth() + 1, curwkDate.getDate(), (curwkDate.getDay() == 0) ? -6 : (1-curwkDate.getDay()) );
	showFlatCalendar(0, '%m/%d/%Y', document.getElementById("txtSelectedDate").value);
}
document.getElementById ("flatCalendarDisplay").style.position = 'relative';
document.getElementById ("flatCalendarDisplay").style.top = -15;
document.getElementById ("flatCalendarDisplay").style.height = 170;
document.getElementById ("flatCalendarDisplay").style.textAlign = "center";
//document.getElementById ("flatCalendarDisplay").style.display = "none";
//DataLoading("0");
initsubtitle();

function goToCal()
{
        if(document.getElementById("lstCalendar") != null)
        {
		    
		    if (document.getElementById("lstCalendar").value == "2")//FB 1779
		    {
			   if( isExpressUser == 1 ) //FB 1779
			      window.location.href = "ConferenceList.aspx?t=3";
			   else
			    window.location.href = "ConferenceList.aspx?t=2";
		    }
		    if (document.getElementById("lstCalendar").value == "3"){
			    viewrmdy();
			    //window.location.href = "roomcalendar.aspx?v=1&r=1&hf=&d=&pub=&m=&comp=" ; //code changed for calendar conversion FB 412
		    }
			//FB 2501 Call Monitoring
		    if (document.getElementById("lstCalendar").value == "4"){
                   window.location.href = "MonitorMCU.aspx";
            }       
            //FB 2501 P2P Call Monitoring
		    if (document.getElementById("lstCalendar").value == "5"){
                   window.location.href = "point2point.aspx";
           }
		}
}

if(queryField("hf") == "1")
{

   if(document.getElementById("lstCalendar") != null)
    {
        document.getElementById("lstCalendar").style.display = 'none'  
    		    
    }
}

function WeekOverlap(weekBeginDate) {
		var overlaps = false;
		var week = new Date(weekBeginDate);
		var nextweeks =  new Date(weekBeginDate);
		var weekbegin = new Date();
		weekbegin.setDate(week.getDate() - week.getDay());
		nextweeks.setDate(weekbegin.getDate() + 7);
		var nextweek = new Date(nextweeks);
		if (nextweek.getMonth() != weekbegin.getMonth()) {
			overlaps = true;
		}
		var weeknew = getWeek(weekbegin);
		document.getElementById ("IsWeekChanged").value = "";
		(document.getElementById ("Weeknum").value != weeknew)
		{
		    document.getElementById ("IsWeekChanged").value = "Y";
		    
		}
		
		return overlaps;
		}
		
function getWeek (weekbegin)
 {
 var wkbegin = new Date(weekbegin);
 var onejan = new Date(wkbegin.getFullYear(),0,1);
 return Math.ceil((((wkbegin - onejan) / 86400000) + onejan.getDay()+1)/7);
 }

function WaitforSession()
{
DataLoading("1");
//        if("<%=Session["CalendarMonthly"]%>" == "")//FB 1888
        {
            naptime = 25 * 1000;
            //alert("<%=Session["CalendarMonthly"]%>");
            var sleeping = true;
            var now = new Date();
            var alarm;
            var startingMSeconds = now.getTime();
            while(sleeping){
            alarm = new Date();
            alarmMSeconds = alarm.getTime();
            if(alarmMSeconds - startingMSeconds > naptime){ sleeping = false; }
            }	
         }
}
//FB 2598 Starts
  removeOption();
//FB 2598 Ends
//ZD 100157 Starts
   function  ShowHrs()
   {
       var ShwHrs= document.getElementById("officehrDaily");
       if(!ShwHrs.checked)
       {
           if(document.getElementById("lstStartHrs_Container")!= null)
            document.getElementById("lstStartHrs_Container").style.display="none";
           if(document.getElementById("lstEndHrs_Container")!= null)
            document.getElementById("lstEndHrs_Container").style.display="none";
           }
       else
       {
           if(document.getElementById("lstStartHrs_Container")!= null)
            document.getElementById("lstStartHrs_Container").style.display="inline";
           if(document.getElementById("lstEndHrs_Container")!= null)
            document.getElementById("lstEndHrs_Container").style.display="inline";
       
       } 
   }
   if(document.getElementById("reglstStartHrs") != null)
        document.getElementById("reglstStartHrs").controltovalidate = "lstStartHrs_Text"; 
    if (document.getElementById("reqlstStartHrs") != null)
        document.getElementById("reqlstStartHrs").controltovalidate = "lstStartHrs_Text";
    if (document.getElementById("reqlstEndHrs") != null)
        document.getElementById("reqlstEndHrs").controltovalidate = "lstEndHrs_Text"; 
    if (document.getElementById("reglstEndHrs") != null)
        document.getElementById("reglstEndHrs").controltovalidate = "lstEndHrs_Text";
     
 function fncheckTime() {

        var stdate = '';
        var stime = document.getElementById("lstStartHrs_Text").value;
        var etime = document.getElementById("lstEndHrs_Text").value;
        
        if('<%=Session["timeFormat"]%>' == "2") 
        {
             stime = stime.replace('Z', '')
            stime = stime.substring(0, 2) + ":" + stime.substring(2, 4);
            
            etime = etime.replace('Z', '')
            etime = etime.substring(0, 2) + ":" + etime.substring(2, 4);
        }
        document.getElementById("labErrTime").style.display="none";
        
        if (document.getElementById("lstEndHrs_Text") && document.getElementById("lstStartHrs_Text")) {
            stdate = GetDefaultDate('01/01/1901', '<%=((Session["timeFormat"] == null) ? "1" : Session["timeFormat"])%>');
            
            if (Date.parse(stdate + " " + etime) < Date.parse(stdate + " " + stime)) {
                document.getElementById("labErrTime").style.display="block";
                return false;
                }
            else if (Date.parse(stdate + " " + etime) == Date.parse(stdate + " " + stime)) {
                document.getElementById("labErrTime").style.display="block";
                return false;
                }
            else
                return true;
        }
    }
    
    if(document.getElementById("lstStartHrs_Text") != null)
    {
        if(navigator.userAgent.indexOf('Trident') > -1)
            document.getElementById("lstStartHrs_Text").setAttribute("onblur","javascript:chnghrs();");
        document.getElementById("lstStartHrs_Text").setAttribute("onchange","javascript:chnghrs();");
    }
    if(document.getElementById("lstEndHrs_Text") != null)
    {
        if(navigator.userAgent.indexOf('Trident') > -1)
            document.getElementById("lstEndHrs_Text").setAttribute("onblur","javascript:chnghrs();");
        document.getElementById("lstEndHrs_Text").setAttribute("onchange","javascript:chnghrs();");
    }




function fnUpdtInfo()
{
    
    if(document.getElementById("lstStartHrs_Text") != null)
        document.getElementById("lstStartHrs_Text").style.width="60px";
    if(document.getElementById("lstEndHrs_Text") != null)
        document.getElementById("lstEndHrs_Text").style.width="60px";
    fnTimeScroll();
}
setTimeout("fnUpdtInfo();",100);
ShowHrs();

   //ZD 100157 Ends
</script>
<script type="text/javascript" src="inc/softedge.js"></script>

<% if (Request.QueryString["hf"] != null)
   {
       if(Request.QueryString["hf"] != "1" )
       {
%>
        <div class="btprint">        
        <!-- FB 2719 Starts -->
        <% if (Session["isExpressUser"].ToString() == "1"){%>
        <!-- #INCLUDE FILE="inc/mainbottomNETExp.aspx" -->
        <%}else{%>
        <!-- #INCLUDE FILE="inc/mainbottomNET.aspx" -->
        <%}%>
        <!-- FB 2719 Ends -->
        </div>
<%     } 
   }
%>

