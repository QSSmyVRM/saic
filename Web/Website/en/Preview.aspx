<%--ZD 100147 start--%>
<%--/* Copyright (C) 2013 myVRM - All Rights Reserved	
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.		
*
* You should have received a copy of the myVRM license with	
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/--%><%--ZD 100147 End--%>
<%@ Page Language="c#" AutoEventWireup="false" Inherits="myVRMAdmin.Web.en.Preview" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<html>
<head>
    <title>Preview</title>
    <meta name="Description" content="VRM (Videoconferencing Resource Management) is a revolutionary Web-based software application that manages any video conferencing environment.">
    <meta name="Keywords" content="VRM, Videoconferencing Resource Management, video conferencing, video bridges, video endpoints">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <link title="Expedite base styles" href="Original/Styles/border-table.css" type="text/css"
        rel="stylesheet">
    <link title="Expedite base styles" href="Original/Styles/main-table.css" type="text/css"
        rel="stylesheet">

    <%--code added for Soft Edge button--%>
    <script type="text/javascript">        // FB 2790
        var path = '<%=Session["OrgCSSPath"]%>';
        path = path.split('.')[0] + '<%=Session["ThemeType"]%>' + ".css"; // FB 2815
        document.write('<link rel="stylesheet" title="Expedite base styles" type="text/css" href="' + path + '?' + new Date().getTime() + '">');
    </script>

</head>
<body leftmargin="0" rightmargin="0" bottommargin="0" topmargin="0" scroll="no">
    <form id="Form1" method="get" runat="server">
    <table width="100%">
        <tr>
            <td>
                <font face="Verdana" size="2pt" color="blue">&nbsp;&nbsp;Below are the sample pages.<br>
                    &nbsp;&nbsp;Operations cannot be performed. </font>
            </td>
        </tr>
        <tr height="540">
            <td width="100%">
                <iframe src="Dashboard.aspx" id="previewframe" scrolling="yes" width="100%" height="500"
                    style="text-decoration: none"></iframe>
            </td>
        </tr>
        <tr>
            <td>
                <table cellspacing="0" cellpadding="0" width="100%" border="0" id="Table8">
                    <tbody>
                        <tr>
                            <td align="center">
                                <input type="button" class="altBlueButtonFormat" name="btnLogin"
                                    value="Dashboard Preview" onclick="fnOpen('Dashboard.aspx');" />
                                <input type="button" class="altBlueButtonFormat" name="btnLobby" value="Lobby Preview"
                                    onclick="fnOpen('SettingSelect2.aspx');" />
                                <input type="button" class="altBlueButtonFormat" name="btnvrm" value="Report Details Preview"
                                    onclick="fnOpen('ReportDetails.aspx');" />
                                <input type="button" class="altBlueButtonFormat" name="Close" value="Close" onclick="FnClose()" />
                            </td>
                        </tr>
                    </tbody>
                </table>
            </td>
        </tr>
    </table>
    </form>
</body>
</html>

<script type="text/javascript" language="javascript">
    function FnClose() {
        window.self.close();
    }

    function fnOpen() {
        args = fnOpen.arguments;
        var previewframe = document.getElementById("previewframe");
        previewframe.src = args[0];
    }
</script>

<%--FB 2603 Starts--%>
<script type="text/javascript">
    window.onload = function() {
    
    var iframes = document.getElementsByTagName('iframe');
        
        for (var i = 0, l = iframes.length; i < l; i++) {
        
            var iframe = iframes[i],
            d = document.createElement('div');

            d.style.width = iframe.offsetWidth + 'px';

            d.style.height = iframe.offsetHeight + 'px';

            d.style.top = iframe.offsetTop + 'px';

            d.style.left = iframe.offsetLeft + 'px';

            d.style.position = 'absolute';

            d.style.opacity = '0';

            d.style.filter = 'alpha(opacity=0)';

            d.style.background = 'black';

            iframe.offsetParent.appendChild(d);

        }

    };
    
</script>
<%--FB 2603 Ends--%>

<%--code added for Soft Edge button--%>

<script type="text/javascript" src="inc/softedge.js"></script>

