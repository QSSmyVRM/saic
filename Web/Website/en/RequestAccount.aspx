﻿<%--ZD 100147 start--%>
<%--/* Copyright (C) 2013 myVRM - All Rights Reserved	
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.		
*
* You should have received a copy of the myVRM license with	
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/--%><%--ZD 100147 End--%>			
<%@ Page Language="C#" AutoEventWireup="true" Inherits="en_RequestAccount" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<!--Altered for Window Dressing start-->
 <link rel="stylesheet" title="Expedite base styles" type="text/css" href="css/border-table.css">
  <link rel="stylesheet" title="Expedite base styles" type="text/css" href="css/main-table.css">
<!--Altered for Window Dressing End-->
<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">  
    <title>myVRM</title>
  <meta name="Description" content="myVRM (Videoconferencing Resource Management) is a revolutionary Web-based software application that manages any video conferencing environment.">
  <meta name="Keywords" content="VRM, myVRM, Videoconferencing Resource Management, video conferencing, video bridges, video endpoints">
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <META NAME="LANGUAGE" CONTENT="en">
  <META NAME="DOCUMENTCOUNTRYCODE" CONTENT="us">

  <LINK title="Expedite base styles" href="../en/Organizations/Original/Styles/main.css" type="text/css" rel="stylesheet" /><%--FB 1830--%>
  <script type="text/javascript" src="script/wincheck.js"></script> <%--FB 3055-URL--%>
  <script language="JavaScript1.2" src="inc/functions.js"></script>
    
    <script type="text/javascript"> 		

    function fnLoginTransfer()
    {
        window.location.replace('genlogin.aspx');
        return false;
    }      
        
    function limitDescriptionLen(obj)
	{
		var iKey;
		var eAny_Event = window.event;
		iKey = eAny_Event.keyCode;
		var re 
		re = new RegExp("\r\n","g")  
		x = obj.value.replace(re,"").length ;
		if (x >= obj.maxlength) {
			alert(EN_140);
			obj.value = (obj.value).substr(0, obj.maxlength)
			window.event.returnValue=false;
		}
	}
	
	function chkURL(srcstr)
	{
		pos = 0;
		if (srcstr.indexOf("</url>", pos) != -1) {
			while (srcstr.indexOf("</url>") != -1) {
				pos=srcstr.indexOf("</url>")
				tmpstr = srcstr.substring (0, pos);
				if (tmpstr.split("<url>").length != 2) {
					return false;
					break;
				}
				srcstr = srcstr.substring (pos+6, srcstr.length);
				pos = 0;
			}
		} else {
			if (srcstr.indexOf("<url>", pos) != -1) {
				return false;
			}
		}
		return true;
	}
	
	function fnValidate()
	{
		if ( (document.frmEmailLogin.TxtFirstName.value == "") ) {
				alert("Please enter a First Name.");
				document.frmEmailLogin.TxtFirstName.focus();
				return (false);		
		}
//		else{
//			if(checkInvalidChar(document.frmEmailLogin.TxtFirstName.value) == false){
//				return false;
//			}
//		}
		
		if ( (document.frmEmailLogin.TxtLastName.value == "") ) {
				alert("Please enter a Last Name.");
				document.frmEmailLogin.TxtLastName.focus();
				return (false);		
		}
//		else{
//			if(checkInvalidChar(document.frmEmailLogin.TxtLastName.value) == false){
//				return false;
//			}
//		}
		
		if ( (document.frmEmailLogin.TxtLoginName.value == "") ) {
				 alert("Please enter a User Login Name.");
				document.frmEmailLogin.TxtLoginName.focus();
				return (false);		
		}
//		else{
//			if(!checkInvalidChar(document.frmEmailLogin.TxtLoginName.value))
//				return false;
//		}
		
		// !! email address
		if(!checkInvalidChar(document.frmEmailLogin.TxtEmail.value))
			return false;
			
		if ( !checkemail(document.frmEmailLogin.TxtEmail.value) ) {
			  alert("Please enter a Valid Email Address");
			document.frmEmailLogin.TxtEmail.focus();
			return(false);
		}
	
		if(!checkInvalidChar(document.frmEmailLogin.TxtConfirmEmail.value))
			return false;
			
		if ( !checkemail(document.frmEmailLogin.TxtConfirmEmail.value) ) {
			  alert("Please enter a valid e-mail address.");
			document.frmEmailLogin.TxtConfirmEmail.focus();
			return(false);
		}
		
		if ( document.frmEmailLogin.TxtEmail.value != document.frmEmailLogin.TxtConfirmEmail.value ) {
			alert("Two e-mail addresses do not match.");
			document.frmEmailLogin.TxtConfirmEmail.focus();
			return(false);
		}
		
	
//		if (Trim(document.frmEmailLogin.TxtAdditionalInfo.value) != "") {
//			if(checkInvalidChar(document.frmEmailLogin.TxtAdditionalInfo.value) == false){
//				return false;
//			}
//		}

//		if (!chkURL(document.frmEmailLogin.TxtAdditionalInfo.value)) {
//			 alert("Please enter a valid e-mail address.");
//			document.frmEmailLogin.TxtAdditionalInfo.focus();
//			return false;
//		}


	return true;
	}       
        
    </script>    
</head>
<body>
   <form id="frmEmailLogin" runat="server" method="post">
    <div>
     <table width="90%">
        <tr>
            <td align="center">
            <br /><br /><br /><br /><br />
               <h3>Request a New myVRM User Account</h3>                 
            </td>
        </tr>  
        <tr>
            <td align="center" style="width: 1168px">
                <asp:Label ID="ErrLabel" runat="server" Text="" CssClass="lblError"></asp:Label>
            </td>
        </tr>              
        </table> 
        <table width="90%" border="0" cellpadding="0" cellspacing="0">
           <tr valign="top">              
            <td>
                <table width="100%" border="0" cellpadding="6">        
                    <tr>                                         
                      <td>
                        <table border="0" cellpadding="2" cellspacing="1" width="700">                
                            <tr>
                                <td style="width:150px"></td>
                                <td colspan="2">
                                    <span style="color:Green"><asp:Label Font-Bold="true" ID="LblSuccess" Visible="False" runat="server" Text="Thank you. Your request was successful. Your myVRM Admin will contact you soon."></asp:Label></span>
                                </td>
                            </tr>
                            <tr>
                                <td style="height:5">
                                </td>
                            </tr>
                            <tr>
                              <td style="width:100px"></td>
                              <td align="left" style="width:600px" colspan="2">
                                <%--Window Dressing--%>
                                <span class="subtitleblueblodtext">Please fill out your account details below. An email will be sent to your local myVRM Administrator notifying him of your request.</span><%--FB 2579--%>
                              </td>
                            </tr>
                            <tr>
                              <td colspan="3"  style="height:10"><br /></td>
                            </tr>
                            <tr>
                              <td></td>
                              <td align="left">
                                 <%--Window Dressing--%>
                                <label for="FirstName"  class="blackblodtext">First Name</label>
                              </td>
                              <td>
                                 <%--FB 1888--%>   
                                <asp:TextBox  ID="TxtFirstName" runat="server" CssClass="altText" style="width: 200px;"  MaxLength="256"></asp:TextBox>      
                                <asp:RegularExpressionValidator ID="RegularExpressionValidator2" ControlToValidate="TxtFirstName" ValidationGroup="Submit" Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<br>& < > + % \ / ? | = ! ` [ ] { } # $ @ and ~ are invalid characters." ValidationExpression="^(a-z|A-Z|0-9)*[^\\/<>+?|!`\[\]{}\=@#$%&~]*$"></asp:RegularExpressionValidator> 
                              </td>
                            </tr>
                            <tr>
                              <td></td>
                              <td align="left">
                                 <%--Window Dressing--%>
                                <label for="LastName"  class="blackblodtext">Last Name</label>
                              </td>
                              <td>
                                 <%--FB 1888--%>   
                                <asp:TextBox ID="TxtLastName" runat="server" CssClass="altText" style="width: 200px;" MaxLength="256" ></asp:TextBox>  
                                <asp:RegularExpressionValidator ID="RegularExpressionValidator1" ControlToValidate="TxtLastName" ValidationGroup="Submit" Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<br>& < > + % \ / ? | = ! ` [ ] { } # $ @ and ~ are invalid characters." ValidationExpression="^(a-z|A-Z|0-9)*[^\\/<>+?|!`\[\]{}\=@#$%&~]*$"></asp:RegularExpressionValidator> 
                              </td>
                            </tr>
                            <tr>
                              <td></td>
                              <td align="left">
                                <%--Window Dressing--%>
                                <label for="LoginName" class="blackblodtext">Login Name</label> 
                              </td>
                              <td>
                                <asp:TextBox ID="TxtLoginName" runat="server" CssClass="altText" style="width: 200px;" MaxLength="256"></asp:TextBox>  
                                 <%--FB 1888--%>   
                                <asp:RegularExpressionValidator ID="RegularExpressionValidator3" ControlToValidate="TxtLoginName" ValidationGroup="Submit" Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<br>& < > ' + % \ / ( ) ; ? | ^ = ! ` , [ ] { } : # $ @ ~ and &#34; are invalid characters." ValidationExpression="^(a-z|A-Z|0-9)*[^\\/<>+;?|!`,\[\]{}\x22;=^:@#$%&()'~]*$"></asp:RegularExpressionValidator>
                              </td>
                            </tr>
                            <tr>
                              <td></td>
                              <td align="left">
                                <%--Window Dressing--%>
                                <label for="Email" class="blackblodtext">Email</label> 
                              </td>
                              <td>
                                <asp:TextBox ID="TxtEmail" runat="server" CssClass="altText" style="width: 200px;" MaxLength="256"></asp:TextBox>                               
                                 <%--FB 1888--%>   
                                <asp:RegularExpressionValidator ID="regEmail1_1" ControlToValidate="TxtEmail" ValidationGroup="Submit" Display="dynamic" runat="server" ErrorMessage="<br>Invalid email address." ValidationExpression="^[a-zA-Z()^][()^\w\.-]*[a-zA-Z0-9()^]@[a-zA-Z0-9][\w\.-]*[a-zA-Z0-9]\.[a-zA-Z][a-zA-Z\.]*[a-zA-Z]$"></asp:RegularExpressionValidator>
                              </td>
                            </tr>
                            <tr>
                              <td></td>
                              <td align="left">
                               <%--Window Dressing--%>
                                <label for="Email2" class="blackblodtext">Confirm Email</label> 
                              </td>
                              <td>
                                <asp:TextBox ID="TxtConfirmEmail" runat="server" CssClass="altText" style="width: 200px;" MaxLength="256"></asp:TextBox>  
                                 <%--FB 1888--%>   
                                <asp:RegularExpressionValidator ID="RegularExpressionValidator4" ControlToValidate="TxtConfirmEmail" ValidationGroup="Submit" Display="dynamic" runat="server" ErrorMessage="<br>Invalid email address." ValidationExpression="^[a-zA-Z()^][()^\w\.-]*[a-zA-Z0-9()^]@[a-zA-Z0-9][\w\.-]*[a-zA-Z0-9]\.[a-zA-Z][a-zA-Z\.]*[a-zA-Z]$"></asp:RegularExpressionValidator>
                              </td>
                            </tr>
                            <tr>
                              <td></td>
                              <td valign="top" align="left">
                               <%--Window Dressing--%>
                               <label for="AdditionalInfo" class="blackblodtext">Additional Info</label> 
                              </td>
                              <td>
                               <%--Window Dressing--%>
                                <asp:TextBox ID="TxtAdditionalInfo" class="altText" TextMode="MultiLine" Rows="3" runat="server"  style="width: 300px;" MaxLength="2000"></asp:TextBox> 
                                
                              </td>
                            </tr>
                            <tr>
                              <td colspan="3" style="height:5"></td>
                            </tr>
                            <tr>
                              <td></td>
                              <td colspan="2">
                                <table width="70%">
                                  <tr>
                                    <td align="center">
                                        <input type="button" name="BtnBack" value="Back" class="altShortBlueButtonFormat" onclick="javascript:return fnLoginTransfer();" />
                                    </td>
                                    <td align="center">
                                        <input type="reset" name="BtnReset" value="Reset" class="altShortBlueButtonFormat" />                                        
                                    </td>
                                    <td align="center">
                                        <%--FB 1888--%>   
                                        <asp:Button ID="BtnSubmit" OnClick="SubmitAccount" runat="server" ValidationGroup="Submit" Text="Submit" CausesValidation="True" CssClass="altShortBlueButtonFormat" />
                                    </td>
                                  </tr>
                                </table>
                              </td>
                            </tr>                        
                        </table>           
                     </td>
                  </tr>
             </table>
          </td>
        </tr>
      </table>
    </div>
   </form>
</body>
</html>
<%--code added for Soft Edge button--%>
<script type="text/javascript" src="inc/softedge.js"></script>
<%--FB 2500--%>
<%--<!-- #INCLUDE FILE="inc/mainbottom2.aspx" -->--%>



