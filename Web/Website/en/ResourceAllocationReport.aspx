<%--ZD 100147 start--%>
<%--/* Copyright (C) 2013 myVRM - All Rights Reserved	
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.		
*
* You should have received a copy of the myVRM license with	
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/--%><%--ZD 100147 End--%>
<%@ Page Language="C#" AutoEventWireup="true"   Inherits="en_ResourceAllocation" %>
<%@ Register TagPrefix="cc1" Namespace="Microsoft.Samples.ReportingServices" Assembly="ReportViewer" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ Register Assembly="MetaBuilders.WebControls.ComboBox" Namespace="MetaBuilders.WebControls" TagPrefix="mbcbb" %>
<%
    if(Session["userID"] == null)
      Response.Write("<script>self.close();window.opener.location.replace('genlogin.aspx');</script>");
%>
<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Resource Allocation Report</title>
    <link href="Css/myVRMStyle.css" type="text/css" rel="stylesheet" /> 
    <script type="text/javascript" src="script/wincheck.js"></script> <%--FB 3055-URL--%>
    <script type="text/javascript">        // FB 2790
    var path = '<%=Session["OrgCSSPath"]%>';
    path = path.split('.')[0] + '<%=Session["ThemeType"]%>' + ".css"; // FB 2815
    document.write('<link rel="stylesheet" title="Expedite base styles" type="text/css" href="' + path + '?' + new Date().getTime() + '">');
    </script>
      <link rel="StyleSheet" href="css/divtable.css" type="text/css" />
    <script src="script/mytreeNET.js" language="javascript" type="text/javascript"></script>
    <script type="text/javascript">
  
        var servertoday = new Date(parseInt("<%=DateTime.Now.Year%>", 10), parseInt("<%=DateTime.Now.Month%>", 10)-1, parseInt("<%=DateTime.Now.Day%>", 10),
        parseInt("<%=DateTime.Now.Hour%>", 10), parseInt("<%=DateTime.Now.Minute%>", 10), parseInt("<%=DateTime.Now.Second%>", 10));
        
    </script>



    <script language="javascript" src='script/lib.js'></script>
<script language="VBScript" src="script/lotus.vbs"></script>
<script type="text/javascript" src="inc/functions.js"></script>
<script type="text/javascript" src="script/cal.js"></script>
<script type="text/javascript" src="script/mousepos.js"></script>

<script type="text/javascript" src="lang/calendar-en.js"></script>
<script type="text/javascript" src="script/calendar-setup.js"></script>

  
<script type="text/javascript" >   
      
        
        function fnSelectAll()
        {
            var args = fnSelectAll.arguments;
            var rooms = '';
            if(args[0] != null)
            {
                if(args[1] != null)
                {
                    var this_ = document.getElementById(args[1]);
                   
                   
                    fnAssignChecked(this_,args[0].checked);
                }
            }
        }        
       
        function fnAssignChecked(this_,checkValue)
        {
            
            if(this_ != null)
            {
                 var checkBoxArray = this_.getElementsByTagName('input');
                 
                 for (var i=0; i<checkBoxArray.length; i++) 
                 { 
                     var checkBoxRef = checkBoxArray[i];
                     
                     checkBoxRef.checked = checkValue;                                 
                 }


            }
        }
        
        function fnDeselectAll()
        {
            var args = fnDeselectAll.arguments;
            
            if(args[0] != null)
            {
                if(!args[0].checked)
                {
                    if(args[1] != null)
                    {
                        var allCheck = document.getElementById(args[1])
                        if(allCheck)
                        {
                            if(allCheck.checked)
                                allCheck.checked = false;
                        }
                    }
                }        
            }
        }
        
        function fnAssignValue()
        {
            document.form1.txtRooms.value = ''
            document.form1.hdnRoomIDs.value = ''
            
            var args = fnAssignValue.arguments;
            var rooms = '';
           
            if(args[0] != null)
            {
                var this_ = document.getElementById(args[0])
                var checkBoxArray = this_.getElementsByTagName('input');
                var checkedValues = '';
           
                for (var i=0; i<checkBoxArray.length; i++) 
                { 
                     var checkBoxRef = checkBoxArray[i];
                     if(checkBoxRef.checked)
                     {
                            
                        var labelArray = checkBoxRef.parentElement.getElementsByTagName('label');
            
                        if ( labelArray.length > 0 )
                        {
                            if (rooms.length > 0 )
                                rooms += ', ';

                            rooms += labelArray[0].innerHTML;
                         }
                     }                            
                }
                
                if(rooms != '')
                    document.form1.txtRooms.value = rooms;
                
            }
            
        }
   
     
        
        function fnValidate()
        {
            
             // regular expression to match required time format
           
            var msg;     
            var startDate;
            var startTime;
            var endDate;
            var endTime;
            var index;
            var textValue;
           
          
         
                if(document.form1.txtStartDate.value != "" && document.form1.txtEndDate.value == "")
                {
                    alert("Please enter the To date");
                    document.form1.txtEndDate.focus();
                    return false;
                    
                }
                else if(document.form1.txtEndDate.value != "" && document.form1.txtStartDate.value == "")
                {
                    alert("Please enter the From date");
                    document.form1.txtStartDate.focus();
                    return false;
                    
                }
        
            textValue = document.form1.txtStartDate.value;
            index = textValue.length;
            startDate = textValue.substring(0,index);
            textValue = document.form1.txtEndDate.value;
            index = textValue.length;
            endDate = textValue.substring(0,index);   
            if ('<%=format%>'=='dd/MM/yyyy') // dd/MM/yyyy
            {
           
                var mDay = startDate.substring(0,2);
                var mMonth = startDate.substring(3,5);
                var mYear = startDate.substring(6,10);
                var strSeperator='/';
                startDate = mMonth+strSeperator+mDay+strSeperator+mYear;  
                var emDay = endDate.substring(0,2);
                var emMonth = endDate.substring(3,5);
                var emYear = endDate.substring(6,10);              
                endDate = emMonth+strSeperator+emDay+strSeperator+emYear; 
                 
            }                     
            
           //code changed for FB 1426 
            //timeValue = document.form1.CmbStrtTime.value;
            timeValue = document.form1.CmbStrtTime_Text.value;
            if(timeValue!="")
                startTime = timeValue.substring(0,textValue.length);
         
            //code changed for FB 1426 
            //timeValue = document.form1.CmbEndTime.value;
            timeValue = document.form1.CmbEndTime_Text.value;
            if(timeValue!="")
                endTime = timeValue.substring(0,timeValue.length);
            
           var compareVal = fnCompareDate(startDate,endDate)
            
           
            if(compareVal == "L")
            {
                alert("From Date should be lesser than To date.");
                return false;
            }
            else
            {
                
                if(compareVal == "E")
                {
                    if((startTime!="")&&(endTime !=""))
                    {
                        var start = startTime.split(':');
                        var end = endTime.split(':');
                        
                        var stTime= start[1].substring(start[1].length,2);
                        var endTime= end[1].substring(end[1].length,2);
                        
                          if((stTime==" PM")&&(endTime==" AM"))
                            {                               
                                alert("From Time should be lesser than To time.");
                                return false;
                              
                            }
                         else if(((stTime ==" AM")&&(endTime==" AM"))||((stTime==" PM")&&(endTime==" PM")))
                            {
                            if(start[0]!="12")
                                {
                                    if(start[0] >  end[0])
                                    {                                
                                        alert("From Time should be lesser than To time.");
                                        return false;
                                    }
                                }
                            }
                         else
                         {
                            if(start[0] > end[0])
                            {
                                alert("From Time should be lesser than To time.");
                                return false;
                            }
                            else if(start[0] == end[0])
                            {
                                if(start[1] > end[1])
                                {
                                    alert("From Time should be lesser than To time.");
                                    return false;
                                }
                            }
                         }
                    }
                }
            }

           if(document.form1.drpTimeZone.value<0)
           {
                alert("Please select the TimeZone");
                return false;
           }
            
            return true;
        }
              
    </script>

</head>
 <body style="margin: 0px; overflow:hidden" >
    <form id="form1" runat="server">     
    <input type="hidden" name="hdnRoomIDs" id="hdnRoomIDs" runat="server" />
            <table width="100%" class="msrs-normal" cellpadding="0" cellspacing="0" ID="Table8">      
			    <tr>
				    <td>
                        <div >
		                    <table cellpadding="0" cellspacing="0" width="100%" class="ParametersFrame ParamsGrid MenuBarBkGnd">
			                    <tr>
				                    <td width="100%" height="100%">
				                        <table cellpadding="5" border="0" width="100%">
					                        <tr>
						                        <td class="ParamLabelCell"><span>From Date:</span></td>
						                        <td class="ParamEntryCell" style="padding-right:0px;" >
						                           <asp:textbox id="txtStartDate" MaxLength="20" Runat="server" Width="120"></asp:textbox> 
						                          <img src="image/calendar.gif" border="0" width="20" height="20" id="cal_triggerd" style="cursor: pointer;" title="Date selector" onblur="javascript:ChangeStartDate(0)" onclick="return showCalendar('<%=txtStartDate.ClientID %>', 'cal_triggerd', 0, '<%=format%>');" /><br />
						                        </td>
						                        <td class="InterParamPadding">
						                        </td>
						                        <td class="ParamLabelCell"><span>To Date:</span></td>
						                        <td class="ParamEntryCell" style="padding-right:0px;">
						                            <asp:textbox id="txtEndDate" MaxLength="20" Runat="server" Width="120"></asp:textbox> 
						                            <img src="image/calendar.gif" border="0" width="20" height="20" id="cal_trigger1" style="cursor: pointer;" title="Date selector" onblur="javascript:ChangeEndDate(0)" onclick="return showCalendar('<%=txtEndDate.ClientID %>', 'cal_trigger1', 0, '<%=format%>');" /><br />						                       
						                        </td>
						                       		                         
					                        </tr>
					                          <tr>
                                                <td align="center" colspan="2">
                                                    <div id="flatCalendarDisplay" style="z-index:999;"></div>
                                                    <div id="flatCalendarDisplay1" style="z-index:999;"></div>
                                                </td>
                                            </tr>
					                        <tr>
					                        <td class="ParamLabelCell"><span>From Time:</span></td>
					                        <td>
					                           <mbcbb:ComboBox id="CmbStrtTime" runat="server" BackColor="White" CssClass="altSelectFormat" onblur="javascript:ChangeStartDate(0)" Rows="10" Width="100px" CausesValidation="True" AutoPostBack="false">					                        
                                                <asp:ListItem Value="01:00 AM"></asp:ListItem>
                                                <asp:ListItem Value="02:00 AM"></asp:ListItem>
                                                <asp:ListItem Value="03:00 AM"></asp:ListItem>
                                                <asp:ListItem Value="04:00 AM"></asp:ListItem>
                                                <asp:ListItem Value="05:00 AM"></asp:ListItem>
                                                <asp:ListItem Value="06:00 AM"></asp:ListItem>
                                                <asp:ListItem Value="07:00 AM"></asp:ListItem>
                                                <asp:ListItem Value="08:00 AM"></asp:ListItem>
                                                <asp:ListItem Value="09:00 AM"></asp:ListItem>
                                                <asp:ListItem Value="10:00 AM"></asp:ListItem>
                                                <asp:ListItem Value="11:00 AM"></asp:ListItem>
                                                <asp:ListItem Value="12:00 PM"></asp:ListItem>
                                                <asp:ListItem Value="01:00 PM"></asp:ListItem>
                                                <asp:ListItem Value="02:00 PM"></asp:ListItem>
                                                <asp:ListItem Value="03:00 PM"></asp:ListItem>
                                                <asp:ListItem Value="04:00 PM"></asp:ListItem>
                                                <asp:ListItem Value="05:00 PM"></asp:ListItem>
                                                <asp:ListItem Value="06:00 PM"></asp:ListItem>
                                                <asp:ListItem Value="07:00 PM"></asp:ListItem>
                                                <asp:ListItem Value="08:00 PM"></asp:ListItem>
                                                <asp:ListItem Value="09:00 PM"></asp:ListItem>
                                                <asp:ListItem Value="10:00 PM"></asp:ListItem>
                                                <asp:ListItem Value="11:00 PM"></asp:ListItem>
                                                <asp:ListItem Value="12:00 AM"></asp:ListItem>
                                             </mbcbb:ComboBox>
                                             </td>
                                             <td class="InterParamPadding">
						                     </td>
                                             <td class="ParamLabelCell"><span>To Time:</span></td>
					                        <td>
					                           <mbcbb:ComboBox id="CmbEndTime" runat="server" BackColor="White" CssClass="altSelectFormat" onblur="javascript:ChangeStartDate(0)" Rows="10" Width="100px" CausesValidation="True" AutoPostBack="false">					                        
                                                <asp:ListItem Value="01:00 AM"></asp:ListItem>
                                                <asp:ListItem Value="02:00 AM"></asp:ListItem>
                                                <asp:ListItem Value="03:00 AM"></asp:ListItem>
                                                <asp:ListItem Value="04:00 AM"></asp:ListItem>
                                                <asp:ListItem Value="05:00 AM"></asp:ListItem>
                                                <asp:ListItem Value="06:00 AM"></asp:ListItem>
                                                <asp:ListItem Value="07:00 AM"></asp:ListItem>
                                                <asp:ListItem Value="08:00 AM"></asp:ListItem>
                                                <asp:ListItem Value="09:00 AM"></asp:ListItem>
                                                <asp:ListItem Value="10:00 AM"></asp:ListItem>
                                                <asp:ListItem Value="11:00 AM"></asp:ListItem>
                                                <asp:ListItem Value="12:00 PM"></asp:ListItem>
                                                <asp:ListItem Value="01:00 PM"></asp:ListItem>
                                                <asp:ListItem Value="02:00 PM"></asp:ListItem>
                                                <asp:ListItem Value="03:00 PM"></asp:ListItem>
                                                <asp:ListItem Value="04:00 PM"></asp:ListItem>
                                                <asp:ListItem Value="05:00 PM"></asp:ListItem>
                                                <asp:ListItem Value="06:00 PM"></asp:ListItem>
                                                <asp:ListItem Value="07:00 PM"></asp:ListItem>
                                                <asp:ListItem Value="08:00 PM"></asp:ListItem>
                                                <asp:ListItem Value="09:00 PM"></asp:ListItem>
                                                <asp:ListItem Value="10:00 PM"></asp:ListItem>
                                                <asp:ListItem Value="11:00 PM"></asp:ListItem>
                                                <asp:ListItem Value="12:00 AM"></asp:ListItem>
                                              </mbcbb:combobox>
                                                </td>                                                
					                        </tr>
					                         <%--Code Modified For FB 1425--%>
					                        <tr id="trTimeZone" runat="server">
						                        <td class="ParamLabelCell"><span>Time Zone:</span></td>
						                        <td class="ParamEntryCell" style="padding-right:0px;">						
						                            <asp:DropDownList ID="drpTimeZone" runat="server" DataTextField="timezoneName" DataValueField="timezoneID" ></asp:DropDownList>
						                        </td>
    				                            <td class="InterParamPadding"></td>    				                        
				                            </tr>			                     
				                            
			                          </table>
				                  </td>
			                      <td width="100px">&nbsp;</td>
			                      <td class="SubmitButtonCell">
			                        <table ID="Table12">
				                        <tr>
					                        <td>
					                            <asp:Button ID="btnviewRep"  runat="server" Text="View Report" />
					                        </td>
				                        </tr>
			                        </table>
			                      </td>
			                    </tr>
		                    </table>
		                </div>		             
                    </td>
                 </tr>               
             </table>      		                    
            <iframe width=80 height=178 name="gToday:normal:agenda.js" id="gToday:normal:agenda.js" src='<%=ResolveUrl("~/en/Javascripts/ipopeng.htm")%>' scrolling="no" frameborder="0" style="Z-INDEX:999; LEFT:-500px; VISIBILITY:visible; POSITION:absolute; TOP:-500px"></iframe>
            <table cellSpacing="0" cellPadding="0" width="100%" background="Images/bg.jpg" border="0">
	            <tbody>
                    <tr bgColor="#f8f8ff">
                        <td width="100%" colspan="3">
	                        <table class="tblHeader" cellSpacing="0" cellPadding="0" width="100%" align="center" bgColor="#f8f8ff"
		                        border="0">
		                        <tr>
			                        <td vAlign="top" width="100%">
			                            <cc1:reportviewer id="ReportViewer1" runat="server" overflow="auto" Height="500px" Visible="False"
					                        align="center" Width="100%" background="#f8f8ff"></cc1:reportviewer></td>
		                        </tr>
	                        </table>
                        </td>
                    </tr>
                </tbody>
           </table>
     
    </form>
 
</body>
</html>
