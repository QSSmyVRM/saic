<%--ZD 100147 start--%>
<%--/* Copyright (C) 2013 myVRM - All Rights Reserved	
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.		
*
* You should have received a copy of the myVRM license with	
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/--%><%--ZD 100147 End--%>
<%@ Page Language="C#" AutoEventWireup="true" EnableEventValidation="false" Inherits="RoomCalendar" ValidateRequest="false" %> <%--ZD 100170--%>
<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>
<%@ Register Assembly="DayPilot" Namespace="DayPilot.Web.Ui" TagPrefix="DayPilot" %>
<%@ Register Assembly="DevExpress.Web.v10.2, Version=10.2.3.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxTabControl" TagPrefix="dxtc"%>
<%@ Register Assembly="DevExpress.Web.v10.2, Version=10.2.3.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxClasses" tagprefix="dxw"%>
<%@ Register Assembly="MetaBuilders.WebControls.ComboBox" Namespace="MetaBuilders.WebControls"
    TagPrefix="mbcbb" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> <!-- FB 2050 -->
<meta http-equiv="X-UA-Compatible" content="IE=8" /> <!-- FB 2050 --><!-- FB 2804 -->
<script language="javascript" src="script/CallMonitorJquery/jquery.1.4.2.js"></script>
<%
    if(Session["userID"] == null)
    {
        Response.Redirect("~/en/genlogin.aspx"); //FB 1830

    }
    else
    {
        Application.Add("COM_ConfigPath", "C:\\VRMSchemas_v1.8.3\\ComConfig.xml");
        Application.Add("MyVRMServer_ConfigPath", "C:\\VRMSchemas_v1.8.3\\");
    }
%>

<% 
Boolean showCompanyLogo = false;
if(Request.QueryString["hf"] != null)
{
    if(Request.QueryString["hf"].ToString() == "1")
    {
	    showCompanyLogo = false;
%>
    <!-- #INCLUDE FILE="inc/maintop2.aspx" --> 

<%
    }
    else 
    {
%>

<!-- FB 2719 Starts -->
<% if (Session["isExpressUser"].ToString() == "1"){%>
<!-- #INCLUDE FILE="inc/maintopNETExp.aspx" -->
<%}else{%>
<!-- #INCLUDE FILE="inc/maintopNET.aspx" -->
<%}%>
<!-- FB 2719 Ends -->

<% }

}%>


<%--FB 3055-URL Starts--%>
    <script>
        var ar = window.location.href.split("."); // ZD 100170
    ar = ar[ar.length - 1];
    var pg = "";
    if (ar.indexOf("?") > -1)
        pg = ar.split("?")[1];
    else
        pg = ar;

    var page = pg.toLowerCase();
    //alert(page);
    var cnt = 0;
    
    if (page.indexOf("v=1&r=1&hf=&d=&pub=&m=&comp=") > -1)
        cnt++;
    if (page.indexOf("f=v&hf=1&m=") > -1)
        cnt++;

    if (cnt == 0)
        window.location.href = "thankyou.aspx";
    </script>
<%--FB 3055-URL End--%>

<script type="text/javascript">    // FB 2790
    var path = '<%=Session["OrgCSSPath"]%>';
    path = path.split('.')[0] + '<%=Session["ThemeType"]%>' + ".css"; // FB 2815
    document.write('<link rel="stylesheet" title="Expedite base styles" type="text/css" href="' + path + '?' + new Date().getTime() + '">');
</script>

<script type="text/javascript">
    // ZD 100335 start
    function setCookie(c_name, value, exdays) {
        var exdate = new Date();
        exdate.setDate(exdate.getDate() + exdays);
        var c_value = escape(value) + ((exdays == null) ? "" : "; expires=" + exdate.toUTCString());
        document.cookie = c_name + "=" + c_value;
    }
    setCookie("hdnscreenres", screen.width, 365);
    //ZD 100335 End

function noError(){return true;} // FB 2050
window.onerror = noError;

  var servertoday = new Date(parseInt("<%=DateTime.Now.Year%>", 10), parseInt("<%=DateTime.Now.Month%>", 10)-1, parseInt("<%=DateTime.Now.Day%>", 10),
  parseInt("<%=DateTime.Now.Hour%>", 10), parseInt("<%=DateTime.Now.Minute%>", 10), parseInt("<%=DateTime.Now.Second%>", 10));
  
  
</script>
<html id="Html1" runat="server"  xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <script type="text/javascript" src="script/errorList.js"></script>
    <script type="text/javascript" language="JavaScript" src="inc/functions.js"></script>
    <script type="text/javascript" src="extract.js"></script>
    <script type="text/javascript" src="script\mousepos.js"></script>
    <script type="text/javascript" src="script\showmsg.js"></script>
    <script type="text/javascript" src="script\roomsearch.js"></script>
    <script type="text/javascript" src="script/mytreeNET.js"></script>
    <script type="text/javascript" src="script/cal-flat.js"></script>
    <script type="text/javascript" src="lang/calendar-en.js"></script>
    <script type="text/javascript" src="script/calendar-flat-setup.js"></script>
    <script language="javascript1.1" src="script/calview.js"></script>
    <link rel="StyleSheet" href="css/divtable.css" type="text/css" />
    <%--<link rel="stylesheet" type="text/css" media="all" href="css/aqua/theme.css" title="Aqua" />--%>
    <link rel="stylesheet" type="text/css" media="all" href="css/calendar-blue.css" /> <%--FB 1861--%> <%--FB 1982--%>
    <title>Room Calendar</title>
</head>
<body>
  <form id="frmCalendarRoom" runat="server" >

   <asp:ScriptManager ID="CalendarScriptManager" runat="server" AsyncPostBackTimeout="600">
                 </asp:ScriptManager>
  
    <input type="hidden" id="cmd" value="GetSettingsSelect" />
    <input type="hidden" id="settings2locstr" name="settings2locstr" value="" runat="server" />
    <input type="hidden" id="helpPage" value="84" />
    <input type="hidden" id="hdnRoomIDs" runat="server" />
    <input type="hidden" id="MonthNum"  />
    <input type="hidden" id="isViewChanged" runat="server" />
    <input type="hidden" id="IsWeekOverLap" runat="server" />
    <input type="hidden" id="YrNum"  />
    <input type="hidden" id="IsWeekChanged" runat="server" />     
    <input type="hidden" id="Weeknum"  />
    <input type="hidden" id="selStatus" runat="server" /><%--FB 2804--%>
    <input type="hidden" id="selectedRooms"/><%--FB 2804--%>
    <input type="hidden" id="selectedListViewRooms"/><%--FB 2804--%>
    <input type="hidden" id="hdnRoomType"/><%--FB 2804--%>
    <input type="hidden" id="hdnRoomView"/>   
    <input type="hidden" id="hdnlstStartHrs"/>   
    <input type="hidden" id="hdnlstEndHrs"/>   
  
  <%--FB 1630 - Changes--%>
  <table align="center"> <%--Added for FF--%>
       <tr>
        <td>
  <div id="footer" style="width:100%;">
    <table border="0"  width="100%" align="left"  style="vertical-align:bottom;">
        <tr>
            <td colspan="12">
              <%--<asp:Label ID="errLabel" runat="server" Text="" CssClass="lblError"></asp:Label>--%> <%--ZD 100157--%>
                <h3>Room Calendar<%--Edited For FF--%>     <%--<input type="button" name="PrsCal"  onfocus="this.blur()" value="Switch to Personal Calendar" class="altShortBlueButtonFormat" style="width:195px" onclick="javascript:viewrmdy(1)" /> </h3>--%>
                <select id="lstCalendar" name="lstCalendar" class="altText" size="1" onChange="goToCal();javascript:DataLoading1('1');" runat="server"> <%--FB 2058--%>
                    <option value="3">Room Calendar View</option>          		                   
                    <option value="1">Personal Calendar View</option>
                    <option value="2">List View</option>
                </select>
            </td>
        </tr>
        <tr>
            <td><asp:Label ID="errLabel" runat="server" Text="" CssClass="lblError"></asp:Label></td> <%--ZD 100157--%>
            <td id="tdSelectRooms" style="display:none"><%--FB 2804--%>
                <span class="lblError" style="margin-left: 100px;">Please Select Rooms</span>               
            </td>
            <td id="tdClosed" runat="server" style="display: none">
                <span class="lblError" style="margin-left: 100px;">Rooms Closed to Scheduling</span>
            </td>
        </tr>
    </table>
  </div>
</td> <%--Added for FF--%>
      </tr>
     </table> 
  
  <%
if (Request.QueryString["hf"] != null)
{
    if (Request.QueryString["hf"].ToString() == "1")
    {
    %>
    <table width="100%" border="0">
        <tr><td align="center"><%--Edited For FF--%>
	        <input type="button" name="close" onfocus="this.blur()" id="close" value="Close" class="altShort2BlueButtonFormat" onclick="javascript:window.close();">
<%     if (Request.QueryString["pub"] != null)
       {if (Request.QueryString["pub"].ToString() == "1")
        {%>
            <input type="button" name="login" id="login" value="Login" class="altShort2BlueButtonFormat" onclick="javascript:window.location.href='~/en/genlogin.aspx'"> <%--FB 1830--%>
        </td></tr></table>
      <%}
        else
        {%> 
         </td></tr></table>
      <%}
      }
      else
      {%> 
        </td></tr></table>
    <%}
    }
  }%>
  <table width="100%" border="0" style="vertical-align:super;">
    <tr valign="top">
        <td id="ActionsTD" valign="top" align="right" style="width:15%" runat="server">
            <div align="center" id="othview">
                <table border="0" width="100%"   class="treeSelectedNode"> 
                    <tr id="trofficehrDaily" style="display:block">
                        <td align="left">
                         <table border="0" style="border-collapse:collapse; text-align:left" >
                            <tr>
                                <td>
                         
                         
                            <asp:CheckBox id="officehrDaily"  onclick="javascript:chnghrs();ShowHrs();fnStartDetectChange();"  AutoPostBack="false" runat="server" tooltip="show office hour only" />
                            <label class="blackblodtext"  id="lblhour">Show Hours</label>
                            <label class="rmnamediv" for="officehr" id="lblOfficehrs" title="show office hours only" style="display:none;">Show Office Hours Only</label>
                            <%--ZD 100157 Starts--%>
                            
                                </td><td>
                                &nbsp;&nbsp;&nbsp;&nbsp;
                            <mbcbb:combobox id="lstStartHrs" cssclass="altText" causesvalidation="true"
                        runat="server" enabled="true" rows="15" >
                    </mbcbb:combobox> 
                    <asp:RequiredFieldValidator ID="reqlstStartHrs" runat="server" ControlToValidate="lstStartHrs"
                        Display="Dynamic" ErrorMessage="Time is Required"></asp:RequiredFieldValidator>
                    <asp:RegularExpressionValidator ID="reglstStartHrs" runat="server" ControlToValidate="lstStartHrs"
                        Display="Dynamic" ErrorMessage="Invalid Time (HH:MM AM/PM)"
                        ValidationExpression="[0-1][0-9]:[0-5][0-9] ?[A|a|P|p][M|m]"></asp:RegularExpressionValidator>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2"></td>
                            </tr>
                            <tr>
                                <td></td>
                                <td style="text-align:right">
                    <mbcbb:combobox id="lstEndHrs" cssclass="altText" causesvalidation="true"
                        runat="server" enabled="true" rows="15" >
                    </mbcbb:combobox> 
                    <asp:RequiredFieldValidator ID="reqlstEndHrs" runat="server" ControlToValidate="lstEndHrs"
                        Display="Dynamic" ErrorMessage="Time is Required"></asp:RequiredFieldValidator>
                    <asp:RegularExpressionValidator ID="reglstEndHrs" runat="server" ControlToValidate="lstEndHrs"
                        Display="Dynamic" ErrorMessage="Invalid Time (HH:MM AM/PM)"
                        ValidationExpression="[0-1][0-9]:[0-5][0-9] ?[A|a|P|p][M|m]"></asp:RegularExpressionValidator>
                         <%--ZD 100157 Ends--%>
                        
                                </td>
                            </tr>
                         </table>
                        
                        </td>
                    </tr>
                    <tr id="trofficehrWeek" style="display:none">
                        <td align="left"  nowrap>
                            <asp:CheckBox id="officehrWeek" Checked="true" onclick="javascript:chnghrs();"   AutoPostBack="false" runat="server" tooltip="show office hour only" />
                            <label class="blackblodtext"  id="lblweek">Show Weekdays Only</label>
                            <label class="rmnamediv" for="officehr" id="Label1" title="show office hours only"  style="display:none;">Show Weekdays Only</label>
                        </td>
                    </tr>
                    <tr id="trofficehrMonth" style="display:none">
                        <td align="left"  nowrap>
                            <asp:CheckBox id="officehrMonth" Checked="true" onclick="javascript:chnghrs();"  AutoPostBack="false" runat="server" tooltip="show week days only" />
                            <label class="blackblodtext"  id="lblmonth">Show Weekdays Only</label>
                            <label class="rmnamediv" for="officehr" id="Label2" title="show office hours only" style="display:none;">Show Weekdays Only</label>
                        </td>
                    </tr>
                    <tr><%--FB 1800--%>
                      <td valign="top" align="left">
                        <asp:CheckBox ID="showDeletedConf" runat="server" onclick="javascript:datechg();"/>
                        <span class="blackblodtext">Deleted Conferences</span>
                      </td>
                    </tr>
                    <tr>
                        <td class="tableHeader"  align="left">Calendar</td>
                    </tr>
                    <tr height="180" align="left"  valign="baseline">
                        <td> &nbsp;<div id="flatCalendarDisplay" align="center"></div><div id="subtitlenamediv"></div></td>
                    </tr>
                    <tr align="center">
                        <td align="center"><hr style="height:2px;color:Black;" /></td>
                    </tr>
                    <tr>
                        <td class="tableHeader"  align="left">Rooms</td>
                    </tr>
                    <tr>
                        <td align="left"><a id="RoomPoupup" runat="server" href="#" onclick="OpenRoomSearchCalen('frmCalendarRoom');" >Advanced Search</a></td> <!-- FB 2050 -->
                    </tr>
                    <tr>
                        <td>
                            <table align="right">
                                <tr>
                                    <td align="left" style="width:100%;">
                                        <asp:UpdatePanel ID="UpdatePanel1" runat="server" RenderMode="Inline" UpdateMode="Conditional">
                                            <Triggers><asp:AsyncPostBackTrigger ControlID="btnfrmSearch" /></Triggers>
                                            <ContentTemplate>
                                                <asp:Panel style="display:block;" ID="PopupRoomPanel" Width="220px" runat="server" HorizontalAlign="Left"  CssClass="treeSelectedNode">
                                                    <input runat="server" id="selectedloc" type="hidden" />
                                                    <div align="left" id="conftypeDIV" style="width:100%;Height:160px;" class="treeSelectedNode"> <%--ZD 100157--%>
                                                        <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                            <tr>
                                                                <td align="left" class="blackblodtext" valign="top" width="100%">
                                                                    <table border="0" style="width: 100%">
                                                                        <tr>
                                                                            <td valign="top" align="left">                              
                                                                                <input type="button" value="Compare" id="btnCompare" runat="server" onclick="javascript:comparesel();" class="altMedium0BlueButtonFormat" />
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td valign="top" align="left" class="blackblodtext">
                                                                              <asp:RadioButtonList ID="rdSelView" runat="server" CssClass="blackblodtext"
                                                                                  RepeatDirection="Horizontal" AutoPostBack="true" RepeatLayout="Flow" ToolTip="Click to show room resources" OnSelectedIndexChanged="rdSelView_SelectedIndexChanged">
                                                                                  <asp:ListItem Selected="True" Value="1"><span class="blackblodtext">Level</span></asp:ListItem>
                                                                                  <asp:ListItem  Value="2"><span class="blackblodtext">List</span></asp:ListItem>
                                                                               </asp:RadioButtonList>
                                                                            </td>
                                                                        </tr>
                                                                    </table>                    
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td width="100%" align="left" style="font-weight: bold; font-size: small; color: green; font-family: arial;display:block" valign="top" >
                                                                    <asp:Panel ID="pnlLevelView" runat="server" Width="220px" ScrollBars="Auto"  Height="100px"  HorizontalAlign="Left" Visible="false" BorderColor="Blue" BorderStyle="Solid" BorderWidth="1px"><%--Edited For FF--%> <%--ZD 100157--%>
                                                                        <asp:TreeView ID="treeRoomSelection" runat="server"  BorderColor="White" ShowCheckBoxes="All" onclick="javascript:getTabCalendarLocal(event)"
                                                                            ShowLines="True" Width="100%"  >
                                                                            <NodeStyle CssClass="tabtreeNode" />
                                                                            <RootNodeStyle Font-Size="XX-Small" CssClass="tabtreeRootNode" />
                                                                            <ParentNodeStyle Font-Size="XX-Small" CssClass="tabtreeParentNode" />
                                                                            <LeafNodeStyle Font-Size="XX-Small" CssClass="tabtreeLeafNode" />
                                                                        </asp:TreeView>
                                                                    </asp:Panel>
                                                                    <asp:Panel ID="pnlListView" runat="server" BorderColor="Blue" BorderStyle="Solid"
                                                                        BorderWidth="1px"  Height="100px" ScrollBars="Auto" Width="100%" HorizontalAlign="Left" Direction="LeftToRight" Font-Bold="True" Font-Names="arial" Font-Size="Small" ForeColor="Green" Visible="false"> <%--ZD 100157--%>
                                                                        
                                                                        <input type="checkbox" id="selectAllCheckBox" runat="server" onclick="CheckBoxListSelectTab('lstRoomSelection',this);"  /><font size="2"> Select All</font>
                                                                        <br />
                                                                        <asp:CheckBoxList ID="lstRoomSelection" runat="server" Width="95%" Font-Size="Smaller" ForeColor="ForestGreen" onclick="javascript:datechg();" Font-Names="arial" RepeatLayout="Flow">
                                                                        </asp:CheckBoxList>
                                                                    </asp:Panel>                              
                                                                    <asp:Panel ID="pnlNoData" runat="server" BorderColor="Blue" BorderStyle="Solid"
                                                                        BorderWidth="1px" Height="290px" ScrollBars="Auto" Visible="False" Width="100%" HorizontalAlign="Left" Direction="LeftToRight" Font-Size="Small">
                                                                        <table><tr align="center"><td>
                                                                        You have no Room(s) available. 
                                                                        </td></tr></table>                            
                                                                    </asp:Panel>  
                                                                    <asp:TextBox runat="server" ID="txtTemp" Text="test" Visible="false" ></asp:TextBox>
                                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" Enabled="false" ControlToValidate="txtTemp" runat="server" ></asp:RequiredFieldValidator>
                                                                </td>
                                                            </tr>                  
                                                        </table>
                                                    </div>
                                                    <input align="middle" type="button" runat="server" style="width:150px;display:none;" id="ClosePUp" value=" Close " class="altButtonFormat" />
                                                    <asp:Button ID="btnfrmSearch" style="display:none" OnClick="FromRoomSearch" runat="server"/>
                                                </asp:Panel> 
                                           </ContentTemplate>
                                         </asp:UpdatePanel>
                                    </td>
                                </tr>
                            </table>
                        </td> 
                    </tr>
                </table>
            </div>
          </td>
         
          <td  style="width:2%"></td> 
          
          <td style="width:85%" valign="top">
             <asp:UpdatePanel ID="PanelTab" runat="server" RenderMode="Inline" UpdateMode="Conditional" >
                <Triggers><asp:AsyncPostBackTrigger ControlID="btnDate" /></Triggers>
                <ContentTemplate>
                    <input type="hidden" id="HdnMonthlyXml" runat="server" />
     <input runat="server" id="IsSettingsChange" type="hidden" />
                    <div id="dataLoadingDIV" name="dataLoadingDIV" align="center"></div> 
                    <input runat="server" id="selectedList" type="hidden" />
    <input type="hidden" id="IsMonthChanged" runat="server" />
                    <input name="locstrname" type="hidden" id="locstrname" runat="server"  /> <!--Added room search-->
                   
                    <input name="Sellocstrname" type="hidden" id="Sellocstrname" runat="server"  />
                    <dxtc:ASPxPageControl ID="CalendarContainer" runat="server" ActiveTabIndex="0" Width="99%" EnableHierarchyRecreation="True">
                        <ClientSideEvents ActiveTabChanged="function(s,e){ChangeTabsIndex(s,e);}" />
                        <TabPages>
                             <dxtc:TabPage Text="Daily View" Name="Daily">
                                <ContentCollection>
                                    <dxw:ContentControl ID="ContentControl1" runat="server">
                                        <table style="width:90%;vertical-align:top;overflow:auto;height:450px" border="0" cellpadding="0" cellspacing="0"> <%--ZD 100157--%>
                                            <tr valign="top" style="height:15px" id="trlegend1" runat="server"> <%--FB 1985 FB 2050--%>
                                                <td>
                                                    <table width="100%">
                                                      <tr>
                                                        <td id="TdAV1" runat="server" width="15" height="15" bgcolor="#BBB4FF"></td>
                                                        <td id="TdAV" runat="server">
                                                            <span id="spnAV" runat="server">Audio/Video Conference</span>
                                                        </td>
                                                        <td width="15" height="15" bgcolor="#F16855"></td>
                                                        <td><span id="spnRmHrg" runat="server">Room Conference</span></td>
                                                        <td id="TdP2p" runat="server" width="15" height="15" bgcolor="#EAA2D4"></td>
                                                        <td id="TdA" runat="server"><span id="spnAudCon" runat="server">Audio-Only Conference</span></td>
                                                        <td id="TdA1" runat="server" width="15" height="15" bgcolor="#85EE99"></td>
                                                        <td id="TdP2p1" runat="server"><span id="spnPpConf"  runat="server">Point-to-Point Conference</span></td>
                                                        <td id="Td31" runat="server" width="15" height="15" bgcolor="#82CAFF"></td> <%--FB 2448--%>
                                                        <td id="Td32" runat="server"><span id="Span3"   runat="server">VMR Conference</span></td>
                                                        <td id="Tdbuffer1" runat="server" width="15" height="15" bgcolor="#ffcc99"></td>
			                                            <td id="Tdbuffer2" runat="server">Setup Time</td>
			                                            <td id="Tdbuffer3" runat="server" width="15" height="15" bgcolor="#cccc99"></td>
			                                            <td id="Tdbuffer4" runat="server">TearDown Time</td><%-- Organization Css Module --%>
	                                                    <td id="Td23" runat="server" width="15" height="15" bgcolor= "#01DFD7"></td>
	                                                    <td id="Td24" runat="server">Deleted Conference</td>
	                                                    <td id="TdDailyHotdeskingColor" runat="server" width="15" height="15" bgcolor= "#cc0033"></td><%--FB 2694--%>	
			                                            <td id="TdDailyHotdesking" runat="server">Hotdesking Conference</td>
                                                      </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr valign="top" >
                                                <td>
                                                    <div id="DayPilotCalendar" style="vertical-align:middle;Width:99%;"><%--ZD 100151--%>
                                                         <DayPilot:DayPilotScheduler ID="schDaypilot" runat="server"  EmptyBackColor="#FFFFD5" 
                                                          RowHeaderColumnWidths="70" DataResourceField="RoomID" Days="1" OnBeforeEventRender="BeforeEventRenderhandler"  CellDuration="15" 
                                                          ClientObjectName="dps" DataStartField="start" DataEndField="end"  DataTextField="confDetails" DataValueField="ConfID" 
                                                          DataTagFields="ConferenceType,CustomDescription,Heading,confName,ID,Setup,ConfTime,Tear,Hours,Minutes,Location,ConfSupport,CustomOptions"
                                                          TimeRangeSelectedHandling="JavaScript" TimeRangeSelectedJavaScript="getConfType(start)"  Height="340px"
                                                          Crosshair="Full" OnBeforeTimeHeaderRender="BeforeTimeHeaderRenderDaily" CellWidth="50"  Layout="TableBased" EventHeight="70" CellGroupBy="Hour" UseEventBoxes="Never" 
                                                          Visible="false" HeaderFontSize="8pt" EventFontSize="8pt"  BubbleID="Details" ShowToolTip="false" OnBeforeCellRender="BeforeCellRenderhandlerClosedDate" HeightSpec="Fixed" 
                                                          /><%--FB 2588--%><%--FB 2804--%><%--ZD 100157--%>
                                                        <DayPilot:DayPilotBubble ID="Details" runat="server" width="0"  Visible="true" OnRenderContent="BubbleRenderhandler" ZIndex="999"></DayPilot:DayPilotBubble>   
                                                    </div>
                                                </td>
                                            </tr>
                                        </table>
                                    </dxw:ContentControl>   
                                </ContentCollection>
                              </dxtc:TabPage>
                              <dxtc:TabPage Text="Weekly View" Name="Weekly">
                                <ContentCollection>
                                    <dxw:ContentControl ID="ContentControl2" runat="server">
                                         <table style="width:90%;vertical-align:top;overflow:auto;height:450px" border="0" cellpadding="0" cellspacing="0"><%--ZD 100157--%>
                                            <tr valign="top" style="height:15px" id="trlegend2" runat="server"><%--FB 1985 FB 2050--%>
                                                <td>
                                                    <table  width="100%">
                                                        <tr>
                                                            <td  id="Td1" runat="server" width="15" height="15" bgcolor="#BBB4FF"></td>
                                                            <td id="Td2" runat="server"><span id="spnAVW" runat="server">Audio/Video Conference</span></td>
                                                            <td width="15" height="15" bgcolor="#F16855"></td>
                                                            <td><span id="spnRmHrgW" runat="server">Room Conference</span></td>
                                                            <td id="Td3" runat="server" width="15" height="15" bgcolor="#EAA2D4"></td>
                                                            <td id="Td4" runat="server"><span id="spnAudConW"  runat="server">Audio-Only Conference</span></td>
                                                            <td id="Td5" runat="server" width="15" height="15" bgcolor="#85EE99"></td>
                                                            <td id="Td6" runat="server"><span id="spnPpConfW"  runat="server">Point-to-Point Conference</span></td>
                                                            <td id="Td27" runat="server" width="15" height="15" bgcolor="#82CAFF"></td> <%--FB 2448--%>
                                                                <td id="Td28" runat="server"><span id="Span1"   runat="server">VMR Conference</span></td>
                                                            <td id="Td7" runat="server" width="15" height="15" bgcolor="#ffcc99"></td>
			                                                <td id="Td8" runat="server">Setup Time</td>
			                                                <td id="Td9" runat="server" width="15" height="15" bgcolor="#cccc99"></td>
			                                                <td id="Td10" runat="server">TearDown Time</td><%-- Organization Css Module --%>
		                                                     <td id="Td21" runat="server" width="15" height="15" bgcolor= "#01DFD7"></td>
		                                                    <td id="Td22" runat="server">Deleted Conference</td>
		                                                    <td id="TdWeeklyHotdeskingColor" runat="server" width="15" height="15" bgcolor= "#cc0033"></td><%--FB 2694--%>	
			                                                <td id="TdWeeklyHotdesking" runat="server">Hotdesking Conference</td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr valign="top">
                                                <td>
                                                    <div id="Div2" align="left"    style="vertical-align:bottom;width:99%;"><%--ZD 100151--%>
                                                    <DayPilot:DayPilotScheduler  ID="schDaypilotweek" runat="server" EmptyBackColor="#FFFFD5"
                                                      RowHeaderColumnWidths="70" DataResourceField="RoomID" Days="7" CellDuration="1440"  OnBeforeEventRender="BeforeEventRenderhandler"  
                                                      ClientObjectName="dps" DataStartField="start" DataEndField="end"  DataTextField="confDetails" DataValueField="ConfID" 
                                                      DataTagFields="ConferenceType,CustomDescription,Heading,confName,ID,Setup,ConfTime,Tear,Hours,Minutes,Location,ConfSupport,CustomOptions" 
                                                      Layout="TableBased" EventHeight="100" HeaderFontSize="8pt" Width="1024px"  
                                                      ShowNonBusiness="false" CellWidth="223" CellGroupBy="Week" Visible="false" UseEventBoxes="ShortEventsOnly" Crosshair="Full"  
                                                      BubbleID="DetailsWeekly" ShowToolTip="false" OnBeforeCellRender="BeforeCellRenderhandler" Height="330px"
                                                      EventFontSize="8pt" OnBeforeTimeHeaderRender="BeforeTimeHeaderRender" HeaderHeight ="33" HeightSpec="Fixed"/><%--FB 1851--%><%--FB 2311--%><%--ZD 100157--%>
                                                    <DayPilot:DayPilotBubble ID="DetailsWeekly" runat="server" width="0"  Visible="true" OnRenderContent="BubbleRenderhandler" ZIndex="999"/>   
                                                    </div>
                                                </td>
                                            </tr>
                                        </table>
                                    </dxw:ContentControl>    
                                </ContentCollection>
                             </dxtc:TabPage>
                             <dxtc:TabPage Text="Monthly View" Name="Monthly">
                                <ContentCollection>
                                    <dxw:ContentControl ID="ContentControl3" runat="server">
                                         <table style="width:90%;vertical-align:top;overflow:auto;height:450px" border="0" cellpadding="0" cellspacing="0"><%--ZD 100157--%>
                                            <tr valign="top" style="height:15px" id="trlegend3" runat="server"><%--FB 1985 FB 2050--%>
                                                <td>
                                                    <table  width="100%">
                                                        <tr>
                                                            <td  id="Td11" runat="server" width="15" height="15" bgcolor="#BBB4FF"></td>
                                                            <td id="Td12" runat="server"><span id="spnAVM"  runat="server">Audio/Video Conference</span></td>
                                                            <td width="15" height="15" bgcolor="#F16855"></td>
                                                            <td><span id="spnRmHrgM"  runat="server">Room Conference</span></td>
                                                            <td id="Td13" runat="server" width="15" height="15" bgcolor="#EAA2D4"></td>
                                                            <td id="Td14" runat="server"><span id="spnAudConM"   runat="server">Audio-Only Conference</span></td>
                                                            <td id="Td15" runat="server" width="15" height="15" bgcolor="#85EE99"></td>
                                                            <td id="Td16" runat="server"><span id="spnPpConfM"   runat="server">Point-<br />to-Point Conference</span></td>
                                                            <td id="Td29" runat="server" width="15" height="15" bgcolor="#82CAFF"></td> <%--FB 2448--%>
                                                            <td id="Td30" runat="server"><span id="Span2"   runat="server">VMR Conference</span></td>
                                                            <td id="Td17" runat="server" width="15" height="15" bgcolor="#ffcc99"></td>
			                                                <td id="Td18" runat="server">Setup Time</td>
			                                                <td id="Td19" runat="server" width="15" height="15" bgcolor="#cccc99"></td>
			                                                <td id="Td20" runat="server">TearDown Time</td><%-- Organization Css Module --%>
		                                                    <td id="Td25" runat="server" width="15" height="15" bgcolor= "#01DFD7"></td>
		                                                    <td id="Td26" runat="server">Deleted Conference</td>
		                                                    <td id="TdMonthlyHotdeskingColor" runat="server" width="15" height="15" bgcolor= "#cc0033"></td><%--FB 2694--%>
		                                                    <td id="TdMonthlyHotdesking" runat="server">Hotdesking Conference</td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr valign="top">
                                                <td>
                                                     <div id="DayPilotCalendarMonth" align="left"    style="vertical-align:bottom;width:99%;"><%--ZD 100151--%>
                                                         <DayPilot:DayPilotScheduler ID="schDaypilotMonth" runat="server" Days="31" RowHeaderColumnWidths="70"  EmptyBackColor="#FFFFD5"
                                                          DataResourceField="RoomID" OnBeforeEventRender="BeforeEventRenderhandler" ClientObjectName="dps" DataStartField="start" 
                                                          DataEndField="end"  DataTextField="confDetails" DataValueField="ConfID" 
                                                          DataTagFields="ConferenceType,CustomDescription,Heading,confName,ID,Setup,ConfTime,Tear,Hours,Minutes,Location,ConfSupport,CustomOptions" 
                                                          ViewType="Days" CellGroupBy="Month" CellDuration="1440" CellWidth="50"  Layout="TableBased" EventHeight="50" Height="320px"
                                                          ShowNonBusiness="false" HeaderFontSize="8pt" UseEventBoxes="ShortEventsOnly" Visible="false" Crosshair="Full" HeightSpec="Fixed"
                                                          BubbleID="DetailsMonthly" ShowToolTip="false" OnBeforeCellRender="BeforeCellRenderhandler"
                                                          EventFontSize="8pt" OnBeforeTimeHeaderRender="BeforeTimeHeaderRender" HeaderHeight ="33" /><%--1851--%><%--ZD 100157--%>
                                                         <DayPilot:DayPilotBubble ID="DetailsMonthly" runat="server" width="0"  Visible="true" OnRenderContent="BubbleRenderhandler" ZIndex="999"></DayPilot:DayPilotBubble>   
                                                    </div>
                                                </td>
                                            </tr>
                                        </table>
                                    </dxw:ContentControl>
                                </ContentCollection>
                             </dxtc:TabPage>
                         </TabPages>
                    </dxtc:ASPxPageControl>
            <asp:Button ID="btnDate" style="display:none" OnClick="ChangeCalendarDate" runat="server"/>
            <asp:Button ID="btnWeekly" style="display:none" OnClick="WeeklyBind" runat="server"/>
            <asp:Button ID="btnbsndhrs" style="display:none" OnClick="ChangeBusinessHour" runat="server"/>
            <div class="btprint" align="center" id="Div1" style="z-index:1;display:none;"></div>
        </ContentTemplate>
   </asp:UpdatePanel>
    <div class="btnprint" align="center" id="roomnumDIV" style="z-index:1;display:none;"></div>
 </td>
</tr>
</table>
<asp:TextBox ID="txtType" Visible="false"  runat="server"></asp:TextBox>  
<input type="hidden" ID="txtSelectedDate" runat="server" />
<input type="hidden" ID="txtSelectedDate1" runat="server" />
</form>
</body>
</html>
  
  
<script language="javascript">

var prm = Sys.WebForms.PageRequestManager.getInstance();
prm.add_initializeRequest(initializeRequest);

prm.add_endRequest(endRequest);

var postbackElement;
  
  function initializeRequest(sender, args) 
  {

if (prm.get_isInAsyncPostBack())
{




args.set_cancel(true);
}


DataLoading("1");
document.body.style.cursor = "wait";
}

function endRequest(sender, args) {document.body.style.cursor = "default";DataLoading("0");}


function ChangeTabsIndex(s,e)
{
    var activetab = s.GetActiveTab();
   var tabname;
   tabname = activetab.name;
   var hour = document.getElementById('trofficehrDaily');
   var week = document.getElementById('trofficehrWeek');
   var month = document.getElementById('trofficehrMonth');
   var selectedListViewRooms = document.getElementById("selectedListViewRooms").value; 
   //ZD 100151
   var IsSettingsChange = document.getElementById("IsSettingsChange"); 
   if(IsSettingsChange)
        IsSettingsChange.value = 'Y';
   
   selectedRooms = $('#selectedRooms').val(); 
   var hdnRoomType = $('#hdnRoomType').val();
      
   if(tabname == "Daily")//FB 2804
   {
   document.getElementById("hdnRoomView").value="1"; //FB 2948
   if(document.getElementById("selStatus").value == "0")
   {
       document.getElementById("tdClosed").style.display='block'; 
       document.getElementById("tdSelectRooms").style.display='none';
    }
   else   
       document.getElementById("tdClosed").style.display='none'; 
     
     //ZD 100151  
     document.getElementById ("btnDate").click();
   
   hour.style.display = 'block';
   week.style.display = 'none';
   month.style.display = 'none';
   }
   else if(tabname == "Weekly")
   {
    document.getElementById("hdnRoomView").value="2";//FB 2948
    if(hdnRoomType == 1)
    {
      if(selectedListViewRooms == 0)  
        document.getElementById("tdSelectRooms").style.display='block';
      else          
        document.getElementById("tdSelectRooms").style.display='none';            
    }
    else
    {
      if(selectedRooms == 0)  
        document.getElementById("tdSelectRooms").style.display='block';
      else          
        document.getElementById("tdSelectRooms").style.display='none';
    }

   document.getElementById("tdClosed").style.display='none';
      
   hour.style.display = 'none';
   week.style.display = 'block';
   month.style.display = 'none';
   //ZD 100151  
   //if(document.getElementById("IsWeekOverLap").value == "Y")
       document.getElementById ("btnDate").click();
   }
   else if(tabname== "Monthly")
   {
    document.getElementById("hdnRoomView").value="3"; //FB 2948
    if(hdnRoomType == 1)
    {            
        if(selectedListViewRooms == 0)  
          document.getElementById("tdSelectRooms").style.display='block';
        else          
          document.getElementById("tdSelectRooms").style.display='none';            
    }
    else
    {
        if(selectedRooms == 0)  
          document.getElementById("tdSelectRooms").style.display='block';
        else          
          document.getElementById("tdSelectRooms").style.display='none';
    }
   document.getElementById("tdClosed").style.display='none';   
     //ZD 100151  
     document.getElementById ("btnDate").click();
   
   hour.style.display = 'none';
   week.style.display = 'none';
   month.style.display = 'block';
   }
   if(fnTimeScroll()) //ZD 100157
   fnTimeScroll();
   } 
var myPopup;
var IE4 = (document.all) ? true : false;


var	dayname = new Array();
var pageleft = 10;
var totaltop = 190, topleft = 245, totalh = 960, calendarh = 180, totalw = getwinwid() - topleft - pageleft, yaxlwid = 65, lineh = 15, splitwid=1;


var allowedrmnum = 0,	isOhOn = false;
var strConfs = "", startDate; 
var aryRooms="", selConfTypeCode;

var isWdOn = false, confmonthfirst = "";

function getwinwid() 
{
	var myWidth = 0, myHeight = 0;
	if( typeof( window.innerWidth ) == 'number' ) {
		//Non-IE
		myWidth = window.innerWidth;
		myHeight = window.innerHeight;
	} else if( document.documentElement && ( document.documentElement.clientWidth || document.documentElement.clientHeight ) ) {
		//IE 6+ in 'standards compliant mode'
		myWidth = document.documentElement.clientWidth;
		myHeight = document.documentElement.clientHeight;
	} else if( document.body && ( document.body.clientWidth || document.body.clientHeight ) ) {
		//IE 4 compatible
		myWidth = document.body.clientWidth;
		myHeight = document.body.clientHeight;
	}
	  
	return (myWidth);
}


function OpenModal()
{
    if(document.getElementById("RoomPoupup"))
        document.getElementById("RoomPoupup").click();
}


function getConfType(start)
{

  if(document.getElementById("selStatus").value == 0)//FB 2804
     return false;
  if( "<%=Session["isExpressUser"]%>" == 1 ) //FB 1779
  {
      //alert("You cannot schedule a conference from a popup calendar.");//FB 1779 - Commented
      return false;  
  }
  var time = start.toString(); //Edited for FF
  var conftime = new Date(time);
  
  var hour,min;
  
  var splttim = conftime.toTimeString().split(':');
  
  hour = splttim[0];
  min = splttim[1];
  
  if(timeFormat == "1")
  {
  
      hour = conftime.getHours();
      min = conftime.getMinutes();
      
      
      if(eval(hour) < 10)
        hour = "0"+hour
        
      if(eval(min) < 10)
        min = "0"+min
        
      //min = min +" "+conftime.toLocaleTimeString().toString().split(" ")[1]
      min = min +" "+ conftime.format("tt"); //FB 2108
  }
  
  
    
  if (queryField("hf") == "1")
    {
        alert("You cannot schedule a conference from a popup calendar.");
        return false;
    }
	
    else {
     
        var mmm_numc, mm_strc, mm_aryc,mmm_strc,mmm_intc,menu;
        
        var menuset = "0";
        var time ;
        
        mm_strc = "<%=Session["sMenuMask"].ToString() %>";;
        mm_aryc = mm_strc.split("-");
        mmm_strc = mm_aryc[0];
        mmm_aryc = mmm_strc.split("*");
        mmm_numc = parseInt(mmm_aryc[0], 10);
        mmm_intc = parseInt(mmm_aryc[1], 10);
        for (i=1; i<=mmm_numc; i++) {
        menu = ( mmm_intc & (1 << (mmm_numc-i)) ) ? ("menu_" + i) : "";
	    //FB 1929
        if(menu == "menu_3")
            menuset = "1";
	    
        }
        //FB 1929
        if(menuset > 0)
        {     
           
//        if(confirm("Are you sure you want to create a new conference beginning " + confdate + " @ " + hour+":"+min  + "?"))
//        {
              //FB 1723
               if(document.getElementById("selectedList"))	             
	            url =  url = "ConferenceSetup.aspx?t=n&sd=" + confdate + "&st=" + hour+":"+min +"&rms=" + document.getElementById("selectedList").value;//Code added for FB 1452
            
	        
            DataLoading("1");
            window.location.href=url;
//        }

         }
    }
}

function ViewDetails(e)
{
 
  url = "ManageConference.aspx?confid="+e.value()+"&t=hf";
  myPopup = window.open(url, "viewconference", "width=1,height=1,resizable=yes,scrollbars=yes,status=no");
  myPopup.focus();
  
}  

/* FB 1659 code starts ... */
function ViewConfDetails()
{
    var args = ViewConfDetails.arguments;
    url = "ManageConference.aspx?confid="+ args[0] +"&t=hf";
    myPopup = window.open(url, "viewconference", "width=1,height=1,resizable=yes,scrollbars=yes,status=no");
    myPopup.focus();
  
}  
/* FB 1659 code ends ... */

function initsubtitle ()
{
   
   var dd, m = "";
    var monthNew,yrNew

    
    dd = new Date(confdate);
                     
    monthNew = (dd.getMonth() + 1);
    yrNew = dd.getFullYear();
    document.getElementById ("IsWeekOverLap").value = "";
    document.getElementById ("IsMonthChanged").value = "";
    
    if(WeekOverlap(confdate))
           document.getElementById ("IsWeekOverLap").value = "Y";
    if(document.getElementById ("MonthNum").value != monthNew || document.getElementById ("YrNum").value != yrNew )
    {
        document.getElementById ("YrNum").value = yrNew ;
        document.getElementById ("MonthNum").value = monthNew;
        document.getElementById ("IsMonthChanged").value = "Y";
    }
    else
    {
    
        document.getElementById ("IsMonthChanged").value = "";
        
    }
    
}
//FB 2804
var selectedListArray = new Array();
var selectedListViewRooms;
function datechg() {
if(datechg.arguments[0])
{
    
    if(datechg.arguments[0] == "1")
    {
    
    if(queryField("hf") == "1")
    {
       document.getElementById("txtSelectedDate").value = GetDefaultDate(queryField("d"),dtFormatType);;
        
     }
     
     document.getElementById ("IsMonthChanged").value = "Y"; 
     document.getElementById ("btnDate").click();       
    }
}
else
{
    
	//document.getElementById ("flatCalendarDisplay").style.display = "none";
    
    document.getElementById("txtSelectedDate").value=confdate;
    
    //FB 2804 - Start
     var SelectedWeekdays, Selectedday, str="", selectedRooms1;
     var isClosed = new Boolean();
     var selDays = new Array();
     isClosed = false;
     
     SelectedWeekdays = new Date(confdate);     
     Selectedday = SelectedWeekdays.getDay()+1;
     
     if( "<%=Session["DaysClosed"]%>" != null)
        str = "<%=Session["DaysClosed"].ToString()%>";       
        
        selDays = str.split(',');
        
        for(var i =0; i<selDays.length; i++)
        {
            if( selDays[i] == Selectedday)        
                isClosed = true;
        }
        if(isClosed)
        {
           if(document.getElementById("hdnRoomView").value == 2 || document.getElementById("hdnRoomView").value == 3) //FB 2948
                document.getElementById("tdClosed").style.display='none';
           else
                document.getElementById("tdClosed").style.display='block';
            document.getElementById("selStatus").value = "0";
        }
        else
        {
            document.getElementById("tdClosed").style.display='none';                  
            document.getElementById("selStatus").value = "1";
        }
        //FB 2804 - End
    initsubtitle();
    if(document.getElementById ("IsMonthChanged").value = "Y")
        document.getElementById ("btnDate").click(); 
    else
        chnghrs();
 }
 //FB 2804
 $('#lstRoomSelection input:checked').each(function() {
            selectedListArray.push($(this).attr('name'));
        });
        $('#selectedListViewRooms').val(selectedListArray.length);
      selectedListViewRooms = $('#selectedListViewRooms').val();             

    selectedRooms = $('#selectedRooms').val();   
    
    if(isClosed)    
    { 
      document.getElementById("tdSelectRooms").style.display='none';
    }
    else
    {                      
        var hdnRoomType = $('#hdnRoomType').val();            
        if(hdnRoomType == 1)
        {
          if(selectedListViewRooms == 0)  
            document.getElementById("tdSelectRooms").style.display='block';
          else          
            document.getElementById("tdSelectRooms").style.display='none';            
        }
        else
        {
            if(selectedRooms == 0)                  
                document.getElementById("tdSelectRooms").style.display='block';                
            else          
                document.getElementById("tdSelectRooms").style.display='none';
        }
    }   
     
 selectedListArray = new Array();
}

//FB 2804 - Start
/*function showhide(bool)
{
    if(bool == '0')
    {    
        document.getElementById("CalendarContainer_schDaypilot").childNodes[0].style.visibility = "hidden";
        document.getElementById("CalendarContainer_schDaypilotweek").childNodes[0].style.visibility = "hidden";
        document.getElementById("CalendarContainer_schDaypilotMonth").childNodes[0].style.visibility = "hidden";
        return false;        
     }   
    else
    {
        document.getElementById("CalendarContainer_schDaypilot").childNodes[0].style.visibility = "visible";
        document.getElementById("CalendarContainer_schDaypilotweek").childNodes[0].style.visibility = "visible";
        document.getElementById("CalendarContainer_schDaypilotMonth").childNodes[0].style.visibility = "visible";
        
    }
}*/
//FB 2804 - End

function DataLoading(val)
{
    if (val=="1")
        document.getElementById("dataLoadingDIV").innerHTML="<b><font color='#FF00FF' size='2'>Data loading ...</font></b>&nbsp;&nbsp;&nbsp;&nbsp;<img border='0' src='image/wait1.gif' "; // FB 2742
    else
        document.getElementById("dataLoadingDIV").innerHTML=""; 
        
     //FB 2804
    //var stat = document.getElementById("selStatus").value;
    //if(stat != "" )
    //setTimeout("showhide('" + stat +"')",250);                       
}
function DataLoading1(val)
 {
    //alert(val);
    if (val == "1")
     document.getElementById("dataLoadingDIV").innerHTML = "<img border='0' src='image/wait1.gif'>";
    else
     document.getElementById("dataLoadingDIV").innerHTML = "";
}   
function showcalendar()
{
	if (document.getElementById ("flatCalendarDisplay").style.display == "")
		document.getElementById ("flatCalendarDisplay").style.display = "none";
	else
		document.getElementById ("flatCalendarDisplay").style.display = "";
}

startDate = queryField("d");




            
var dtFormatType, convDate ,calendarTyp ,timeFormat;
dtFormatType = '<%=dtFormatType%>';
calendarTyp = '<%=CalendarType%>';
timeFormat = '<%=timeFormat%>';
var confdate,confweekmon,curwkDate
var curdate = new Date();
if (document.getElementById("txtSelectedDate").value == "") {
	confdate = (curdate.getMonth() + 1) + "/" + curdate.getDate() + "/" + curdate.getFullYear();
	confweekmon = addDates(curdate.getFullYear(), curdate.getMonth() + 1, curdate.getDate(), (curdate.getDay() == 0) ? -6 : (1-curdate.getDay()) );
	document.getElementById("<%=txtSelectedDate.ClientID %>").value=confdate;
	showFlatCalendar(0, '%m/%d/%Y');
} else {
	confdate = document.getElementById("txtSelectedDate").value;
	curwkDate = new Date(confdate);
	confweekmon = addDates(curwkDate.getFullYear(), curwkDate.getMonth() + 1, curwkDate.getDate(), (curwkDate.getDay() == 0) ? -6 : (1-curwkDate.getDay()) );
	showFlatCalendar(0, '%m/%d/%Y', document.getElementById("txtSelectedDate").value);
}
document.getElementById ("flatCalendarDisplay").style.position = 'relative';
document.getElementById ("flatCalendarDisplay").style.top = 0;
document.getElementById ("flatCalendarDisplay").style.height = 150;
document.getElementById ("flatCalendarDisplay").style.textAlign = "center";
//document.getElementById ("flatCalendarDisplay").style.display = "none";


DataLoading("0");
initsubtitle();

//datechg("1");



function goToCal()
{
        if(document.getElementById("lstCalendar") != null)
        {
		    
		    if (document.getElementById("lstCalendar").value == "2")//FB 1779 
		    {
			   if( "<%=Session["isExpressUser"]%>" == 1 ) //FB 1779
			      window.location.href = "ConferenceList.aspx?t=3";
			   else
			     window.location.href = "ConferenceList.aspx?t=2";
		    }
		    if (document.getElementById("lstCalendar").value == "1"){
			    viewrmdy(1);
			    //window.location.href = "PersonalCalendar.aspx?v=1&r=1&hf=&d=" ; //code changed for calendar conversion FB 412
		    }
			//FB 2501 Call Monitoring
		    if (document.getElementById("lstCalendar").value == "4"){
                   window.location.href = "MonitorMCU.aspx";
            } 
            //FB 2501 P2P Call Monitoring
		    if (document.getElementById("lstCalendar").value == "5"){
                   window.location.href = "point2point.aspx";
           }
		}
}

if(queryField("hf") == "1")
{

   if(document.getElementById("lstCalendar") != null)
    {
        document.getElementById("lstCalendar").style.display = 'none'  
    		    
    }
}

function WeekOverlap(weekBeginDate) {
		var overlaps = false;
		var week = new Date(weekBeginDate);
		var nextweeks =  new Date(weekBeginDate);
		var weekbegin = new Date();
		weekbegin.setDate(week.getDate() - week.getDay());
		nextweeks.setDate(weekbegin.getDate() + 7);
		var nextweek = new Date(nextweeks);
		if (nextweek.getMonth() != weekbegin.getMonth()) {
			overlaps = true;
		}
		var weeknew = getWeek(weekbegin);
		document.getElementById ("IsWeekChanged").value = "";
		(document.getElementById ("Weeknum").value != weeknew)
		{
		    document.getElementById ("IsWeekChanged").value = "Y";
		    
		}
		
		return overlaps;
		}
		
function getWeek (weekbegin)
 {
 var wkbegin = new Date(weekbegin);
 var onejan = new Date(wkbegin.getFullYear(),0,1);
 return Math.ceil((((wkbegin - onejan) / 86400000) + onejan.getDay()+1)/7);
 }
 


</script>

<script language="javascript" type="text/javascript">
 //************************** Treeview Parent-Child check behaviour ****************************//  

   function OnTreeClick(evt)
   {
        var src = window.event != window.undefined ? window.event.srcElement : evt.target;
        var isChkBoxClick = (src.tagName.toLowerCase() == "input" && src.type == "checkbox");
        if(isChkBoxClick)
        {
            var parentTable = GetParentByTagName("table", src);
            var nxtSibling = parentTable.nextSibling;
            if(nxtSibling && nxtSibling.nodeType == 1)//check if nxt sibling is not null & is an element node
            {
                if(nxtSibling.tagName.toLowerCase() == "div") //if node has children
                {
                    //check or uncheck children at all levels
                    CheckUncheckChildren(parentTable.nextSibling, src.checked);
                }
            }
            //check or uncheck parents at all levels
            CheckUncheckParents(src, src.checked);
        }
        
        var btn = document.getElementById('btnDate');
        var hdn = document.getElementById('IsSettingsChange');
        
        if(hdn)
           hdn.value = "Y"; 
        
        if(btn)
            btn.click();
   } 

   function CheckUncheckChildren(childContainer, check)
   {
      var childChkBoxes = childContainer.getElementsByTagName("input");
      var childChkBoxCount = childChkBoxes.length;
      for(var i = 0; i<childChkBoxCount; i++)
      {
        childChkBoxes[i].checked = check;
      }
   }

   function CheckUncheckParents(srcChild, check)
   {
       var parentDiv = GetParentByTagName("div", srcChild);
       var parentNodeTable = parentDiv.previousSibling;
       
       if(parentNodeTable)
        {
            var checkUncheckSwitch;
            
            if(check) //checkbox checked
            {
                var isAllSiblingsChecked = AreAllSiblingsChecked(srcChild);
                if(isAllSiblingsChecked)
                    checkUncheckSwitch = true;
                else    
                    return; //do not need to check parent if any child is not checked
            }
            else //checkbox unchecked
            {
                checkUncheckSwitch = false;
            }
            
            var inpElemsInParentTable = parentNodeTable.getElementsByTagName("input");
            if(inpElemsInParentTable.length > 0)
            {
                var parentNodeChkBox = inpElemsInParentTable[0]; 
                parentNodeChkBox.checked = checkUncheckSwitch; 
                //do the same recursively
                CheckUncheckParents(parentNodeChkBox, checkUncheckSwitch);
            }
        }
   }

   function AreAllSiblingsChecked(chkBox)
   {
     var parentDiv = GetParentByTagName("div", chkBox);
     var childCount = parentDiv.childNodes.length;
     for(var i=0; i<childCount; i++)
     {
        if(parentDiv.childNodes[i].nodeType == 1) //check if the child node is an element node
        {
            if(parentDiv.childNodes[i].tagName.toLowerCase() =="table")
            {
               var prevChkBox = parentDiv.childNodes[i].getElementsByTagName("input")[0];
              //if any of sibling nodes are not checked, return false
              if(!prevChkBox.checked) 
              {
                return false;
              } 
            }
        }
     }
     return true;
   }

   //utility function to get the container of an element by tagname
   function GetParentByTagName(parentTagName, childElementObj)
   {
      var parent = childElementObj.parentNode;
      while(parent.tagName.toLowerCase() != parentTagName.toLowerCase())
      {
         parent = parent.parentNode;
      }
    return parent;    
   } 
   
   function chnghrs()
   {
   
   if (!fncheckTime())
            return false;
    var hourbtn = document.getElementById('btnbsndhrs');
    if(hourbtn)
        hourbtn.click();
   } 
   //ZD 100157
   function  ShowHrs()
   {
       var ShwHrs= document.getElementById("officehrDaily");
       if(!ShwHrs.checked)
       {
           if(document.getElementById("lstStartHrs_Container")!= null)
            document.getElementById("lstStartHrs_Container").style.display="none";
           if(document.getElementById("lstEndHrs_Container")!= null)
            document.getElementById("lstEndHrs_Container").style.display="none";
           }
       else
       {
           if(document.getElementById("lstStartHrs_Container")!= null)
            document.getElementById("lstStartHrs_Container").style.display="inline";
           if(document.getElementById("lstEndHrs_Container")!= null)
            document.getElementById("lstEndHrs_Container").style.display="inline";
       
       } 
   }
   
   function OpenRoomSearchCalen()
{
        if(OpenRoomSearchCalen.arguments != null)
        {
            var sellst = document.getElementById('selectedList');
            var rmargs = OpenRoomSearchCalen.arguments;
            var prnt = rmargs[0];
            var url = "";
           // var locs = document.getElementById("selectedloc");
           // sellst.value =  locs.value;
            url = "RoomSearch.aspx?rmsframe="+sellst.value+"&hf=1&frm="+prnt;//frmCalendarRoom";
            window.open(url, "RoomSearch", "width="+ screen. availWidth +",height=666px,resizable=no,scrollbars=yes,status=no,top=0,left=0");
        }
    
}

function comparesel()
{
	rms = document.getElementById("selectedList").value;
	rms = getListViewChecked(rms);
	rms = rms.replace(", ,",",");
    //rms += ",";
	if (rms.split(",").length <=2)
		alert("Please select at least 2 locations to compare.");
	else
	    if (rms.split(",").length > 6) //fogbugz case 179 kapil said it is 5
	        alert("Please select 2 to 5 rooms");
	else {
		url = "roomresourcecomparesel.aspx?wintype=pop&f=pop&rms=" + rms;
		rmresPopup = window.open(url,'roomresource','status=yes,width=500,height=280,resizable=yes,scrollbars=yes');
			
		rmresPopup.focus();
		if (!rmresPopup.opener) {
			rmresPopup.opener = self;
		}
	}
}
//FB 2804
$(document).ready(function(){ 
     
   $('#selectAllCheckBox').live('click',function(){          
            
        var sList = new Array();
        $('#lstRoomSelection input:checked').each(function() {
            sList.push($(this).attr('name'));
        });       
        
        var selRoom = sList.length;                  
        if(selRoom == 0)
             document.getElementById("tdSelectRooms").style.display='block';                 
        else
             document.getElementById("tdSelectRooms").style.display='none';                 
     });
     
     $('#rdSelView input').live('click',function(){     
        $('#hdnRoomType').val($('#rdSelView input').index(this));
     });
     
     // Level View Rooms
     getSelectedRoomListCount();
             
    $('#treeRoomSelection').live('click',function(){           
       getSelectedRoomListCount();       
    });
    
    function getSelectedRoomListCount()
    {   
        var selectedList = new Array();
        $('#treeRoomSelection input:checked').each(function() {
            selected.push($(this).attr('name'));
        });
        $('#selectedRooms').val(selected.length);
    }    
    var selectedRooms = $('#selectedRooms').val();
    
    
    
     selected = new Array();
 $('#treeRoomSelection input:checked').each(function() {
            selected.push($(this).attr('name'));
        });
        $('#selectedRooms').val(selected.length);
      var selectedRooms = $('#selectedRooms').val();
    if(document.getElementById("selStatus").value != 0)//FB 2804                   
        if(selectedRooms == 0)
            document.getElementById("tdSelectRooms").style.display='block';                 
        else
            document.getElementById("tdSelectRooms").style.display='none';
      
 selected = new Array();
 
 
  //---------------------------------------------------
  
// List View Rooms

 getSelectedListViewRoomCount();
 
 $('#lstRoomSelection').live('click',function(){    
    getSelectedListViewRoomCount();       
 });
 
 function getSelectedListViewRoomCount()
 {
     var selectedListCount = new Array();
     $('#lstRoomSelection input:checked').each(function() {
         selectedListCount.push($(this).attr('name'));
     });
     $('#selectedListViewRooms').val(selectedListCount.length);        
 }
 var selectedListViewRooms = $('#selectedListViewRooms').val();
 
 
 selectedListArray = new Array();    
   $('#lstRoomSelection input:checked').each(function() {
            selectedListArray.push($(this).attr('name'));
        });
        $('#selectedListViewRooms').val(selectedListArray.length);
      var selectedListViewRooms = document.getElementById("selectedListViewRooms").value;             

  if(selectedListViewRooms == 0)
  {
   if(isClosed)  
      document.getElementById("tdSelectRooms").style.display='none';
   else
      document.getElementById("tdSelectRooms").style.display='block';         
   }   
 else
     document.getElementById("tdSelectRooms").style.display='none';
 
 selectedListArray = new Array();
 
//    //---------------------------------------------------
    
});

 var selected = new Array();

 //Code added for Tab Calendar pages in daypilot
function getTabCalendarLocal(evt)
{
// obj gives us the node on which check or uncheck operation has performed 
    var obj;
    if (window.event != window.undefined)
       obj =  window.event.srcElement;
    else
       obj = evt.target; 
       
    var treeNodeFound = false; 
    var checkedState;
    
    if (obj.tagName == "INPUT" && obj.type == "checkbox") 
    {     //1 checking whether obj consists of checkbox or not 
        var treeNode = obj; 
        //record the checked state of the TreeNode 
        checkedState = treeNode.checked; 
        //work our way back to the parent <table> element 
        do {
            obj = obj.parentNode; 
        } while (obj.tagName != "TABLE") 
        //keep track of the padding level for comparison with any children 
        var parentTreeLevel = obj.rows[0].cells.length; 
        var parentTreeNode = obj.rows[0].cells[0]; 
        //get all the TreeNodes inside the TreeView (the parent <div>) 
        var tables = obj.parentNode.getElementsByTagName("TABLE"); 
        //checking for any node is checked or unchecked during operation 
        if(obj.tagName == "TABLE") 
        { //2
            // if any node is unchecked then their parent node are unchecked 
            if (!treeNode.checked) 
            { //3
                //head1 gets the parent node of the unchecked node 
                //document.getElementById("tdSelectRooms").style.display='block';//FB 2802
                var head1 = obj.parentNode.previousSibling; 
                if(head1.tagName == "TABLE") 
                {
                    //checks for the input tag which consists of checkbox 
                    var matchElement1 = head1.getElementsByTagName("INPUT"); 
                    //matchElement1[0] gives us the checkbox and it is unchecked 
                    matchElement1[0].checked = false; 
                } 
                else 
                    head1 = obj.parentNode.previousSibling;
                if(head1.tagName == "TABLE") 
                { //4
                //head2 gets the parent node of the unchecked node 
                    var head2 = obj.parentNode.parentNode.previousSibling; 
                    if(head2.tagName == "TABLE") 
                    { //5
                        //checks for the input tag which consists of checkbox 
                        var matchElement2 = head2.getElementsByTagName("INPUT"); 
                        matchElement2[0].checked = false; 
                    } //5
                }//4
                else 
                    head2 = obj.parentNode.previousSibling;
                if(head2.tagName == "TABLE") 
                {//6
                    //head3 gets the parent node of the unchecked node 
                    var head3 = obj.parentNode.parentNode.parentNode.previousSibling; 
                    if(head3.tagName == "TABLE") 
                    {//7
                        //checks for the input tag which consists of checkbox 
                        var matchElement3 = head3.getElementsByTagName("INPUT"); 
                        matchElement3[0].checked = false; 
                    }//7
                }//6
                else 
                    head3 = obj.parentNode.previousSibling;
                if(head3.tagName == "TABLE") 
                { //7
                   //head4 gets the parent node of the unchecked node 
                    var head4 = obj.parentNode.parentNode.parentNode.parentNode.previousSibling; 
                    if(head4 != null) 
                    {//8 
                        if(head4.tagName == "TABLE") 
                        {//8
                            //checks for the input tag which consists of checkbox 
                            var matchElement4 = head4.getElementsByTagName("INPUT"); 
                            matchElement4[0].checked = false; 
                        }//8
                    }//7
                } //6
            } //end if - unchecked
            //total number of TreeNodes 
            var numTables = tables.length 
            if (numTables >= 1) 
            {
                for (i=0; i < numTables; i++) 
                {
                    if (tables[i] == obj) 
                    {
                        treeNodeFound = true; 
                        i++;
                        if (i == numTables) 
                            break;
                    }
                    if (treeNodeFound == true) 
                    {
                        var childTreeLevel = tables[i].rows[0].cells.length; 
                        if (childTreeLevel > parentTreeLevel) 
                        { 
                            var cell = tables[i].rows[0].cells[childTreeLevel - 1]; 
                            var inputs = cell.getElementsByTagName("INPUT"); 
                            inputs[0].checked = checkedState; 
                        } 
                        else 
                            break;
                    } //end if 
                    if (childTreeLevel == "5")
                    {
                        if (inputs[0].checked)
                        {
                            var tnName = inputs[0].id; var nodeNum;
                            if (tnName.indexOf("Wizard1") >= 0)
                               nodeNum = tnName.substring(26, tnName.indexOf("CheckBox"));
                            else
                                nodeNum = tnName.substring(18, tnName.indexOf("CheckBox"));
                            if (nodeNum != "")
                            {
                                var obbj;
                                //alert(tnName.indexOf("Wizard1"));
                                if (tnName.indexOf("Wizard1") >= 0)
                                    obbj = document.getElementById("Wizard1_treeRoomSelectiont" + nodeNum);
                                else
                                    obbj = document.getElementById("treeRoomSelectiont" + nodeNum);
                                if (obbj.innerText != "")
                                  if (document.getElementById("selectedList").value.indexOf(" " + obbj.innerText + ", ") < 0)
                                    document.getElementById("selectedList").value += " " + obbj.innerText + ", ";
                            }   
                        }
                        else
                        {
                            var tnName = inputs[0].id;
                            var nodeNum;
                            if (tnName.indexOf("Wizard1") >= 0)
                               nodeNum = tnName.substring(26, tnName.indexOf("CheckBox"));
                            else
                                nodeNum = tnName.substring(18, tnName.indexOf("CheckBox"));
                            if (nodeNum != "")
                            {
                                var obbj;
                                
                                if (tnName.indexOf("Wizard1") >= 0)
                                    obbj = document.getElementById("Wizard1_treeRoomSelectiont" + nodeNum);
                                else
                                    obbj = document.getElementById("treeRoomSelectiont" + nodeNum);
                                var obbbj = document.getElementById("selectedList");
                                if ((obbbj.value.indexOf(" " + obbj.innerText + ", ") >=0) && (obbj.innerText != ""))
                                     obbbj.value = obbbj.value.replace(" " + obbj.innerText + ", ", "");
                            }
                        }
                    }
                }//end for 
                
                
            } //end if - numTables >= 1

        // if all child nodes are checked then their parent node is checked
        //Wizard1_treeRoomSelectiont4
        //Wizard1_treeRoomSelectionn4Checkbox
        if (treeNode.checked) 
        {
           //document.getElementById("tdSelectRooms").style.display='none';//FB 2802
            var tnName = treeNode.id;
            var nodeNum;
            if (tnName.indexOf("Wizard1") >= 0)
               nodeNum = tnName.substring(26, tnName.indexOf("CheckBox"));
            else
                nodeNum = tnName.substring(18, tnName.indexOf("CheckBox"));
            if (nodeNum != "")
            {
                var obbj;
                if (tnName.indexOf("Wizard1") >= 0)
                    obbj = document.getElementById("Wizard1_treeRoomSelectiont" + nodeNum);
                else
                    obbj = document.getElementById("treeRoomSelectiont" + nodeNum);
                    
                if (obbj == null)
                {
                    obbj = document.getElementById(nodeNum);
                    tle = obbj.parentNode.innerText;
                }
                else
                    tle = obbj.innerText;
                if (obbj != null)
                if (tle != "")
                  if (document.getElementById("selectedList").value.indexOf(" " + tle + ", ") < 0)
                    document.getElementById("selectedList").value += " " + tle + ", ";
            }
            var chk1 = true;
            var head1 = obj.parentNode.previousSibling;
            var pTreeLevel1 = obj.rows[0].cells.length;
            if(head1.tagName == "TABLE") 
            {
                var tbls = obj.parentNode.getElementsByTagName("TABLE");
                var tblsCount = tbls.length;
                for (i=0; i < tblsCount; i++) 
                {
                    var childTreeLevel = tbls[i].rows[0].cells.length;
                    if (childTreeLevel = pTreeLevel1) 
                    {
                        var chld = tbls[i].getElementsByTagName("INPUT");
                        if (chld[0].checked == false) 
                        {
                            chk1 = false;
                            break;
                        }
                    }
                }
                var nd = head1.getElementsByTagName("INPUT");
                nd[0].checked = chk1;
            }
            else
                head1 = obj.parentNode.previousSibling;
            var chk2 = true;
            if(head1.tagName == "TABLE") 
            {
                var head2 = obj.parentNode.parentNode.previousSibling; 
                if(head2.tagName == "TABLE") 
                {
                    var tbls = head1.parentNode.getElementsByTagName("TABLE");
                    var pTreeLevel2 = head1.rows[0].cells.length;
                    var tblsCount = tbls.length;
                    for (i=0; i < tblsCount; i++) 
                    {
                        var childTreeLevel = tbls[i].rows[0].cells.length;
                        if (childTreeLevel = pTreeLevel2) 
                        {
                            var chld = tbls[i].getElementsByTagName("INPUT");
                            if (chld[0].checked == false) 
                            {
                                chk2 = false;
                                break;
                            }
                        }
                    }
                    var nd = head2.getElementsByTagName("INPUT");
                    nd[0].checked = (chk2 && chk1);
                }
            }
            else 
                head2 = obj.parentNode.previousSibling;
            var chk3 = true;
            if(head2.tagName == "TABLE") 
            {
                var head3 = obj.parentNode.parentNode.parentNode.previousSibling; 
                if(head3.tagName == "TABLE") 
                {
                    var tbls = head2.parentNode.getElementsByTagName("TABLE");
                    var pTreeLevel3 = head2.rows[0].cells.length;
                    var tblsCount = tbls.length;
                    for (i=0; i < tblsCount; i++) 
                    {
                        var childTreeLevel = tbls[i].rows[0].cells.length;
                        if (childTreeLevel = pTreeLevel3) 
                        {
                            var chld = tbls[i].getElementsByTagName("INPUT");
                            if (chld[0].checked == false) 
                            {
                                chk3 = false;
                                break;
                            }
                        }
                    }
                    var nd = head3.getElementsByTagName("INPUT");
                    nd[0].checked = (chk3 && chk2 && chk1);
                }
            }
            else 
                head3 = obj.parentNode.previousSibling;
            var chk4 = true;
            if(head3.tagName == "TABLE") 
            {
                var head4 = obj.parentNode.parentNode.parentNode.parentNode.previousSibling; 
                //Added for FB 1415,1416,1417,1418 Start
                if(head4 != null)
                {
                //Added for FB 1415,1416,1417,1418 End
                    if(head4.tagName == "TABLE") 
                    {
                        var tbls = head3.parentNode.getElementsByTagName("TABLE");
                        var pTreeLevel4 = head3.rows[0].cells.length;
                        var tblsCount = tbls.length;
                        for (i=0; i < tblsCount; i++) 
                        {
                            var childTreeLevel = tbls[i].rows[0].cells.length;
                            if (childTreeLevel = pTreeLevel4) 
                            {
                                var chld = tbls[i].getElementsByTagName("INPUT");
                                if (chld[0].checked == false) 
                                {
                                    chk4 = false;
                                    break;
                                }
                            }
                        }
                        var nd = head4.getElementsByTagName("INPUT");
                        nd[0].checked = (chk4 && chk3 && chk2 && chk1);
                        //alert(chk1.value + " : " + chk2.value + " : " + chk3.value + " : " + chk4.value);
                    }
                }  //Added for FB 1415,1416,1417,1418 
            }
        }//end if - checked
        document.getElementById ("IsSettingsChange").value = "Y";
        document.getElementById ("IsMonthChanged").value = "";     
        document.getElementById ("btnDate").click();
    }
  
 } //end if
 //FB 2804
 selected = new Array();
 $('#treeRoomSelection input:checked').each(function() {
            selected.push($(this).attr('name'));
        });
        $('#selectedRooms').val(selected.length);
      var selectedRooms = $('#selectedRooms').val();             
      
        if((selectedRooms == 0 && (document.getElementById("hdnRoomView").value == 2 || document.getElementById("hdnRoomView").value == 3)) || (document.getElementById("selStatus").value != 0 && (document.getElementById("hdnRoomView").value !=1 || document.getElementById("hdnRoomView").value ==1) && selectedRooms == 0 ))//FB 2948
            document.getElementById("tdSelectRooms").style.display='block';                 
        else
            document.getElementById("tdSelectRooms").style.display='none';
       
} //end function
 
 //FB 2598 Starts

    function removeOption()
    {
        if("<%=Session["EnableCallmonitor"]%>" == "0")
        {
        
        var x=document.getElementById("lstCalendar");
        x.remove(4);
        x.remove(3);
        }
       
    }

 removeOption();
//FB 2598 Ends
//ZD 100157 Start
if(document.getElementById("reglstStartHrs") != null)
  document.getElementById("reglstStartHrs").controltovalidate = "lstStartHrs_Text"; 
    if (document.getElementById("reqlstStartHrs") != null)
        document.getElementById("reqlstStartHrs").controltovalidate = "lstStartHrs_Text";
    if (document.getElementById("reqlstEndHrs") != null)
        document.getElementById("reqlstEndHrs").controltovalidate = "lstEndHrs_Text"; 
    if (document.getElementById("reglstEndHrs") != null)
        document.getElementById("reglstEndHrs").controltovalidate = "lstEndHrs_Text";
    /*
    if(document.getElementById("lstStartHrs_Text") != null)
        document.getElementById("lstStartHrs_Text").setAttribute("onchange","javascript:__doPostBack('btnbsndhrs', '');");
    if(document.getElementById("lstEndHrs_Text") != null)
        document.getElementById("lstEndHrs_Text").setAttribute("onchange","javascript:__doPostBack('btnbsndhrs', '');");
    */
    
  function fnDetectChange()
  {

    var hdn1 = document.getElementById("hdnlstStartHrs");
    var hdn2 = document.getElementById("hdnlstEndHrs");
    var obj1 = document.getElementById("lstStartHrs_Text");
    var obj2 = document.getElementById("lstEndHrs_Text");
    
    if(hdn1.value != obj1.value || hdn2.value != obj2.value)
    {
          hdn1.value = obj1.value;
          hdn2.value = obj2.value;
          //__doPostBack('btnbsndhrs', '');
          chnghrs();
    }
    setTimeout("fnDetectChange()",250);
    
  }
  
  function fnStartDetectChange()
  {
    document.getElementById("hdnlstStartHrs").value = document.getElementById("lstStartHrs_Text").value;
    document.getElementById("hdnlstEndHrs").value = document.getElementById("lstEndHrs_Text").value;
    if(document.getElementById("officehrDaily").checked == true)
        fnDetectChange();
  }
  setTimeout("fnStartDetectChange()",1000);
  
  function fncheckTime() {
        var stdate = '';
         var stime = document.getElementById("lstStartHrs_Text").value;
        var etime = document.getElementById("lstEndHrs_Text").value;
          if('<%=Session["timeFormat"]%>' == "2") 
        {
             stime = stime.replace('Z', '')
            stime = stime.substring(0, 2) + ":" + stime.substring(2, 4);
            
            etime = etime.replace('Z', '')
            etime = etime.substring(0, 2) + ":" + etime.substring(2, 4);
        }
        if (document.getElementById("lstEndHrs_Text") && document.getElementById("lstStartHrs_Text")) {
            stdate = GetDefaultDate('01/01/1901', '<%=((Session["timeFormat"] == null) ? "1" : Session["timeFormat"])%>');
            
            if (Date.parse(stdate + " " + etime) < Date.parse(stdate + " " + stime)) {
                alert("End Time should be greater than Start Time."); //FB 2148
                document.getElementById("lstStartHrs_Text").focus();
                return false;
            }
            else if (Date.parse(stdate + " " + etime) == Date.parse(stdate + " " + stime)) {
                alert("End Time should be greater than Start Time.");
                document.getElementById("lstEndHrs_Text").focus();
                return false;
            }
        }
        return true;
    }
    
setTimeout("document.getElementById('officehrDaily').style.marginLeft = '1px'",100);

function fnTimeScroll()
{
var StartHr;
var n = 0;
var d = new Date();
var t = d.getHours();
if(document.getElementById("lstStartHrs_Text") !=null)
    StartHr = document.getElementById("lstStartHrs_Text").value.toLowerCase();
    
if(StartHr.indexOf("z") > -1)
    n = parseInt(StartHr.substring(0,2),10);
    
else if(StartHr.indexOf("am") > -1 || StartHr.indexOf("pm") > -1)
{
    if(StartHr.indexOf("12") > -1 && StartHr.indexOf("am") > -1)
        n = 0;
    else
        n = parseInt(StartHr.substring(0,2),10);
    
    if(StartHr.indexOf("pm") > -1)
        n += 12;
}
else
{
    n = parseInt(StartHr.substring(0,2),10);
}

var pos;

if(document.getElementById("officehrDaily").checked == true)
    pos = (t * 200) - (n * 200);
else
    pos = t * 200;

pos -= 400;
var dv = document.getElementById("CalendarContainer_schDaypilot").childNodes[0].childNodes[0].childNodes[1].childNodes[1].childNodes[0];
dv.scrollLeft=pos;

//alert("n: " + n + ", pos:" + pos);
}
ShowHrs();
//ZD 100157 Ends

 </script>

<% if (Request.QueryString["hf"] != null)
   {
       if(Request.QueryString["hf"] != "1" )
       {
%>
        <div class="btprint">        
        <!-- FB 2719 Starts -->
        <% if (Session["isExpressUser"].ToString() == "1"){%>
        <!-- #INCLUDE FILE="inc/mainbottomNETExp.aspx" -->
        <%}else{%>
        <!-- #INCLUDE FILE="inc/mainbottomNET.aspx" -->
        <%}%>
        <!-- FB 2719 Ends -->
        </div>
<%     } 
   }
%>