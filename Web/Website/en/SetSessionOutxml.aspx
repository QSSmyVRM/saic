<%--ZD 100147 start--%>
<%--/* Copyright (C) 2013 myVRM - All Rights Reserved	
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.		
*
* You should have received a copy of the myVRM license with	
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/--%><%--ZD 100147 End--%>
<%@ Page Language="C#"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<script runat="server">
// We iterate through the Form collection and assign the names and values
// to ASP.NET session variables! We have another Session Variable, "DestPage"
// that tells us where to go after taking care of our business...
private void Page_Load(object sender, System.EventArgs e)
{
     //Response.Write(Session["outxml"].ToString().Trim());
    //Session["outxml"] = Session["outxml"].ToString().Replace("\n", " ").ToString();
    //Session["outxml"] = Session["outxml"].ToString().Replace("<", "&lt;").ToString();
    //Session["outxml"] = Session["outxml"].ToString().Replace(">","&gt;").ToString();
    
    Session["outXML"] = Session["outXML"].ToString().Replace('\'',' ').ToString();
    Session["outXML"] = Session["outXML"].ToString().Replace("&","&amp;").ToString();
    Response.Write("<form name='frmSetoutml' action='GetSessionOutxml.aspx' onsubmit='return true' method='post'>");
    Response.Write("<textarea name='outxml' rows='1' cols='4'>" + Session["outxml"].ToString() + "</textarea>");
    string qstring = Request.QueryString["tp"].ToString() + "?wintype=pop";
    if (Request.QueryString["f"] != null)
         qstring += "&f=" + Request.QueryString["f"].ToString();
    if (Request.QueryString["n"] != null)
        qstring += "&n=" + Request.QueryString["n"].ToString();
    //Code added by revathi  for aspx conversion --start.
    if (Request.QueryString["GroupID"] != null)
        qstring += "&GroupID=" + Request.QueryString["GroupID"].ToString();
    //Code added by revathi for aspx conversion--end
    if (Request.QueryString["ii"] != null)
        qstring += "&ii=" + Request.QueryString["ii"].ToString();
    Response.Write("<input type='hidden' name='tp' value='" + qstring + "'>");
    Response.Write("</form>");
    //Response.End();
}
</script>
<HTML>

<body>
<textarea cols="40" value=""></textarea>
</body>
</HTML>
<script language="javascript">
    setTimeout("document.frmSetoutml.submit()",0);
</script>