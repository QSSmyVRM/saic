<%--ZD 100147 start--%>
<%--/* Copyright (C) 2013 myVRM - All Rights Reserved	
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.		
*
* You should have received a copy of the myVRM license with	
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/--%><%--ZD 100147 End--%>
<%@ Page Language="C#" AutoEventWireup="true" Inherits="en_ShowError.ShowError" %>

<%--FB 2779 Starts--%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<meta http-equiv="X-UA-Compatible" content="IE=8" />
<%--ZD 100170--%>
<% if (Session["isExpressUser"].ToString() == "1"){%>
<!-- #INCLUDE FILE="inc/maintopNETExp.aspx" -->
<%}else{%>
<!-- #INCLUDE FILE="inc/maintopNET.aspx" -->
<%}%><%--FB 2779 Ends--%>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title>myVRM Video Conference Reservation Scheduler</title>
</head>
<body>
<%
if(paramWinType == "inifr") 
{
%>

<script language="JavaScript" type="text/javascript">
<!--

	parent.window.location.href = "showerror.aspx";

-->
</script>

<%
}
%>


<script language="javascript" src="extract.js" type="text/javascript"></script>
<script language="javascript" type="text/javascript">
<!--

	if ( (opener) && (queryField("winType") != "pop") )
		location.href = "showerror.aspx?winType=pop";

-->
</script>








<%
if((Session["sMenuMask"] == "") || (paramWinType == "pop") || (paramWinType == "ifr"))
{
%>

<!--
<body bottommargin="0" leftmargin="0" rightmargin="0" topmargin="0" marginheight="0" marginwidth="0">
  <table width="100%" border="0" cellpadding="0" cellspacing="0">
    <tr valign="top">
      <td background="image/vrmtop.gif" width="100%" height="72">
      </td>
    </tr>
  </table>
  -->
  

  
<%
}
else
{
%>

<!--maintopNet.asp-->
<%
}
%>

<br>
<table width="100%">

  <tr>
    <td align="center" height="10">
    </td>
  </tr>
  <tr><%--ZD 100170--%>
    <td align="center" >
    <br /><br /><br /><br /><br /><!-- TIK# 100108 -->
      <font size="3"><b> Your request could not be completed due to system error</b></font><br /><br />
      <font size="2"><b> Please contact an Administrator for further assistance</b></font>
      <div style="height:150px"></div>
    </td>
  </tr>
  <tr>
    <td align="center" height="20">
    </td>
  </tr>
    <tr>
    <td align="center">
<% 
	
	if(Session["errMsg"] != null)
	{
	    if(Session["errMsg"].ToString() != "")
	    {
	    String strError = "";

	    strError = strError + "<font size='3' color='red'><b>";
	    strError = strError + Session["errMsg"].ToString();
    	
	    if(Session["errCode"].ToString() != "")
	    
	    {
		    strError = strError + "<br>";
		    strError = strError + "Error Number #" + Session["errCode"].ToString();
        }    	
	    strError = strError + "</b></font>"; 
	    Response.Write(strError);  
	    }
	}

	 
%> 
    </td>
  </tr>
</table>

<br/>



	
<table width="100%">
  <tr>
    <td align="center">
      &nbsp;
<%
if((Session["sMenuMask"].ToString() == "") || (paramWinType == "pop") || (paramWinType == "ifr")) 
{
%>
      </td>
    </tr>
  </table>	


<%
}
else
{
%>

<br/><br/><br/>
<%
}
%>
</body>
<%--code added for Soft Edge button--%>
<script type="text/javascript" src="inc/softedge.js"></script><%--ZD 100170--%>
<% if (Session["isExpressUser"].ToString() == "1"){%>
<!-- #INCLUDE FILE="inc/mainbottomNETExp.aspx" -->
<%}else{%>
<!-- #INCLUDE FILE="inc/mainbottomNET.aspx" -->
<%}%>
</html>
<script type="text/javascript">
    function fnChkWinType() {
        var url1 = window.parent.location.href;
        var url2 = window.location.href;
        var url3 = window.opener;

        if (url3 != undefined || url3 != null) {
            window.opener.location.href = "showerror.aspx"
            self.close();
        }
        if (url1 != url2) {
            window.parent.location.href = "showerror.aspx";
        }
    }
    fnChkWinType()
</script>