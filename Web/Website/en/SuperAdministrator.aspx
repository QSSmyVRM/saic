<%--ZD 100147 start--%>
<%--/* Copyright (C) 2013 myVRM - All Rights Reserved	
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.		
*
* You should have received a copy of the myVRM license with	
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/--%><%--ZD 100147 End--%>
<%@ Page Language="C#" Inherits="ns_SuperAdministrator.SuperAdministrator" %>

<%@ Register Assembly="MetaBuilders.WebControls.ComboBox" Namespace="MetaBuilders.WebControls"
    TagPrefix="mbcbb" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajax" %>
<%@ Register TagPrefix="cc1" Namespace="myVRMWebControls" Assembly="myVRMWebControls" %>
<%--Edited for FF--%>
<%if (Request.ServerVariables["HTTP_USER_AGENT"].ToString().ToUpper().Contains("MSIE"))
{%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%}
else {%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<%} %>
<meta http-equiv="X-UA-Compatible" content="IE=8" />
<!--window Dressing-->
<!-- #INCLUDE FILE="inc/maintopNET.aspx" -->
<script runat="server"></script>
<script type="text/javascript" language="javascript" src='script/lib.js'></script>
<script type="text/javascript" src="script/calview.js"></script>
<script type="text/javascript" src="inc/functions.js"></script>

<script type="text/javascript" src="script/CallMonitorJquery/jquery.1.4.2.js"></script><%--FB 2659--%>
<script type="text/javascript" src="script/CallMonitorJquery/jquery.bpopup-0.7.0.min.js"></script>
<script type="text/javascript" src="script/CallMonitorJquery/json2.js" ></script>

<script language="JavaScript">
<!--





    //FB Case 807 starts here
    function SavePassword()// FB 1896
    {

        if (document.getElementById("hdnMailServer").value != "") {

            document.getElementById("txtMSPassword1").value = document.getElementById("hdnMailServer").value;
            document.getElementById("txtMSPassword2").value = document.getElementById("hdnMailServer").value;
        }
        if (document.getElementById("hdnLDAPPassword").value != "") {
            document.getElementById("txtLDAPAccountPassword1").value = document.getElementById("hdnLDAPPassword").value;
            document.getElementById("txtLDAPAccountPassword2").value = document.getElementById("hdnLDAPPassword").value;
        }
        //FB 2363
        if (document.getElementById("hdnExternalPassword").value != "") {
            document.getElementById("txtP1Password").value = document.getElementById("hdnExternalPassword").value;
            document.getElementById("txtP2Password").value = document.getElementById("hdnExternalPassword").value;
        }
        // FB 2501 EM7 Starts
        if (document.getElementById("hdnEM7Password").value != "") {
            document.getElementById("txtEM7Password").value = document.getElementById("hdnEM7Password").value;
            document.getElementById("txtEM7ConformPassword").value = document.getElementById("hdnEM7Password").value;
        }
        // FB 2501 EM7 Ends
    }
	//FB 2659
    function PopupWindow() {
        var popwin = window.showModalDialog("DefaultLicense.aspx", document, "resizable: yes; scrollbar: no; help: yes; status:yes; dialogHeight:580px; dialogWidth:510px");
    }


    function PreservePassword()// FB 1896
    {
        document.getElementById("hdnMailServer").value = document.getElementById("txtMSPassword1").value
        document.getElementById("hdnLDAPPassword").value = document.getElementById("txtLDAPAccountPassword2").value;
        document.getElementById("hdnExternalPassword").value = document.getElementById("txtP2Password").value; //FB 2363
        document.getElementById("hdnEM7Password").value = document.getElementById("txtEM7Password").value; //FB 2501 EM7
    }
    //FB Case 807 ends here
    function TestLDAPServerConnection() {
        if (document.getElementById("txtLDAPAccountPassword1").value == "") {
            alert("Please enter a value to Test LDAP Server connection.");
            document.getElementById("txtLDAPAccountPassword1").focus();
            return false;
        }

        if (document.getElementById("txtLDAPAccountLogin").value == "") {
            alert("Please enter a value to Test LDAP Server connection.");
            document.getElementById("txtLDAPAccountLogin").focus();
            return false;
        }

        if (document.getElementById("txtLDAPServerPort").value == "") {
            alert("Please enter a value to Test LDAP Server connection.");
            document.getElementById("txtLDAPServerPort").focus();
            return false;
        }
        if (document.getElementById("txtLDAPServerAddress").value == "") {
            alert("Please enter a valid Address.");
            document.getElementById("txtLDAPServerAddress").focus();
            return false;
        }
        if (document.getElementById("lstLDAPConnectionTimeout").value == "") {
            alert("Please enter a valid connection timeout value.");
            document.getElementById("lstLDAPConnectionTimeout").focus();
            return false;
        }
        //	alert(document.frmMainsuperadministrator.LDAPConnectionTimeout.value);
        url = "dispatcher/admindispatcher.asp?cmd=TestLDAPConnection" + "&sadd=" + escape(document.getElementById("txtLDAPServerAddress").value) + "&port=" + document.getElementById("txtLDAPServerPort").value + "&lg=" + escape(document.getElementById("txtLDAPAccountLogin").value) + "&pd=" + escape(document.getElementById("txtLDAPAccountPassword1").value) + "&cto=" + document.getElementById("lstLDAPConnectionTimeout").value;

        if (!window.winrtc) {	// has not yet been defined
            winrtc = window.open(url, "", "width=700,height=300,top=0,left=0,resizable=yes,scrollbars=yes,status=no");
            winrtc.focus();
        } else // has been defined
            if (!winrtc.closed) {     // still open
            winrtc.close();
            winrtc = window.open(url, "", "width=700,height=300,top=0,left=0,resizable=yes,scrollbars=yes,status=no");
            winrtc.focus();
        } else {
            winrtc = window.open(url, "", "width=700,height=300,top=0,left=0,resizable=yes,scrollbars=yes,status=no");
            winrtc.focus();
        }
    }


    function TestMailServerConnection() {
        if (document.getElementById("txtMailServerLogin").value == "") {
            alert("Please enter a login name to test Mail Server connection.");
            document.getElementById("txtMailServerLogin").focus();
            return false;
        }

        if (document.getElementById("txtMSPassword1").value == "") {
            alert("Please enter a password to test Mail Server connection.");
            document.getElementById("txtMSPassword1").focus();
            return false;
        }

        if (document.getElementById("txtServerAddress").value == "") {
            alert("Please enter a server address to test Mail Server connection.");
            document.getElementById("txtServerAddress").focus();
            return false;
        }
        url = "dispatcher/admindispatcher.asp?cmd=TestMailConnection" + "&sa=" + escape(document.getElementById("txtServerAddress").value) + "&lg=" + escape(document.getElementById("txtMailServerLogin").value) + "&sp=" + document.getElementById("txtServerPort").value + "&se=" + escape(document.getElementById("txtSystemEmail").value) + "&dn=" + escape(document.getElementById("txtDisplayName").value) + "&pd=" + escape(document.getElementById("txtMSPassword1").value);
        //alert(url);
        if (!window.winrtc) {	// has not yet been defined
            winrtc = window.open(url, "", "width=700,height=300,top=0,left=0,resizable=yes,scrollbars=yes,status=no");
            winrtc.focus();
        } else // has been defined
            if (!winrtc.closed) {     // still open
            winrtc.close();
            winrtc = window.open(url, "", "width=700,height=300,top=0,left=0,resizable=yes,scrollbars=yes,status=no");
            winrtc.focus();
        } else {
            winrtc = window.open(url, "", "width=700,height=300,top=0,left=0,resizable=yes,scrollbars=yes,status=no");
            winrtc.focus();
        }
    }


    function getYourOwnEmailList(i) {
        url = "dispatcher/conferencedispatcher.asp?frm=approverNET&frmname=frmMainsuperadministrator&no=" + i + "&cmd=GetEmailList&emailListPage=1&wintype=pop";
        if (!window.winrtc) {	// has not yet been defined
            winrtc = window.open(url, "", "width=700,height=300,top=0,left=0,resizable=yes,scrollbars=yes,status=no");
            winrtc.focus();
        } else // has been defined
            if (!winrtc.closed) {     // still open
            winrtc.close();
            winrtc = window.open(url, "", "width=700,height=300,top=0,left=0,resizable=yes,scrollbars=yes,status=no");
            winrtc.focus();
        } else {
            winrtc = window.open(url, "", "width=700,height=300,top=0,left=0,resizable=yes,scrollbars=yes,status=no");
            winrtc.focus();
        }
    }

    function frmMainsuperadministrator_Validator() {
        if (Trim(document.getElementById("txtSystemEmail").value) == "") {
            alert(EN_81);
            document.getElementById("txtSystemEmail").focus();
            return (false);
        }

        if (!checkemail(document.getElementById("txtSystemEmail").value)) {
            alert(EN_81);
            document.getElementById("txtSystemEmail").focus();
            return (false);
        }

        if (Trim(document.getElementById("txtwebsiteURL").value) == "") {
            alert("Please enter a value for Website URL.");
            document.getElementById("txtwebsiteURL").focus();
            return (false);
        }

        if (Trim(document.getElementById("txtServerAddress").value) == "") {
            alert(EN_82);
            document.getElementById("txtServerAddress").focus();
            return (false);
        }

        if (Trim(document.getElementById("txtServerPort").value) == "") {
            alert(EN_83);
            document.getElementById("txtServerPort").focus();
            return (false);
        }

        if (isPositiveInt(document.getElementById("txtServerPort").value, "Port No") != 1) {
            document.getElementById("txtServerPort").focus();
            return (false);
        }

        if (Trim(document.getElementById("txtLDAPServerPort").value) != "") {
            if (isPositiveInt(document.getElementById("txtLDAPServerPort").value, "LDAP Port No") != 1) {
                document.getElementById("txtLDAPServerPort").focus();
                return (false);
            }

            pn = parseInt(document.getElementById("txtLDAPServerPort").value, 10);
            if (pn != 389) {
                var isChangeLDAPPortNo = confirm("Are you sure you do not want to use 389 as LDAP Port No?")
                if (isChangeLDAPPortNo == false) {
                    document.getElementById("txtLDAPServerPort").focus()
                    return (false);
                }
            }
        }
        return (true);
    }
    //Code addded for organisation
    function OpenOrg() {
        //window.location.replace("ManageOrganization.aspx"); //FB 2565
        DataLoading(1); // ZD 100176 
        window.location = 'ManageOrganization.aspx';
    }

    //Code added for displaying User login FB 1969
    function Openuser() {
        //window.location.replace("UserHistoryReport.aspx"); //FB 2565
        DataLoading(1); // ZD 100176  
        window.location='UserHistoryReport.aspx';
    }
    /*Commented for FB 1849
    function fnChangeOrganization()
    {
    var btnchng = document.getElementById("BtnChangeOrganization");
    var drporg = document.getElementById("DrpOrganization");
    var cnfrm = confirm("Hereafter all the transactions performed in system will be for the selected organization.Do you wish to continue");
            
    if(cnfrm)
    return true;
    else
    return false;
   
    }*/
 
//-->
    //FB 2594 Starts
    function SetButtonStatus(sender, target) {

        if (sender.value.length > 0)

            document.getElementById(target).disabled = false;

        else

            document.getElementById(target).disabled = true;

    }
    //    function PollWhygo() {
    //        if ((document.getElementById("txtWhyGoURL").value == "") || (document.getElementById("txtWhygoUsr").value == "") || document.getElementById("trWhygoPassword").value == "") ){
    //            alert("Please enter the Whygo Integration Settings.");
    //            document.getElementById("txtWhyGoURL").focus();
    //            return false;
    //        } 
    //    }
    //FB 2594 Ends
    //FB 3054 Starts
    function SMTPTC() {
        document.getElementById("btnTestMailConnection").disabled = "true";
        document.getElementById("btnTestMailConnection").className = "btndisable";
    }
    function LDAPTC() {
        document.getElementById("btnTestLDAPConnection").disabled = "true";
        document.getElementById("btnTestLDAPConnection").className = "btndisable";
    }
    function PasswordChange(par,par1) {
        if (par == 1) {

            document.getElementById("hdnSMTPPW").value = true;
            if (par1 == 1)
                document.getElementById("hdnSMTP1Visit").value = true;
            else
                document.getElementById("hdnSMTP2Visit").value = true;
        }
        else if (par == 2) {

            document.getElementById("hdnLDAPPW").value = true;
            if (par1 == 1)
                document.getElementById("hdnLDAP1Visit").value = true;
            else
                document.getElementById("hdnLDAP2Visit").value = true;
        }
        else if (par == 3) {

            document.getElementById("hdnEM7PW").value = true;
            if (par1 == 1)
                document.getElementById("hdnEM71Visit").value = true;
            else
                document.getElementById("hdnEM72Visit").value = true;
        }
        else if (par == 4) {

            document.getElementById("hdnExternalPW").value = true;
            if (par1 == 1)
                document.getElementById("hdnExt1Visit").value = true;
            else
                document.getElementById("hdnExt2Visit").value = true;
        }
        else if (par == 5) {

            document.getElementById("hdnWhygoPW").value = true;
            if (par1 == 1)
                document.getElementById("hdnWhygo1Visit").value = true;
            else
                document.getElementById("hdnWhygo2Visit").value = true;
        }
    }
    function fnTextFocus(xid, par, par2) {

        var obj = document.getElementById(xid);
        if (par2 == 1) {
            // ZD 100263 Starts
            var obj1 = document.getElementById("txtMSPassword1");
            var obj2 = document.getElementById("txtMSPassword2");
            if (obj1.value == "" && obj2.value == "") {
                document.getElementById("txtMSPassword1").style.backgroundImage = "";
                document.getElementById("txtMSPassword2").style.backgroundImage = "";
                document.getElementById("txtMSPassword1").value = "";
                document.getElementById("txtMSPassword2").value = "";
            }
            return false;
            // ZD 100263 Ends
        
            if (par == 1) {
                if (document.getElementById("hdnSMTP2Visit") != null) {
                    if (document.getElementById("hdnSMTP2Visit").value == "false") {
                        document.getElementById("txtMSPassword1").value = "";
                        document.getElementById("txtMSPassword2").value = "";
                    } else {
                        document.getElementById("txtMSPassword1").value = "";
                    }
                }
                else {
                    document.getElementById("txtMSPassword1").value = "";
                    document.getElementById("txtMSPassword2").value = "";
                }
            } else {
                if (document.getElementById("hdnSMTP1Visit") != null) {
                    if (document.getElementById("hdnSMTP1Visit").value == "false") {
                        document.getElementById("txtMSPassword1").value = "";
                        document.getElementById("txtMSPassword2").value = "";
                    } else {
                        document.getElementById("txtMSPassword2").value = "";
                    }
                }
                else {
                    document.getElementById("txtMSPassword1").value = "";
                    document.getElementById("txtMSPassword2").value = "";
                }
            }
        }
        else if (par2 == 2) {
        // ZD 100263 Starts
        var obj1 = document.getElementById("txtLDAPAccountPassword1");
        var obj2 = document.getElementById("txtLDAPAccountPassword2");
        if (obj1.value == "" && obj2.value == "") {
            document.getElementById("txtLDAPAccountPassword1").style.backgroundImage = "";
            document.getElementById("txtLDAPAccountPassword2").style.backgroundImage = "";
            document.getElementById("txtLDAPAccountPassword1").value = "";
            document.getElementById("txtLDAPAccountPassword2").value = "";
        }
        return false;
        // ZD 100263 Ends
            if (par == 1) {

                if (document.getElementById("hdnLDAP2Visit") != null) {
                    if (document.getElementById("hdnLDAP2Visit").value == "false") {
                        document.getElementById("txtLDAPAccountPassword1").value = "";
                        document.getElementById("txtLDAPAccountPassword1").value = "";
                    } else {
                        document.getElementById("txtLDAPAccountPassword1").value = "";
                    }
                }
                else {
                    document.getElementById("txtLDAPAccountPassword1").value = "";
                    document.getElementById("txtLDAPAccountPassword2").value = "";
                }
            } else {
                if (document.getElementById("hdnLDAP1Visit") != null) {
                    if (document.getElementById("hdnLDAP1Visit").value == "false") {
                        document.getElementById("txtLDAPAccountPassword1").value = "";
                        document.getElementById("txtLDAPAccountPassword2").value = "";
                    } else {
                        document.getElementById("txtLDAPAccountPassword2").value = "";
                    }
                }
                else {
                    document.getElementById("txtLDAPAccountPassword1").value = "";
                    document.getElementById("txtLDAPAccountPassword2").value = "";
                }
            }

        }
        else if (par2 == 3) {
        // ZD 100263 Starts
        var obj1 = document.getElementById("txtEM7Password");
        var obj2 = document.getElementById("txtEM7ConformPassword");
        if (obj1.value == "" && obj2.value == "") {
            document.getElementById("txtEM7Password").style.backgroundImage = "";
            document.getElementById("txtEM7ConformPassword").style.backgroundImage = "";
            document.getElementById("txtEM7Password").value = "";
            document.getElementById("txtEM7ConformPassword").value = "";
        }
        return false;
        // ZD 100263 Ends
            if (par == 1) {

                if (document.getElementById("hdnEM72Visit") != null) {
                    if (document.getElementById("hdnEM72Visit").value == "false") {
                        document.getElementById("txtEM7Password").value = "";
                        document.getElementById("txtEM7ConformPassword").value = "";
                        document.getElementById("hdnEM7Password").value = "";
                    } else {
                        document.getElementById("txtEM7Password").value = "";
                        document.getElementById("hdnEM7Password").value = "";
                    }
                }
                else {
                    document.getElementById("txtEM7Password").value = "";
                    document.getElementById("txtEM7ConformPassword").value = "";
                    document.getElementById("hdnEM7Password").value = "";
                }
            } else {
                if (document.getElementById("hdnEM71Visit") != null) {
                    if (document.getElementById("hdnEM71Visit").value == "false") {
                        document.getElementById("txtEM7Password").value = "";
                        document.getElementById("txtEM7ConformPassword").value = "";
                        document.getElementById("hdnEM7Password").value = "";
                    } else {
                        document.getElementById("txtEM7ConformPassword").value = "";
                        document.getElementById("hdnEM7Password").value = "";
                    }
                }
                else {
                    document.getElementById("txtEM7Password").value = "";
                    document.getElementById("txtEM7ConformPassword").value = "";
                    document.getElementById("hdnEM7Password").value = "";
                }
            }
        }
        else if (par2 == 4) {
        // ZD 100263 Starts
        var obj1 = document.getElementById("txtP1Password");
        var obj2 = document.getElementById("txtP2Password");
        if (obj1.value == "" && obj2.value == "") {
            document.getElementById("txtP1Password").style.backgroundImage = "";
            document.getElementById("txtP2Password").style.backgroundImage = "";
            document.getElementById("txtP1Password").value = "";
            document.getElementById("txtP2Password").value = "";
        }
        return false;
        // ZD 100263 Ends
            if (par == 1) {

                if (document.getElementById("hdnExt2Visit") != null) {
                    if (document.getElementById("hdnExt2Visit").value == "false") {
                        document.getElementById("txtP1Password").value = "";
                        document.getElementById("txtP2Password").value = "";
                        document.getElementById("hdnExternalPassword").value = "";
                    } else {
                        document.getElementById("txtP1Password").value = "";
                        document.getElementById("hdnExternalPassword").value = "";
                    }
                }
                else {
                    document.getElementById("txtP1Password").value = "";
                    document.getElementById("txtP2Password").value = "";
                    document.getElementById("hdnExternalPassword").value = "";
                }
            } else {
                if (document.getElementById("hdnExt1Visit") != null) {
                    if (document.getElementById("hdnExt1Visit").value == "false") {
                        document.getElementById("txtP1Password").value = "";
                        document.getElementById("txtP2Password").value = "";
                        document.getElementById("hdnExternalPassword").value = "";
                    } else {
                        document.getElementById("txtP2Password").value = "";
                        document.getElementById("hdnExternalPassword").value = "";
                    }
                }
                else {
                    document.getElementById("txtP1Password").value = "";
                    document.getElementById("txtP2Password").value = "";
                    document.getElementById("hdnExternalPassword").value = "";
                }
            }
        } else if (par2 == 5) {
        // ZD 100263 Starts
        var obj1 = document.getElementById("txtWhygoPwd");
        var obj2 = document.getElementById("txtWhygoPwd2");
        if (obj1.value == "" && obj2.value == "") {
            document.getElementById("txtWhygoPwd").style.backgroundImage = "";
            document.getElementById("txtWhygoPwd2").style.backgroundImage = "";
            document.getElementById("txtWhygoPwd").value = "";
            document.getElementById("txtWhygoPwd2").value = "";
        }
        return false;
        // ZD 100263 Ends
            if (par == 1) {

                if (document.getElementById("hdnWhygo2Visit") != null) {
                    if (document.getElementById("hdnWhygo2Visit").value == "false") {
                        document.getElementById("txtWhygoPwd").value = "";
                        document.getElementById("txtWhygoPwd2").value = "";
                    } else {
                        document.getElementById("txtWhygoPwd").value = "";
                    }
                }
                else {
                    document.getElementById("txtWhygoPwd").value = "";
                    document.getElementById("txtWhygoPwd2").value = "";
                }
            } else {
                if (document.getElementById("hdnWhygo1Visit") != null) {
                    if (document.getElementById("hdnWhygo1Visit").value == "false") {
                        document.getElementById("txtWhygoPwd").value = "";
                        document.getElementById("txtWhygoPwd2").value = "";
                    } else {
                        document.getElementById("txtWhygoPwd2").value = "";
                    }
                }
                else {
                    document.getElementById("txtWhygoPwd").value = "";
                    document.getElementById("txtWhygoPwd2").value = "";
                }
            }
        }

        if (document.getElementById("cmpValPassword1") != null) {
            ValidatorEnable(document.getElementById('cmpValPassword1'), false);
            ValidatorEnable(document.getElementById('cmpValPassword1'), true);
        }
    }
    //FB 3054 Ends

    //ZD 100176 start
    function DataLoading(val) {
        if (val == "1")
            document.getElementById("dataLoadingDIV").innerHTML = "<img border='0' src='image/wait1.gif'>";
        else
            document.getElementById("dataLoadingDIV").innerHTML = "";
    }
    //ZD 100176 End
</script>

<script type="text/javascript" src="script\approverdetails.js">

</script>

<html xmlns="http://www.w3.org/1999/xhtml">
<%--FB 1969 Start--%>
<head>
    <style type="text/css">
        #btnuser
        {
            width: 100px;
        }
    </style>
    <%--FB 2363 End--%>
    <title>Super administartor</title>
    <script type="text/javascript" src="script/cal-flat.js"></script>
    <script type="text/javascript" src="lang/calendar-en.js"></script>
    <script type="text/javascript" src="script/calendar-setup.js"></script>
    <script type="text/javascript" src="script/calendar-flat-setup.js"></script>
    <link rel="stylesheet" type="text/css" media="all" href="css/calendar-blue.css" />
    <%--FB 1982--%>
</head>
<%--FB 1969 End--%>
<body>
    <form name="frmMainsuperadministrator" id="frmMainsuperadministrator" method="Post"
    action="SuperAdministrator.aspx" language="JavaScript" runat="server">
    <!-- -->
    <asp:ScriptManager ID="CalendarScriptManager" runat="server" AsyncPostBackTimeout="600">
    </asp:ScriptManager>
    <center>
        <input type="hidden" id="helpPage" value="92">
        <input type="hidden" id="hdnMailServer" runat="server" />
        <input type="hidden" id="hdnLDAPPassword" runat="server" />
        <input type="hidden" id="hdnExternalPassword" runat="server" />
        <input type="hidden" id="hdnESMailUsrSent" runat="server" /> <%--FB 2363--%>
        <input type="hidden" id="hdnEM7Password" runat="server" /><%-- FB 2501 EM7--%>
        <%--FB 3054 Starts--%>
        <input type="hidden" id="hdnSMTPPW" runat="server" /> 
        <input type="hidden" id="hdnLDAPPW" runat="server" /> 
        <input type="hidden" id="hdnEM7PW" runat="server" /> 
        <input type="hidden" id="hdnExternalPW" runat="server" /> 
        <input type="hidden" id="hdnWhygoPW" runat="server" /> 
        <input type="hidden" id="hdnSMTP1Visit" value="false" runat="server" /> 
        <input type="hidden" id="hdnSMTP2Visit" value="false" runat="server" /> 
        <input type="hidden" id="hdnLDAP1Visit" value="false" runat="server" /> 
        <input type="hidden" id="hdnLDAP2Visit" value="false" runat="server" /> 
        <input type="hidden" id="hdnEM71Visit" value="false" runat="server" /> 
        <input type="hidden" id="hdnEM72Visit" value="false" runat="server" /> 
        <input type="hidden" id="hdnExt1Visit" value="false" runat="server" /> 
        <input type="hidden" id="hdnExt2Visit" value="false" runat="server" /> 
        <input type="hidden" id="hdnWhygo1Visit" value="false" runat="server" /> 
        <input type="hidden" id="hdnWhygo2Visit" value="false" runat="server" /> 
        <%--FB 3054 Ends--%>
        <h3>
            Site Settings</h3>
        <asp:Label ID="errLabel" runat="server" Text="" CssClass="lblError"></asp:Label><br />
    </center>
    <div id="dataLoadingDIV" align="center"></div> <%--ZD 100176--%>
    <table width="100%" border="0">
        <tr valign="top" id="trSwt" runat="server">
            <td colspan="5" align="left" valign="top" style="display: none">
                <a id="ChgOrg" runat="server" href="#" class="blueblodtext">Switch Organization
                </a>
            </td>
        </tr>
        <tr>
            <td align="left" class="subtitleblueblodtext" colspan="5" style="height: 21; font-weight: bold">
                License
            </td>
        </tr>
        <tr>
            <td align="left" height="21" style="font-weight: bold" width="6%">
            </td>
            <%--Window Dressing--%>
            <td align="left" class="blackblodtext" height="21" style="font-weight: bold" width="18%"
                valign="top">
                myVRM License Key
            </td>
            <td style="height: 21px;" valign="top" width="30%">
                <asp:TextBox ID="txtLicenseKey" runat="server" Columns="8" Rows="4" CssClass="altText"
                    TextMode="MultiLine" Width="213px" Height="74px"></asp:TextBox>
            </td>
            <%--Window Dressing--%>
            <td align="left" valign="top" style="font-weight: bold" class="blackblodtext">
                License Details
            </td>
            <%--Window Dressing--%>
            <td class="altblackblodttext" width="45%" colspan="2" align="left">
                <%--FB 2685 Start--%>
                <table width="100%" border="0">
                    <tr>
                        <%--<td align="left" nowrap valign="top">
                            <span class="blackblodtext"></span>
                        </td>--%>
                        <td style="height: 21px;" valign="top" width="100%">
                        <asp:TextBox ID="lblLicenseDetails" TextMode="MultiLine" ReadOnly="true" Width="300" Height="160" runat="server" CssClass ="altText" 
                              Columns="8" Rows="10"></asp:TextBox>
                            <%--<asp:Label ID="lblLicenseDetails" Style="vertical-align: baseline;" runat="server"
                                Width="95%"></asp:Label>--%>
                        </td>
                    </tr>
                </table>
                <%--FB 2685 End--%>
                <table width="100%" cellspacing="3" cellpadding="3" border="0">
                    <tr>
                        <td width="30%" valign="top" nowrap>
                            <span class="blackblodtext">License Status :</span><asp:Label ID="ActStatus" runat="server"></asp:Label>
                            <asp:Label ID="EncrypTxt" runat="server" Style="display: none;"></asp:Label>
                        </td>
                        <td id="ExportTD" runat="server" style="display: none;" align="left">
                            <asp:Button ID="TxyButton" runat="server" CssClass="altMedium0BlueButtonFormat" OnClick="btnExportTxt_Click" Width="150px"
                                Text="Export to Text" />
                        </td>
                    </tr>
                </table>
                <table>
                    <%-- added for FF--%>
                    <tr id="ActivationTR" runat="server" style="display: none;">
                        <td colspan="2" width="100%">
                            <p>
                                <font face="Verdana, Arial, Helvetica, sans-serif" size="2">Your system will be locked
                                    out on: </font>
                                <asp:Label ID="DeactLbl" runat="server" CssClass="lblError"></asp:Label>.Please
                                click above button to get activation code and mail to <span class="contacttext"><a
                                    href='mailto:Support@myvrm.com'>Support@myvrm.com</a></span> to get encrypted
                                activated license.</p>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td align="left" class="subtitleblueblodtext" colspan="5" style="height: 21; font-weight: bold">
                Organization Options
            </td>
        </tr>
        <tr>
            <td align="left" height="21" style="font-weight: bold" width="2%">
            </td>
            <td align="left" height="21" style="font-weight: bold" class="blackblodtext" width="18%">
                Organization
            </td>
            <td align="left" valign="bottom"><%--FB 2678--%>
                <input type="button" id="btnOrg" runat="server"  style="width :150pt" name="btnOrganization" value="Manage Organizations"
                   class="altLongBlueButtonFormat" onclick="javascript:OpenOrg();"/> <%--FB 2664--%>
                <%--FB 1662--%>
            </td>
            <%--FB 2678 Start--%>
            <%--<td align="left" style="height: 21px; width: 15%;">
            </td>
            <td style="height: 21px;" width="35%">
            </td>--%>
            <td align="left" height="21" style="font-weight: bold" class="blackblodtext">
                Individual Organization Expiry
            </td>
            <td style="height: 21px;">
                <asp:DropDownList ID="lstOrgExpiry" runat="server" Width="20%" CssClass="alt2SelectFormat">
                    <asp:ListItem Value="0" Text="No"></asp:ListItem>
                    <asp:ListItem Value="1" Text="Yes" Selected="True"></asp:ListItem>
                </asp:DropDownList>
            </td>
            <%--FB 2678 End--%>
        </tr>
        <tr>
            <td align="left" class="subtitleblueblodtext" colspan="5">
                User Interface Settings
            </td>
        </tr>
        <tr>
            <td align="left" height="21" style="font-weight: bold" width="2%" colspan="5">
            </td>
        </tr>
        <%-- Code Added for FB 1428--%>
        <tr style="display:none" >
            <td align="left" height="21" style="font-weight: bold" width="2%">
            </td>
            <td align="left" valign="top" style="font-weight: bold" class="blackblodtext">
                Company Message
            </td>
            <td valign="top">
                <asp:TextBox ID="txtCompanymessage" runat="server" Columns="30" Width="212px" CssClass="altText"
                    Rows="2"></asp:TextBox> <%--FB 2512--%>
                <asp:Label ID="hdnCompanymessage" Text="" Visible="false" runat="server"></asp:Label>
                <asp:Label ID="lblvalidation" Text="" runat="server"></asp:Label>
                <asp:RequiredFieldValidator Enabled="false" ID="RequiredFieldValidator9" ValidationGroup="submit"
                    runat="server" ControlToValidate="txtCompanymessage" ErrorMessage="Required."
                    Display="dynamic"></asp:RequiredFieldValidator>
            </td>
            <%-- //Commented for FB 1633 start  --%>
            <td align="left" valign="top" height="21" style="font-weight: bold" class="blackblodtext"
                nowrap>
                Standard Resolution Banner
            </td>
            <%-- //Commented for FB 1633 end  --%>
            <td valign="top" style="height: 21px;" nowrap>
                <input type="file" id="fleMap2" contenteditable="false" enableviewstate="true" size="20"
                    class="altText" runat="server" />
                <asp:Label ID="hdnUploadMap2" Text="" Visible="false" runat="server"></asp:Label>
                <input type="hidden" id="Map2ImageDt" name="Map1ImageDt" runat="server" />
                <asp:Label ID="hdnImageId1" Text="" Visible="false" runat="server"></asp:Label>
                <span class="blackItalictext">(1024 x 72 pixels)</span>
                <asp:ImageButton ImageUrl="image/btn_delete.gif" runat="server" ID="btnRemoveMap2"
                    ToolTip="Delete" OnCommand="RemoveStdBanner" /> <%--FB 2798--%>
            </td>
        </tr>
        <tr>
            <%-- //Commented for FB 1633 start  --%>
            <td align="left" style="font-weight: bold" width="2%">
            </td>
            <%--FB 2512 Starts--%>
            <%--</td>
            <td>
            </td>
            <td>
            </td>--%> 
            <td align="left" valign="top" style="font-weight: bold" class="blackblodtext" style="height: 9px;">
                Login History
            </td>
            <td align="left" valign="bottom">
                <input type="button" id="btnuser" runat="server" name="btnview" value="View" class="altLongBlueButtonFormat"
                    onclick="javascript:Openuser();" style ="margin-bottom: 5px" /> <%--FB 2512--%>
            </td>
            <td align="left" valign="top" style="font-weight: bold" class="blackblodtext" style="height: 9px">
                Site Logo
            </td>
            <%--FB 2512 Ends--%>
            <%-- //Commented for FB 1633 end  --%>
            <td valign="top" style="height: 9px;" nowrap="nowrap"><%--FB 2909 Start--%>
             <div>
               <input type="text" class="file_input_textbox" readonly="readonly" value='No file selected' style=" height:24px"/>
                <div class="file_input_div"><input type="button" value="Browse" class="file_input_button" style="vertical-align:middle"  />
                  <input type="file"  class="file_input_hidden" accept="image/*" id="fleMap1" contenteditable="false" enableviewstate="true" runat="server" onchange="getfilename(this)"/></div><%--FB 3055-Filter in Upload Files--%>
                  </div>
                <%--<input type="file" id="fleMap1" contenteditable="false" enableviewstate="true" size="20"
                    class="altText" runat="server" />--%><%--FB 2909 End--%>
                <span class="blackItalictext">(244 x 88 pixels)</span><%--(122 x 44)FB 2407--%>
                <asp:ImageButton ImageUrl="image/btn_delete.gif" runat="server" ID="btnRemoveMap1"
                    ToolTip="Delete" OnCommand="RemoveFile" /> <%--FB 2798--%>
                <asp:Label ID="hdnUploadMap1" Text="" Visible="false" runat="server"></asp:Label>
                <input type="hidden" id="Map1ImageDt" name="Map1ImageDt" runat="server" />
                <asp:Label ID="hdnImageId" Text="" Visible="false" runat="server"></asp:Label>
                <br />
                <asp:RegularExpressionValidator ID="regfleMap1" runat="server" Display="Dynamic" ValidationGroup="submit" ControlToValidate="fleMap1" CssClass="lblError" ErrorMessage="File type is invalid." ValidationExpression="^.*\.((j|J)(p|P)(e|E)?(g|G)|(g|G)(i|I)(f|F)|(p|P)(n|N)(g|G))$"></asp:RegularExpressionValidator>
            </td>
            <%-- //Commented for FB 1633 start  --%>
        </tr>
        <%-- FB 2719 Starts --%>
        <tr id="loginBg" runat="server"><%-- FB 2779 --%>
            <td colspan="3"></td>
            <td align="left" valign="top" style="font-weight: bold; height: 9px" class="blackblodtext" ><%--FB 2977--%>
                Login Background
            </td>
            
            <td valign="top" style="height: 9px;" nowrap="nowrap"><%--FB 2909 Start--%>
             <div>
               <input type="text" class="file_input_textbox" readonly="readonly" value='No file selected' style=" height:24px"/>
                <div class="file_input_div"><input type="button" value="Browse" class="file_input_button" style=" margin-bottom"  />
                  <input type="file"  class="file_input_hidden" accept="image/*" id="fleMap4" contenteditable="false" enableviewstate="true" runat="server" onchange="getfilename(this)"/></div> <%--FB 3055-Filter in Upload Files--%>
                  </div>
                <%--<input type="file" id="fleMap4" contenteditable="false" enableviewstate="true" size="20"
                    class="altText" runat="server" />--%><%--FB 2909 End--%>
                <span class="blackItalictext">(1920 x 1080 pixels)</span><%--(122 x 44)FB 2407--%><%--FB 2719--%>
                <asp:ImageButton ImageUrl="image/btn_delete.gif" runat="server" ID="btnRemoveMap4"
                    ToolTip="Delete" OnCommand="RemoveLoginBackground" /> <%--FB 2798--%>
                <asp:Label ID="hdnUploadMap4" Text="" Visible="false" runat="server"></asp:Label>
                <input type="hidden" id="Map4ImageDt" name="Map4ImageDt" runat="server" />
                <asp:Label ID="hdnImageId3" Text="" Visible="false" runat="server"></asp:Label>
                <br />
                <asp:RegularExpressionValidator ID="regfleMap4" runat="server" Display="Dynamic" ValidationGroup="submit" ControlToValidate="fleMap4" CssClass="lblError" ErrorMessage="File type is invalid." ValidationExpression="^.*\.((j|J)(p|P)(e|E)?(g|G)|(g|G)(i|I)(f|F)|(p|P)(n|N)(g|G))$"></asp:RegularExpressionValidator>
            </td>
        </tr>
        <%-- FB 2719 Ends --%>
        <tr>
            <td align="left" height="21" style="font-weight: bold;display:none" width="2%"><%-- FB 2512--%>
            </td>
            <td valign="top" align="left" style="font-weight: bold;display:none" class="blackblodtext" rowspan="0"
                width="15%"><%-- FB 2512--%>
                Login Page Text
            </td>
            <td style="font-weight: bold;display:none" width="30%"> <%--FB 2512--%>
                <asp:TextBox ID="CompanyInfo" runat="server" rowspan="0" Columns="20" CssClass="altText"
                    Rows="4" TextMode="MultiLine"></asp:TextBox>
            </td>
            <%-- //Commented for FB 1633  
                <td align="right" valign="top" style="font-weight:bold" class="blackblodtext">
                    High Resolution Banner 
                </td>--%>
            <td valign="top" nowrap>
                <input type="file" id="fleMap3" contenteditable="false" enableviewstate="true" size="20"
                    class="altText" runat="server" visible="false" />
                <span class="blackItalictext">
                    <%--(1600 x 72 pixels)--%></span>
                <asp:ImageButton ImageUrl="image/btn_delete.gif" runat="server" ID="btnRemoveMap3"
                    ToolTip="Delete" OnCommand="RemoveHighBanner" Visible="false" /> <%--FB 2798--%>
                <asp:Label ID="hdnUploadMap3" Text="" Visible="false" runat="server"></asp:Label>
                <input type="hidden" id="Map3ImageDt" name="Map1ImageDt" runat="server" />
                <asp:Label ID="hdnImageId2" Text="" Visible="false" runat="server"></asp:Label>
            </td>
            <%-- //Commented for FB 1633 end --%>
        </tr>
        <%--FB 1969 - Start--%> <%--FB 2512 Starts--%>
        <%--<tr>
            <td align="right" style="font-weight: bold">
            </td>
            <td>
            </td>
            <td align="right" valign="top" style="font-weight: bold" class="blackblodtext" style="height: 9px">
                Login History
            </td>
            <td align="left" valign="bottom">
                <input type="button" id="btnuser" runat="server" name="btnview" value="View" class="altLongBlueButtonFormat"
                    onclick="javascript:Openuser();" />
            </td>
        </tr>--%>
        <%--FB 1969 - End--%> <%--FB 2512 Ends--%>
        <tr>
            <td class="subtitleblueblodtext" colspan="5">
                Mail Server Configuration
            </td>
        </tr>
        <tr>
            <td align="left" height="21" style="font-weight: bold" width="2%">
            </td>
            <%--Window Dressing--%>
            <td align="left" height="21" style="font-weight: bold" class="blackblodtext" width="18%">
                System Email
            </td>
            <td style="height: 21px;" width="30%">
                <asp:TextBox ID="txtSystemEmail" runat="server" CssClass="altText" Text="support@myvrm.com">
                </asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ValidationGroup="TestEmail"
                    runat="server" ControlToValidate="txtSystemEmail" ErrorMessage="Required." Display="dynamic"></asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator is="regSystemEmail" runat="server" ControlToValidate="txtSystemEmail"
                    ErrorMessage="Invalid Email Address" Display="dynamic" ValidationExpression="^[a-zA-Z()^][()^\w\.-]*[a-zA-Z0-9()^]@[a-zA-Z0-9][\w\.-]*[a-zA-Z0-9]\.[a-zA-Z][a-zA-Z\.]*[a-zA-Z]$"></asp:RegularExpressionValidator>
                <%--FB Case 699--%>
            </td>
            <%--Window Dressing--%>
            <td align="left" style="font-weight: bold" class="blackblodtext">
                Mail Server Login
            </td>
            <td style="height: 21px;" width="35%">
                <asp:TextBox ID="txtMailServerLogin" runat="server" CssClass="altText"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td align="left">
            </td>
            <%--Window Dressing--%>
            <td align="left" style="font-weight: bold" width="18%" class="blackblodtext">
                Mail Server Password
            </td>
            <td style="height: 37px;" width="30%">
                <asp:TextBox ID="txtMSPassword1" style="background-image:url('../image/pwdbg.png'); background-repeat:no-repeat" onchange="javascript:PreservePassword()" runat="server" 
                    CssClass="altText" TextMode="Password" onblur="PasswordChange(1,1)" onfocus=" fnTextFocus(this.id,1,1)">
               </asp:TextBox><asp:CompareValidator ID="CompareValidator2"
                    runat="server" ControlToValidate="txtMSPassword1" ControlToCompare="txtMSPassword2"
                    ErrorMessage="Passwords do not match." Font-Names="Verdana" Font-Size="X-Small"
                    Font-Bold="False" Display="Dynamic" />
            </td>
            <%--Window Dressing--%>
            <td align="left" style="font-weight: bold" class="blackblodtext">
                Retype Password
            </td>
            <td style="height: 37px;" width="35%">
                <asp:TextBox ID="txtMSPassword2" runat="server" style="background-image:url('../image/pwdbg.png'); background-repeat:no-repeat" onchange="javascript:PreservePassword()" onblur="PasswordChange(1,2)" onfocus=" fnTextFocus(this.id,2,1)"
                    CssClass="altText" TextMode="Password"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td align="left" height="21" style="font-weight: bold">
                <%--FB 1710--%>
                <%--Window Dressing--%>
                <td align="left" height="21" style="font-weight: bold" class="blackblodtext" width="18%">
                    Display Name
                </td>
                <td style="height: 21px;" width="30%">
                    <asp:TextBox ID="txtDisplayName" runat="server" CssClass="altText"></asp:TextBox>
                </td>
                <%--Window Dressing--%><%--FB 1710 Starts--%>
                <td align="left" style="font-weight: bold" class="blackblodtext">
                    Website URL
                </td>
                <td style="height: 21px;" width="35%">
                    <asp:TextBox ID="txtWebsiteURL" runat="server" CssClass="altText">
                    </asp:TextBox><asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server"
                        ControlToValidate="txtWebsiteURL" ErrorMessage="Website URL is required." Font-Names="Verdana"
                        Font-Size="X-Small" Font-Bold="False"><font color="red" size="1pt"> required</font></asp:RequiredFieldValidator>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator6" ControlToValidate="txtWebsiteURL"
                        Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<br>& < > ' + % \ ( ) ; ? | ^ = ! ` , [ ] { } # $ @ ~ and &#34; are invalid characters."
                        ValidationExpression="^(a-z|A-Z|0-9)*[^\\<>+;?|!`,\[\]{}\x22;=^@#$%&()']*$"></asp:RegularExpressionValidator>
                </td>
                <%--FB 1710 ends--%>
        </tr>
        <tr style="height: 35px">
            <%--FB 1710--%>
            <td align="left" height="21" style="font-weight: bold">
                <%--FB 1710--%>
            </td>
            <%--Window Dressing--%>
            <td align="left" height="21" style="font-weight: bold" width="18%" class="blackblodtext">
                Mail Server Address
            </td>
            <td style="height: 21px;" width="30%">
                <asp:TextBox ID="txtServerAddress" runat="server" CssClass="altText" onfocus="SMTPTC()"></asp:TextBox>&nbsp;
                <asp:RequiredFieldValidator ID="reqEmailServerAddress" ValidationGroup="TestEmail"
                    Display="Dynamic" ErrorMessage="Required" runat="server" CssClass="lblError"
                    ControlToValidate="txtServerAddress"></asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator ID="RegularExpressionValidator1" ControlToValidate="txtServerAddress"
                    Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<br>& < > ' + % \ / ( ) ; ? | ^ = ! ` , [ ] { } : # $ @ ~ and &#34; are invalid characters."
                    ValidationExpression="^(a-z|A-Z|0-9)*[^\\/<>+;?|!`,\[\]{}\x22;=^:@#$%&()'~]*$"></asp:RegularExpressionValidator>
                <asp:RegularExpressionValidator ID="regPortP" Enabled="false" SetFocusOnError="true"
                    ControlToValidate="txtServerAddress" ValidationExpression="^([1-9]|[1-9][0-9]|1[0-9][0-9]|2[0-4][0-9]|25[0-5])(\.([0-9]|[1-9][0-9]|1[0-9][0-9]|2[0-4][0-9]|25[0-5])){3}$"
                    ErrorMessage="Invalid IP Address" Display="dynamic" runat="server"></asp:RegularExpressionValidator>
                <%--FB Case 491: Saima --%>
            </td>
            <%--Window Dressing--%>
            <td align="left" height="21" style="font-weight: bold" class="blackblodtext">
                Mail Server Port
            </td>
            <td style="height: 21px;" width="35%">
                <asp:TextBox ID="txtServerPort" runat="server" CssClass="altText" Text="25"></asp:TextBox>
                <span class="blackblodtext">25 is default</span> <%--FB 2552--%>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" ValidationGroup="TestEmail"
                    runat="server" ControlToValidate="txtServerPort" ErrorMessage="Required."></asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator ID="RegularExpressionValidator9" ControlToValidate="txtServerPort"
                    Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<br>Numeric values only."
                    ValidationExpression="[\d]+"></asp:RegularExpressionValidator>
            </td>
        </tr>
        <%--FB 2552 Start--%>
        <tr>
            <td align="left" height="21" style="font-weight: bold" width="2%">
            </td>
            <td align="left" height="21" style="font-weight: bold" width="18%" class="blackblodtext">
               Mail Retry Count
            </td>
            <td align="left">
                <asp:TextBox ID="txtRetryCount" CssClass="altText" runat="server"></asp:TextBox>
                <span class="blackblodtext">(Max 30)</span>
                <asp:RegularExpressionValidator ID="RetryCountValidator" ControlToValidate="txtRetryCount" ValidationGroup="submit"
                    Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<br>Numeric values only."
                    ValidationExpression="[\d]+"></asp:RegularExpressionValidator>
                <asp:RangeValidator ID="retryCountRangeValid" SetFocusOnError="true" Type="Integer" MinimumValue="0" MaximumValue="30"
                            Display="Dynamic" ControlToValidate="txtRetryCount" ValidationGroup="submit" runat="server" ErrorMessage="<br/>Maximum Retry Count is 30."></asp:RangeValidator>
            </td>
        </tr>
        <%--FB 2552 Ends--%>
        <tr>
            <td align="left" height="21" style="font-weight: bold">
            </td>
            <%--Commented for FB 1710 start--%>
            <%--Window Dressing //Add Message Text--%>
            <td align="left" valign="top" style="font-weight: bold" class="blackblodtext" visible="false"
                width="18%">
            </td>
            <td valign="top" width="30%">
                <asp:TextBox ID="txtMailMessage" runat="server" Columns="20" CssClass="altText" Rows="4"
                    TextMode="MultiLine" Width="200px" Wrap="False" Visible="false"></asp:TextBox>
                <%--<asp:RegularExpressionValidator ID="RegularExpressionValidator10" ControlToValidate="txtMailMessage" Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<br>& < > ' + % \ / ( ) ; ? | ^ = ! ` , [ ] { } : # $ @ ~ and &#34; are invalid characters." ValidationExpression="^(a-z|A-Z|0-9)*[^\\/<>+;?|!`,\[\]{}\x22;=^:@#$%&()'~]*$"></asp:RegularExpressionValidator>     Mail Footer      --%>
            </td>
            <%--Commented for FB 1710 End--%>
            <td align="left" height="21">
            </td>
            <td style="height: 21px;" width="35%" class="blackblodtext">
            </td>
        </tr>
        <tr>
            <td align="left" colspan="1" style="font-weight: bold" width="2%">
            </td>
            <td align="center" colspan="4" width="18%">
                <asp:Button ID="btnTestMailConnection" Text="Test Connection" ValidationGroup="TestEmail" Width="160pt" 
                    CssClass="altLongBlueButtonFormat"  runat="server" OnClick="TestMailServerConnection" OnClientClick="javascript:DataLoading('1');" /> <%--ZD 100176 --%>
                <%--<input id="btnTestMailServer" class="altLongBlueButtonFormat" type="button" value="Test Connection" onclick="javascript:TestMailServerConnection();" />--%>
            </td>
        </tr>
        <tr>
            <td align="left" class="subtitleblueblodtext" colspan="5" style="font-weight: bold">
                MS Active Directory or LDAP Directory Configuration
            </td>
        </tr>
        <tr>
            <td align="left" height="21" style="font-weight: bold" width="2%">
            </td>
            <%--Window Dressing--%>
            <td align="left" height="21" style="font-weight: bold" width="18%" class="blackblodtext">
                Server Address
            </td>
            <td style="height: 21px;" width="30%">
                <asp:TextBox ID="txtLDAPServerAddress" runat="server" CssClass="altText" onfocus="LDAPTC()"></asp:TextBox>
                <%--                     <asp:RegularExpressionValidator ID="RegularExpressionValidator1" ControlToValidate="txtLDAPServerAddress" Display="dynamic" runat="server" 
                         ErrorMessage="<br>+'&<>%;:( )& / \ ^#$@ and double quotes are invalid characters for this field." ValidationExpression="[A-Za-z0-9._~?!`* \-]+"></asp:RegularExpressionValidator>                    
--%>
                <asp:RegularExpressionValidator runat="server" ID="regAddress" Enabled="false" ControlToValidate="txtLDAPServerAddress"
                    Display="Dynamic" ValidationExpression="^([1-9]|[1-9][0-9]|1[0-9][0-9]|2[0-4][0-9]|25[0-5])(\.([0-9]|[1-9][0-9]|1[0-9][0-9]|2[0-4][0-9]|25[0-5])){3}$"
                    ErrorMessage="Invalid IP Address"></asp:RegularExpressionValidator>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator7" ValidationGroup="LDAP" ErrorMessage="Required"
                    ControlToValidate="txtLDAPServerAddress" Display="dynamic" runat="server" SetFocusOnError="true"></asp:RequiredFieldValidator>
            </td>
            <%--Window Dressing--%>
            <td align="left" style="font-weight: bold" class="blackblodtext">
                Port Number
            </td>
            <td>
                <asp:TextBox ID="txtLDAPServerPort" runat="server" CssClass="altText" Text="389"></asp:TextBox>
                <span class="blackblodtext">389 is default</span>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="txtLDAPServerPort"
                    ErrorMessage="Required."></asp:RequiredFieldValidator>
            </td>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator6" ValidationGroup="LDAP" ErrorMessage="Required"
                ControlToValidate="txtLDAPServerPort" Display="dynamic" runat="server" SetFocusOnError="true"></asp:RequiredFieldValidator>
        </tr>
        <tr>
            <td align="left" height="21" style="font-weight: bold" width="2%">
            </td>
            <%--Window Dressing--%>
            <td align="left" height="21" style="font-weight: bold" width="18%" class="blackblodtext">
                Account Login
            </td>
            <td style="height: 21px;" width="30%">
                <asp:TextBox ID="txtLDAPAccountLogin" runat="server" CssClass="altText"></asp:TextBox>
                <asp:RegularExpressionValidator ID="RegularExpressionValidator2" ControlToValidate="txtLDAPAccountLogin"
                    Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<br>& < > ' + % \ / ( ) ; ? | ^ = ! ` , [ ] { } : # $ @ ~ and &#34; are invalid characters."
                    ValidationExpression="^(a-z|A-Z|0-9)*[^\\/<>+;?|!`,\[\]{}\x22;=^:@#$%&()']*$"></asp:RegularExpressionValidator>
                <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator5" ValidationGroup="LDAP" ErrorMessage="Required" ControlToValidate="txtLDAPAccountLogin" Display="dynamic" runat="server" SetFocusOnError="true"></asp:RequiredFieldValidator>--%><%--PSU--%>
            </td>
            <%--Window Dressing--%>
            <td align="left" style="font-weight: bold" class="blackblodtext">
                Connection Timeout
            </td>
            <td style="height: 21px;" width="35%">
                <asp:DropDownList ID="lstLDAPConnectionTimeout" runat="server" CssClass="altSelectFormat">
                    <asp:ListItem Value="10">10 Seconds</asp:ListItem>
                    <asp:ListItem Value="20">20 Seconds</asp:ListItem>
                    <asp:ListItem Value="30">30 Seconds</asp:ListItem>
                    <asp:ListItem Value="60">60 Seconds</asp:ListItem>
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td align="left" height="21" style="font-weight: bold" width="2%">
            </td>
            <%--Window Dressing--%>
            <td align="left" height="21" style="font-weight: bold" width="18%" class="blackblodtext">
                Account Password
            </td>
            <td style="height: 21px;" width="30%">
                <asp:TextBox ID="txtLDAPAccountPassword1" style="background-image:url('../image/pwdbg.png'); background-repeat:no-repeat" onchange="javascript:PreservePassword();" onblur="PasswordChange(2,1)" onfocus=" fnTextFocus(this.id,1,2)"
                    runat="server" CssClass="altText" TextMode="Password"></asp:TextBox>
                <asp:RegularExpressionValidator ID="RegularExpressionValidator3" Enabled="false"
                    ControlToValidate="txtLDAPAccountPassword1" Display="dynamic" runat="server"
                    SetFocusOnError="true" ErrorMessage="<br>& < > ' + % \ / ( ) ; ? | ^ = ! ` , [ ] { } : # $ @ ~ and &#34; are invalid characters."
                    ValidationExpression="^(a-z|A-Z|0-9)*[^\\/<>+;?|!`,\[\]{}\x22;=^:@#$%&()']*$"></asp:RegularExpressionValidator>
                <asp:CompareValidator ID="CompareValidator3" Display="dynamic" runat="server" ControlToValidate="txtLDAPAccountPassword1"
                    ControlToCompare="txtLDAPAccountPassword2" ErrorMessage="Passwords do not match."
                    Font-Names="Verdana" Font-Size="X-Small" Font-Bold="False" />
                <%--<asp:RequiredFieldValidator ID="reqLDAPPass" ValidationGroup="LDAP" ErrorMessage="Required" ControlToValidate="txtLDAPAccountPassword1" Display="dynamic" runat="server" SetFocusOnError="true"></asp:RequiredFieldValidator>--%><%--PSU--%>
            </td>
            <%--Window Dressing--%>
            <td align="left" style="font-weight: bold" class="blackblodtext">
                Retype Password
            </td>
            <td style="height: 21px;" width="35%">
                <asp:TextBox ID="txtLDAPAccountPassword2" style="background-image:url('../image/pwdbg.png'); background-repeat:no-repeat" onchange="javascript:PreservePassword();" onblur="PasswordChange(2,2)" onfocus=" fnTextFocus(this.id,2,2)"
                    runat="server" CssClass="altText" TextMode="Password"></asp:TextBox>
                <asp:RegularExpressionValidator ID="RegularExpressionValidator4" Enabled="false"
                    ControlToValidate="txtLDAPAccountPassword2" Display="dynamic" runat="server"
                    SetFocusOnError="true" ErrorMessage="<br>& < > ' + % \ / ( ) ; ? | ^ = ! ` , [ ] { } : # $ @ ~ and &#34; are invalid characters."
                    ValidationExpression="^(a-z|A-Z|0-9)*[^\\/<>+;?|!`,\[\]{}\x22;=^:@#$%&()']*$"></asp:RegularExpressionValidator>
                <asp:CompareValidator ID="CompareValidator4" runat="server" ControlToValidate="txtLDAPAccountPassword2"
                    ControlToCompare="txtLDAPAccountPassword1" ErrorMessage="Passwords do not match."
                    Font-Names="Verdana" Font-Size="X-Small" Font-Bold="False" />
            </td>
        </tr>
        <tr>
            <td align="left" height="21" style="font-weight: bold" width="2%">
            </td>
            <%--Window Dressing--%>
            <td align="left" height="21" style="font-weight: bold" width="18%" class="blackblodtext">
                Login Key
            </td>
            <td style="height: 21px;" width="30%">
                <asp:TextBox ID="txtLDAPLoginKey" runat="server" CssClass="altText"></asp:TextBox>
                <asp:RegularExpressionValidator ID="RegularExpressionValidator5" ControlToValidate="txtLDAPLoginKey"
                    Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<br>& < > ' + % \ / ( ) ; ? | ^ = ! ` , [ ] { } : # $ @ ~ and &#34; are invalid characters."
                    ValidationExpression="^(a-z|A-Z|0-9)*[^\\/<>+;?|!`,\[\]{}\x22;=^:@#$%&()']*$"></asp:RegularExpressionValidator>
            </td>
            <%--Window Dressing--%>
            <td align="left" height="21" style="font-weight: bold" width="18%" class="blackblodtext"
                valign="top">
                Search Filter
            </td>
            <!--Added Regular expression by Vivek to perform junk character validation as a fix for issue number 315 -->
            <td style="height: 21px;" width="30%">
                <asp:TextBox ID="txtLDAPSearchFilter" runat="server" CssClass="altText"></asp:TextBox>
                &nbsp;
                <br />
                <span style="color: #666666;">ex: cn=users,dc=domain,dc=local</span>
                <%--FB 2096--%>
                <asp:RegularExpressionValidator ID="regLDAPSearchFilter" ControlToValidate="txtLDAPSearchFilter"
                    Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<br>' + % \ / ; ? | ^ ` [ ] { } : # $ @ ~ and &#34; are invalid characters."
                    ValidationExpression="^(a-z|A-Z|0-9)*[^\\/+;?|`\[\]{}\x22;^:@#$%~']*$"></asp:RegularExpressionValidator>
                <%--FB Case 491: Saima --%>
            </td>
        </tr>
        <tr>
            <td align="left" height="21" style="font-weight: bold" width="2%">
            </td>
            <%--Window Dressing--%>
            <td align="left" height="21" style="font-weight: bold" width="18%" class="blackblodtext">
                Domain Prefix
            </td>
            <td align="left">
                <asp:TextBox ID="txtLDAPPrefix" CssClass="altText" runat="server"></asp:TextBox>
                <asp:RegularExpressionValidator ID="regConfName" ControlToValidate="txtLDAPPrefix"
                    Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<br>& < > ' + % ( ) ; ? | ^ = ! ` , [ ] { } : # $ @ ~ and &#34; are invalid characters."
                    ValidationExpression="^(a-z|A-Z|0-9)*[^<>+;?|!`,\[\]{}\x22;=^:@#$%&()~']*$"></asp:RegularExpressionValidator>
            </td>
            <%--Window Dressing--%>
            <%--FB 2462 START--%>
            <td align="left" height="21" style="font-weight: bold" width="18%" class="blackblodtext"
                valign="top">
                Synchronization Time
            </td>
            <td align="left">
                <mbcbb:ComboBox ID="lstLDAPScheduleTime" CssClass="altText" CausesValidation="true"
                    runat="server" Enabled="true" Rows="15">
                    <asp:ListItem Value="01:00 AM" Selected="true"></asp:ListItem>
                    <asp:ListItem Value="02:00 AM"></asp:ListItem>
                    <asp:ListItem Value="03:00 AM"></asp:ListItem>
                    <asp:ListItem Value="04:00 AM"></asp:ListItem>
                    <asp:ListItem Value="05:00 AM"></asp:ListItem>
                    <asp:ListItem Value="06:00 AM"></asp:ListItem>
                    <asp:ListItem Value="07:00 AM"></asp:ListItem>
                    <asp:ListItem Value="08:00 AM"></asp:ListItem>
                    <asp:ListItem Value="09:00 AM"></asp:ListItem>
                    <asp:ListItem Value="10:00 AM"></asp:ListItem>
                    <asp:ListItem Value="11:00 AM"></asp:ListItem>
                    <asp:ListItem Value="12:00 PM"></asp:ListItem>
                    <asp:ListItem Value="01:00 PM"></asp:ListItem>
                    <asp:ListItem Value="02:00 PM"></asp:ListItem>
                    <asp:ListItem Value="03:00 PM"></asp:ListItem>
                    <asp:ListItem Value="04:00 PM"></asp:ListItem>
                    <asp:ListItem Value="05:00 PM"></asp:ListItem>
                    <asp:ListItem Value="06:00 PM"></asp:ListItem>
                    <asp:ListItem Value="07:00 PM"></asp:ListItem>
                    <asp:ListItem Value="08:00 PM"></asp:ListItem>
                    <asp:ListItem Value="09:00 PM"></asp:ListItem>
                    <asp:ListItem Value="10:00 PM"></asp:ListItem>
                    <asp:ListItem Value="11:00 PM"></asp:ListItem>
                    <asp:ListItem Value="12:00 AM" Selected="true"></asp:ListItem>
                </mbcbb:ComboBox>
                <asp:RequiredFieldValidator ID="reqlstLDAPScheduleTime" runat="server" ControlToValidate="lstLDAPScheduleTime" ValidationGroup="submit"
                                                                Display="Dynamic" ErrorMessage="Time is Required"></asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator ID="reglstLDAPScheduleTime" runat="server" ControlToValidate="lstLDAPScheduleTime" ValidationGroup="submit"
                                         Display="Dynamic" ErrorMessage="Invalid Time (HH:MM AM/PM)" ValidationExpression="[0-1][0-9]:[0-5][0-9] ?[A|a|P|p][M|m]"></asp:RegularExpressionValidator>
            </td>
            <%--FB 2462 END--%>
        </tr>
        <tr style="display:none"> <%--FB 2993 LDAP--%>
            <td align="left" height="21" style="font-weight: bold" width="2%">
            </td>
            <%--Window Dressing--%>
            <td align="left" height="21" width="18%" class="blackblodtext" style="display: none">
                Synchronization Days
            </td>
            <td align="left" colspan="4" style="font-weight: normal; color: Blue">
                <%--Window Dressing--%>
                <asp:CheckBoxList ID="chkLstDays" runat="server" RepeatColumns="9" RepeatLayout="Flow"
                    Textalign="left" Style="display: none">
                    <asp:ListItem Text="Monday" Value="1"><font class='blackblodtext'>Monday</font></asp:ListItem>
                    <asp:ListItem Text="Tuesday" Value="2"><font class='blackblodtext'>Tuesday</font></asp:ListItem>
                    <asp:ListItem Text="Wednesday" Value="3"><font class='blackblodtext'>Wednesday</font></asp:ListItem>
                    <asp:ListItem Text="Thursday" Value="4"><font class='blackblodtext'>Thursday</font></asp:ListItem>
                    <asp:ListItem Text="Friday" Value="5"><font class='blackblodtext'>Friday</font></asp:ListItem>
                    <asp:ListItem Text="Saturday" Value="6"><font class='blackblodtext'>Saturday</font></asp:ListItem>
                    <asp:ListItem Text="Sunday" Value="7"><font class='blackblodtext'>Sunday</font></asp:ListItem>
                </asp:CheckBoxList>
            </td>
        </tr>
        <%--FB 2993 LDAP START--%>
        <tr>
            <td align="left" height="21" style="font-weight: bold" width="2%">
            </td>
            <%--Window Dressing--%>
            <td align="left" height="21" width="18%" class="blackblodtext" >
                Authentication Type
            </td>
            <td align="left" colspan="4" style="font-weight: normal; color: Blue">
                <%--Window Dressing--%>
                <asp:DropDownList ID="drpAuthType" runat="server" Width="150px" RepeatColumns="9" RepeatLayout="Flow" Textalign="left">
                    <asp:ListItem Text="None" Value="0"></asp:ListItem>
                    <asp:ListItem Text="Signing" Value="1"></asp:ListItem>
                    <asp:ListItem Text="Sealing" Value="2"></asp:ListItem>
                    <asp:ListItem Text="Secure" Value="3"></asp:ListItem>
                    <asp:ListItem Text="Anonymous" Value="4"></asp:ListItem>
                    <asp:ListItem Text="Delegation" Value="5"></asp:ListItem>
                    <asp:ListItem Text="Encryption" Value="6"></asp:ListItem>
                    <asp:ListItem Text="FastBind" Value="7"></asp:ListItem>
                    <asp:ListItem Text="ReadonlyServer" Value="8"></asp:ListItem>
                    <asp:ListItem Text="SecureSocketsLayer" Value="9"></asp:ListItem>
                    <asp:ListItem Text="ServerBind" Value="10"></asp:ListItem>
                </asp:DropDownList>
            </td>
        </tr>
        <%--FB 2993 LDAP END--%>
        <tr>
            <td align="left" colspan="1" style="font-weight: bold" width="2%">
            </td>
            <td align="left" colspan="4" style="font-weight: bold; font-size: small; color: black;
                font-family: verdana; height: 21px; text-align: center;" >
                <asp:Button runat="server" ID="btnTestLDAPConnection" ValidationGroup="LDAP" Text="Test Connection" Width="160pt" 
                    CssClass="altLongBlueButtonFormat" OnClick="TestLDAPConnection" OnClientClick="javascript:DataLoading('1');" /> <%--ZD 100176 --%>
                <%--               			<input type="button" name="btnTestLDAPServer" value="Test Connection" class="altLongBlueButtonFormat" onclick="javascript:TestLDAPServerConnection();">
--%>
                <asp:Button runat="server" ID="btnSyncLdapNow" Text="Sync Now" CssClass="altLongBlueButtonFormat"
                    OnClick="SyncNow" Style="display: none;" />
            </td>
        </tr>
        <tr>
            <td align="left" height="21" style="font-weight: bold" width="2%">
            </td>
            <td align="left" height="21" style="font-weight: bold" width="18%">
            </td>
            <td style="height: 21px; text-align: center;" width="30%">
            </td>
            <td align="left" style="font-weight: bold; font-size: small; width: 15%; color: black;
                font-family: verdana; height: 21px; text-align: center">
            </td>
            <td style="height: 21; font-weight: bold" width="35%">
            </td>
        </tr>
        <tr>
            <td colspan="5" align="left">
                <table width="100%">
                    <tr>
                        <td width="96%" height="20" align="left">
                            <span class="subtitleblueblodtext">Server Options</span>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td style="width: 3%">
            </td>
            <td align="left" height="21" style="font-weight: bold; width: 18%" class="blackblodtext">
                Server Time Zone
            </td>
            <td align="left" style="width: 15%">
                <asp:DropDownList ID="PreferTimezone" runat="server" CssClass="altLong0SelectFormat"
                    DataTextField="timezoneName" OnInit="UpdateTimezones" DataValueField="timezoneID">
                </asp:DropDownList>
                <asp:RequiredFieldValidator ID="TimeZoneValidator" runat="server" ControlToValidate="PreferTimezone"
                    ErrorMessage="Required" Font-Bold="True" InitialValue="-1" Display="Dynamic"></asp:RequiredFieldValidator>
            </td>
            <%--FB 2007--%>
            <td align="left" height="21" style="font-weight: bold" class="blackblodtext">
                Launch Buffer (in minutes)
            </td>
            <td style="height: 21px;">
                <asp:TextBox ID="txtLaunchBuffer" runat="server" CssClass="altText"></asp:TextBox>
                <asp:RegularExpressionValidator ID="RegularExpressionValidator7" ControlToValidate="TxtLaunchBuffer"
                    Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<br>Please Enter Numbers only."
                    ValidationExpression="^[0-9]*$"></asp:RegularExpressionValidator>
            </td>
        </tr>
        <tr> <%--FB 2437--%> 
        <%--FB 2501 starts--%>
         <td style="width: 3%">
            </td>
         <td align="left" height="21" style="font-weight: bold; width: 18%" class="blackblodtext">
                 Start Mode
         </td>
         <td align="left" style="width: 15%">
                <asp:DropDownList ID="lstStartMode" runat="server" Width="30%" CssClass="alt2SelectFormat">
             <asp:ListItem Value="0" Selected="True" Text="Automatic"></asp:ListItem>
             <asp:ListItem Value="1" Text="Manual"></asp:ListItem>
           </asp:DropDownList>  
         </td>
         <td align="left" height="21" style="font-weight: bold" class="blackblodtext">
            <asp:Label ID="Label8" runat="server" Text="Enable Launch Buffer for P2P"></asp:Label>
         </td>
        <td style="height: 21px;">
           <asp:DropDownList ID="lstEnableLaunchBufferP2P" runat="server" Width="20%" CssClass="alt2SelectFormat">
             <asp:ListItem Value="0" Selected="True" Text="No"></asp:ListItem>
             <asp:ListItem Value="1" Text="Yes"></asp:ListItem>
           </asp:DropDownList>
        </td>
        <%--FB 2501 ends--%>
        </tr>
        <%--FB 2670 Start--%>
        <tr>
          <td style="width: 3%">
          </td>
          <td align ="left" style="font-weight:bold;width: 18%" class="blackblodtext" >
                 Conference Support <%--FB 3023--%>
          </td>
          <td align="left" style="width:15%">
              <asp:DropDownList ID="lstConciergeSupport" runat="server" Width="30%" CssClass="alt2SelectFormat">
                <asp:ListItem Value="1"  Text="Yes"></asp:ListItem>
                <asp:ListItem Value="2"  Text="No"></asp:ListItem>
              </asp:DropDownList>
          </td>
		 <%--FB 2858 Start--%>
          <td align ="left" style="font-weight:bold;width: 18%" class="blackblodtext" >
                 Display Public Conference on Login Screen
          </td>
          <td align="left" style="width:15%">
              <asp:DropDownList ID="lstViewPublic" runat="server" Width="20%" CssClass="alt2SelectFormat">
                <asp:ListItem Value="1"  Text="Yes"></asp:ListItem>
                <asp:ListItem Value="2"  Text="No" Selected="True"></asp:ListItem>
              </asp:DropDownList>
          </td>
         <%--FB 2858 End--%>
        </tr>
        <%--FB 2670 End--%>
		<%--FB 2659 Starts  --%>
        <tr>
            <td style="width: 3%" nowrap="nowrap">
            </td>
        
            <td align="left" height="21" style="font-weight: bold; width: 18%" class="blackblodtext">
                <b>Cloud Installation</b>
            </td>
            <td>
                <asp:DropDownList ID="drpCloudInstallation" runat="server" CssClass="alt2SelectFormat" Width="30%" >
                    <asp:ListItem Value="0">No</asp:ListItem>
                    <asp:ListItem Value="1">Yes</asp:ListItem>
                </asp:DropDownList>
            </td>
            <td id="tdLicense" runat="server" align="left" height="21" style="font-weight: bold" class="blackblodtext">
                Set Defaults for License
            </td>
            <td id="tdBtnLicense" style="height: 21px;" runat="server">
                <input type="button" id="btnDefaultClick" class="altShortBlueButtonFormat" style="width:130px" runat="server" value="Default License" />
            </td>
        </tr>
        <%--FB 2659 ends--%>        
        <%--FB 2501 EM7  Start--%>
        <%-- FB 2598 EnableEM7 Starts tr-id --%>
        <tr id="trEM7Connectivity" runat="server">
            <td colspan="5" align="left">
                <table width="100%">
                    <tr>
                        <td width="96%" height="20" align="left">
                            <span class="subtitleblueblodtext">EM7 Connectivity</span>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
         <tr id="trEM7URIUsrName" runat="server">
            <td align="left" height="21" style="font-weight: bold" width="2%">
            </td>
            <td align="left" height="21" style="font-weight: bold" class="blackblodtext" width="18%">
                EM7 URI
            </td>
            <td style="height: 21px;" width="30%">
                <asp:TextBox ID="txtEM7URI" runat="server" CssClass="altText" MaxLength="256"></asp:TextBox>                                
                <asp:RegularExpressionValidator ID="RegularExpressionValidator18" ControlToValidate="txtEM7URI" ValidationGroup="submit"
                    Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<br>& < > ' + % \  ( ) ; ? | ^ = ! ` , [ ] { }  # $ @ ~ and  &#34; are invalid characters."
                    ValidationExpression="^(a-z|A-Z|0-9)*[^\\/<>+;?|!`,\[\]{}\x22;=^:@#$%&()']*$"></asp:RegularExpressionValidator>
                
            </td>
           <td align="left" style="font-weight: bold" class="blackblodtext">
                EM7 UserName
            </td>
            <td style="height: 21px;" width="35%">
                <asp:TextBox ID="txtEM7Username" runat="server" CssClass="altText" MaxLength="50"></asp:TextBox>
                <asp:RegularExpressionValidator ID="RegularExpressionValidator17" ControlToValidate="txtEM7Username" ValidationGroup="submit"
                    Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<br>& < > ' + % \ / ( ) ; ? | ^ = ! ` , [ ] { } : # $ @ ~  and &#34; are invalid characters."
                    ValidationExpression="^(a-z|A-Z|0-9)*[^\\/<>+;?|!`,\[\]{}\x22;=^:@#$%&()']*$"></asp:RegularExpressionValidator>
                
            </td>
        </tr>
        <tr id="trEM7Pwd" runat="server">
            <td align="left">
            </td>
            <td align="left" style="font-weight: bold" width="18%" class="blackblodtext">
                EM7 Password
            </td>
            <td style="height: 37px;" width="30%" nowrap="nowrap">
                <asp:TextBox ID="txtEM7Password" runat="server" CssClass="altText" TextMode="Password" style="background-image:url('../image/pwdbg.png'); background-repeat:no-repeat" onchange="javascript:PreservePassword()" MaxLength="20" onblur="PasswordChange(3,1)" onfocus=" fnTextFocus(this.id,1,3)"></asp:TextBox>   
                <asp:CompareValidator ID="cmpValPassword1" runat="server" ValidationGroup="submit" ControlToCompare="txtEM7ConformPassword"
                    ControlToValidate="txtEM7Password" Display="Dynamic" ErrorMessage="<br>Re-enter password."></asp:CompareValidator>
                <asp:RegularExpressionValidator ID="RegularExpressionValidator12" Enabled="true" ValidationGroup="submit"
                    ControlToValidate="txtEM7Password" Display="dynamic" runat="server" SetFocusOnError="true"
                    ErrorMessage="<br>& < > ' + % \ / ( ) ; ? | ^ = ! ` , [ ] { } : # $ @ ~ <br> and &#34; are invalid characters."
                    ValidationExpression="^(a-z|A-Z|0-9)*[^\\/<>+;?|!`,\[\]{}\x22;=^:@#$%&()']*$"></asp:RegularExpressionValidator>             
            </td>  
             <td align="left" style="font-weight: bold" width="18%" class="blackblodtext">
                EM7 Confirm Password
            </td>
            <td style="height: 37px;" width="30%">
                <asp:TextBox ID="txtEM7ConformPassword" runat="server" CssClass="altText" style="background-image:url('../image/pwdbg.png'); background-repeat:no-repeat" onchange="javascript:PreservePassword()" TextMode="Password" MaxLength="20" onblur="PasswordChange(3,2)" onfocus=" fnTextFocus(this.id,2,3)"></asp:TextBox> 
                <asp:CompareValidator ID="CompareValidator6" runat="server" ValidationGroup="submit"
                    ControlToValidate="txtEM7ConformPassword" ControlToCompare="txtEM7Password" ErrorMessage="Passwords do not match."
                     Font-Bold="False" />   
                <asp:RegularExpressionValidator ID="RegularExpressionValidator13" Enabled="true" ValidationGroup="submit"
                    ControlToValidate="txtEM7ConformPassword" Display="dynamic" runat="server" SetFocusOnError="true"
                    ErrorMessage="<br>& < > ' + % \ / ( ) ; ? | ^ = ! ` , [ ] { } : # $ @ ~ <br> and &#34; are invalid characters."
                    ValidationExpression="^(a-z|A-Z|0-9)*[^\\/<>+;?|!`,\[\]{}\x22;=^:@#$%&()']*$"></asp:RegularExpressionValidator> 
             </td> 
             
         </tr>
         <tr id="trEM7Port" runat="server">
          <td align="left" height="21" style="font-weight: bold" width="2%">
            </td>
            <td align="left" height="21" style="font-weight: bold" class="blackblodtext" width="18%">
                EM7 Port
            </td> 
          <td style="height: 21px;" width="35%">
                <asp:TextBox ID="txtEM7Port" runat="server" CssClass="altText" MaxLength="4" ></asp:TextBox>
                <asp:RegularExpressionValidator ID="RegularExpressionValidator19" ControlToValidate="txtEM7Port"
                    Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<br>Numeric values only."
                    ValidationExpression="[\d]+"></asp:RegularExpressionValidator>
          </td>
         </tr>
         <%-- FB 2598 EnableEM7 tr-id Ends--%>
         <%--FB 2501 EM7 End--%>
                  
        <%-- ZD 100256 Start --%>    
        <tr id="trsyncsettings" runat="server">
            <td colspan="5" align="left">
                <table width="100%">
                    <tr>
                        <td width="96%" height="20" align="left">
                            <span class="subtitleblueblodtext">Sync Connectivity Settings</span>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr id="trsyncAddress" runat="server">
            <td align="left" height="21" style="font-weight: bold" width="2%">
            </td>
            <td align="left" height="21" style="font-weight: bold" class="blackblodtext" width="18%">
                Address
            </td>
            <td style="height: 21px;" width="30%">
                <asp:TextBox ID="txtsyncaddress" runat="server" CssClass="altText" MaxLength="256"></asp:TextBox>  
                <asp:RegularExpressionValidator ID="RegularExpressionValidator20" ControlToValidate="txtsyncaddress"
                    Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<br>& < > ' + % \ / ( ) ; ? | ^ = ! ` , [ ] { } : # $ @ ~ and &#34; are invalid characters."
                    ValidationExpression="^(a-z|A-Z|0-9)*[^\\/<>+;?|!`,\[\]{}\x22;=^:@#$%&()'~]*$"></asp:RegularExpressionValidator>
                <asp:RegularExpressionValidator ID="RegularExpressionValidator21" Enabled="false" SetFocusOnError="true"
                    ControlToValidate="txtsyncaddress" ValidationExpression="^([1-9]|[1-9][0-9]|1[0-9][0-9]|2[0-4][0-9]|25[0-5])(\.([0-9]|[1-9][0-9]|1[0-9][0-9]|2[0-4][0-9]|25[0-5])){3}$"
                    ErrorMessage="Invalid IP Address" Display="dynamic" runat="server"></asp:RegularExpressionValidator>                                
            </td>
           <td align="left" style="font-weight: bold" class="blackblodtext">
                Configuration
            </td>
            <td style="height: 21px;" width="35%">
                 <asp:DropDownList ID="ddlsyncconfiguration" runat="server" CssClass="altSelectFormat" Width="25%">
                    <asp:ListItem Value="1">Master</asp:ListItem>
                    <asp:ListItem Value="2">Slave</asp:ListItem>
                </asp:DropDownList>
            </td>
        </tr>
        <tr id="trsyncport" runat="server">
          <td align="left" height="21" style="font-weight: bold" width="2%">
            </td>
            <td align="left" height="21" style="font-weight: bold" class="blackblodtext" width="18%">
                Port
            </td> 
          <td style="height: 21px;" width="35%">
                <asp:TextBox ID="txtsyncPort" runat="server" CssClass="altText" MaxLength="4" ></asp:TextBox>
                <asp:RegularExpressionValidator ID="RegularExpressionValidator24" ControlToValidate="txtsyncPort"
                    Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<br>Numeric values only."
                    ValidationExpression="[\d]+"></asp:RegularExpressionValidator>
          </td>
           <td align="left" style="font-weight: bold" width="18%" class="blackblodtext">
                Email Address<br />(Semicolon separated)
            </td>
            <td style="height: 37px;" width="30%">
                <asp:TextBox ID="txtsyncemailaddress" runat="server" CssClass="altText" TextMode="MultiLine" Width="200px" ></asp:TextBox> 
                 <asp:RegularExpressionValidator ID="regMultipleAssistant" ControlToValidate="txtsyncemailaddress"
                    Display="dynamic" runat="server" ValidationGroup="Submit" SetFocusOnError="true"
                    ErrorMessage="<br>& < > ' + % \ / ( ) ; ? | ^ = ! ` , [ ] { } : # $ ~ and &#34 are invalid characters."
                    ValidationExpression="^(a-z|A-Z|0-9)*[^\\/<>^+?|!`,\[\]{}\x22=:#$%&()'~]*$"></asp:RegularExpressionValidator>
             </td> 
         </tr>
        <%-- ZD 100256 End --%>          
                  
        <%-- FB 2363 - Start--%>
        <%if((Application["External"].ToString() != "")){%>
        <tr>
            <td colspan="5" align="left">
                <span class="subtitleblueblodtext">External Scheduling Settings</span>
            </td>
        </tr>
        <tr>
            <td style="width: 3%">
            </td>
            <td class="blackblodtext" align="left">
                Partner Name
            </td>
            <td>
                <asp:TextBox ID="txtPartnerName" runat="server" CssClass="altText" Width="200px"></asp:TextBox>
                <asp:RegularExpressionValidator ID="RegularExpressionValidator14" ControlToValidate="txtPartnerName"
                    ValidationGroup="submit" Display="dynamic" runat="server" SetFocusOnError="true"
                    ErrorMessage="<br>& < > ' + % \ / ( ) ; ? | ^ = ! ` , [ ] { } : # $ @ ~ and &#34; are invalid characters."
                    ValidationExpression="^(a-z|A-Z|0-9)*[^\\/<>+;?|!`,\[\]{}\x22;=^:@#$%&()']*$"></asp:RegularExpressionValidator>
            </td>
            <td class="blackblodtext" align="left">
                Partner Email
            </td>
            <td>
                <asp:TextBox ID="txtPartnerEmail" runat="server" CssClass="altText" Width="200px"></asp:TextBox>
                <asp:RegularExpressionValidator ID="RegularExpressionValidator15" is="regSystemEmail"
                    runat="server" ControlToValidate="txtPartnerEmail" ValidationGroup="submit" ErrorMessage="Invalid Email Address"
                    Display="dynamic" ValidationExpression="^[a-zA-Z()^][()^\w\.-]*[a-zA-Z0-9()^]@[a-zA-Z0-9][\w\.-]*[a-zA-Z0-9]\.[a-zA-Z][a-zA-Z\.]*[a-zA-Z]$"></asp:RegularExpressionValidator>
                <%--FB Case 699--%>
            </td>
        </tr>
        <tr>
            <td style="width: 3%">
            </td>
            <td class="blackblodtext" align="left">
                Partner URL
            </td>
            <td>
                <asp:TextBox ID="txtPartnerURL" runat="server" CssClass="altText" TextMode="MultiLine"
                    Width="200px"></asp:TextBox>
                <asp:RegularExpressionValidator ID="RegularExpressionValidator11" ControlToValidate="txtPartnerURL"
                    ValidationGroup="submit" Display="dynamic" runat="server" SetFocusOnError="true"
                    ErrorMessage="<br>& < > ' + % \ ( ) ; ? | ^ = ! ` , [ ] { } # $ @ ~ and &#34; are invalid characters."
                    ValidationExpression="^(a-z|A-Z|0-9)*[^\\<>+;?|!`,\[\]{}\x22;=^@#$%&()']*$"></asp:RegularExpressionValidator>
            </td>
            <td class="blackblodtext" align="left">
                User Name
            </td>
            <td>
                <asp:TextBox ID="txtPUserName" runat="server" CssClass="altText" Rows="3" Width="200px"></asp:TextBox>
                <asp:RegularExpressionValidator ID="RegularExpressionValidator16" ControlToValidate="txtPUserName"
                    ValidationGroup="submit" Display="dynamic" runat="server" SetFocusOnError="true"
                    ErrorMessage="<br>& < > ' + % \ / ( ) ; ? | ^ = ! ` , [ ] { } : # $ @ ~ and &#34; are invalid characters."
                    ValidationExpression="^(a-z|A-Z|0-9)*[^\\/<>+;?|!`,\[\]{}\x22;=^:@#$%&()']*$"></asp:RegularExpressionValidator>
            </td>
        </tr>
        <tr>
            <td style="width: 3%">
            </td>
            <td class="blackblodtext" align="left">
                Password
            </td>
            <td style="height: 21px;">
                <asp:TextBox ID="txtP1Password" style="background-image:url('../image/pwdbg.png'); background-repeat:no-repeat" onchange="javascript:PreservePassword();" runat="server" onblur="PasswordChange(4,1)" onfocus=" fnTextFocus(this.id,1,4)"
                    CssClass="altText" TextMode="Password"></asp:TextBox>
                <asp:RegularExpressionValidator ID="RegularExpressionValidator8" Enabled="false"
                    ControlToValidate="txtP1Password" Display="dynamic" runat="server" SetFocusOnError="true"
                    ErrorMessage="<br>& < > ' + % \ / ( ) ; ? | ^ = ! ` , [ ] { } : # $ @ ~ and &#34; are invalid characters."
                    ValidationExpression="^(a-z|A-Z|0-9)*[^\\/<>+;?|!`,\[\]{}\x22;=^:@#$%&()']*$"></asp:RegularExpressionValidator>
                <asp:CompareValidator ID="CompareValidator1" Display="dynamic" ValidationGroup="submit"
                    runat="server" ControlToValidate="txtP1Password" ControlToCompare="txtP2Password"
                    ErrorMessage="Passwords do not match." Font-Names="Verdana" Font-Size="X-Small"
                    Font-Bold="False" />
            </td>
            <td align="left" style="font-weight: bold" class="blackblodtext">
                Retype Password
            </td>
            <td style="height: 21px;">
                <asp:TextBox ID="txtP2Password" style="background-image:url('../image/pwdbg.png'); background-repeat:no-repeat" onchange="javascript:PreservePassword();" runat="server" onblur="PasswordChange(4,2)" onfocus=" fnTextFocus(this.id,2,4)"
                    CssClass="altText" TextMode="Password"></asp:TextBox>
                <asp:RegularExpressionValidator ID="RegularExpressionValidator10" Enabled="false"
                    ControlToValidate="txtP2Password" Display="dynamic" runat="server" SetFocusOnError="true"
                    ErrorMessage="<br>& < > ' + % \ / ( ) ; ? | ^ = ! ` , [ ] { } : # $ @ ~ and &#34; are invalid characters."
                    ValidationExpression="^(a-z|A-Z|0-9)*[^\\/<>+;?|!`,\[\]{}\x22;=^:@#$%&()']*$"></asp:RegularExpressionValidator>
                <asp:CompareValidator ID="CompareValidator5" runat="server" ValidationGroup="submit"
                    ControlToValidate="txtP2Password" ControlToCompare="txtP1Password" ErrorMessage="Passwords do not match."
                    Font-Names="Verdana" Font-Size="X-Small" Font-Bold="False" />
            </td>
        </tr>
        <tr>
            <td style="width: 3%"> <%--FB 2363--%>
            </td>
            <td class="blackblodtext" align="left">
                Timeout Value
            </td>
            <td style="height: 21px;">
                <asp:TextBox ID="txtTimeoutValue" runat="server" CssClass="altText"></asp:TextBox>
                <asp:RegularExpressionValidator ID="regTimeoutVlaue" runat="server" ControlToValidate="txtTimeoutValue" ValidationGroup="submit" Display="Dynamic" ErrorMessage="Numeric values only." ValidationExpression="[\d]+" ></asp:RegularExpressionValidator>
            </td>
        </tr>
        <tr>
            <td colspan="2" align="left" style="width: 20%">
                <span class="subtitleblueblodtext">Event Queue</span>
            </td>
            <td >
                <input type="button" id="btnEvent" runat="server" name="btnOrganization" value="Manage Event"
                    class="altMedium0BlueButtonFormat"  onclick="javascript:fnView();">
            </td>
            <td colspan="2" ></td>
        </tr>        
        <tr>
            <td colspan="5" align="left">
                <span class="subtitleblueblodtext">Mailing User Report Settings</span>
            </td>
        </tr>
        <tr>
            <td style="width: 2%">
            </td>
            <td class="blackblodtext" align="left" style="width: 18%">
                Customer Name
            </td>
            <td style="width: 30%">
                <asp:TextBox ID="txtUsrRptCustmName" runat="server" CssClass="altText" Width="200px"></asp:TextBox>
                <asp:RegularExpressionValidator ID="RegtxtUsrRptCustmName" ControlToValidate="txtUsrRptCustmName"
                    ValidationGroup="submit" Display="dynamic" runat="server" SetFocusOnError="true"
                    ErrorMessage="<br>& < > ' + % \ / ( ) ; ? | ^ = ! ` , [ ] { } : # $ @ ~ and &#34; are invalid characters."
                    ValidationExpression="^(a-z|A-Z|0-9)*[^\\/<>+;?|!`,\[\]{}\x22;=^:@#$%&()']*$"></asp:RegularExpressionValidator>
            </td>
            <td class="blackblodtext" align="left">
                Destination
            </td>
            <td>
                <asp:TextBox ID="txtUsrRptDestination" runat="server" CssClass="altText" Width="200px"></asp:TextBox>
                <asp:RegularExpressionValidator ID="RegtxtUsrRptDestination" ControlToValidate="txtUsrRptDestination"
                    ValidationGroup="submit" Display="dynamic" runat="server" SetFocusOnError="true"
                    ErrorMessage="<br>Invalid email address." ValidationExpression="[a-zA-Z][\w\.-]*[a-zA-Z0-9]@[a-zA-Z0-9][\w\.-]*[a-zA-Z0-9]\.[a-zA-Z][a-zA-Z\.]*[a-zA-Z]$"></asp:RegularExpressionValidator>
            </td>
        </tr>
        <tr>
            <td style="width: 3%"></td>
            <td class="blackblodtext" align="left">
                StartTime
            </td>
            <td>
                <asp:TextBox ID="txtUsrRptStartTime" runat="server" CssClass="altText" Width="70px"></asp:TextBox>
                <img alt="" src="image/calendar.gif" border="0" width="20" height="20" id="cal_trigger1"
                    style="cursor: pointer; vertical-align: middle" onclick="return showCalendar('<%=txtUsrRptStartTime.ClientID %>', 'cal_trigger1', 0, '<%=dtFormatType%>');" />
            </td>
            <td class="blackblodtext" align="left">
                Sent Time
            </td>
            <td>
                <asp:TextBox ID="txtUsrRptSentTime" runat="server" CssClass="altText" Enabled="false" Width="70px"
                    ReadOnly="true"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td style="width: 3%">
            </td>
            <td class="blackblodtext" align="left">
                Frequency Count / Type
            </td>
            <td>
                <asp:DropDownList ID="lstUsrRptFrequencyCount" runat="server" CssClass="altSelectFormat"
                    Width="15%">
                    <asp:ListItem Value="1">1</asp:ListItem>
                    <asp:ListItem Value="2">2</asp:ListItem>
                    <asp:ListItem Value="3">3</asp:ListItem>
                    <asp:ListItem Value="4">4</asp:ListItem>
                </asp:DropDownList>
                <asp:DropDownList ID="lstUsrRptFrequencyType" runat="server" CssClass="altSelectFormat"
                    Width="30%">
                    <asp:ListItem Value="1">Days</asp:ListItem>
                    <asp:ListItem Value="2">Weeks</asp:ListItem>
                    <asp:ListItem Value="3">Months</asp:ListItem>
                </asp:DropDownList>
            </td>
            <td colspan="2"></td>
        </tr>
        <tr style="display:none">
            <td style="width: 3%">
            </td>
            <td class="blackblodtext" align="left">
                DeliverType
            </td>
            <td>
                <asp:DropDownList ID="lstUsrRptDeliverType" runat="server" CssClass="altSelectFormat"
                    Width="15%">
                    <asp:ListItem Value="1">1</asp:ListItem>
                    <asp:ListItem Value="2">2</asp:ListItem>
                </asp:DropDownList>
            </td>
            <td colspan="2"></td>            
          </tr>
        <%} %>
        <%-- FB 2363 - End--%>
          <%-- FB 2392 - Start--%>
        <tr>
            <td colspan="5" align="left">
            </td>
         </tr>
         <tr runat="server" id="trWhygo" >
            <td colspan="5" align="left">
                <span class="subtitleblueblodtext">WhyGo Integration Settings</span>
            </td>
         </tr>
         <tr runat="server" id="trWhygoURL">
            <td style="width: 2%">
            </td>
            <td class="blackblodtext" align="left" style="width: 18%">
                URL
            </td>
            <td style="width: 30%">
                <asp:TextBox ID="txtWhyGoURL" runat="server" CssClass="altText" Width="200px" onkeyup="SetButtonStatus(this, 'PollWhyGo') "></asp:TextBox> <%--FB 2594--%>
                <asp:RegularExpressionValidator ID="regttxtWhyGoURL" ControlToValidate="txtWhyGoURL"
                    ValidationGroup="submit" Display="dynamic" runat="server" SetFocusOnError="true"
                    ErrorMessage="<br> & < and > are invalid characters." ValidationExpression="^[^<>&]*$"></asp:RegularExpressionValidator>
            </td>
             <td class="blackblodtext" align="left">
                 User Name
            </td>
            <td>
                <asp:TextBox ID="txtWhygoUsr" runat="server" CssClass="altText" Width="200px"></asp:TextBox>
                <asp:RegularExpressionValidator ID="RglrvalidatorWhygo12" ControlToValidate="txtUsrRptCustmName"
                    ValidationGroup="submit" Display="dynamic" runat="server" SetFocusOnError="true"
                    ErrorMessage="<br> & < and > are invalid characters." ValidationExpression="^[^<>&]*$"></asp:RegularExpressionValidator>
            </td>
        </tr>
         <tr runat="server" id="trWhygoPassword">
             <td style="width: 3%">
            </td>
            <td class="blackblodtext" align="left">
                Password
            </td>
            <td style="height: 21px;">
                <asp:TextBox ID="txtWhygoPwd" style="background-image:url('../image/pwdbg.png'); background-repeat:no-repeat" onchange="javascript:PreservePassword();" runat="server" onblur="PasswordChange(5,1)" onfocus=" fnTextFocus(this.id,1,5)"
                    CssClass="altText" TextMode="Password"></asp:TextBox>
                <asp:RegularExpressionValidator ID="regtxtWhygoPwd" Enabled="false"
                    ControlToValidate="txtWhygoPwd" Display="dynamic" runat="server" SetFocusOnError="true"
                    ErrorMessage="<br> & < and > are invalid characters." ValidationExpression="^[^<>&]*$"></asp:RegularExpressionValidator>
                <asp:CompareValidator ID="cmptxtWhygoPwd" Display="dynamic" ValidationGroup="submit"
                    runat="server" ControlToValidate="txtWhygoPwd" ControlToCompare="txtWhygoPwd2"
                    ErrorMessage="Passwords do not match." Font-Names="Verdana" Font-Size="X-Small"
                    Font-Bold="False" />
            </td>
            <td align="left" style="font-weight: bold" class="blackblodtext">
                Retype Password
            </td>
            <td style="height: 21px;">
                <asp:TextBox ID="txtWhygoPwd2" style="background-image:url('../image/pwdbg.png'); background-repeat:no-repeat" onchange="javascript:PreservePassword();" runat="server" onblur="PasswordChange(5,2)" onfocus=" fnTextFocus(this.id,2,5)"
                    CssClass="altText" TextMode="Password"></asp:TextBox>
                <asp:RegularExpressionValidator ID="regtxtWhygoPwd2" Enabled="false"
                    ControlToValidate="txtWhygoPwd2" Display="dynamic" runat="server" SetFocusOnError="true"
                    ErrorMessage="<br> & < and > are invalid characters." ValidationExpression="^[^<>&]*$"></asp:RegularExpressionValidator>
                <asp:CompareValidator ID="cmptxtWhygoPwd2" runat="server" ValidationGroup="submit"
                    ControlToValidate="txtWhygoPwd2" ControlToCompare="txtWhygoPwd" ErrorMessage="Passwords do not match."
                    Font-Names="Verdana" Font-Size="X-Small" Font-Bold="False" />
            </td>
        </tr>
         <tr runat="server" id="trPoll" >
            <td style="width: 3%">
            </td>
            <td class="blackblodtext" align="left">
                 Poll
            </td>
            <td style="height: 21px;">
                <asp:Button ID="PollWhyGo" runat="server" ValidationGroup="submit" Enabled="false" CssClass="altShortBlueButtonFormat" Text="Now" OnClick="PollWhyGoNow"/>  <%-- OnClick="PollWhyGo"--%>
                <asp:ImageButton ImageUrl="image/btn_delete.gif" runat="server" ID="delPubliRoom"
                                        ToolTip="Remove Public Rooms" OnClick="DeletePublicRooms" />
            </td>
            <td colspan="2">
            </td>
        </tr>
        <%-- FB 2392 - End--%>    
        <tr>
            <%--  Commented for FB 1849
            <td colspan="5" align="center"> 
            <ajax:ModalPopupExtender ID="RoomPopUp" runat="server" TargetControlID="ChgOrg" BackgroundCssClass="modalBackground"   PopupControlID="switchOrgPnl" DropShadow="false" Drag="true"  CancelControlID="ClosePUp"></ajax:ModalPopupExtender>
            <asp:Panel ID="switchOrgPnl" runat="server" HorizontalAlign="Center" Width="35%"  CssClass="treeSelectedNode" >
                <table width="100%" align="center" border="0"> 
                    <tr>
                      <td align="center" class="blackblodtext"><SPAN class=subtitleblueblodtext>Switch Organization</SPAN><br />
                      <p>
                      All the transactions performed will be for the below selected organization after the switch.
                     </p>
                      </td>
                      </tr>
                    <tr>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                      <td align="center"> 
                        <asp:DropDownList ID="DrpOrganization" DataTextField="OrganizationName" DataValueField="OrgId" runat="server" CssClass="altLong0SelectFormat" Width="205px">
                        </asp:DropDownList>
                      </td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td align="center">
                     <asp:button runat="server" ID="BtnChangeOrganization" CssClass="altShortBlueButtonFormat" Text=" Submit " OnClientClick="javascript:return fnChangeOrganization();" OnClick="btnChgOrg_Click" ></asp:button>&nbsp;
                     <input align="middle" type="button" runat="server" style="width:100px;height:21px;" id="ClosePUp" value=" Close " class="altButtonFormat" onclick="javascript:fnClearOrg();" />
                      </td>
                    </tr>
                </table>
              </asp:Panel>
            </td>--%>
        </tr>
        <tr>
            <td align="center" colspan="5" style="font-weight: bold; font-size: small; color: black;
                font-family: verdana;" height="21" style="font-weight: bold">
                <input id="btnReset" type="reset" value="Reset" class="altMedium0BlueButtonFormat" />&nbsp;
                <asp:Button runat="server" ID="btnSubmit" CssClass="altMedium0BlueButtonFormat" OnClick="btnSubmit_Click" OnClientClick="javascript:DataLoading('1');"
                 Text="Submit" ValidationGroup="submit"></asp:Button> <%--ZD 100176 --%>
            </td>
        </tr>
    </table>
    <input type="hidden" name="formname" id="formname" value="frmMainsuperadministrator" />
    <img src="keepalive.asp" name="myPic" width="1" height="1">

    <script language="javascript">

        //FB 2363
        function fnView() {
            window.location = 'ESEventReport.aspx';
        }

        function fnTransferPage() {
            window.location = 'UITextChange.aspx';
        }

        /*Commented for FB 1849
        function fnClearOrg() 
        {
        var obj1 = document.getElementById('DrpOrganization');
        if(obj1)
        {
        obj1.value = '<%=orgid%>';
        }        
        }*/
        function Validation() {
            var file1 = document.getElementById('fleMap1');
            var msg = document.getElementById('txtCompanymessage');
            var lblval = document.getElementById('lblvalidation');
            var lblfile = document.getElementById('lblfilevalidation');
            if (file1.value == "") {
                lblval.value = "Required";
                return false;
            }
            else if (msg.value == "") {
                lblfile.value = "Required";
                return false;
            }
            else
                return true;

        }

//        SavePassword(); //FB 1896
    </script>

    </form>
    <%--FB 2659 Starts--%>
    <div id="PopupDefaultLicense" class="rounded-corners" style="position: absolute;
    overflow:hidden; border: 0px;
    width: 1000px; display: none;">    
    <iframe src="" id="DefaultLicense" name="DefaultLicense" style="height: 530px; border: 0px; overflow:hidden; width: 1000px; overflow: hidden;"></iframe>
	</div>
	<%--FB 2659 End--%>
</body>
</html>
<%--code added for Soft Edge button--%>

<script type="text/javascript" src="inc/softedge.js"></script>

<!-- #INCLUDE FILE="inc/mainbottomNET.aspx" -->
<!-- FB 2050 Start -->

<script type="text/javascript">

    function refreshImage() {
        var obj = document.getElementById("mainTop");
        if (obj != null) {
            var src = obj.src;
            var pos = src.indexOf('?');
            if (pos >= 0) {
                src = src.substr(0, pos);
            }
            var date = new Date();
            obj.src = src + '?v=' + date.getTime();

            if (obj.width > 804)
                obj.setAttribute('width', '804');
        }
        //refreshStyle(); // Commented for Refresh Issue
        setMarqueeWidth();
        return false;
    }

    function refreshStyle() {
        var i, a, s;
        a = document.getElementsByTagName('link');
        for (i = 0; i < a.length; i++) {
            s = a[i];
            if (s.rel.toLowerCase().indexOf('stylesheet') >= 0 && s.href) {
                var h = s.href.replace(/(&|\\?)forceReload=d /, '');
                s.href = h + (h.indexOf('?') >= 0 ? '&' : '?') + 'forceReload=' + (new Date().valueOf());
            }
        }
    }

    function setMarqueeWidth() {
        var screenWidth = screen.width - 25;
        if (document.getElementById('martickerDiv') != null)
            document.getElementById('martickerDiv').style.width = screenWidth + 'px';

        if (document.getElementById('marticDiv') != null)
            document.getElementById('marticDiv').style.width = screenWidth + 'px';

        if (document.getElementById('marticker2Div') != null)
            document.getElementById('marticker2Div').style.width = (screenWidth - 15) + 'px';

        if (document.getElementById('martic2Div') != null)
            document.getElementById('martic2Div').style.width = (screenWidth - 15) + 'px';
    }



    window.onload = refreshImage;

    document.getElementById("reglstLDAPScheduleTime").controltovalidate = "lstLDAPScheduleTime_Text"; //FB 2462
    document.getElementById("reqlstLDAPScheduleTime").controltovalidate = "lstLDAPScheduleTime_Text"; //FB 2462
</script>

<!-- FB 2050 End -->

<script type="text/javascript">
//FB 2659 Start
    $(document).ready(function() {        
        $('#btnDefaultClick').click(function() {            
            $('#popupdiv').fadeIn();
            $("#DefaultLicense").attr("src", "DefaultLicense.aspx");
            $('#PopupDefaultLicense').show();
            $('#PopupDefaultLicense').bPopup({
                fadeSpeed: 'slow',
                followSpeed: 1500,
                modalColor: 'gray'
            });
        });
});
//FB 2659 End
</script>