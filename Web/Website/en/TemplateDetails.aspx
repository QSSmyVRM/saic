﻿<%--ZD 100147 start--%>
<%--/* Copyright (C) 2013 myVRM - All Rights Reserved	
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.		
*
* You should have received a copy of the myVRM license with	
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/--%><%--ZD 100147 End--%>
<%@ Page Language="C#" AutoEventWireup="true"  Inherits="en_TemplateDetails" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
  
  <meta name="Description" content="myVRM (Videoconferencing Resource Management) is a revolutionary Web-based software application that manages any video conferencing environment.">
  <meta name="Keywords" content="myVRM, Videoconferencing Resource Management, video conferencing, video bridges, video endpoints">
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <meta NAME="LANGUAGE" CONTENT="en">
  <meta NAME="DOCUMENTCOUNTRYCODE" CONTENT="us">
    <%--FB 2790 Starts--%>
    <script type="text/javascript" src="script/wincheck.js"></script> <%--FB 3055--%>
<script type="text/javascript">
    var path = '<%=Session["OrgCSSPath"]%>';
    path = path.split('.')[0] + '<%=Session["ThemeType"]%>' + ".css"; // FB 2815
    document.write('<link rel="stylesheet" title="Expedite base styles" type="text/css" href="' + path + '?' + new Date().getTime() + '">');
   </script>   
<%--FB 2790 Ends--%>

  <script type="text/javascript" src="script/errorList.js"></script>
  <script type="text/javascript" src="script\mytree.js"></script>
  
<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>myVRM</title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
         <table width="90%" >
            <tr>
                <td align="center">
                  <h3>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Template Details</h3>
                              
                </td>
            </tr>  
            <tr>
                <td align="center" style="width: 1168px">
                    <asp:Label ID="ErrLabel" runat="server" Text="" CssClass="lblError"></asp:Label>
                </td>
            </tr>              
        </table> 
         <table border="0" cellpadding="2" cellspacing="3" width="90%">
            <tr>
                <td colspan=2>
                <%--Window Dressing--%>
                <b><font class="subtitleblueblodtext">Template Summary</font></b><br /><br />      
                </td>
            </tr>
            <tr>
              <td align="left" style="width:30%" valign="top"><span class="blackblodtext">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Description</span></td><%--FB 2508--%>
              <td align="left">
                <asp:Label ID="LblDescription" runat="server"></asp:Label>
              </td>
            </tr>
            <tr>
              <td align="left" style="width:30%"><span class="blackblodtext">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<%if (Application["Client"].ToString().ToUpper() == "MOJ"){%>Public Hearing<%}else{ %>Public Conference<%}%></span></td><%--added for FB 1428 Start--%>
              <td align="left">
                <asp:Label ID="LblConference" runat="server"></asp:Label>
              </td>
            </tr>
            <tr>
              <td align="left"><span class="blackblodtext">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Room(s)</span></td>
              <td align="left">                               
                <asp:Label ID="LblRoom" runat="server"></asp:Label>
              </td>
            </tr>
     </table>
     <table id="tbPrt" runat="server"> <%--Edited for FB 1425 QA Bug--%>
        <tr>
            <%--Window Dressing--%>
            <td align="center" class="subtitleblueblodtext">
                <br /><b><span class="subtitleblueblodtext">Participant(s) Status </span></b><br /><br />
             </td>
        </tr>
     </table>
     <table style="margin:0 auto" width="100%" id="tbDgPrt" runat="server">        
        <tr>
            <td align="center">
                <asp:DataGrid ID="DgTemplates"  runat="server" AutoGenerateColumns="False" CellPadding="0" GridLines="None" AllowSorting="true" 
                     BorderColor="blue" BorderStyle="solid" BorderWidth="1" ShowFooter="true" OnItemDataBound="BindGrid" 
                       Width="100%" Visible="true" style="border-collapse:separate"> <%--Edited for FF--%>
                    <SelectedItemStyle  CssClass="tableBody"/>
                     <AlternatingItemStyle CssClass="tableBody" />
                     <ItemStyle CssClass="tableBody"  />
                    <HeaderStyle CssClass="tableHeader" Height="30px" />
                    <EditItemStyle CssClass="tableBody" />                    
                     <%--Window Dressing START--%>
                    <FooterStyle CssClass="tableBody" />
                    <Columns>                       
                        <asp:BoundColumn  HeaderStyle-CssClass="tableHeader" HeaderText="Name" ItemStyle-CssClass="tableBody"></asp:BoundColumn>
                        <asp:BoundColumn  HeaderStyle-CssClass="tableHeader" HeaderText="Email" ItemStyle-CssClass="tableBody"></asp:BoundColumn>                        
                        <asp:BoundColumn  HeaderStyle-CssClass="tableHeader" HeaderText="Status" ItemStyle-CssClass="tableBody"></asp:BoundColumn>                                    
                    </Columns>
                    <%--Window Dressing END--%>
                </asp:DataGrid>
                <asp:Table  runat="server" ID="TblNoTemplates" Visible="false" CellPadding="1" CellSpacing="0" Width="100%" >
                    <asp:TableRow>                                       
                        <asp:TableCell width="90%" align="left">
                             <span class="lightgrayboldstext">
                                  This template includes no participant.
                                </SPAN>
                        </asp:TableCell>
                    </asp:TableRow>
                    
                </asp:Table>                    
            </td>
          </tr>
     </table>
     
     <table border="0" cellpadding="1" cellspacing="0" width="100%">
        <tr>
          <td align="right"><br />
<%--code added for Soft Edge button--%>
            <input type="button" name="close" value="Close" class="altMedium0BlueButtonFormat" onclick="Javascript: window.close();" />
          </td>
       </tr>
    </table>
    </div>
    </form>
</body>
</html>
<%--code added for Soft Edge button--%>
<script type="text/javascript" src="inc/softedge.js"></script>