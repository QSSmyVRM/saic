<%--ZD 100147 start--%>
<%--/* Copyright (C) 2013 myVRM - All Rights Reserved	
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.		
*
* You should have received a copy of the myVRM license with	
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/--%><%--ZD 100147 End--%>
<%@ Page Language="C#" AutoEventWireup="true" Inherits="en_Transcoding1" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>Transcoding</title>
    <link rel="StyleSheet" href="css/myprompt.css" type="text/css" />
    <script type="text/javascript" src="script/wincheck.js"></script> <%--FB 3055-URL--%>
    <%--FB 2790 Starts--%>
<script type="text/javascript">
    var path = '<%=Session["OrgCSSPath"]%>';
    path = path.split('.')[0] + '<%=Session["ThemeType"]%>' + ".css"; // FB 2815
    document.write('<link rel="stylesheet" title="Expedite base styles" type="text/css" href="' + path + '?' + new Date().getTime() + '">');
   </script>   
<%--FB 2790 Ends--%>

    <script language="javascript" src="extract.js" type="text/javascript" />

    <script type="text/javascript" src="inc/functions.js" language="javascript"></script>

    <script type="text/javascript" src= "script/settings2.js" language="javascript"></script>

    <script type="text/javascript" src= "script/mousepos.js" language="javascript"></script>

</head>
<body>

    <script type="text/javascript" language="javascript"> 

function all_up()
{
/*
	for (i=1; i<=6; i++) {
	    alert('document.' + 'layout_' + i);
	   eval('document.' + 'layout_' + i).src = "image/layout/t" + i + "_up.gif";
	} 
*/}


function ManualLayoutChg(cb) 
{
	/*if (document.frmTranscoding.VideoLayout[1].checked) {
		if ( (cb.src).indexOf("_up.gif") != -1 ) {
			all_up();
			cb.src = (cb.src).replace("_up.gif", "_down.gif");
		}
	}*/
}


function VideoSessionChg()
{
	cb = document.frmTranscoding.VideoSession;
	if (cb.selectedIndex >= 0) {
		if ( ((cb[cb.selectedIndex].text).toLowerCase()).indexOf("continuous") == -1 ) {
			//document.getElementById("VideoDisplayLayoutDiv").style.display = "block";
		} else {
			//document.getElementById("VideoDisplayLayoutDiv").style.display = "none";
		}
	}
}

function managelayout (dl, epid, epty)
{


	//dl=0;
        htmlString = "<font class='subtitleblueblodtext'> Manage Display Layout </font>";	
		window.resizeTo(1200,450);
	change_display_layout_prompt('image/pen.gif', htmlString, epid, epty, dl, parseInt('<%= rowsize %>'), '<%= imageFiles %>' + '|' + '<%= imageFilesBT %>', '<%= DISPLAY_LAYOUT_IMAGE_PATH %>');

}



function manualLayoutSelected(cbval)
{
	/*if (cbval == "1")
		all_up();
	else {
		mvl = opener.document.frmSettings2.ManualVideoLayout.value;
		if ( (parseInt(mvl, 10) > 0) && (parseInt(mvl, 10) < 7) )
			eval('document.layout_' + mvl).src = "image/layout/t" + mvl + "_down.gif";
	}*/
}


function setinitval (cb, initval)
{
	for (i = 0; i < cb.length; i++) {
		if (cb[i].value == initval) {
			cb[i].selected = true;
		}
	}
}

function setTranscoding ()
{

	if (opener.document.frmSettings2.maxVideo.value == "0"){
		document.frmTranscoding.maxVideo.value = "";
	}
	else{
		document.frmTranscoding.maxVideo.value = opener.document.frmSettings2.maxVideo.value;
	}

	if (opener.document.frmSettings2.maxAudio.value == "0"){
		document.frmTranscoding.maxAudio.value  = "";
	}
	else{
		document.frmTranscoding.maxAudio.value = opener.document.frmSettings2.maxAudio.value;
	}

	
	cb = document.frmTranscoding.VideoSession;
	initval = opener.document.frmSettings2.VideoSession.value;
	setinitval (cb, initval);

	cb = document.getElementById("LineRate");
	initval = opener.document.frmSettings2.LineRate.value;
	
	if ( (initval == 0) && ("<%= (Convert.ToString(Application["Client"])).ToUpper() %>" == "BTBOCES") )
		initval = "512";	

	setinitval (cb, initval);

	cb = document.frmTranscoding.AudioAlgorithm;
	initval = opener.document.frmSettings2.AudioAlgorithm.value;
	setinitval (cb, initval);
	
	cb = document.frmTranscoding.VideoProtocol;
	initval = opener.document.frmSettings2.restrictProtocol.value;
	setinitval (cb, initval);
    
	cb = document.frmTranscoding.restrictUsage;
	createBy = opener.document.frmSettings2.CreateBy.value;
	
	initval = opener.document.frmSettings2.restrictAV.value;
	if (initval == "") {
		if (createBy != "7")
			initval = 3;
		else {alert("here");
			initval = 2;}
	}
	setinitval (cb, initval);

//********************************* set display layout **************************************
   if (opener.document.frmSettings2.ManualVideoLayout.value != "") {
	initval = opener.document.frmSettings2.ManualVideoLayout.value;

 	if ( (initval > 0) && (initval < 10))
		initval = "0" + parseInt(initval,10);

//alert(initval);
	if ( ("<%= (Convert.ToString(Application["Client"])).ToUpper() %>" == "BTBOCES") || ("<%= (Convert.ToString(Application["Client"])).ToUpper() %>" == "WHOUSE"))
		document.frmTranscoding.fname.value = "BTBoces/" + initval + ".gif"
	else
		document.frmTranscoding.fname.value = initval + ".gif"
		

	document.frmTranscoding.newLayout.value = initval;	
	document.frmTranscoding.layoutimg.src = document.frmTranscoding.fname.value;
}

//*********************************set display layout ends here******************************
	
	initval = opener.document.frmSettings2.dualStream.value;
	document.frmTranscoding.dualStreamMode.checked = (initval == "1") ? true : false;

	initval = opener.document.frmSettings2.LectureMode.value;
	document.frmTranscoding.LectureMode.checked = (initval == "1") ? true : false;
	
	if((opener.document.frmSettings2.conferenceOnPort.value)=="1" || (opener.document.frmSettings2.encryption.value)=="1"){
		document.frmTranscoding.polycom.checked = true;
	}
	else{
		document.frmTranscoding.polycom.checked = false;
	}
	polycomSpecific();
	
	initval = opener.document.frmSettings2.conferenceOnPort.value;
	document.frmTranscoding.confOnPort.checked = (initval == "1") ? true : false;
	
	initval = opener.document.frmSettings2.encryption.value;
	document.frmTranscoding.encryption.checked = (initval == "1") ? true : false;
 //alert(document.frmTranscoding.fpath.value + document.frmTranscoding.fname.value+"HI")
  document.frmTranscoding.layoutimg.src = document.frmTranscoding.fpath.value + document.frmTranscoding.fname.value;
	VideoSessionChg();
}


function frmSubmit() 
{

   
    if (isNaN(document.getElementById("maxVideo").value))
    {
        alert("Invalid value for Maximum Video Ports");
        document.getElementById("maxVideo").focus();
        return false;
    }
    if (isNaN(document.getElementById("maxAudio").value))
    {
        alert("Invalid value for Maximum Audio Ports");
        document.getElementById("maxAudio").focus();
        return false;
    }
    
	cb = document.frmTranscoding.LineRate;
	opener.document.frmSettings2.LineRate.value = (cb.selectedIndex>=0) ? cb[cb.selectedIndex].value : "";

	cb = document.frmTranscoding.AudioAlgorithm;
	opener.document.frmSettings2.AudioAlgorithm.value = (cb.selectedIndex>=0) ? cb[cb.selectedIndex].value : "";

	//cb = document.frmTranscoding.VideoProtocol;
	//opener.document.frmSettings2.VideoProtocol.value =(cb.selectedIndex>=0) ? cb[cb.selectedIndex].value : "";

	opener.document.frmSettings2.LectureMode.value = (document.frmTranscoding.LectureMode.checked ? "1" : "0");

	opener.document.frmSettings2.dualStream.value = (document.frmTranscoding.dualStreamMode.checked ? "1" : "0");

	opener.document.frmSettings2.conferenceOnPort.value = (document.frmTranscoding.confOnPort.checked ? "1" : "0");

	opener.document.frmSettings2.encryption.value = (document.frmTranscoding.encryption.checked ? "1" : "0");

	opener.document.frmSettings2.maxAudio.value = document.frmTranscoding.maxAudio.value;

	opener.document.frmSettings2.maxVideo.value = document.frmTranscoding.maxVideo.value;

	cb = document.frmTranscoding.VideoSession;
	opener.document.frmSettings2.VideoSession.value = (cb.selectedIndex>=0) ? cb[cb.selectedIndex].value : "";

	cb = document.frmTranscoding.VideoProtocol;
	opener.document.frmSettings2.restrictProtocol.value = (cb.selectedIndex>=0) ? cb[cb.selectedIndex].value : "";

	cb = document.frmTranscoding.restrictUsage;
	opener.document.frmSettings2.restrictAV.value =(cb.selectedIndex>=0) ? cb[cb.selectedIndex].value : "";

	t = true;
	if (cb.selectedIndex >= 0) {
		if ( ((cb[cb.selectedIndex].text).toLowerCase()).indexOf("continuous") != -1 ) {
			t = false;
		} 
	}

	/*if (t) {
		vl = (document.frmTranscoding.VideoLayout[0].checked ? "1" : "2");
			
		mvl = "";
		if (vl == "1")
			mvl = "";
		else {
			for (var i=1; i<=6; i++)
				mvl = ( (eval('document.' + 'layout_' + i).src).indexOf("_down.gif") != -1 ) ? i : mvl;
		}
	} else {
		vl = "";
		mvl = "";
	}*/

	if (queryField("from") == "rm") {
		// !! conf password
		if ( !chkConfPassword(document.frmTranscoding.ConferencePassword) )
			return false;

		opener.document.frmSettings2.ConferencePassword.value = document.frmTranscoding.ConferencePassword.value;
	}

	opener.document.frmSettings2.ManualVideoLayout.value = document.frmTranscoding.newLayout.value;
	

//	alert("VideoLayout=" + opener.document.frmSettings2.VideoLayout.value);
//	alert("ManualVideoLayout=" + opener.document.frmSettings2.ManualVideoLayout.value);
//	alert("VideoSession=" + opener.document.frmSettings2.VideoSession.value);
//	alert("ConferencePassword=" + opener.document.frmSettings2.ConferencePassword.value);
	if (queryField("from") == "rm") {
		opener.TranscodeSubmit ();
	}
	else
		if (opener.document.frmSettings2.Settings2Audio.value.indexOf("(Set)") < 0)
			opener.document.frmSettings2.Settings2Audio.value = opener.document.frmSettings2.Settings2Audio.value + " (Set)";
	window.close();
}

function polycomSpecific()
{
	if(document.frmTranscoding.polycom.checked) {
		t = "block";
	}
	else
	{
		t = "none";
	}
	
	for (var i=1; i<3; i++) {
		document.getElementById("polycomDIV" + i).style.display = t;
	}
}

function change_display_layout_prompt(promptpicture, prompttitle, epid, epty, dl, rowsize, images, imgpath) 
{
	var title = new Array()
	title[0] = "Default ";
	title[1] = "Custom ";
	promptbox = document.createElement('div'); 
	promptbox.setAttribute ('id' , 'prompt');
	document.getElementsByTagName('body')[0].appendChild(promptbox);
	promptbox = eval("document.getElementById('prompt').style");

	rowsize = 4;
	promptbox.position = 'absolute'
	promptbox.top = -50+mousedownY+"px";//Edited for FF...
	promptbox.left = mousedownX - 450+"px";//Edited for FF...
	promptbox.width = rowsize * 125+"px"; //FB 1373
	promptbox.border = 'outset 1 #bbbbbb' 
	promptbox.height = 400+"px";//FB 1373
	promptbox.overflow ='auto';//FB 1373
	

    //Window Dressing
	m = "<table cellspacing='0' cellpadding='0' border='0' width='100%'><tr valign='middle'><td width='22' height='22' style='text-indent:2;' class='tableHeader'><img src='" + promptpicture + "' height='18' width='18'></td><td class='titlebar'>" + prompttitle + "</td></tr></table>" 
	m += "<table cellspacing='2' cellpadding='2' border='0' width='100%' class='promptbox'>";

	if (("<%= (Convert.ToString(Application["Client"])).ToUpper() %>" == "BTBOCES") || ("<%= (Convert.ToString(Application["Client"])).ToUpper() %>" == "WHOUSE") )
  	  for (s=0;s<2;s++) {
		imagesary = images.split("|")[s].split(":")

		if (s==1)
		{
			path = imgpath + "<%= (Convert.ToString(Application["Client"])).ToUpper() %>" + "/";
		}	
		else
			path = imgpath;
		rowNum = parseInt( (imagesary.length + rowsize - 2) / rowsize, 10 );
		m += "	<tr>";
		m += "    <td colspan='" + (rowsize * 2) + "' align='left'><b>" + title[s] + "Display Layout</b><input name='default" + s + "' id='default" + s + "' type='checkbox' onclick='javascript:displayLayout(" + s + ")'></td>";
		m += "  </tr>"
		m += "	<tr><td>";
		m += "  <div name='display" + s + "' id='display" + s + "' style='display:None'>";
		m += "  <table>";
		m += "  <tr>"
		m += "    <td colspan='" + (rowsize * 2) + "' height='5'></td>";
		m += "  </tr>"
	
		imgno = 0;
		for (i = 0; i < rowNum; i++) {
			m += "  <tr>";
			for (j = 0; (j < rowsize) && (imgno < imagesary.length-1); j++) {
				
			
				m += "    <td valign='middle'>";
				m += "      <input type='radio' name='layout1' id='layout1' value='" + imagesary[imgno] + "' onClick='chgimg(\"" + epid + "\", \"" + epty + "\", \"" + imagesary[imgno] + "\", \"" + imgpath + "\")'" + ( (dl == imagesary[imgno]) ? " checked" : "" ) +">";
				m += "    </td>";
				m += "    <td valign='middle'>";
				//
				m += "      <img src='" + path  + imagesary[imgno] + ".gif' width='57' height='43'>";
				m += "    </td>";
				imgno ++;
			}
			m += "  </tr>";
		}
		m += " </table></div></td><tr>";
	}	
	else {
		imagesary = images.split(":");
		rowNum = parseInt( (imagesary.length + rowsize - 2) / rowsize, 10 );
		m += "	<tr>";
		//Window Dressing
		m += "    <td colspan='" + (rowsize * 2) + "' align='left' class='blackblodtext'>Display Layout</td>";//FB 2579
		m += "  </tr>"
		m += "  <tr>"
		m += "    <td colspan='" + (rowsize * 2) + "' height='5'></td>";
		m += "  </tr>"
	
		imgno = 0;
		for (i = 0; i < rowNum; i++) {
			m += "  <tr>";
			for (j = 0; (j < rowsize) && (imgno < imagesary.length-1); j++) {
				
			
				m += "    <td valign='middle'>";
				m += "      <input type='radio' name='layout' id='layout' value='" + imagesary[imgno] + "' onClick='chgimg(\"" + epid + "\", \"" + epty + "\", \"" + imagesary[imgno] + "\", \"" + imgpath + "\")'" + ( (dl == imagesary[imgno]) ? " checked" : "" ) +">";
				m += "    </td>";
				m += "    <td valign='middle'>";
				m += "      <img src='" + imgpath + imagesary[imgno] + ".gif' width='57' height='43'>";
				m += "    </td>";
				imgno ++;
			}
			m += "  </tr>";
		}
	}	

	m += "  <tr>";
	m += "    <td colspan='" + (rowsize * 2) + "' height='5'></td>";
	m += "  </tr>"
	m += "  <tr><td align='right' colspan='" + (rowsize * 2) + "'>"
	//code added for Soft Edge button
//	m += "    <input type='button' class='prompt' value='Submit' onMouseOver='this.style.border=\"1 outset #dddddd\"' onMouseOut='this.style.border=\"1 solid transparent\"' onClick='saveOrder(\"" + epid + "\", \"" + epty + "\");'>"
//	m += "    <input type='button' class='prompt' value='Cancel' onMouseOver='this.style.border=\"1 outset transparent\"' onMouseOut='this.style.border=\"1 solid transparent\"' onClick='cancelthis();'>"
	m += "    <input type='button' onfocus='this.blur()' class='altMedium0BlueButtonFormat' value='Submit'  onClick='saveOrder(\"" + epid + "\", \"" + epty + "\");'>"
	m += "    <input type='button' onfocus='this.blur()' class='altMedium0BlueButtonFormat' value='Cancel'  onClick='cancelthis();'>"
	m += "  </td></tr>"
	m += "</table>" 
	
	document.getElementById('prompt').innerHTML = m;
	
	if (("<%= (Convert.ToString(Application["Client"])).ToUpper() %>" == "BTBOCES") || ("<%= (Convert.ToString(Application["Client"])).ToUpper() %>" == "WHOUSE") )
	    cb = document.getElementsByName ("layout1");
	else
	    cb = document.getElementsByName ("layout");
	var fileAndPath =document.getElementById("layoutimg").src;
	var lastPathDelimiter = fileAndPath.lastIndexOf("/");
    var fileNameOnly = fileAndPath.substring(lastPathDelimiter+1);
    var imgIndex =fileNameOnly.substring(0,fileNameOnly.length-4); 
   
    if('<%=(Convert.ToString(Application["Client"])).ToUpper() %>" == "PSU")%>')
    {   
        if(imgIndex=="16")
        cb[4].checked=true;
        else if(imgIndex=="05")
        cb[3].checked=true;
         else
	    cb[imgIndex-1].checked=true;
    }
    else if(imgIndex>100)
    {
    if(('<%=(Convert.ToString(Application["Client"])).ToUpper() %>" == "WHOUSE")%>')||('<%=(Convert.ToString(Application["Client"])).ToUpper() %>" == "BTBOCES")%>'))
    cb[imgIndex-100-1].checked=true;
    }
    else
	cb[imgIndex-1].checked=true;
} 

function displayLayout(s)
{	
	var obj = document.getElementById("default" + s);
	var temp = document.getElementById("display" + s);
	//alert(obj.checked);
	if (obj.checked)
		temp.style.display = "block";
	else
		temp.style.display = "none";
}

function chgimg(epid, epty, img, imgpath)
{
//	document.getElementById("layoutimg" + epid + "-" + epty).src = imgpath + img + ".gif";
}


function saveOrder(epid, epty) 
{
	newlayout = "";
	//Added for transcoding
  if (("<%= (Convert.ToString(Application["client"])).ToUpper() %>" == "BTBOCES") || ("<%= (Convert.ToString(Application["client"])).ToUpper() %>" == "WHOUSE"))
	cb = document.getElementsByName ("layout1")
	else
	cb = document.getElementsByName ("layout")
	
		if (cb.length == 1) {
		if (cb.checked)
			newlayout = cb.value;
	} else {
		for (i = 0; i < cb.length; i++) {
			if (cb[i].checked) {
				newlayout = cb[i].value;
				break;
			}
		}
	}
	
	if(newlayout=='')
	    newlayout="01";
	   
	document.frmTranscoding.displaylayout.value = newlayout;
	document.frmTranscoding.epid.value = epid;
	document.frmTranscoding.terminaltype.value = epty;
	if ( ( ("<%= (Convert.ToString(Application["client"])).ToUpper() %>" == "BTBOCES") || ("<%= (Convert.ToString(Application["client"])).ToUpper() %>" == "WHOUSE")) && (newlayout > 100))
		document.frmTranscoding.fname.value = document.frmTranscoding.fpath.value + "BTBoces/" + newlayout + ".gif";
	else
		document.frmTranscoding.fname.value = document.frmTranscoding.fpath.value + newlayout + ".gif";
	document.frmTranscoding.newLayout.value = newlayout;
	document.frmTranscoding.layoutimg.src = document.frmTranscoding.fname.value;

	cancelthis();
} 


function cancelthis()
{
	document.getElementsByTagName("body")[0].removeChild(document.getElementById("prompt"));
    window.resizeTo(750,450);
}

function FnchkLimit(obj, tpe)
{

    var spCharacters = "";
//	if (obj.type == "textarea")
//	{
    var specialCharacters = new Array("&", "<", ">", "'", "+", "%", "/", "\"", "\\", "\(", "\)", ";", "?", "|", "^", "=", "!", "`", ",", "[", "]", "{", "}", ":", "#", "$", "@", "~", "-"); //Changed for FB 1721
	var	spCharacters = "& < > ' + % \ / ( ) ; ? | ^ = ! ` , [ ] { } : # $ @ ~ - and \""; //Changed for FB 1721
	
	if (tpe == "e") // FB case 505, 457
	{
        specialCharacters = new Array("&", "<", ">", "'", "+", "%", "\"", "\\", ";", "?", "|", "=", "!", "`", ",", "[", "]", "{", "}", ":", "#", "$", "~", "-"); //Changed for FB 1721
	    spCharacters = "& < > ' + % \ ; ? | = ! ` , [ ] { } : # $ ~ - and \""; //Changed for FB 1721
	}
	else if (tpe == "u") //FB 615 & 616
	{
	    specialCharacters[8] = "&"; // 8 is the character "\" - BackSlash
	    tpe = "2";
	}
//	}
//	else
//	{
//		var specialCharacters = new Array("&", "<", ">", "'", "+", "%", "/", "\"", "\(", "\)", ";");
//		spCharacters = "& < > ' + % \" \( \)";
//    }
	var temp="";	
//	if (len <= 512)
	for (var i=0;i<specialCharacters.length;i++) {
//		alert(specialCharacters[i]);
		if (obj.value.indexOf(specialCharacters[i])!= -1 )
		{
			//temp = "'" + specialCharacters[i] + "', " + temp;
			alert( specialCharacters[i] + " is an Invalid Character for this field. Removed from the input.\r\n" + spCharacters + " characters are invalid.");
			obj.value = obj.value.replace(specialCharacters[i], "");
			//return false;
		}
	}
	//if (temp != "") alert( temp + " are Invalid Character(s) for this field. Removed from the input.");
// 1 lmtSvrAddr = 15
// 2 lmtFLName = 256
// 3 lmtMessage = 4000
// 4 lmtLogin = 256
// 5 lmtPWD = 256
// 6 lmtAddress = 512
// 7 lmtPhone = 20
// 8 lmtDesc = 2000
// 9 lmtImage = 500
// 10 lmtIPISDN = 512
// 11 lmtSecKey = 256
// 12 lmtURL = 512
// 13 lmtMenuMask = 200
// 14 lmtEmail = 512
// 15 lmtSubject = 2000
// 16 lmtLicKey = 8000
	switch(tpe) {
	case "1":
		len = 14;
		break;
	case "2":
		len = 255;
		break;
	case "3":
		len=3999;
		break;
	case "4":
		len = 255;
		break;
	case "5":
		len = 255;
		break;
	case "6":
		len = 511;
		break;
	case "7":
		len = 19;
		break;
	case "8":
		len = 1999;
		break;
	case "9":
		len = 499;
		break;
	case "10":
		len = 512;
		break;
	case "11":
		len = 255;
		break;
	case "12":
		len = 511;
		break;
	case "13":
		len = 199;
		break;
	case "14":
		len = 511;
		break;
	case "15":
		len = 1999;
		break;
	case "16":
		len = 7999;
		break;
	case "17":
		len = 29;
		break;
	case "e":
	    len = 255;
	    break;
	default:
		len = tpe;
	}										
		
	if (obj.value.length >= len)
	{
		alert("You have reached the maximum limit allowed for this field, " + (parseInt(len) + parseInt(1)) + " characters. The rest has be truncated.");
		obj.value = obj.value.substring(0,len+1);
	}
}

//-->
    </script>

    <form id="frmTranscoding" runat="server" method="post">
        <div>
            <center>
                <input type="hidden" name="displaylayout" value=""/>
                <input type="hidden" name="epid" value="" />
                <input type="hidden" name="terminaltype" value="" />
                <input type="hidden" name="cmd" value="" runat="server" />
                <input type="hidden" name="confid" id="confID" runat="server" />
                <input type="hidden" name="trans" value="" runat="server" />
                <input type="hidden" name="fname" value="01.gif" />
                <input type="hidden" name="newLayout" value="01" />
                <input type="hidden" name="rowsize" value="4" id="rowsize" runat="server" />
                <input type="hidden" name="currentVideoLayout" id="selvsName" runat="server">
                <input type="hidden" name="fpath" value="<%=DISPLAY_LAYOUT_IMAGE_PATH%>"/>
                <table border="0" cellpadding="1" cellspacing="0" width="90%">
                    <tr>
                        <td colspan="2" style="height: 5">
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" align="center">
                           <h3><asp:Label ID="lblHeader" runat="server" Text="Advanced Audio/Video Settings"></asp:Label></h3><br />
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" align="left">
                            <span class="subtitlexxsblueblodtext" style="margin-left:-20px; position:relative;">Common Settings (vendor-neutral). </span>
                        </td>
                    </tr>
                    <tr>
                        <td style="height: 200" valign="top">
                            <table width="100%" cellpadding="2" cellspacing="3">
                                <tr>
                                    <td align="left">
                                        <span class="blackblodtext">Restrict Network Access to</span>
                                    </td>
                                    <td align="left">
                                        <asp:DropDownList ID="VideoProtocol" runat="server" CssClass="SelectFormat" DataValueField="vpIDs"
                                            DataTextField="vpNames">
                                            <%--FB 1721 Start--%>
                                            <asp:ListItem Text="IP" Value="1"></asp:ListItem>
                                            <asp:ListItem Text="IP,ISDN" Value="2"></asp:ListItem>
                                            <asp:ListItem Text="IP,ISDN,SIP" Value="3"></asp:ListItem>
                                            <asp:ListItem Text="IP,ISDN,SIP,MPI" Value="4"></asp:ListItem>
                                             <%--FB 1721 end--%>
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left">
                                        <span class="blackblodtext">Restrict Usage to</span>
                                    </td>
                                    <td align="left">
                                        <asp:DropDownList ID="restrictUsage" runat="server" CssClass="SelectFormat" DataValueField="audioIDs"
                                            DataTextField="audioNames" />
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" valign="top">
                                        <span class="blackblodtext">Video Codecs</span>
                                    </td>
                                    <td align="left">
                                        <asp:DropDownList ID="VideoSession" runat="server" CssClass="SelectFormat" DataValueField="vsIDs"
                                            DataTextField="vsNames" />
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left">
                                        <span class="blackblodtext">Audio Codecs</span>
                                    </td>
                                    <td align="left">
                                        <asp:DropDownList ID="AudioAlgorithm" runat="server" CssClass="SelectFormat" DataValueField="aaIDs"
                                            DataTextField="aaNames" />
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" style="width: 40%">
                                        <span class="blackblodtext">Dual Stream Mode</span>
                                    </td>
                                    <td align="left" style="width: 60%">
                                        <!--Have to add Client Validation code snippet -->
                                        <%
                                            if (Convert.ToString(Application["client"]) == "LHRIC")
                                            {
                                        %>
                                        <input type="checkbox" name="dualStreamMode" id="dualStreamMode1" value="1" checked="checked" runat="server"/>
                                        <%
                                            }
                                            else
                                            {
                                        %>
                                        <input type="checkbox" name="dualStreamMode" id="dualStreamMode" value="1" runat="server"/>
                                        <%
                                            }
                                        %>
                                    </td>
                                    
                                </tr>
                                <%-- Code chaged for FB 1360 --%>
                                <tr>
                                    <td align="left">
                                        <span class="blackblodtext">Maximum Line Rate</span>
                                    </td>
                                    <td align="left">
                                        <asp:DropDownList ID="LineRate" runat="server" CssClass="altSelectFormat">
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <%-- Code chaged for FB 1360 --%>
                                <tr>
                                    <td colspan="4" align="left">
                                        <span class="subtitlexxsblueblodtext" style="margin-left:-20px; position:relative;">Polycom-specific Settings</span>
                                        <input type="checkbox" name="polycom" value="1" onclick="javaScript:polycomSpecific();" />
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="4">
                                        <table width="100%">
                                        <%-- Code chaged for FB 1360--%> 
                                            <tr id="polycomDIV1">
                                                <td colspan="4"></td>
                                                 
                                            </tr>
                                         <%--Code chaged for FB 1360 --%>
                                            <tr id="polycomDIV2">
                                                <td align="left" style="width: 40%">
                                                    <span class="blackblodtext">Conference on Port</span>
                                                </td>
                                                <td align="left" style="width: 60%">
                                                    <input type="checkbox" name="confOnPort" value="1" />
                                                </td>
                                                <td align="left" style="width: 40%">
                                                    <span class="blackblodtext">Lecture Mode</span>
                                                </td>
                                                <td align="left" style="width: 60%">
                                                    <input type="checkbox" name="LectureMode" value="1" />
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <td style="height: 200" valign="top">
                            <table width="100%" cellpadding="2" cellspacing="3">
                                <tr>
                                    <td align="left">
                                        <span class="blackblodtext">Maximum Video Ports</span>
                                    </td>
                                    <td align="left">
                                        <input type="text" id="maxVideo" maxlength="9" name="maxVideo" value="" class="altText" onkeyup="FnchkLimit(this,'2')"
                                            runat="server" /> <%-- Changed for FB 1721 --%>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left">
                                        <span class="blackblodtext">Maximum Audio Ports</span>
                                    </td>
                                    <td align="left">
                                        <input type="text" id="maxAudio" maxlength="9" name="maxAudio" value="" class="altText" onkeyup="FnchkLimit(this,'2')"
                                            runat="server" /> <%-- Changed for FB 1721 --%>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" style="width: 40%">
                                        <span class="blackblodtext">Video Display</span>
                                    </td>
                                    <td align="left" style="width: 60%">
                                        <table border="0">
                                            <tr>
                                                <td>
                                                    <img name="layoutimg" alt="Layout" id="layoutimg"  src="" />
                                                </td>
                                                <td>
                                                    <input type="button" name="ConfLayoutSubmit" id="ConfLayoutSubmit" value="Change" class="altMedium0BlueButtonFormat"
                                                        onclick="javascript:managelayout('', '01', '');" />
                                                    <!-- <% %> removed in displayLayout-->
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <%-- Code chaged for FB 1360 --%>
                                <tr>
                                     <td align="left" style="width: 40%">
                                        <span class="blackblodtext">Encryption</span>
                                    </td>
                                    <td align="left" style="width: 60%">
                                        <input type="checkbox" name="encryption" value="1">
                                    </td>
                                </tr>
                               <%-- Code chaged for FB 1360 --%>
                            </table>
                        </td>
                    </tr>
                </table>
                <br />
                <table border="0" cellpadding="2" cellspacing="0" width="90%">
                    <tr>
                        <td align="center">
                            <input type="reset" value="Reset" name="TranscodingSubmit" class="altMedium0BlueButtonFormat" />
                        </td>
                        <td align="center">
                            <input type="button" value="Cancel" onfocus="this.blur()" name="TranscodingSubmit" class="altMedium0BlueButtonFormat"
                                onclick="JavaScript:window.close();" />
                        </td>
                        <td align="center">
                            <input type="button" value="Submit" onfocus="this.blur()" name="TranscodingSubmit" class="altMedium0BlueButtonFormat"
                                onclick="JavaScript:frmSubmit();" />
                        </td>
                    </tr>
                </table>
            </center>
        </div>
    </form>
</body>

<script language="JavaScript" type="text/javascript">
<!--

	setTranscoding ();
	if (queryField("from") == "rm")
		document.frmTranscoding.ConferencePassword.value = opener.document.frmSettings2.ConferencePassword.value;
	window.resizeTo(750,450);

//-->

</script>

</html>
 <%--code added for Soft Edge button--%>
<script type="text/javascript" src="inc/softedge.js"></script>