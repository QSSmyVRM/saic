<%--ZD 100147 start--%>
<%--/* Copyright (C) 2013 myVRM - All Rights Reserved	
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.		
*
* You should have received a copy of the myVRM license with	
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/--%><%--ZD 100147 End--%>
<%@ Page language="c#" Inherits="myVRMAdmin.Web.en.Esthetic" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> <!-- FB 2050 -->
<meta http-equiv="X-UA-Compatible" content="IE=8" /> <!-- FB 2050 -->
<!-- <!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" > Commented for FB 2050 -->
<HTML>
	<!-- #INCLUDE FILE="inc/maintopNET.aspx" --> 
    <%--<style type="text/css">.hdiv{position:absolute;left:0; visibility:hidden;overflow:scroll;}</style>--%>
    <%--ZD 100156--%>
    <style type="text/css">
    .hdiv{ width:300px; height:200px; position:absolute; 
    left:150px; margin-left: auto; margin-right: auto;display:none;
}</style>
	<BODY bottomMargin="0" leftMargin="0" topMargin="0" rightMargin="0">
		<script language="javascript" src="../en/Organizations/Original/Javascript/RGBColorPalette.js"> </script><%-- FB 1830--%>
		<script language="JavaScript">
<!--
  
var imtime1 = parseInt("", 10);
imtime1 = ( (imtime1 < 0.5) || isNaN(imtime1) ) ? 0.5 : imtime1;

//-->

//Code added for FB 1473 - start
function fnShow()
{
    var args = fnShow.arguments;
    var defaultThemeRow = document.getElementById("DefaultThemeRow");
    var customizeRow = document.getElementById("CustomizeRow");
    var btnsave = document.getElementById("btnSave");
    var btnpreview = document.getElementById("btnPreview");
    
    if(args[0] == "1")
    {
        customizeRow.style.display = '';//Edited For FF..
        btnsave.value = 'Submit';
        btnpreview.style.display = '';//Edited For FF..
        document.getElementById("hdnValue").value = '';
    }
    else if(args[0] == "0")
    {
        customizeRow.style.display = 'None';
        btnsave.value = 'Apply Theme';
        btnpreview.style.display = 'None';
    }
}

function fnValue()
{
    var args = fnValue.arguments;
    document.getElementById("hdnValue").value = args[0];  
    fnShow('0');
}
//ZD 100156 - Start
function toggleDiv(id,flagit) 
{
    if (flagit=="1")
    {
        if (document.layers) document.layers[''+id+''].visibility = "show"
        else if (document.all) document.all[''+id+''].style.display = "block"
        else if (document.getElementById) document.getElementById('' + id + '').style.display = "block"
    }
    else if (flagit=="0")
    {
        if (document.layers) document.layers[''+id+''].visibility = "hide"
        else if (document.all) document.all['' + id + ''].style.display = "none"
        else if (document.getElementById) document.getElementById('' + id + '').style.display = "none"
    }
}
//ZD 100156 - End
//Code added for FB 1473 - end
//ZD 100176 start
function DataLoading(val) {
    if (val == "1")
        document.getElementById("dataLoadingDIV").innerHTML = "<img border='0' src='image/wait1.gif'>";
    else
        document.getElementById("dataLoadingDIV").innerHTML = "";
}
//ZD 100176 End

</script>

   <%-- All 'Organizations/Original' path changed to '..' for FB 1830--%>
		<!-- script finished -->
		<TABLE cellPadding="2" width="100%" border="0">
			<TBODY>
				<TR>
					<TD><!--------------------------------- CONTENT START HERE ---------------> <!--------------------------------- CONTENT GOES HERE --------------->  <!-- Java Script Begin -->
						<!-- Java Script End -->
						<FORM id="frmSample" name="frmSample" method="post" runat="server">
						<input type="hidden" id="hdnValue" runat="server" />
						<CENTER style="height:30"> <!-- FB 1633 -->
							<H3>User Interface Logo & Theme</H3><!-- FB 2570 --><%--FB 2787--%>
							<asp:label id="errLabel" Runat="server" CssClass="lblError"></asp:label>
							 <div id="dataLoadingDIV" align="center"></div> <%--ZD 100176--%>
						</CENTER>
							<CENTER> 
								<TABLE id="Table5" cellspacing="0" cellpadding="0" width="100%" align="center" border="0"><!-- FB 1633 --> <%--FB 1775--%>
									<TBODY>
									<TR>
											
											<TD align="left" width="20%"  height="20"><!-- FB 1633 -->
											<SPAN class="subtitleblueblodtext">&nbsp;Customize Images</SPAN>
											</TD> <%--FB 1775--%>
										</TR>
									    <tr>
											
											<td vAlign="top" align="center" width="100%">
												<table id="tablebanner" cellSpacing="0" cellpadding="2" width="100%" align="left" border="0">
													<tbody>
														<tr><!-- FB 1633 --><%--FB 2579 Start--%>
															<td vAlign="top" align="left" width="16%" class="blackblodtext">&nbsp;&nbsp;Standard Resolution Company Logo<%--FB 2787--%>
															</td> <%--FB 1775--%>
															<td align="left" width="63%" valign="top"><table cellpadding="0px" cellspacing="0px" width="100%" border="0"><tr><td align="left"><%--FB 2909 Start--%>
                                                                <div>
                                                                <input type="text" class="file_input_textbox" readonly="readonly" value='No file selected'/>
                                                                <div class="file_input_div"><input type="button" value="Browse" class="file_input_button"  />
                                                                <input type="file" class="file_input_hidden" id="Bannerfile1024" accept="image/*" contenteditable="false" enableviewstate="true" runat="server" onchange="getfilename(this)"/></div></div><%--FB 3055-Filter in Upload Files--%>
															<%--<input class="altText" id="Bannerfile1024" type="file" size="60" name="Banner1024" runat="server">--%><%--FB 2909 End--%>
															    <span class="blackItalictext"> (200 x 75 pixels)</span><%-- FB 2779 --%>
                                                                <asp:RegularExpressionValidator ID="regBannerfile1024" runat="server" Display="Dynamic" ValidationGroup="Submit1" ControlToValidate="Bannerfile1024" CssClass="lblError" ErrorMessage="File type is invalid." ValidationExpression="^.*\.((j|J)(p|P)(e|E)?(g|G)|(g|G)(i|I)(f|F)|(p|P)(n|N)(g|G)|(b|B)(m|M)(p|P))$"></asp:RegularExpressionValidator>
															    <asp:Label ID="lblstdres" Text="" Visible="false" ForeColor=Red runat="server"></asp:Label>															    
															    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:ImageButton ImageUrl="image/btn_delete.gif" runat="server" ID="RImg1" ToolTip="Delete"  OnClientClick="javascript:DataLoading(1);"/><%--ZD 100176--%> 
															    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:Button ID="btnBanner" runat="server" OnClick="btnBanner_Click" CssClass="altMedium0BlueButtonFormat" Text="Upload" OnClientClick="javascript:DataLoading(1);" /> <%--FB 2719 FB 2798--%> <%--ZD 100176--%> 
															    </table><!-- FB 2739 -->
															</td>															
														</tr>
														<%--Commented for FB 1633 start--%>
														<tr>
															<td vAlign="top" align="left" class="blackblodtext"><%--<B>Company Logo</B>--%>
															</td>
															<td align="left" valign="top"><table cellpadding="0px" cellspacing="0px" width="100%" border="0"><tr><td align="left"><INPUT class="altText" id="companylogo" type="file" size="60" name="CompanyLogo" runat="server" visible="false">
															 <span class="blackItalictext"> <%--(122 x 44 pixels)--%></span>
															 <asp:Label ID="lblsitelogo" Text="" Visible="false" ForeColor=Red runat="server"></asp:Label>
															&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:ImageButton ImageUrl="image/btn_delete.gif" runat="server" ID="RImg3" ToolTip="delete"  Visible="false" />
															<INPUT class="altText" id="Bannerfile1600" type="file" size="60" name="Banner1600" runat="server" visible="false">
															<span class="blackItalictext" > <%--(1380 x 72 pixels)--%></span>
															<asp:Label ID="lblHighres" Text="" Visible="false" ForeColor=Red runat="server"></asp:Label>&nbsp;&nbsp;															
															    <asp:ImageButton ImageUrl="image/btn_delete.gif" runat="server" ID="RImg2"  ToolTip="delete" visible="false" />
															</td></tr></table>
															</td>
														</tr>
														<%--Commented for FB 1633 end--%>
													</tbody>
												</table>
											</td>
										</tr>
								         <%--Code added for FB 1473 - start--%>
								         <tr> 
								            <td colspan="3" width="100%">
								                <table border="0"  width="100%">
										        	<tr>
											            <td align="left" width="20%"><span class="subtitleblueblodtext">UI Themes 
														           </span></td>
            <%--												<table id="tabletitle" width="25" align="right" border="0">
													            <tr>
														            <td align="center" class="tableHeader" height="20">3</td>
													            </tr>
												            </table></td>
            --%>											<!-- UI Changes for FB 1775 -->
											            <td width="1%" height="20">&nbsp;</td>
											            <td align="left" height="20" ></td>
										            </tr>
										            <tr>
                    						            <td ></td>
									                    <td vAlign="top" align="center" colspan="2">
											                <table id="table1" cellSpacing="0" cellPadding="2" width="100%" align="center" border="0">
												                <tbody>
													                <tr>													                    
														                <td align="left"> <%-- FB 2050 --%>
															                <table cellspacing="0" cellpadding="3" class="tableBody" border="0" width="95%">
															                    <tr class="tableHeader">
                                                                                    <td ></td>
                                                                                    <td align="left" class="tableHeader" >Theme Name</td>
                                                                                    <td align="center" class="tableHeader" width="30%" >Theme View</td>
                                                                                    <td width="5%"></td>
                                                                                    <td align="left" class="tableHeader" >Theme Name</td>
                                                                                    <td align="center" class="tableHeader" width="30%" >Theme View</td>
                                                                                    <td width="5%"></td>
                                                                                    <td align="left" class="tableHeader" >Theme Name</td>
                                                                                    <td align="center" class="tableHeader" width="30%" >Theme View</td>										                            
                                                                                </tr>
                                                                                 <tr>
                                                                                    <td>
                                                                                        <asp:RadioButton ID="RDefault" onclick="javascript:fnValue('Default')" runat="server" GroupName="Theme"  />
                                                                                    </td>										                            
                                                                                    <td class="tableBody">
                                                                                        Default
                                                                                    </td>
                                                                                    <td align="center">
                                                                                        <asp:ImageButton ToolTip="Click to view full size Image"  BorderStyle="groove" BorderWidth="1" ID="ImageButton1" OnClientClick="javascript:toggleDiv('DefaultDiv',1);return false;" ImageUrl="Image/Default.bmp"  runat="server" Width="60" Height="40"   />
                                                                                        <div align="center" class='hdiv' id='DefaultDiv' style=" width:950px;height:550px;border-style:groove; background-color:White; border-color:Menu;z-index:8;position:absolute;">
                                                                                            <input type="button" value="Close" onfocus="this.blur()" name="cls1" class="altShortBlueButtonFormat" onclick="javascript:return toggleDiv('DefaultDiv',0)"/>
                                                                                        <img src='Image/Default.bmp' /></div><%-- FB 2779 --%>
                                                                                    </td>
                                                                                    <td>
                                                                                        <asp:RadioButton ID="RTheme1" onclick="javascript:fnValue('Theme1')" runat="server" GroupName="Theme"  />
                                                                                    </td>										                            
                                                                                    <td class="tableBody">
                                                                                        Blue <%--FB 2719--%>
                                                                                    </td>
                                                                                    <td align="center">
                                                                                        <asp:ImageButton ToolTip="Click to view full size Image"  BorderStyle="groove" BorderWidth="1" ID="Theme1" OnClientClick="javascript:toggleDiv('Theme1Div',1);return false;" ImageUrl="Image/Theme1.bmp"  runat="server" Width="60" Height="40"   />
                                                                                        <div align="center" class='hdiv' id='Theme1Div' style=" width:950px;height:550px;border-style:groove; background-color:White; border-color:Menu;z-index:8;position:absolute;">
                                                                                            <input type="button" value="Close" onfocus="this.blur()" name="cls1" class="altShortBlueButtonFormat" onclick="javascript:return toggleDiv('Theme1Div',0)"/>
                                                                                        <img src='Image/Theme1.bmp' /></div><%-- FB 2779 --%>
                                                                                    </td>
                                                                                    <td>
                                                                                        <asp:RadioButton ID="RTheme2" onclick="javascript:fnValue('Theme2')" runat="server" GroupName="Theme"  />
                                                                                    </td>
                                                                                    <td class="tableBody">
                                                                                        Red <%--FB 2719--%>
                                                                                    </td>
                                                                                    <td align="center" >
                                                                                        <asp:ImageButton ToolTip="Click to view full size Image" BorderStyle="groove" BorderWidth="1" ID="Theme2" OnClientClick="javascript:toggleDiv('Theme2Div',1);return false;" ImageUrl="Image/Theme2.bmp" runat="server" Width="60" Height="40"   />
                                                                                        <div align="center" class='hdiv' id='Theme2Div' style="width:950px;height:550px;border-style:groove; background-color:White; border-color:ActiveBorder;z-index:8;position:absolute;">
                                                                                            <input type="button" value="Close" onfocus="this.blur()" class="altShortBlueButtonFormat" onclick="javascript:return toggleDiv('Theme2Div',0)"/>
                                                                                        <img src='Image/Theme2.bmp' /></div><%-- FB 2779 --%>
                                                                                    </td>
                                                                                </tr>										                        
                                                                                <tr style="display:none"><%-- FB 2739 & FB 2719--%>
                                                                                    <td>
                                                                                        <asp:RadioButton ID="RTheme3" onclick="javascript:fnValue('Theme3')" runat="server" GroupName="Theme"  />
                                                                                    </td>
                                                                                    <td class="tableBody">
                                                                                        Earth
                                                                                    </td>
                                                                                    <td align="center">
                                                                                        <asp:ImageButton ToolTip="Click to view full size Image" BorderStyle="groove" BorderWidth="5" ID="Theme3" OnClientClick="javascript:toggleDiv('Theme3Div',1);return false;" ImageUrl="Image/Theme3.bmp" runat="server" Width="60" Height="40"   />
                                                                                        <div align="center" class='hdiv' id='Theme3Div' style="width:950px;height:550px;border-style:groove; background-color:White; border-color:ActiveBorder;z-index:8;position:absolute;">
                                                                                            <input type="button" value="Close" onfocus="this.blur()" class="altShortBlueButtonFormat" onclick="javascript:return toggleDiv('Theme3Div',0)"/>
                                                                                        <img src='Image/Theme3.bmp' /></div>
                                                                                    </td>
                                                                                    <td>
                                                                                        <asp:RadioButton ID="RTheme4" onclick="javascript:fnValue('Theme4')" runat="server" GroupName="Theme"  />
                                                                                    </td>
                                                                                    <td class="tableBody">
                                                                                        Green
                                                                                    </td>
                                                                                    <td align="center">
                                                                                        <asp:ImageButton ToolTip="Click to view full size Image" BorderStyle="groove" BorderWidth="5" ID="Theme4" OnClientClick="javascript:toggleDiv('Theme4Div',1);return false;" ImageUrl="Image/Theme4.bmp" runat="server" Width="60" Height="40"   />
                                                                                        <div align="center" class='hdiv' id='Theme4Div' style="width:950px;height:550px;border-style:groove; background-color:White; border-color:ActiveBorder;z-index:8;position:absolute;">
                                                                                            <input type="button" value="Close" onfocus="this.blur()" class="altShortBlueButtonFormat" onclick="javascript:return toggleDiv('Theme4Div',0)"/>
                                                                                        <img src='Image/Theme4.bmp' /></div>
                                                                                    </td>										                        									                        
                                                                                    <td>
                                                                                        <asp:RadioButton ID="RTheme5" onclick="javascript:fnValue('Theme5')" runat="server" GroupName="Theme"  />
                                                                                    </td>
                                                                                    <td class="tableBody">
                                                                                        Pastel
                                                                                    </td>
                                                                                    <td align="center">
                                                                                        <asp:ImageButton ToolTip="Click to view full size Image" BorderStyle="groove" BorderWidth="5" ID="Theme5" OnClientClick="javascript:toggleDiv('Theme5Div',1);return false;" ImageUrl="Image/Theme5.bmp" runat="server" Width="60" Height="40"   />
                                                                                        <div align="center" class='hdiv' id='Theme5Div' style="width:950px;height:550px;border-style:groove; background-color:White; border-color:ActiveBorder;z-index:8;position:absolute;">
                                                                                            <input type="button" value="Close" onfocus="this.blur()" class="altShortBlueButtonFormat" onclick="javascript:return toggleDiv('Theme5Div',0)"/>
                                                                                        <img src='Image/Theme5.bmp' /></div>
                                                                                    </td>
                                                                                 </tr> 
                                                                                <tr style="display:none"><%-- FB 2739 & FB 2719--%>
                                                                                    <td>
                                                                                        <br />
                                                                                        <asp:RadioButton ID="RThemeCustom" OnClick="javascript:return fnShow('1');" runat="server" GroupName="Theme"  />
                                                                                    </td>
                                                                                    <td class="tableBody">
                                                                                        <br />
                                                                                        Custom
                                                                                    </td>
                                                                                    <td align="center">
                                                                                    </td>
                                                                                    <td colspan="6"></td>
                                                                                </tr>
                                                                            </table>
                                                                         </td>
                                                                    </tr>
                                                                </tbody>
                                                             </table>
                                                         </td>
                                                    </tr>            
										        </table>
            								</td>
            						    </tr>
										<tr id="CustomizeRow" runat="server" style="display:none" width="100%"><%--ZD 100263--%>
										    <td colspan="3" width="100%">
										    <%--FB 1633 alignment start--%>
										        <table border="0" width="100%"> <%--edited for FF--%>
                                                    <tr> 
                                                        <td align="left" width="20%"><span class="subtitleblueblodtext">Customize Menus 
														           </span></td>											</td>
											            <td width="1%" height="20">&nbsp;</td>
											            <td align="left" height="20" ></td>                             
											         </tr>
										            <tr>
                    						            <td height="20"></td>
										                <td valign="top" align="left" colspan="2">
										                    <table width="100%" border="0">
										                        <tbody>
														            <tr>
															            <td align="left">
																            <table cellSpacing="0" cellPadding="3" bgColor="#e1e1e1" border="0" width="95%">
																	            <tr class="tableHeader">
																		            <td align="center" width="29%" rowspan="2" nowrap class="tableHeader">Description</td>
																		            <td align="center" width="37%" colspan="2" nowrap class="tableHeader">Menu Bar</td>
																		            <td align="center" width="35%" colspan="2" nowrap class="tableHeader">Dropdown List</td>
																	            </tr>
																	            <tr class="tableHeader">
																		            <td align="center" class="tableHeader">Current</td>
																		            <td align="center" class="tableHeader">New</td>
																		            <td align="center" class="tableHeader">Current</td>
																		            <td align="center" class="tableHeader">New</td>
																	            </tr>
																	            <tr>
																		            <td style="PADDING-RIGHT: 0px; PADDING-LEFT: 0px; PADDING-BOTTOM: 0px; PADDING-TOP: 0px" colSpan="5" bgcolor="#ffffff" >
																			            <div id="md" style="BORDER-RIGHT: #e1e1e1 0px solid; BORDER-TOP: #e1e1e1 1px solid; OVERFLOW: auto; BORDER-LEFT: #e1e1e1 0px solid; WIDTH: 100%; BORDER-BOTTOM: #e1e1e1 0px solid; POSITION: relative;">
																				            <table id="Table10"  cellSpacing="1" cellPadding="3" class="tableBody" border="0" width="100%">
																					            <tr>
																						            <td class="tableBody">Mouse Off Font</td>
																						            <td align="left" class="tableBody"><asp:Label ID="lblm_offfont" Runat="server" BorderWidth="1px" BorderColor="#000000" BorderStyle="Solid"></asp:Label>&nbsp;<asp:Label ID="lblm_offfont1" Runat="server"></asp:Label></td>
																						            <td>
																							            <table>
																								            <tr>
																									            <td><asp:textbox EnableViewState="True" id="m_mouseofffont" Runat="server" CssClass="altText" Width="70px"></asp:textbox></td>
																									            <td><IMG title="Click to select the color" onclick="FnShowPalette(event,'m_mouseofffont')"
																											            height="23" alt="Color" src="../Image/color.jpg" width="20" name="image3">    <%-- Organization Css Module  --%>
																									            </td>
																								            </tr>
																							            </table>
																						            </td>
																						            <td align="left" class="tableBody"><asp:Label ID="lbld_offfont" Runat="server" BorderWidth="1px" BorderColor="#000000" BorderStyle="Solid"></asp:Label>&nbsp;<asp:Label ID="lbld_offfont1" Runat="server"></asp:Label></td>
																						            <td>
																							            <table>
																								            <tr>
																									            <td><asp:textbox EnableViewState="True" id="d_mouseofffont" Runat="server" CssClass="altText" Width="70px"></asp:textbox></td>
																									            <td><IMG title="Click to select the color" onclick="FnShowPalette(event,'d_mouseofffont')"
																											            height="23" alt="Color" src="../Image/color.jpg" width="20" name="image4"><%-- Organization Css Module  --%>
																									            </td>
																								            </tr>
																							            </table>
																						            </td>
																					            </tr>
																					            <tr>
																						            <td class="tableBody">Mouse Off Background</td>
																						            <td align="left" class="tableBody"><asp:Label ID="lblm_Offback" Runat="server" BorderWidth="1px" BorderColor="#000000" BorderStyle="Solid"></asp:Label>&nbsp;<asp:Label ID="lblm_Offback1" Runat="server"></asp:Label></td>
																						            <td>
																							            <table>
																								            <tr>
																									            <td><asp:textbox EnableViewState="True" id="m_mouseOffback" Runat="server" CssClass="altText" Width="70px"></asp:textbox></td>
																									            <td>
																										            <IMG title="Click to select the color" onclick="FnShowPalette(event,'m_mouseOffback')" height="23" alt="Color" src="../Image/color.jpg" width="20" name="image5"><%-- Organization Css Module  --%>
																									            </td>
																								            </tr>
																							            </table>
																						            </td>
																						            <td align="left" class="tableBody"><asp:Label ID="lbld_Offback" Runat="server" BorderWidth="1px" BorderColor="#000000" BorderStyle="Solid"></asp:Label>&nbsp;<asp:Label ID="lbld_Offback1" Runat="server"></asp:Label></td>
																						            <td>
																							            <table>
																								            <tr>
																									            <td><asp:textbox EnableViewState="True" id="d_mouseOffback" Runat="server" CssClass="altText" Width="70px" ></asp:textbox></td>
																									            <td><IMG title="Click to select the color" onclick="FnShowPalette(event,'d_mouseOffback')"
																											            height="23" alt="Color" src="../Image/color.jpg" width="20" name="image6"> <%-- Organization Css Module  --%>
																									            </td>
																								            </tr>
																							            </table>
																						            </td>
																					            </tr>
																					            <tr>
																						            <td class="tableBody">Mouse On Font</td>
																						            <td align="left" class="tableBody"><asp:Label ID="lblm_Onfont" Runat="server" BorderWidth="1px" BorderColor="#000000" BorderStyle="Solid"></asp:Label>&nbsp;<asp:Label ID="lblm_Onfont1" Runat="server"></asp:Label></td>
																						            <td>
																							            <table>
																								            <tr>
																									            <td><asp:textbox EnableViewState="True" id="m_mouseOnfont" Runat="server" CssClass="altText" Width="70px"></asp:textbox></td>
																									            <td><IMG title="Click to select the color" onclick="FnShowPalette(event,'m_mouseOnfont')"
																											            height="23" alt="Color" src="../Image/color.jpg" width="20" name="image7"> <%-- Organization Css Module  --%>
																									            </td>
																								            </tr>
																							            </table>
																						            </td>
																						            <td align="left" class="tableBody"><asp:Label ID="lbld_Onfont" Runat="server" BorderWidth="1px" BorderColor="#000000" BorderStyle="Solid"></asp:Label>&nbsp;<asp:Label ID="lbld_Onfont1" Runat="server"></asp:Label></td>
																						            <td>
																							            <table>
																								            <tr>
																									            <td><asp:textbox EnableViewState="True" id="d_mouseOnfont" Runat="server" CssClass="altText" Width="70px"></asp:textbox></td>
																									            <td><IMG title="Click to select the color" onclick="FnShowPalette(event,'d_mouseOnfont')"
																											            height="23" alt="Color" src="../Image/color.jpg" width="20" name="image8"> <%-- Organization Css Module  --%>
																									            </td>
																								            </tr>
																							            </table>
																						            </td>
																					            </tr>
																					            <tr>
																						            <td class="tableBody">Mouse On Background</td>
																						            <td align="left" class="tableBody"><asp:Label ID="lblm_Onback" Runat="server" BorderWidth="1px" BorderColor="#000000" BorderStyle="Solid"></asp:Label>&nbsp;<asp:Label ID="lblm_Onback1" Runat="server"></asp:Label></td>
																						            <td>
																							            <table>
																								            <tr>
																									            <td><asp:textbox EnableViewState="True" id="m_mouseOnback" Runat="server" CssClass="altText" Width="70px"></asp:textbox></td>
																									            <td><IMG title="Click to select the color" onclick="FnShowPalette(event,'m_mouseOnback')"
																											            height="23" alt="Color" src="../Image/color.jpg" width="20" name="image9"> <%-- Organization Css Module  --%>
																									            </td>
																								            </tr>
																							            </table>
																						            </td>
																						            <td align="left" class="tableBody"><asp:Label ID="lbld_Onback" Runat="server" BorderWidth="1px" BorderColor="#000000" BorderStyle="Solid"></asp:Label>&nbsp;<asp:Label ID="lbld_Onback1" Runat="server"></asp:Label></td>
																						            <td>
																							            <table>
																								            <tr>
																									            <td><asp:textbox EnableViewState="True" id="d_mouseOnback" Runat="server" CssClass="altText" Width="70px"></asp:textbox></td>
																									            <td><IMG title="Click to select the color" onclick="FnShowPalette(event,'d_mouseOnback')"
																											            height="23" alt="Color" src="../Image/color.jpg" width="20" name="image10">  <%-- Organization Css Module  --%>
																									            </td> 
																								            </tr>
																							            </table>
																						            </td>
																					            </tr>
																					            <tr>
																						            <td class="tableBody">Menu Border</td>
																						            <td align="left" class="tableBody"><asp:Label ID="lblm_mborder" Runat="server" BorderWidth="1px" BorderColor="#000000" BorderStyle="Solid"></asp:Label>&nbsp;<asp:Label ID="lblm_mborder1" Runat="server"></asp:Label></td>
																						            <td>
																							            <table>
																								            <tr>
																									            <td><asp:textbox EnableViewState="True" id="m_menuborder" Runat="server" CssClass="altText" Width="70px"></asp:textbox></td>
																									            <td><IMG title="Click to select the color" onclick="FnShowPalette(event,'m_menuborder')"
																											            height="23" alt="Color" src="../Image/color.jpg" width="20" name="image11">  <%-- Organization Css Module  --%>
																									            </td>
																								            </tr>
																							            </table>
																						            </td>
																						            <td align="left" class="tableBody"><asp:Label ID="lbld_mborder" Runat="server" BorderWidth="1px" BorderColor="#000000" BorderStyle="Solid"></asp:Label>&nbsp;<asp:Label ID="lbld_mborder1" Runat="server"></asp:Label></td>
																						            <td>
																							            <table>
																								            <tr>
																									            <td><asp:textbox EnableViewState="True" id="d_menuborder" Runat="server" CssClass="altText" Width="70px"></asp:textbox></td>
																									            <td><IMG title="Click to select the color" onclick="FnShowPalette(event,'d_menuborder')"
																											            height="23" alt="Color" src="../Image/color.jpg" width="20" name="image12"> <%-- Organization Css Module  --%>
																									            </td>
																								            </tr>
																							            </table>
																						            </td>
																					            </tr>
																					            <tr style="visibility:hidden;height:0px"><%--Edited for FF--%>
																						            <td class="tableBody">Menu Header Font Color</td>
																						            <td align="left" class="tableBody"><asp:Label ID="lblm_hfontcol" Runat="server" BorderWidth="1px" BorderColor="#000000" BorderStyle="Solid"></asp:Label>&nbsp;<asp:Label ID="lblm_hfontcol1" Runat="server"></asp:Label></td>
																						            <td>
																							            <table>
																								            <tr>
																									            <td><asp:textbox EnableViewState="True" id="m_headfontcol" Runat="server" CssClass="altText" Width="70px"></asp:textbox></td>
																									            <td><IMG title="Click to select the color" onclick="FnShowPalette(event,'m_headfontcol')"
																											            height="23" alt="Color" src="../Image/color.jpg" width="20" name="image13">  <%-- Organization Css Module  --%>
																									            </td>
																								            </tr>
																							            </table>
																						            </td>
																						            <td align="left" class="tableBody"><asp:Label ID="lbld_hfontcol" Runat="server" BorderWidth="1px" BorderColor="#000000" BorderStyle="Solid"></asp:Label>&nbsp;<asp:Label ID="lbld_hfontcol1" Runat="server"></asp:Label></td>
																						            <td>
																							            <table>
																								            <tr>
																									            <td><asp:textbox EnableViewState="True" id="d_headfontcol" Runat="server" CssClass="altText" Width="70px"></asp:textbox></td>
																									            <td><IMG title="Click to select the color" onclick="FnShowPalette(event,'d_headfontcol')"
																											            height="23" alt="Color" src="../Image/color.jpg" width="20" name="image14">  <%-- Organization Css Module  --%>
																									            </td>
																								            </tr>
																							            </table>
																						            </td>
																					            </tr>
																					            <tr style="visibility:hidden;height:0px"> <%--Edited for FF--%>
																						            <td class="tableBody">Menu Header Background Color</td>
																						            <td align="left" class="tableBody"><asp:Label ID="lblm_hbackcol" Runat="server" BorderWidth="1px" BorderColor="#000000" BorderStyle="Solid"></asp:Label>&nbsp;<asp:Label ID="lblm_hbackcol1" Runat="server"></asp:Label></td>
																						            <td>
																							            <table>
																								            <tr>
																									            <td><asp:TextBox EnableViewState="True" id="m_headbackcol" Runat="server" CssClass="altText" Width="70px"></asp:textbox></td>
																									            <td><IMG title="Click to select the color" onclick="FnShowPalette(event,'m_headbackcol')"
																											            height="23" alt="Color" src="../Image/color.jpg" width="20" name="image15">   <%-- Organization Css Module  --%>
																									            </td>
																								            </tr>
																							            </table>
																						            </td>
																						            <td align="left" class="tableBody"><asp:Label ID="lbld_hbackcol" Runat="server" BorderWidth="1px" BorderColor="#000000" BorderStyle="Solid"></asp:Label>&nbsp;<asp:Label ID="lbld_hbackcol1" Runat="server"></asp:Label></td>
																						            <td>
																							            <table>
																								            <tr>
																									            <td><asp:textbox EnableViewState="True" id="d_headbackcol" Runat="server" CssClass="altText" Width="70px"></asp:textbox></td>
																									            <td><IMG title="Click to select the color" onclick="FnShowPalette(event,'d_headbackcol')"
																											            height="23" alt="Color" src="../Image/color.jpg" width="20" name="image16"> <%-- Organization Css Module  --%>
																									            </td>
																								            </tr>
																							            </table>
																						            </td>
																					            </tr>
																				            </table>
																			            </div>
																		            </td>
																	            </tr>
																            </table>
															            </td>
														            </tr>
													            </tbody>
												            </table>
											            </td>
										            </tr>	
										            <tr>
                                                        <td align="left" width="20%"><span class="subtitleblueblodtext">Title Font Property
														           </span></td>											</td>
											            <td width="1%" height="20">&nbsp;</td>
											            <td align="left" height="20" ></td>
										            </tr>
										            <tr>
											            <td height="20"></td>
											            <td vAlign="top" align="center" colspan="2">
												            <table id="tabletitlefont" cellSpacing="0" cellPadding="2" width="100%" align="left" border="0">
													            <tbody>
														            <tr>
															            <td align="left">
																            <table cellSpacing="0" cellPadding="3" class="tableBody" border="0" width="95%">
																	            <tr>
																		            <td align="center" width="15%" class="tableHeader">Description</td>
																		            <td align="center" width="22%" class="tableHeader">Current</td>
																		            <td align="center" width="32%" class="tableHeader">New</td>
																	            </tr>
																                <tr>
																	                <td class="tableBody">Font Name</td>
																	                <td class="tableBody">
																		                <asp:Label ID="lbltitlename" Runat="server"></asp:Label>
        															                </td>
																	                <td>
																	                    <asp:dropdownlist id="drptitlefont" Runat="server" CssClass="altText" Width="150">
																		                    <asp:ListItem Value="">Select the Font</asp:ListItem>
																		                    <asp:ListItem Value="Arial">Arial</asp:ListItem>
																		                    <asp:ListItem Value="Bookman Old Style">Bookman Old Style</asp:ListItem>
																		                    <asp:ListItem Value="Century Gothic">Century Gothic</asp:ListItem>
																		                    <asp:ListItem Value="Comic Sans Ms">Comic Sans Ms</asp:ListItem>
																		                    <asp:ListItem Value="Courier New">Courier New</asp:ListItem>
																		                    <asp:ListItem Value="Georgia">Georgia</asp:ListItem>
                    													                    <asp:ListItem Value="Microsoft Sans Serif">Microsoft Sans Serif</asp:ListItem>
																				            <asp:ListItem Value="Tahomo">Tahomo</asp:ListItem>
																			                <asp:ListItem Value="Times New Roman">Times New Roman</asp:ListItem>
																			                <asp:ListItem Value="Verdana">Verdana</asp:ListItem>
																		                </asp:dropdownlist>
																		            </td>
																	            </tr>
																	            <tr>
																		            <td class="tableBody">Font Size</td>
																		            <td class="tableBody">
																			            <asp:Label ID="lbltitlesize" Runat="server"></asp:Label>
																		            </td>
																		            <td>
																			            <asp:dropdownlist id="drptitlesize" Runat="server" CssClass="altText" Width="55">
																				            <asp:ListItem Value="">Size</asp:ListItem>
																				            <asp:ListItem Value="8">8</asp:ListItem>
																				            <asp:ListItem Value="9">9</asp:ListItem>
																				            <asp:ListItem Value="10">10</asp:ListItem>
																				            <asp:ListItem Value="11">11</asp:ListItem>
																				            <asp:ListItem Value="12">12</asp:ListItem>
																				            <asp:ListItem Value="14">14</asp:ListItem>
																				            <asp:ListItem Value="16">16</asp:ListItem>
																				            <asp:ListItem Value="18">18</asp:ListItem>
																				            <asp:ListItem Value="20">20</asp:ListItem>
																				            <asp:ListItem Value="22">22</asp:ListItem>
																			            </asp:dropdownlist>
																		            </td>
																	            </tr>
																	            <tr>
																	                <td class="tableBody">Font Color</td>
																	                <td class="tableBody">
																		                <asp:Label ID="lbltitlecolor" Runat="server" BorderWidth="1px" BorderColor="#000000" BorderStyle="Solid"></asp:Label>
																		                <asp:Label ID="lbltitlecolor1" Runat="server"></asp:Label>
																	                </td>
																	                <td>
																		                <asp:textbox EnableViewState="True" id="txttitlecolor" Runat="server" CssClass="altText"></asp:textbox>
																		                <IMG title="Click to select the color" onclick="FnShowPalette(event,'txttitlecolor')"
																			                height="23" alt="Color" src="../Image/color.jpg" width="27" name="imgtitlefontcolor"> <%-- Organization Css Module  --%>
																	                </td>
																	            </tr>
																            </table>
															            </td>
														            </tr>
													            </table>
											            </td>
										            </tr>
										            <tr>
										                <td align="left" width="20%"><span class="subtitleblueblodtext">Button Font Property
														           </span></td>											
											            <td width="1%" height="20">&nbsp;</td>
											            <td align="left" height="20" ></td>
										            </tr>
										            <tr>
											            <td height="20"></td>
											            <td vAlign="top" align="center" colspan="2">
												            <table id="tablebutton" cellSpacing="0" cellPadding="2" width="100%" align="left" border="0">
													            <tbody>
														            <tr>
															            <td align="left">
																            <table cellSpacing="0" cellPadding="3" class="tableBody" border="0" width="95%">
																	            <tr>
																		            <td align="center" width="19%" class="tableHeader">Description</td>
																		            <td align="center" width="22%" class="tableHeader">Current</td>
																		            <td align="center" width="32%" class="tableHeader">New</td>
																	            </tr>
																	            <tr>
																		            <td class="tableBody">Font Name</td>
																		            <td class="tableBody"><asp:Label ID="lblbttnfontname" Runat="server"></asp:Label></td>
																		            <td>
																			            <asp:dropdownlist id="dropbttnfont" Runat="server" CssClass="altText" Width="150">
																				            <asp:ListItem Value="">Select the Font</asp:ListItem>
																				            <asp:ListItem Value="Arial">Arial</asp:ListItem>
																				            <asp:ListItem Value="Bookman Old Style">Bookman Old Style</asp:ListItem>
																				            <asp:ListItem Value="Century Gothic">Century Gothic</asp:ListItem>
																				            <asp:ListItem Value="Comic Sans Ms">Comic Sans Ms</asp:ListItem>
																				            <asp:ListItem Value="Courier New">Courier New</asp:ListItem>
																				            <asp:ListItem Value="Georgia">Georgia</asp:ListItem>
																				            <asp:ListItem Value="Microsoft Sans Serif">Microsoft Sans Serif</asp:ListItem>
																				            <asp:ListItem Value="Tahomo">Tahomo</asp:ListItem>
																				            <asp:ListItem Value="Times New Roman">Times New Roman</asp:ListItem>
																				            <asp:ListItem Value="Verdana">Verdana</asp:ListItem>
																			            </asp:dropdownlist>
																		            </td>
																	            </tr>
																	            <tr>
																		            <td class="tableBody">Font Size</td>
																		            <td class="tableBody">
																			            <asp:Label ID="lblbuttonsize" Runat="server"></asp:Label>
																		            </td>
																		            <td>
																			            <asp:dropdownlist id="drpbuttonsize" Runat="server" CssClass="altText" Width="55">
																				            <asp:ListItem Value="">Size</asp:ListItem>
																				            <asp:ListItem Value="8">8</asp:ListItem>
																				            <asp:ListItem Value="9">9</asp:ListItem>
																				            <asp:ListItem Value="10">10</asp:ListItem>
																				            <asp:ListItem Value="11">11</asp:ListItem>
																				            <asp:ListItem Value="12">12</asp:ListItem>
																				            <asp:ListItem Value="14">14</asp:ListItem>
																				            <asp:ListItem Value="16">16</asp:ListItem>
																				            <asp:ListItem Value="18">18</asp:ListItem>
																				            <asp:ListItem Value="20">20</asp:ListItem>
																				            <asp:ListItem Value="22">22</asp:ListItem>
																			            </asp:dropdownlist>
																		            </td>
																	            </tr>
																	            <tr>
																		            <td class="tableBody">Font Color</td>
																		            <td class="tableBody">
																			            <asp:Label ID="lblbttnfontcolor" Runat="server" BorderWidth="1px" BorderColor="#000000" BorderStyle="Solid"></asp:Label>
																			            <asp:Label ID="lblbttnfontcolor1" Runat="server"></asp:Label>
																    	            </td>
																		            <td>
																			            <asp:textbox EnableViewState="True" id="txtbuttonfontcolor" Runat="server" CssClass="altText"></asp:textbox>
																			            <IMG title="Click to select the color" onclick="FnShowPalette(event,'txtbuttonfontcolor')"
																				            height="23" alt="Color" src="../Image/color.jpg" width="27" name="iimgbuttonfontcolor">  <%-- Organization Css Module  --%>
																		            </td>
																	            </tr>
																                <tr>
																	                <td nowrap class="tableBody">Font Background Color</td>
																	                <td nowrap class="tableBody">
																		                <asp:Label ID="lblbuttonbgcolor" Runat="server" BorderWidth="1px" BorderColor="#000000" BorderStyle="Solid"></asp:Label>
																		                <asp:Label ID="lblbuttonbgcolor1" Runat="server"></asp:Label>
																	                </td>
																		            <td>
																			            <asp:textbox EnableViewState="True" id="txtbuttonbgcolor" Runat="server" CssClass="altText"></asp:textbox>
																			            <IMG title="Click to select the color" onclick="FnShowPalette(event,'txtbuttonbgcolor')"
																				            height="23" alt="Color" src="../Image/color.jpg" width="27" name="imgbttnbgcolor"><%-- Organization Css Module  --%>
																		            </td>
																	            </tr>
																            </table>
															            </td>
														            </tr>
    												            </tbody>
												            </table>
											            </td>
										            </tr>
            										
										            <TR>
											            <td align="left" width="20%"><span class="subtitleblueblodtext"> Background Color
														           </span></td>											
											            <td width="1%" height="20">&nbsp;</td>
											            <td align="left" height="20" ></td>
										            </TR>
										            <tr>
											            <td height="20"></td>
											            <td vAlign="top" align="center" colspan="2">
												            <table cellSpacing="0" cellPadding="2" width="100%" align="left" border="0">
													            <tbody>
														            <tr>
															            <td align="left">
																            <table cellSpacing="0" cellPadding="3" class="tableBody" border="0" width="95%">
																	            <tr class="tableHeader">
																		            <td align="center" width="20%" class="tableHeader">Description</td>
																		            <td align="center" width="20%" class="tableHeader">Current</td>
																		            <td align="center" width="60%" class="tableHeader">New</td>
																	            </tr>
																	            <tr>
															                        <td class="tableBody">Background Color</td>
															                        <td>
																                        <table>
																	                        <tr>
																		                        <td class="tableBody">
																			                        <asp:Label ID="lblbodybgcolor1" runat="server" BorderWidth="1px" BorderColor="#000000" BorderStyle="Solid" ></asp:Label>
																			                        <asp:Label ID="lblbodybgcolor" Runat="server" ></asp:Label>
																		                        </td>
																	                        </tr>
																                        </table>
															                        </td>
															                        <td>
																                        <table cellspacing="0" cellpadding="2" width="100%" align="left" border="0">
																	                        <tbody>
																	                            <%--FB 1473--%>
																		                        <tr style="display:none;">
																			                        <td valign="top" align="left"  class="tableBody"  ><asp:radiobutton id="NamedColor" Runat="server" GroupName="ColorSystem" Checked="True"></asp:radiobutton>Color Name</td>
																			                        <td align="left"  class="tableBody">
																			                        <asp:dropdownlist id="NamedCol" Runat="server" CssClass="altText">
																					                        <asp:ListItem Value="">Select the Color</asp:ListItem>
																					                        <asp:ListItem Value="#008000">Green</asp:ListItem>
																					                        <asp:ListItem Value="#00ff00">Lime</asp:ListItem>
																					                        <asp:ListItem Value="#008080">Teal</asp:ListItem>
																					                        <asp:ListItem Value="#00ffff">Aqua</asp:ListItem>
																					                        <asp:ListItem Value="#000080">Navy</asp:ListItem>
																					                        <asp:ListItem Value="#0000ff">Blue</asp:ListItem>
																					                        <asp:ListItem Value="#800080">Purple</asp:ListItem>
																					                        <asp:ListItem Value="#ff00ff">Fuchsia</asp:ListItem>
																					                        <asp:ListItem Value="#800000">Maroon</asp:ListItem>
																					                        <asp:ListItem Value="#ff0000">Red</asp:ListItem>
																					                        <asp:ListItem Value="#808000">Olive</asp:ListItem>
																					                        <asp:ListItem Value="Fuchsia">Fuchsia</asp:ListItem>
																					                        <asp:ListItem Value="Yellow">Yellow</asp:ListItem>
																					                        <asp:ListItem Value="White">White</asp:ListItem>
																					                        <asp:ListItem Value="Silver">Silver</asp:ListItem>
																					                        <asp:ListItem Value="Black">Black</asp:ListItem>
																					                        <asp:ListItem Value="Gray">Gray</asp:ListItem>
																				                        </asp:dropdownlist>
																			                        </td>
																		                        </tr>
																		                        <tr>
																		                            <%-- FB 1473 --%>
																			                        <td vAlign="middle" align="left"  class="tableBody"><asp:radiobutton id="RGBColor" Runat="server" GroupName="ColorSystem" style="display:none;"></asp:radiobutton>RGB Palette</td>
																			                        <td align="left">
																				                        <table>
																					                        <tr>
																						                        <td><asp:textbox EnableViewState="True" id="TxtRGBColor" Runat="server" CssClass="altText" ></asp:textbox></td>
																						                        <td>
																							                        <IMG title="Click to select the color" height="23" alt="Color" onclick="FnShowPalette(event,'TxtRGBColor')" src="../Image/color.jpg"
																								                        width="27" name="rgbimg">  <%-- Organization Css Module  --%>
																						                        </td>
																					                        </tr>
																				                        </table>
																			                        </td>
																		                        </tr>
																	                        </tbody>
																                        </table>
															                        </td>
														                        </tr>
																            </table>
															            </td>
														            </tr>
													            </tbody>
												            </table>
											            </td>
										            </tr>
            										
										            <TR>
											            <td align="left" width="20%"><span class="subtitleblueblodtext">General Font Property
														           </span></td>											
											            <td width="1%" height="20">&nbsp;</td>
											            <td align="left" height="20" ></td>
										            </TR>
            										
										            <tr>
											            <td height="20"></td>
											            <td vAlign="top" align="center" colspan="2">
												            <table id="tablegeneral" cellSpacing="0" cellPadding="2" width="100%" align="left" border="0">
													            <tbody>
														            <tr>
															            <td align="left">
																            <table cellSpacing="0" cellPadding="3" class="tableBody" border="0" width="95%">
																	            <tr>
																		            <td align="center" width="15%" class="tableHeader">Description</td>
																		            <td align="center" width="22%" class="tableHeader">Current</td>
																		            <td align="center" width="32%" class="tableHeader">New</td>
																	            </tr>
																	            <tr>
																		            <td class="tableBody">Font Name</td>
																		            <td class="tableBody">
																			            <asp:Label ID="lblgenname" Runat="server" ></asp:Label>
																		            </td>
																	                <td>
																		                <asp:dropdownlist id="drpgenfont" Runat="server" CssClass="altText" Width="150">
																			                <asp:ListItem Value="">Select the Font</asp:ListItem>
																			                <asp:ListItem Value="Arial">Arial</asp:ListItem>
																			                <asp:ListItem Value="Bookman Old Style">Bookman Old Style</asp:ListItem>
																			                <asp:ListItem Value="Century Gothic">Century Gothic</asp:ListItem>
																			                <asp:ListItem Value="Comic Sans Ms">Comic Sans Ms</asp:ListItem>
																			                <asp:ListItem Value="Courier New">Courier New</asp:ListItem>
																			                <asp:ListItem Value="Georgia">Georgia</asp:ListItem>
																			                <asp:ListItem Value="Microsoft Sans Serif">Microsoft Sans Serif</asp:ListItem>
																			                <asp:ListItem Value="Tahomo">Tahomo</asp:ListItem>
																			                <asp:ListItem Value="Times New Roman">Times New Roman</asp:ListItem>
																			                <asp:ListItem Value="Verdana">Verdana</asp:ListItem>
																		                </asp:dropdownlist>
																	                </td>
																	            </tr>
																	            <tr>
																	                <td class="tableBody">Font Size</td>
																	                <td class="tableBody">
																		                <asp:Label ID="lblgensize" Runat="server"></asp:Label>
																	                </td>
																	                <td>
                											                            <asp:dropdownlist id="drpgensize" Runat="server" CssClass="altText" Width="55">
															                                <asp:ListItem Value="">Size</asp:ListItem>
															                                <asp:ListItem Value="8">8</asp:ListItem>
															                                <asp:ListItem Value="9">9</asp:ListItem>
															                                <asp:ListItem Value="10">10</asp:ListItem>
															                                <asp:ListItem Value="11">11</asp:ListItem>
															                                <asp:ListItem Value="12">12</asp:ListItem>
															                                <asp:ListItem Value="14">14</asp:ListItem>
															                                <asp:ListItem Value="16">16</asp:ListItem>
															                                <asp:ListItem Value="18">18</asp:ListItem>
															                                <asp:ListItem Value="20">20</asp:ListItem>
															                                <asp:ListItem Value="22">22</asp:ListItem>
														                                </asp:dropdownlist>
															                        </td>
																	            </tr>
																	            <tr>
																		            <td class="tableBody">Font Color</td>
																		            <td class="tableBody">
																		                <asp:Label ID="lblgencolor" Runat="server" BorderWidth="1px" BorderColor="#000000" BorderStyle="Solid"></asp:Label>
																		                <asp:Label ID="lblgencolor1" Runat="server"></asp:Label>
																	                </td>
																		            <td>
																			            <asp:textbox EnableViewState="True" id="txtgencolor" Runat="server" CssClass="altText"></asp:textbox>
																			            <IMG title="Click to select the color" onclick="FnShowPalette(event,'txtgencolor')" height="23"
																				            alt="Color" src="../Image/color.jpg" width="27" name="imggen">  <%-- Organization Css Module  --%>
																		            </td>
																	            </tr>
																            </table>
															            </td>
														            </tr>
													            </tbody>
												            </table>
											            </td>
										            </tr>

										            <tr>
											            <td align="left" width="20%"><span class="subtitleblueblodtext">Subtitle Font Property
														           </span></td>											
											            <td width="1%" height="20">&nbsp;</td>
											            <td align="left" height="20" ></td>
										            </tr>
            										
										            <tr>
											            <td height="20"></td>
											            <td vAlign="top" align="center" colspan="2">
												            <table id="Table11" cellSpacing="0" cellPadding="2" width="100%" align="left" border="0">
													            <tbody>
														            <tr>
															            <td align="left">
																            <table cellSpacing="0" cellPadding="3" class="tableBody" border="0" width="95%">
																	            <tr>
																		            <td align="center" width="15%" class="tableHeader">Description</td>
																		            <td align="center" width="22%" class="tableHeader">&nbsp;&nbsp;Current</td>
																		            <td align="center" width="32%" class="tableHeader">New</td>
																	            </tr>
																	            <tr>
																		            <td class="tableBody">Font Name</td>
																		            <td class="tableBody">
																			            <asp:Label ID="lblsubtitlename" Runat="server"></asp:Label></td>
																		            <td>
																			            <asp:dropdownlist id="drpsubtitlefont" Runat="server" CssClass="altText" Width="150">
																				            <asp:ListItem Value="">Select the Font</asp:ListItem>
																				            <asp:ListItem Value="Arial">Arial</asp:ListItem>
																				            <asp:ListItem Value="Bookman Old Style">Bookman Old Style</asp:ListItem>
																				            <asp:ListItem Value="Century Gothic">Century Gothic</asp:ListItem>
																				            <asp:ListItem Value="Comic Sans Ms">Comic Sans Ms</asp:ListItem>
																				            <asp:ListItem Value="Courier New">Courier New</asp:ListItem>
																				            <asp:ListItem Value="Georgia">Georgia</asp:ListItem>
																				            <asp:ListItem Value="Microsoft Sans Serif">Microsoft Sans Serif</asp:ListItem>
																				            <asp:ListItem Value="Tahomo">Tahomo</asp:ListItem>
																				            <asp:ListItem Value="Times New Roman">Times New Roman</asp:ListItem>
																				            <asp:ListItem Value="Verdana">Verdana</asp:ListItem>
											    	    					            </asp:dropdownlist>
																		            </td>
																	            </tr>
																	            <tr>
																		            <td class="tableBody">Font Size</td>
																		            <td class="tableBody">
																			            <asp:Label ID="lblsubtitlesize" Runat="server"></asp:Label>
																		            </td>
																		            <td>
																			            <asp:dropdownlist id="drpsubtitlesize" Runat="server" CssClass="altText" Width="55">
																				            <asp:ListItem Value="">Size</asp:ListItem>
																				            <asp:ListItem Value="8">8</asp:ListItem>
																				            <asp:ListItem Value="9">9</asp:ListItem>
																				            <asp:ListItem Value="10">10</asp:ListItem>
																				            <asp:ListItem Value="11">11</asp:ListItem>
																				            <asp:ListItem Value="12">12</asp:ListItem>
																				            <asp:ListItem Value="14">14</asp:ListItem>
																				            <asp:ListItem Value="16">16</asp:ListItem>
																				            <asp:ListItem Value="18">18</asp:ListItem>
																				            <asp:ListItem Value="20">20</asp:ListItem>
																				            <asp:ListItem Value="22">22</asp:ListItem>
																			            </asp:dropdownlist>
																		            </td>
																	            </tr>
																	            <tr>
																		            <td class="tableBody">Font Color</td>
																		            <td class="tableBody">
																			            <asp:Label ID="lblsubtitlecolor" Runat="server" BorderWidth="1px" BorderColor="#000000" BorderStyle="Solid"></asp:Label>
    																		            <asp:Label ID="lblsubtitlecolor1" Runat="server"></asp:Label>
																		            </td>
																		            <td>
																			            <asp:textbox EnableViewState="True" id="txtsubtitlecolor" Runat="server" CssClass="altText"></asp:textbox>
																			            <IMG title="Click to select the color" onclick="FnShowPalette(event,'txtsubtitlecolor')"
																				            height="23" alt="Color" src="../Image/color.jpg" width="27" name="imgsubtitlecolor"> <%-- Organization Css Module  --%>
																		            </td>
																	            </tr>
																            </table>
															            </td>
														            </tr>
													            </tbody>
												            </table>
											            </td>
										            </tr>

                                                    <tr>
											            <td align="left" width="20%"><span class="subtitleblueblodtext">Table Property
														           </span></td>											
											            <td width="1%" height="20">&nbsp;</td>
											            <td align="left" height="20" ></td>
										            </tr>
            										
										            <tr>
											            <td height="20"></td>
											            <td vAlign="top" align="center" colspan="2">
												            <table id="tbltableheader" cellSpacing="0" cellPadding="2" width="100%" align="left" border="0">
													            <tr>
														            <td align="left">
															            <table cellSpacing="0" cellPadding="3" class="tableBody" border="0" width="95%">
																            <tr class="tableHeader">
																	            <td ALIGN="center" width="19%" class="tableHeader">Description</td>
																	            <td ALIGN="center" width="22%" class="tableHeader">Current</td>
																	            <td ALIGN="center" width="32%" class="tableHeader">New</td>
																            </tr>
																            <tr>
																	            <td class="tableBody">Header Font Color</td>
									        						            <td class="tableBody">
																		            <asp:Label ID="lbltableheaderforecolor" Runat="server" BorderWidth="1px" BorderColor="#000000" BorderStyle="Solid"></asp:Label>
																		            <asp:Label ID="lbltableheaderforecolor1" Runat="server"></asp:Label>
																	            </td>
																	            <td nowrap>
																		            <asp:TextBox EnableViewState="True" id="txttableheaderfontcolor" Runat="server" CssClass="altText"></asp:textbox>
																		            <IMG title="Click to select the color" onclick="FnShowPalette(event,'txttableheaderfontcolor')"
																			            height="23" alt="Color" src="../Image/color.jpg" width="27" name="iimgtablefontcolor"> <%-- Organization Css Module  --%>
																	            </td>
																            </tr>
																            <tr>
																	            <td nowrap class="tableBody">Header Background Color</td>
																	            <td class="tableBody">
																		            <asp:Label ID="lbltableheaderbackcolor" runat="server" BorderWidth="1px" BorderColor="#000000" BorderStyle="Solid"></asp:Label>
																		            <asp:Label ID="lbltableheaderbackcolor1" Runat="server"></asp:Label>
																	            </td>
																	            <td>
																		            <asp:textbox EnableViewState="True" id="txttableheaderbackcolor" Runat="server" CssClass="altText"></asp:textbox>
																		            <IMG title="Click to select the color" onclick="FnShowPalette(event,'txttableheaderbackcolor')"
																			            height="23" alt="Color" src="../Image/color.jpg" width="27" name="imgtblheaderbgcolor"> <%-- Organization Css Module  --%>
																	            </td>
																            </tr>
																            <tr>
																	            <td class="tableBody">Body Font Color</td>
									        						            <td class="tableBody">
																		            <asp:Label ID="lbltablebodyforecolor" runat="server" BorderWidth="1px" BorderColor="#000000" BorderStyle="Solid"></asp:Label>
																		            <asp:Label ID="lbltablebodyforecolor1" Runat="server"></asp:Label>
																	            </td>
																	            <td nowrap>
																		            <asp:textbox EnableViewState="True" id="txttablebodyfontcolor" Runat="server" CssClass="altText"></asp:textbox>
																		            <IMG title="Click to select the color" onclick="FnShowPalette(event,'txttablebodyfontcolor')"
																			            height="23" alt="Color" src="../Image/color.jpg" width="27" name="iimgtablefontcolor1"> <%-- Organization Css Module  --%>
																	            </td>
																            </tr>
																            <tr>
																	            <td nowrap class="tableBody">Body Background Color</td>
																	            <td class="tableBody">
																		            <asp:Label ID="lbltablebodybackcolor" Runat="server" BorderWidth="1px" BorderColor="#000000" BorderStyle="Solid"></asp:Label>
																		            <asp:Label ID="lbltablebodybackcolor1" Runat="server"></asp:Label>
																	            </td>
																	            <td>
																		            <asp:textbox EnableViewState="True" id="txttablebodybackcolor" Runat="server" CssClass="altText"></asp:textbox>
																		            <IMG title="Click to select the color" onclick="FnShowPalette(event,'txttablebodybackcolor')"
																			            height="23" alt="Color" src="../Image/color.jpg" width="27" name="imgtblheaderbgcolor1"><%-- Organization Css Module  --%>
																	            </td>
																            </tr>
    														            </table>
															            </td>
														            </tr>
												            </table>
											            </td>
										            </tr>
            										
										            <tr>
											            <td align="left" width="20%" colspan="3">
											            <span class="subtitleblueblodtext">Error Message Display Property
														           </span></td>											
										            </tr>
            										
										            <tr>
											            <td height="20"></td>
											            <td vAlign="top" align="center" colspan="2">
												            <table id="Table3" cellSpacing="0" cellPadding="2" width="100%" align="left" border="0">
													            <tr>
														            <td align="left">
															            <table cellSpacing="0" cellPadding="3" class="tableBody" border="0" width="95%">
																            <tr class="tableHeader">
																	            <td ALIGN="center" width="19%" class="tableHeader">Description</td>
																	            <td ALIGN="center" width="22%" class="tableHeader">Current</td>
																	            <td ALIGN="center" width="32%" class="tableHeader">New</td>
																            </tr>
																            <tr>
																	            <td class="tableBody">Font Color</td>
									        						            <td class="tableBody">
																		            <asp:Label ID="lblerrormessageforecolor" Runat="server" BorderWidth="1px" BorderColor="#000000" BorderStyle="Solid"></asp:Label>
																		            <asp:Label ID="lblerrormessageforecolor1" Runat="server"></asp:Label>
																	            </td>
																	            <td nowrap>
																		            <asp:textbox EnableViewState="True" id="txterrormessagefontcolor" Runat="server" CssClass="altText"></asp:textbox>
																		            <IMG title="Click to select the color" onclick="FnShowPalette(event,'txterrormessagefontcolor')"
																			            height="23" alt="Color" src="../Image/color.jpg" width="27" name="iimgerrormessagefontcolor"><%-- Organization Css Module  --%>
																	            </td>
																            </tr>
																            <tr visible="false" runat="server" id="ErRow">
																	            <td nowrap class="tableBody">Background Color</td>
																	            <td class="tableBody">
																		            <asp:Label ID="lblerrormessagebackcolor" Runat="server" BorderWidth="1px" BorderColor="#000000" BorderStyle="Solid"></asp:Label>
																		            <asp:Label ID="lblerrormessagebackcolor1" Runat="server"></asp:Label>
																	            </td>
																	            <td>
																		            <asp:textbox EnableViewState="True" id="txterrormessagebackcolor" Runat="server" CssClass="altText"></asp:textbox>
																		            <IMG title="Click to select the color" onclick="FnShowPalette(event,'txterrormessagebackcolor')"
																			            height="23" alt="Color" src="../Image/color.jpg" width="27" name="imgerrormessagebgcolor"> <%-- Organization Css Module  --%>
																	            </td>
																            </tr>
																            <tr>
																	            <td class="tableBody">Font Size</td>
																		            <td class="tableBody">
																			            <asp:Label ID="lblerrormessagefontsize" Runat="server"></asp:Label>
																		            </td>
																		            <td>
																			            <asp:dropdownlist id="drperrormessagefontsize" Runat="server" CssClass="altText" Width="55">
																				            <asp:ListItem Value="">Size</asp:ListItem>
																				            <asp:ListItem Value="8">8</asp:ListItem>
																				            <asp:ListItem Value="9">9</asp:ListItem>
																				            <asp:ListItem Value="10">10</asp:ListItem>
																				            <asp:ListItem Value="11">11</asp:ListItem>
																				            <asp:ListItem Value="12">12</asp:ListItem>
																				            <asp:ListItem Value="14">14</asp:ListItem>
																				            <asp:ListItem Value="16">16</asp:ListItem>
																				            <asp:ListItem Value="18">18</asp:ListItem>
																				            <asp:ListItem Value="20">20</asp:ListItem>
																				            <asp:ListItem Value="22">22</asp:ListItem>
																			            </asp:dropdownlist>
																		            </td>
																            </tr>  													</table>
															            </td>
														            </tr>
												            </table>
											            </td>
										            </tr>
            										
                                                    <tr>
                                                        <td align="left" width="20%" colspan="3"><span class="subtitleblueblodtext">Input Text Property
														           </span></td><%--FB 2579 End--%>										
										            </tr>
            										
										            <tr>
											            <td height="20"></td>
											            <td vAlign="top" align="center" colspan="2">
												            <table id="tblInputTextBox" cellSpacing="0" cellPadding="2" width="100%" align="left" border="0">
													            <tr>
														            <td align="left">
															            <table cellSpacing="0" cellPadding="3" class="tableBody" border="0" width="95%">
																            <tr class="tableHeader">
																	            <td ALIGN="center" width="19%" class="tableHeader">Description</td>
																	            <td ALIGN="center" width="22%" class="tableHeader">Current</td>
																	            <td ALIGN="center" width="32%" class="tableHeader">New</td>
																            </tr>
																            <tr>
																	            <td class="tableBody">Font Color</td>
									        						            <td class="tableBody">
																		            <asp:Label ID="lblinputtextforecolor" Runat="server" BorderWidth="1px" BorderColor="#000000" BorderStyle="Solid"></asp:Label>
																		            <asp:Label ID="lblinputtextforecolor1" Runat="server"></asp:Label>
																	            </td>
																	            <td nowrap>
																		            <asp:textbox EnableViewState="True" id="txtinputtextfontcolor" Runat="server" CssClass="altText"></asp:textbox>
																		            <IMG title="Click to select the color" onclick="FnShowPalette(event,'txtinputtextfontcolor')"
																			            height="23" alt="Color" src="../Image/color.jpg" width="27" name="imginputtextfontcolor"> <%-- Organization Css Module  --%>
																	            </td>
																            </tr>
																            <tr>
																	            <td nowrap class="tableBody">Background Color</td>
																	            <td class="tableBody">
																		            <asp:Label ID="lblinputtextbackcolor" Runat="server" BorderWidth="1px" BorderColor="#000000" BorderStyle="Solid"></asp:Label>
																		            <asp:Label ID="lblinputtextbackcolor1" Runat="server"></asp:Label>
																	            </td>
																	            <td>
																		            <asp:textbox EnableViewState="True" id="txtinputtextbackcolor" Runat="server" CssClass="altText"></asp:textbox>
																		            <IMG title="Click to select the color" onclick="FnShowPalette(event,'txtinputtextbackcolor')"
																			            height="23" alt="Color" src="../Image/color.jpg" width="27" name="imginputtextbgcolor"> <%-- Organization Css Module  --%>
																	            </td>
																            </tr>
																            <tr>
																	            <td nowrap class="tableBody">Border Color</td>
																	            <td class="tableBody">
																		            <asp:Label ID="lblinputtextbordercolor" Runat="server" BorderWidth="1px" BorderColor="#000000" BorderStyle="Solid"></asp:Label>
																		            <asp:Label ID="lblinputtextbordercolor1" Runat="server"></asp:Label>
																	            </td>
																	            <td>
																		            <asp:textbox EnableViewState="True" id="txtinputtextbordercolor" Runat="server" CssClass="altText"></asp:textbox>
																		            <IMG title="Click to select the color" onclick="FnShowPalette(event,'txtinputtextbordercolor')"
																			            height="23" alt="Color" src="../Image/color.jpg" width="27" name="imginputtextbordercolor"> <%-- Organization Css Module  --%>
																	            </td>
																            </tr>
																            <tr>
																	            <td class="tableBody">Font Size</td>
																		            <td class="tableBody">
																			            <asp:Label ID="lblinputtextfontsize" Runat="server"></asp:Label>
																		            </td>
																		            <td>
																			            <asp:dropdownlist id="drpinputtextfontsize" Runat="server" CssClass="altText" Width="55">
																				            <asp:ListItem Value="">Size</asp:ListItem>
																				            <asp:ListItem Value="8">8</asp:ListItem>
																				            <asp:ListItem Value="9">9</asp:ListItem>
																				            <asp:ListItem Value="10">10</asp:ListItem>
																				            <asp:ListItem Value="11">11</asp:ListItem>
																				            <asp:ListItem Value="12">12</asp:ListItem>
																				            <asp:ListItem Value="14">14</asp:ListItem>
																				            <asp:ListItem Value="16">16</asp:ListItem>
																				            <asp:ListItem Value="18">18</asp:ListItem>
																				            <asp:ListItem Value="20">20</asp:ListItem>
																				            <asp:ListItem Value="22">22</asp:ListItem>
																			            </asp:dropdownlist>
																		            </td>
																            </tr>
                                                                            <tr>
																		            <td class="tableBody">Font Name</td>
																		            <td class="tableBody">
																			            <asp:Label ID="lblinputtextfontname" Runat="server" ></asp:Label>
																		            </td>
																	                <td>
																		                <asp:dropdownlist id="drpinputtextfontname" Runat="server" CssClass="altText" Width="150">
																			                <asp:ListItem Value="">Select the Font</asp:ListItem>
																			                <asp:ListItem Value="Arial">Arial</asp:ListItem>
																			                <asp:ListItem Value="Bookman Old Style">Bookman Old Style</asp:ListItem>
																			                <asp:ListItem Value="Century Gothic">Century Gothic</asp:ListItem>
																			                <asp:ListItem Value="Comic Sans Ms">Comic Sans Ms</asp:ListItem>
																			                <asp:ListItem Value="Courier New">Courier New</asp:ListItem>
																			                <asp:ListItem Value="Georgia">Georgia</asp:ListItem>
																			                <asp:ListItem Value="Microsoft Sans Serif">Microsoft Sans Serif</asp:ListItem>
																			                <asp:ListItem Value="Tahomo">Tahomo</asp:ListItem>
																			                <asp:ListItem Value="Times New Roman">Times New Roman</asp:ListItem>
																			                <asp:ListItem Value="Verdana">Verdana</asp:ListItem>
																		                </asp:dropdownlist>
																	                </td>
																	            </tr>											
															                    </table>
															            </td>
														            </tr>
												            </table>
											            </td>
										            </tr>
            										 <%--FB 1633 alignment end--%>
                                                </table>
								             </td>
								         </tr>
								         <%--Code added for FB 1473 - end--%>
									</TBODY>
								</TABLE>
								<BR>
								<TABLE id="Table17" cellSpacing="3" cellPadding="0" width="70%" border="0">
									<TBODY>
										<TR>
										    <%--code added for Soft Edge Button--%>
										    <td>
											    <input type='submit' name='SoftEdgeTest1' style='max-height:0px;max-width:0px;height:0px;width:0px;display:none'/> <%--Edited for FF--%>
										    </td>
											<TD align="center">
												<asp:button id="btnOriginal" Runat="server" CssClass="altShort0BlueButtonFormat" Text="Default Configuration" Visible="false"></asp:button>
											</TD>
											<TD align="center" style="display:none"><!-- FB 2739 --><%--ZD 100263--%>
												<asp:button id="btnPreview" Enabled="false" Runat="server" CssClass="altShort0BlueButtonFormat" Text="Preview"></asp:button>
											</TD>
											<TD align="center">
												<input class="altMedium0BlueButtonFormat" onclick="FnCancel()" type="button" value="Cancel" name="btnCancel">
											</TD>
											<TD align="center"><!-- FB 2739 -->
												<asp:button id="btnSave" Enabled="true" Runat="server"  Text="Submit" Width="150pt"></asp:button> <%--FB 2719--%> <%--FB 2796--%>
											</TD>
										</TR>
									</TBODY>
								</TABLE>
							</CENTER>
						</FORM>
					</TD>
				</TR>
<tr><td height=20></td></tr>
			</TBODY>
		</TABLE>
		<script language="javascript">
			function Browser() 
			{
			var ua, s, i;
			this.isIE    = false;
			this.isNS    = false;
			this.version = null;

			ua = navigator.userAgent;

			s = "MSIE";
			if ((i = ua.indexOf(s)) >= 0) {
				this.isIE = true;
				this.version = parseFloat(ua.substr(i + s.length));
				return;
			}

			s = "Netscape6/";
			if ((i = ua.indexOf(s)) >= 0) {
				this.isNS = true;
				this.version = parseFloat(ua.substr(i + s.length));
				return;
			}

			// Treat any other "Gecko" browser as NS 6.1.

			s = "Gecko";
			if ((i = ua.indexOf(s)) >= 0) {
				this.isNS = true;
				this.version = 6.1;
				return;
			}
			}

			var browser = new Browser();
			FnHideShow();
			//Div Height Patch for Netscape 
			if (browser.isNS) 
			{
				var divmd = document.getElementById("md");
				divmd.style.height = "260px"; 		
			}
			//Div Height Patch for Netscape 


			function FnShowPalette()
			{
				var args = FnShowPalette.arguments;
				var x,y,e;
				e= args[0]; 
				if (!e) e = window.event;
				if (browser.isIE) 
				{
					x = e.clientX + document.documentElement.scrollLeft + document.body.scrollLeft;
					y = e.clientY + document.documentElement.scrollTop + document.body.scrollTop;
				}
				if (browser.isNS) 
				{
					x = e.clientX + window.scrollX;
					y = e.clientY + window.scrollY;
				}
				show_RGBPalette(args[1],x,y);
			}

			function FnHideShow()
			{
				if(document.frmSample.ColorSystem[0].checked == true)
				{
					document.frmSample.NamedCol.disabled = false;
				}
				else
				{
					document.frmSample.NamedCol.disabled = true;
				}
				
				if(document.frmSample.ColorSystem[1].checked == true)
				{
					document.frmSample.TxtRGBColor.disabled = false;
					document.frmSample.rgbimg.disabled = false;					
//					document.frmSample.rgbimg.onclick = function(event)
//					{	FnShowPalette(event,'TxtRGBColor');	}
				}
				else
				{
					document.frmSample.TxtRGBColor.disabled = true;
					document.frmSample.rgbimg.disabled = true;
//					document.frmSample.rgbimg.onclick = function(){}
				}
			}

			function FnValidateForm()
			{
				if(document.frmSample.ColorSystem[0].checked == true)
				{
					if(document.frmSample.NamedCol.value == "0")
					{
						alert("Please select the color name.");
						return false;
					}
				}
				
				if(document.frmSample.ColorSystem[1].checked == true)
				{
					if(document.frmSample.TxtRGBColor.value == "")
					{
						alert("Please select the RGB color.");
						return false;
					}
				}
				return true;	
			}

			function FnConfirm()
			{
                //Code added for FB 1473 - start
			    var hdnvalue = document.getElementById("hdnValue");
                var rThemeCustom = document.getElementById("RThemeCustom");
                
                if(rThemeCustom)
                {
                    if(!rThemeCustom.checked && hdnvalue.value == "")
                    {
                        alert("Please select one theme.");
						return false;
                    }
                }

                //Code added for FB 1473 - end

                if (confirm("All existing settings will be lost")) {

                    return true;
                    DataLoading(1); //ZD 100176
				    
				}
				else
				{
					return false;
				}
			}
			
			function FnPreview()
			{
				window.open('Preview.aspx?styletype=P','Preview','resizable=yes,scrollbars=yes,toolbar=no, width=900,height=600')
			}

			function FnCancel() {
			    DataLoading(1); //ZD 100176
				window.location.replace('OrganisationSettings.aspx');
			}
			function FnOriginal()
			{
				document.frmSample.submit();
			}

		</script>
	</BODY>

</HTML>
<%--code added for Soft Edge button--%>
<script type="text/javascript" src="inc/softedge.js"></script>
<!-- #INCLUDE FILE="inc/mainbottomNET.aspx" -->