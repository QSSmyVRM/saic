<!--ZD 100147 Start-->
<!--/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/-->
<!--ZD 100147 End-->
<%@ Page Language="C#" AutoEventWireup="true" EnableEventValidation="false" Inherits="en_UserMenuController" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>UserMenuController</title>
	<script type="text/javascript" src="script/wincheck.js"></script> <%--FB 3055-URL--%>
    <script type="text/javascript" src="extract.js"></script>

    <script type="text/javascript">        // FB 2790
        // ZD 100263
        if (document.addEventListener != null) {
            document.addEventListener("contextmenu", function(e) {
                e.preventDefault();
            }, false);
        }
    
        var path = '<%=Session["OrgCSSPath"]%>';
        path = path.split('.')[0] + '<%=Session["ThemeType"]%>' + ".css"; // FB 2815
        document.write('<link rel="stylesheet" title="Expedite base styles" type="text/css" href="' + path + '?' + new Date().getTime() + '">');
    </script>

    <script type="text/javascript">

var mmm_num, mma_num, mms_num=new Array(8);
var mmm_int, mma_int, mms_int=new Array(8);
var mms_tn = 10;
var needblock = false;

var orgfrm = ((queryField("n") == "1") ? "opener.document." : "parent.document.");
function SubmenuLayout(cb,idno)
{

	var t = (cb.checked) ? true : false;
	tmpary = (getIdScope (cb.name)).split(",");
	switch (cb.name) {
		case "ma":
			num = getMenuNumber (cb.name);
			for (var i = 1; i <= num; i++) {
				eval("document.frmUsermenucontroller." + cb.name + "_" + i).checked = (!cb.checked)? false : eval("document.frmUsermenucontroller." + cb.name + "_" + i).checked;
			}
		break;
		case "m5":
		    for (i=parseInt(tmpary[0],10); i<=parseInt(tmpary[1],10); i++) 
		    {
	            for (j=1; j<=mms_num[i]; j++) {
	            if(eval("document.frmUsermenucontroller.m"+idlayout(i)+"_"+j)!= null)
	                eval("document.frmUsermenucontroller.m"+idlayout(i)+"_"+j).checked = (!cb.checked)? false : eval("document.frmUsermenucontroller.m"+idlayout(i)+"_"+j).checked;
	            }
	            tmp="m"+idlayout(i);
	            idShow(tmp, mms_num[i]);
            }
         break;   
		default:
		    for (j=1; j<=mms_num[idno]; j++) 
		        eval("document.frmUsermenucontroller."+cb.name+"_"+j).checked = (!cb.checked)? false : eval("document.frmUsermenucontroller."+cb.name+"_"+j).checked;
	        
	        tmp=cb.name;
	        idShow(tmp, mms_num[idno]);
		break;
	}
	document.frmUsermenucontroller.ma_5.checked = true;
	document.frmUsermenucontroller.ma_5.disabled = true;
	setFoodRoom ();

	switch (cb.name) {
		case "m1":
			chklink(1);
		break;
		case "m2":
		case "m3":
		case "m4":
		case "m5":
		case "m6":
		case "m7": //FB 1779
		case "m8": //FB 1779
			chklink(0);
		break;
	}

	if (queryField("n") == "0") {
		getUserMenuOpinion();
	}
}

function AnalyseMenuStr(mstr)
{

	mm_ary = mstr.split("-");
	if (mm_ary.length!=3) {
		alert ("Menu mask is not correct. Please check the database or developer.");
		var isContinue = confirm("Do you want to ignore the error and continue?\nIf click OK, we will help you fix.\nIf click cancel, we will leave it as it be.")
		if (isContinue) {
			ShortkeyLayout(0) 
		} else {
			blockanyinput();
		}
		return;
	}
	mmm_str = mm_ary[0];	mms_str = mm_ary[1];	mma_str = mm_ary[2];

	mmm_ary = mmm_str.split("*");
	if (mmm_ary.length!=2) {
		alert ("Menumask is not correct. Please check the database or developer.")
		var isContinue = confirm("Do you want to ignore the error and continue?\nIf click OK, we will help you fix.\nIf click cancel, we will leave it as it be.")
		if (isContinue) {
			ShortkeyLayout(0) 
		} else {
			blockanyinput();
		}
		return;
	}
	mmm_num = parseInt(mmm_ary[0], 10);	
	mmm_int = parseInt(mmm_ary[1], 10);

	mms_ary = mms_str.split("+");

	for (i=0; i<mms_ary.length; i++) {
		mms_ary[i] = mms_ary[i].split("*");
		if (mms_ary[i].length!=2) {
			alert ("Menumask is not correct. Please check the database or developer.")
			var isContinue = confirm("Do you want to ignore the error and continue?\nIf click OK, we will help you fix.\nIf click cancel, we will leave it as it be.")
			if (isContinue) {
				ShortkeyLayout(0) 
			} else {
				blockanyinput();
			}
			return;
		}
		
		mms_num[(i+1)] = parseInt(mms_ary[i][0], 10);	
		mms_int[(i+1)] = parseInt(mms_ary[i][1], 10);
	}

	mma_ary = mma_str.split("*");
	if (mma_ary.length!=2) {
		alert ("Menumask is not correct. Please check the database or developer.")
		var isContinue = confirm("Do you want to ignore the error and continue?\nIf click OK, we will try to help you fix.\nIf click cancel, we will leave it as it be.")
		if (isContinue) {
			ShortkeyLayout(0) 
		} else {
			blockanyinput();
		}
		return;
	}
	mma_num = parseInt(mma_ary[0], 10);	
	mma_int = parseInt(mma_ary[1], 10);
}
function getMenuNumber (strMname) {
	trgname = strMname.substring(1, strMname.length);
	if ( isNaN(trgname) ) {
		menunum = eval("m" + strMname + "_num");
	} else {
		trgname = parseInt(trgname, 10);
		menunum = mms_num[trgname];
	}
	
	return (menunum);
}
function idShow(srcidstr, rstidnum)
{
	var cb = eval("document.frmUsermenucontroller." + srcidstr);
	var t = (cb.checked) ? '' : 'none';
	for (var i = 1; i <= rstidnum; i++) {		
		if(document.getElementById('id_'+srcidstr+'_'+i) != null)		
		document.getElementById('id_'+srcidstr+'_'+i).style.display = t;
		
		if (!cb.checked) {
		if(document.getElementById(cb.name+'_'+i)!=null)
			document.getElementById(cb.name+'_'+i).checked = false;
		}
	}
}

function getIdScope(idstr)
{
	switch (idstr) {
		case "m1":
			startidno = 0;			endidno = -1;
		break;
		case "m2":
			startidno = 0; 			endidno = 2;
		break;
		case "m3":
			startidno = 0; 			endidno = -1;
		break;
		case "m4":
			startidno = 1; 			endidno = 4;
		break;
		case "m5":
			startidno = 1;			endidno = 8;
		break;
		case "m6":
			startidno = 1;			endidno = -1;
		break;
		//FB 1779
		case "m7":
			startidno = 0;			endidno = -1;
		break;
		case "m8":
			startidno = 0;			endidno = -1;
		break;
		//FB 1779
		case "ma":
			startidno = 1; 			endidno = 6;
		break;
		default:
			startidno = 0; 			endidno = -1;
		break;		
	}
	return(startidno+","+endidno);
}

function SubsubmenuLayout(cb, idno)
{

   	var t = (cb.checked) ? true : false;
	for (j=1; j<=mms_num[idno]; j++) 
		document.getElementById('m'+idlayout(idno)+'_'+j).checked = (!cb.checked)? false : document.getElementById('m'+idlayout(idno)+'_'+j).checked;
	tmp="m"+idlayout(idno);
	idShow(tmp, mms_num[idno]);
	maHide ();
	
	switch (idno) {
		case 4:
		case 18:
			chklink(0);
			break;
	}
	
	if (queryField("n") == "0") {
		getUserMenuOpinion();
	}
}

function blockanyinput()
{
	for (var i=0; i<document.forms[0].length; i++)
		document.frmUsermenucontroller.elements[i].disabled = true;
	needblock = true;
}
function init ()
{
		needblock = false;
		if ( ( (eval(orgfrm + queryField("f") + ".Active").value) == "1" ) || 
		     ( (eval(orgfrm + queryField("f") + ".Locked").value) == "1" ) ) 
		{
			blockanyinput();
			
			//---
			mstr = eval(orgfrm + queryField("f") + ".MenuMask").value;
			mm_ary = mstr.split("-");
			if (mm_ary.length==3) {
				mmm_str = mm_ary[0];
				mmm_ary = mmm_str.split("*");
				if (mmm_ary.length==2) {
					mmm_num = parseInt(mmm_ary[0], 10);	
					mmm_int = parseInt(mmm_ary[1], 10);
				}
			}
	    } else {
	        if(document.forms[0].length > 0)
    		    for (var i=0; i<document.forms[0].length; i++)
	    		    document.frmUsermenucontroller.elements[i].disabled = false;
        }
   		setUserMenuOpinion(eval(orgfrm + queryField("f") + ".MenuMask").value);
   		document.getElementById("m1").disabled = true;
		//FB 1779 start
		 document.getElementById("m7").disabled = true;
		 document.getElementById("m8").disabled = true;
		//FB 1779 end
		 document.getElementById("m6").disabled = true; //FB 1662
}

function setUserMenuOpinion(mm_str)
{

    AnalyseMenuStr(mm_str);
	for (i=1; i<=mmm_num; i++)
		eval("document.frmUsermenucontroller.m" + i).checked = (mmm_int & (1<<(mmm_num-i))) ? true : false;
		
    //FB 1779 start
    document.getElementById("m1").checked = true;
    if(eval(document.getElementById("m7").checked) && eval(document.getElementById("m8").checked))
        document.getElementById("m1").checked = false;
        
    if(eval(document.getElementById("m7").checked) && eval(document.getElementById("m2").checked))
        document.getElementById("m1").checked = false;
    //FB 1779 end    
    
	for (i=1; i<=mms_tn; i++) 
	{
		for (j=1; j<=mms_num[i]; j++) 
		{
		    if(eval("document.frmUsermenucontroller.m"+idlayout(i)+"_"+j) !=null)
			eval("document.frmUsermenucontroller.m"+idlayout(i)+"_"+j).checked = (mms_int[i] & (1<<(mms_num[i]-j) )) ? true : false;
		}
		tmp="m"+idlayout(i);
		idShow(tmp, mms_num[i]);
	}
	document.frmUsermenucontroller.ma.checked = true;
	for (i=1; i<=mma_num; i++) {
		eval("document.frmUsermenucontroller.ma_"+i).checked = (mma_int & (1<<(mma_num-i))) ? true : false;
	}

	SubmenuLayout(document.frmUsermenucontroller.ma);

	maHide ();

}
function maHide ()
{

	document.getElementById('id_ma_1').style.display = "none";
	document.getElementById('id_ma_2').style.display = "none";
	//document.getElementById('id_ma_4').style.display = "none";
	document.getElementById('id_ma_6').style.display = "none";
}

function idlayout(idno)
{
	switch (idno) {
		case 1:
		    idstr = "2";	break;//2
		case 2:
			idstr = "4";	break;//4
		case 3:
			idstr = "5";	break;//8
		case 4:
			idstr = "5_1";	break;//3
		case 5:
			idstr = "5_2";	break;//2
		case 6:
			idstr = "5_3";	break;//8
		case 7:
			idstr = "5_6";	break;//2 //FB 2593 //FB 2885
		case 8:
			idstr = "5_7";	break;//2 //FB 2593 //FB 2885
		case 9:
			idstr = "5_8";	break;//2 //FB 2593 //FB 2885
		case 10:
			idstr = "6";	break;//1	
		default:
			alert("There is error in menu mask, please contact your administrator."); break;
	}
	
	return (idstr);
}
function setFoodRoom()
{
	showFood = parseInt("<%=Session["foodModule"]%>", 10);
	showRoom = parseInt("<%=Session["roomModule"]%>", 10);
	showhk = parseInt("<%=Session["hkModule"]%>", 10);

    //FB 2885 Starts
	if (!showFood) {
		document.getElementById('id_m5_7').style.display = "none";
		document.getElementById('id_m5_7_1').style.display = "none";
		document.getElementById('id_m5_7_2').style.display = "none";
	}

	if (!showRoom) {
		document.getElementById('id_m5_6').style.display = "none";
		document.getElementById('id_m5_6_1').style.display = "none";
		document.getElementById('id_m5_6_2').style.display = "none";
	}
	
	if (!showhk) {
		document.getElementById('id_m5_8').style.display = "none";
		document.getElementById('id_m5_8_1').style.display = "none";
		document.getElementById('id_m5_8_2').style.display = "none";
	}
	//FB 2885 Ends
}

function getUserMenuOpinion()
{	
	mm_str = "";
	mmm_int = 0;	mma_int = 0;
	for (i=1; i<=mms_tn; i++) {
		mms_int[i] = 0;
	}

	for (i=1; i<=mmm_num; i++) {
		mmm_int = (document.getElementById('m'+i).checked) ? ((mmm_int+1)<<1)  : (mmm_int<<1);
	}

	mmm_int = mmm_int>>>1;
	
	for (i=1; i<=mms_tn; i++) {
		for (j=1; j<=mms_num[i]; j++) {
		if(document.getElementById('m'+idlayout(i)+'_'+j) !=null)
			mms_int[i] = (document.getElementById('m'+idlayout(i)+'_'+j).checked) ? ((mms_int[i]+1)<<1)  : (mms_int[i]<<1);
		}
		mms_int[i] = mms_int[i]>>>1;
	}
	
	for (i=1; i<=mma_num; i++) {
		mma_int = (document.getElementById('ma_'+i).checked) ? ((mma_int+1)<<1)  : (mma_int<<1);
	}
	mma_int = mma_int>>>1;
	
	mm_str = mmm_num + "*" + mmm_int + "-"
	for (i=1; i<=mms_tn; i++) {
		if (i==mms_tn) {
			mm_str += mms_num[i] + "*" + mms_int[i]
		} else {
			mm_str += mms_num[i] + "*" + mms_int[i] + "+"
		}
	}
	mm_str += "-" + mma_num + "*" + mma_int


	eval(orgfrm + queryField("f") + ".MenuMask").value = mm_str;
	
	if (eval(orgfrm + queryField("f") + ".sapMValue"))
		eval(orgfrm + queryField("f") + ".sapMValue").value = document.getElementById("m5_5").checked;
	
	if (queryField("f") == "frmManageusertemplate2") {
		eval(orgfrm + queryField("f") + ".RoleID")[0].selected = true;
	}
	
	if (queryField("n") == "0") {
		parent.save() ;
	}
	
	
	window.close ();
}
function chklink(icn)
{
	var s=new Array(6), t=new Array(6);
	if (queryField("n") == "0") {
		getUserMenuOpinion();
	}
}


function chkcheckbox()
{
	if (queryField("n") == "0") {
		getUserMenuOpinion();
	}
}

function deSelect(cb, cid)
{
    if (cb.checked)
        document.getElementById(cid).checked = false;
}

    </script>

    <!-- script finished -->
    <%--CONTENT GOES HERE --%>
</head>
<body style="margin-bottom: 0; margin-left: 0; margin-right: 0; margin-top: 0;">
    <form id="frmUsermenucontroller" method="POST" runat="server">
    <div style="align: left">
        <table border="0" cellpadding="2" width="350">
            <tr>
                <td style="height: 0; width: 20">
                </td>
                <td style="height: 0; width: 30">
                </td>
                <td style="height: 0; width: 300">
                </td>
            </tr>
            <tr id="id_m1">
                <td colspan="3">
                    <asp:CheckBox ID="m1" onclick="javascript:SubmenuLayout(this);" Enabled="false" runat="server" />
                    <!-- onclick="javascript:this.='true';" //FB 1779-->
                    <font class="blackblodtext ">Home</font><%--FB 2579 Start--%>
                </td>
            </tr>
            <tr id="id_m2">
                <td colspan="3">
                    <asp:CheckBox ID="m2" onclick="javascript:SubmenuLayout(this,1);" runat="server" />
                    <!-- onclick="javascript:this.='true';" -->
                    <font class="blackblodtext">Conferences</font>
                </td>
            </tr>
            <tr id="id_m2_1">
                <td colspan="2">
                    &nbsp;&nbsp;
                    <asp:CheckBox ID="m2_1" onclick="javascript:chkcheckbox();" runat="server" />
                    <font size="2" class="blackblodtext">Calender</font>
                </td>
            </tr>
            <tr id="id_m2_2">
                <td colspan="2">
                    &nbsp;&nbsp;
                    <asp:CheckBox ID="m2_2" onclick="javascript:chkcheckbox();" runat="server" />
                    <font size="2" class="blackblodtext">Call Monitor</font>
                </td>
            </tr>
            <tr id="id_m3">
                <td colspan="3">
                    <asp:CheckBox ID="m3" onclick="javascript:SubmenuLayout(this);" runat="server" />
                    <font class="blackblodtext">
                        <%if (Application["Client"].ToString().ToUpper() == "MOJ")
                          {%>New Hearing<%}
                          else
                          { %>New
                        Conference<%}%></span>
                        <%--added for FB 1428 Start--%></font>
                </td>
            </tr>
            <tr id="id_m4">
                <td colspan="3">
                    <asp:CheckBox ID="m4" onclick="javascript:SubmenuLayout(this,2);" runat="server" />
                    <font class="blackblodtext">My Settings</font>
                </td>
            </tr>
            <tr id="id_m4_1">
                <td colspan="2">
                    &nbsp;&nbsp;
                    <asp:CheckBox ID="m4_1" onclick="javascript:chkcheckbox();" runat="server" />
                    <font size="2" class="blackblodtext">Preferences</font>
                </td>
            </tr>
            <tr id="id_m4_2">
                <td colspan="2">
                    &nbsp;&nbsp;
                    <asp:CheckBox ID="m4_2" onclick="javascript:chkcheckbox();" runat="server" />
                    <font size="2" class="blackblodtext">Reports</font>
                </td>
            </tr>
            <tr id="id_m4_3">
                <td colspan="2">
                    &nbsp;&nbsp;
                    <asp:CheckBox ID="m4_3" onclick="javascript:chkcheckbox();" runat="server" />
                    <font size="2" class="blackblodtext">Templates</font>
                </td>
            </tr>
            <tr id="id_m4_4">
                <td colspan="2">
                    &nbsp;&nbsp;
                    <asp:CheckBox ID="m4_4" onclick="javascript:chkcheckbox();" runat="server" />
                    <font size="2" class="blackblodtext">Groups</font>
                </td>
            </tr>
            <tr id="id_m5">
                <td colspan="3">
                    <asp:CheckBox ID="m5" onclick="javascript:SubmenuLayout(this,3);" runat="server" />
                    <font class="blackblodtext">Administration</font>
                </td>
            </tr>
            <tr id="id_m5_1">
                <td colspan="2">
                    &nbsp;&nbsp;
                    <asp:CheckBox ID="m5_1" onclick="javascript:SubsubmenuLayout(this, 4);" runat="server" />
                    <font class="blackblodtext">Hardware</font>
                </td>
            </tr>
            <tr id="id_m5_1_1">
                <td colspan="2">
                    &nbsp;&nbsp;&nbsp;&nbsp;
                    <asp:CheckBox ID="m5_1_1" onclick="javascript:chkcheckbox();" runat="server" />
                    <font class="blackblodtext">Endpoints</font>
                </td>
            </tr>
            <tr id="id_m5_1_2">
                <td colspan="2">
                    &nbsp;&nbsp;&nbsp;&nbsp;
                    <asp:CheckBox ID="m5_1_2" onclick="javascript:chkcheckbox();" runat="server" />
                    <font size="2" class="blackblodtext">Diagnostics</font>
                </td>
            </tr>
            <tr id="id_m5_1_3">
                <td colspan="2">
                    &nbsp;&nbsp;&nbsp;&nbsp;
                    <asp:CheckBox ID="m5_1_3" onclick="javascript:chkcheckbox();" runat="server" />
                    <font size="2" class="blackblodtext">MCU's</font>
                </td>
            </tr>
            <tr id="id_m5_1_4">
                <%--FB 2023--%>
                <td colspan="2">
                    &nbsp;&nbsp;&nbsp;&nbsp;
                    <asp:CheckBox ID="m5_1_4" onclick="javascript:chkcheckbox();" runat="server" />
                    <font size="2" class="blackblodtext">Audio Bridges</font>
                </td>
            </tr>
            <tr id="id_m5_1_5">
                <%--DD2--%>
                <td colspan="2">
                    &nbsp;&nbsp;&nbsp;&nbsp;
                    <asp:CheckBox ID="m5_1_5" onclick="javascript:chkcheckbox();" runat="server" />
                    <font size="2" class="blackblodtext">EM7</font>
                </td>
            </tr>
            <tr id="id_m5_2">
                <td colspan="2">
                    &nbsp;&nbsp;
                    <asp:CheckBox ID="m5_2" onclick="javascript:SubsubmenuLayout(this, 5);" runat="server" />
                    <font size="2" class="blackblodtext">Locations</font>
                </td>
            </tr>
            <tr id="id_m5_2_1">
                <td colspan="2">
                    &nbsp;&nbsp;&nbsp;&nbsp;
                    <asp:CheckBox ID="m5_2_1" onclick="javascript:chkcheckbox();" runat="server" />
                    <font size="2" class="blackblodtext">Rooms</font>
                </td>
            </tr>
            <tr id="id_m5_2_2">
                <td colspan="2">
                    &nbsp;&nbsp;&nbsp;&nbsp;
                    <asp:CheckBox ID="m5_2_2" onclick="javascript:chkcheckbox();" runat="server" />
                    <font size="2" class="blackblodtext">Tiers</font>
                </td>
            </tr>
            <tr id="id_m5_3">
                <td colspan="2">
                    &nbsp;&nbsp;
                    <asp:CheckBox ID="m5_3" onclick="javascript:SubsubmenuLayout(this, 6);" runat="server" />
                    <font size="2" class="blackblodtext">Users</font>
                </td>
            </tr>
            <tr id="id_m5_3_1">
                <td colspan="2">
                    &nbsp;&nbsp;&nbsp;&nbsp;
                    <asp:CheckBox ID="m5_3_1" onclick="javascript:chkcheckbox();" runat="server" />
                    <font size="2" class="blackblodtext">Active Users</font>
                </td>
            </tr>
            <tr id="id_m5_3_2">
                <td colspan="2">
                    &nbsp;&nbsp;&nbsp;&nbsp;
                    <asp:CheckBox ID="m5_3_2" onclick="javascript:chkcheckbox();" runat="server" />
                    <font size="2" class="blackblodtext">Bulk Tool</font>
                </td>
            </tr>
            <tr id="id_m5_3_3">
                <td colspan="2">
                    &nbsp;&nbsp;&nbsp;&nbsp;
                    <asp:CheckBox ID="m5_3_3" onclick="javascript:chkcheckbox();" runat="server" />
                    <font size="2" class="blackblodtext">Departments</font>
                </td>
            </tr>
            <tr id="id_m5_3_4">
                <td colspan="2">
                    &nbsp;&nbsp;&nbsp;&nbsp;
                    <asp:CheckBox ID="m5_3_4" onclick="javascript:chkcheckbox();" runat="server" />
                    <font size="2" class="blackblodtext">Guests</font>
                </td>
            </tr>
            <tr id="id_m5_3_5">
                <td colspan="2">
                    &nbsp;&nbsp;&nbsp;&nbsp;
                    <asp:CheckBox ID="m5_3_5" onclick="javascript:chkcheckbox();" runat="server" ForeColor="green" />
                </td>
            </tr>
            <tr id="id_m5_3_6">
                <td colspan="2">
                    &nbsp;&nbsp;&nbsp;&nbsp;
                    <asp:CheckBox ID="m5_3_6" onclick="javascript:chkcheckbox();" runat="server" />
                    <font size="2" class="blackblodtext">LDAP Directory Import</font>
                </td>
            </tr>
            <tr id="id_m5_3_7">
                <td colspan="2">
                    &nbsp;&nbsp;&nbsp;&nbsp;
                    <asp:CheckBox ID="m5_3_7" onclick="javascript:chkcheckbox();" runat="server" />
                    <font size="2" class="blackblodtext">Roles</font>
                </td>
            </tr>
            <tr id="id_m5_3_8">
                <td colspan="2">
                    &nbsp;&nbsp;&nbsp;&nbsp;
                    <asp:CheckBox ID="m5_3_8" onclick="javascript:chkcheckbox();" runat="server" />
                    <font size="2" class="blackblodtext">Templates</font>
                    <br />
                    <hr width="100%" />
                </td>
            </tr>
            <tr id="id_m5_4">
                <td colspan="2">
                    &nbsp;&nbsp;
                    <asp:CheckBox ID="m5_4" onclick="javascript:chkcheckbox();" runat="server" />
                    <font size="2" class="blackblodtext">Options</font>
                </td>
            </tr>
            <tr id="id_m5_5">
                <td colspan="2">
                    &nbsp;&nbsp;
                    <asp:CheckBox ID="m5_5" onclick="javascript:chkcheckbox();" runat="server" />
                    <font size="2" class="blackblodtext">Settings</font>
                    <br />
                    <hr width="100%" />
                </td>
            </tr>
            <tr id="id_m5_6">
                <td colspan="2">
                    &nbsp;&nbsp;
                    <asp:CheckBox ID="m5_6" onclick="javascript:SubsubmenuLayout(this, 7);" runat="server" />
                    <font size="2" class="blackblodtext">Inventory</font>
                </td>
            </tr>
            <tr id="id_m5_6_1">
                <td colspan="2">
                    &nbsp;&nbsp;&nbsp;&nbsp;
                    <asp:CheckBox ID="m5_6_1" onclick="javascript:chkcheckbox();" runat="server" />
                    <font size="2" class="blackblodtext">Inventory Sets</font>
                </td>
            </tr>
            <tr id="id_m5_6_2">
                <td colspan="2">
                    &nbsp;&nbsp;&nbsp;&nbsp;
                    <asp:CheckBox ID="m5_6_2" onclick="javascript:chkcheckbox();" runat="server" />
                    <font size="2" class="blackblodtext">Work Orders</font>
                </td>
            </tr>
            <tr id="id_m5_7">
                <td colspan="2">
                    &nbsp;&nbsp;
                    <asp:CheckBox ID="m5_7" onclick="javascript:SubsubmenuLayout(this, 8);" runat="server" />
                    <font size="2" class="blackblodtext">Catering</font>
                </td>
            </tr>
            <tr id="id_m5_7_1">
                <td colspan="2">
                    &nbsp;&nbsp;&nbsp;&nbsp;
                    <asp:CheckBox ID="m5_7_1" onclick="javascript:chkcheckbox();" runat="server" />
                    <font size="2" class="blackblodtext">Menus</font>
                </td>
            </tr>
            <tr id="id_m5_7_2">
                <td colspan="2">
                    &nbsp;&nbsp;&nbsp;&nbsp;
                    <asp:CheckBox ID="m5_7_2" onclick="javascript:chkcheckbox();" runat="server" />
                    <font size="2" class="blackblodtext">Work Orders</font>
                </td>
            </tr>
            <tr id="id_m5_8">
                <td colspan="2">
                    &nbsp;&nbsp;
                    <asp:CheckBox ID="m5_8" onclick="javascript:SubsubmenuLayout(this, 9);" runat="server" />
                    <font size="2" class="blackblodtext">Facility</font>
                    <%--FB 2570--%>
                </td>
            </tr>
            <tr id="id_m5_8_1">
                <td colspan="2">
                    &nbsp;&nbsp;&nbsp;&nbsp;
                    <asp:CheckBox ID="m5_8_1" onclick="javascript:chkcheckbox();" runat="server" />
                    <font size="2" class="blackblodtext">Groups</font>
                </td>
            </tr>
            <tr id="id_m5_8_2">
                <td colspan="2">
                    &nbsp;&nbsp;&nbsp;&nbsp;
                    <asp:CheckBox ID="m5_8_2" onclick="javascript:chkcheckbox();" runat="server" />
                    <font size="2" class="blackblodtext">Work Orders</font>
                </td>
            </tr>
            <%--FB 2885 Ends --%>
            <tr id="id_m6">
                <td colspan="3">
                    <asp:CheckBox ID="m6" onclick="javascript:SubmenuLayout(this,10);" runat="server" />
                    <font class="blackblodtext">Site</font>
                </td>
            </tr>
            <tr id="id_m6_1">
                <td colspan="2">
                    &nbsp;&nbsp;
                    <asp:CheckBox ID="m6_1" onclick="javascript:chkcheckbox();" runat="server" />
                    <font size="2" class="blackblodtext">Settings</font>
                </td>
            </tr>
            <tr>
                <td colspan="3">
                    <hr style="width: 10" />
                </td>
            </tr>
            <%--//FB 1779 EXPRESS CONFERENCE Starts--%>
            <tr id="id_m7">
                <td colspan="3">
                    <asp:CheckBox ID="m7" onclick="javascript:SubmenuLayout(this);" runat="server" />
                    <font size="2" class="blackblodtext">Schedule a Call </font>
                </td>
            </tr>
            <tr id="id_m8">
                <td colspan="3">
                    <asp:CheckBox ID="m8" onclick="javascript:SubmenuLayout(this);" runat="server" />
                    <font size="2" class="blackblodtext">View/Edit Reservations</font>
                </td>
            </tr>
            <tr>
                <td colspan="3">
                    <hr style="width: 10" />
                </td>
            </tr>
            <%-- //FB 1779EXPRESS CONFERENCE Ends--%>
            <tr>
                <td colspan="3">
                    <asp:CheckBox ID="ma" onclick="javascript:SubmenuLayout(this);" runat="server" />
                    <font color="green" size="2"><i>Others</i></font>&nbsp;<%--FB 2579--%>
                </td>
            </tr>
            <tr id="id_ma_1">
                <td colspan="2">
                    &nbsp;&nbsp;
                    <asp:CheckBox ID="ma_1" Checked="true" onclick="javascript:chkcheckbox();" runat="server" />
                    <font size="2" color="green"><i>Search</i></font>
                </td>
            </tr>
            <tr id="id_ma_2">
                <td colspan="2">
                    &nbsp;&nbsp;
                    <asp:CheckBox ID="ma_2" Checked="true" onclick="javascript:chkcheckbox();" runat="server" />
                    <font size="2" color="green"><i>About Us</i></font>
                </td>
            </tr>
            <tr id="id_ma_3">
                <td colspan="2">
                    &nbsp;&nbsp;
                    <asp:CheckBox ID="ma_3" Checked="true" onclick="javascript:chkcheckbox();" runat="server" />
                    <font size="2" color="green"><i>Feedback</i></font>
                </td>
            </tr>
            <tr id="id_ma_4">
                <td colspan="2">
                    &nbsp;&nbsp;
                    <asp:CheckBox ID="ma_4" Checked="true" onclick="javascript:chkcheckbox();" runat="server" />
                    <font size="2" color="green"><i>Help</i></font>
                </td>
            </tr>
            <tr id="id_ma_5">
                <td colspan="2">
                    &nbsp;&nbsp;
                    <asp:CheckBox ID="ma_5" Checked="true" Enabled="false" onclick="javascript:chkcheckbox();"
                        runat="server" />
                    <font size="2" color="green"><i>Logoff</i></font>
                </td>
            </tr>
            <tr id="id_ma_6">
                <td colspan="2">
                    &nbsp;&nbsp;
                    <asp:CheckBox ID="ma_6" Checked="true" onclick="javascript:chkcheckbox();" runat="server" />
                    <font size="2" color="green"><i>Company Link</i></font>
                </td>
            </tr>
        </table>
    </div>
    <br>
    <center>
        <table border="0" cellpadding="0" width="98%" id="ButtonTable" runat="server" style="display: none">
            <tr>
                <td align="center">
                    <asp:Button ID="UsermenucontrolSubmit1" Text="Submit" CssClass="altShort2BlueButtonFormat"
                        runat="server" />
                </td>
                <td align="center">
                    <asp:Button ID="UsermenucontrolSubmit2" Text="Cancel" CssClass="altShort2BlueButtonFormat"
                        runat="server" />
                </td>
            </tr>
        </table>
    </center>
    </form>

    <script type="text/javascript">
        init();
    </script>

</body>
</html>

<script type="text/javascript" src="inc/softedge.js"></script>

