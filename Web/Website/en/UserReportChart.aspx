﻿<%--ZD 100147 start--%>
<%--/* Copyright (C) 2013 myVRM - All Rights Reserved	
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.		
*
* You should have received a copy of the myVRM license with	
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/--%><%--ZD 100147 End--%>
<%@ Page Language="C#" AutoEventWireup="true"  Inherits="en_UserReportChart" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<html>
<head>
  <title>myVRM</title>
  <meta name="Description" content="myVRM (Videoconferencing Resource Management) is a revolutionary Web-based software application that manages any video conferencing environment.">
  <meta name="Keywords" content="VRM, myVRM, Videoconferencing Resource Management, video conferencing, video bridges, video endpoints">
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <%--FB 2790 Starts--%>
<script type="text/javascript">
    var path = '<%=Session["OrgCSSPath"]%>';
    path = path.split('.')[0] + '<%=Session["ThemeType"]%>' + ".css"; // FB 2815
    document.write('<link rel="stylesheet" title="Expedite base styles" type="text/css" href="' + path + '?' + new Date().getTime() + '">');
   </script>   
<%--FB 2790 Ends--%>
  <script type="text/javascript" src="script/errorList.js"></script>
 
  
  <script language="javascript">
  <!--
	
	function errorHandler( e, f, l ){
		alert("An error has ocurred in the JavaScript on this page.\nFile: " + f + "\nLine: " + l + "\nError:" + e);
		return true;
	}
	
  //-->
  </script>

</head>
<body bottommargin="0" leftmargin="5" rightmargin="3" topmargin="8" marginheight="0" marginwidth="0">
 <input type="hidden" name="used" id="used" runat="server" />
     <input type="hidden" name="left" id="left" runat="server" />
     <input type="hidden" name="total" id="hdnTotal" runat="server" />
     <input type="hidden" name="login" id="login" runat="server" />
<!--------------------------------- CONTENT START HERE --------------->



  <center>

<!--------------------------------- CONTENT GOES HERE --------------->

    
<br>
<%
    if (Convert.ToInt32(used.Value) >= 0)
    {
%>
<font size=1 face="verdana"> Can't see anything ? Install latest JRE from <a href="http://www.java.com" target="_blank">here</a>
<%
    }
%>

<br>
    
   
<%

if (Convert.ToInt32(Application["appletChart"]) > 2 ){
%>  
	<!-- 
		Purpose		: To display user billing statistics.
		
		Description : This applet displays a pie-chart with 2 params. The params are :
						1. Used Minutes : Number of minutes a user has used already in various confs.
						2. Unused Minutes : Number of unused minutes a user has remaining.
						
		Library Used: http://www.jfree.org/jfreechart 
		
		History		: 09/14/2004 - KM : UserReport applet created.					
	-->
	<applet name="UserReport" code="UserReport.class"  codebase="classes/"     
			archive = "jcommon-0.9.5.jar,jfreechart-0.9.20.jar,gnujaxp.jar"
			width=80% height=200 VIEWASTEXT id=Applet3>
		<param name = "title" value="USER BILLING REPORT">
		<param name = "used_minutes" value="<%=used.Value%>">
		<param name = "unused_minutes" value="<%=left.Value%>">
	</applet>
<%
}
%>

   
<%
if (Convert.ToInt32(Application["appletChart"]) == 1 ) {
%>  
  
<applet name="Multiple Resource Day Schedule" code=netcharts4.apps.NFPiechartApp
			codebase="./classes" archive=netcharts4.jar
			width=80% height=200 id=Applet2>
    <param 
      name=NFParamScript 
      value='
		ChartName                  = "User Report Chart";
		ChartType                  = PIECHART;
		ChartWidth                 = 377;
		ChartHeight                = 299;

		Background                 = (white,NONE,1,null,TILE,black);
		DwellLabel                 = ("",black,"Courier",16,0);
		DwellLabelBox              = (white,SHADOW,3,null,TILE,black);
		SliceData                  = <%=used.Value%>, <%=left.Value%>;
		SliceLabels                = "Used Minutes","Left Minutes";
		Pie3DDepth                 = 7;
		SliceBorder                = (SOLID,1,gray);
		PieAngle                   = 283;
		SliceLabelStyle            = EXTERIOR;
		SliceLabelLine             = (SOLID,1,black);
		SliceLabelContent          = PERCENTAGE,DATA,LABEL;
		SliceLabelContentDelimiter = "\n";
		SliceLabel                 = ("ON",black,"SansSerif",10,0);
		SliceLabelBox              = (white,NONE,2,null,TILE,black);
		ColorTable                 = xb8bc9c,xecf0b9,x999966,x333366,xc3c3e6,x594330,xa0bdc4,x005699,x999966,
									 x213321,xdace98,x284b53,x005699,x271651,xaa0036;
		AntiAlias                  = "ON";
		LegendBox                  = (null,NONE,1,null,TILE,black);
		Legend                     = (OFF);
		LegendLayout               = (VERTICAL,RIGHT,0,0,TOPLEFT,-1);
		HeaderBox                  = (null,NONE,2,null,TILE,black);
		Header                     = ("Total <%=hdnTotal.Value%> minutes",black,"SansSerif",12,0);
		ActiveLabels               = (),();
		SliceColors                = null;
		SlicePos                   = 0.09573610660243298,0,0;
		PieSquare                  = ON;
		LicenseKeys				   = "NetCharts4.0 KEY=EDNYFMYCNDHZYNJEDZXFDPGJAAGJBEZK";
 	 '>
    </applet>
	   
<%
}
%>

<%
     if (Convert.ToInt32(Application["appletChart"]) == 2)    
     {
	    if(Convert.ToInt32(used.Value) < 0 )
        {
            used.Value = Convert.ToString(System.Math.Abs(Convert.ToInt32(used.Value)));
            Int32 result = Convert.ToInt32(left.Value) - Convert.ToInt32(used.Value);
		
	%>
	
	<font style="font-size: 13pt; color: black; font-familiy: Verdana; font-weight: bold">
	<%=login.Value%>'s Time Usage Chart
	</font>
	
	<br><br>
	
	
	<font style="font-size: 12pt; color: red; font-familiy: Verdana; font-weight: bold">
	  You have exceeded the allocated minutes.<br><br>
	  total allocated minutes = <%=result%><br>
      over-usage minutes = <%=used.Value%>
	</b></font>	
	
	<br><br><br>
	<%
	
     } else {
            
	
	%>
	<APPLET width=450 height=220 CODE="piechart.class" codebase="classes/" VIEWASTEXT>
	  <PARAM NAME="title"  VALUE="<%=login.Value%>'s Time Usage Chart">
	  <PARAM NAME="depth"  VALUE="20">
	  <PARAM NAME="radius"  VALUE="180">
	  <PARAM NAME="values" VALUE="<%=used.Value%>@@blue@@Used Minutes,<%=left.Value%>@@darkgreen@@Unused Minutes" />
	</APPLET>
<%
	}
    }   
    
%>

   <br><br>
 <table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr> 
        <td align="center">
         <%--code added for Soft Edge button--%>                    
            <input type='submit' name='SoftEdgeTest' style='max-height:0px;max-width:0px;height:0px;width:0px;display:none'/><%--edited for FF--%>
          <input type="submit" onfocus="this.blur()" name="Submit" value="Close Window" onClick="window.close()" class="altBlueButtonFormat">
        </td>
      </tr>
    </table>

  </center>
	 
</body>

</html>

