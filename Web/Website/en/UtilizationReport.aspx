<%--ZD 100147 start--%>
<%--/* Copyright (C) 2013 myVRM - All Rights Reserved	
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.		
*
* You should have received a copy of the myVRM license with	
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/--%><%--ZD 100147 End--%>
<%@ Page Language="C#" AutoEventWireup="true"  Inherits="myVRMNet.UtilizationReport" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<!-- #INCLUDE FILE="inc/maintopNET.aspx"-->

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>
    <link href="Css/myVRMStyle.css" type="text/css" rel="stylesheet" />
    
    <script type="text/javascript" src="script/lib.js"></script> <%--FB 2404--%>
    <script type="text/javascript" src="script/cal-flat.js"></script>
    <script type="text/javascript" src="script/calview.js"></script>
    <script type="text/javascript" src="lang/calendar-en.js"></script>
    <script type="text/javascript" src="script/calendar-setup.js"></script>
    <script type="text/javascript" src="script/calendar-flat-setup.js"></script>
    <link rel="stylesheet" type="text/css" media="all" href="css/calendar-blue.css" />
    

    <script type="text/javascript">
    
        function fnShowRooms() // FB 2354
        {
           var _cntrl = document.getElementById('<%=RoomDiv.ClientID%>');
           if(_cntrl.style.display == 'none')
               {            
                   _cntrl.style.display = 'block';
                   document.getElementById("imgIE").style.visibility = "visible";
               }
           else
               {
                   _cntrl.style.display = 'none';
                   document.getElementById("imgIE").style.visibility = "hidden";
               }
        }
        
        function fnSelectAll()
        {
        
            var args = fnSelectAll.arguments;
            var rooms = '';
            if(args[0] != null)
            {
                if(args[1] != null)
                {                
                    var this_ = document.getElementById(args[1]);                   
                   
                    fnAssignChecked(this_,args[0].checked);
                }
            }
        }
        
        function fnAssignChecked(this_,checkValue)
        {
            
            if(this_ != null)
            {
                 var checkBoxArray = this_.getElementsByTagName('input');
                 
                 for (var i=0; i<checkBoxArray.length; i++) 
                 { 
                     var checkBoxRef = checkBoxArray[i];
                     
                     checkBoxRef.checked = checkValue;                                 
                 }


            }
        }
        
        function fnDeselectAll()
        {
            var args = fnDeselectAll.arguments;
            
            if(args[0] != null)
            {
                if(!args[0].checked)
                {
                    if(args[1] != null)
                    {
                        var allCheck = document.getElementById(args[1])
                        if(allCheck)
                        {
                            if(allCheck.checked)
                                allCheck.checked = false;
                        }
                    }
                }        
            }
        }
        
        function fnAssignValue()
        {
            var _rooms = document.getElementById('<%=txtRooms.ClientID%>');
            var _hdnrooms = document.getElementById('<%=hdnRoomIDs.ClientID%>');
          
            _rooms.value = '';
            _hdnrooms.value = '';
            
            var args = fnAssignValue.arguments;
            var rooms = '';
           
            if(args[0] != null)
            {
                var this_ = document.getElementById(args[0])
                if(this_)
                {
                    var checkBoxArray = this_.getElementsByTagName('input');
                    var checkedValues = '';
               
                    for (var i=0; i<checkBoxArray.length; i++) 
                    { 
                         var checkBoxRef = checkBoxArray[i];
                         if(checkBoxRef.checked)
                         {
                                
                            var labelArray = checkBoxRef.parentElement.getElementsByTagName('label');
                
                            if ( labelArray.length > 0 )
                            {
                                if (rooms.length > 0 )
                                    rooms += ', ';

                                rooms += labelArray[0].innerHTML;
                             }
                         }                            
                    }
                    
                    if(rooms != '')
                    {
                         _rooms.value = '';
                         _hdnrooms.value = '';
                        _rooms.value = rooms;
                    }   
                }
            }
            
        }
        
        function fnAssignDefault()
        {            
       
                
            if(document.form1.chkSelectall)
            {
                document.form1.chkSelectall.checked = true;
                fnSelectAll(document.form1.chkSelectall,'<%=cblRoom.ClientID%>');
                fnAssignValue('<%=cblRoom.ClientID%>');
            }
        }
        
        function fnValidate()
        {
            
             // regular expression to match required time format
           
            var msg;     
            var startDate;
            var endDate;
           
            if(document.form1.drpEntitiyCodeRange.value == '2')
            {
                if(document.form1.drpEntity.value == '')
                {
                    alert("Please select an Entity Code.");
                    return false;
                }
            }
           
            if(document.form1.txtStartDate.value != '' && document.form1.txtEndDate.value != '')
            {
               msg = fnCheck(document.form1.txtStartDate.value,"start");
                 
               if(msg != "")
               {
                    if(msg != "alert")
                        alert(msg);
                    document.form1.txtStartDate.focus();
                    return false;
               }
               
               msg = "";
               msg = fnCheck(document.form1.txtEndDate.value,"end");
               if(msg != "")
               {
                    if(msg != "alert")
                        alert(msg);
                    document.form1.txtEndDate.focus();
                    return false;
               }
            }
            else
            {
              //FB 2404 - Starts
                if(document.form1.txtStartDate.value != "" && document.form1.txtEndDate.value == "")
                {
                    alert("Please enter the End Date"); 
                    document.form1.txtEndDate.focus();
                    return false;
                }
                else if(document.form1.txtEndDate.value != "" && document.form1.txtStartDate.value == "")
                {
                    alert("Please enter the Start Date");
                    document.form1.txtStartDate.focus();
                    return false;
                }
            }

            //textValue = document.form1.txtStartDate.value;
            //index = textValue.indexOf(" ");
            //startDate = textValue.substring(0,index);
            //startTime = textValue.substring(index+1,textValue.length);
            //textValue = document.form1.txtEndDate.value;
            //index = textValue.indexOf(" ");
            //endDate = textValue.substring(0,index);
            //endTime = textValue.substring(index+1,textValue.length);
                
            startDate = document.form1.txtStartDate.value;
            endDate = document.form1.txtEndDate.value;
                
            var compareVal = fnCompareDate(startDate,endDate)
            
            if(compareVal == "L")
            {
                alert("Start Date should be lesser than End Date.");
                return false;
            }
            return true;
            
            /* else
            {
                if(compareVal == "E")
                {
                    var start = startTime.split(':');
                    var end = endTime.split(':');
                    
                    if(parseInt(start[0]) >  parseInt(end[0]))
                    {
                        alert("Start Date/Time should be lesser than End Date/tTime.");
                        return false;
                    }
                }
            }*/
          //FB 2404 - End
                       
        }
        
        //FB 2404 - Starts
        function fnCheck()
        {
            var args = fnCheck.arguments;
            var date;
            //var re = /^\d{1,2}:\d{2}([ap]m)?$/;
            if(args[0] != null)
                textValue = args[0];
            
            if(args[0] != null)
                date = args[0];
                
            if(!IsValidDate(date))
            {
                return "alert";                 
            }
            return "";
            
            /*if(textValue.indexOf(" ") > 0)
            {
                index = textValue.indexOf(" ");
                date = textValue.substring(0,index);
                if(!IsValidDate(date))
                    return "alert";                 
                startTime = textValue.substring(index+1,textValue.length);
              
                if(!startTime.match(re))
                    return "Please enter the valid time in " + args[1] + " date.";                    
             }
             else
                return "Please enter the " + args[1] + " date in the format {mm/dd/yyyy}" //FB 2404
             */
        }
        //FB 2404 - End
        
    </script>

</head>
<body> 
    <form id="form1" runat="server">
    <input type="hidden" name="hdnRoomIDs" id="hdnRoomIDs" runat="server" />
    <br />
    <table width="100%" align="center" cellpadding="0" cellspacing="0" id="Table8" border="0"><%-- ZD 100156 --%>
        <tr>
            <td valign="top" height="100%" width="100%"><h3>Utilization Reports</h3>
                <table cellpadding="0" width="100%" cellspacing="0" border="0" align="center"><%-- ZD 100156 --%>
                    <tr>
                        <td align="center" style="width: 1168px" colspan="2">
                            <asp:Label ID="errLabel" runat="server" Text="" CssClass="lblError"></asp:Label>
                        </td>
                    </tr>
                     <%-- FB 1750 START --%>
                    <tr>
                        <td valign="middle" align="left">
                            <table cellpadding="0" cellspacing="0" border="0" width="99%" >
                                <tr>
                                    <td valign="middle" align="right">
                                        <a href="GraphicalReport.aspx" title="Back To Reports">Back to Reports</a>
                                    </td>
                                </tr>
                           </table>
                        </td>
                    </tr>
                    <%-- FB 1750 END --%>
                    <tr>
                        <td>
                            <div id="InputParamDiv"  style="background-color:#f3f3f3; border-color:Blue;border-width:1px;border-style:Solid;width:99%;Height:115px;overflow:hidden;text-align:left;">
                                <table cellpadding="0" cellspacing="0" width="100%" >
                                    <tr>
                                        <td width="100%" height="100%">
                                            <table onclick='fnAssignValue("<%=cblRoom.ClientID%>")' cellpadding="5" border="0" width="100%">
                                                <tr>
                                                    <td class="ParamLabelCell" width="25%">
                                                        <span class="blackblodtext">From</span><br />
                                                        <%--FB 2404--%>
                                                        <asp:TextBox ID="txtStartDate" MaxLength="10" runat="server" Width="120" CssClass="altText"></asp:TextBox>
                                                        <img src="image/calendar.gif" border="0" width="20" height="20" id="cal_triggerd"
                                                                style="cursor: pointer; vertical-align: middle" title="Date selector"
                                                                onclick="return showCalendar('<%=txtStartDate.ClientID%>', 'cal_triggerd', 0, '<%=format%>');"
                                                                 alt="" />
                                                        <br />
                                                        {mm/dd/yyyy} <a style="display: none" href="javascript:void(0)" onclick="if(self.gfPop)gfPop.fPopCalendar(<%=txtStartDate.ClientID%>);return false;"
                                                            hidefocus>
                                                            <img style="display: none" name="popcal" align="absMiddle" src='<%=ResolveUrl("~/Javascripts/calbtn.gif")%>'
                                                                width="34" height="22" border="0" alt="">
                                                        </a>
                                                    </td>
                                                    <td class="ParamLabelCell" width="25%">
                                                        <span class="blackblodtext">Until</span><br />
                                                        <%--FB 2404--%>
                                                        <asp:TextBox ID="txtEndDate" MaxLength="10" runat="server" Width="120" CssClass="altText"></asp:TextBox>
                                                        <img src="image/calendar.gif" border="0" width="20" height="20" id="cal_triggerd1"
                                                                style="cursor: pointer; vertical-align: middle" title="Date selector"
                                                                onclick="return showCalendar('<%=txtEndDate.ClientID%>', 'cal_triggerd1', 0, '<%=format%>');"
                                                                 alt="" />
                                                        <br />
                                                        {mm/dd/yyyy} <a style="display: none" href="javascript:void(0)" onclick="if(self.gfPop)gfPop.fPopCalendar(<%=txtEndDate.ClientID%>);return false;"
                                                            hidefocus>
                                                            <img style="display: none" name="popcal" align="absMiddle" src='<%=ResolveUrl("~/Javascripts/calbtn.gif")%>'
                                                                width="34" height="22" border="0" alt="" />
                                                        </a>
                                                    </td>
                                                    <td class="ParamLabelCell">
                                                        <span class="blackblodtext">Time Zone</span><br />
                                                    
                                                        <asp:DropDownList ID="drpTimeZone" runat="server" CssClass="altText" Height="25">
                                                        </asp:DropDownList>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="ParamLabelCell">
                                                        <span class="blackblodtext">Entity Code Range</span><br />
                                                    
                                                        <asp:DropDownList ID="drpEntitiyCodeRange" runat="server" CssClass="altText" >
                                                        </asp:DropDownList>
                                                    </td>
                                                    <td class="ParamLabelCell">
                                                        <span class="blackblodtext">Specific Entity Code</span><br />
                                                    
                                                        <asp:DropDownList ID="drpEntity" runat="server" Width="95" CssClass="altText" />
                                                    </td>
                                                    
                                                    <td class="ParamLabelCell">
                                                        <span class="blackblodtext">Select a Room</span><br />
                                                   
                                                        <asp:TextBox ID="txtRooms" runat="server" Width="210px" CssClass="altText" />
                                                        <img id="imgRoom" src="image/DDImage.gif" onclick="fnShowRooms()" style="cursor: hand;" />
                                                        <img id="imgIE" src="image/info.png" title="To roll-back-up the room list, please click a second time in �drop arrow�" onclick="fnShowRooms()" style="cursor: hand;  vertical-align:top; visibility:hidden" /> <%-- FB 2354 --%>
                                                        <br />
                                                        <div id="RoomDiv" class="RoomDiv" runat="server" style="display: none;">
                                                            &nbsp;<asp:CheckBox ID="chkSelectall" runat="server" Text="Select All" />
                                                            <asp:CheckBoxList ID="cblRoom" runat="server">
                                                            </asp:CheckBoxList>
                                                        </div>
                                                    </td>
                                                </tr>
                                             </table>
                                        </td>
                                        <td width="6px">
                                        </td>
                                        <td class="SubmitButtonCell">
                                            <table id="Table12" align="center">
                                            <tr></tr><tr></tr><tr></tr><tr></tr><%--FB 2292--%>
                                                <tr>
                                                    <td valign="middle">
                                                        <asp:Button ID="btnExcel" runat="server" OnClick="ExportExcel" Text=" Export To Excel " CssClass="altMedium0BlueButtonFormat" Width="120px"/>
                                                    </td>
                                                </tr>
                                                <%--FB 2292 Start--%>
                                                <tr></tr><tr></tr><tr></tr><tr></tr><tr></tr><tr>
                                                <td valign="middle"> 
                                                        <asp:Button ID="btnviewreport" runat="server" OnClick="ViewReport" Text=" View Report " CssClass="altMedium0BlueButtonFormat" Width="130px"/>
                                                    </td>
                                                </tr>
                                                <%--FB 2292 End--%>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </td>
                    </tr>
                    <tr height="4px"><td></td></tr>
                    <tr>
                        <td>
                            <div id="Div1"  style="background-color:#f3f3f3; border-color:Blue;border-width:1px;border-style:Solid;width:99%;Height:360px;overflow:auto;text-align:left;">
                                <br />
                                <table cellpadding="0" cellspacing="0" width="99%" >
                                    <tr>
                                        <td align="center" colspan="2">
                                        <span class="subtitleblueblodtext">
                                                <asp:Label ID="lblDetails" runat="server" Text="Details" Visible="true"></asp:Label></span>
                                            <asp:DataGrid ID="dgRptDt" runat="server" AutoGenerateColumns="false" CellPadding="2" GridLines="Both" AllowSorting="true" BorderColor="Black" BorderStyle="solid" BorderWidth="1" ShowFooter="False" Width="95%" Visible="true" style="border-collapse:collapse">
                                                <SelectedItemStyle  CssClass="tableBody"/> <AlternatingItemStyle CssClass="tableBody" /> <ItemStyle CssClass="tableBody"  />
                                                <HeaderStyle CssClass="tableHeader" Height="30px" /> <EditItemStyle CssClass="tableBody" /> <FooterStyle CssClass="tableBody"/>
                                                <Columns>
                                                    <asp:BoundColumn DataField="Date" ItemStyle-CssClass="tableBody" HeaderStyle-CssClass="tableHeader" HeaderText="Date"></asp:BoundColumn>
                                                    <asp:BoundColumn DataField="Start" ItemStyle-CssClass="tableBody" HeaderStyle-CssClass="tableHeader" HeaderText="Start"></asp:BoundColumn>
                                                    <asp:BoundColumn DataField="End" ItemStyle-Width="15%" ItemStyle-CssClass="tableBody" HeaderStyle-CssClass="tableHeader" HeaderText="End"></asp:BoundColumn>
                                                    <asp:BoundColumn ItemStyle-Width="40%" DataField="Room" ItemStyle-CssClass="tableBody" ItemStyle-HorizontalAlign="Left" HeaderStyle-CssClass="tableHeader" HeaderText="Room" HeaderStyle-Wrap="false" ></asp:BoundColumn>
                                                    <asp:BoundColumn DataField="Conf #" ItemStyle-Width="15%" ItemStyle-CssClass="tableBody"  HeaderStyle-CssClass="tableHeader" HeaderText="Conf #" HeaderStyle-Wrap="false" ></asp:BoundColumn>
                                                    <asp:BoundColumn ItemStyle-Width="35%" DataField="Meeting Title" ItemStyle-CssClass="tableBody" ItemStyle-HorizontalAlign="Left" HeaderStyle-CssClass="tableHeader" HeaderText="Meeting Title" HeaderStyle-Wrap="false" ></asp:BoundColumn>
                                                    <asp:BoundColumn DataField="Entity Code" ItemStyle-CssClass="tableBody"  HeaderStyle-CssClass="tableHeader" HeaderText="Entity Code" HeaderStyle-Wrap="true" ></asp:BoundColumn>
                                                    <asp:BoundColumn DataField="Conference Type" ItemStyle-CssClass="tableBody"  HeaderStyle-CssClass="tableHeader" HeaderText="Conference Type" HeaderStyle-Wrap="true" ></asp:BoundColumn>
                                                    <asp:BoundColumn DataField="Protocol" ItemStyle-CssClass="tableBody"  HeaderStyle-CssClass="tableHeader" HeaderText="Protocol"></asp:BoundColumn>
                                                    <asp:BoundColumn DataField="Status" ItemStyle-CssClass="tableBody"  HeaderStyle-CssClass="tableHeader" HeaderText="Status"></asp:BoundColumn>
                                                </Columns> 
                                            </asp:DataGrid>
                                            <asp:Table runat="server" ID="tblNoDt" Visible="false" Width="90%">
                                                <asp:TableRow CssClass="lblError">
                                                    <asp:TableCell CssClass="lblError" HorizontalAlign="center" >
                                                    <span CssClass="lblError">No details found.</span> 
                                                    </asp:TableCell>
                                                </asp:TableRow>
                                            </asp:Table>
                                        </td>
                                    </tr>
                                </table>
                                <br />
                                <table cellpadding="0" cellspacing="0" width="99%" >
                                    <tr>
                                        <td align="center" colspan="2">
                                        <span class="subtitleblueblodtext">
                                                <asp:Label ID="lblSummary" runat="server" Text="Summary"></asp:Label></span>
                                            <asp:DataGrid ID="dgSummary" runat="server"  CellPadding="2" GridLines="Both" 
                                            BorderColor="Black" BorderStyle="solid" BorderWidth="1" ShowFooter="False" Width="95%" 
                                            Visible="true" style="border-collapse:collapse"> <SelectedItemStyle  CssClass="tableBody"/> 
                                            <AlternatingItemStyle CssClass="tableBody" /> <ItemStyle CssClass="tableBody"/> 
                                            <HeaderStyle CssClass="tableHeader" Height="30px" /> <EditItemStyle CssClass="tableBody" />
                                            <FooterStyle CssClass="tableBody"/>
                                            </asp:DataGrid>
                                            <br />
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </td> 
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    </form>
</body>
</html>
 <!-- #INCLUDE FILE="inc/mainbottomNET.aspx" -->
<%--code added for Soft Edge button--%>
<script type="text/javascript" src="inc/softedge.js"></script>
<script type="text/javascript">

        fnAssignDefault();
</script>