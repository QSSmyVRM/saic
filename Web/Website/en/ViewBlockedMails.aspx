<%--ZD 100147 start--%>
<%--/* Copyright (C) 2013 myVRM - All Rights Reserved	
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.		
*
* You should have received a copy of the myVRM license with	
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/--%><%--ZD 100147 End--%>
<%@ Page Language="C#" AutoEventWireup="true" Inherits="ns_MyVRM.ViewBlockedMails"%>
<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajax" %>


<%if (Request.ServerVariables["HTTP_USER_AGENT"].ToString().ToUpper().Contains("MSIE"))
{%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<%}
else {%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN"> 
<%} %>
<!-- #INCLUDE FILE="inc/maintopNET.aspx" --> 

<link rel="stylesheet" title="Expedite base styles" type="text/css" href="css/border-table.css">
<link rel="stylesheet" title="Expedite base styles" type="text/css" href="css/main-table.css">

<script>

function FnCancel()
{
	window.location.replace('organisationsettings.aspx');
}
</script>
<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server" id="Head1">
    <title>View Blocked Mails</title>
</head>
<body >
    <form id="frmCustomAttribute" runat="server" method="post" >
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
     <input type="hidden" id="helpPage" value="73"/>
     <input type="hidden" id="HdnCustOptID" runat="server" value="<%=customAttrID%>" />
        <table width="100%">
            <tr>
                <td align="center" >
                    <h3>
                       View Blocked Mails
                    </h3>
                </td>
            </tr>
            <tr>
                <td align="center" style="width: 1168px" >
                    <asp:Label ID="errLabel" runat="server" Text="" CssClass="lblError"></asp:Label>
                </td>
            </tr>
			<!--FB 2711 Starts-->
			<tr>
            <td align="right" style="padding-right:25px">        
            &nbsp;        
            <asp:Label ID="lbsel" class="blackblodtext" Text="Release  All"  runat="server"></asp:Label> 
            <asp:CheckBox ID="ChkRel" runat="server" OnClick="javascript:return fnSel(this);"/>&nbsp;&nbsp;&nbsp;<asp:Label ID="lbdel" class="blackblodtext" Text="Delete  All" runat="server"></asp:Label><asp:CheckBox ID="Chkdel" runat="server" OnClick="javascript:return fnDel(this);" /><%-- FB 2711 --%>
               </td>
            </tr>
			<!--FB 2711 Starts-->
            <tr style="display:none;">
                <td align="right">
                    <asp:LinkButton ID="LnkDeleteAll" runat="server" OnClientClick="javascript:return Deleteall();" OnClick="DeleteAllBlockMail" Text="Delete All" CssClass="tabtreeRootNode"></asp:LinkButton>
                </td>
            </tr>
            <tr>
                <td align="center"> 
                    <asp:DataGrid ID="dgBlockEmail" runat="server" AutoGenerateColumns="False" CellPadding="3" GridLines="None" AllowSorting="true" 
                         BorderColor="blue" BorderStyle="solid" BorderWidth="1" ShowFooter="False" Width="95%"  CellSpacing="1"  
                         OnEditCommand="EditBlockEmail" Visible="true" style="border-collapse:separate" AllowPaging="true" PageSize="7"  OnPageIndexChanged="BindBlockEmail"
                         PagerStyle-HorizontalAlign="Right">
                        <SelectedItemStyle  CssClass="tableBody"/>
                          <AlternatingItemStyle CssClass="tableBody" />
                         <ItemStyle CssClass="tableBody"  />
                        <HeaderStyle CssClass="tableHeader" Height="30px" />
                        <EditItemStyle CssClass="tableBody" />
                        <FooterStyle CssClass="tableBody"/>
                        <PagerStyle CssClass="tableBody" Visible = "true" Wrap="False" HorizontalAlign="Right" ForeColor="Blue"  Mode="NumericPages" Position="Bottom" PageButtonCount="7" />
                        <Columns>
                            <asp:BoundColumn DataField="UUID" ItemStyle-CssClass="tableBody" HeaderStyle-CssClass="tableHeader" Visible="false"></asp:BoundColumn>
                            <asp:BoundColumn DataField="Iscalendar" ItemStyle-CssClass="tableBody" HeaderStyle-CssClass="tableHeader" Visible="false"></asp:BoundColumn>
                            <asp:BoundColumn DataField="RowID" ItemStyle-CssClass="tableBody" HeaderStyle-CssClass="tableHeader" HeaderText="S.No"></asp:BoundColumn>
                            <asp:BoundColumn DataField="From" ItemStyle-CssClass="tableBody" HeaderStyle-CssClass="tableHeader" HeaderText="From"></asp:BoundColumn>
                            <asp:BoundColumn DataField="To" ItemStyle-CssClass="tableBody"  HeaderStyle-CssClass="tableHeader" HeaderText="To"></asp:BoundColumn>
                            <asp:BoundColumn DataField="Subject" ItemStyle-CssClass="tableBody" ItemStyle-HorizontalAlign="Left" HeaderStyle-CssClass="tableHeader" HeaderText="Subject"></asp:BoundColumn>
                            <asp:BoundColumn DataField="Message" ItemStyle-CssClass="tableBody" ItemStyle-HorizontalAlign="Left"  HeaderStyle-CssClass="tableHeader" HeaderText="Message"></asp:BoundColumn>
                            <asp:BoundColumn ItemStyle-CssClass="tableBody" ItemStyle-HorizontalAlign="Left"  HeaderStyle-CssClass="tableHeader" HeaderText="iCal"></asp:BoundColumn>
                            <asp:TemplateColumn HeaderText="Edit" HeaderStyle-HorizontalAlign="center">
                              <HeaderStyle CssClass="tableHeader" />
                              <ItemTemplate>                              
                                  <asp:LinkButton runat="server" Text="Edit" ID="btnEdit" CommandName="Edit" ></asp:LinkButton>                              
                               </ItemTemplate>
                            </asp:TemplateColumn>
                             <asp:TemplateColumn HeaderText="Release" HeaderStyle-HorizontalAlign="center">
                              <HeaderStyle CssClass="tableHeader" />
                              <ItemTemplate>                              
                                  <asp:CheckBox runat="server" ID="chkRelease" onClick="javascript:ChangeRelease(this)" />
                               </ItemTemplate>
                            </asp:TemplateColumn>
                             <asp:TemplateColumn HeaderText="Delete" HeaderStyle-HorizontalAlign="center">
                              <HeaderStyle CssClass="tableHeader" />
                              <ItemTemplate>                              
                                  <asp:CheckBox runat="server" ID="chkDelete" onClick="javascript:ChangeDelete(this)" />
                               </ItemTemplate>
                            </asp:TemplateColumn>
                        </Columns>
                    </asp:DataGrid>
                    <asp:Table runat="server" ID="tblNoBlockMail" Visible="false" Width="90%">
                        <asp:TableRow CssClass="lblError">
                            <asp:TableCell CssClass="lblError" HorizontalAlign="center"  >
                               <font class="lblError"> No block e-Mails found.</font>  
                            </asp:TableCell>
                        </asp:TableRow>
                    </asp:Table>                   
                </td>
            </tr>    
            <tr>
                <td >
                    <table width="100%" align="center">
                        <tr>
                            <td align="right">
                                <asp:Button Text="Cancel" runat="server" ID="btnCancel" CssClass="altMedium0BlueButtonFormat" OnClick="RedirectToTargetPage"/>
                            </td>
                            <td width="2%"></td>
                            <td>
                                 <asp:Button Text="Submit" runat="server" Width="100pt" ID="btnSubmit" OnClick="DeleteBlockMail" /> <%--FB 2796--%>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>       
        </table>
    </form>
</body>
</html>
<script type="text/javascript">
    // Release Email
    //FB2711 starts
   function fnSel(drpdown)
     {
         var str = document.getElementById("ChkRel");
         var s = document.getElementById("Chkdel");
         var i = 3;
         if (str.checked)
             s.checked = false;
         var ird;
         while (i<10) {
             ird = "dgBlockEmail_ctl0" + i + "_chkRelease";
             isd = "dgBlockEmail_ctl0" + i + "_chkDelete";
             if (idd = document.getElementById(ird) == null)
                 break;
             var idd = document.getElementById(ird);
             var ifd = document.getElementById(isd);
             if (str.checked)
              {
                 idd.checked = true;
                 ifd.checked = false;
             }
             else 
             {
                 idd.checked = false;
                 
             }
             i++;
         }
            }        

    function fnDel(drpdown) {
        var str = document.getElementById("Chkdel");
        var s = document.getElementById("ChkRel");
        var i = 3;
        var ird;
        if (str.checked)
            s.checked = false;
        while (i < 10) {
            ird = "dgBlockEmail_ctl0" + i + "_chkDelete";
            isd = "dgBlockEmail_ctl0" + i + "_chkRelease";
            if (idd = document.getElementById(ird) == null)
                break;
            var idd = document.getElementById(ird);
            var ifd = document.getElementById(isd);
            if (str.checked) {
                idd.checked = true;
                ifd.checked = false;
            }
            else
                idd.checked = false;
            i++;
        }
//FB2711 ends
    }

function ChangeRelease(drpdwm)
    {
        var str=drpdwm.id;
        var flag=0;//FB 2711
        var ird;//FB 2711
        var i;//FB 2711
        str = str.replace("chkRelease", "chkDelete");
        var dropdwn = document.getElementById(str);
		//FB 2711 Start
		var s = document.getElementById("ChkRel");//FB2711
        var s2s = document.getElementById("Chkdel"); //FB2711
        if (drpdwm.checked) {
            for (i = 3; i < 10; i++) {
                var isd = "dgBlockEmail_ctl0" + i + "_chkRelease";
                var idd = document.getElementById(isd);
                if (idd.checked)
                    flag++;
            }
        }
            
            if (flag == 7)
                s.checked = true;

		//FB 2711 Ends
        if(dropdwn)
        {
            if(drpdwm.checked)
            {
                if(dropdwn.checked)
                {
                    dropdwn.checked = false;
                    s2s.checked = false;//FB2711
                }
            }
            else 
                s.checked = false;//FB2711
            
        }
     
    }
    
    function ChangeDelete(drpdwm)
    {
        var str = drpdwm.id;
        var flag = 0;//FB 2711
        var i;//FB 2711
        str = str.replace("chkDelete", "chkRelease");
        var dropdwn = document.getElementById(str);
		//FB 2711 Starts
		var s = document.getElementById("Chkdel");
        var s2s = document.getElementById("ChkRel");
        if (drpdwm.checked)
         {
             for (i = 3; i < 10; i++)
             {
                var isd = "dgBlockEmail_ctl0" + i + "_chkDelete";
                var idd = document.getElementById(isd);
                if (idd.checked)
                    flag++;
            }
        }
            
            if (flag == 7)
                s.checked = true;
        
        

		//FB 2711 Ends
        if(dropdwn)
        {
            if(drpdwm.checked)
            {
                if(dropdwn.checked)
                {
                    dropdwn.checked = false;
                    s2s.checked = false;//FB2711
                }
            }
                else 
                s.checked = false;//FB2711
            
        }
    }
    
    function Deleteall()
    {
       if(confirm("Are you sur to delete all blocked emails displayed. This operation is irreversible."))
            return true;
       
       return false;
       
    }

// Release Email
</script>
<script type="text/javascript" src="inc/softedge.js"></script>
<!-- #INCLUDE FILE="inc/mainbottomNET.aspx" -->