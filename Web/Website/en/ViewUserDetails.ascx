﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="ns_MyVRM.en_ViewUserDetails" %>
<script language="javascript" type="text/javascript">
    function ClosePopUp()
    {   
        parent.document.getElementById("viewHostDetails").style.display = 'none';
        return false;
    }
</script>

<table cellpadding="0" cellspacing="0" border="0" align="center" width="100%">
	<tr>
		<td align="center">
            <table cellpadding="2" cellspacing="1" style="border-color:Black;border-width:1px;border-style:Solid;"  class="tableBody" align="center" width="50%">
              <tr>
                <td class="subtitleblueblodtext" align="center" colspan="3">
                    User Details<br />
                </td>            
              </tr>
              <tr><%--FB 2579 Start--%>
               <td align="left" style="width:40%" class="blackblodtext">Name </td> 
               <td style="width:1px; vertical-align:top"><b>:</b>&nbsp;</td>
               <td align="left">
                   <asp:Label ID="lblUsrName" runat="server" Text="N/A"></asp:Label>
               </td>
              </tr>
              <tr>
               <td align="left" class="blackblodtext">Email ID </td> 
               <td style="width:1px; vertical-align:top"><b>:</b>&nbsp;</td>
               <td align="left">
                   <asp:Label ID="lblUsrEmail" runat="server"></asp:Label>
               </td>
              </tr>
              <tr>
               <td align="left" class="blackblodtext">AD/LDAP Login </td> 
               <td style="width:1px; vertical-align:top"><b>:</b>&nbsp;</td>
               <td align="left">
                   <asp:Label ID="lblUsrLogin" runat="server" Text="N/A"></asp:Label>
               </td>
              </tr>
              <% if(Session["timezoneDisplay"].ToString() == "1") { %> 
              <tr>
               <td align="left" class="blackblodtext">Time Zone </td> 
               <td style="width:1px; vertical-align:top"><b>:</b>&nbsp;</td>
               <td align="left">
                   <asp:Label ID="lblUsrTimeZone" runat="server"></asp:Label>
               </td>
              </tr>
              <% } else {%>
              <tr>
               <td align="left" class="blackblodtext">Time Zone Display </td>
               <td style="width:1px; vertical-align:top"><b>:</b>&nbsp;</td>
               <td align="left">Off</td>
              </tr>
              <% }%>
              <tr>
               <td align="left" class="blackblodtext">Preferred Language </td> 
               <td style="width:1px; vertical-align:top"><b>:</b>&nbsp;</td>
               <td align="left">
                   <asp:Label ID="lblUsrLang" runat="server"></asp:Label>
               </td>
              </tr>
              <tr>
               <td align="left" class="blackblodtext">Email Language </td> 
               <td style="width:1px; vertical-align:top"><b>:</b>&nbsp;</td>
               <td align="left">
                   <asp:Label ID="lblUsrEmailLang" runat="server" Text="N/A"></asp:Label>
               </td>
              </tr>
              <tr>
               <td align="left" class="blackblodtext">Block Emails </td> 
               <td style="width:1px; vertical-align:top"><b>:</b>&nbsp;</td>
               <td align="left">
                   <asp:Label ID="lblUsrBlockedEmail" runat="server"></asp:Label>
               </td>
              </tr>
              <tr>
               <td align="left" class="blackblodtext">Work </td> 
               <td style="width:1px; vertical-align:top"><b>:</b>&nbsp;</td>
               <td align="left">
                   <asp:Label ID="lblUsrWork" runat="server" Text="N/A"></asp:Label>
               </td>
              </tr>
              <tr>
               <td align="left" class="blackblodtext">Cell </td> 
               <td style="width:1px; vertical-align:top"><b>:</b>&nbsp;</td>
               <td align="left">
                   <asp:Label ID="lblUsrCell" runat="server" Text="N/A"></asp:Label>
               </td>
              </tr><%--FB 2579 End--%>
              <tr>
               <td align="center" colspan="3"><br />
                  <asp:Button ID="BtnUsrDetailClose" Text="Close" CssClass="altMedium0BlueButtonFormat" runat="server" OnClientClick="javascript:return ClosePopUp()"></asp:Button>
               </td>
              </tr>
           </table>
		</td>
	</tr>
</table>
