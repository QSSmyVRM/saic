<%--ZD 100147 start--%>
<%--/* Copyright (C) 2013 myVRM - All Rights Reserved	
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.		
*
* You should have received a copy of the myVRM license with	
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/--%><%--ZD 100147 End--%>
<%@ Page Language="C#" Inherits="ns_Workorder.WorkorderDetails" ValidateRequest="false"  %><%--ZD 100170--%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> <!-- FB 2050 -->
<meta http-equiv="X-UA-Compatible" content="IE=8" /> <!-- FB 2050 --> <%--FB 2779--%>

    <% 
    
if (Request.QueryString["hf"] != null)
if (!Request.QueryString["hf"].ToString().Equals("1"))
   { %>
    <!-- #INCLUDE FILE="inc/maintopNET.aspx" --> 
<% }
   else
   {
%>
    <!-- #INCLUDE FILE="inc/maintop4.aspx" --> 
<%} %>

<% 
    
if (Request.QueryString["hf"] != null)
if (Request.QueryString["hf"].ToString().Equals("1"))
   { %>
        <!-- #INCLUDE FILE="inc/maintop4.aspx" --> 
<%} %>


<script type="text/javascript" src="script/DisplayDIV.js"></script>
<html>
<head>
    <script type="text/javascript">        // FB 2790
        var path = '<%=Session["OrgCSSPath"]%>';
        path = path.split('.')[0] + '<%=Session["ThemeType"]%>' + ".css"; // FB 2815
        document.write('<link rel="stylesheet" title="Expedite base styles" type="text/css" href="' + path + '?' + new Date().getTime() + '">');
    </script>
    <script type="text/javascript" src="script/mousepos.js"></script>
    <script type="text/javascript" src="inc/functions.js"></script> <%--FB 1830--%>
    

<script language="javascript">

    function ShowItems(obj)
    {
        //alert(document.getElementById("tblMenuItems").innerHTML);
        getMouseXY();
        var str = document.getElementById(obj.id.substring(0, obj.id.lastIndexOf("_")) + "_dgMenuItems").innerHTML;
        //Window Dressing
        str = str.replace("<TBODY>", "<TABLE border='0' cellspacing='0' cellpadding='3' class='tableBody' width='200'><TBODY><tr class='tableHeader'><td height='25'><font class='tableHeader'>Items list</font></td></tr>");
        str = str.replace("</TBODY>", "</TBODY></TABLE>");
        document.getElementById("tblMenuItems").style.position = 'absolute';
        document.getElementById("tblMenuItems").style.left = mousedownX - 200;
        document.getElementById("tblMenuItems").style.top = mousedownY;
        document.getElementById("tblMenuItems").style.border = "1";
        document.getElementById("tblMenuItems").style.display="";
        document.getElementById("tblMenuItems").innerHTML = str;
    }
    
    function HideItems()
    {
       document.getElementById("tblMenuItems").style.display="none";
       document.getElementById("tblMenuItems").innerHTML = "";
    }

    function ShowImage(obj)
    {
        //alert(obj.src);
        document.getElementById("myPic").src = obj.src;
        //getMouseXY();
        //alert(document.body.scrollHeight);
        document.getElementById("divPic").style.position = 'fixed';
        document.getElementById("divPic").style.left = mousedownX + 20 + 'px';
        document.getElementById("divPic").style.top = mousedownY + 'px';
        document.getElementById("divPic").style.display = "block";
        document.getElementById("divPic").style.zIndex = 999; //FB 2214
        //alert(obj.style.height + " : " + obj.style.width);
    }

    function HideImage()
    {
        document.getElementById("divPic").style.display="none";
    }
    
    function pdfReport() //fogbugz case 38
    {
        var loc = document.location.href; 
        loc = loc.substring(0,loc.indexOf("ViewWorkorderDetails.aspx"));

        var htmlString = document.getElementById("tblMain").innerHTML;
        //remove export to pdf button from PDF
        toBeRemoved = document.getElementById("tdButtons");
        if (toBeRemoved != null)
            htmlString = htmlString.replace(toBeRemoved.innerHTML, "");
            
        //WO Bug Fix - start
        var txtType = document.getElementById("txtType");
        
        if(txtType != null)
        {
          if(txtType.value == "2")
          {
            toBeRemoved = document.getElementById("tblNCater");
            if (toBeRemoved != null)
                htmlString = htmlString.replace(toBeRemoved.innerHTML, "");
          }
          else
          {
            toBeRemoved = document.getElementById("tblCatering");
            if (toBeRemoved != null)
                htmlString = htmlString.replace(toBeRemoved.innerHTML, "");
          }
        }  
        //WO Bug Fix - end
        //toBeRemoved = document.getElementById("btnEXCEL");
        //if (toBeRemoved != null)
        //    htmlString = htmlString.replace(toBeRemoved.parentElement.outerHTML, "");
        loc = "http://localhost" + loc.replace(loc.substring(0, loc.indexOf("/", 8)), "");   //fogbugz case 386 Saima
        loc = ""; //Image Project
        //replace all doube " with single '        
        //htmlString = htmlString.replace(new RegExp("\"","g"), "'"); //COmmented for FB 1888
        //remove all image source relative paths with the absolute path
        htmlString = htmlString.replace(new RegExp("image/","g"), loc + "image/");
        //FB 1830
        htmlString = htmlString.replace(new RegExp("�","g"),"  �");
        //insert the banner on top of page and style sheet on top.
        //htmlString = "<html><link rel='stylesheet' type='text/css' href='" + loc + "mirror/styles/main.css' /><body><center><table><tr><td><img src='" + loc + "mirror/image/lobbytop1600.jpg' width='100%' height='72'></td></tr></table>" + htmlString + "</center></body></html>";
        if (document.getElementById("tempText") != null)
        {
            document.getElementById("tempText").value = "";
            document.getElementById("tempText").value = htmlString;
        }
    }

    function ExportToExcel()
    {
        var loc = document.location.href; 
        loc = loc.substring(0,loc.indexOf("ViewWorkorderDetails.aspx"));
        var htmlString = document.getElementById("tblMain").innerHTML;
        //remove export to pdf button from PDF
        toBeRemoved = document.getElementById("btnPDF");
        if (toBeRemoved != null)
            htmlString = htmlString.replace(toBeRemoved.parentNode.innerHTML, "");//edited for FF
        toBeRemoved = document.getElementById("btnEXCEL");
        if (toBeRemoved != null)
            htmlString = htmlString.replace(toBeRemoved.parentNode.innerHTML, "");//FB 1981
        
        //WO Bug Fix - start
        var txtType = document.getElementById("txtType");
        
        if(txtType != null)
        {
          if(txtType.value == "2")
          {
            toBeRemoved = document.getElementById("tblNCater");
            if (toBeRemoved != null)
                htmlString = htmlString.replace(toBeRemoved.innerHTML, "");
          }
          else
          {
            toBeRemoved = document.getElementById("tblCatering");
            if (toBeRemoved != null)
                htmlString = htmlString.replace(toBeRemoved.innerHTML, "");
          }
        }  
        //WO Bug Fix - end
        
        //replace all doube " with single '        
        //htmlString = htmlString.replace(new RegExp("\"","g"), "'"); //COmmented for FB 1888
        //remove all image source relative paths with the absolute path
//        htmlString = htmlString.replace(new RegExp("image/","g"), loc + "image/");//Image Project
        htmlString = htmlString.replace(new RegExp("image/","g"), "image/");//Image Project
        //insert the banner on top of page and style sheet on top.
        //FB 1830
        //htmlString = htmlString.replace(/.00/g,".00&nbsp;"); //FB 1686
        htmlString = "<html><body>" + htmlString + "</body></html>";
        htmlString = htmlString.replace(" style='BORDER-RIGHT: black 2px solid; BORDER-TOP: black 2px solid; FONT-WEIGHT: normal; FONT-SIZE: x-small; BORDER-LEFT: black 2px solid; WIDTH: 95%; BORDER-BOTTOM: black 2px solid; FONT-FAMILY: Verdana; BORDER-COLLAPSE: collapse' borderColor=black cellSpacing=0 border=0", " border=1");
        htmlString = htmlString.replace(" style='COLOR: yellow; HEIGHT: 30px; BACKGROUND-COLOR: black'", "");
        //htmlString = htmlString.replace(" style='COLOR: yellow; HEIGHT: 30px; BACKGROUND-COLOR: black'", "");
        
        
        //alert(document.getElementById("dgItems").innerHTML.replace("style=\"COLOR: yellow; HEIGHT: 30px; BACKGROUND-COLOR: BLACK\"", ""));
        if (document.getElementById("tempText") != null)
            document.getElementById("tempText").value = htmlString;
        return false;
    }
</script>
</head>
<body>

    <form id="frmWorkorderDetails" runat="server" method="post" onsubmit="return true">
          <input type="hidden" id="helpPage" value="40">

    <div>
        <table width="100%" id="tblMain">
            <tr>
                <td align="center">
                    <h3>
                        <asp:Label ID="lblType" runat="server" Text="">
                        </asp:Label>Workorder Details
                    </h3>
                </td>
            </tr>
            <tr>
                <td align="center">
                    <asp:Label ID="errLabel" runat="server" Font-Size="Small" ForeColor="Red" Visible="False" Font-Bold="True"></asp:Label>
                    <asp:TextBox ID="txtType" runat="server" Width="0px" BackColor="transparent" BorderColor="transparent" BorderStyle="none" style="display:none;" BorderWidth="0px" ForeColor="transparent" ></asp:TextBox>
                    <asp:DropDownList ID="lstServices" Visible="false" runat="server" DataTextField="Name" DataValueField="ID" OnInit="LoadCateringServices"></asp:DropDownList>  <%--OnLoad="GetServices"--%>                    
                </td>
            </tr>
            <tr>
                <td align="right" id="tdButtons" class="btprint">
                    <asp:ImageButton ID="btnPrint" ImageUrl="Image/printer.gif" OnClientClick="javascript:window.print();return false;" Text="Print" runat="server" ToolTip="Print"></asp:ImageButton> <%--FB 3024--%>
                    <asp:ImageButton ID="btnPDF" runat="server" ImageUrl="Image/pdf.gif" OnClick="ExportToPDF" OnClientClick="javascript:pdfReport();" Text="Export to PDF" ToolTip="Export to PDF" ></asp:ImageButton><%--FB 3024--%>
                    <asp:ImageButton ID="btnEXCEL" runat="server" ImageUrl="Image/xls.gif" OnClientClick="javascript:ExportToExcel()" Text="Export to Excel" OnClick="ExportToEXCEL" ToolTip="Export to Excel"></asp:ImageButton><%----%><%--FB 3024--%>
                </td>
            </tr>
            <tr id="trNonCatering" runat="server">
                <td align="center">
                  <table width="100%" bgcolor="blue" cellspacing="1" cellpadding="0">
                    <tr>
                        <td>
                            <table border="0" cellpadding="0" cellspacing="0" bgcolor="white" width="100%" id="tblNCater">
                                <tr height="30">
                                    <td class="tableHeader" align="left">Name</td>
                                    <td class="tableHeader" align="left">Person-in-charge</td>
                                    <td class="tableHeader" align="left">Rooms</td>
                                    <td class="tableHeader" align="left" visible="false" id="lablRMLO" runat="server">Room Layout</td> <%--FB 2214--%>
                                    <td class="tableHeader" align="left">Start by<br/>Date/Time</td>
                                    <td class="tableHeader" align="left">Completed by<br/>Date/Time</td>
                                    <td class="tableHeader" align="left">Timezone</td>
                                    <td class="tableHeader" align="left" id="trDC" runat="server" visible="false">Delivery<br>Cost (<%=currencyFormat %>)</td> <%--FB 1830--%>
                                    <td class="tableHeader" align="left" id="trSC" runat="server" visible="false">Service<br>Charges (<%=currencyFormat %>)</td> <%--FB 1830--%>
                                    <td class="tableHeader" align="left">Status</td>
                                    <td class="tableHeader" align="left">Comments</td>
                                </tr>
                                <tr height="50">
                                    <td class="tableBody" align="left"><asp:Label ID="lblName" runat="server"></asp:Label></td>
                                    <td class="tableBody" align="left"><asp:Label ID="lblPersonInCharge" runat="server"></asp:Label></td>
                                    <td class="tableBody" align="left"><asp:Label ID="lblRooms" runat="server"></asp:Label></td>
                                    <td class="tableBody" align="left"><asp:Label ID="lblStartBy" runat="server"></asp:Label></td>
                                    <td class="tableBody" align="left"><asp:Label ID="lblCompletedBy" runat="server"></asp:Label></td>
                                    <td class="tableBody" align="left"><asp:Label ID="lblTimezone" runat="server"></asp:Label></td>
                                    <td class="tableBody" align="left" id="trDC1" runat="server" visible="false"><asp:Label ID="lblDeliveryCost" runat="server"></asp:Label></td>
                                    <td class="tableBody" align="left" id="trSC1" runat="server" visible="false"><asp:Label ID="lblServiceCharges" runat="server"></asp:Label></td>
                                    <td class="tableBody" align="left"><asp:Label ID="lblStatus" runat="server"></asp:Label></td>
                                    <td class="tableBody" align="left"><asp:Label ID="lblComments" runat="server"></asp:Label></td>
                                </tr>
                                <tr>
                                    <td colspan="10" height="20"></td>
                                </tr>
                                <tr>
                                    <td colspan="10">
                                        <asp:DataGrid ID="dgItems"  runat="server" AutoGenerateColumns="False" BackColor="#e1e1e1"
                                        GridLines="Vertical" Width="95%" Visible="true" BorderColor="blue" BorderStyle="Solid" BorderWidth="1" CellPadding="5" CellSpacing="0" OnItemDataBound="BindImages">
                                            <%--Window Dressing - Start--%>
                                            <FooterStyle CssClass="tableBody" />
                                            <SelectedItemStyle CssClass="tableBody" Font-Bold="True"/>
                                            <EditItemStyle CssClass="tableBody" />
                                            <AlternatingItemStyle CssClass="tableBody" />
                                            <ItemStyle CssClass="tableBody" />
                                            <HeaderStyle BorderColor="blue" CssClass="tableHeader" BorderStyle="solid" BorderWidth="1" />
                                            <%--Window Dressing - End--%>
                                            <Columns>
                                                <asp:BoundColumn DataField="ID" Visible="False" ><HeaderStyle CssClass="tableHeader" BorderColor="blue" BorderStyle="solid" BorderWidth="1" HorizontalAlign="Left" Height="30px" /></asp:BoundColumn>
                                                <asp:TemplateColumn HeaderText="Name" ItemStyle-CssClass="tableBody" HeaderStyle-CssClass="tableHeader" HeaderStyle-BorderColor="blue" HeaderStyle-BorderStyle="solid" HeaderStyle-BorderWidth="1" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblName1" Text='<%# DataBinder.Eval(Container, "DataItem.Name") %>' runat="server" Visible='<%# !DataBinder.Eval(Container, "DataItem.QuantityRequested").ToString().Equals("0") %>' ></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateColumn>
<%--                                                <asp:BoundColumn DataField="Name" HeaderText="Name"><HeaderStyle  CssClass="tableHeader" Height="30px" /></asp:BoundColumn>
--%>                                                <asp:BoundColumn DataField="SerialNumber" HeaderText="Serial #" ItemStyle-HorizontalAlign="Left"><HeaderStyle BorderColor="blue" BorderStyle="solid" BorderWidth="1" HorizontalAlign="Left" CssClass="tableHeader" Height="30px" /></asp:BoundColumn>
                                                <asp:TemplateColumn HeaderText="Image" ItemStyle-CssClass="tableBody"  HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left">
                                                    <HeaderStyle BorderColor="blue" BorderStyle="solid" BorderWidth="1" CssClass="tableHeader" Height="30px" />
                                                    <ItemTemplate >
                                                       <%--<cc1:ImageControl id="imgItem" Width="30" Height="30"   Runat="server"></cc1:ImageControl>--%>
                                                       <asp:Image ID="imgItem" visible="true" Width="30" Height="30" runat="server" onmouseover="javascript:ShowImage(this)" onmouseout="javascript:HideImage()" />
                                                    </ItemTemplate>
                                                </asp:TemplateColumn> 
                                                <asp:BoundColumn DataField="Price" ItemStyle-CssClass="tableBody"  HeaderText="Price(USD)" HeaderStyle-BorderColor="blue" HeaderStyle-BorderStyle="solid" HeaderStyle-BorderWidth="1" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Right"><HeaderStyle  CssClass="tableHeader" Height="30px" /></asp:BoundColumn>
                                                <asp:BoundColumn DataField="ServiceCharge" Visible="false" ItemStyle-CssClass="tableBody"   HeaderText="Service Charge (USD)" HeaderStyle-BorderColor="blue" HeaderStyle-BorderStyle="solid" HeaderStyle-BorderWidth="1" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Right"><HeaderStyle  CssClass="tableHeader" Height="30px" /></asp:BoundColumn>
                                                <asp:BoundColumn DataField="DeliveryCost" Visible="false" ItemStyle-CssClass="tableBody"   HeaderText="Delivery Cost (USD)" HeaderStyle-BorderColor="blue" HeaderStyle-BorderStyle="solid" HeaderStyle-BorderWidth="1" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Right"><HeaderStyle  CssClass="tableHeader" Height="30px" /></asp:BoundColumn>
                                                <asp:BoundColumn DataField="Comments" ItemStyle-CssClass="tableBody"   HeaderText="Comments" HeaderStyle-HorizontalAlign="Left" HeaderStyle-BorderColor="blue" HeaderStyle-BorderStyle="solid" HeaderStyle-BorderWidth="1" ItemStyle-HorizontalAlign="Left"><HeaderStyle  CssClass="tableHeader" Height="30px" /></asp:BoundColumn>
<%--                                                <asp:BoundColumn DataField="Quantity" HeaderText="Quantity in hand"><HeaderStyle Font-Bold="true" BackColor="black" ForeColor="yellow" Height="30px" /></asp:BoundColumn>
--%>                                                
                                                <asp:TemplateColumn ItemStyle-VerticalAlign="bottom" ItemStyle-CssClass="tableBody"   HeaderText="Requested Quantity" HeaderStyle-BorderColor="blue" HeaderStyle-BorderStyle="solid" HeaderStyle-BorderWidth="1" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Right"><HeaderStyle  CssClass="tableHeader" Height="30px" />
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblReqQuantity" runat="server" Width="30px" Text='<%# DataBinder.Eval(Container, "DataItem.QuantityRequested") %>'></asp:Label>
                                                     </ItemTemplate>
                                                </asp:TemplateColumn>
                                                <asp:BoundColumn DataField="ItemCost" ItemStyle-CssClass="tableBody"   HeaderText="Item Total (USD)" HeaderStyle-BorderColor="blue" HeaderStyle-BorderStyle="solid" HeaderStyle-BorderWidth="1" HeaderStyle-CssClass="tableHeader" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Right"></asp:BoundColumn>
                                                <asp:TemplateColumn HeaderText="Equipment ID" ItemStyle-CssClass="tableBody"   HeaderStyle-HorizontalAlign="Left" HeaderStyle-BorderColor="blue" HeaderStyle-BorderStyle="solid" HeaderStyle-BorderWidth="1" ItemStyle-HorizontalAlign="Left">
                                                    <HeaderStyle  CssClass="tableHeader" Height="30px" />
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblSignature" Text="_______________" runat="server" ></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateColumn>
                                            </Columns>
                                        </asp:DataGrid>
                                    </td>
                                </tr>
                                 <tr>
                                    <td colspan="10" height="20"></td>
                                </tr>
                                <tr>
                                    <td colspan="10" align="center" class="subtitleblueblodtext" runat="server">Total Cost (<%=currencyFormat %>): &nbsp;&nbsp;<asp:Label ID="lblTotalCharges" runat="server"></asp:Label></td>
                                </tr>
                           </table>
                        </td>
                    </tr>
                  </table>
                </td>
            </tr>
            <tr id="trCatering" runat="server">
                <td>
                    <table width="100%" cellspacing="0" cellpadding="2" >
                        <tr> 
                            <td>
                                <table border="0" cellpadding="0" cellspacing="0" bgcolor="white" width="100%" id="tblCatering">                        
                                    <tr class="tableHeader">
                                        <%--Window Dressing Start--%>
                                        <td class="tableHeader">Name</td>
                                        <td class="tableHeader">Room</td>
                                        <td class="tableHeader">Service Type</td>
                                        <td class="tableHeader">Delivery By Date/Time</td>
                                        <td class="tableHeader">Price(USD)</td>     <%-- FB 1686 -- ALL $ changed to USD in this Page--%>
                                        <td class="tableHeader">Comments</td>
                                        <td>
                                            <table width="100%" cellpadding="0" cellpadding="0" border="0">
                                                <tr>
                                                    <td width="75%" class="tableHeader">Items</td>
                                                    <td width="25%" class="tableHeader">Quantity</td>
                                                </tr>
                                        <%--Window Dressing End--%>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr class="tableBody" valign="top"><%--FB 2508--%>
                                        <td width="330px"><asp:Label ID="lblCateringName" runat="server" CssClass="tableBody"></asp:Label></td>
                                        <td width="125px"><asp:Label ID="lblCateringRoomName" runat="server" CssClass="tableBody" ></asp:Label></td>
                                        <td width="125px"><asp:Label ID="lblCateringServiceType" runat="server" CssClass="tableBody"></asp:Label></td>
                                        <td width="125px"><asp:Label ID="lblDeliverByDateTime" runat="server" CssClass="tableBody"></asp:Label></td>
                                        <td width="125px"><asp:Label ID="lblPrice" runat="server" CssClass="tableBody"></asp:Label></td>
                                        <td width="125px"><asp:Label ID="lblPComments" runat="server" CssClass="tableBody"></asp:Label></td>
                                        <td>
                                            <asp:DataGrid ID="dgMenus" BorderWidth="0" BorderStyle="none" CellPadding="2" CellSpacing="0" AutoGenerateColumns="false" ShowHeader="false" runat="server" Width="100%">
                                            <ItemStyle VerticalAlign="top" HorizontalAlign="left" />
                                            <AlternatingItemStyle VerticalAlign="top" HorizontalAlign="left" />
                                                <Columns>
                                                    <asp:BoundColumn DataField="ID" Visible="false"></asp:BoundColumn>
                                                    <asp:TemplateColumn ItemStyle-Width="75%">
                                                        <ItemTemplate>
                                                            <a href="#"><asp:Label ID="lblMenuName" onmouseover="javascript:ShowItems(this)" onmouseout="javascript:HideItems()" Text='<%# DataBinder.Eval(Container, "DataItem.Name") %>' runat="server"></asp:Label></a>
                                                        </ItemTemplate>
                                                    </asp:TemplateColumn>
                                                    <asp:BoundColumn DataField="Quantity" ItemStyle-Width="25%"></asp:BoundColumn>
                                                    <asp:TemplateColumn>
                                                        <ItemTemplate>
                                                            <asp:DataGrid ShowHeader="false" ID="dgMenuItems" Width="100px" runat="server" AutoGenerateColumns="false" >
                                                                <Columns>
                                                                    <asp:BoundColumn DataField="ID" Visible="false"></asp:BoundColumn>
                                                                    <asp:BoundColumn DataField="Name" ItemStyle-CssClass="tableBody"></asp:BoundColumn>
                                                                </Columns>
                                                            </asp:DataGrid>
                                                        </ItemTemplate>
                                                    </asp:TemplateColumn>
                                                </Columns>
                                            </asp:DataGrid>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td height="50">&nbsp;</td>
            </tr>
            <tr>
                <td align="right">
                   <table width="100%" cellpadding="10" cellspacing="4">
                        <tr>
                            <%--Window Dressing Start--%>
                            
                            <td align="right" class="blackblodtext">
                                Pickup Signature
                            </td>
                            <td align="left" >
                                ______________________
                            </td>
                            <td align="right" class="blackblodtext">
                                Date
                            </td>
                            <td align="left" >
                                __________
                            </td>
                            <td align="right" class="blackblodtext">
                                Return Signature
                            </td>
                            <td align="left">
                                ______________________
                            </td>
                            <td align="right" class="blackblodtext">
                                Date
                            </td>
                            <%--Window Dressing End--%>
                            <td align="left">
                                __________
                            </td>
                        </tr>
                   </table>
                </td>
            </tr>
        </table>
        
        <div id="tblMenuItems" style="display:none">
            
        </div>
        <br /><br /><%--WO Bug Fix--%>
        <input type="hidden" id="tempText" runat="server" /> 
        <%--<asp:TextBox ID="tempText" runat="server" TextMode="MultiLine" width="0" Height="0" ForeColor="transparent" BackColor="transparent" BorderColor="transparent"></asp:TextBox>--%>
        <asp:DropDownList ID="lstTimezones" Visible="false" DataTextField="timezoneName" DataValueField="timezoneID" runat="server"></asp:DropDownList>
     </div>
<div id="divPic" style="display:none;  z-index:1;">
    <img src="" name="myPic" id="myPic" width="200" height="200"> <%--Edited for FF--%>
</div>
<script language="javascript">
    if (document.getElementById("txtType").value == "2")
    {
        document.getElementById("trNonCatering").style.display="none";
        document.getElementById("trCatering").style.display="";
    }
    else
    {
        document.getElementById("trNonCatering").style.display="";
        document.getElementById("trCatering").style.display="none";
    }

    //FB 1830
    changeCurrencyFormat("dgItems",'<%=currencyFormat %>');
    changeCurrencyFormat("tblCatering",'<%=currencyFormat %>');

</script>
 </form>
 
</body></html>
<%--code added for Soft Edge button--%>
<script type="text/javascript" src="inc/softedge.js"></script>
    <!-- #INCLUDE FILE="inc/mainbottomNET.aspx" --> 
