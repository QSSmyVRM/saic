<%--ZD 100147 start--%>
<%--/* Copyright (C) 2013 myVRM - All Rights Reserved	
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.		
*
* You should have received a copy of the myVRM license with	
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/--%><%--ZD 100147 End--%>
<%@ Page Language="C#" AutoEventWireup="true" Inherits="en_Wizard" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Wizard</title>
    <%--Window Dressing--%>
  <link rel="stylesheet" title="Expedite base styles" type="text/css" href="Organizations/Orginal/Styles/main.css">  
    <script language="JavaScript">
	
	function openhelp()
	{
//		var url = "help/WebHelp/myvrm-documentation_ver2.0.htm";
	    var url = "help/WebHelp/myVRM_User_and_Administrator_Help.htm";
	
		var w = 700, h = 350; // default sizes
		if (window.screen) {
			w = window.screen.availWidth * 70 / 100;
			h = window.screen.availHeight * 60 / 100;
		}

		window.open(url,'vrmhelp','resizable=yes,scrollbars=yes,toolbar=yes, width='+w+',height='+h);
	}
	
    </script>
</head>
<body>
    <form id="form1" runat="server">   
        <table width="100%"  style="font-size: 10pt; font-family: Tahoma, Verdana, Helvetica" cellpadding="0" cellspacing="0" border="0">
            <tr>
              <td style='text-align:justify;'>
                <li>Use of pop-up blockers may interfere with some features within myVRM's website. We recommend that you disable pop-up blockers while logged into your account.</li>
              </td>
            </tr>
 <%--FB 1985--%>
 <% if (Convert.ToString(Application["client"]).ToUpper() != "DISNEY" ) { %>  
            <tr>
              <td height="5"></td>
            </tr>
            <tr>
              <td>
                <li><a href="javascript: openhelp();">Quick Start Guide</a></li>
              </td>
            </tr>
             <% }%>
        </table>  
    </form>
</body>
</html>
