﻿<%--ZD 100147 start--%>
<%--/* Copyright (C) 2013 myVRM - All Rights Reserved	
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.		
*
* You should have received a copy of the myVRM license with	
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/--%><%--ZD 100147 End--%>
<%@ Page Language="C#" Inherits="ns_WorkingDays.WorkingDays" Buffer="true" %><%--ZD 100170--%>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajax" %>
<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>
<%--Edited for FF--%>
<%if (Request.ServerVariables["HTTP_USER_AGENT"].ToString().ToUpper().Contains("MSIE"))
  {%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<meta http-equiv="X-UA-Compatible" content="IE=8" />
<!-- FB 2050 -->
<%}
  else
  {%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<%} %>
<!-- #INCLUDE FILE="inc/maintopNET.aspx" -->

<script language="javascript" type="text/javascript">
 function OnChanged(sender, args)	
    {	    
        var errLabel = document.getElementById('errLabel');
        if(errLabel)
            errLabel.style.display = 'None';
         
        sender.get_clientStateField().value = sender.saveClientState();	
        var activeIndex = sender.get_activeTabIndex();
        
        document.getElementById('hdnActiveIndex').value = activeIndex;
        
    }	
   
var monName = ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'];

function fnCancel() {
    DataLoading(1); //ZD 100176
	window.location.replace('organisationsettings.aspx');
}

function OpenMonthlist()
{
    var i = -1;
    var tempName = "";
    var ChkName = "";
    for(i = 0; i < 12; i++)    
    {
        ChkName = 'WorkingDaysTabs_MonthlyView_chk' + monName[i];
        tempName = 'WorkingDaysTabs_MonthlyView_Txt' + monName[i];
        if(document.getElementById(ChkName).checked == true) 
        document.getElementById(tempName).value  = document.getElementById("DdlMonth").value;
    }
}
function OpenWeeklist()
{
    var i = 0;
    var tempName = "";
    var ChkName = "";
    for(i = 1; i <= 52; i++)    
    {
        ChkName = 'WorkingDaysTabs_WeeklyView_ChkWeek' + i;
        tempName = 'WorkingDaysTabs_WeeklyView_TxtWeek' + i;
        if(document.getElementById(ChkName).checked == true) 
        document.getElementById(tempName).value  = document.getElementById("DdlWeek").value;
    }
}

function MonthSelectAll(obj)
{
    if(obj.id == "WorkingDaysTabs_MonthlyView_chkMonthSelectAll" ) 
    {
        var i = -1;
        var ChkName = "";
        for(i = 0; i < 12; i++)    
        {
            ChkName = 'WorkingDaysTabs_MonthlyView_chk' + monName[i];
            if(obj.checked)
                document.getElementById(ChkName).checked  = true;
            else
                document.getElementById(ChkName).checked  = false;
        }
	}
}

function WeekSelectAll(obj)
{
    if(obj.id == "WorkingDaysTabs_WeeklyView_ChkWeekSelectAll")
    {
        var i = 0;
        var ChkName = "";
        for(i = 1; i <= 52; i++)    
        {
            ChkName = 'WorkingDaysTabs_WeeklyView_ChkWeek' + i;
            if(obj.checked)
                document.getElementById(ChkName).checked  = true;
            else
                document.getElementById(ChkName).checked  = false;
        }
	}
}

function fnMonthdrop(obj)
{
     var ChkName = "";
     for ( i = 0; i < 12; i++)
     {
       ChkName = 'WorkingDaysTabs_MonthlyView_chk' + monName[i];
       if(document.getElementById(ChkName).checked == true)
       document.getElementById("WorkingDaysTabs_MonthlyView_chkMonthSelectAll").checked = true;
       else
       {
       document.getElementById("WorkingDaysTabs_MonthlyView_chkMonthSelectAll").checked = false;
       break;
       }
     }     
}

function fnWeekdrop(obj)
{
    var ChkName = "";
    for(j=1;j<=52;j++)
    {
        ChkName = 'WorkingDaysTabs_WeeklyView_ChkWeek' + j;
        if(document.getElementById(ChkName).checked == true)
        document.getElementById("WorkingDaysTabs_WeeklyView_ChkWeekSelectAll").checked = true;
       else
       {
        document.getElementById("WorkingDaysTabs_WeeklyView_ChkWeekSelectAll").checked = false;
        break;
       }
    }
}

function fnMonthSubmit()
{           
    if (!Page_ClientValidate())
               return Page_IsValid;
     var monthdays = "dummy";
     var i = -1;
     var tempName = "";
     var ChkName = "";
     var chckBox = false;
     var days = parseInt(document.getElementById("WorkingDaysTabs_MonthlyView_DdlMonth").value);
     var monDays = ['31','29','31','30','31','30','31','31','30','31','30','31'];
     var Name = ['January','Febuary','March','April','May','June','July','August','September','October','November','December'];
     var febDays = parseInt(document.getElementById('hdnFebDays').value);
     for ( i = 0; i < 12; i++)
     {
       ChkName = 'WorkingDaysTabs_MonthlyView_chk' + monName[i];
       if(i==1 && days > febDays)
       {
        if(document.getElementById(ChkName).checked == true)
            {
            alert("Invalid day(s) for the month " +Name[i]);
            return false;
            }
       }
       else if(days > parseInt(monDays[i]))
       {
        if(document.getElementById(ChkName).checked == true)
            {
            alert("Invalid day(s) for the month " + Name[i]);
            return false;
            }
       }
       monthdays += "|";
       if(document.getElementById(ChkName).checked == true)
       {
         monthdays += days;
         chckBox = true;
       }  
       else
         monthdays += "-1";
     }    
     if(chckBox == false)
    {
       alert("Select any Checkbox for further changes.");
       return false;
    }                                   
    document.getElementById("WorkingDaysTabs_MonthlyView_hdnMonthdays").value = monthdays;
}

function fnMonthReset() {
    DataLoading(1); //ZD 100176
    var ChkName = "";
     for ( i = 0; i < 12; i++)
     {
       ChkName = 'WorkingDaysTabs_MonthlyView_chk' + monName[i];
       if(document.getElementById(ChkName).checked == true)
       {
            document.getElementById(ChkName).checked = false;
       }
       document.getElementById("WorkingDaysTabs_MonthlyView_chkMonthSelectAll").checked = false;
      
     }     
}
function fnWeekReset()
{
    var ChkWeekName = "";
    for(j=1;j<=52;j++)
    {
        ChkWeekName = 'WorkingDaysTabs_WeeklyView_ChkWeek' + j;
        if(document.getElementById(ChkWeekName).checked == true)
        {
            document.getElementById(ChkWeekName).checked = false;
        }
        document.getElementById("WorkingDaysTabs_WeeklyView_ChkWeekSelectAll").checked = false;
    }
}

function fnWeekSubmit()
{
    var Weekdays = "dummy";
    var weekname = "";
    var chckBox = false;
    var ChkName = "";
    if (!Page_ClientValidate())
         return Page_IsValid;
   
    for(j=1;j<=52;j++)
    {
        ChkName = 'WorkingDaysTabs_WeeklyView_ChkWeek' + j;
        Weekdays += "|";
        if(document.getElementById(ChkName).checked == true)
        {
          Weekdays +=  document.getElementById("WorkingDaysTabs_WeeklyView_DdlWeek").value;
          chckBox = true;
        }
        else
          Weekdays += "-1";       
    }
    if(chckBox == false)
    {
       alert("Select any Checkbox for further changes.");
       return false;
    }
    document.getElementById("WorkingDaysTabs_WeeklyView_hdnWeekdays").value = Weekdays;
}
//ZD 100176 start
function DataLoading(val) {
    if (val == "1")
        document.getElementById("dataLoadingDIV").innerHTML = "<img border='0' src='image/wait1.gif'>";
    else
        document.getElementById("dataLoadingDIV").innerHTML = "";
}
//ZD 100176 End
</script>

<style type="text/css">
    .txtStyle
    {
        text-align: center;
        background-color: Transparent;
        font-weight: bold;
        border-style: none;
    }
</style>
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Manage Working Day Details</title>

    <script type="text/javascript" src="inc/functions.js"></script>

    <script type="text/javascript" src="script/mousepos.js"></script>

    <script type="text/javascript" src="script/managemcuorder.js"></script>

</head>
<body>
    <form id="frmManageWorkingDays" runat="server" method="post" onsubmit="DataLoading(1);"><%-- ZD 100176--%>
    <asp:ScriptManager ID="WorkingDaysScriptManager" runat="server" LoadScriptsBeforeUI="false">
    </asp:ScriptManager>
    <input type="hidden" runat="server" id="hdnFebDays" />
    <input type="hidden" runat="server" id="hdnActiveIndex" />
    <div>
        <table width="80%" border="0" cellpadding="5" cellspacing="5" align="center">
            <tr>
                <td align="center">
                    <h3>
                        <asp:Label ID="lblHeader" runat="server">Manage Working Days</asp:Label>
                    </h3>
                </td>
            </tr>
            <tr>
                <td align="center">
                    <asp:Label ID="errLabel" runat="server" Text="" CssClass="lblError"></asp:Label>
                     <div id="dataLoadingDIV" align="center"></div><%--ZD 100176--%>
                </td>
            </tr>
            <tr>
                <td>
                    <ajax:TabContainer ID="WorkingDaysTabs" runat="server" OnClientActiveTabChanged="OnChanged">
                        <ajax:TabPanel ID="MonthlyView" runat="server">
                            <HeaderTemplate>
                                <font class="blackblodtext">Configure Month Working Days</font>
                            </HeaderTemplate>
                            <ContentTemplate>
                                <input type="hidden" runat="server" id="hdnSelectYear" />
                                <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                    <tr id="trMonth" runat="server">
                                        <td align="left" runat="server">
                                            <div id="DivMonth">
                                                <table width="100%" border="0" cellpadding="3">
                                                    <tr>
                                                        <td class="blackblodtext">
                                                            Configured Working Days for Year:
                                                            <asp:DropDownList ID="lstYearConfigMonth" runat="server" DataTextField="CurrentYear"
                                                                DataValueField="CurrentYear" OnSelectedIndexChanged="GetMonthDaysforYear" AutoPostBack="true">
                                                            </asp:DropDownList>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="blackblodtext">
                                                            Select All
                                                            <asp:CheckBox ID="chkMonthSelectAll" runat="server" onclick="javascript:MonthSelectAll(this)" />
                                                            &nbsp;&nbsp;&nbsp;&nbsp; Days<span class="reqfldText">*</span> &nbsp;
                                                            <asp:DropDownList ID="DdlMonth" CssClass="altText" runat="server" onchange="javascript:OpenMonthlist()"
                                                                Style="vertical-align: middle;">
                                                                <asp:ListItem Value="10"></asp:ListItem>
                                                                <asp:ListItem Value="11"></asp:ListItem>
                                                                <asp:ListItem Value="12"></asp:ListItem>
                                                                <asp:ListItem Value="13"></asp:ListItem>
                                                                <asp:ListItem Value="14"></asp:ListItem>
                                                                <asp:ListItem Value="15"></asp:ListItem>
                                                                <asp:ListItem Value="16"></asp:ListItem>
                                                                <asp:ListItem Value="17"></asp:ListItem>
                                                                <asp:ListItem Value="18"></asp:ListItem>
                                                                <asp:ListItem Value="19"></asp:ListItem>
                                                                <asp:ListItem Value="20"></asp:ListItem>
                                                                <asp:ListItem Value="21" Selected="True"></asp:ListItem>
                                                                <asp:ListItem Value="22"></asp:ListItem>
                                                                <asp:ListItem Value="23"></asp:ListItem>
                                                                <asp:ListItem Value="24"></asp:ListItem>
                                                                <asp:ListItem Value="25"></asp:ListItem>
                                                                <asp:ListItem Value="26"></asp:ListItem>
                                                                <asp:ListItem Value="27"></asp:ListItem>
                                                                <asp:ListItem Value="28"></asp:ListItem>
                                                                <asp:ListItem Value="29"></asp:ListItem>
                                                                <asp:ListItem Value="30"></asp:ListItem>
                                                                <asp:ListItem Value="31"></asp:ListItem>
                                                            </asp:DropDownList>
                                                            &nbsp;&nbsp;&nbsp;&nbsp; Select Year<span class="reqfldText">*</span>&nbsp;&nbsp;<asp:TextBox
                                                                ID="txtMonthYear" runat="server" Width="5%" MaxLength="4"></asp:TextBox>
                                                            <asp:RequiredFieldValidator ID="requiredMonthlyYear" runat="server" SetFocusOnError="true"
                                                                ControlToValidate="txtMonthYear" CssClass="lblError" ValidationGroup="SubmitM"
                                                                Display="Dynamic" ErrorMessage="Required"></asp:RequiredFieldValidator>
                                                            <asp:RangeValidator ID="rangeMonthlyYear" SetFocusOnError="true" Type="Integer" MinimumValue="2000"
                                                                MaximumValue="2025" CssClass="lblError" Display="Dynamic" ControlToValidate="txtMonthYear"
                                                                runat="server" ValidationGroup="SubmitM" ErrorMessage="Enter Year between 2000 and 2025."></asp:RangeValidator>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="left">
                                                            <table id="tblMonthDetails" width="50%" cellspacing="5" cellpadding="3" border="0">
                                                                <tr>
                                                                    <td align="center" colspan="2" class="blackblodtext">
                                                                        Month
                                                                    </td>
                                                                    <td align="center" class="blackblodtext">
                                                                        Number of Days
                                                                    </td>
                                                                    <td align="center" colspan="2" class="blackblodtext">
                                                                        Month
                                                                    </td>
                                                                    <td align="center" class="blackblodtext">
                                                                        Number of Days
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td style="width: 3%">
                                                                        <asp:CheckBox ID="chkJan" runat="server" OnClick="javascript:return fnMonthdrop(this)" />
                                                                    </td>
                                                                    <td align="left" style="width: 5%">
                                                                        January
                                                                    </td>
                                                                    <td align="center" style="width: 5%">
                                                                        <asp:TextBox ID="TxtJan" runat="server" CssClass="txtStyle" Enabled="False"></asp:TextBox>
                                                                    </td>
                                                                    <td style="width: 3%">
                                                                        <asp:CheckBox ID="chkJul" runat="server" OnClick="javascript:return fnMonthdrop(this)" />
                                                                    </td>
                                                                    <td align="left" style="width: 5%">
                                                                        July
                                                                    </td>
                                                                    <td align="center" style="width: 5%">
                                                                        <asp:TextBox ID="TxtJul" runat="server" CssClass="txtStyle" Enabled="False"></asp:TextBox>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <asp:CheckBox ID="chkFeb" runat="server" OnClick="javascript:return fnMonthdrop(this)" />
                                                                    </td>
                                                                    <td align="left"> <%--FB 2384--%>
                                                                        February 
                                                                    </td>
                                                                    <td>
                                                                        <asp:TextBox ID="TxtFeb" runat="server" CssClass="txtStyle" Enabled="False"></asp:TextBox>
                                                                    </td>
                                                                    <td>
                                                                        <asp:CheckBox ID="chkAug" runat="server" OnClick="javascript:return fnMonthdrop(this)" />
                                                                    </td>
                                                                    <td align="left">
                                                                        August
                                                                    </td>
                                                                    <td>
                                                                        <asp:TextBox ID="TxtAug" runat="server" CssClass="txtStyle" Enabled="False"></asp:TextBox>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <asp:CheckBox ID="chkMar" runat="server" OnClick="javascript:return fnMonthdrop(this)" />
                                                                    </td>
                                                                    <td align="left">
                                                                        March
                                                                    </td>
                                                                    <td>
                                                                        <asp:TextBox ID="TxtMar" runat="server" CssClass="txtStyle" Enabled="False"></asp:TextBox>
                                                                    </td>
                                                                    <td>
                                                                        <asp:CheckBox ID="chkSep" runat="server" OnClick="javascript:return fnMonthdrop(this)" />
                                                                    </td>
                                                                    <td align="left">
                                                                        September
                                                                    </td>
                                                                    <td>
                                                                        <asp:TextBox ID="TxtSep" runat="server" CssClass="txtStyle" Enabled="False"></asp:TextBox>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <asp:CheckBox ID="chkApr" runat="server" OnClick="javascript:return fnMonthdrop(this)" />
                                                                    </td>
                                                                    <td align="left">
                                                                        April
                                                                    </td>
                                                                    <td>
                                                                        <asp:TextBox ID="TxtApr" runat="server" CssClass="txtStyle" Enabled="False"></asp:TextBox>
                                                                    </td>
                                                                    <td>
                                                                        <asp:CheckBox ID="chkOct" runat="server" OnClick="javascript:return fnMonthdrop(this)" />
                                                                    </td>
                                                                    <td align="left">
                                                                        October
                                                                    </td>
                                                                    <td>
                                                                        <asp:TextBox ID="TxtOct" runat="server" CssClass="txtStyle" Enabled="False"></asp:TextBox>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <asp:CheckBox ID="chkMay" runat="server" OnClick="javascript:return fnMonthdrop(this)" />
                                                                    </td>
                                                                    <td align="left">
                                                                        May
                                                                    </td>
                                                                    <td>
                                                                        <asp:TextBox ID="TxtMay" runat="server" CssClass="txtStyle" Enabled="False"></asp:TextBox>
                                                                    </td>
                                                                    <td>
                                                                        <asp:CheckBox ID="chkNov" runat="server" OnClick="javascript:return fnMonthdrop(this)" />
                                                                    </td>
                                                                    <td align="left">
                                                                        November
                                                                    </td>
                                                                    <td>
                                                                        <asp:TextBox ID="TxtNov" runat="server" CssClass="txtStyle" Enabled="False"></asp:TextBox>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <asp:CheckBox ID="chkJun" runat="server" OnClick="javascript:return fnMonthdrop(this)" />
                                                                    </td>
                                                                    <td align="left">
                                                                        June
                                                                    </td>
                                                                    <td>
                                                                        <asp:TextBox ID="TxtJun" runat="server" CssClass="txtStyle" Enabled="False"></asp:TextBox>
                                                                    </td>
                                                                    <td>
                                                                        <asp:CheckBox ID="chkDec" runat="server" OnClick="javascript:return fnMonthdrop(this)" />
                                                                    </td>
                                                                    <td align="left">
                                                                        December
                                                                    </td>
                                                                    <td>
                                                                        <asp:TextBox ID="TxtDec" runat="server" CssClass="txtStyle" Enabled="False"></asp:TextBox>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="width: 50%" align="center">
                                                            <input type="button" name="GoBack" value="Go Back" onclick="javascript:fnCancel()"
                                                                class="altMedium0BlueButtonFormat">
                                                            <asp:Button runat="server" ID="btnResetMonthly" Text="Reset" ValidationGroup="Reset"
                                                                OnClick="GetResetMonthly" OnClientClick="javascript:return fnMonthReset()" CssClass="altMedium0BlueButtonFormat" />
                                                            <asp:Button ID="btnMonthSubmit" runat="server"  Width="100pt"
                                                                Text="Submit" ValidationGroup="SubmitM" OnClientClick="javascript:return fnMonthSubmit()" 
                                                                OnClick="UpdateMonthWorkingDays" /> <%--FB 2796--%>
                                                            <input id="hdnMonthdays" type="hidden" runat="server" />
                                                            &nbsp;&nbsp;
                                                        </td>
                                                    </tr>
                                                </table>
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                            </ContentTemplate>
                        </ajax:TabPanel>
                        <ajax:TabPanel ID="WeeklyView" runat="server">
                            <HeaderTemplate>
                                <font class="blackblodtext">Configure Week Working Days</font>
                            </HeaderTemplate>
                            <ContentTemplate>
                                <table cellspacing="0" cellpadding="0" border="0" width="100%">
                                    <tr id="trWeek" runat="server">
                                        <td align="center">
                                            <div id="DivWeek">
                                                <table width="100%" cellpadding="3">
                                                    <tr align="left">
                                                        <td class="blackblodtext" align="left">
                                                            Configured Working Days for Year:
                                                            <asp:DropDownList ID="lstYearConfigWeek" runat="server" DataTextField="CurrentYear"
                                                                DataValueField="CurrentYear" OnSelectedIndexChanged="GetWeekDaysforYear" AutoPostBack="true">
                                                            </asp:DropDownList>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="left" class="blackblodtext">
                                                            Select All
                                                            <asp:CheckBox ID="ChkWeekSelectAll" runat="server" onclick="javascript:WeekSelectAll(this)" />
                                                            &nbsp;&nbsp;&nbsp;&nbsp; Days<span class="reqfldText">*</span>&nbsp;
                                                            <asp:DropDownList ID="DdlWeek" CssClass="altLong0SelectFormat" runat="server" onchange="javascript:OpenWeeklist()"
                                                                Width="50px">
                                                                <asp:ListItem Value="0"></asp:ListItem>
                                                                <asp:ListItem Value="1"></asp:ListItem>
                                                                <asp:ListItem Value="2"></asp:ListItem>
                                                                <asp:ListItem Value="3"></asp:ListItem>
                                                                <asp:ListItem Value="4"></asp:ListItem>
                                                                <asp:ListItem Value="5" Selected="True"></asp:ListItem>
                                                                <asp:ListItem Value="6"></asp:ListItem>
                                                                <asp:ListItem Value="7"></asp:ListItem>
                                                            </asp:DropDownList>
                                                            &nbsp;&nbsp;&nbsp;&nbsp; Select Year<span class="reqfldText">*</span>&nbsp;&nbsp;<asp:TextBox
                                                                ID="txtWeekYear" runat="server" Width="5%" MaxLength="4"></asp:TextBox>
                                                            <asp:RequiredFieldValidator ID="requiredWeeklyYear" runat="server" SetFocusOnError="true"
                                                                ControlToValidate="txtWeekYear" CssClass="lblError" ValidationGroup="SubmitW"
                                                                Display="Dynamic" ErrorMessage="Required"></asp:RequiredFieldValidator>
                                                            <asp:RangeValidator ID="rangeWeekYear" SetFocusOnError="true" Type="Integer" MinimumValue="2000"
                                                                MaximumValue="2025" CssClass="lblError" Display="Dynamic" ControlToValidate="txtWeekYear"
                                                                ValidationGroup="SubmitW" runat="server" ErrorMessage="Enter Year between 2000 and 2025."></asp:RangeValidator>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="center">
                                                            <table id="tblWeekDetails" width="100%" border="0" cellspacing="3" cellpadding="3">
                                                                <tr>
                                                                    <td align="center" colspan="2" class="blackblodtext">
                                                                        Week
                                                                    </td>
                                                                    <td align="center" class="blackblodtext">
                                                                        Number of Days
                                                                    </td>
                                                                    <td style="color: Gray;">
                                                                        &nbsp;
                                                                    </td>
                                                                    <td align="center" colspan="2" class="blackblodtext">
                                                                        Week
                                                                    </td>
                                                                    <td align="center" class="blackblodtext">
                                                                        Number of Days
                                                                    </td>
                                                                    <td style="color: Gray;">
                                                                        &nbsp;
                                                                    </td>
                                                                    <td align="center" colspan="2" class="blackblodtext">
                                                                        Week
                                                                    </td>
                                                                    <td align="center" class="blackblodtext">
                                                                        Number of Days
                                                                    </td>
                                                                    <td style="color: Gray;">
                                                                        &nbsp;
                                                                    </td>
                                                                    <td align="center" colspan="2" class="blackblodtext">
                                                                        Week
                                                                    </td>
                                                                    <td align="center" class="blackblodtext">
                                                                        Number of Days
                                                                    </td>
                                                                    <td style="color: Gray;">
                                                                        &nbsp;
                                                                    </td>
                                                                    <td align="center" colspan="2" class="blackblodtext">
                                                                        Week
                                                                    </td>
                                                                    <td align="center" class="blackblodtext">
                                                                        Number of Days
                                                                    </td>
                                                                    <td style="color: Gray;">
                                                                        &nbsp;
                                                                    </td>
                                                                    <td align="center" colspan="2" class="blackblodtext">
                                                                        Week
                                                                    </td>
                                                                    <td align="center" class="blackblodtext">
                                                                        Number of Days
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td style="width: 3%">
                                                                        <asp:CheckBox ID="ChkWeek1" runat="server" onclick="javascript:fnWeekdrop(this)" />
                                                                    </td>
                                                                    <td>
                                                                        Week 1
                                                                    </td>
                                                                    <td>
                                                                        <asp:TextBox runat="server" Width="20px" ID="txtWeek1" CssClass="txtStyle" Enabled="false"></asp:TextBox><br />
                                                                    </td>
                                                                    <td style="color: Gray;">
                                                                        &nbsp;
                                                                    </td>
                                                                    <td style="width: 3%">
                                                                        <asp:CheckBox ID="ChkWeek11" runat="server" onclick="javascript:fnWeekdrop(this)" />
                                                                    </td>
                                                                    <td nowrap>
                                                                        Week 11
                                                                    </td>
                                                                    <td>
                                                                        <asp:TextBox runat="server" Width="20px" ID="txtWeek11" CssClass="txtStyle" Enabled="false"></asp:TextBox><br />
                                                                    </td>
                                                                    <td style="color: Gray;">
                                                                        &nbsp;
                                                                    </td>
                                                                    <td style="width: 3%">
                                                                        <asp:CheckBox ID="ChkWeek21" runat="server" onclick="javascript:fnWeekdrop(this)" />
                                                                    </td>
                                                                    <td nowrap>
                                                                        Week 21
                                                                    </td>
                                                                    <td>
                                                                        <asp:TextBox runat="server" Width="20px" ID="txtWeek21" CssClass="txtStyle" Enabled="false"></asp:TextBox><br />
                                                                    </td>
                                                                    <td style="color: Gray;">
                                                                        &nbsp;
                                                                    </td>
                                                                    <td style="width: 3%">
                                                                        <asp:CheckBox ID="ChkWeek31" runat="server" onclick="javascript:fnWeekdrop(this)" />
                                                                    </td>
                                                                    <td nowrap>
                                                                        Week 31
                                                                    </td>
                                                                    <td>
                                                                        <asp:TextBox runat="server" Width="20px" ID="txtWeek31" CssClass="txtStyle" Enabled="false"></asp:TextBox><br />
                                                                    </td>
                                                                    <td style="color: Gray;">
                                                                        &nbsp;
                                                                    </td>
                                                                    <td style="width: 3%">
                                                                        <asp:CheckBox ID="ChkWeek41" runat="server" onclick="javascript:fnWeekdrop(this)" />
                                                                    </td>
                                                                    <td nowrap>
                                                                        Week 41
                                                                    </td>
                                                                    <td>
                                                                        <asp:TextBox runat="server" Width="20px" ID="txtWeek41" CssClass="txtStyle" Enabled="false"></asp:TextBox><br />
                                                                    </td>
                                                                    <td style="color: Gray;">
                                                                        &nbsp;
                                                                    </td>
                                                                    <td style="width: 3%">
                                                                        <asp:CheckBox ID="ChkWeek51" runat="server" onclick="javascript:fnWeekdrop(this)" />
                                                                    </td>
                                                                    <td nowrap>
                                                                        Week 51
                                                                    </td>
                                                                    <td>
                                                                        <asp:TextBox runat="server" Width="20px" ID="txtWeek51" CssClass="txtStyle" Enabled="false"></asp:TextBox>
                                                                        <br />
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td style="width: 3%">
                                                                        <asp:CheckBox ID="ChkWeek2" runat="server" onclick="javascript:fnWeekdrop(this)" />
                                                                    </td>
                                                                    <td>
                                                                        Week 2
                                                                    </td>
                                                                    <td>
                                                                        <asp:TextBox runat="server" Width="20px" ID="txtWeek2" CssClass="txtStyle" Enabled="false"></asp:TextBox><br />
                                                                    </td>
                                                                    <td style="color: Gray;">
                                                                        &nbsp;
                                                                    </td>
                                                                    <td style="width: 3%">
                                                                        <asp:CheckBox ID="ChkWeek12" runat="server" onclick="javascript:fnWeekdrop(this)" />
                                                                    </td>
                                                                    <td>
                                                                        Week 12
                                                                    </td>
                                                                    <td>
                                                                        <asp:TextBox runat="server" Width="20px" ID="txtWeek12" CssClass="txtStyle" Enabled="false"></asp:TextBox><br />
                                                                    </td>
                                                                    <td style="color: Gray;">
                                                                        &nbsp;
                                                                    </td>
                                                                    <td style="width: 3%">
                                                                        <asp:CheckBox ID="ChkWeek22" runat="server" onclick="javascript:fnWeekdrop(this)" />
                                                                    </td>
                                                                    <td>
                                                                        Week 22
                                                                    </td>
                                                                    <td>
                                                                        <asp:TextBox runat="server" Width="20px" ID="txtWeek22" CssClass="txtStyle" Enabled="false"></asp:TextBox><br />
                                                                    </td>
                                                                    <td style="color: Gray;">
                                                                        &nbsp;
                                                                    </td>
                                                                    <td style="width: 3%">
                                                                        <asp:CheckBox ID="ChkWeek32" runat="server" onclick="javascript:fnWeekdrop(this)" />
                                                                    </td>
                                                                    <td>
                                                                        Week 32
                                                                    </td>
                                                                    <td>
                                                                        <asp:TextBox runat="server" Width="20px" ID="txtWeek32" CssClass="txtStyle" Enabled="false"></asp:TextBox><br />
                                                                    </td>
                                                                    <td style="color: Gray;">
                                                                        &nbsp;
                                                                    </td>
                                                                    <td style="width: 3%">
                                                                        <asp:CheckBox ID="ChkWeek42" runat="server" onclick="javascript:fnWeekdrop(this)" />
                                                                    </td>
                                                                    <td>
                                                                        Week 42
                                                                    </td>
                                                                    <td>
                                                                        <asp:TextBox runat="server" Width="20px" ID="txtWeek42" CssClass="txtStyle" Enabled="false"></asp:TextBox><br />
                                                                    </td>
                                                                    <td style="color: Gray;">
                                                                        &nbsp;
                                                                    </td>
                                                                    <td style="width: 3%">
                                                                        <asp:CheckBox ID="ChkWeek52" runat="server" onclick="javascript:fnWeekdrop(this)" />
                                                                    </td>
                                                                    <td>
                                                                        Week 52
                                                                    </td>
                                                                    <td>
                                                                        <asp:TextBox runat="server" Width="20px" ID="txtWeek52" CssClass="txtStyle" Enabled="false"></asp:TextBox>
                                                                        <br />
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td style="width: 3%">
                                                                        <asp:CheckBox ID="ChkWeek3" runat="server" onclick="javascript:fnWeekdrop(this)" />
                                                                    </td>
                                                                    <td>
                                                                        Week 3
                                                                    </td>
                                                                    <td>
                                                                        <asp:TextBox runat="server" Width="20px" ID="txtWeek3" CssClass="txtStyle" Enabled="false"></asp:TextBox><br />
                                                                    </td>
                                                                    <td style="color: Gray;">
                                                                        &nbsp;
                                                                    </td>
                                                                    <td style="width: 3%">
                                                                        <asp:CheckBox ID="ChkWeek13" runat="server" onclick="javascript:fnWeekdrop(this)" />
                                                                    </td>
                                                                    <td>
                                                                        Week 13
                                                                    </td>
                                                                    <td>
                                                                        <asp:TextBox runat="server" Width="20px" ID="txtWeek13" CssClass="txtStyle" Enabled="false"></asp:TextBox><br />
                                                                    </td>
                                                                    <td style="color: Gray;">
                                                                        &nbsp;
                                                                    </td>
                                                                    <td style="width: 3%">
                                                                        <asp:CheckBox ID="ChkWeek23" runat="server" onclick="javascript:fnWeekdrop(this)" />
                                                                    </td>
                                                                    <td>
                                                                        Week 23
                                                                    </td>
                                                                    <td>
                                                                        <asp:TextBox runat="server" Width="20px" ID="txtWeek23" CssClass="txtStyle" Enabled="false"></asp:TextBox><br />
                                                                    </td>
                                                                    <td style="color: Gray;">
                                                                        &nbsp;
                                                                    </td>
                                                                    <td style="width: 3%">
                                                                        <asp:CheckBox ID="ChkWeek33" runat="server" onclick="javascript:fnWeekdrop(this)" />
                                                                    </td>
                                                                    <td>
                                                                        Week 33
                                                                    </td>
                                                                    <td>
                                                                        <asp:TextBox runat="server" Width="20px" ID="txtWeek33" CssClass="txtStyle" Enabled="false"></asp:TextBox><br />
                                                                    </td>
                                                                    <td style="color: Gray;">
                                                                        &nbsp;
                                                                    </td>
                                                                    <td style="width: 3%">
                                                                        <asp:CheckBox ID="ChkWeek43" runat="server" onclick="javascript:fnWeekdrop(this)" />
                                                                    </td>
                                                                    <td>
                                                                        Week 43
                                                                    </td>
                                                                    <td>
                                                                        <asp:TextBox runat="server" Width="20px" ID="txtWeek43" CssClass="txtStyle" Enabled="false"></asp:TextBox><br />
                                                                    </td>
                                                                    <td colspan="2">
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td style="width: 3%">
                                                                        <asp:CheckBox ID="ChkWeek4" runat="server" onclick="javascript:fnWeekdrop(this)" />
                                                                    </td>
                                                                    <td>
                                                                        Week 4
                                                                    </td>
                                                                    <td>
                                                                        <asp:TextBox runat="server" Width="20px" ID="txtWeek4" CssClass="txtStyle" Enabled="false"></asp:TextBox><br />
                                                                    </td>
                                                                    <td style="color: Gray;">
                                                                        &nbsp;
                                                                    </td>
                                                                    <td style="width: 3%">
                                                                        <asp:CheckBox ID="ChkWeek14" runat="server" onclick="javascript:fnWeekdrop(this)" />
                                                                    </td>
                                                                    <td>
                                                                        Week 14
                                                                    </td>
                                                                    <td>
                                                                        <asp:TextBox runat="server" Width="20px" ID="txtWeek14" CssClass="txtStyle" Enabled="false"></asp:TextBox><br />
                                                                    </td>
                                                                    <td style="color: Gray;">
                                                                        &nbsp;
                                                                    </td>
                                                                    <td style="width: 3%">
                                                                        <asp:CheckBox ID="ChkWeek24" runat="server" onclick="javascript:fnWeekdrop(this)" />
                                                                    </td>
                                                                    <td>
                                                                        Week 24
                                                                    </td>
                                                                    <td>
                                                                        <asp:TextBox runat="server" Width="20px" ID="txtWeek24" CssClass="txtStyle" Enabled="false"></asp:TextBox><br />
                                                                    </td>
                                                                    <td style="color: Gray;">
                                                                        &nbsp;
                                                                    </td>
                                                                    <td style="width: 3%">
                                                                        <asp:CheckBox ID="ChkWeek34" runat="server" onclick="javascript:fnWeekdrop(this)" />
                                                                    </td>
                                                                    <td>
                                                                        Week 34
                                                                    </td>
                                                                    <td>
                                                                        <asp:TextBox runat="server" Width="20px" ID="txtWeek34" CssClass="txtStyle" Enabled="false"></asp:TextBox><br />
                                                                    </td>
                                                                    <td style="color: Gray;">
                                                                        &nbsp;
                                                                    </td>
                                                                    <td style="width: 3%">
                                                                        <asp:CheckBox ID="ChkWeek44" runat="server" onclick="javascript:fnWeekdrop(this)" />
                                                                    </td>
                                                                    <td>
                                                                        Week 44
                                                                    </td>
                                                                    <td>
                                                                        <asp:TextBox runat="server" Width="20px" ID="txtWeek44" CssClass="txtStyle" Enabled="false"></asp:TextBox><br />
                                                                    </td>
                                                                    <td colspan="2">
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td style="width: 3%">
                                                                        <asp:CheckBox ID="ChkWeek5" runat="server" onclick="javascript:fnWeekdrop(this)" />
                                                                    </td>
                                                                    <td>
                                                                        Week 5
                                                                    </td>
                                                                    <td>
                                                                        <asp:TextBox runat="server" Width="20px" ID="txtWeek5" CssClass="txtStyle" Enabled="false"></asp:TextBox><br />
                                                                    </td>
                                                                    <td style="color: Gray;">
                                                                        &nbsp;
                                                                    </td>
                                                                    <td style="width: 3%">
                                                                        <asp:CheckBox ID="ChkWeek15" runat="server" onclick="javascript:fnWeekdrop(this)" />
                                                                    </td>
                                                                    <td>
                                                                        Week 15
                                                                    </td>
                                                                    <td>
                                                                        <asp:TextBox runat="server" Width="20px" ID="txtWeek15" CssClass="txtStyle" Enabled="false"></asp:TextBox><br />
                                                                    </td>
                                                                    <td style="color: Gray;">
                                                                        &nbsp;
                                                                    </td>
                                                                    <td style="width: 3%">
                                                                        <asp:CheckBox ID="ChkWeek25" runat="server" onclick="javascript:fnWeekdrop(this)" />
                                                                    </td>
                                                                    <td>
                                                                        Week 25
                                                                    </td>
                                                                    <td>
                                                                        <asp:TextBox runat="server" Width="20px" ID="txtWeek25" CssClass="txtStyle" Enabled="false"></asp:TextBox><br />
                                                                    </td>
                                                                    <td style="color: Gray;">
                                                                        &nbsp;
                                                                    </td>
                                                                    <td style="width: 3%">
                                                                        <asp:CheckBox ID="ChkWeek35" runat="server" onclick="javascript:fnWeekdrop(this)" />
                                                                    </td>
                                                                    <td>
                                                                        Week 35
                                                                    </td>
                                                                    <td>
                                                                        <asp:TextBox runat="server" Width="20px" ID="txtWeek35" CssClass="txtStyle" Enabled="false"></asp:TextBox><br />
                                                                    </td>
                                                                    <td style="color: Gray;">
                                                                        &nbsp;
                                                                    </td>
                                                                    <td style="width: 3%">
                                                                        <asp:CheckBox ID="ChkWeek45" runat="server" onclick="javascript:fnWeekdrop(this)" />
                                                                    </td>
                                                                    <td>
                                                                        Week 45
                                                                    </td>
                                                                    <td>
                                                                        <asp:TextBox runat="server" Width="20px" ID="txtWeek45" CssClass="txtStyle" Enabled="false"></asp:TextBox><br />
                                                                    </td>
                                                                    <td colspan="2">
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td style="width: 3%">
                                                                        <asp:CheckBox ID="ChkWeek6" runat="server" onclick="javascript:fnWeekdrop(this)" />
                                                                    </td>
                                                                    <td>
                                                                        Week 6
                                                                    </td>
                                                                    <td>
                                                                        <asp:TextBox runat="server" Width="20px" ID="txtWeek6" CssClass="txtStyle" Enabled="false"></asp:TextBox><br />
                                                                    </td>
                                                                    <td style="color: Gray;">
                                                                        &nbsp;
                                                                    </td>
                                                                    <td style="width: 3%">
                                                                        <asp:CheckBox ID="ChkWeek16" runat="server" onclick="javascript:fnWeekdrop(this)" />
                                                                    </td>
                                                                    <td>
                                                                        Week 16
                                                                    </td>
                                                                    <td>
                                                                        <asp:TextBox runat="server" Width="20px" ID="txtWeek16" CssClass="txtStyle" Enabled="false"></asp:TextBox><br />
                                                                    </td>
                                                                    <td style="color: Gray;">
                                                                        &nbsp;
                                                                    </td>
                                                                    <td style="width: 3%">
                                                                        <asp:CheckBox ID="ChkWeek26" runat="server" onclick="javascript:fnWeekdrop(this)" />
                                                                    </td>
                                                                    <td>
                                                                        Week 26
                                                                    </td>
                                                                    <td>
                                                                        <asp:TextBox runat="server" Width="20px" ID="txtWeek26" CssClass="txtStyle" Enabled="false"></asp:TextBox><br />
                                                                    </td>
                                                                    <td style="color: Gray;">
                                                                        &nbsp;
                                                                    </td>
                                                                    <td style="width: 3%">
                                                                        <asp:CheckBox ID="ChkWeek36" runat="server" onclick="javascript:fnWeekdrop(this)" />
                                                                    </td>
                                                                    <td>
                                                                        Week 36
                                                                    </td>
                                                                    <td>
                                                                        <asp:TextBox runat="server" Width="20px" ID="txtWeek36" CssClass="txtStyle" Enabled="false"></asp:TextBox><br />
                                                                    </td>
                                                                    <td style="color: Gray;">
                                                                        &nbsp;
                                                                    </td>
                                                                    <td style="width: 3%">
                                                                        <asp:CheckBox ID="ChkWeek46" runat="server" onclick="javascript:fnWeekdrop(this)" />
                                                                    </td>
                                                                    <td>
                                                                        Week 46
                                                                    </td>
                                                                    <td>
                                                                        <asp:TextBox runat="server" Width="20px" ID="txtWeek46" CssClass="txtStyle" Enabled="false"></asp:TextBox><br />
                                                                    </td>
                                                                    <td colspan="2">
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td style="width: 3%">
                                                                        <asp:CheckBox ID="ChkWeek7" runat="server" onclick="javascript:fnWeekdrop(this)" />
                                                                    </td>
                                                                    <td>
                                                                        Week 7
                                                                    </td>
                                                                    <td>
                                                                        <asp:TextBox runat="server" Width="20px" ID="txtWeek7" CssClass="txtStyle" Enabled="false"></asp:TextBox><br />
                                                                    </td>
                                                                    <td style="color: Gray;">
                                                                        &nbsp;
                                                                    </td>
                                                                    <td style="width: 3%">
                                                                        <asp:CheckBox ID="ChkWeek17" runat="server" onclick="javascript:fnWeekdrop(this)" />
                                                                    </td>
                                                                    <td>
                                                                        Week 17
                                                                    </td>
                                                                    <td>
                                                                        <asp:TextBox runat="server" Width="20px" ID="txtWeek17" CssClass="txtStyle" Enabled="false"></asp:TextBox><br />
                                                                    </td>
                                                                    <td style="color: Gray;">
                                                                        &nbsp;
                                                                    </td>
                                                                    <td style="width: 3%">
                                                                        <asp:CheckBox ID="ChkWeek27" runat="server" onclick="javascript:fnWeekdrop(this)" />
                                                                    </td>
                                                                    <td>
                                                                        Week 27
                                                                    </td>
                                                                    <td>
                                                                        <asp:TextBox runat="server" Width="20px" ID="txtWeek27" CssClass="txtStyle" Enabled="false"></asp:TextBox><br />
                                                                    </td>
                                                                    <td style="color: Gray;">
                                                                        &nbsp;
                                                                    </td>
                                                                    <td style="width: 3%">
                                                                        <asp:CheckBox ID="ChkWeek37" runat="server" onclick="javascript:fnWeekdrop(this)" />
                                                                    </td>
                                                                    <td>
                                                                        Week 37
                                                                    </td>
                                                                    <td>
                                                                        <asp:TextBox runat="server" Width="20px" ID="txtWeek37" CssClass="txtStyle" Enabled="false"></asp:TextBox><br />
                                                                    </td>
                                                                    <td style="color: Gray;">
                                                                        &nbsp;
                                                                    </td>
                                                                    <td style="width: 3%">
                                                                        <asp:CheckBox ID="ChkWeek47" runat="server" onclick="javascript:fnWeekdrop(this)" />
                                                                    </td>
                                                                    <td>
                                                                        Week 47
                                                                    </td>
                                                                    <td>
                                                                        <asp:TextBox runat="server" Width="20px" ID="txtWeek47" CssClass="txtStyle" Enabled="false"></asp:TextBox><br />
                                                                    </td>
                                                                    <td colspan="2">
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td style="width: 3%">
                                                                        <asp:CheckBox ID="ChkWeek8" runat="server" onclick="javascript:fnWeekdrop(this)" />
                                                                    </td>
                                                                    <td>
                                                                        Week 8
                                                                    </td>
                                                                    <td>
                                                                        <asp:TextBox runat="server" Width="20px" ID="txtWeek8" CssClass="txtStyle" Enabled="false"></asp:TextBox><br />
                                                                    </td>
                                                                    <td style="color: Gray;">
                                                                        &nbsp;
                                                                    </td>
                                                                    <td style="width: 3%">
                                                                        <asp:CheckBox ID="ChkWeek18" runat="server" onclick="javascript:fnWeekdrop(this)" />
                                                                    </td>
                                                                    <td>
                                                                        Week 18
                                                                    </td>
                                                                    <td>
                                                                        <asp:TextBox runat="server" Width="20px" ID="txtWeek18" CssClass="txtStyle" Enabled="false"></asp:TextBox><br />
                                                                    </td>
                                                                    <td style="color: Gray;">
                                                                        &nbsp;
                                                                    </td>
                                                                    <td style="width: 3%">
                                                                        <asp:CheckBox ID="ChkWeek28" runat="server" onclick="javascript:fnWeekdrop(this)" />
                                                                    </td>
                                                                    <td>
                                                                        Week 28
                                                                    </td>
                                                                    <td>
                                                                        <asp:TextBox runat="server" Width="20px" ID="txtWeek28" CssClass="txtStyle" Enabled="false"></asp:TextBox><br />
                                                                    </td>
                                                                    <td style="color: Gray;">
                                                                        &nbsp;
                                                                    </td>
                                                                    <td style="width: 3%">
                                                                        <asp:CheckBox ID="ChkWeek38" runat="server" onclick="javascript:fnWeekdrop(this)" />
                                                                    </td>
                                                                    <td>
                                                                        Week 38
                                                                    </td>
                                                                    <td>
                                                                        <asp:TextBox runat="server" Width="20px" ID="txtWeek38" CssClass="txtStyle" Enabled="false"></asp:TextBox><br />
                                                                    </td>
                                                                    <td style="color: Gray;">
                                                                        &nbsp;
                                                                    </td>
                                                                    <td style="width: 3%">
                                                                        <asp:CheckBox ID="ChkWeek48" runat="server" onclick="javascript:fnWeekdrop(this)" />
                                                                    </td>
                                                                    <td>
                                                                        Week 48
                                                                    </td>
                                                                    <td>
                                                                        <asp:TextBox runat="server" Width="20px" ID="txtWeek48" CssClass="txtStyle" Enabled="false"></asp:TextBox><br />
                                                                    </td>
                                                                    <td colspan="2">
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td style="width: 3%">
                                                                        <asp:CheckBox ID="ChkWeek9" runat="server" onclick="javascript:fnWeekdrop(this)" />
                                                                    </td>
                                                                    <td>
                                                                        Week 9
                                                                    </td>
                                                                    <td>
                                                                        <asp:TextBox runat="server" Width="20px" ID="txtWeek9" CssClass="txtStyle" Enabled="false"></asp:TextBox><br />
                                                                    </td>
                                                                    <td style="color: Gray;">
                                                                        &nbsp;
                                                                    </td>
                                                                    <td style="width: 3%">
                                                                        <asp:CheckBox ID="ChkWeek19" runat="server" onclick="javascript:fnWeekdrop(this)" />
                                                                    </td>
                                                                    <td>
                                                                        Week 19
                                                                    </td>
                                                                    <td>
                                                                        <asp:TextBox runat="server" Width="20px" ID="txtWeek19" CssClass="txtStyle" Enabled="false"></asp:TextBox><br />
                                                                    </td>
                                                                    <td style="color: Gray;">
                                                                        &nbsp;
                                                                    </td>
                                                                    <td style="width: 3%">
                                                                        <asp:CheckBox ID="ChkWeek29" runat="server" onclick="javascript:fnWeekdrop(this)" />
                                                                    </td>
                                                                    <td>
                                                                        Week 29
                                                                    </td>
                                                                    <td>
                                                                        <asp:TextBox runat="server" Width="20px" ID="txtWeek29" CssClass="txtStyle" Enabled="false"></asp:TextBox><br />
                                                                    </td>
                                                                    <td style="color: Gray;">
                                                                        &nbsp;
                                                                    </td>
                                                                    <td style="width: 3%">
                                                                        <asp:CheckBox ID="ChkWeek39" runat="server" onclick="javascript:fnWeekdrop(this)" />
                                                                    </td>
                                                                    <td>
                                                                        Week 39
                                                                    </td>
                                                                    <td>
                                                                        <asp:TextBox runat="server" Width="20px" ID="txtWeek39" CssClass="txtStyle" Enabled="false"></asp:TextBox><br />
                                                                    </td>
                                                                    <td style="color: Gray;">
                                                                        &nbsp;
                                                                    </td>
                                                                    <td style="width: 3%">
                                                                        <asp:CheckBox ID="ChkWeek49" runat="server" onclick="javascript:fnWeekdrop(this)" />
                                                                    </td>
                                                                    <td>
                                                                        Week 49
                                                                    </td>
                                                                    <td>
                                                                        <asp:TextBox runat="server" Width="20px" ID="txtWeek49" CssClass="txtStyle" Enabled="false"></asp:TextBox><br />
                                                                    </td>
                                                                    <td colspan="2">
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td style="width: 3%">
                                                                        <asp:CheckBox ID="ChkWeek10" runat="server" onclick="javascript:fnWeekdrop(this)" />
                                                                    </td>
                                                                    <td nowrap>
                                                                        Week 10
                                                                    </td>
                                                                    <td>
                                                                        <asp:TextBox runat="server" Width="20px" ID="txtWeek10" CssClass="txtStyle" Enabled="false"></asp:TextBox><br />
                                                                    </td>
                                                                    <td style="color: Gray;">
                                                                        &nbsp;
                                                                    </td>
                                                                    <td style="width: 3%">
                                                                        <asp:CheckBox ID="ChkWeek20" runat="server" onclick="javascript:fnWeekdrop(this)" />
                                                                    </td>
                                                                    <td>
                                                                        Week 20
                                                                    </td>
                                                                    <td>
                                                                        <asp:TextBox runat="server" Width="20px" ID="txtWeek20" CssClass="txtStyle" Enabled="false"></asp:TextBox><br />
                                                                    </td>
                                                                    <td style="color: Gray;">
                                                                        &nbsp;
                                                                    </td>
                                                                    <td style="width: 3%">
                                                                        <asp:CheckBox ID="ChkWeek30" runat="server" onclick="javascript:fnWeekdrop(this)" />
                                                                    </td>
                                                                    <td>
                                                                        Week 30
                                                                    </td>
                                                                    <td>
                                                                        <asp:TextBox runat="server" Width="20px" ID="txtWeek30" CssClass="txtStyle" Enabled="false"></asp:TextBox><br />
                                                                    </td>
                                                                    <td style="color: Gray;">
                                                                        &nbsp;
                                                                    </td>
                                                                    <td style="width: 3%">
                                                                        <asp:CheckBox ID="ChkWeek40" runat="server" onclick="javascript:fnWeekdrop(this)" />
                                                                    </td>
                                                                    <td>
                                                                        Week 40
                                                                    </td>
                                                                    <td>
                                                                        <asp:TextBox runat="server" Width="20px" ID="txtWeek40" CssClass="txtStyle" Enabled="false"></asp:TextBox><br />
                                                                    </td>
                                                                    <td style="color: Gray;">
                                                                        &nbsp;
                                                                    </td>
                                                                    <td style="width: 3%">
                                                                        <asp:CheckBox ID="ChkWeek50" runat="server" onclick="javascript:fnWeekdrop(this)" />
                                                                    </td>
                                                                    <td>
                                                                        Week 50
                                                                    </td>
                                                                    <td>
                                                                        <asp:TextBox runat="server" Width="20px" ID="txtWeek50" CssClass="txtStyle" Enabled="false"></asp:TextBox><br />
                                                                    </td>
                                                                    <td colspan="2">
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="width: 50%" align="center">
                                                            <input type="button" name="GoBack" value="Go Back" onclick="javascript:fnCancel()"
                                                                class="altMedium0BlueButtonFormat">
                                                            <asp:Button runat="server" ID="btnResetWeekly" Text="Reset" ValidationGroup="Reset"
                                                                OnClick="GetResetWeekly" OnClientClick="javascript:return fnWeekReset();" CssClass="altMedium0BlueButtonFormat" />
                                                            <asp:Button ID="btnWeekSubmit" runat="server"  Width="100pt"
                                                                Text="Submit" ValidationGroup="SubmitW" OnClick="UpdateWeekWorkingDays" OnClientClick="javascript:return fnWeekSubmit()" /> <%--FB 2796--%>
                                                            <input name="hdnWeekdays" id="hdnWeekdays" type="hidden" runat="server" />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                            </ContentTemplate>
                        </ajax:TabPanel>
                    </ajax:TabContainer>
                </td>
            </tr>
        </table>
    </div>
    <%--code added for Soft Edge button--%>
    </form>
</body>
</html>
<!-- #INCLUDE FILE="inc/mainbottomNET.aspx" -->

<script type="text/javascript" src="inc/softedge.js"></script>

