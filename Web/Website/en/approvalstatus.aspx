<%--ZD 100147 start--%>
<%--/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/--%>
<%--ZD 100147 End--%>
<%@ Page Language="C#" AutoEventWireup="true" Inherits="en_approvalstatus" ValidateRequest="false" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<script type="text/javascript" language="JavaScript" src="sorttable.js"></script> 
<script language="JavaScript" src="sorttable.js" type="text/javascript"></script>
<script type="text/javascript" src="script/wincheck.js"></script> <%--FB 3055--%>

<head runat="server">
    <title></title>
      <!--Organization CSS Module-->
<script type="text/javascript">    // FB 2790
    var path = '<%=Session["OrgCSSPath"]%>';
    path = path.split('.')[0] + '<%=Session["ThemeType"]%>' + ".css"; // FB 2815
    document.write('<link rel="stylesheet" title="Expedite base styles" type="text/css" href="' + path + '?' + new Date().getTime() + '">');
</script>

</head>
<body>
    <form id="frmApprovalstatus" runat="server">
    <div>
        <center>
            <div id="dataLoadingDIV" style="z-index:1"></div> 
                <%--Window Dressing--%>
               <h3><asp:Label ID="lblTitle" runat="server" Text="Approval Status" Font-Bold="true"></asp:Label> </h3>                   
            <asp:Label ID="errLabel" runat="server" CssClass="lblError" ></asp:Label>
        </center>
        <table border="0">
            <tr >  
            <br />              
                <%--Window Dressing--%>
                <td align="center" class="blackblodtext"><b>Conference Name</b>: <%=Request.QueryString["m"]%></td>
                <td align="center" class="blackblodtext"><b>Date &amp; time</b>: <%=Request.QueryString["d"].Replace("linebreak", " ")%></td>              
            </tr>
            
<script language="JavaScript">
<!-- 

	var room_list = new SortTable('room_list');
	
	room_list.AddColumn("Roomname","","center","email");
	room_list.AddColumn("Approvalname","","center","email");
	room_list.AddColumn("Decision","","center","email");
	room_list.AddColumn("Message","","center","email");
	room_list.AddColumn("Responsetime","","center","email");
	
	var rmDetails = "<%=roomDetails%>";
	
	if(rmDetails != "")
	{
	     //FB 1888
	    rmDetails = replaceSpcChar(rmDetails, "\"", "??");
	    rmDetails = replaceSpcChar(rmDetails, "'", "**");
	    var rmAryList = rmDetails.split("`")
	    for(i=0;i<rmAryList.length;i++)
	    { 	    
	        var rmAryList1 = rmAryList[i].split("~"); /* Code Modified For FB 1448 */
	        room_list.AddLine(rmAryList1[0], rmAryList1[1], rmAryList1[2], rmAryList1[3], rmAryList1[4]);
        }
    }   
	var mcu_list = new SortTable('mcu_list');
	
	mcu_list.AddColumn("MCUname","","center","email");
	mcu_list.AddColumn("Approvalname","","center","email");
	mcu_list.AddColumn("Decision","","center","email");
	mcu_list.AddColumn("Message","","center","email");
	mcu_list.AddColumn("Responsetime","","center","email");

    var mDetails = "<%=mcuDetails%>";
	
	if(mDetails != "")
	{
	    //FB 1888
	    mDetails = replaceSpcChar(mDetails, "\"", "??");
	    mDetails = replaceSpcChar(mDetails, "'", "**");
	    var mcuAryList = mDetails.split("`")
	    for(i=0;i<mcuAryList.length;i++)
	    { 
	        var mcuAryList1 = mcuAryList[i].split("~"); /* Code Modified For FB 1448 */
	        mcu_list.AddLine(mcuAryList1[0], mcuAryList1[1], mcuAryList1[2], mcuAryList1[3], mcuAryList1[4]);
        }
    }
    
	var department_list = new SortTable('department_list');
	
	department_list.AddColumn("Departmentname","","center","email");
	department_list.AddColumn("Approvalname","","center","email");
	department_list.AddColumn("Decision","","center","email");
	department_list.AddColumn("Message","","center","email");
	department_list.AddColumn("Responsetime","","center","email");

    var deDetails = "<%=deptDetails%>";
	
	if(deDetails != "")
	{
	    //FB 1888
	    deDetails = replaceSpcChar(deDetails, "\"", "??");
	    deDetails = replaceSpcChar(deDetails, "'", "**");
	    var deptAryList = deDetails.split("`")
	    for(i=0;i<deptAryList.length;i++)
	    { 
	        var deptAryList1 = deptAryList[i].split("~"); /* Code Modified For FB 1448 */
	        department_list.AddLine(deptAryList1[0], deptAryList1[1], deptAryList1[2], deptAryList1[3], deptAryList1[4]);
        }
    }
    
	var sys_list = new SortTable('sys_list');
	
	sys_list.AddColumn("Approvalname","","center","email");
	sys_list.AddColumn("Decision","","center","email");
	sys_list.AddColumn("Message","","center","email");
	sys_list.AddColumn("Responsetime","","center","email");
	
	var sDetails = "<%=sysDetails%>";
	
	if(sDetails != "")
	{	
	    //FB 1888
	    sDetails = replaceSpcChar(sDetails, "\"", "??");
	    sDetails = replaceSpcChar(sDetails, "'", "**");
	    
	    var sysAryList = sDetails.split("`");
	    for(i=0;i<sysAryList.length;i++)
	    { 
	        var sysAryList1 = sysAryList[i].split("~"); /* Code Modified For FB 1448 */
	        sys_list.AddLine(sysAryList1[0], sysAryList1[1], sysAryList1[2], sysAryList1[3]);
        }
    }   

        
    //FB 1888
    function replaceSpcChar(strName,symbol,srcSym)
    {
       do
        {
          strName = strName.replace(srcSym, symbol);
        }while((strName.indexOf(srcSym)) >= "0");
        
        return strName;
    }
		
//-->
</script>
      <tr>
        <td colspan="2">
            <table border="0" width="100%" runat="server" id="RoomDetailsTable">
              <tr>
                 <%--Window Dressing--%>
                <td class="tableHeader">Room</td>
              </tr>
              <tr>
                <td>
                  <table border="0" width="100%" cellpadding="2">      
                    <tr>
                      <td width=70 align=center><A href="javascript:SortRows(room_list,0)" class="sttitlelink">Room Name</A></td>
                      <td width=170 align=center><A href="javascript:SortRows(room_list,1)" class="sttitlelink">Approver Name</A></td>
                      <td width=70 align=center><A href="javascript:SortRows(room_list,2)" class="sttitlelink">Decision</A></td>
                      <td width=220 align=center><A href="javascript:SortRows(room_list,3)" class="sttitlelink">Message</A></td>
                      <td width=170 align=center><A href="javascript:SortRows(room_list,4)" class="sttitlelink">Response Time</A></td>
                    </tr>
                    <tr>
                      <td height=1 colspan=5 bgcolor="CCCCCC"></td>
                    </tr>
                    <SCRIPT>room_list.WriteRows()</SCRIPT>
                  </table>
                </td>
              </tr>
            </table>
            <br>
            <table border="0" width="100%" runat="server" id="MCUDetailsTable">
              <tr>
                <%--Window Dressing--%>
                <td class="tableHeader">MCU</td>
              </tr>
              <tr>
                <td>
                  <table border="0" width="100%" cellpadding="2">      
                    <tr>
                      <td width=70 align=center><A href="javascript:SortRows(mcu_list,0)" class="sttitlelink">MCU Name</A></td>
                      <td width=170 align=center><A href="javascript:SortRows(mcu_list,1)" class="sttitlelink">Approver Name</A></td>
                      <td width=70 align=center><A href="javascript:SortRows(mcu_list,2)" class="sttitlelink">Decision</A></td>
                      <td width=220 align=center><A href="javascript:SortRows(mcu_list,3)" class="sttitlelink">Message</A></td>
                      <td width=170 align=center><A href="javascript:SortRows(mcu_list,4)" class="sttitlelink">Response Time</A></td>
                    </tr>
                    <tr>
                      <td height=1 colspan=5 bgcolor="CCCCCC"></td>
                    </tr>
                    <SCRIPT>mcu_list.WriteRows()</SCRIPT>
                  </table>
                </td>
              </tr>
            </table>
            <br>
            <table border="0" width="100%" runat="server" id="DeptDetailsTable">
              <tr>
                <%--Window Dressing--%>
                <td class="tableHeader">Department</td>
              </tr>
              <tr>
                <td>
                  <table border="0" width="100%" cellpadding="2">      
                    <tr>
                      <td width=70 align=center><A href="javascript:SortRows(mcu_list,0)" class="sttitlelink">Department Name</A></td>
                      <td width=170 align=center><A href="javascript:SortRows(mcu_list,1)" class="sttitlelink">Approver Name</A></td>
                      <td width=70 align=center><A href="javascript:SortRows(mcu_list,2)" class="sttitlelink">Decision</A></td>
                      <td width=220 align=center><A href="javascript:SortRows(mcu_list,3)" class="sttitlelink">Message</A></td>
                      <td width=170 align=center><A href="javascript:SortRows(mcu_list,4)" class="sttitlelink">Response Time</A></td>
                    </tr>
                    <tr>
                      <td height=1 colspan=5 bgcolor="CCCCCC"></td>
                    </tr>
                    <SCRIPT>department_list.WriteRows()</SCRIPT>
                  </table>
                </td>
              </tr>
            </table>
            <br>
            <table border="0" width="100%" runat="server" id="SysDetailsTable">
              <tr>
                <%--Window Dressing--%>
                <td class="tableHeader">System</td>
              </tr>
              <tr>
                <td>
                  <table border="0" width="100%" cellpadding="2">      
                    <tr>
                      <td align=center><A href="javascript:SortRows(sys_list,0)" class="sttitlelink">Approver Name</A></td>
                      <td align=center><A href="javascript:SortRows(sys_list,1)" class="sttitlelink">Decision</A></td>
                      <td align=center><A href="javascript:SortRows(sys_list,2)" class="sttitlelink">Message</A></td>
                      <td align=center><A href="javascript:SortRows(sys_list,3)" class="sttitlelink">Response Time</A></td>
                    </tr>
                    <tr>
                      <td height=1 colspan=5 bgcolor="CCCCCC"></td>
                    </tr>
                    <SCRIPT>sys_list.WriteRows()</SCRIPT>
                  </table>
                </td>
              </tr>
            </table>
            <br>
            <table border="0" cellpadding="2" width="100%" >
                <tr>
                  <td width="100%">
		            <table border="0"  width="650"cellpadding="2">      
	                    <tr><td>
		                    <font size="1" color="blue"><b><!--
		                      <img border="0" src="image/iconmail.gif" width="15" height="15"> send email
		                      &nbsp;&nbsp;&nbsp;
		            -->
		                      <IMG height=15 src="image/icon_thumbup.gif" width=15 border=0> 
		                      Approved&nbsp;&nbsp;&nbsp;
		                      <IMG height=15 src="image/icon_thumbdown.gif" width=15 border=0> 
		                      Denied&nbsp;&nbsp;&nbsp;&nbsp;
		                      <IMG height=15 src="image/icon_warning.gif" width=15 border=0> No response yet
		                      &nbsp;&nbsp;&nbsp;
		                    </b></font>
			            </td></tr>
                    </table>
                  </td>
                </tr>
            </table> 
             <br>
              <center>
                <input type="button" name="closewindow" value="Close Window" class="altYellowButtonFormat" onClick='window.close()' style="width:150px"> <%--FB 2985--%>
              </center>    
           </td>
         </tr>
      </table>     
    </div>
    </form>
</body>
</html>
<%--code added for Soft Edge button--%>
<script type="text/javascript" src="inc/softedge.js"></script>