<%--ZD 100147 start--%>
<%--/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/--%><%--ZD 100147 End--%>
<html>

<head>
<meta http-equiv="Content-Language" content="en-us">
<title>myVRM : Session has expired</title>
</head>

<body bottommargin="0" leftmargin="0" rightmargin="0" topmargin="0" marginheight="0" marginwidth="0" bgcolor="">
  <table width="100%" border="0" cellpadding="0" cellspacing="0">
    <tr valign="top">
      <td background="image/Lobbytop.jpg" width="100%" height="72">
      </td>
    </tr>
  </table>


<center>

<table width="100%" border="0" cellpadding="0" cellspacing="0">
  <tr valign="top">
    <td width="20%" valign="top" align="right">
	  <br><br><br><br><br><br><br><br>
      <img src="<%=Application["companyLogo"]%>">
    </td>

    <td width="80%">
      <br><br><br>
		  <table width="100%" cellpadding="6"><tr><td>
			  
			  
<!--------------------------------- CONTENT GOES HERE --------------->



<%
	
	if (Session["userID"] != null && Session["userID"].ToString() != "")
    {
        if(Application["OnlineUserList"] != null)
        {
        
            String localUserList = Application["OnlineUserList"].ToString();
            String user = Session["userID"].ToString() + ",";

            if (localUserList.Contains(user))
            {
                String newlocalUserList = localUserList.Replace(user, "");
                Application.Lock();
                Application["OnlineUserList"] = newlocalUserList;
                Application.UnLock();
            }
        }
    }
%>

<%
String bodytext ="";
if(Session["winType"] != null)
{
	switch(Session["winType"].ToString())
	{
	
		case "pop":
			bodytext = "" ;
			bodytext = "<script language='javascript'>&nbsp;";
			bodytext = "function closewindow()&nbsp;" ;
			bodytext = "{&nbsp;" ;
			bodytext = "window.close();&nbsp;" ;
			bodytext = "}&nbsp;&nbsp;" ;
			bodytext = "setTimeout('closewindow()',5000);&nbsp;" ;
			bodytext = "</script>&nbsp;" ;
			
			bodytext = bodytext + "<center><p><br><br><font size='1'>" ;
			bodytext = bodytext + "myVRM session has expired due to inactivity.<br>" ;
			bodytext = bodytext + "Please re-login and try again." ;
			
			bodytext = bodytext + "<br><br>this window will be closed in 5 seconds or click the button below to close the window now</font></p>"  ;
			bodytext = bodytext + "<p>&nbsp;</p><p>&nbsp;</p>" ;
			bodytext = bodytext + "<input type='button' name='closewindow' value='Close Window' class='altBlueButtonFormat' onClick='window.close()'></center>" ;
			break;
		case "ifr":
			bodytext = "" ;
			bodytext = bodytext + "<p><br><font size='1'>" ;
			bodytext = bodytext + "myVRM session has expired due to inactivity.<br>" ;
			bodytext = bodytext + "Please re-login and try again.</font></p>" ;
			break;
			
		default:
			bodytext = "" ;
			bodytext = bodytext + "<p><font size='4' color='#808080'>" ;
			bodytext = bodytext + "myVRM session has expired due to inactivity.<br>" ;
			bodytext = bodytext + "Please click </font>" ;
			bodytext = bodytext + "<a href='~/en/genlogin.aspx'><font size='4' color='#666699'>here</font></a>" ; //FB 1830
			bodytext = bodytext + "<font size='4' color='#808080'> to re-login.</p>" ;
			break;
	}
}
		
		
	Response.Write(bodytext) ;

%>


<!-------------------------------------------------------------------->

            <!-------->
          </td>
        </tr>
      </table>
  
    </td>
  </tr>
</table>

</center>

<%

Response.Redirect ("~/en/genlogin.aspx?m=" + Request.QueryString["m"] ); //FB 1830
%>

</body>
</html>
<%--code added for Soft Edge button--%>
<script type="text/javascript" src="inc/softedge.js"></script>