<%--ZD 100147 start--%>
<%--/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/--%><%--ZD 100147 End--%>
<%@ Page Language="C#" AutoEventWireup="true" Inherits="ns_Login.genlogin" ValidateRequest="true" %>
<%--FB 2779 Starts--%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<meta http-equiv="X-UA-Compatible" content="IE=8" />
<%--FB 2779 Ends--%>
<html>
<head>
    <link rel="stylesheet" title="Expedite base styles" type="text/css" href="en/App_Themes/CSS/main.css" />
    <%--FB 2719 Starts--%>
    <style>
        body
        {
        	overflow:hidden;
        }
        #divBody
        {
            /* background: url(../image/company-logo/LoginBackground.jpg) no-repeat center center fixed; */
            /* background: url(../image/company-logo/bluePattern.png); */
            background-repeat:no-repeat;
            background-position:center center fixed;
            -webkit-background-size: cover;
            -moz-background-size: cover;
            -o-background-size: cover;
            background-size: cover;
            width:100%; /* FB 2976 */
            margin-top:25px;
            
        }
        #UserName
        {
            height: 40px;
            width: 275px;
            border: 1px solid #BEBEBE;
            background-color: #FFFFFF;
            color: gray;
            font-size: 15px;
            text-align: left;
            background-image: url('../image/user.png');
            background-repeat: no-repeat;
            background-position: right;
        }
        #UserPassword
        {
            height: 40px;
            width: 275px;
            border: 1px solid #BEBEBE;
            background-color: #FFFFFF;
            color: gray;
            font-size: 15px;
            text-align: left;
            background-image: url('../image/pwd.png');
            background-repeat: no-repeat;
            background-position: right;
        }
        #divFooter span
        {
            color: Black;
        }
    </style>
    <%--FB 2719 Ends--%>

    <script language="javascript" type="text/javascript">
    
        //FB-1669 Fix
        //    <!--
        if (document.images) {
            preload_image = new Image(25, 25);
            preload_image.src = "image/wait1.gif";
        }
        //-->
        //Fb-1669 Fix End here

        // FB 2719 Starts
       
        function fnTextFocus(xid, par) {
            if (navigator.userAgent.indexOf("MSIE") == -1) {
                var obj = document.getElementById(xid);
                if (par == 1) {
                    if (trim(obj.value) == "Username") {
                        obj.value = "";
                    }
                }
                else {
                    if (trim(obj.value) == "Password") {
                        obj.value = "";
                        obj.type = "password";
                    }
                }
            }
        }


        function fnTextBlur(xid, par) {
            if (navigator.userAgent.indexOf("MSIE") == -1) {
                var obj = document.getElementById(xid);
                if (par == 1) {
                    if (trim(obj.value) == "") {
                        obj.value = "Username";
                    }
                }
                else {
                    if (trim(obj.value) == "") {
                        obj.type = "text";
                        obj.value = "Password";
                    }
                }
            }
			
			// FB 2850 Starts FB 3055 Starts
            strUsr = Trim(document.frmGenlogin.UserName.value);
            var obj2 = document.getElementById("errLabel");
            if (obj2.innerHTML == "Please enter Username")
                obj2.innerHTML = "";
            if (strUsr == "Username" || strUsr == "")
                return false;

            if (strUsr.indexOf('@') == -1 && "<%=mailextn %>" != "") {

                //FB 1943


                if ("<%=mailextn %>" != "") {
                    strUsr = strUsr + "@" + "<%=mailextn %>";
                    document.frmGenlogin.UserName.value = strUsr;
                }
                else {

                    if (obj2) {

                        obj2.innerHTML = "Invalid Username";
                        DataLoading("0");
                    }
                    return false;
                }
            }
            else if ("<%=mailextn %>" != "") {
                var extn = strUsr.split('@')[1];
                var dotchk = extn.substring(1, extn.length - 1);
                if (dotchk.indexOf('.') == -1) {
                    obj2.innerHTML = "Invalid Username";
                }
            }
			// FB 2850 Ends FB 3055 Ends
			
        }

        function trim(stringToTrim) {
            return stringToTrim.replace(/^\s+|\s+$/g, "");
        }

        // FB 2719 Ends
    </script>

</head>
<!--  #INCLUDE FILE="inc/maintop5.aspx"  -->

<script language="JavaScript" src="inc/functions.js" type="text/javascript"></script>
<body>
<div id="divHeader" style="background-color: White; left: 0px; top: 0px; width: 100%;
    height: 25px; position: absolute; z-index: 100;"> <%--FB 2976 --%>
</div><%-- FB 2779 --%>
<div id="divFooter" style="background-color: White; bottom: 0; width: 100%; height: 50px;
    position: absolute; z-index: 100; left:0px"> <%--FB 2976 --%>
</div><%-- FB 2779 --%>
<div id="divBody">
<img style="position:absolute; z-index:-1 " id="divBodybg" width="100%" height="100%" alt="bg image" src="#" /> <%--FB 2976 --%>
<%--Added for FB 1648--%>
<% 
    HttpContext.Current.Application.Lock();
    //Version and Year upgradation start 
    HttpContext.Current.Application.Remove("CopyrightsDur");
    HttpContext.Current.Application.Remove("Version");
    HttpContext.Current.Application.Add("Version", "2.9.2.20.4");//Can Edit for version upgradation //FB 2864
    HttpContext.Current.Application.Add("CopyrightsDur", "2002-2013");//FB 1814//Can Edit for year upgradation //FB 2682
    //Version and Year upgradation end
    HttpContext.Current.Application.UnLock();

%>
<% if (wizard_enable)
   { %>

<script language="JavaScript" type="text/javascript">
	<!--

    var timerRunning = false;
    var toTimer = null;

    function startTimer() {
        toTimer = setTimeout('startTimer()', 1000);
        checkAndRefresh(0);
        timerRunning = true;
    }

    function stopTimer() {
        checkAndRefresh(1);
        if (timerRunning) {
            clearTimeout(toTimer);
        }
    }

    function NewbuttonLogin() {
        if (document.getElementById("UserName").value == "Username")
            document.getElementById("UserName").value = "";
        if (document.getElementById("UserPassword").value == "Password")
            document.getElementById("UserPassword").value = "";
        //if (document.getElementById('btnSubmit') != null) { // FB 2850
        //    document.getElementById('btnSubmit').click();
        //}
    }

    function validateLogin() {

        NewbuttonLogin(); // FB 2850

        DataLoading("1");
        var strUsr;
        var stralert;
        document.getElementById("errLabel").style.color = "#9E0B0F" // FB 2850
        var obj1 = document.getElementById("errLabel");
        if(obj1)
          obj1.innerHTML = "";

        if (document.frmGenlogin.UserName.value == "") {

            if (obj1) {

                obj1.innerHTML = "Please enter Username"
                if (navigator.userAgent.indexOf("MSIE") == -1) { // FB 2850
                    document.getElementById("UserName").value = "Username"
                    document.getElementById("UserPassword").value = "Password"
                }
                DataLoading("0");
                return false; // FB 2310
            }
            //return false; 
        }
        else {


            strUsr = Trim(document.frmGenlogin.UserName.value);


            // FB 2850 FB 3055 Starts
            if (strUsr.indexOf('@') == -1 && "<%=mailextn %>" != "") {

                //FB 1943


                if ("<%=mailextn %>" != "") {
                    strUsr = strUsr + "@" + "<%=mailextn %>";
                    document.frmGenlogin.UserName.value = strUsr;
                }
                else {

                    if (obj1) {

                        obj1.innerHTML = "Invalid Username";
                        DataLoading("0");
                    }
                    return false;
                }
            }

            //FB 1943
            if (checkInvalidChar(strUsr) == false) {

                if (obj1) {

                    obj1.innerHTML = "Invalid Username";
                    DataLoading("0");
                }
                return false;
            } // FB 3055 Ends
        }
        var str;
        str = Trim(document.frmGenlogin.UserPassword.value);



        if (str == "") {
            if (obj1) {
                obj1.innerHTML = "Please enter the password";
                if (navigator.userAgent.indexOf("MSIE") == -1) // FB 2850
                {
                    document.getElementById("UserPassword").value = "Password"
                    document.getElementById("UserPassword").type = "text"
                }
                DataLoading("0");
                return false; // FB 2310
            }
            //return false;
        }
        else {
            if (checkInvalidPassChar(str) == false) {//FB 2339
                DataLoading("0");
                return false;
            }
        }
        //checkAndRefresh(1);
        //DataLoading("0");
        return true;
    }

    function DataLoading(val) {

        if (val == "1")
            document.getElementById("dataLoadingDIV").innerHTML = '<b><img border="0" src="image/wait1.gif" >';  //Edited for FF FB-1669
        else
            document.getElementById("dataLoadingDIV").innerHTML = "";
    }


	//-->
</script>

<% }%>
<% if (Application["global"] != null) if (Application["global"].ToString() == "enable")
       {%>

<script language="JavaScript" type="text/javascript">
	<!--

    function international(cb) {
        if (cb.options[cb.selectedIndex].value != "") {
            str = "" + window.location;
            newstr = str.replace(/\/en\//i, "/" + cb.options[cb.selectedIndex].value + "/");
            if ((ind1 = newstr.indexOf("sct=")) != -1) {
                if ((ind2 = newstr.indexOf("&", ind1)) != -1)
                    newstr = newstr.substring(0, ind1 - 1) + newstr.substring(ind2, newstr.length - 1);
                else
                    newstr = newstr.substring(0, ind1 - 1);
            }
            newstr += ((newstr.indexOf("?") == -1) ? "?" : "&") + "sct=" + cb.selectedIndex;
            document.location = newstr;
        }
    }
	
	//-->
</script>

<% } %>
<table width="100%" border="0" cellpadding="0" cellspacing="0">
</table>
<br />
<center>
    <table width="100%" border="0" cellpadding="3" cellspacing="0">
        <tr>
            <td>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td width="100%" valign="top" align="center">
                <table width="100%" cellpadding="0" border="0" align="center">
                    <tr>
                        <td width="100%" valign="top" align="center">
                            <form id="frmGenlogin" method="post" runat="server" defaultbutton="btnCSS" onsubmit="return validateLogin();"> <%-- FB 2850 --%>
                            <div>
                                <input type="hidden" name="cmd" value="GetHome" />
                                <input type="hidden" name="start" value="0" />
                                <input type="hidden" name="init" value="1" />
                                <%--<input type="hidden" id="hdnscreenres" name="hdnscreenres" runat="server" />--%> <%--ZD 100157--%> <%--ZD 100335--%>
                                <center>
                                    <table id="tabSingin" border="0" cellpadding="0" cellspacing="0" style="background-color: #FFFFFF;
                                        -moz-border-radius: 10px; -webkit-border-radius: 10px; border-radius: 10px; border: 3px solid;
                                        font-family: Arial; font-size: medium; width: 290px; height: 400px;">
                                        <%--<tr valign="middle"> FB 2719
      <td width="100%" height="150" align="center"  colspan="2">
           <img id="siteLogoId" src="../image/company-logo/SiteLogo.jpg" onload="" alt="" /> <%-- FB 2050 --%>
                                        <%--width="234" height="84" FB 2407--%>
                                        <%--</td>
        </tr>--%>
                                        <tr align="left">
                                            <td width="40%" style="padding-left: 20px; 
 border-bottom: 1px solid #BEBEBE;" nowrap="nowrap" colspan="2">
                                                <img id="siteLogoId" src="../image/company-logo/SiteLogo.jpg" style="width: 200px;"
                                                    onload="" alt="" /><%-- FB 2789 --%>
                                            </td>
                                        </tr>                                        
                                        <tr align="left">
                                            <td width="40%" style="padding-left: 20px" nowrap="nowrap" colspan="2">
                                                <span style="color: #4E4F4F; font-size:23px; font-family:Calibri">Sign in</span>
                                            </td>
                                        </tr>
                                        <tr valign="top">
                                            <td width="100%" align="center" colspan="2">
                                                <div id="dataLoadingDIV" name="dataLoadingDIV" align="center">
                                                    
                                                </div> <%-- FB 2850 --%>
                                                <asp:label id="errLabel" runat="server" cssclass="lblError"></asp:label>
                                            </td>
                                        </tr>
                                        <tr valign="top">
                                            <%--<td style="width:141; vertical-align:middle" align="center"> FB 2719                  
                  <label for="UserName" class="blackblodtext">Username</label>
                </td>--%>
                                            <td colspan="2" align="center">
                                                <input type="text" name="UserName" runat="server" onfocus="javascript:return fnTextFocus(this.id,1)"
                                                    onblur="javascript:return fnTextBlur(this.id,1)" value="" id="UserName" maxlength="256"
                                                    onkeyup="javascript:chkLimit(this,'u');" /> <%-- FB 2851 --%>
                                            </td>
                                        </tr>
                                        <tr valign="top">
                                            <%--<td style="width:141; vertical-align:middle" align="center"> FB 2719
                    
                  <label for="UserPassword" class="blackblodtext">Password</label>
                </td>--%>
                                            <td colspan="2" align="center">
                                                <input type="password" name="UserPassword" onkeydown="if(event.keyCode == 13){document.getElementById('btnCSS').focus();document.getElementById('btnCSS').click();}" runat="server" id="UserPassword" value=""
                                                    onfocus="javascript:return fnTextFocus(this.id,2)" onblur="javascript:return fnTextBlur(this.id,2)"
                                                    maxlength="256" onkeyup="javascript:chkloginLimit(this,'5');" /><%--FB 2339 FB 2850--%>
                                            </td>
                                        </tr>
                                        <tr valign="top" style="height: 50px;">
                                            <td align="center" colspan="2">
                                                <asp:button id="btnSubmit2" runat="server" text="Log In" style="width: 250px; display: none;
                                                    height: 50px; background-color: Red" onclientclick="return validateLogin();"
                                                    /> <%-- 2850 --%>
                                                <asp:button id="btnCSS" runat="server" Text="SIGN IN" style="width: 275px; height: 50px;
                                                    color: #FFFFFF; font-size:larger; border: 0PX; background-color:#444444; font-weight:bold"
                                                    onclick="btnSubmit_Click" OnClientClick="return validateLogin();" /><!-- FB 2779 -->
                                                    
                                            </td>
                                        </tr>
                                        <tr valign="top" style="display: none">
                                            <td colspan="2" align="center">
                                                &nbsp;
                                            </td>
                                        </tr>
                                       
                                        <tr>
                                            <td colspan="2" valign="top" align="center" style="padding-top: 10px; padding-left: 20px">
                                                <table width="281px" border="0">
                                                    <tr valign="top">
                                                        <td id="lblForgotPassword" runat="server" style="width: 150" nowrap="nowrap">
                                                            &nbsp;<a href="emaillogin.aspx" style="color: #696B6E; font-size: small; font-weight: bold">Forgot
                                                                password?</a>
                                                        </td>
                                                        <% if (Application["GetActLNK"] != null) if (Application["GetActLNK"].ToString() == "enable")
                                                               { 
                                                        %>
                                                        <td id="lblViewPublicConf" runat="server" style="width: 150" nowrap="nowrap"><!-- FB 2858 -->
                                                            &nbsp;<a href="ConferenceList.aspx?t=4&hf=1" style="color: #4272A8; font-size: small;
                                                                font-weight: bold; white-space: nowrap">View public conference</a>
                                                        </td>
                                                        <% 
                                                            }   
                                                        %>
                                                        <% if (Application["ViwPubLNK"] != null) if (Application["ViwPubLNK"].ToString() == "enable")
                                                               {   
                                                        %>
                                                        <%--<tr valign="top">             
               <td style="width:141" align="center">                    
                  &nbsp;
                </td>
                <td style="width:245">&nbsp;<a href="ConferenceList.aspx?t=4&hf=1" onclick="javascript:DataLoading('1')">View Public Conferences?</a></td>
                </tr>--%>
                                                        <% 
                                                            }   
                                                        %>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </center>
                            </div>
                            </form>

                            <script language="JavaScript" type="text/javascript">
<!--

                                document.frmGenlogin.UserName.focus();
                                //	startTimer();

//-->
                            </script>

                            <!-------------------------------------------------------------------->
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</center>

<script language="javascript" type="text/javascript">
    if (document.layers) document.captureEvents(Event.KEYPRESS)
    document.onkeypress = kpress;
    function kpress(e) {
        key = (document.layers) ? e.which : window.event.keyCode

        if (key == 13) {

            if (document.getElementById('btnSubmit') != null) {
                //document.getElementById('btnSubmit').click();
            }

        }
    }
    
    // ZD 100263
    if ("<%=mailextn %>" == "") {
        document.getElementById("UserName").setAttribute("autocomplete", "off");
        document.getElementById("UserPassword").setAttribute("autocomplete", "off");
    }
</script>

<br />
<br />

<script language="JavaScript" type="text/javascript"> //mainbottom1.asp Code Lines
	var mt = "";
	var _d = document;

	mt += "<center><table border='0' cellpadding='2' cellspacing='2'>";
	mt += "<tr valign='bottom'>";
	mt += "<td colspan=5 align=center>";
    //FB 1985 - Starts
	if('<%=Application["Client"]%>'.toUpperCase() == "DISNEY")
	{
	    mt += "<span class=srcstext2>myVRM Version <%=Application["Version"].ToString()%>(c), Copyright <%=Application["CopyrightsDur"].ToString()%> myVRM.com.  All Rights Reserved.</span>";//FB 1648
	}
	else
	{
	    mt += "<span class=srcstext2>myVRM Version <%=Application["Version"].ToString()%>(c), Copyright <%=Application["CopyrightsDur"].ToString()%> <a href='http://www.myvrm.com' target='_blank'>myVRM.com</a>.  All Rights Reserved.</span>";//FB 1648
	}
    //FB 1985 - Starts
	mt += "</td>";
	mt += "</tr>";
	mt += "</table></center>";

    document.getElementById("divFooter").innerHTML = mt;
	//_d.write(mt)

</script>

<!--code added for CSS Module-->

<script language="javascript" type="text/javascript">
    //    function adjustWidth(obj)
    //    {
    //        alert('hi');
    //        if(obj.style.width == "")
    //        if (obj.src.indexOf("lobbytop1024.jpg") >= 0)
    //        {
    //            obj.style.width=window.screen.width-25;            

    var obj = document.getElementById('mainTop');
    if (obj != null) {
        //FB 1830
        //                if (window.screen.width <= 1024)
        //                    obj.src = "../en/image/company-logo/StdBanner.jpg"; // Organization Css Module 
        //                else
        //                    obj.src = "../en/image/company-logo/HighBanner.jpg";  //Organization Css Module 

        if (window.screen.width <= 1024)
            obj.src = "../image/company-logo/StdBanner.jpg"; // Organization Css Module  //FB 1830
        else
            obj.src = "../image/company-logo/HighBanner.jpg";  //Organization Css Module //FB 1830
    }
    //        }
    //    }

   
 
</script>

<!-- PB gradiend -->
<!-- FB 2050 Start -->

<script type="text/javascript">
    function refreshImage() {
        var obj = document.getElementById("siteLogoId");
        if (obj != null) {
            var src = obj.src;
            var pos = src.indexOf('?');
            if (pos >= 0) {
                src = src.substr(0, pos);
            }
            var date = new Date();
            obj.src = src + '?v=' + date.getTime();
        }
        return false;
    }
    if (navigator.userAgent.indexOf("Chrome") == -1) // FB 3055
        window.onload = refreshImage;

    //FB 2487 - Start
    var obj = document.getElementById("errLabel");
    if (obj != null) {
        var strInput = obj.innerHTML.toUpperCase();

        if ((strInput.indexOf("SUCCESS") > -1) && !(strInput.indexOf("UNSUCCESS") > -1) && !(strInput.indexOf("ERROR") > -1)) {
            obj.setAttribute("class", "lblMessage");
            obj.setAttribute("className", "lblMessage");
        }
        else {
            obj.setAttribute("class", "lblError");
            obj.setAttribute("className", "lblError");
        }
    }
    //FB 2487 - End
    // FB 2719 Starts
    /* Commented for FB 2976 
    if (screen.width > 900) {
        document.getElementById("divHeader").style.marginLeft = (screen.width-900)/2 + 'px';
        document.getElementById("divFooter").style.marginLeft = (screen.width - 900) / 2 + 'px';
        document.getElementById("divBody").style.marginLeft = (screen.width - 900) / 2 + 'px';
    } */

    if (navigator.userAgent.indexOf("MSIE") == -1) {
        if(document.getElementById("UserName").value == "") // FB 2850
            document.getElementById("UserName").value = "Username";
        document.getElementById("UserPassword").type = "text";
        document.getElementById("UserPassword").value = "Password";
        document.getElementById("btnCSS").focus();
    }
    
    // FB 2779 Starts
    var themeType = '<%=Session["ThemeType"]%>';
    if (themeType == 2) {
        document.getElementById("divBodybg").src = "../image/company-logo/redPattern.png"; // FB 2976
        document.getElementById("tabSingin").style.borderColor = "#CD2522";
    }
    else if (themeType == 1) {
    document.getElementById("divBodybg").src = "../image/company-logo/bluePattern.png"; // FB 2976
        document.getElementById("tabSingin").style.borderColor = "#4277B3";
    }
    else {
        var stat = '<%=exist%>';
        if(stat == 1)
            document.getElementById("divBodybg").src = "../image/company-logo/LoginBackground.jpg"; // FB 2976
        else
            document.getElementById("divBodybg").src = "../image/company-logo/bluePattern.png"; // FB 2976
        document.getElementById("tabSingin").style.borderColor = "#888888";
    }
    // FB 2779 Ends
        
    // FB 2719 Ends


    //document.getElementById("hdnscreenres").value = screen.width; //ZD 100157 //ZD 100335
    

    
</script>
</div>
<!-- FB 2050 End -->
</body>
</html>
