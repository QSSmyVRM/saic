<%--ZD 100147 start--%>
<%--/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/--%><%--ZD 100147 End--%>
<script type="text/javascript">  // FB 2815
   var path = '<%=Session["OrgCSSPath"]%>';
   path = path.split('.')[0] + '<%=Session["ThemeType"]%>' + ".css";
   document.write('<link rel="stylesheet" title="Expedite base styles" type="text/css" href="' + path + '?' + new Date().getTime() + '">');
</script>
<body bottommargin="0" leftmargin="0" rightmargin="0" topmargin="0" marginheight="0" marginwidth="0">
<script language="JavaScript" src="inc/functions.js"></script>
<script type="text/javascript" src="script/wincheck.js"></script> <%--FB 3055--%>

<!-- JavaScript begin -->
<script language="JavaScript">
<!--

var	oldpartyno = 0;
var	newpartyno = 0;

if (parent.document.frmManagegroup2.ConfID)
	var isCreate = ( (parent.document.frmManagegroup2.ConfID.value == "new") ? true : false ) ;
else	// template
	var isCreate = true ;

function deleteBlankLine(linenum, str)
{
	var newstr = "";
	
	strary = str.split("||"); // FB 1888
	for (var i=linenum; i < strary.length-1; i++) {
		newstr += strary[i] + "||"; // FB 1888
	}
	return newstr;
}


function addBlankLine(linenum, str)
{
	var newstr = str;
	
	for (var i=0; i < linenum; i++) {
		newstr = "!!!!!!!!0||" + newstr; //FB 1888
	}
	return newstr;
}


function assemble(ary, endstr)
{
	var newstr = "";
	for (var i=0; i < ary.length-1; i++) {
		newstr += ary[i] + endstr;
	}
	newstr += ary[ary.length-1];
	return newstr;
}


function checkNewParty(fncb, lncb, emlcb)
{
//alert(emlcb.value)
	if ((Trim(fncb.value) == "[First Name]") && (Trim(lncb.value) == "[Last Name]") && (Trim(emlcb.value) == ""))
		return true;
	if (Trim(fncb.value) == "[First Name]") {
		alert(EN_4)
		fncb.focus();
		return false;
	}
	if (Trim(lncb.value) == "[Last Name]") {
		alert(EN_5)
		lncb.focus();
		return false;
	}
	if (Trim(emlcb.value) == "") {
		alert(EN_6)
		emlcb.focus();
		return false;
	}
	if (checkemail(Trim(emlcb.value)))
		return true;
	else {
		alert(EN_6);
		emlcb.focus();
		return false;
	}
}


function bfrRefresh()
{
	partysinfo = parent.document.frmManagegroup2.PartysInfo.value;	
//alert("before refresh=" + partysinfo);
//alert("newpartyno=" + newpartyno);
	eno = -1;
	cornew = 0;
	partysinfo = deleteBlankLine(newpartyno, partysinfo);
//alert("after deleteBlankLine=" + partysinfo);
//alert(document.frmManagegroup2party.elements[eno+1] + ";" + document.frmManagegroup2party.elements[eno+2] + ";" + document.frmManagegroup2party.elements[eno+3]);
	
//=== need add: check input valid.
//=== if above check is wrong, then return false cause two calling function break.
	for (i=0; i<newpartyno; i++) {
		willContinue = checkNewParty(document.frmManagegroup2party.elements[eno+1], document.frmManagegroup2party.elements[eno+2], document.frmManagegroup2party.elements[eno+3]);
		if (!willContinue)
			return false;
		if ( (document.frmManagegroup2party.elements[eno+3].value != "") && (partysinfo.indexOf("!!" + document.frmManagegroup2party.elements[eno+3].value + "!!") == -1) ) {  // FB 1888
			partysinfo = "new!!" + document.frmManagegroup2party.elements[++eno].value + "!!" + document.frmManagegroup2party.elements[++eno].value + "!!" +    // FB 1888
							document.frmManagegroup2party.elements[++eno].value + "!!-1||" + partysinfo; // FB 1888
			cornew ++;
		} else {
			eno += 3;
		}
	}
//alert("after New Party=" + partysinfo);

	partysinfo = addBlankLine(newpartyno-cornew, partysinfo);
//alert("after addBlankLine=" + partysinfo);


	parent.document.frmManagegroup2.PartysInfo.value = partysinfo;	
//alert("after bfrRefresh=" + parent.document.frmManagegroup2.PartysInfo.value);
	
	return true;
}


function nFocus(cb)
{
	if ( (cb.value=="[First Name]") || (cb.value=="[Last Name]") ) {
		cb.value = "";
	}
}


function nBlur(cb)
{
	if ( ((cb.name).indexOf("NewParticipantFirstName") != -1) && (cb.value=="") ) {
		cb.value = "[First Name]";
	}
	
	if ( ((cb.name).indexOf("NewParticipantLastName") != -1) && (cb.value=="") ) {
		cb.value = "[Last Name]";
	}
}


function deleteThisParty(thisparty)
{
	var needRemove = confirm("Are you sure you want to remove this participant?")
	if (needRemove == true) {
		if (thisparty != "") {
			willContinue = bfrRefresh(); 
			if (willContinue) {
				partysinfo = parent.document.frmManagegroup2.PartysInfo.value;	

				if ( (tmploc = partysinfo.indexOf("!!" + thisparty + "!!")) != -1 ) // FB 1888
					parent.document.frmManagegroup2.PartysInfo.value = partysinfo.substring(0, partysinfo.lastIndexOf("||", tmploc)+1) + partysinfo.substring(partysinfo.indexOf("||", tmploc)+1, partysinfo.length);;
			
				//history.go(0);
				parent.refreshIframe(); // FB 2050
			}
		} else {
				partysinfo = parent.document.frmManagegroup2.PartysInfo.value;	
				
				if ( (tmploc = partysinfo.indexOf("!!" + thisparty + "!!")) != -1 ) // FB 1888
					parent.document.frmManagegroup2.PartysInfo.value = partysinfo.substring(0, partysinfo.lastIndexOf("||", tmploc)+1) + partysinfo.substring(partysinfo.indexOf("||", tmploc)+1, partysinfo.length);;
			
				//history.go(0);
				parent.refreshIframe(); // FB 2050
		}
	}
}


function frmManagegroup2party_Validator()
{
	return true;
}

//-->
</script>
<!-- JavaScript finish -->


<!-- JavaScript begin -->
<script language="JavaScript">
<!--

	if (parent.document.frmManagegroup2.PartysInfo) {
		partysinfo = parent.document.frmManagegroup2.PartysInfo.value;
	} else {
		partysinfo = ""
		setTimeout('window.location.reload();',500);
	}

//alert(partysinfo)

	_d = document;
	var mt = "";
	
	mt += "<div align='center'>";
	mt += "  <form name='frmManagegroup2party' method='POST' action='dispatcher/userdispatcher.asp' onsubmit='return frmManagegroup2party_Validator()' language='JavaScript'>"
	mt += "  <table border='0' cellpadding='3' cellspacing='1' width='100%'>";
	partysary = partysinfo.split("||"); // FB 1888
	for (var i=0; i < partysary.length-1; i++)
	 {
		tdbgcolor = (i % 2 == 0) ? "" : "#E0E0E0";
		partyary = partysary[i].split("!!"); // FB 1888
		partyemail = partyary[3];

		if (partyemail=="") {
			newpartyno++;
			mt += "    <tr class='tableBody'>"
            mt += "      <td align='center' width='3%' >"
			mt += "        <a href='#' onCLick='Javascript:deleteThisParty(\"" + partyary[3] + "\")'><img border='0' src='image/btn_delete.gif' alt='Delete' width='18' height='18' style='cursor:pointer;' title='Delete'></a>"
            mt += "      </td>"
            mt += "      <td align='center' width='28%' >"
            mt += "        <input type='text' name='NewParticipantFirstName" + newpartyno + "' size='10' value='[First Name]' onFocus='JavaScript: nFocus(this);' onBlur='JavaScript: nBlur(this);' maxlength='256' class='altText'>"
            mt += "        <input type='text' name='NewParticipantLastName" + newpartyno + "' size='10' value='[Last Name]' onFocus='JavaScript: nFocus(this);' onBlur='JavaScript: nBlur(this);' maxlength='256' class='altText'>"
            mt += "      </td>"
            mt += "      <td align='center' width='27%>"
            mt += "        <input type='text' name='NewParticipantEmail" + newpartyno + "' size='15' maxlength='512' class='altText'>"
            mt += "      </td>"
			mt += "    </tr>"
		
		} else {
			oldpartyno++;
			//Window Dressing - start
			//FB 1888 Starts
			partyary[1]= replaceSpcChar(partyary[1].replace(new RegExp("~","g"), "'"));
			partyary[2]= replaceSpcChar(partyary[2].replace(new RegExp("~","g"), "'"));
			
			mt += "    <tr class='tableBody'>"
			mt += "      <td align='center' class='tableBody' width='3%' >" + " <a href='#' onCLick='Javascript:deleteThisParty(\"" + partyary[3] + "\")'><img border='0' src='image/btn_delete.gif' alt='Delete' width='18' height='18' style='cursor:pointer;' title='Delete'></a></td>"
			mt += "      <td align='center' class='tableBody' width='28%'>" + partyary[1] + " " + partyary[2] + "</td>" // FB 1888
			mt += "      <td align='center' class='tableBody' width='27%' >";
			if (partyary[2].replace(/\s/g, "") != "") //FB 2023
			    mt += partyary[3] + "</td>";
			else
			    mt += "</td>";
			    
			//Window Dressing - end
			mt += "    </tr>"
		}
	}
		
	mt += "  </table>"
	mt += "  </form>"

	_d.write(mt)

// FB 1888
function replaceSpcChar(strName)
{
   do
    {
      strName = strName.replace("?", "\"");
    }while((strName.indexOf('?')) >= "0");
    
    return strName;
}

//-->
</script>
<!-- JavaScript finish -->


	 
</body>

</html>
