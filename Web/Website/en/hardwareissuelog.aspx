<%--ZD 100147 start--%>
<%--/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/--%><%--ZD 100147 End--%>
<%@ Page Language="C#" Inherits="ns_Administration.eventLog" %>
<%@ Register TagPrefix="mbcbb" Namespace="MetaBuilders.WebControls" Assembly="MetaBuilders.WebControls.ComboBox" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> <!-- FB 2050 -->
<meta http-equiv="X-UA-Compatible" content="IE=8" /> <!-- FB 2050 --> <%--FB 2779--%>
<!--Window Dressing-->
<!-- #INCLUDE FILE="inc/maintopNET.aspx" --> 

<script type="text/javascript" src="inc/functions.js"></script>

<!--<link rel="stylesheet" type="text/css" media="all" href="css/calendar-win2k-1.css" title="win2k-1" />-->

<script type="text/javascript">
  var servertoday = new Date();
</script>
<script type="text/javascript" src="script/cal.js"></script>
<script type="text/javascript" src="lang/calendar-en.js"></script>
<script type="text/javascript" src="script/calendar-setup.js"></script>

<script type="text/javascript">
  function deleteLog(moduleName, deleteAll)
  {
    frmSetLog.action = "hardwareissuelog.aspx?m=" + moduleName + "&d=" + deleteAll;
    frmSetLog.submit();
  }
</script>

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>myVRM Event Logs Page</title>
</head>
<body>
  <input type="hidden" id="helpPage" value="34">

        <center>
      <h3>Event Logs</h3>
      <br />
      <asp:Label ID="lblMessage" runat="server" Visible="false" CssClass="lblError"></asp:Label>
                </center>
 <center>   
    <form name="frmSetLog" id="frmSetLog" method="Post" action="hardwareissuelog.aspx" onsubmit="return true" language="JavaScript" runat="server">
        &nbsp;<table border="0" cellpadding="2" cellspacing="2" width="900">
	        <tr>
		        <td width="95%" valign=top>
        			<input type="hidden" name="SetLog" value="SetLogPreferences">
			        <table border="0" cellpadding="0" cellspacing="0" width="90%">        	
				        <tr>
					        <td width="1%">&nbsp;</td>
					        <td width="98%" align=left>
						        <SPAN class=subtitleblueblodtext>Log Settings</SPAN><br>
					        </td>
					        <td width="1%">&nbsp;</td>
				        </tr>
        				
				        <tr><td colspan=3>&nbsp;</td></tr>
				        <tr>
					        <td width="1%">&nbsp;</td>
					        <td height=200 valign=top>
					        <%--Window Dressing--%>
                                <asp:Table ID="tblLogPref" runat="server" CssClass="tableBody" CellPadding="4" CellSpacing="0">
                                </asp:Table> 
					        </td>
					        <td width="1%">&nbsp;</td>
				        </tr>
                        
                        
						<tr>
					        <td width="1%">&nbsp;</td>
					        <td align=center>
						        <table border="0" cellspacing="0" cellpadding="0">
						        <tr>
						          <td align="center">
						            <input type="reset" name="SetlogReset" value="Reset" class="altShortBlueButtonFormat">
						          </td>
						          <td width="5%"></td>
						          <td align="center">
                                      <asp:Button ID="btnSubmit" runat="server" Text="Update" CssClass="altShortBlueButtonFormat" OnClick="btnSubmit_Click" />
						          </td>
						        </tr>
						        </table>
        					
					        </td>
					        <td width="1%">&nbsp;</td>
				        </tr>
			        
			        </table>
        			
		        </td>
        		
	        </tr>
	        
	        <tr><td colspan=3>&nbsp;</td></tr>
	        
	        <tr>
		        <td height="2" colspan=3 align="center">
			        <img border="0" src="image/aqualine.gif" width="95%" height="2" valign="middle">
		        </td>
	        </tr>
        	
	        <tr>
		        <td valign=top>
       	
			        <table border="0" cellpadding="0" cellspacing="0" width="90%">
				        <tr>
					        <td width="1%">&nbsp;</td>
					        <td align=left>
						        <SPAN class=subtitleblueblodtext>Search Logs</SPAN><br>
					        </td>
					        <td width="1%">&nbsp;</td>
				        </tr>
        				
				        <tr><td colspan="3">
				        <table border="0" width="90%" align="right">
				        <tr>
					        <td width="1%">&nbsp;</td>
					        <td align="left">
					            <%--Window Dressing--%>
						        <label class="blackblodtext"><b>From</b></label><br>
                                <asp:TextBox ID="startDate" runat="server" CssClass="altText" ValidationGroup="Search"></asp:TextBox>
						        <%--  <!--//Code changed by Offshore for FB Issue 1073 -- Start
							        onclick="return showCalendar('<%=startDate.ClientID%>', 'cal_trigger1', -1, '%m/%d/%y');" />-->--%>
						        <img src="image/calendar.gif" border="0" width="20" height="20" id="cal_trigger1" style="cursor: pointer;" title="Date selector"
							        onclick="return showCalendar('<%=startDate.ClientID%>', 'cal_trigger1', -1, '<%=format%>');" />
							     <!--//Code changed by Offshore for FB Issue 1073 -- End-->
                                <asp:RequiredFieldValidator ID="startDateRequired" runat="server" ControlToValidate="startDate"
                                    Display="Dynamic" ErrorMessage="* Required" Font-Size="Smaller" SetFocusOnError="True" ValidationGroup="Search"></asp:RequiredFieldValidator>								
					            <br />
					            <asp:CompareValidator ID="startDateComp" runat="server" ErrorMessage="Must start before To date" Font-Size="Smaller" Display="Dynamic" ControlToCompare="endDate" ControlToValidate="startDate" CultureInvariantValues="true" SetFocusOnError="true" Operator="LessThanEqual" ValueToCompare="Search"></asp:CompareValidator>
                                <%-- <!--//Code changed by Offshore for FB Issue 1073 -- Start
                                <asp:RegularExpressionValidator ID="startDateRegEx" runat="server" ErrorMessage="e.g. 5/29/2007" Display="Dynamic" Font-Size="Smaller" ControlToValidate="startDate" SetFocusOnError="true" ValidationExpression="^(((((((0?[13578])|(1[02]))[\-/]?((0?[1-9])|([12]\d)|(3[01])))|(((0?[469])|(11))[\-/]?((0?[1-9])|([12]\d)|(30)))|((0?2)[\-/]?((0?[1-9])|(1\d)|(2[0-8]))))[\-/]?(((19)|(20))?([\d][\d]))))|((0?2)[\.\-/]?(29)[\-/]?(((19)|(20))?(([02468][048])|([13579][26])))))$" ValidationGroup="Search"></asp:RegularExpressionValidator>-->--%>
                                <asp:RegularExpressionValidator ID="startDateRegEx" runat="server" ErrorMessage="Invalid Date <%=format%>" Display="Dynamic" Font-Size="Smaller" ControlToValidate="startDate" SetFocusOnError="true" ValidationExpression="^(((((((0?[13578])|(1[02]))[\-/]?((0?[1-9])|([12]\d)|(3[01])))|(((0?[469])|(11))[\-/]?((0?[1-9])|([12]\d)|(30)))|((0?2)[\-/]?((0?[1-9])|(1\d)|(2[0-8]))))[\-/]?(((19)|(20))?([\d][\d]))))|((0?2)[\.\-/]?(29)[\-/]?(((19)|(20))?(([02468][048])|([13579][26])))))$|(0[1-9]|[12][0-9]|3[01])[- /.](0[1-9]|1[012])[- /.](19|20)\d\d" ValidationGroup="Search"></asp:RegularExpressionValidator>
                               <%-- <!--//Code changed by Offshore for FB Issue 1073 -- End-->--%>
					        </td>
					        <td>&nbsp;</td>
					        <td align="left">
					            <%--Window Dressing--%>
						        <label class="blackblodtext"> <b>Time</b></label><br>
						        <table CELLSPACING="0" CELLPADDING="0" BORDER="0">
							        <tr>
								        <td align="left">
									        <mbcbb:ComboBox ID="startTimeCombo" runat=server CssClass="altSelectFormat" Width=150 CausesValidation=true ValidationGroup="Search">
                                                <asp:ListItem Value="12:00AM">12:00AM</asp:ListItem>
                                                <asp:ListItem Value="01:00AM">01:00AM</asp:ListItem>
                                                <asp:ListItem Value="01:30AM">01:30AM</asp:ListItem>
                                                <asp:ListItem Value="02:00AM">02:00AM</asp:ListItem>
                                                <asp:ListItem Value="02:30AM">02:30AM</asp:ListItem>
                                                <asp:ListItem Value="03:00AM">03:00AM</asp:ListItem>
                                                <asp:ListItem Value="03:30AM">03:30AM</asp:ListItem>
                                                <asp:ListItem Value="04:00AM">04:00AM</asp:ListItem>
                                                <asp:ListItem Value="04:30AM">04:30AM</asp:ListItem>
                                                <asp:ListItem Value="05:00AM">05:00AM</asp:ListItem>
                                                <asp:ListItem Value="05:30AM">05:30AM</asp:ListItem>
                                                <asp:ListItem Value="06:00AM">06:00AM</asp:ListItem>
                                                <asp:ListItem Value="06:30AM">06:30AM</asp:ListItem>
                                                <asp:ListItem Value="07:00AM">07:00AM</asp:ListItem>
                                                <asp:ListItem Value="08:00AM">08:00AM</asp:ListItem>
                                                <asp:ListItem Value="08:30AM">08:30AM</asp:ListItem>
                                                <asp:ListItem Value="09:00AM">09:00AM</asp:ListItem>
                                                <asp:ListItem Value="09:30AM">09:30AM</asp:ListItem>
                                                <asp:ListItem Value="10:00AM">10:00AM</asp:ListItem>
                                                <asp:ListItem Value="10:30AM">10:30AM</asp:ListItem>
                                                <asp:ListItem Value="11:00AM">11:00AM</asp:ListItem>
                                                <asp:ListItem Value="11:30AM">11:30AM</asp:ListItem>
                                                <asp:ListItem Value="12:00PM">12:00PM</asp:ListItem>
                                                <asp:ListItem Value="12:30PM">12:30PM</asp:ListItem>
                                                <asp:ListItem Value="01:00PM">01:00PM</asp:ListItem>
                                                <asp:ListItem Value="01:30PM">01:30PM</asp:ListItem>
                                                <asp:ListItem Value="02:00PM">02:00PM</asp:ListItem>
                                                <asp:ListItem Value="02:30PM">02:30PM</asp:ListItem>
                                                <asp:ListItem Value="03:00PM">03:00PM</asp:ListItem>
                                                <asp:ListItem Value="03:30PM">03:30PM</asp:ListItem>
                                                <asp:ListItem Value="04:00PM">04:00PM</asp:ListItem>
                                                <asp:ListItem Value="04:30PM">04:30PM</asp:ListItem>
                                                <asp:ListItem Value="05:00PM">05:00PM</asp:ListItem>
                                                <asp:ListItem Value="05:30PM">05:30PM</asp:ListItem>
                                                <asp:ListItem Value="06:00PM">06:00PM</asp:ListItem>
                                                <asp:ListItem Value="06:30PM">06:30PM</asp:ListItem>
                                                <asp:ListItem Value="07:00PM">07:00PM</asp:ListItem>
                                                <asp:ListItem Value="08:00PM">08:00PM</asp:ListItem>
                                                <asp:ListItem Value="08:30PM">08:30PM</asp:ListItem>
                                                <asp:ListItem Value="09:00PM">09:00PM</asp:ListItem>
                                                <asp:ListItem Value="09:30PM">09:30PM</asp:ListItem>
                                                <asp:ListItem Value="10:00PM">10:00PM</asp:ListItem>
                                                <asp:ListItem Value="10:30PM">10:30PM</asp:ListItem>
                                                <asp:ListItem Value="11:00PM">11:00PM</asp:ListItem>
                                                <asp:ListItem Value="11:30PM">11:30PM</asp:ListItem>
                                            </mbcbb:ComboBox>
                                            <asp:RequiredFieldValidator ID="startTimeReq" runat="server" ErrorMessage="* Required" ControlToValidate="startTimeCombo" Font-Size="Smaller" Display="Dynamic" SetFocusOnError="true" ValidationGroup="Search"></asp:RequiredFieldValidator>
                                            <asp:RegularExpressionValidator ID="startTimeRegEx" runat="server" Font-Size="Smaller" ControlToValidate="startTimeCombo" ErrorMessage="Invalid Time (HH:MM AM/PM)" ValidationExpression="[0|1][0-9]:[0-5][0-9] [a|A|p|P][M|m]" Display="Dynamic" SetFocusOnError="true" ValidationGroup="Search"></asp:RegularExpressionValidator>
                                            
                                           &nbsp;&nbsp;
                                    </tr>
						        </table>
					        </td>
        					
					        <td width="1%">&nbsp;</td>
				        </tr>
        				
				        <tr>
					        <td width="1%">&nbsp;</td>
					        <td align="left">
					            <%--Window Dressing--%>
						        <label class="blackblodtext"> <b>To</b></label><br>
                                <asp:TextBox ID="endDate" runat="server" CssClass="altText" ValidationGroup="Search"></asp:TextBox>
						        <%-- <!--//Code changed by Offshore for FB Issue 1073 -- Start--%>
						        <img src="image/calendar.gif" border="0" width="20" height="20" id="cal_trigger2" style="cursor: pointer;" title="Date selector"
							        onclick="return showCalendar('<%=endDate.ClientID%>', 'cal_trigger2', -1, '<%=format%>');" />
							   <%-- <!--//Code changed by Offshore for FB Issue 1073 -- End--%>
                                <asp:RequiredFieldValidator ID="endDateRequired" runat="server" ControlToValidate="endDate"
                                    Display="Dynamic" ErrorMessage="* Required" Font-Size="Smaller" SetFocusOnError="True" ValidationGroup="Search"></asp:RequiredFieldValidator></td>								
					        <td>&nbsp;</td>
					        <td align="left">
					            <%--Window Dressing--%>
						        <label class="blackblodtext">  <b>Time</b></label><br>
						        <table CELLSPACING="0" CELLPADDING="0" BORDER="0">
							        <tr>
							        	<td align="left" style="height: 24px">
									        <mbcbb:ComboBox ID="endTimeCombo" runat=server  CssClass="altSelectFormat" Width=150 CausesValidation="true" ValidationGroup="Search">
                                                <asp:ListItem Value="12:00AM">12:00AM</asp:ListItem>
                                                <asp:ListItem Value="01:00AM">01:00AM</asp:ListItem>
                                                <asp:ListItem Value="01:30AM">01:30AM</asp:ListItem>
                                                <asp:ListItem Value="02:00AM">02:00AM</asp:ListItem>
                                                <asp:ListItem Value="02:30AM">02:30AM</asp:ListItem>
                                                <asp:ListItem Value="03:00AM">03:00AM</asp:ListItem>
                                                <asp:ListItem Value="03:30AM">03:30AM</asp:ListItem>
                                                <asp:ListItem Value="04:00AM">04:00AM</asp:ListItem>
                                                <asp:ListItem Value="04:30AM">04:30AM</asp:ListItem>
                                                <asp:ListItem Value="05:00AM">05:00AM</asp:ListItem>
                                                <asp:ListItem Value="05:30AM">05:30AM</asp:ListItem>
                                                <asp:ListItem Value="06:00AM">06:00AM</asp:ListItem>
                                                <asp:ListItem Value="06:30AM">06:30AM</asp:ListItem>
                                                <asp:ListItem Value="07:00AM">07:00AM</asp:ListItem>
                                                <asp:ListItem Value="08:00AM">08:00AM</asp:ListItem>
                                                <asp:ListItem Value="08:30AM">08:30AM</asp:ListItem>
                                                <asp:ListItem Value="09:00AM">09:00AM</asp:ListItem>
                                                <asp:ListItem Value="09:30AM">09:30AM</asp:ListItem>
                                                <asp:ListItem Value="10:00AM">10:00AM</asp:ListItem>
                                                <asp:ListItem Value="10:30AM">10:30AM</asp:ListItem>
                                                <asp:ListItem Value="11:00AM">11:00AM</asp:ListItem>
                                                <asp:ListItem Value="11:30AM">11:30AM</asp:ListItem>
                                                <asp:ListItem Value="12:00PM">12:00PM</asp:ListItem>
                                                <asp:ListItem Value="12:30PM">12:30PM</asp:ListItem>
                                                <asp:ListItem Value="01:00PM">01:00PM</asp:ListItem>
                                                <asp:ListItem Value="01:30PM">01:30PM</asp:ListItem>
                                                <asp:ListItem Value="02:00PM">02:00PM</asp:ListItem>
                                                <asp:ListItem Value="02:30PM">02:30PM</asp:ListItem>
                                                <asp:ListItem Value="03:00PM">03:00PM</asp:ListItem>
                                                <asp:ListItem Value="03:30PM">03:30PM</asp:ListItem>
                                                <asp:ListItem Value="04:00PM">04:00PM</asp:ListItem>
                                                <asp:ListItem Value="04:30PM">04:30PM</asp:ListItem>
                                                <asp:ListItem Value="05:00PM">05:00PM</asp:ListItem>
                                                <asp:ListItem Value="05:30PM">05:30PM</asp:ListItem>
                                                <asp:ListItem Value="06:00PM">06:00PM</asp:ListItem>
                                                <asp:ListItem Value="06:30PM">06:30PM</asp:ListItem>
                                                <asp:ListItem Value="07:00PM">07:00PM</asp:ListItem>
                                                <asp:ListItem Value="08:00PM">08:00PM</asp:ListItem>
                                                <asp:ListItem Value="08:30PM">08:30PM</asp:ListItem>
                                                <asp:ListItem Value="09:00PM">09:00PM</asp:ListItem>
                                                <asp:ListItem Value="09:30PM">09:30PM</asp:ListItem>
                                                <asp:ListItem Value="10:00PM">10:00PM</asp:ListItem>
                                                <asp:ListItem Value="10:30PM">10:30PM</asp:ListItem>
                                                <asp:ListItem Value="11:00PM">11:00PM</asp:ListItem>
                                                <asp:ListItem Value="11:30PM">11:30PM</asp:ListItem>
									        </mbcbb:ComboBox>
                                            <asp:RequiredFieldValidator ID="endTimeReq" runat="server" ErrorMessage="* Required" ControlToValidate="endTimeCombo" Font-Size="Smaller" Display="Dynamic" SetFocusOnError="true" ValidationGroup="Search"></asp:RequiredFieldValidator>
                                            <asp:RegularExpressionValidator ID="endTimeRegEx" runat="server" ErrorMessage="Invalid Time (HH:MM AM/PM)" ValidationExpression="[0|1][0-9]:[0-5][0-9] [a|A|p|P][M|m]" Font-Size="Smaller" ControlToValidate="endTimeCombo" Display="Dynamic" SetFocusOnError="true" ValidationGroup="Search"></asp:RegularExpressionValidator>

                                            &nbsp;&nbsp;
								        </td>
							        </tr>
						        </table>
					        </td>
					        <td width="1%">&nbsp;</td>
				        </tr>
        				
				        <tr><td colspan=3>&nbsp;</td></tr>
				        <tr>
					        <td width="1%">&nbsp;</td>
					        <td align="left" valign=top>
					            <%--Window Dressing--%>
						        <label class="blackblodtext"> <b>Module</b></label><br> 
                                <asp:DropDownList CssClass="altText" ID="moduleType" runat="server">
                                </asp:DropDownList>
					        </td>
					        <td>&nbsp;</td>
					        <td valign=top align=left>
					            <%--Window Dressing--%>
					            <label class="blackblodtext"><b>Error Code</b></label><br> 
                                <asp:TextBox ID="txtErrorCode" runat="server" CssClass="altText"></asp:TextBox>
                                <asp:RegularExpressionValidator ID="RegularExpressionValidator4" ControlToValidate="txtErrorCode" Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<br>& < > ' + % \ / ( ) ; ? | ^ = ! ` , [ ] { } : # $ @ ~ and &#34; are invalid characters." ValidationExpression="^(a-z|A-Z|0-9)*[^\\/<>+;?|!`,\[\]{}\x22;=^:@#$%&()'~]*$"></asp:RegularExpressionValidator>
						    </td>
					        <td width="1%">&nbsp;</td>
				        </tr>
				        <tr><td colspan=3>&nbsp;</td></tr>
				        <tr>
					        <td width="1%">&nbsp;</td>
					        <td valign=top colspan=3 align=left>
						        <%--Window Dressing--%>
						        <label class="blackblodtext">  <b>Error Message</b></label><br>  
                                <asp:TextBox ID="ErrorMessage" runat="server" MaxLength="4000" Rows="3" TextMode="MultiLine" CssClass="altText"></asp:TextBox>
                                <asp:RegularExpressionValidator ID="RegularExpressionValidator1" ControlToValidate="ErrorMessage" Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<br>& < > ' + % \ / ( ) ; ? | ^ = ! ` , [ ] { } : # $ @ ~ and &#34; are invalid characters." ValidationExpression="^(a-z|A-Z|0-9)*[^\\/<>+;?|!`,\[\]{}\x22;=^:@#$%&()'~]*$"></asp:RegularExpressionValidator>
					        </td>
					        <td width="1%">&nbsp;</td>
				        </tr>
				        <tr><td colspan=3>&nbsp;</td></tr>
				        <tr>
					        <td width="1%">&nbsp;</td>
					         <%--Window Dressing start --%>
					        <td align=left width=50% valign=top colspan=3 class="blackblodtext">
						        <label class="blackblodtext"> <b>Event</b></label>
					            <%--Window Dressing end--%>
						        <asp:CustomValidator ID="validateSeverity" runat="server" ErrorMessage="Please select at least one option" Font-Size="Smaller" OnServerValidate="validateSeverity_ServerValidate" Display="Dynamic" SetFocusOnError="true" AutoPostBack="true" ValidationGroup="Search"></asp:CustomValidator>
						        <table border="0" cellpadding="3" cellspacing="0" width="100%">
						          <tr>
						           <%--Window Dressing --%>
						          <td class="blackblodtext">
						          <asp:CheckBoxList ID="Severity" runat="server" RepeatColumns="3" RepeatDirection="Horizontal" Height="50px" Width="600px">
                                      </asp:CheckBoxList>
                                      
						          </td>			    
						          </tr>
						        </table>
					        </td>
					        <td width="1%">&nbsp;</td>
				        </tr></table></td></tr>
				        <tr><td colspan=3>&nbsp;</td></tr>
				        <tr>
					        <td width="1%">&nbsp;</td>
					        <td colspan=3 align=center>
						        <table border="0" cellspacing="0" cellpadding="0">
						        <tr>
						          <td align="center">
						            <input type="reset" name="HardwareissuelogSubmit1" value="Reset" class="altShortBlueButtonFormat">
						          </td>
						          <td width="5%"></td>
						          <td align="center">
                                      <asp:Button ID="searchSubmit" runat="server" Text="Search" CssClass="altShortBlueButtonFormat" OnClick="searchSubmit_Click" ValidationGroup="Search" />
						          </td>
						        </tr>
						        </table>
					        </td>
        					
					        <td width="1%">&nbsp;</td>
				        </tr>
				        <tr><td colspan=5>&nbsp;</td></tr>
        		
        				
        				
			        </table>
			        
		        </td>
	        </tr>
        	
	        <tr>
		        <td colspan=3 align="center" style="height: 4px">
			        <img border="0" src="image/aqualine.gif" width="95%" height="2" valign="middle">
		        </td>
	        </tr>
        	
	        <tr>
		        <td colspan=3>
			        <table border="0" cellpadding="0" cellspacing="0" width="90%">
				        <tr>
					        <td width="1%">&nbsp;</td>
        				
					        <td width=98% align=left><SPAN class=subtitleblueblodtext>Retrieve Log File</span></td>
        				
					        <td width="1%">&nbsp;</td>
				        </tr>
        				
				        <tr>
					        <td width="1%">&nbsp;</td>
        				
					        <td width=98%>
        						
						     <table border="0">
								        <tr>
									        <td align="left" colspan="3" class="blackblodtext"> 
										        <b>Enter the file path</b>
                                                <asp:TextBox ID="filePath" runat="server" CssClass="altText" CausesValidation="true" MaxLength="150" Width="260" Wrap="false"></asp:TextBox>
                                                
									        </td>
								        </tr>
								        <tr>
									        <td align="center" colspan="2">
										        <table align="center"><tr>
											        <td>
												        <input type="reset" name="HardwareissuelogSubmit2" value="Reset" class="altShortBlueButtonFormat">
											        </td><td width="2%"></td>
											        <td align="center">
                                                        <asp:Button ID="logRetSubmit" runat="server" Text="Retrieve" CssClass="altShortBlueButtonFormat" OnClick="logRetSubmit_Click" UseSubmitBehavior="false" />
												    </td>
										        </tr></table>
									        </td>
        									
								        </tr>
							        </table>
        					
						        
					        </td>
        				
					        <td width="1%">&nbsp;</td>
				        </tr>
        				
			        </table>
		        </td>
	        </tr>	
<%--            <tr visible="false">
		        <td colspan=3 align="center" style="height: 4px">
			        <img border="0" src="image/aqualine.gif" width="95%" height="2" valign="middle">
		        </td>
	        </tr>			
	        <tr visible="false">
		        <td colspan=3>
			        <table border="0" cellpadding="0" cellspacing="0" width="90%">
				        <tr>
					        <td width="1%">&nbsp;</td>
        				
					        <td width=98% align=left><SPAN class=subtitleblueblodtext>Site Diagnostics</span></td>
        				
					        <td width="1%">&nbsp;</td>
				        </tr>
				        <tr>
					        <td width="1%">&nbsp;</td>
        				    <td width=98%>
   					            <table border="0" cellspacing="5">
						            <tr>
							            <td align="right" width="50%">
    							            <b>MyVRM Version:</b>
							            </td>
							            <td align="left" width="50%">
							                <asp:Label ID="lblMyVRMVersion" runat="server"></asp:Label>
							            </td>
						            </tr>
						            <tr>
							            <td align="right" width="50%">
    							            <b>Database Version:</b>
							            </td>
							            <td align="left" width="50%">
							                <asp:Label ID="lblDatabaseVersion" runat="server"></asp:Label>
							            </td>
						            </tr>
						            <tr>
							            <td align="right" width="50%">
    							            <b>RTC Version:</b>
							            </td>
							            <td align="left" width="50%">
							                <asp:Label ID="lblRTCVersion" runat="server"></asp:Label>
							            </td>
						            </tr>
						            <tr>
							            <td align="right" width="50%">
    							            <b>ASPIL/Business Layer/Data Layer Version:</b>
							            </td>
							            <td align="left" width="50%">
							                <asp:Label ID="lblASPILVersion" runat="server"></asp:Label>
							            </td>
						            </tr>
						        </table>
					        </td>
					        <td width="1%">&nbsp;</td>
				        </tr>
			        </table>
		        </td>
	        </tr>	--%>
	    </table>
        
	</form>     
    </center>
<script language="JavaScript">
    startTimeReq.controltovalidate = "startTimeCombo_Text";
    startTimeRegEx.controltovalidate = "startTimeCombo_Text";
    
    endTimeReq.controltovalidate = "endTimeCombo_Text";
    endTimeRegEx.controltovalidate = "endTimeCombo_Text";
   /* 
    if(document.getElementById("startTimeCombo_Text").value =="")
    {
        document.getElementById("startTimeCombo_Text").value = "01:00AM";
        
        
        for (i = 0; i < document.getElementById("startTimeCombo").length; i++)
        {
        
            if(document.getElementById("startTimeCombo")[i].value == "01:00AM")
            {
                document.getElementById("startTimeCombo")[i].selected = true;
            }
        }
    }
    
    if(document.getElementById("endTimeCombo_Text").value =="")
    {
        document.getElementById("endTimeCombo_Text").value = "11:00PM";
        for (i = 0; i < document.getElementById("endTimeCombo").length; i++)
        {
            if(document.getElementById("endTimeCombo")[i].value == "11:00PM")
            {
                document.getElementById("endTimeCombo")[i].selected = true;
            }
        }
    }
    */
</script>
</body>
</html>
<%--code added for Soft Edge button--%>
<script type="text/javascript" src="inc/softedge.js"></script>
    <!-- #INCLUDE FILE="inc/mainbottomNET.aspx" -->
