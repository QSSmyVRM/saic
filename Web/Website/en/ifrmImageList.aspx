<%--ZD 100147 start--%>
<%--/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.	
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/--%><%--ZD 100147 End--%>
<%@ Page Language="C#" AutoEventWireup="true"  Inherits="en_ifrmImageList.ifrmImageList" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >

<head runat="server">
    <title>Untitled Page</title>
    <meta http-equiv="Content-Language" content="en-us" />
  <link title="myVRM base styles"  type="text/css" rel="stylesheet" />
<%--window Dressing--%>
   <script type="text/javascript"> // FB 2815
       var path = '<%=Session["OrgCSSPath"]%>';
       path = path.split('.')[0] + '<%=Session["ThemeType"]%>' + ".css";
       document.write('<link rel="stylesheet" title="Expedite base styles" type="text/css" href="' + path + '?' + new Date().getTime() + '">');
   </script>   
   <script type="text/javascript" src="script/wincheck.js"></script> <%--FB 3055--%>
</head>
<%--window Dressing--%>
<body bottommargin="0" leftmargin="0" rightmargin="0" topmargin="0" marginheight="0" marginwidth="0" class="tableBody">
  <form id="ifrmImgList" name="ifrmImgList" runat="server">
  <script language="JavaScript" type="text/javascript">
<!--

	function selImg(fimg) 
	{
		parent.document.getElementById ("ImgNameDIV").innerHTML = "Room : <span class=srcstext3><b><u>" + fimg + "</u></b></span>"
        parent.document.getElementById ("RoomImgDIV").innerHTML = "<img style='width:400;height:300' src='image\\room\\"+fimg+".jpg'>"
        parent.document.frmManageimage.RoomImage.value = fimg+".jpg";
	}

//-->
</script>
  
  <input type="hidden" id="ROOM_IMAGE_PATH" runat="server" />
  </form>
</body>

</html>
