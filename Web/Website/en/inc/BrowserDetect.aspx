<!--ZD 100147 Start-->
<!--/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/-->
<!--ZD 100147 End-->
<%

        Session["ip"] = Request.ServerVariables["REMOTE_ADDR"];
        Session["referrer"] = Request.ServerVariables["HTTP_REFERER"];
        if (Request.ServerVariables["HTTP_USER_AGENT"].ToString().ToUpper().Contains("WINDOWS"))
            Session["os"] = "Windows";
        else if (Request.ServerVariables["HTTP_USER_AGENT"].ToString().ToUpper().Contains("MAC"))
            Session["os"] = "Mac";
        else if (Request.ServerVariables["HTTP_USER_AGENT"].ToUpper().ToUpper().Contains("LINUX"))
            Session["os"] = "Linux";
        else
            Session["os"] = "Other";

        if(Request.ServerVariables["HTTP_USER_AGENT"].ToString().ToUpper().Contains("MOZILLA"))
        {
            if (Request.ServerVariables["HTTP_USER_AGENT"].ToString().ToUpper().Contains("MSIE"))
            {
                 Session["browser"] = "Internet Explorer";
                 Session["versionNo"] = Request.Browser.MinorVersionString;
                 Session["version"] = Request.Browser.Version;
            }
            if (Request.ServerVariables["HTTP_USER_AGENT"].ToString().ToUpper().Contains("NETSCAPE"))
            {
                Session["browser"] = "Netscape";
                Session["versionNo"] = Request.Browser.MinorVersionString;
                Session["version"] = Request.Browser.Version;
            }

            if (Request.ServerVariables["HTTP_USER_AGENT"].ToString().ToUpper().Contains("FIREFOX"))
            {
                Session["browser"] = "Firefox";
                Session["versionNo"] = Request.Browser.MinorVersionString;
                Session["version"] = Request.Browser.Version;
            }
            if (Request.ServerVariables["HTTP_USER_AGENT"].ToString().ToUpper().Contains("SAFARI"))
            {
                Session["browser"] = "Firefox";
                Session["versionNo"] = Request.Browser.MinorVersionString;
                Session["version"] = Request.Browser.Version;
            }
            if (Request.ServerVariables["HTTP_USER_AGENT"].ToString().ToUpper().Contains("CHROME"))
            {
                Session["browser"] = "Firefox";
                Session["versionNo"] = Request.Browser.MinorVersionString;
                Session["version"] = Request.Browser.Version;
            }
            if (Session["browser"] == null)
                Session["browser"] = "Undetectable";
            if (Session["versionNo"] == null)
                Session["versionNo"] = "Undetectable";
            if (Session["version"] == null)
                Session["version"] = "Undetectable";
        }
        else
        {
            if (Request.ServerVariables["HTTP_USER_AGENT"].ToString().ToUpper().Contains("OPERA"))
            {
                Session["browser"] = "Opera";
                Session["versionNo"] = Request.Browser.MinorVersionString;
                Session["version"] = Request.Browser.Version;
            }
            else
            {
                Session["browser"] = "Not Mozilla";
                Session["version"] = "Undetectable";
            }
        }
%>