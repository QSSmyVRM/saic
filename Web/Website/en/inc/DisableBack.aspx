<!--ZD 100147 Start-->
<!--/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/-->
<!--ZD 100147 End-->
<%
String addr = Request.ServerVariables["PATH_INFO"].ToString();
Boolean enableBACK = false;
if(addr.Contains("emaillogin.aspx") || addr.Contains("guestlogin.aspx") || addr.Contains("guestregister.aspx") || addr.Contains("requestaccount.aspx"))
   enableBACK = true;
%>

<%
    if (!enableBACK)
    {
%>


	<script language="JavaScript">
		onback = history.forward(1);

		document.onkeydown = function (e) {		// can not deal with chkbx
			if (window.event) {	// IE
				if (window.event.keyCode == 113)
					openhelp();
					
				switch (window.event.srcElement.tagName) {
					case "INPUT" :
					case "TEXTAREA" :
						return true;
						break;
					default :
						return ( (window.event.keyCode != 8) && (window.event.keyCode != 37) );
						break;
				}

			} else if (e.which) {	// netscape
				if (e.which == 113)
					openhelp();
					
				switch (e.target.tagName) {
					case "INPUT" :
					case "TEXTAREA" :
						return true;
						break;
					default :
						return ( (e.which != 8) && (e.which != 37) );
						break;
				}
			} else {
				return -1;
			}
			
		};
		
	</script>
<%
    }
    else
    {
%>

	<script language="JavaScript">
		document.onkeydown = function (e) {		// can not deal with check box
			if (window.event)	// IE
				k = window.event.keyCode
			else if (e.which)	// netscape
				k = e.which
			else
				return -1;

			if (k == 113)
				openhelp();
		};
	</script>
	
<%
    }
%>
