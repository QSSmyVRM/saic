/*ZD 100147 Start*/
/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
/*ZD 100147 End*/
var node_list_frm = document.getElementsByTagName('form');
var menufnd = "0";

if(node_list_frm)
{
    if(node_list_frm.length > 0 )
    {
        for (var i = 0; i < node_list_frm.length; i++) 
        {
            var node = node_list_frm[i];
            
            if(node.name.indexOf('frmMenu') >= 0)
            {  
                menufnd = "1";       
                var node_list_tds = document.getElementsByTagName('td');
                if(node_list_tds)
                {
                    if(node_list_tds.length > 18)
                    {
                        var frmValues = node_list_tds[18].innerHTML;                
                        node_list_tds[18].innerHTML = "<input type='submit' value='' name='SoftEdgeTest' style='width:0%;height:0%;background-color:Transparent;border:none;'/>" + "" + frmValues; 
                    }                
                }
            }
            
            if(menufnd == "0")
            {
                var node_list_td = document.getElementsByTagName('td');
                if(node_list_td)
                {
                    if(node_list_td.length > 0)
                    {
                        var frmValues = node_list_td[0].innerHTML;                
                        node_list_td[0].innerHTML = "<input type='submit'  value='' name='SoftEdgeTest' style='width:0%;height:0%;background-color:Transparent;border:none;'/>" + "" + frmValues; 
                    }                
                }  
            }
        }
     }
}

var node_list = document.getElementsByTagName('input');

if(node_list)
{
    for (var i = 0; i < node_list.length; i++) 
    {
       var node = node_list[i];

        if(node)
        {
           if (node.getAttribute('type') == 'button' || node.getAttribute('type') == 'submit' || node.getAttribute('type') == 'reset') 
           {           
              var nodeName = "";
              
              if(node.name != "")
                nodeName = node.name
              else if(node.id != "")
                nodeName = node.id
                
              if(nodeName != "")
              {
                  var id = document.getElementById(nodeName);   
                  if(id)
                  {       
                      id.onfocus  =  function()
                      {
                          this.blur();
                      };
                  }
              }
           }
       }
    }
}

