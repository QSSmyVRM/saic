//ZD 100147 Start
/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
//ZD 100147 End
using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class en_inc_calendarviewinc : System.Web.UI.Page
{
    myVRMNet.NETFunctions obj;
    msxml4_Net.IXMLDOMElement root = null;
    private msxml4_Net.IXMLDOMNodeList nodes = null;
    private msxml4_Net.IXMLDOMNodeList subnotes = null;
    private msxml4_Net.IXMLDOMNodeList subnotes2 = null;

    public msxml4_Net.DOMDocument40Class XmlDoc = null;
    String buffertxt = "";
    String comParamP5 = "";
    String paramP1 = "";
    String paramP2 = "";
    String paramP3 = "";
    String paramP4 = "";
    public String outXML = "";
    public int setupTime;  //buffer zone
    public int teardownTime;  //buffer zone
    string xmlstr = "";
    string errorMessage = "";
    string startTime = "";
    string endTime = "";
    string tempFor = "";
    public int size = 0;
    
    public DateTime confdate;
    public int idx=0;
    public String confID = "";
    public String confName = "";
    public String confSTime = "";

    public Double durationMin;
    public String confType = "";
    public DateTime confSDate;

    public DateTime confETime;
    public DateTime confEDate;
    public String newconfETime = "";
    public String newconfSTime = "";
    public String newconfSetupTimeEnd = "";
    public DateTime newconfSetupTime;
    public String confType1 = "";
    public String confType2 = "";
    public String newconfSTeardownTime = "";
    public DateTime newconfSTeardownTimeEnd;
    public Int32 newdur;
    public String cinfo = "";
    public String timezoneName = "";
    public String startHour = "";
    public String startMin = "";
    public String startSet = "";
    public String endHour = "";
    public String endMin = "";
    public String endSet = "";
    public string inXML = "";

    public string[] mary = null;
    public string[] mmary = null;
    public string[] ccary = null;
    public int ccint = 0;
    public bool hasApproal = false;
    public string finalstr = "";
    public string locationstr = "";
    public string splitstr = "";

    public String setupSTime = "";  //buffer zone
    public String teardownSTime = ""; //buffer zone
    public bool isAdminRole = false;    //buffer zone
    public bool isParticipant = false;  //buffer zone
    public bool isHost = false;  //buffer zone
    public bool showBuffer = true;  //buffer zone

    #region Page Load Event
    /// <summary>
    /// Page Load Event
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            SetQueryStringVal();
            GetCalendarData();
        }
        catch (System.Threading.ThreadAbortException)
        { }
        catch (Exception ex)
        {
            Response.Write(ex.Message);
            Response.End();
        }
    }
    #endregion

    #region Setting Querystring Values
    /// <summary>
    /// Method to set the query string values passed from Calview.js
    /// if session of userid is not set user id is hardcoded as 11
    /// </summary>
    private void SetQueryStringVal()
    {
        if (Session["userID"].ToString() == "")
        {
            Session["userID"] = "11";
        }

        if (Session["admin"].ToString() == "")
        {
            Session["admin"] = "3";
        }

        if (Session["admin"].ToString() == "1" || Session["admin"].ToString() == "2")   //buffer zone - Super & conf admin
            isAdminRole = true; //buffer zone

        XmlDoc = new msxml4_Net.DOMDocument40Class();
        XmlDoc.async = false;

        if (Request.QueryString["p2"] != null)
        {
            paramP2 = Request.QueryString["p2"].ToString();
        }
        if (Request.QueryString["p5"] != null)
        {
            comParamP5 = Request.QueryString["p5"].ToString().Trim();
        }
        if (Request.QueryString["p1"] != null)
        {
            paramP1 = Request.QueryString["p1"].ToString();
        }
        if (Request.QueryString["p3"] != null)
        {
            paramP3 = Request.QueryString["p3"].ToString();
        }
        if (Request.QueryString["p4"] != null)
        {
            paramP4 = Request.QueryString["p4"].ToString();
        }
    }
    #endregion

    #region Method to retrieve calendar data
    /// <summary>
    /// Method to retrieve calendar details
    /// </summary>
    private void GetCalendarData()
    {
        switch (comParamP5)
        {
            case "1":
                buffertxt = "";
                //daily
                break;
            case "2":
                buffertxt = "W";
                //weekly
                break;
            case "3":
                buffertxt = "M";
                //monthly
                break;
        }

        obj = new myVRMNet.NETFunctions();

        if (buffertxt == "")
        {
            XmlDoc.loadXML("<login/>");
            root = XmlDoc.documentElement;

            AppendTextNode(root, "userID", Session["userID"].ToString());

            if (paramP2 != "")
            {
                AppendTextNode(root, "roomID", paramP2);
            }
            root = null;

            inXML = XmlDoc.xml;

            outXML = obj.CallCOM("GetOldRoom", inXML, Application["COM_ConfigPath"].ToString());

            XmlDoc.loadXML(outXML);

            if(XmlDoc.selectSingleNode("//room/setupTime")!= null)
                tempFor = XmlDoc.selectSingleNode("//room/setupTime").text;

            if (tempFor != "")
                setupTime = Convert.ToInt32(tempFor); //buffer zone

            if(XmlDoc.selectSingleNode("//room/teardownTime") != null)
                tempFor = XmlDoc.selectSingleNode("//room/teardownTime").text;

            if (tempFor != "")
                teardownTime = Convert.ToInt32(tempFor); //buffer zone
        }

        if (Session["systemTimezone"] != null)
            Session["uptz"] = Session["systemTimezone"].ToString();

        XmlDoc.loadXML("<calendarView/>");
        root = XmlDoc.documentElement;

        AppendTextNode(root, "userID", Session["userID"].ToString());
        AppendTextNode(root, "date", paramP1);

        if (paramP2 == "-1")
        {
            AppendTextNode(root, "room", "");
        }
        else
        {
            AppendTextNode(root, "room", paramP2);
        }

        inXML = XmlDoc.xml;

        switch (comParamP5)
        {
            case "1":
                outXML = obj.CallCOM("GetRoomDailyView", inXML, Application["COM_ConfigPath"].ToString());
                break;
            case "2":
                outXML = obj.CallCOM("GetRoomWeeklyView", inXML, Application["COM_ConfigPath"].ToString());
                break;
            case "3":
                outXML = obj.CallCOM("GetRoomMonthlyView", inXML, Application["COM_ConfigPath"].ToString());
                break;
        }
        xmlstr = outXML;
        if (Session["errMsg"] != null)  //code added
        {
            if (Session["errMsg"].ToString() != "")
            {
                if (xmlstr.IndexOf("<error>") > 0)
                {
                    Response.Write("<b>" + xmlstr + "<b>");
                    Response.End();
                }
            }
        }

        XmlDoc = null;
        XmlDoc = new msxml4_Net.DOMDocument40Class();
        XmlDoc.async = false;
        XmlDoc.loadXML(xmlstr);

        if (XmlDoc.parseError.errorCode > 0)
        {
            //if (Session["who_use"].ToString() == "VRM")
            //{
            errorMessage = "Outcoming XML document is illegal:" + Environment.NewLine;
            errorMessage = errorMessage + xmlstr;
            //}
            XmlDoc = null;
            Response.Write("<b>" + errorMessage + "<b>");
            Response.End();
        }


        if(XmlDoc.documentElement.selectSingleNode("timezoneName") != null)
            timezoneName = XmlDoc.documentElement.selectSingleNode("timezoneName").text;

        if(XmlDoc.documentElement.selectSingleNode("systemAvail/startTime/startHour") != null)
            startHour = XmlDoc.documentElement.selectSingleNode("systemAvail/startTime/startHour").text;

        if(XmlDoc.documentElement.selectSingleNode("systemAvail/startTime/startMin") != null)
            startMin = XmlDoc.documentElement.selectSingleNode("systemAvail/startTime/startMin").text;

        if(XmlDoc.documentElement.selectSingleNode("systemAvail/startTime/startSet") != null)
            startSet = XmlDoc.documentElement.selectSingleNode("systemAvail/startTime/startSet").text;

        if(XmlDoc.documentElement.selectSingleNode("systemAvail/endTime/endHour") != null)
            endHour = XmlDoc.documentElement.selectSingleNode("systemAvail/endTime/endHour").text;

        if(XmlDoc.documentElement.selectSingleNode("systemAvail/endTime/endMin") != null)
            endMin = XmlDoc.documentElement.selectSingleNode("systemAvail/endTime/endMin").text;

        if(XmlDoc.documentElement.selectSingleNode("systemAvail/endTime/endSet") != null)
            endSet = XmlDoc.documentElement.selectSingleNode("systemAvail/endTime/endSet").text;

        if (Session["startTime"] != null)
            Session["startTime"] = startHour + ":" + startMin + " " + startSet;
        else
            Session.Add("startTime", (startHour + ":" + startMin + " " + startSet));

        if (Session["endTime"] != null)
            Session["endTime"] = endHour + ":" + endMin + " " + endSet;
        else
            Session.Add("endTime", (endHour + ":" + endMin + " " + endSet));


        switch (comParamP5)
        {
            case "1":
                size = 1;
                break;
            case "2":
                size = 7;
                break;
            case "3":
                size = 31;
                break;
        }

        //Array [size-1] dailyconfinfo;
        String[] dailyconfinfo = new String[size];

        if (Session["sMenuMask"] != null)
        {
            mary = Session["sMenuMask"].ToString().Split('-');
            if (mary.Length > 0)
            {
                mmary = mary[1].Split('+');

                if (mmary.Length > 0)
                {
                    ccary = mmary[1].Split('*');
                    if (ccary.Length > 0)
                    {
                        if (ccary[1] != "")
                        {
                            ccint = Convert.ToInt32(ccary[1]);

                            if ((ccint & 2) == 0)
                                hasApproal = false;
                            else
                                hasApproal = true;
                        }
                    }
                }
            }
        }
        string owner = "";
        string ownerparty = "";
        string party = "";

        nodes = XmlDoc.documentElement.selectNodes("days/day");
        if (nodes != null)
        {
            foreach (msxml4_Net.IXMLDOMNode node in nodes)
            {
                tempFor = node.selectSingleNode("date").text;
                if(tempFor != "")
                    confdate = Convert.ToDateTime(tempFor);

                idx = getAryIndex(confdate);
                subnotes = node.selectNodes("conferences/conference");

                if (subnotes != null)
                {
                    foreach (msxml4_Net.IXMLDOMNode subnode in subnotes)
                    {
                        if(subnode.selectSingleNode("confID") != null)
                            confID = subnode.selectSingleNode("confID").text;

                        if(subnode.selectSingleNode("confName") != null)
                            confName = subnode.selectSingleNode("confName").text;

                        if(subnode.selectSingleNode("confTime") != null)
                            confSTime = subnode.selectSingleNode("confTime").text;   

                        /* *** code added for buffer zone *** --Start   */

                        if (subnode.selectSingleNode("setupTime") != null)
                            setupSTime = subnode.selectSingleNode("setupTime").text;

                        if (subnode.selectSingleNode("teardownTime") != null)
                            teardownSTime = subnode.selectSingleNode("teardownTime").text;

                        /* *** code added for buffer zone *** --End */

                        if (subnode.selectSingleNode("durationMin") != null)
                        {
                            tempFor = subnode.selectSingleNode("durationMin").text;
                            if (tempFor != "")
                                durationMin = Convert.ToDouble(tempFor);
                        }

                        if(subnode.selectSingleNode("ConferenceType") != null)
                            confType = subnode.selectSingleNode("ConferenceType").text;

                        owner = "0";
                        if (subnode.selectNodes("owner") != null)
                        {
                            if (subnode.selectNodes("owner").length >= 1)
                            {
                                owner = subnode.selectSingleNode("owner").text;
                            }
                        }

                        party = "0";
                        if (subnode.selectNodes("party") != null)
                        {
                            if (subnode.selectNodes("party").length >= 1)
                            {
                                party = subnode.selectSingleNode("party").text;
                            }
                        }

                        /* *** Code changed for FB 1389 - start *** */

                        //we need to check the logic
                        //if (owner != "1") // Commented for FB 1389
                        //    owner = "0";

                        //if (party != "1")
                        //    party = "0";
                                                
                        if (owner == "" || owner == null) 
                            owner = "0";

                        if (party == "" || party == null)
                            party = "0";

                        /* Buffer zone - start */

                        if (Session["userID"].ToString() == owner)
                            isHost = true;
                        else
                            isHost = false;

                        if (Session["userID"].ToString() == party)
                            isParticipant = true;
                        else
                            isParticipant = false;

                        if (isParticipant)
                            showBuffer = false;

                        if (isAdminRole || isHost)
                            showBuffer = true;

                        /* Buffer zone - end */

                        ownerparty = owner + "OP" + party;

                        /* *** Code changed for FB 1389 - end *** */

                        confSDate = confdate;

                        if (subnode.selectNodes("confDate") != null)
                        {
                            if (subnode.selectNodes("confDate").length >= 1)
                            {
                                tempFor = subnode.selectSingleNode("confDate").text;
                                if (tempFor != "")
                                    confSDate = Convert.ToDateTime(tempFor);
                            }
                        }

                        locationstr = "";

                        subnotes2 = subnode.selectNodes("mainLocation/location");

                        if (subnotes2 != null)
                        {
                            foreach (msxml4_Net.IXMLDOMNode subnode2 in subnotes2)
                            {
                                locationstr = locationstr + subnode2.selectSingleNode("locationName").text;
                                locationstr = locationstr + "<br>";
                            }
                        }

                        subnotes2 = subnode.selectNodes("mainLocation/split/locations/location");

                        if (subnotes2 != null)
                        {
                            foreach (msxml4_Net.IXMLDOMNode subnode2 in subnotes2)
                            {
                                if (subnode2.selectSingleNode("level1ID") != null)
                                {
                                    if (paramP2 == subnode2.selectSingleNode("level1ID").text)
                                    {
                                        if(subnode2.selectSingleNode("startTime") != null)
                                            confSTime = subnode2.selectSingleNode("startTime").text;

                                        if (subnode2.selectSingleNode("duration") != null)
                                        {
                                            tempFor = subnode2.selectSingleNode("duration").text;

                                            if (tempFor != "")
                                                durationMin = Convert.ToDouble(tempFor);
                                        }
                                    }
                                }
                            }
                        }
                        string isfut = "";
                        string isimm = "";
                        string ispub = "";
                        string ispen = "";
                        string isapp = "";

                        if (subnode.selectNodes("isFuture") != null)
                        {
                            if (subnode.selectNodes("isFuture").length >= 1)
                                isfut = subnode.selectSingleNode("isFuture").text;
                        }

                        if (subnode.selectNodes("isImmediate") != null)
                        {
                            if (subnode.selectNodes("isImmediate").length >= 1)
                                isimm = subnode.selectSingleNode("isImmediate").text;
                        }

                        if (subnode.selectNodes("isPublic") != null)
                        {
                            if (subnode.selectNodes("isPublic").length >= 1)
                                ispub = subnode.selectSingleNode("isPublic").text;
                        }

                        if (subnode.selectNodes("isPending") != null)
                        {
                            if (subnode.selectNodes("isPending").length >= 1)
                                ispen = subnode.selectSingleNode("isPending").text;
                        }

                        if (subnode.selectNodes("isApproval") != null)
                        {
                            if (subnode.selectNodes("isApproval").length >= 1)
                                isapp = subnode.selectSingleNode("isApproval").text;
                        }

                        if ((isapp == "1") && (isfut != "1") && (isimm != "1") && (ispub != "1") && (ispen != "1") && (!hasApproal))
                        {
                            //Nothing needs to be done
                        }
                        else
                        {
                            confETime = Convert.ToDateTime(confSDate.ToShortDateString() + " " + confSTime).AddMinutes(durationMin);
                            confEDate = Convert.ToDateTime(confETime.Month + "/" + confETime.Day + "/" + confETime.Year);

                            if (confETime.Day == (Convert.ToDateTime(confdate.ToShortDateString() + " " + confSTime).Day))
                                newconfETime = confETime.ToShortTimeString();
                            else
                                newconfETime = "11:59 PM";

                            if ((Convert.ToDateTime(confSDate.ToShortDateString() + " " + confSTime).Day) == (Convert.ToDateTime(confdate.ToShortDateString() + " " + confSTime).Day))
                                newconfSTime = confSTime;
                            else
                                newconfSTime = "12:00 AM";

                            /* *** code modified for buffer zone *** --Start */

                            if (setupSTime == "00:00 AM")
                                setupSTime = newconfSTime;

                            if (teardownSTime == "00:00 AM")
                                teardownSTime = newconfETime;

                            setupTime = 0;
                            teardownTime = 0;
                            if (paramP2 != "-1" && paramP2 != "" && buffertxt == "" && setupSTime != "00:00 AM")
                            {
                                DateTime stime = Convert.ToDateTime(setupSTime);
                                DateTime cstime = Convert.ToDateTime(confSTime);
                                TimeSpan Stime = stime - cstime;
                                setupTime = Convert.ToInt32(Stime.TotalMinutes);

                                DateTime etime = Convert.ToDateTime(newconfETime);
                                DateTime ttime = Convert.ToDateTime(teardownSTime);
                                TimeSpan Ttime = etime - ttime;
                                teardownTime = Convert.ToInt32(Ttime.TotalMinutes);
                            }

                            if (Session["EnableBufferZone"] == null)//Organization Module Fixes
                            {
                                Session["EnableBufferZone"] = "0";
                            }

                            if (Session["EnableBufferZone"].ToString() == "0") //buffer zone//Organization Module Fixes
                            {
                                cinfo = confID + "``" + confName + "``" + newconfSTime + "``" + newconfETime + "``" + confSDate.ToShortDateString() + "``";
                                cinfo = cinfo + ownerparty + "``" + durationMin.ToString() + "``" + locationstr + "``" + confSTime + "``" + confETime.ToString() + "``";
                                cinfo = cinfo + isimm + "``" + isfut + "``" + ispub + "``" + ispen + "``" + isapp + "``" + confType + "``" + setupTime + "``" + teardownTime;

                                dailyconfinfo[idx] = getDailyConfInfo(dailyconfinfo[idx], cinfo, confdate);

                            }
                            else
                            {
                                if (!showBuffer)
                                {
                                    int confDuration = 0;
                                    confDuration = Convert.ToInt32(durationMin) - (setupTime + teardownTime);

                                    //cinfo = confID + "``" + confName + "``" + newconfSTime + "``" + newconfETime + "``" + confSDate.ToShortDateString() + "``";
                                    //cinfo = cinfo + ownerparty + "``" + durationMin.ToString() + "``" + locationstr + "``" + confSTime + "``" + confETime.ToString() + "``";
                                    //cinfo = cinfo + isimm + "``" + isfut + "``" + ispub + "``" + ispen + "``" + isapp + "``" + confType + "``" + setupTime + "``" + teardownTime;

                                    //dailyconfinfo[idx] = getDailyConfInfo(dailyconfinfo[idx], cinfo, confdate);

                                    cinfo = confID + "``" + confName + "``" + setupSTime + "``" + teardownSTime + "``" + confSDate.ToShortDateString() + "``";
                                    cinfo = cinfo + ownerparty + "``" + confDuration.ToString() + "``" + locationstr + "``" + setupSTime + "``" + teardownSTime + "``";
                                    cinfo = cinfo + isimm + "``" + isfut + "``" + ispub + "``" + ispen + "``" + isapp + "``" + confType + "``" + setupTime + "``" + teardownTime;

                                    dailyconfinfo[idx] = getDailyConfInfo(dailyconfinfo[idx], cinfo, confdate);
                                }
                                else
                                {
                                    if (setupTime > 0 || teardownTime > 0)
                                    {
                                        if (setupTime > 0) //Setup duration
                                        {
                                            cinfo = confID + "``" + "SetupTime" + "``" + newconfSTime + "``" + setupSTime + "``" + confSDate.ToShortDateString() + "``";
                                            cinfo = cinfo + ownerparty + "``" + durationMin.ToString() + "``" + locationstr + "``" + confSTime + "``" + confETime.ToString() + "``";
                                            cinfo = cinfo + isimm + "``" + isfut + "``" + ispub + "``" + ispen + "``" + isapp + "``" + "S" + "``" + setupTime + "``" + teardownTime;

                                            dailyconfinfo[idx] = getDailyConfInfo(dailyconfinfo[idx], cinfo, confdate);
                                        }

                                        cinfo = confID + "``" + confName + "``" + setupSTime + "``" + teardownSTime + "``" + confSDate.ToShortDateString() + "``";
                                        cinfo = cinfo + ownerparty + "``" + durationMin.ToString() + "``" + locationstr + "``" + confSTime + "``" + confETime.ToString() + "``";
                                        cinfo = cinfo + isimm + "``" + isfut + "``" + ispub + "``" + ispen + "``" + isapp + "``" + confType + "``" + setupTime + "``" + teardownTime;

                                        dailyconfinfo[idx] = getDailyConfInfo(dailyconfinfo[idx], cinfo, confdate);

                                        if (teardownTime > 0)    //TearDown duration
                                        {
                                            cinfo = confID + "``" + "TesrDownTime" + "``" + teardownSTime + "``" + newconfETime + "``" + confSDate.ToShortDateString() + "``";
                                            cinfo = cinfo + ownerparty + "``" + durationMin.ToString() + "``" + locationstr + "``" + confSTime + "``" + confETime.ToString() + "``";
                                            cinfo = cinfo + isimm + "``" + isfut + "``" + ispub + "``" + ispen + "``" + isapp + "``" + "T" + "``" + setupTime + "``" + teardownTime;

                                            dailyconfinfo[idx] = getDailyConfInfo(dailyconfinfo[idx], cinfo, confdate);
                                        }
                                    }

                                    else
                                    {

                                        cinfo = confID + "``" + confName + "``" + newconfSTime + "``" + newconfETime + "``" + confSDate.ToShortDateString() + "``";
                                        cinfo = cinfo + ownerparty + "``" + durationMin.ToString() + "``" + locationstr + "``" + confSTime + "``" + confETime.ToString() + "``";
                                        cinfo = cinfo + isimm + "``" + isfut + "``" + ispub + "``" + ispen + "``" + isapp + "``" + confType + "``" + setupTime + "``" + teardownTime;

                                        dailyconfinfo[idx] = getDailyConfInfo(dailyconfinfo[idx], cinfo, confdate);
                                    }

                                }
                            }

                            /* *** code modified for buffer zone *** --End */
                        }
                        //code changed for FB 1072 and Fb 1170 --end
                    }
                }
            }
        }

        string finalstr = "";
        for (int i = 0; i < size; i++)
        {
            finalstr = finalstr + paramP2 + "!!" + paramP3 + "!!" + paramP1 + "!!" + dailyconfinfo[i] + "!!" + paramP4 + "%%";
        }
        Response.Write(finalstr);

        Response.End();

        nodes = null;
        subnotes = null;
        XmlDoc = null;
    }
    #endregion

    //User defined methods
    
    #region Appends the leaf node to the Xml document
    /// <summary>
    /// Appends the leaf node to the Xml document
    /// </summary>
    /// <param name="parentNode"></param>
    /// <param name="nodeName"></param>
    /// <param name="nodeValue"></param>
    public void AppendTextNode(msxml4_Net.IXMLDOMElement parentNode, string nodeName, string nodeValue)
    {

        msxml4_Net.IXMLDOMElement myXMLFuncNode = XmlDoc.createElement(nodeName);
        myXMLFuncNode.text = nodeValue;
        parentNode.appendChild(myXMLFuncNode);
    }
    #endregion

    #region Get Array Index
    /// <summary>
    /// Method to find out the array index
    /// </summary>
    /// <param name="cfdate"></param>
    /// <returns></returns>
    public int getAryIndex(DateTime cfdate)
    {
        int arrayIndex = 0;

        switch (comParamP5)
        {
            case "1":
                arrayIndex = 0;
                break;
            case "2":

                Int32 wdDayNo = (Int32)cfdate.DayOfWeek;

                /* In .Net sunday is 0 and saturday is 6 */
                if (wdDayNo == 0)
                    arrayIndex = 6;
                else
                    arrayIndex = wdDayNo - 1;

                break;
            case "3":
                arrayIndex = cfdate.Day - 1;
                break;
        }
        return arrayIndex;
    }
    #endregion
        
    #region To Identify whether there is conflict in the conference detail
    /// <summary>
    /// To Identify whether there is conflict in the conference detail
    /// </summary>
    /// <param name="strconf"></param>
    /// <param name="strconfs"></param>
    /// <param name="confdate"></param>
    /// <returns></returns>
    public bool isConflict(String strconf, String strconfs, DateTime confdate)
    {
        string STime = "";
        string ETime = "";
        String[] confs;
        bool conflicted = false;

        Char[] delimitArr = { '`', '`' };
        Char[] delimitArr2 = { '^', '^' };
        
        if (strconf == null)
            return false;

        if (strconfs == null)
            return false;

        String[] confary = strconf.Split(delimitArr,StringSplitOptions.RemoveEmptyEntries);

        if (confary.Length > 3)
        {
            STime = confdate.ToShortDateString() + " " + confary[2];
            ETime = confdate.ToShortDateString() + " " + confary[3];

            if (STime != "" && ETime != "")
            {
                DateTime ST = Convert.ToDateTime(STime);
                DateTime ET = Convert.ToDateTime(ETime);

                confs = strconfs.Split(delimitArr2, StringSplitOptions.RemoveEmptyEntries);

                for (int i = 0; i < confs.Length; i++)
                {
                    confary = null;
                    startTime = "";
                    endTime = "";
                    confary = confs[i].Split(delimitArr, StringSplitOptions.RemoveEmptyEntries);

                    if (confary.Length > 3)
                    {
                        startTime = confdate.ToShortDateString() + " " + confary[2];
                        endTime = confdate.ToShortDateString() + " " + confary[3];

                        if (startTime != "" && endTime != "")
                        {
                            DateTime startDate = Convert.ToDateTime(startTime);
                            DateTime endDate = Convert.ToDateTime(endTime);

                            if (!((CalculateMinutesDiff(ET, startDate) >= 0) || (CalculateMinutesDiff(endDate, ST) >= 0)))
                            {
                                conflicted = true;
                            }
                        }
                    }
                }
            }
        }
        return conflicted;
    }
    #endregion

    #region To get the minutes difference between two dates
    /// <summary>
    /// This routine will return the minutes difference between two dates
    /// return type is int(built-in type)
    /// receive three arguments
    /// first arguement - first date
    /// second arguement - end date 
    /// </summary>
    /// <param name="firstDate"></param>
    /// <param name="secondDate"></param>
    /// <returns></returns>
    public int CalculateMinutesDiff(DateTime firstDate, DateTime secondDate)
    {
        int minDiff = 0;
        try
        {
            TimeSpan span = secondDate.Subtract(firstDate);
            Double mind = span.TotalMinutes;

            minDiff = Convert.ToInt32(mind);

            //minDiff = 60;
        }
        catch (Exception ex)
        {
            Response.Write("VRM System Error");
            Response.End();
        }
        return minDiff;
    }
    #endregion

    #region To build the conference string for a single date
    /// <summary>
    /// To build the conference string for a single date
    /// </summary>
    /// <param name="orgstr"></param>
    /// <param name="substr"></param>
    /// <param name="confDate"></param>
    /// <returns></returns>
    public String getDailyConfInfo(String orgstr, String substr, DateTime confDate)
    {
        String[] orgconfs;
        String newconfs="";
        bool isadded = false;
        Char[] delimitedArr = { '@', '@' };

        switch (comParamP5)
        {
            case "1":
                {
                    isadded = false;
                    newconfs = "";

                    if (orgstr != null)
                    {
                        orgconfs = orgstr.Split(delimitedArr,StringSplitOptions.RemoveEmptyEntries);
                        if (orgconfs.Length > 0)
                        {
                            for (int i = 0; i < orgconfs.Length; i++)
                            {
                                if (!isadded)
                                {
                                    if (!isConflict(substr, orgconfs[i], confDate))
                                    {
                                        //if (orgconfs[i] == "")
                                        if (orgconfs[i] == "" || orgconfs[i] == null)
                                            orgconfs[i] = orgstr;
                                        else
                                            orgconfs[i] = orgconfs[i] + "^^" + substr;

                                        isadded = true;
                                    }
                                }
                            }
                            
                            newconfs = "";
                            for (int k = 0; k < orgconfs.Length; k++)
                            {
                                newconfs = newconfs + orgconfs[k] + "@@";
                            }
                        }
                    }
                    if (!isadded)
                        newconfs = newconfs + substr + "@@";

                    break;
                }
            case "2":
                {
                    newconfs = orgstr + substr + "@@";
                    break;
                }
            case "3":
                {
                    newconfs = orgstr + substr + "@@";
                    break;
                }
        }
        return newconfs;
    }
    #endregion
}
