<!--ZD 100147 Start-->
<!--/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/-->
<!--ZD 100147 End-->
      </td>
    </tr>
  </table>

  <br>

  <table width="100%" cellpadding="6">
    <tr>
      <td>

<script language="JavaScript">

	var mt = "";
	var _d = document;
	
	contactname = "<%=Session["contactName"]%>";	
	contactemail = "<%=Session["contactEmail"]%>";
	contactphone = "<%=Session["contactPhone"]%>";
	contactaddinfo = "<%=Session["contactAddInfo"]%>";
	appVersion = "<%=Application["Version"]%>";
	appCpyDur = "<%=Application["CopyrightsDur"]%>";
	
		
	mt += "<table width='100%' border='0' cellpadding='0' cellspacing='0'>";

	mt += "<tr valign='bottom'>";
   	mt += "<td width='70%' height='20'>";

    
		mt += "<a class=sb1 href='#top'>Back to Top</a>";

	mt += "</td>";
	mt += "<td width='28%' valign='bottom' align='right'>";
	mt += "<table>";
	mt += "<tr>";

	mt += "</tr>"
	mt += "</table>"
	mt += "</td>"
	mt += "<td width='2%'></td>"
	mt += "</tr>"
	mt += "<tr valign='top'>"
	mt += "<td colspan='3' bgcolor='#000080' width='937' height='1'><IMG height='3' alt='' src='image/space.gif' width='100%'></td>" //Edited for FF
	mt += "</tr>"
	mt += "</table>"

	mt += "<center><table border='0' cellpadding='2' cellspacing='2'>";
	mt += "<tr valign='bottom'>";
	mt += "<td>";
	mt += "<span class=srcstext2>Tech Support Contact : </span><span class=contacttext>" + contactname + "</span>";
	mt += "</td>";
	mt += "<td width=10></td>";
	mt += "<td>";
	mt += "<span class=srcstext2>Tech Support Email : </span><span class=contacttext><a  href='mailto:" + contactemail + "'>" + contactemail + "</a></span>";
	mt += "</td>";
	mt += "<td width=10></td>";
	mt += "<td>";
	mt += "<span class=srcstext2>Tech Support Phone : </span><span class=contacttext>" + contactphone + "</span>";
	mt += "</td>";
	mt += "</tr>";
	mt += "<td colspan=5 align=center>";
	//FB 1985 - Starts
	if('<%=Application["Client"]%>'.toUpperCase() == "DISNEY")
    {	
	    mt += "<span class=srcstext2>myVRM Version "+ appVersion +",(c)Copyright "+ appCpyDur +"myVRM.com. All Rights Reserved.</span>"; 
	}
	else
	{
	    mt += "<span class=srcstext2>myVRM Version "+ appVersion +",(c)Copyright "+ appCpyDur +"<a href='http://www.myvrm.com'>myVRM.com</a>. All Rights Reserved.</span>"; //FB 1648
	}
	//FB 1985 - End
	mt += "</td>";
	mt += "</tr>";
	mt += "</table></center>";

	_d.write(mt)
	

</script>

      </td>
    </tr>
  </table>	

</body>
</html>