﻿<!--ZD 100147 Start-->
<!--/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/-->
<!--ZD 100147 End-->
<%
    if(Session["userID"] == null)
    {
        if (Request.QueryString["hf"] != null)
        {
            if (Request.QueryString["hf"].ToString() == "1")
            {
%>
            <script language="javascript" type="text/javascript">
             <!--
                if(window.opener)
                {
                        self.close();
                        window.opener.location.replace("~/en/genlogin.aspx");//FB 1830
                }
              //-->	
            </script>
<%                
            }
            else
            {            
                Response.Redirect("~/en/genlogin.aspx"); //FB 1830
            }
        }     
        else
        {
            Response.Redirect("~/en/genlogin.aspx"); //FB 1830
        }
    }
    else
    {
       
    }
%>
<!--  #INCLUDE FILE="../inc/browserdetect.aspx"  -->
<!--  #INCLUDE FILE="../inc/Holiday.aspx"  --><%--FB 1861--%>
<html>
<head>
  <title>myVRM</title>
  <meta name="Description" content="myVRM (Videoconferencing Resource Management) is a revolutionary Web-based software application that manages any video conferencing environment.">
  <meta name="Keywords" content="VRM, myVRM, Videoconferencing Resource Management, video conferencing, video bridges, video endpoints">
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <META NAME="LANGUAGE" CONTENT="en">
  <META NAME="DOCUMENTCOUNTRYCODE" CONTENT="us">

  <LINK title="Expedite base styles" href="<%=Session["OrgCSSPath"]%>" type=text/css rel=stylesheet>
<STYLE>
@media print {

    .btprint {
        display:none;
        }
}

</STYLE> 
<% if(Application["global"].ToString().Equals("enable")) 
   {
%>

  <script language="javascript">
  <!--
  
	function international (cb)
	{
		if ( cb.options[cb.selectedIndex].value != "" ) {
			str="" + window.location; 
			newstr=str.replace(/\/en\//i, "/" + cb.options[cb.selectedIndex].value + "/"); 
			if ((ind1 = newstr.indexOf("sct=")) != -1) {
				if ( (ind2=newstr.indexOf("&", ind1)) != -1)
					newstr = newstr.substring(0, ind1-1) + newstr.substring(ind2, newstr.length-1);
				else
					newstr = newstr.substring(0, ind1-1);
			}
			newstr += ((newstr.indexOf("?") == -1) ? "?" : "&") + "sct=" + cb.selectedIndex; 
			document.location = newstr; 
		} 
	}
    
      //-->	
  </script>
	
<% }%>
    <script language="javascript">
    <!--
    function adjustWidth(obj)
    {
        if(obj.style.width == "")
        if (obj.src.indexOf("lobbytop1024.jpg") >= 0)
        {
            obj.style.width=window.screen.width-165;
            if (window.screen.width <= 1024)
                obj.src = "<%=Session["OrgBanner1024Path"]%>";
            else
                obj.src = "<%=Session["OrgBanner1600Path"]%>";
                
        }
    }
     //-->	
  </script>
</head>
<!--  #INCLUDE FILE="../inc/disableback.aspx"  -->

<%
if(Request.QueryString["sct"] !=null)
{ 
    if(Request.QueryString["sct"].ToString().Trim() != "")
	    Session["c"] = Request.QueryString["sct"];
}

String ind = "";
if(Session["c"] != null)
{
    ind = Session["c"].ToString();
}
%>


<script type="text/javascript" src="script/errorList.js"></script>

<body bottommargin="0" leftmargin="0" rightmargin="0" topmargin="0" marginheight="0" marginwidth="0">

<% 
Application["global"] = "";
if(Application["global"].ToString().Equals("enable")) 
{
%>

  <table height="25" cellSpacing="0" cellPadding="0" align="right" border="0">
    <tr>
      <td noWrap><span class="para_small">Choose language&nbsp;</span></td>
      <td>
        <table cellSpacing="0" cellPadding="1" border="0">
          <tbody>
            <tr>
              <form name="frmInternational">
                <td>
                  <select class="para_small" onchange="international(this)" id=select1 name=select1>
                    <option value="en" selected>English</option>
                    <option value="ch">Chinese Simplified</option>
                    <option value="zh">Chinese Traditional</option>
                    <option value="fr">French</option>
                    <option value="sp">Spanish</option>
                  </select>
                </td>
              </form>
            </tr>
          </tbody>
        </table>
      </td>
    </tr>
  </table>

<script language="javascript">
	
	i = parseInt("<%=ind%>", 10);
	cb = document.frmInternational.select1;

	if (!isNaN(i) || i < 0 || i >= cb.length) {
		cb.options[i].selected = true;
	}

</script>

<% 
}
%>





<!--------------------------------- CONTENT START HERE --------------->
