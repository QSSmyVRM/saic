<!--ZD 100147 Start-->
<!--/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/-->
<!--ZD 100147 End-->
 <!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
 <meta http-equiv="X-UA-Compatible" content="IE=8" /> <%-- FB 2827 --%>
<%     
    
    if(Session["userID"] == null)
        Response.Redirect("~/en/genlogin.aspx"); //FB 1830
%>
<!--  #INCLUDE FILE="../inc/browserdetect.aspx"  -->

<%--FB 1861, FB 1913--%>
<!--  #INCLUDE FILE="../inc/Holiday.aspx"  -->
<html>
<head>
  <title>myVRM</title>
  <meta name="Description" content="myVRM (Videoconferencing Resource Management) is a revolutionary Web-based software application that manages any video conferencing environment.">
  <meta name="Keywords" content="myVRM, Videoconferencing Resource Management, video conferencing, video bridges, video endpoints">
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <meta NAME="LANGUAGE" CONTENT="en">
  <meta NAME="DOCUMENTCOUNTRYCODE" CONTENT="us">
  
  <link rel="stylesheet" title="Expedite base styles" type="text/css" href="css/border-table.css">
  <link rel="stylesheet" title="Expedite base styles" type="text/css" href="css/main-table.css">
	<script type="text/javascript">	    // FB 2827
	    var path = '<%=Session["OrgCSSPath"]%>';
	    path = path.split('.')[0] + '<%=Session["ThemeType"]%>' + ".css"; 
	    document.write('<link rel="stylesheet" title="Expedite base styles" type="text/css" href="' + path + '?' + new Date().getTime() + '">');
    </script>    <%-- Organization Css Module --%>  
  <script language="javascript" src="script/RoboHelp_CSH.js"></script>
   <style type="text/css">
    @media print
    {
        #btprint
        {
            display: none;
        }   
    } /* FB 2827  */
        
        /* FB 2827 Starts */
        #menuCalRed
        {
            background-image: url('../en/image/menu_icons/cal_red.png');
            background-repeat: no-repeat;
            width: 90px;
            height: 70px;
        }
        #menuSetRed
        {
            background-image: url('../en/image/menu_icons/set_red.png');
            background-repeat: no-repeat;
            width: 90px;
            height: 70px;
        }

        #menuCalRed:hover
        {
            background-image: url('../en/image/menu_icons/black_cal.png');
            background-repeat: no-repeat;
            width: 90px;
            height: 70px;
        }
        #menuSetRed:hover
        {
            background-image: url('../en/image/menu_icons/black_set.png');
            background-repeat: no-repeat;
            width: 90px;
            height: 70px;
        }

        #menuCalBlue
        {
            background-image: url('../en/image/menu_icons/cal_blue.png');
            background-repeat: no-repeat;
            width: 90px;
            height: 70px;
        }
        #menuSetBlue
        {
            background-image: url('../en/image/menu_icons/set_blue.png');
            background-repeat: no-repeat;
            width: 90px;
            height: 70px;
        }

        #menuCalBlue:hover
        {
            background-image: url('../en/image/menu_icons/black_cal.png');
            background-repeat: no-repeat;
            width: 90px;
            height: 70px;
        }
        #menuSetBlue:hover
        {
            background-image: url('../en/image/menu_icons/black_set.png');
            background-repeat: no-repeat;
            width: 90px;
            height: 70px;
        }

        #menuCal
        {
            background-image: url('../en/image/menu_icons/cal.png');
            background-repeat: no-repeat;
            width: 90px;
            height: 70px;
        }
        #menuCal:hover
        {
            background-image: url('../en/image/menu_icons/black_cal.png');
            background-repeat: no-repeat;
            width: 90px;
            height: 70px;
        }

        #menuSet
        {
            background-image: url('../en/image/menu_icons/set.png');
            background-repeat: no-repeat;
            width: 90px;
            height: 70px;
        }
        #menuSet:hover
        {
            background-image: url('../en/image/menu_icons/black_set.png');
            background-repeat: no-repeat;
            width: 90px;
            height: 70px;
        }
        
        
        #menuCallRed
        {
            background-image: url('../en/image/menu_icons/call_red.png');
            background-repeat: no-repeat;
            width: 90px;
            height: 70px;
        }
        #menuCallRed:hover
        {
            background-image: url('../en/image/menu_icons/black_call.png');
            background-repeat: no-repeat;
            width: 90px;
            height: 70px;
        }
        
        #menuReservationRed
        {
            background-image: url('../en/image/menu_icons/reservation_red.png');
            background-repeat: no-repeat;
            width: 90px;
            height: 70px;
        }
        #menuReservationRed:hover
        {
            background-image: url('../en/image/menu_icons/black_reservation.png');
            background-repeat: no-repeat;
            width: 90px;
            height: 70px;
        }
        
        #menuCallBlue
        {
            background-image: url('../en/image/menu_icons/call_blue.png');
            background-repeat: no-repeat;
            width: 90px;
            height: 70px;
        }
        #menuCallBlue:hover
        {
            background-image: url('../en/image/menu_icons/black_call.png');
            background-repeat: no-repeat;
            width: 90px;
            height: 70px;
        }
        
        #menuReservationBlue
        {
            background-image: url('../en/image/menu_icons/reservation_blue.png');
            background-repeat: no-repeat;
            width: 90px;
            height: 70px;
        }
        #menuReservationBlue:hover
        {
            background-image: url('../en/image/menu_icons/black_reservation.png');
            background-repeat: no-repeat;
            width: 90px;
            height: 70px;
        }

        #menuCall
        {
            background-image: url('../en/image/menu_icons/call.png');
            background-repeat: no-repeat;
            width: 90px;
            height: 70px;
        }
        #menuCall:hover
        {
            background-image: url('../en/image/menu_icons/black_call.png');
            background-repeat: no-repeat;
            width: 90px;
            height: 70px;
        }
        
        #menuReservation
        {
            background-image: url('../en/image/menu_icons/reservation.png');
            background-repeat: no-repeat;
            width: 90px;
            height: 70px;
        }
        #menuReservation:hover
        {
            background-image: url('../en/image/menu_icons/black_reservation.png');
            background-repeat: no-repeat;
            width: 90px;
            height: 70px;
        }
        
        
        #menu1:focus
        {
            outline: none;
        }
        #tabnav1, #tabnav1 ul
        {
            margin: 0;
            padding: 0;
            list-style-type: none;
            list-style-position: outside;
            position: relative;
            line-height: 1.5em;
        }
        #tabnav1 a
        {
            display: block;
            padding: 0px 5px;
            border: 1px;
            color: #5E5D5E;
            background-color: #E0E0E0;
        }
        #tabnav1 a:hover
        {
            color: #fff;
            background-color: #7c7c7c;
        }
        #tabnav1 li
        {
            float: inherit;
            position: relative;
        }
        #tabnav1 ul
        {
            position: absolute;
            width: 130px;
            display: none;
            width: 100px;
            top: 1.5em;
        }
        #tabnav1 li ul a
        {
            width: 130px;
            height: 30px;
            float: inherit;
            background-color: #E0E0E0; /* select ul background */
        }
        #tabnav1 ul ul
        {
            top: auto;
        }
        #tabnav1 li ul ul
        {
            margin: 0px 0 0 10px;
        }
        #tabnav1 li:hover ul ul, #tabnav1 li:hover ul ul ul, #tabnav1 li:hover ul ul ul ul
        {
            display: none;
        }
        #tabnav1 li:hover ul, #tabnav1 li li:hover ul, #tabnav1 li li li:hover ul, #tabnav1 li li li li:hover ul
        {
            display: block;
        }
        /* Menu - 1 Style Ends *//* Menu - 2 Style starts */#tabnav2, #tabnav2 ul
        {
            margin: 0;
            padding: 0;
            list-style-type: none;
            list-style-position: outside;
            position: relative;
            line-height: 1.5em;
        }
        #tabnav2 a
        {
            display: block;
            padding: 0px 1px 0px 5px;
            border: 1px;
            color: #5E5D5E;
            background-color: #E0E0E0;
        }
        #tabnav2 a:hover
        {
            color: #fff;
            background-color: #7c7c7c;
        }
        #tabnav2 li
        {
            float: inherit;
            position: relative;
        }
        #tabnav2 ul
        {
            position: absolute;
            width: 130px;
            display: none;
            width: 100px;
            top: 1.5em;
        }
        #tabnav2 li ul a
        {
            width: 140px;
            height: 30px;
            float: inherit;
            background-color: #E0E0E0; /* select ul background */
        }
        #tabnav2 ul ul
        {
            top: auto;
        }
        #tabnav2 li ul ul
        {
            margin: 0px 0 0 1px;
        }
        #tabnav2 li:hover ul ul, #tabnav2 li:hover ul ul ul, #tabnav2 li:hover ul ul ul ul
        {
            display: none;
        }
        #tabnav2 li:hover ul, #tabnav2 li li:hover ul, #tabnav2 li li li:hover ul, #tabnav2 li li li li:hover ul
        {
            display: block;
        }
        /* Menu - 2 Style Ends *//* Menu - 3 Starts */#orglist, #orglist ul
        {
            margin: 0;
            padding: 0;
            list-style-type: none;
            list-style-position: outside;
            position: relative;
            line-height: 1.5em;
        }
        #orglist a
        {
            display: block;
            padding: 0px 1px 0px 5px;
            border: 1px;
            color: #5E5D5E;
            background-color: #E0E0E0;
        }
        #orglist a:hover
        {
            color: #fff;
            background-color: #7c7c7c;
        }
        #orglist li
        {
            float: right;
            position: relative;
        }
        #orglist ul
        {
            position: absolute;
            width: 130px;
            display: none;
            width: 100px;
            top: 0em;
        }
        #orglist li ul a
        {
            width: 130px;
            height: 30px;
            float: right;
            background-color: #E0E0E0;
        }
        #orglist ul ul
        {
            top: auto;
        }
        #orglist li ul ul
        {
            margin: 0px 0 0 0px;
        }
        #orglist li:hover ul ul, #orglist li:hover ul ul ul, #orglist li:hover ul ul ul ul
        {
            display: none;
        }
        #orglist li:hover ul, #orglist li li:hover ul, #orglist li li li:hover ul, #orglist li li li li:hover ul
        {
            display: block;
        }
        /* FB 2827 Ends */        
    </style> 


  <script language="javascript">
  <!--
    
    // FB 2687 Starts
    function preventBack(){window.history.forward();}
    setTimeout("preventBack()", 0);
    window.onunload=function(){null};
    // FB 2687 Ends
    
   //FB 2827 Starts 
    function fnmenu1mover(val) {            
            document.getElementById("tabnav1li").style.display = "block";
            document.getElementById("tabnav2").style.visibility = "hidden";            
     }
        
     function fnmenu1mout(val) {            
        document.getElementById("tabnav1li").style.display = "none";
        document.getElementById("tabnav2").style.visibility = "visible";            
     }
      
     function fnmenu2mover(val) {            
            document.getElementById("tabnav2li").style.display = "block";
     }
        
     function fnmenu2mout(val) {        
        document.getElementById("tabnav2li").style.display = "none";
     }  
     //FB 2827 Ends
    
    version = '<%=Application["Version"]%>';
    copyrightsDur = '<%=Application["CopyrightsDur"]%>';//FB 1648
    var timerRunning = false;
	var toTimer = null;
	defaultConfTempJS = '<%=Session["defaultConfTemp"]%>'; //FB 1746
    var isExpressUser = '<%=Session["isExpressUser"]%>'; //FB 1779
	var curtalker = ""
	curtalker = "<%=Session["IMTalker"]%>";
	var EnableAudioBridge = '<%=Session["EnableAudioBridges"]%>'; //FB 2023
	var EnableEM7Opt='<%=Session["EnableEM7"]%>'; //FB 2633
	var EnableCallMonitor='<%=Session["EnableCallmonitor"]%>';  //FB 2996
	
	/* Commented for FB 2397
	function displayAlert()
	{
		alert("Your session will expire in 1 minute due to inactivity.\nPlease click on the Refresh button of your browser to avoid time out.");
	}
	*/ 
	
	function logout()
	{
//	    alert("in .NET");
		window.location.href = "thankyou.aspx?t=1"; // FB 2397
	}
	
	function startTimer()
	{
		t2 = parseInt("<%=Application["timeoutSecond"]%>", 10) * 1000;
		t1 = t2 - 60000;

		//toTimer = setTimeout('displayAlert()',t1); // Commented for FB 2397
		toTimer = setTimeout('logout()',t2);
		timerRunning = true;
	}
	
	function stopTimer() {
		if (timerRunning) {
			clearTimeout(toTimer);
		}
	}
	
	
	function openFeedback()
	{
//		window.open ('dispatcher/gendispatcher.asp?cmd=GetFeedback', 'Feedback', 'width=450,height=310,resizable=no,scrollbars=no,status=no,left=' + (screen.availWidth-478) + ',top=198');//Login Management
		window.open ('feedback.aspx?wintype=pop', 'Feedback', 'width=450,height=410,resizable=no,scrollbars=no,status=no,left=' + (screen.availWidth-478) + ',top=198'); //Login Management
		
	}
	
	function close_popwin()
	{
		if (window.winrtc) {
			winrtc.close();
		}
		if (window.wincrt) {
			wincrt.close();
		}
	}
	
	
	function errorHandler( e, f, l ){
		alert("An error has ocurred in the JavaScript on this page.\nFile: " + f + "\nLine: " + l + "\nError:" + e);
		return true;
	}
	
//    function adjustWidth(obj)
//    {
//        if(obj.style.width == "")
//        if (obj.src.indexOf("lobbytop1024.jpg") >= 0)
//        {
            
            
                
//        }
  //-->
  </script>
  
<% if (Application["global"] == null )
    Response.Redirect("expired_page.aspx");
if (Application["global"].Equals("enable") ) { %>

  <script language="javascript">
  <!--
  
	function international (cb)
	{
		if ( cb.options[cb.selectedIndex].value != "" ) {
			str="" + window.location; 
			newstr=str.replace(/\/en\//i, "/" + cb.options[cb.selectedIndex].value + "/"); 
			if ((ind1 = newstr.indexOf("sct=")) != -1) {
				if ( (ind2=newstr.indexOf("&", ind1)) != -1)
					newstr = newstr.substring(0, ind1-1) + newstr.substring(ind2, newstr.length-1);
				else
					newstr = newstr.substring(0, ind1-1);
			}
			newstr += ((newstr.indexOf("?") == -1) ? "?" : "&") + "sct=" + cb.selectedIndex; 
			document.location = newstr; 
		} 
	}
	
  //-->
  </script>
	
<%}%>
</head>

	<script language="JavaScript">
		startTimer(); // FB 2397
	</script>
	



<%
if (Session["sMenuMask"] == null ){
	Response.Redirect ("expired_page.aspx");
}
if (Session["userInterface"].Equals("2") ) {
%>

<body bottommargin="0" leftmargin="0" rightmargin="0" topmargin="0" marginheight="0" marginwidth="0">
   <input id="advRep" type="hidden" value='<%=Session["EnableAdvancedReport"]%>' />
   <input id="callMon" type="hidden" value='<%=Session["EnableCallmonitor"]%>' />
    <input id="userAdminLevel" type="hidden" value='<%=Session["Admin"]%>' />
    <input id="isExpressUser" type="hidden" value='<%=Session["isExpressUser"]%>' />
    <input id="isExpressUserAdv" type="hidden" value='<%=Session["isExpressUserAdv"]%>' />
    <input id="isExpressManage" type="hidden" value='<%=Session["isExpressManage"]%>' />
   <table style="position:fixed; background-color:white; z-index:1000" width="100%" border="0" cellpadding="0" cellspacing="0" class="btprint"> <%-- FB 2827 --%>
     <tr valign="top" width="100%">
        <!--<td width="100%" height="72" align="right" valign="bottom" background="image/Lobbytop.jpg">-->
        <td width="60%" height="50" nowrap>
             <%--<img border="0" src="<%=Session["CompanyLogo"]%>" id="Img1" onload="javascript:adjustWidth(this)"/>--%>
             <img  id="mainTop11" width="200" height="72" style="visibility:hidden;margin-right: 550px;" runat="server"/> <%--Edited for FF--%><%--ZD 100156--%>
           </td>            
            <td align="right">
            <table border="0" cellpadding="0" cellspacing="0" style="padding-right: 25px; margin-top:10px"> <%--FB 2827--%>
                    <tr>
                        <td style="width:130px" nowrap="nowrap">
                            <table border="0" width="100%">
                                <tr>
                                    <td colspan="2" style="height: 30px">
                                        <%if(Request.Browser.Type.ToUpper().Contains("MSIE")){ %>
                                        <ul style="text-align:left" id="tabnav1" onmouseover="javascript:fnmenu1mover(this)" onmouseout="javascript:fnmenu1mout(this)">
                                            <li><b style="color: red">
                                                <%=Session["userName"].ToString()%></b>
                                                <ul style="display: none; margin-left: -125px; margin-top: -2px; text-align: left"
                                                    id="tabnav1li">
                                                    <li><a href="ManageUserProfile.aspx">Account Settings</a></li>
                                                    <li><a href="thankyou.aspx">Logout</a></li> 
                                                </ul>
                                            </li>
                                        </ul>
                                        <%}else{ %>
                                        <ul style="text-align:left" id="tabnav1" onmouseover="javascript:fnmenu1mover(this)" onmouseout="javascript:fnmenu1mout(this)">
                                            <li><b style="color: red">
                                                <%=Session["userName"].ToString()%></b>
                                                <ul style="display: none; margin-left: -5px; text-align: left" id="tabnav1li">
                                                    <li><a href="ManageUserProfile.aspx">Account Settings</a></li>
                                                    <li><a href="thankyou.aspx">Logout</a></li> 
                                                </ul>
                                            </li>
                                        </ul>
                                        <%} %>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="3" style="height: 1px; background-color: Gray">
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" valign="bottom">
                                        <table border="0">
                                            <tr>
                                                <td align="left">
                                                    <table border="0">
                                                        <tr>
                                                            <td>
                                                                <%if(Request.Browser.Type.ToUpper().Contains("MSIE")){ %>
                                                                <ul style="text-align:left" id="tabnav2">
                                                                    <li onmouseover="javascript:fnmenu2mover(this)" onmouseout="javascript:fnmenu2mout(this)">
                                                                        <b style="color: #5E5D5E"><%=Session["organizationName"].ToString()%></b>
                                                                        <ul style="display: none; margin-left: -80px; margin-top: -2px" id="tabnav2li">
                                                                            <li id="lstSetting" onmouseover="document.getElementById('orglist').style.display='none'">
                                                                                <a href="OrganisationSettings.aspx">Settings</a></li>
                                                                            <li id="lstSearch"><a href="SearchConferenceInputParameters.aspx">Search</a></li>
                                                                            <li id="Li3" onmouseover="document.getElementById('orglist').style.display='none'"> <%--FB 2979--%>
                                                                                <a onclick="javascript:openhelp();return false;" href="#" >Help</a></li>  
                                                                            <li id="switchorgMain" style="visibility: hidden" onmouseover="document.getElementById('orglist').style.display='block'">
                                                                                <a href="#">Switch Organization</a>
                                                                                <ul id="orglist" style="margin-left: -135px; margin-top: -41px">
                                                                                </ul>
                                                                            </li>
                                                                        </ul>
                                                                    </li>
                                                                </ul>
                                                                <%}else{ %>
                                                                <ul style="text-align:left" id="tabnav2">
                                                                    <li onmouseover="javascript:fnmenu2mover(this)" onmouseout="javascript:fnmenu2mout(this)">
                                                                        <b style="color: #5E5D5E"><%=Session["organizationName"].ToString()%></b>
                                                                        <ul style="display: none; margin-left: -10px" id="tabnav2li">
                                                                            <li id="lstSetting"><a href="OrganisationSettings.aspx">Settings</a></li>
                                                                            <li id="lstSearch"><a href="SearchConferenceInputParameters.aspx">Search</a></li>
                                                                            <li id="Li1" onmouseover="document.getElementById('orglist').style.display='none'"> <%--FB 2979--%>
                                                                                <a onclick="javascript:openhelp();return false;" href="#" >Help</a></li> 
                                                                            <li id="switchorgMain" style="visibility: hidden"><a href="#">Switch Organization</a>
                                                                                <ul id="orglist" style="margin-left: -100px; margin-top: -31px; position:relative; z-index:10000">
                                                                                </ul>
                                                                            </li>
                                                                            
                                                                        </ul>
                                                                    </li>
                                                                </ul>
                                                                <%} %>
                                                                
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table><%--FB 2827 Ends--%>
                    </td>
                    <td style="width:0px">
                    <form name="frmMenu">
                  <input type="hidden" name="menumask" value="<% =Session["sMenuMask"] %>">
                  <input type="hidden" name="adminlevel" value="<% =Session["admin"] %>">
                  <input type="hidden" name="userinterface" value="<% =Session["userInterface"] %>">
                  <input type="hidden" name="feedback_enable" value="<% =Session["hasFeedback"] %>">
                  <input type="hidden" name="help_enable" value="<% =Session["hasHelp"] %>">
                  <input type="hidden" name="roomfood" value="<%=Convert.ToInt16(Session["hkModule"]) * 4 + Convert.ToInt16(Session["foodModule"])*2 + Convert.ToInt16(Session["roomModule"])%>">
                  <input type="hidden" name="p2p" value="<%=Session["P2PEnable"]%>">
                  <input type="hidden" name="ssoMode" value="<%=Application["ssoMode"]%>">
                  <input type="hidden" name="txtClient" id="txtClient" value="<%=Application["Client"]%>">
                  <input type="hidden" name="txtSAlerts" id="txtSAlerts" value="<%= Session["Alerts"] %>" />
                  <input type="hidden" name="txtSAlertsP" id="txtSAlertsP" value="<%= Session["AlertsP"] %>" />
                  <input type="hidden" name="txtSAlertsA" id="txtSAlertsA" value="<%= Session["AlertsA"] %>" />
                </form>
                <iframe name="ifrmListen" style="border:none" width="0" height="0" src="blank.htm"> <%-- FB 2827 --%>
                  <p>Listening page</p>
                </iframe>
                                
            </td>
                </tr>        
            </table>
        </td>
       </tr>
    </table>
     
<%
	}
	else
	{	
%>

<body bottommargin="0" leftmargin="0" rightmargin="0" topmargin="0" marginheight="0" marginwidth="0">
  <table width="100%" border="0" cellpadding="0" cellspacing="0" class="btprint" bgcolor="Green" background="image/Lobbytop.jpg">
    <tr valign="top">
      <td bgcolor="Green"><img border="0" src="image/VRM.GIF" width="130" height="72"></td>
      <td bgcolor="Green" background="image/Lobbytop.jpg" width="50%">
      <table>
      <tr valign="top" width="70%">
        <!--<td width="100%" height="72" align="right" valign="bottom" background="image/Lobbytop.jpg">-->
        <td width="70%" height="72">
           <%-- <img border="0"  id="mainTop"  height="72">--%>
            <%--<%--<img border="--%><%--0" src="mirror/image/lobby_logo.jpg" id="Img1" onload="javascript:adjustWidth(this)">--%> --%>          
        </td>
        <td align="right">
            <table border="0" cellpadding="0" cellspacing="0" width="80%">
               <tr>
                    <td align="right">
                    <table>
                       <tr>
                         <td align="left"><b><font face="Verdana" size="1" color="#FF0000"><%=Session["userName"].ToString()%></font></b></td>
                         <td align="left">
                                <table border="0">
                                    <tr>
                                        <td>|</td>
                                        <td nowrap align="left">
                                            <table border="0">
                                                <tr>                            
                                                <td align="center">
                                                <a href="thankyou.aspx" title="Logout">Logout</a>                            
                                                </td>
                                                </tr>
                                            </table>                            
                                        </td>
                                    </tr>
                                </table>
                            </td>
                       </tr>
                       <tr>
                            <td align="left"><b><font face="Verdana" size="1" color="#FF0000"><%=Session["organizationName"].ToString()%></font></b></td>
                            <td align="left">
                            <table border="0">
                            <tr>
                                <%--FB 1639--%>
                                <% if((Session["UsrCrossAccess"].ToString().Trim()=="1")&& Session["OrganizationsLimit"].ToString().Trim()!= "1")
                                   {
                                %>
                                            <td>|</td>
                                            <td nowrap align="left">
                                                <table border="0">
                                                    <tr>                            
                                                        <td align="center">
                                                        <%-- FB 1849--%> 
                                                        <%--<a href="SuperAdministrator.aspx?c=1">Change</a>--%>                            
                                                        <a href="OrganisationSettings.aspx?c=1">Change</a>                           
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                <% }%>
                                   <%--else
                                   { Response.Write("&nbsp;");}--%>
                               </tr>
                               </table>
                           </td>
                            </tr>
                            </table>
                            </td>
                        </tr>
            </table>
          </td>
        <td width="0px">         
        <form name="frmMenu">
          <input type="hidden" name="menumask" value="<% =Session["sMenuMask"] %>">
          <input type="hidden" name="adminlevel" value="<% =Session["admin"] %>">
          <input type="hidden" name="userinterface" value="<% =Session["userInterface"] %>">
          <input type="hidden" name="feedback_enable" value="<% =Session["hasFeedback"] %>">
          <input type="hidden" name="help_enable" value="<% =Session["hasHelp"] %>">
          <input type="hidden" name="roomfood" value="<%=Convert.ToInt16(Session["hkModule"]) * 4 + Convert.ToInt16(Session["foodModule"])*2 + Convert.ToInt16(Session["roomModule"])%>">
          <input type="hidden" name="p2p" value="<%=Session["P2PEnable"]%>">
          <input type="hidden" name="ssoMode" value="<%=Application["ssoMode"]%>">
          <input type="hidden" name="txtClient" id="Hidden1" value="<%=Application["Client"]%>">
          <input type="hidden" name="txtSAlerts" id="Hidden2" value="<%= Session["Alerts"] %>" />
          <input type="hidden" name="txtSAlertsP" id="Hidden3" value="<%= Session["AlertsP"] %>" />
          <input type="hidden" name="txtSAlertsA" id="Hidden4" value="<%= Session["AlertsA"] %>" />
         
        </form>
      </td>
      <td width="50%" bgcolor="Green" background="image/Lobbytop.jpg"><img border="0" src="image/topright.jpg" width="580" height="72"></td>
    </tr>
    </table>
    </td>
    </tr>
  </table>

<%
}

%>
  

<!-- script begin -->
<script Language="JavaScript">
<!--
        // FB 2827 Starts
        if('<%=Session["SettingsMenu"]%>'=="0")                 
         document.getElementById("lstSetting").style.display = "none";          
            
          //FB 2968 START
//        if('<%=Session["isExpressUser"]%>'=="1")
//        {
//         document.getElementById("lstSearch").style.display = "none";
//         //document.getElementById('mainTop').style.visibility = "hidden";
//        } 
        //FB 2968 END
        
        if(('<%=Session["UsrCrossAccess"]%>'=="1")&& ('<%=Session["OrganizationsLimit"]%>'!= "1"))
         {
            document.getElementById("switchorgMain").style.visibility = "visible";    
            var OrgList = '<%= Session["orgList"].ToString() %>';            
            OrgList = OrgList.split(",");
            var content, orgID, orgName;

            for (var i = 0; i < OrgList.length-1; i++) 
            {            
                orgID = OrgList[i].split("~")[0];
                orgName = OrgList[i].split("~")[1];                
                if(i==0)
                content = "<li><a style='white-space:nowrap; overflow:hidden; width:150px' href='OrganisationSettings.aspx?c="+orgID+"'>"+orgName+"</a></li>"; // FB 2811
                else
                content += "<li><a style='white-space:nowrap; overflow:hidden; width:150px' href='OrganisationSettings.aspx?c="+orgID+"' >"+orgName+"</a></li>"; // FB 2811
            }            
            document.getElementById("orglist").innerHTML = content;
            document.getElementById("orglist").style.visibility = "visible";            
        }
        // FB 2827 Ends
var imtime1 = parseInt("<%=Session["ImRefreshRate"]%>", 10);
imtime1 = ( (imtime1 < 0.5) || isNaN(imtime1) ) ? 0.5 : imtime1;

//-->
</script>

<script type="text/javascript" src="script/errorList.js"></script>
<div class="btprint" id="mainmenu" style="position:fixed; z-index:1000"> <%-- FB 2827 --%>
	<script language="javascript" src="inc/menuinc.js"></script>
	<script type="text/javascript"> // FB 2050
	    var path = '<%=Session["OrgJsPath"]%>';
	    path = path.split('.')[0] + '<%=Session["ThemeType"]%>' + ".js"; // FB 2827
    document.write('<script type="text/javascript" src="' + path + '?' + new Date().getTime() + '"><\/script>');
    </script>
    <script language="javascript1.1" src="mmenu.js"></script>
</div>

<script language="javascript1.1" src="script/ajaxmulti.js"></script>
<link rel="StyleSheet" href="css\myprompt.css" type="text/css" />
<script language="javascript" src="script/showimuser.js"></script>


<a name="top"></a>

<script Language="JavaScript">
<!--
showMenu(0, "", "", "", "", "<%=Session["IMEnabled"]%>", "<%=Session["listenOn"]%>");

//-->
</script>
<div id="topSpace" style="height:120px"></div><%-- FB 2827 --%>
  <%--<table width="100%" border="0" cellpadding="0" cellspacing="0" class="btprint">
        <tr>
             <td colspan="2"> --%>   
                <table width="100%" border="0" cellpadding="0" cellspacing="0" class="btprint">  
                    <tr>
                        <td>

<% 
if (Session["tickerStatus1"].Equals("1") && Session["tickerStatus"].Equals("1") ) { 
%>
          <table width="100%" border="0" cellpadding="0" cellspacing="0">
            <tr valign="top">
              <td width="3%"></td>
              <td align="left">
                        <%--<b><font face="Verdana" size="1" color="#FF0000">

<%
Response.Write("Welcome " + Session["userName"] + " - ("+ Session["organizationName"].ToString() + ")" );
%>

</font></b>--%>
               </td>
               <td align="right">
                    <b><font face="Verdana" size="1" color="#FF0000">
                    	
                    </font></b>
                  </td>
                </tr>
              </table>
            </td>
          </tr>
<% 
}
%>



<% 
if (Session["tickerStatus"].Equals("0") && Session["tickerPosition"].Equals("0") ) { 
%>
<tr>
<td style="width:4px"></td>
<td  bgcolor="#848484" style="font-weight:bold;" nowrap><div id="martickerDiv" style="width:999px; overflow:hidden"> <%-- FB 2050 --%>
<% 
if(Session["tickerDisplay"].Equals("0")) { 
%>
<% 
Response.Write("My Conferences");
%> <%} %>
<% 
if(Session["tickerDisplay"].Equals("1")) { 
%>
<% 
Response.Write(Session["RSSTitle"]);
%> <%} %>
<%--Edited for FF--%><marquee id="marticker"   onmouseover="this.stop()" onmouseout="this.start()" 
bgcolor="<%=Session["tickerBackground"]%>" SCROLLAMOUNT="<%=Session["tickerSpeed"]%>"></>
<%
       string dtCurrent;
		string frmtDT;
		DateTime dt;
		char[] splitter  = {'/'};
		
		dt = DateTime.Parse(Session["systemDate"].ToString() +"  "+  Session["systemTime"].ToString());
		
		frmtDT = dt.ToString("dddd, MMMM dd yyyy");
		
		if(Session["FormatDateType"] != null)
		{
		
		    if (Session["FormatDateType"].ToString() == "dd/MM/yyyy")
		        frmtDT = dt.ToString("dddd, dd MMMM yyyy");
		
		}
		    
		    
		dtCurrent= frmtDT + ", " + " Current time is " + Session["systemTime"] ;
		
		if(Session["timeFormat"] != null)
		{
		    if(Session["timeFormat"].ToString() == "0")
		    {
		        dtCurrent= frmtDT + ", " + " Current time is " + dt.ToString("HH:mm") +" " ;						        					        
		    }
		    
		}
		
		if(Session["timeZoneDisplay"] != null)
		{
		    if(Session["timeZoneDisplay"].ToString() == "1")
		    {
		        dtCurrent +=  " " + Session["systemTimezone"]  +" " ;					        					        
		    }
		}    
        Response.Write("Welcome " + Session["userName"] + ". "+ dtCurrent + "  "+ Session["ticker"] + " ");
    %>
</marquee></div> <%-- FB 2050 --%>
</td>
</tr>
<% 
}
%>
<% 
if (Session["tickerStatus1"].Equals("0") && Session["tickerPosition1"].Equals("0") ) { 
%>
<tr style="height:15px">
<td style="width:4px"></td>
<td  bgcolor="#BDBDBD" style="font-weight:bold;" nowrap><div id="marticDiv" style="width:999px; overflow:hidden"> <%-- FB 2050 --%>
<% 
if(Session["tickerDisplay1"].Equals("0")) { 
%>
<% 
Response.Write("My Conferences");
%> <%} %>
<% 
if(Session["tickerDisplay1"].Equals("1")) { 
%>
<% 
Response.Write(Session["RSSTitle1"]);
%> <%} %>
<%--Edited for FF--%><marquee id="martic"  onmouseover="this.stop()" onmouseout="this.start()" 
bgcolor="<%=Session["tickerBackground1"]%>" SCROLLAMOUNT="<%=Session["tickerSpeed1"]%>"></>
<%
       string dtCurrent1;
		string frmtDT1;
		DateTime dt1;
		char[] splitter1  = {'/'};
		
		dt1 = DateTime.Parse(Session["systemDate"].ToString() +"  "+  Session["systemTime"].ToString());
		
		frmtDT1 = dt1.ToString("dddd, MMMM dd yyyy");
		
		if(Session["FormatDateType"] != null)
		{
		
		    if (Session["FormatDateType"].ToString() == "dd/MM/yyyy")
		        frmtDT1 = dt1.ToString("dddd, dd MMMM yyyy");
		
		}
		    
		    
		dtCurrent1 = frmtDT1 + ", " + " Current time is " + Session["systemTime"] ;
		
		if(Session["timeFormat"] != null)
		{
		    if(Session["timeFormat"].ToString() == "0")
		    {
		        dtCurrent1 = frmtDT1 + ", " + " Current time is " + dt1.ToString("HH:mm") +" " ;					        					        
		    }
		    
		}
		
		if(Session["timeZoneDisplay"] != null)
		{
		    if(Session["timeZoneDisplay"].ToString() == "1")
		    {
		        dtCurrent1 +=  " " + Session["systemTimezone"]  +" " ;					        					        
		    }
		} 
		
       Response.Write("Welcome " + Session["userName"] + ". "+ dtCurrent1 + "  "+ Session["ticker1"] + " ");
    %>
 
</marquee></div> <%-- FB 2050 --%>
</td>
</tr>

<%} %>

   <% 

if (Application["global"].Equals("enable") ) { 
%>
  <tr>
    <td>
      <table height="25" cellSpacing="0" cellPadding="0" align="right" border="0" class="btprint">
        <tr>
          <td noWrap><span class="para_small">Choose language&nbsp;</span></td>
          <td>
            <table cellSpacing="0" cellPadding="1" border="0">
              <tr>
                <form name="frmInternational">
                  <td>
                    <select class="para_small"  onchange="international(this)" id="select1" name="select1">
                    <option value="en">English</option>
                    <option value="ch">Chinese Simplified</option>
                    <option value="zh">Chinese Traditional</option>
                    <option value="fr">French</option>
                    <option value="sp">Spanish</option>
                    </select>
                  </td>
                </form>
              </tr>
            </table>
          </td>
        </tr>
      </table>
    </td>
  </tr>
  <% 
}

%>


</table>
    
<script language="javascript" type="text/javascript">

  var obj = document.getElementById('mainTop');
          
    //FB 1830
   var std = '../en/' + '<%=Session["OrgBanner1024Path"]%>';
   var high = '../en/' + '<%=Session["OrgBanner1600Path"]%>';
    //var std = '../' + '<%=Session["OrgBanner1024Path"]%>';                    
  // var high = '../' + '<%=Session["OrgBanner1600Path"]%>';


   //obj.style.width=window.screen.width-25; // Commented to show actual size of Image
   //FB ICON
   if (obj != null) {
       if (window.screen.width <= 1024)
           obj.src = std;
       else
           obj.src = high;
   }       
    
    /*
    if(obj != null)
    {
    if (window.screen.width <= 1152)
        {
            obj.src = std;
            obj.style.width = window.screen.width - 220;//FB 1981
        }
    else
        {
            obj.src = high;
            //FB 1633 start
            img = new Image(); 
            img.src = obj.src;
            obj.style.width= img.width;   
            //FB 1633 end
        }
        
     }
     if(navigator.appName != "Microsoft Internet Explorer") //Edited for FF
     { 
        //obj.style.height ='72';
        obj.style.height ='70'; //FB 1836//FB 1981
     } */
   document.getElementById("menu1").style.top = "";

   // FB 2827 Starts
   if (document.getElementById("menuCalRed") != null) {
       document.getElementById("menuCalRed").parentNode.style.borderLeft = '#ccc 1px solid';
       document.getElementById("menuCalRed").parentNode.style.borderRight = '#ccc 1px solid';
   }
   if (document.getElementById("menuSetRed") != null) {
       document.getElementById("menuSetRed").parentNode.style.borderLeft = '#ccc 1px solid';
       document.getElementById("menuSetRed").parentNode.style.borderRight = '#ccc 1px solid';
   }

   if (document.getElementById("menuCalBlue") != null) {
       document.getElementById("menuCalBlue").parentNode.style.borderLeft = '#ccc 1px solid';
       document.getElementById("menuCalBlue").parentNode.style.borderRight = '#ccc 1px solid';
   }
   if (document.getElementById("menuSetBlue") != null) {
       document.getElementById("menuSetBlue").parentNode.style.borderLeft = '#ccc 1px solid';
       document.getElementById("menuSetBlue").parentNode.style.borderRight = '#ccc 1px solid';
   }

   if (document.getElementById("menuCal") != null) {
       document.getElementById("menuCal").parentNode.style.borderLeft = '#ccc 1px solid';
       document.getElementById("menuCal").parentNode.style.borderRight = '#ccc 1px solid';
   }
   if (document.getElementById("menuSet") != null) {
       document.getElementById("menuSet").parentNode.style.borderLeft = '#ccc 1px solid';
       document.getElementById("menuSet").parentNode.style.borderRight = '#ccc 1px solid';
   }

   if (document.getElementById("menuHomeRed") != null) {
       document.getElementById("menuHomeRed").parentNode.style.borderLeft = '#ccc 1px solid';
       document.getElementById("menuHomeRed").parentNode.style.borderRight = '#ccc 1px solid';
   }

   if (document.getElementById("menuCallRed") != null) {
       document.getElementById("menuCallRed").parentNode.style.borderLeft = '#ccc 1px solid';
       document.getElementById("menuCallRed").parentNode.style.borderRight = '#ccc 1px solid';
   }

   if (document.getElementById("menuReservationRed") != null) {
       document.getElementById("menuReservationRed").parentNode.style.borderLeft = '#ccc 1px solid';
       document.getElementById("menuReservationRed").parentNode.style.borderRight = '#ccc 1px solid';
   }

   if (document.getElementById("menuHomeBlue") != null) {
       document.getElementById("menuHomeBlue").parentNode.style.borderLeft = '#ccc 1px solid';
       document.getElementById("menuHomeBlue").parentNode.style.borderRight = '#ccc 1px solid';
   }

   if (document.getElementById("menuCallBlue") != null) {
       document.getElementById("menuCallBlue").parentNode.style.borderLeft = '#ccc 1px solid';
       document.getElementById("menuCallBlue").parentNode.style.borderRight = '#ccc 1px solid';
   }

   if (document.getElementById("menuReservationBlue") != null) {
       document.getElementById("menuReservationBlue").parentNode.style.borderLeft = '#ccc 1px solid';
       document.getElementById("menuReservationBlue").parentNode.style.borderRight = '#ccc 1px solid';
   }

   if (document.getElementById("menuHome") != null) {
       document.getElementById("menuHome").parentNode.style.borderLeft = '#ccc 1px solid';
       document.getElementById("menuHome").parentNode.style.borderRight = '#ccc 1px solid';
   }

   if (document.getElementById("menuCall") != null) {
       document.getElementById("menuCall").parentNode.style.borderLeft = '#ccc 1px solid';
       document.getElementById("menuCall").parentNode.style.borderRight = '#ccc 1px solid';
   }

   if (document.getElementById("menuReservation") != null) {
       document.getElementById("menuReservation").parentNode.style.borderLeft = '#ccc 1px solid';
       document.getElementById("menuReservation").parentNode.style.borderRight = '#ccc 1px solid';
   }

   if (document.getElementById("menu1") != null) {
       document.getElementById("menu1").style.borderRight = '#ccc 1px solid';
       document.getElementById("menu1").style.top = '5px'; // FB 2996
   }

   var themeType = '<%=Session["ThemeType"]%>';
   if (themeType == 2) {
       document.getElementById("menuTopLine").style.backgroundColor = "#CD2522";
   }
   else if (themeType == 1) {
       document.getElementById("menuTopLine").style.backgroundColor = "#4277B3";
   }
   else
       document.getElementById("menuTopLine").style.backgroundColor = "#cccccc";
   // FB 2827 Ends

  </script>
</body>
</html>

<%--FB 3027--%>
<%--<table width="100%" cellpadding="6">
    <tr>
      <td>--%>



  
 

