<%--ZD 100147 start--%>
<%--/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.	
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/--%><%--ZD 100147 End--%>
<%--FB 2779 Starts--%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<meta http-equiv="X-UA-Compatible" content="IE=8" />
<%--FB 2779 Ends--%>
<!-- #INCLUDE FILE="inc/maintop3.aspx" -->

<script type="text/javascript">    // FB 2790
    var path = '<%=Session["OrgCSSPath"]%>';
    path = path.split('.')[0] + '<%=Session["ThemeType"]%>' + ".css"; // FB 2815
    document.write('<link rel="stylesheet" title="Expedite base styles" type="text/css" href="' + path + '?' + new Date().getTime() + '">');
</script>
<script language="javascript1.1" src="extract.js"></script>
<script language="JavaScript" src="inc\functions.js"></script>
<script type="text/javascript" src="script/wincheck.js"></script> <%--FB 3055--%>

<script language="JavaScript">
<!--

function addLecturerInfo()
{
	cb = document.frmHostinfoinput.Lecturer;
	
	if (cb.selectedIndex == -1) {
		alert("Error: Please select one for lecturer.")
		return false;
	}

	opener.document.frmSettings2.Lecturer.value = cb[cb.selectedIndex].value;
    //Code Changed by Offshore FB issue no:412-Start
    opener.document.getElementById("hdnSubmit").value = "M";
    //Code Changed by Offshore FB issue no:412-End
	opener.document.frmSettings2.submit();

	window.close();
}

//-->
</script>


<form name="frmHostinfoinput" method="POST" >

  <center>
  <table border="0" width="100%" cellspacing="2" cellpadding="2">
    <tr>
      <td>
        <font color='blue'><b>
        Because this is a lecture conference, Please select one participant from the list as lecturer.
        </b></font>
      </td>
    </tr>
  </table>
  
  <br>
  
  <table border="0" width="80%" cellspacing="0" cellpadding="0">
    <tr>
      <td align="center">
      
        
                    
<script language="JavaScript">
<!--

//Code Changed by Offshore FB issue no:412-Start
if(opener.document.location.href.indexOf(".aspx") > 0)
{
    if (opener.document.frmSettings2.txtPartysInfo) {
        partysinfo = opener.document.frmSettings2.txtPartysInfo.value;
    } else {
        partysinfo = ""
        setTimeout('window.location.reload();',500000);
    }

}
else
{
 if (opener.document.frmSettings2.PartysInfo) {
        partysinfo = opener.document.frmSettings2.PartysInfo.value;
    } else {
        partysinfo = ""
        setTimeout('window.location.reload();',500000);
    }
}
//Code Changed by Offshore FB issue no:412-End

//<option value='1'>1</option>
var _d = document;
var num = 0;
var m = "<select name='Lecturer' size='4' class='SelectFormat'>";//Edited for FF Fixes
var mt = "";

/*
if ( (queryField("h") == "1") || (queryField("h") == "2") ) {
	mt +=  "<option value='" + opener.document.frmSettings2.hostemail.value + "'>" + "<%=Session["userName"]%>" + "</option>";
	num ++;
}
*/

//partysary = partysinfo.split(";");
partysary = partysinfo.split("||");//FB 1957

for (var i=0; i < partysary.length-1; i++) {
//	tdbgcolor = (i % 2 == 0) ? "" : "#E0E0E0";
	//pary = partysary[i].split(",");
	pary = partysary[i].split("!!");//FB 1957
	pemail = pary[3];
	//if ( (pary[4] == "1")) { //|| (pary[5] == "1") 
	if ( (pary[4] == "1") || (pary[5] == "1") ) // Changed for FB 1721 //FB 1957
	{ 
		mt +=  "<option value='" + pary[3] + "'" + ((queryField("l").toLowerCase() == pary[3].toLowerCase()) ? " selected" : "") +">" + pary[1] + " " + pary[2] + "</option>";
		num ++;
	}
	
}

if (num == 0)
	m = "<div align='left'><font color=red>No External Attendees or Room Attendees for lecturer.<br><br>Please re-select your particpants and their status.</font></div>";
else
	m = "<select name='Lecturer' size='4' class='SelectFormat'>" + mt + "</select>"; //Edited for FF Fixes

_d.write(m);

//-->
</script>
	
      </td>
    </tr>
  </table>
  </center>

  <br>
  
  <table border="0" width="90%">
    <tr>
      <td align="center">
        <input type="button" value="Cancel" name="btncancel" onClick="Javascript: window.close();" class="altShortBlueButtonFormat"></td>
      </td>


<script language="JavaScript">
<!--

	if (num != 0) {
		m  = "<td align='center'>";
		m += "  <input type='button' value='Submit' name='btnsubmit' onClick='Javascript: addLecturerInfo();' class='altShortBlueButtonFormat'>";
		m += "</td>";
		
		document.write(m);
		
		document.frmHostinfoinput.btnsubmit.focus();
	} else	
		document.frmHostinfoinput.btncancel.focus();

//-->
</script>


    </tr>
  </table>

</form>

</body>
</html>