<%--ZD 100147 start--%>
<%--/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.	
*
* You should have received a copy of the myVRM license with	
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/--%><%--ZD 100147 End--%>
<%@ Page Language="C#" AutoEventWireup="true" Inherits="ns_license.license" %>

<%--FB 2779 Starts--%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<meta http-equiv="X-UA-Compatible" content="IE=8" />
<%--FB 2779 Ends--%>

<!-- #INCLUDE FILE="inc/maintop3.aspx" -->
<script language="javascript" type="text/javascript">

function declineLicense(){
	var msg = "\n Are you sure you don't want \n " +
				"to continue the myVRM demo?";
				
	if (confirm(msg)){
		window.location='<%=Application["loginPage"]%>';
	}

}

</script>
<script type="text/javascript" src="script/wincheck.js"></script> <%--FB 3055--%>

  <table width="100%" border="0" cellpadding="0" cellspacing="0">
    <tr valign="top">
      <td style="background:image/vrmtop.gif" width="100%" height="72">
      </td>
    </tr>
  </table>


<center>

<table width="100%" border="0" cellpadding="0" cellspacing="0">
  <tr valign="top">
    <td width="20%" valign="top" align="right">
	  <br/><br/><br/><br/><br/><br/><br/><br/>
      <img src="<%=Application["company_logo"].ToString() %>">
    </td>

    <td width="80%">
      <br/><br/><br/>

<%if(init == "1") {%>
      <form method="post" action="reservationOption.aspx" name="frmLicense"/>
<%}else {%>
	<form method="post" action="demoInit.aspx" name="frmLicense">
<% } %>
		<table width="100%" cellpadding="6" border="0">
			<tr><td align="middle" colspan="3">Please read the following License Agreement and choose to accept or decline it.</td></tr>
		  <tr>
		  <td align="middle" colspan=3>
		  <input type="hidden" name="start" value=0/>
		  <input type="hidden" name="cmd" value="GetHome"/>
		  <%--FB 1882 EULA - address change--%> 
		  <textarea wrap="soft" style="WIDTH: 700px; HEIGHT: 350px; padding-left:40px; padding-right:20px;" name=agreement rows=80 readOnly>	
				LICENSE AGREEMENT

THIS LICENSE AGREEMENT by and between Interactive Ideas, LLC., a Delaware Limited Liability Corporation d/b/a myVRM having an office at 325 Duffy Ave., Hicksville, NY 11801 ("myVRM"), and the party that has indicated its acceptance to this agreement (the "Licensee").

WHEREAS, myVRM is the sole and exclusive owner of certain video conferencing software known as Versatile Resource Management ("myVRM") and the Licensee desires to utilize the myVRM software in its business; and

WHEREAS, myVRM has agreed to license the myVRM software to the Licensee upon the terms and conditions set forth in this Agreement.

NOW, THEREFORE, in consideration of the mutual promises, agreements and covenants set forth herein, the parties, each intending to be legally bound hereby, do hereby agree as follows:

1. License Grant.  
myVRM hereby grants to the Licensee a fully paid non-exclusive and non-transferable, non-sublicenseable license (the "License" ) to use the myVRM software modules listed on Schedule A hereto (the "Software") at only the locations identified on Schedule A hereto to manage the Licensee's video conferencing as provided herein.

2. License Term.  
The term of the License shall be perpetual, commencing upon myVRM's receipt of payment of the License fee ( as hereinafter defined) , subject to termination as provided herein (the "License Term").  

3. License Fees.  
The Licensee shall pay to myVRM a one-time license fee in the amount set forth on Schedule B hereto (the "License Fee").  The License Fee shall cover the license to use the Software as provided herein, for an indefinite time, and maintenance of the Software during the Thirty Day Period. After the Thirty Day Period, continued usage of the Software as provided herein shall not require further payment.  

4. Provision of Services.  
During the thirty day period from the date the Software is delivered to the Licensee (the "Thirty Day Period"), myVRM shall provide such installation, training and maintenance services to the Licensee as provided on Schedule C hereto.  After the Thirty Day Period, myVRM shall provide such Maintenance Services to the Licensee as mutually determined between myVRM and the Licensee.

5. Maintenance Services. 
If the Licensee desires to utilize myVRM's Maintenance Services after the Thirty Day Period, the Licensee shall pay to myVRM an annual maintenance fee (the "Maintenance Fee") in an amount equal to myVRM's then prevailing rate within 10 days of the date of execution of a Maintenance Services Agreement. The Maintenance Services Agreement shall automatically renew for a successive period at myVRM's then prevailing Maintenance Fee subject to termination as set forth in this agreement.  If the Licensee fails to timely pay the applicable Maintenance Fee, myVRM shall be under no obligation to provide maintenance services or provide necessary updates or patches to the Licensee.  If the Licensee desires to reinstate maintenance thereafter, the Licensee shall pay to myVRM a fee equal to 150% of myVRM's then prevailing Maintenance Fee for each year for which the Maintenance Fee has not been paid.  All fees shall be due and paid by the Licensee upon invoicing.  Any fees that are not paid within 30 days of invoicing shall accrue a late fee of 1.5% per annum until paid in full.

6. Software Updates.  
During the License Term, so long as the myVRM is providing Maintenance Services and the Licensee continues to pay the Maintenance Fee to myVRM, myVRM shall provide to the Licensee all updates and patches to the Software that myVRM deems necessary without additional charge.  If the Licensee desires any updates, patches or software modules to the Software in addition to those that myVRM deems necessary, the Licensee may request such additional updates, patches or software modules from myVRM in writing, and if myVRM agrees, in its discretion to provide such updates, patches or software modules, it shall do so and the Licensee shall pay myVRM's then standard fees therefore upon receipt of an invoice.  The term "Software" shall include all updates and patches to the Software and any additional software modules licensed by the Licensee pursuant to this Agreement.

7. Ownership of Software.  
It is understood and agreed that myVRM owns, and shall retain, all right, title and interest in and to the Software and any patent, patent application, trademark, registered or unregistered, copyright, trade secret, good will and other intellectual and proprietary rights therein (collectively, the "Intellectual Property Rights").  The Licensee shall not challenge myVRM's ownership of the Software or the Intellectual Property Rights, and the Licensee shall not obtain any rights thereto except the License expressly provided herein, subject to all laws protecting trade secrets, know-how and the like. The Licensee shall not be entitled to make any modifications, changes, enhancements or improvements to the Software without the prior written consent of myVRM.  It is understood and agreed that myVRM shall retain all right, title and interest in and to any modifications, changes, enhancements, or improvements made to the Software by the Licensee.  The Licensee shall not decompile, disassemble, reverse assemble or otherwise reverse engineer or attempt to reconstruct or deconstruct any software code or underlying ideas or algorithms of or relating to the Software.  The Licensee shall not copy, distribute, sublicense or use for the benefit of third parties the Software, except as expressly stated in this Agreement.

8. Confidentiality.  
a) The Licensee, its employees, agents, officers, directors and affiliates shall 
hold in confidence and not disclose or use, directly or indirectly, for its or their benefit
(except as expressly provided in this Agreement) any confidential information of, or 
received from, myVRM.  Confidential information shall include, without limitation, all 
data, reports, designs, programs, trade secrets, proprietary information, interpretations, 
forecasts and records containing or otherwise reflecting information concerning myVRM, 
the Software and the installation, and the maintenance thereof, training of personnel with 
respect to the use and operation of the Software and/or the terms of this Agreement (the 
"Confidential Information").  Confidential Information shall not include  (i) information 
already known to the Licensee when received, (ii) information in the public domain when 
received or thereafter in the public domain through sources other than myVRM, or 
myVRM's employees, officers, directors, consultants, contractors, as its agents or 
representatives ("Representatives"), or (iii) information lawfully obtained from a third 
party not subject to a confidentiality obligation to myVRM. 
b) The Licensee agrees to monitor the activities  of all its Representatives 
who receive or otherwise gain access to Confidential Information for the purpose of 
ensuring compliance with this Agreement.
c) In the event that the Licensee or its representatives are requested in any 
judicial or administrative proceeding to disclose any Confidential Information, the
Licensee will give prompt notice of such request to myVRM so that myVRM may seek 
an appropriate protective order.  If, in the absence of a protective order (or other 
protective remedy), the Licensee or its Representatives are nonetheless compelled to 
disclose Review Material, the Licensee or its Representatives may disclose such 
information without liability hereunder, provided, however, (i) that the Licensee give
written notice of the information to be disclosed as far in advance of its disclosure as is
practable and, upon myVRM's request, use its best efforts to obtain assurances that
confidential treatment will be accorded to such Confidential Information; and (ii) only
that portion of the Confidential Information which the Licensee is advised in writing by 
its counsel is legally required to be disclosed will be disclosed.  
d) The Licensee agrees that money damages would not be a sufficient remedy 
for any breach of this agreement by it or its Representatives, and that, without the
necessity of proving damages and in addition to all other remedies, myVRM shall be 
entitled to specific performance and injunctive or other equitable relief as a remedy for 
any such breach, and the Licensee further agrees to waive and to use its best efforts to 
cause it Representatives to waive, any requirements for securing or posting of any
bond in connection with such remedy.

9. System Maintenance and Back-up.  
a) Prior to the installation of the Software and during all times that (i) the 
Licensee uses the Software and (ii) the Licensee uses myVRM's Maintenance Services, 
the Licensee shall be responsible for ensuring that its computer hardware and operating 
system is sufficient to install and operate the Software.
b) The Licensee shall back up all of the data, configurations and settings 
on all of its MCUs, networks and servers not more than 24 hours prior to the installation 
of any Software or the provisions of the Maintenance Services.

10. Warranty.  
a) Provided that the Licensee is in compliance with Section 9 hereof and the 
Software is properly installed in accordance with Schedule B hereof, myVRM warrants 
to the Licensee that the Software will operate substantially in accordance with the 
specifications set forth in the user guide for the Software as in effect on the date the 
Software is installed for a period of 30 days from the date the Software is installed (the 
"Warranty") .  In order to invoke the foregoing Warranty, the Licensee must notify
myVRM in writing within thirty (30)  days following the installation of the Software to
the Licensee of any defect in the Software.  In the event that the Licensee determines that the Software does not operate substantially in accordance with its specifications and 
reports such defect to myVRM in writing during such 30-day period, myVRM shall use 
reasonable efforts, consistent with industry standards, to cure such defect.  The 
Licensee's sole remedy for breach of the Warranty shall be repair of the 
Software, replacement of the Software or refund of the License Fee paid hereunder, at 
myVRM's sole option.  If the Licensee makes any modifications, changes, 
enhancements, or improvements to the Software during such 30-day period, the Warranty 
shall be of no force and effect.  myVRM shall not be liable under the Warranty if its 
testing and examination disclose that the alleged defect or malfunction in the Software 
does not exist or was caused by the Licensee's or any third party's misuse, negligence, 
improper installation or test (other than installation or testing by employees or agents of
myVRM), unauthorized attempts to open, repair ,modify, decompile, disassemble, 
reverse engineer or reconstruct the Software, use of the Software with Software as on 
hardware not provided or approved in writing by myVRM, use of Software outside of the 
scope of the License, or any other cause beyond the range of intended use, 
use contrary to myVRM's specifications or instructions, damage caused by computer 
'viruses,' or by accident, disaster, fire, lightning, other hazards, failure or defect of 
electrical power, external circuitry, or air conditioning or humidity, moisture,  acts of 
terrorism or acts of God or the Licensee's non-compliance with Section 9 hereof
myVRM makes no warranty in respect of the Software after expiration of the Warranty.
b) Correction for difficulties or defects traceable matters that are outside the 
scope of the Warranty, that are requested by the Licensee shall be billed by myVRM to 
the Licensee at myVRM's standard time and material charges.   If the Licensee reports 
any defects to myVRM after the expiration of the Warranty, myVRM shall use 
commercially reasonable efforts to incorporate a cure for such defects into a patch that 
may be released to its customers as it deems appropriate, in myVRM's sole discretion.

11. Disclaimers; Limitation of Liability.    
TO THE FULLEST EXTENT ALLOWED BY LAW, THE WARRANTY AND REMEDIES SET FORTH IN SECTION 9 HEREOF ARE EXCLUSIVE AND IN LIEU OF ALL OTHER WARRANTIES OR CONDITIONS, EXPRESS OR IMPLIED, EITHER IN FACT OR BY OPERATION OF LAW, STATUTORY OR OTHERWISE, INCLUDING WARRANTIES OR CONDITIONS OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE, SATISFACTORY QUALITY, CORRESPONDENCE WITH DESCRIPTION OR ARISING FROM A COURSE OF DEALING OR USAGE OF TRADE AND ANY WARRANTIES OF NONINFRINGEMENT, ALL OF WHICH ARE EXPRESSLY DISCLAIMED.  THE WARRANTY CONTAINED IN SECTION 9 HEREOF RUNS ONLY TO THE LICENSEE AND IS NOT EXTENDED TO ANY THIRD PARTIES.  IN NO EVENT WILL myVRM BE LIABLE TO THE LICENSEE OR ANY OTHER PARTY FOR ANY CLAIM OF LOSS, INCLUDING TIME, MONEY, GOODWILL OR CONSEQUENTIAL, EXEMPLARY, SPECIAL OR INCIDENTAL DAMAGES, WHICH MAY ARISE FROM THE USE, OPERATION, OR MODIFICATION OF THE SOFTWARE.  IN THE EVENT THAT THE ABOVE LIABILITY LIMITATION IS FOUND TO BE INVALID UNDER APPLICABLE LAW, THEN myVRM'S LIABILITY FOR ANY SUCH CLAIM SHALL BE LIMITED TO THE LICENSE FEE ACTUALLY RECEIVED BY myVRM UNDER THIS AGREEMENT.  myVRM SHALL HAVE NO LIABILITY TO THE LICENSEE IN RESPECT OF FAILED CALLS, LOST CALLS, MISSED CALLS, FAILURE OF THE SOFTWARE TO DISENGAGE A CALL OR LAUNCH UNWANTED CALLS OR LOSS OF DATA, CONFIGURATIONS OR SETTINGS. THE LIABILITY OF myVRM UNDER THIS AGREEMENT, WHETHER CONTRACTUAL OR IN TORT, SHALL BE LIMITED AS SET FORTH ABOVE AND SUCH LIMITATION SHALL APPLY NOTWITHSTANDING THE ELECTION OF THE LICENSEE, IF SO ENTITLED, TO TERMINATE OR BE DISCHARGED FROM THIS AGREEMENT.

12. Marketing and References.  
The Licensee shall permit myVRM to use the Licensee's name in connection with the 
promotion and marketing of the Software, myVRM and its software and video conferencing 
software solutions and shall permit myVRM to use the Licensee's name as a positive 
reference in connection with myVRM's efforts to sell or license its products to 
customers.

13. Termination. 
a) Right of Termination.  myVRM shall have the right to immediately 
terminate this Agreement, including the License granted herein, by giving written notice
to the Licensee in the event that the Licensee breaches or threatens to breach Section 6 or 
7 of this Agreement. 
b) Post-Termination Rights.  Except as otherwise provided herein, upon the 
termination of this Agreement pursuant to Section 12(a) above, all of the rights of the 
Licensee under this Agreement shall forthwith terminate and immediately revert to 
myVRM and the Licensee shall immediately discontinue all use of the Software at no 
cost whatsoever to myVRM.  Upon such termination, the Licensee shall immediately 
return to myVRM all material relating to the Software, at no cost whatsoever to myVRM.  
c) Survival of Provisions.  The provisions of Sections 6, 7, 10, 12(b) and (c), 
13, 14, 15 and 17 through 24 shall survive the expiration or termination of this Agreement.
d) Maintenance Termination.  myVRM shall have the right to discontinue the
provision of Maintenance Services to the Licensee if (i) the Licensee fails to make any payment of Maintenance Fees hereunder within ten (10) days following its due date. (ii) the Licensee breaches any other provision of this Agreement and fails to cure such breach after fifteen (15) days prior written notice thereof from myVRM to the Licensee, or (iii) myVRM sells or assigns its rights, title and interest in the Software to a third party, upon thirty (30) days prior written notice thereof given by myVRM to the Licensee.  The termination of Maintenance Services shall have no effect on the License granted herein.

14. Taxes.  
The Licensee shall, in addition to other amounts payable under this Agreement, pay all 
sales, use and other taxes, federal, state, or otherwise, however designated, which are 
levied or imposed by reason of the transactions contemplated by this Agreement.

15. Independent Contractors. 
The relationship established by this Agreement is that of independent contractors. 
Nothing contained herein shall constitute this arrangement to be employment, a joint 
venture or a partnership.

16. Notices.
All notices, consents, demands, requests, approvals and other communications which are
required or may be given hereunder shall be in writing and shall be deemed to have been
duly given (a) when delivered personally, (b) the second day following the day on which
the same has been delivered prepaid to a reputable national overnight courier service, or 
(c) if sent by mail, three business days following deposit in the mail as registered or
certified, postage prepaid in each case.  All such  notices shall be addressed to myVRM, 
at its address set forth in the header to this Agreement and to the Licensee at its address
set forth on the signature page to this Agreement, or to such other person or persons at
such address or addresses as may be designated by written notice hereunder.

17. Assignment.  
The Licensee may not assign this Agreement and its rights and obligations hereunder 
without the prior written consent of myVRM.  myVRM may assign this Agreement and 
its rights and obligations hereunder upon five (5) days' prior notice to the Licensee.  Any 
attempted assignment in violation of this Section 15 shall be null and void and of no 
effect whatsoever.

18. Headings.  
Headings in this Agreement are for convenience of reference only and shall not be 
deemed to have any substantive effect.

19. Severability. 
In the event that any part of this Agreement shall be held to be invalid or unenforceable, 
the remaining parts thereof shall nevertheless continue to be valid and enforceable as 
though the invalid portions were not a part hereof.

20. Construction.  
Whenever used herein, the singular number shall include the plural, the plural the
singular and the use of the masculine, feminine or neuter gender shall include all genders.

21. Binding Effect; Benefit.  
This Agreement shall inure to the benefit of and be binding upon the parties hereto and
their respective successors and permitted assigns; provided, however, that nothing in this 
Agreement, expressed or implied, is intended to confer on any person other than the 
parties hereto or their respective successors and assigns, any rights, remedies, obligations 
or liabilities under or by reason of this Agreement.

22. Waiver.  
No delay or failure of any party hereto to exercise any right, remedy or power hereunder 
shall impair the same or be construed as a waiver thereof.  The waiver by either party 
hereto of a breach of any provision of this Agreement shall not operate or be construed as 
a waiver of any subsequent breach.

23. Entire Agreement; Amendment.  
This Agreement (including the Schedules hereto which constitute an integral part hereof) 
embodies the entire agreement and understanding of the parties hereto with respect to the 
subject matter hereof and supersedes any prior agreement or understanding between the 
parties.  This Agreement may be modified or amended only by a writing signed by each 
of the parties hereto.

24. Governing Law and Jurisdiction.  
This Agreement shall be governed by, construed, interpreted and enforced in accordance 
with the laws of the State of New York, without reference to principles of conflicts of 
law.  Each of the parties hereto hereby irrevocably agrees that all actions or proceedings 
in any way, manner or respect, arising out of or from or related to this Agreement or the 
transactions referenced herein shall be litigated only in courts having situs within Nassau 
County, New York.  In connection therewith, each of the parties hereto hereby consents 
and submits to the jurisdiction of any local, state or federal court located within said 
county and state and hereby waives any rights such party may have to transfer or change 
the venue of any such litigation.

25. Representation by Counsel; Interpretation. 
The Licensee acknowledges that it has been represented by counsel or has had the
opportunity to be represented by counsel in connection with this Agreement and the 
transactions contemplated by this Agreement.  Accordingly, any rule or law or any legal 
decision that would require interpretation of any claimed ambiguities in this Agreement
against any party that drafted it has no application and is expressly waived by such 
parties.  The provisions of this Agreement shall be interpreted in a reasonable manner to 
effect the intent of the parties hereto.

26. Counterparts.  
This Agreement may be executed in one or more counterparts, each of which shall be 
deemed an original and all of which, when taken together, shall constitute one and the 
same instrument.

27. Legal Fees. 
In the event that myVRM brings an action against the Licensee to enforce any provision 
of this Agreement and substantially prevails, Licensee shall be required all legal fees, 
costs and disbursements.

			</TEXTAREA>
          </td>
        </tr>
        
        <tr>
			<td height="27" align="right"> 
				<input type="submit" name="Submit" value="Accept" class="altShortBlueButtonFormat"/>
			</td>
            <td width="30"> 
			</td>
            <td height="27"> 
				<input type="reset" name="Reset" value="Decline" class="altShortBlueButtonFormat" onclick="javascript:declineLicense()"/>
			</td>
        </tr>
      </table>
	</form>
    </td>
  </tr>
</table>

</center>

<script language="JavaScript" type="text/javascript">
<!--

	document.body.style.background = "<%=bgimg%>";
	document.body.style.margin = "0px";
	document.body.style.bgcolor = "#fff7e7";

//-->
</script>

</body>
</html>

