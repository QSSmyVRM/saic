<%--ZD 100147 start--%>
<%--/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.	
*
* You should have received a copy of the myVRM license with	
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/--%><%--ZD 100147 End--%>
<%@ Page Language="C#" Inherits="ns_Administration.logList" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> <!-- FB 2050 -->
<meta http-equiv="X-UA-Compatible" content="IE=8" /> <!-- FB 2050 --> <%--FB 2779--%>

<!-- #INCLUDE FILE="inc/maintopNET.aspx" --> 

<script type="text/javascript" src="inc/functions.js"></script>

<!--<link rel="stylesheet" type="text/css" media="all" href="css/calendar-win2k-1.css" title="win2k-1" />-->

<script type="text/javascript">
  var servertoday = new Date();
</script>
<script type="text/javascript" src="script/cal.js"></script>
<script type="text/javascript" src="lang/calendar-en.js"></script>
<script type="text/javascript" src="script/calendar-setup.js"></script>

<script type="text/javascript">
function goBack()
{
    //Diagnostics
    frmResult.action = "EventLog.aspx?S=ES";
    frmResult.submit();
}
</script>

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>myVRM Event Log Result Page</title>
</head>
<body>
    <center>
      <input type="hidden" id="helpPage" value="34">

        <form id="frmResult" runat="server">
        <H3>Event Log(s)</H3>
        <div>
            <asp:Label ID="errLabel" runat="server" Text="Label"></asp:Label>
        </div>
            <asp:PlaceHolder ID="resultHolder" runat="server"></asp:PlaceHolder>
            <asp:TextBox ID="txtPage" runat="server" BackColor="white" ForeColor="white" Width="0px" Height="0px" BorderStyle="none"></asp:TextBox>
            <asp:Table ID="tblPage1" runat="server" HorizontalAlign="center" Width="100%" CellPadding="5">
                <asp:TableRow HorizontalAlign="left">
                    <asp:TableCell HorizontalAlign="center">
                        <asp:DataGrid ID="dgResult" runat="server" Width="100%" AutoGenerateColumns="false" CellSpacing="1">
                            <HeaderStyle CssClass="tableHeader" Height="30" />
                            <%--Window Dressing--%>
                            <Columns>
                                <asp:BoundColumn DataField="moduleName" ItemStyle-Height="20" HeaderText="Module Name" ItemStyle-HorizontalAlign="center" ItemStyle-CssClass="tableBody">
                                <HeaderStyle CssClass="tableHeader" HorizontalAlign="Center" /></asp:BoundColumn>
                                <asp:BoundColumn ItemStyle-HorizontalAlign="center" DataField="moduleErrorCode" HeaderText="Error Code" ItemStyle-CssClass="tableBody"><HeaderStyle CssClass="tableHeader" HorizontalAlign="Center" /></asp:BoundColumn>
                                <asp:BoundColumn ItemStyle-HorizontalAlign="center" DataField="severity" HeaderText="Severity" ItemStyle-CssClass="tableBody"><HeaderStyle CssClass="tableHeader" HorizontalAlign="Center"/></asp:BoundColumn>
                                <asp:BoundColumn ItemStyle-HorizontalAlign="Left" DataField="message" HeaderText="Message" ItemStyle-CssClass="tableBody"><HeaderStyle CssClass="tableHeader" HorizontalAlign="Center" /></asp:BoundColumn>
                                <asp:BoundColumn ItemStyle-HorizontalAlign="center" DataField="timestamp" HeaderText="Time" ItemStyle-CssClass="tableBody"><HeaderStyle CssClass="tableHeader" HorizontalAlign="Center" /></asp:BoundColumn>
                            </Columns>
                        </asp:DataGrid>
                        <asp:Label ID="lblNoRecords" runat="server" Text="No records found" CssClass="lblError" Visible="false"></asp:Label>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Table ID="tblSearchAgain" runat=server Width="100%">
                            <asp:TableRow>
                                <asp:TableCell Height="20">&nbsp;
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow HorizontalAlign="center">
                                <asp:TableCell>
                                    <asp:Table ID="tblPage" Visible="false" runat="server">
                                        <asp:TableRow runat="server">
                                         </asp:TableRow>
                                    </asp:Table>
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow HorizontalAlign=right>
                                <asp:TableCell HorizontalAlign="right" Width="100%">
                                    <asp:Button ID="searchAgain" runat="server" Text="Search Again" OnClick="searchAgain_Click" CssClass="altLongBlueButtonFormat"/>
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>

                    </form>
    </center>
</body>
</html>
<%--code added for Soft Edge button--%>
<script type="text/javascript" src="inc/softedge.js"></script>
<!-- #INCLUDE FILE="inc/mainbottomNET.aspx" --> 
