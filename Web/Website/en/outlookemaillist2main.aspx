﻿<%--ZD 100147 start--%>
<%--/* Copyright (C) 2013 myVRM - All Rights Reserved	
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.		
*
* You should have received a copy of the myVRM license with	
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/--%><%--ZD 100147 End--%>
<%@ Page Language="C#" AutoEventWireup="true"  Inherits="en_outlookemaillist2main" %><%--ZD 100170--%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>myVRM</title>
   <meta name="Description" content="myVRM (Videoconferencing Resource Management) is a revolutionary Web-based software application that manages any video conferencing environment." />
  <meta name="Keywords" content="VRM, myVRM, Videoconferencing Resource Management, video conferencing, video bridges, video endpoints" />
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
  <script type="text/javascript" src="script/wincheck.js"></script> <%--FB 3055-URL--%>
   <script type="text/javascript"> // FB 2815
       var path = '<%=Session["OrgCSSPath"]%>';
       path = path.split('.')[0] + '<%=Session["ThemeType"]%>' + ".css";
       document.write('<link rel="stylesheet" title="Expedite base styles" type="text/css" href="' + path + '?' + new Date().getTime() + '">');
   </script>
<script type="text/javascript" language="JavaScript" src="sorttable.js"></script> 
<script type="text/javascript" language="javascript1.1" src="extract.js"></script>
 <script type="text/javascript" src="script/errorList.js"></script>
   
  <script type="text/javascript"  language="javascript">
  <!--
	
	function errorHandler( e, f, l ){
		alert("An error has ocurred in the JavaScript on this page.\nFile: " + f + "\nLine: " + l + "\nError:" + e);
		return true;
	}
	
  //-->
  </script>
<script language="JavaScript" type="text/javascript">

<!--

var tabs=new Array("0","A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z")

var isCreate, isFu, isRm, isFuCreate, isRmCreate;

if (queryField("frm") == "party2") {
	if (opener.document.frmSettings2) {
		isCreate = ( (opener.document.frmSettings2.ConfID.value == "new") ? true : false ) ;
		isFu = ( (opener.document.frmSettings2.CreateBy.value == "1") ? true : false ) ;
		isRm = ( ((parent.opener.document.frmSettings2.CreateBy.value == "2") || (parent.opener.document.frmSettings2.CreateBy.value == "7")) ? true : false ) ;

		isFuCreate = (isFu && isCreate)
		isRmCreate = (isRm && isCreate)
	} else {
		alert(EN_88);
		window.close();
	}
}

switch (queryField("frm")) {
	case "party2":	// all settings2 pages
		if (opener.ifrmPartylist)
			opener.ifrmPartylist.bfrRefresh();
		break;
	case "party2NET":
	    	if (opener.ifrmPartylist)
			opener.ifrmPartylist.bfrRefresh();
		break;
	case "group":	// managegroup2
		if (opener.ifrmPartylist)
			opener.ifrmPartylist.bfrRefresh();
		break;
	default:
}


function selname(sel)
{
	//ifrmList.window.location.href="outlookemaillist2.aspx?wintype=ifr&frm=" + "<%=Request.QueryString["frm"]%>" + "&sn=" + sel + "&st=" + document.frmOutlookemaillist2main.SelectType.selectedIndex;
	ifrmList.window.location.href="outlookemaillist2.aspx?wintype=ifr&frm=" + "<%=Request.QueryString["frm"]%>" + "&sn=" + sel + "&st=" + document.frmOutlookemaillist2main.SelectType.selectedIndex + "&srch=" + "<%=Request.QueryString["srch"]%>" + "&alSel=Y";
	
	for (i in tabs) {
		document.getElementById(tabs[i]).style.backgroundColor = ""; 
	}

	document.getElementById(sel).style.backgroundColor = "#FF6699"; 
	
	document.frmOutlookemaillist2main.hdnsel.value = sel;
	
}


function seltype()
{
	ifrmList.window.location.href="outlookemaillist2.aspx?wintype=ifr&frm=" + "<%=Request.QueryString["frm"]%>" + "&sn="+document.frmOutlookemaillist2main.hdnsel.value+"&st=" + document.frmOutlookemaillist2main.SelectType.selectedIndex;  
	//ifrmList.window.location.href="outlookemaillist2.aspx?wintype=ifr&frm=" + "<%=Request.QueryString["frm"]%>" + "&sn=0&st=" + document.frmOutlookemaillist2main.SelectType.selectedIndex;
	for (i in tabs) {
		document.getElementById(tabs[i]).style.backgroundColor = ""; 
		
	sel = document.frmOutlookemaillist2main.hdnsel.value;
		
	document.getElementById(sel).style.backgroundColor = "#FF6699"; 
		
	}
}
function nameimg(prename, sel)
{
   	for (i in tabs) {
		document.getElementById(prename + tabs[i]).style.backgroundColor = ""; 
	}
	if(queryField("srch") == "y")
	    document.getElementById(prename + sel.toUpperCase()).style.backgroundColor = "";	
	else	
	    document.getElementById(prename + sel.toUpperCase()).style.backgroundColor = "#FF6699"; 
}	
function Sort(id)
{
	ifrmList.sortlist(id);
}

function Reset ()
{
	ifrmList.Reset();
}
//-->
</script>
</head>
    
<body bottommargin="0" leftmargin="5" rightmargin="3" topmargin="8" marginheight="0" marginwidth="0"> 


	<form name="frmOutlookemaillist2main" method="POST" action="dispatcher/userdispatcher.asp">

	<input type="hidden" name="outlookEmails" value="" />
	<input type="hidden" name="emailListPage" value="<% =Convert.ToString( Convert.ToInt32(Request.QueryString["page"])+1 ) %>" />
    <input type="hidden" name="mt" id="mt" value="" />
    <input type="hidden" name="hdnsel" id="hdnsel" runat="server"/>
    

<%
if (Request.QueryString["frm"]!=  "")
	Response.Write ("<input type='hidden' name='frm' value='" + Request.QueryString["frm"] + "'>");
else
    Response.Write("<input type='hidden' name='frm' value='" + Request.QueryString["frm"] + "'>");


if (Request.QueryString["srch"] == "y" )
	Response.Write ("<input type='hidden' name='fromSearch' value='yes'>");
	Response.Write ("<input type='hidden' name='FirstName' value='" + Request.QueryString["fname"] + "'>");
	Response.Write ("<input type='hidden' name='LastName' value='" + Request.QueryString["lname"] + "'>");

%>

  <center>
  <table width="100%" border="0" cellpadding="2" cellspacing="4">
    <tr>
      <td align="center">
        <font size="4" color="blue"><b>MS Outlook Address Book</b></font>
      </td>
    </tr>
    <tr>
      <!--Response.Write(" hi" + Request.QueryString);
//if (Request.QueryString ["srch"] != "y" ){ -->
      <td align="center">
		<select name="SelectType" size="1" class="altSmallSelectFormat" onchange="seltype();">
          <option value='1'>Last Name</option>
          <option value='2'>First Name</option>
          <option value='3'>Email</option>
        </select>
        <font name="Verdana" size="2" class="blackblodtext"><b>Start With:</b></font>
        <a onclick="JavaScript: selname('A')"><span class="tabtext" id="A">A</span></a>
        <a onclick="JavaScript: selname('B')"><span class="tabtext" id="B">B</span></a>
        <a onclick="JavaScript: selname('C')"><span class="tabtext" id="C">C</span></a>
        <a onclick="JavaScript: selname('D')"><span class="tabtext" id="D">D</span></a>
        <a onclick="JavaScript: selname('E')"><span class="tabtext" id="E">E</span></a>
        <a onclick="JavaScript: selname('F')"><span class="tabtext" id="F">F</span></a>
        <a onclick="JavaScript: selname('G')"><span class="tabtext" id="G">G</span></a>
        <a onclick="JavaScript: selname('H')"><span class="tabtext" id="H">H</span></a>
        <a onclick="JavaScript: selname('I')"><span class="tabtext" id="I">I</span></a>
        <a onclick="JavaScript: selname('J')"><span class="tabtext" id="J">J</span></a>
        <a onclick="JavaScript: selname('K')"><span class="tabtext" id="K">K</span></a>
        <a onclick="JavaScript: selname('L')"><span class="tabtext" id="L">L</span></a>
        <a onclick="JavaScript: selname('M')"><span class="tabtext" id="M">M</span></a>
        <a onclick="JavaScript: selname('N')"><span class="tabtext" id="N">N</span></a>
        <a onclick="JavaScript: selname('O')"><span class="tabtext" id="O">O</span></a>
        <a onclick="JavaScript: selname('P')"><span class="tabtext" id="P">P</span></a>
        <a onclick="JavaScript: selname('Q')"><span class="tabtext" id="Q">Q</span></a>
        <a onclick="JavaScript: selname('R')"><span class="tabtext" id="R">R</span></a>
        <a onclick="JavaScript: selname('S')"><span class="tabtext" id="S">S</span></a>
        <a onclick="JavaScript: selname('T')"><span class="tabtext" id="T">T</span></a>
        <a onclick="JavaScript: selname('U')"><span class="tabtext" id="U">U</span></a>
        <a onclick="JavaScript: selname('V')"><span class="tabtext" id="V">V</span></a>
        <a onclick="JavaScript: selname('W')"><span class="tabtext" id="W">W</span></a>
        <a onclick="JavaScript: selname('X')"><span class="tabtext" id="X">X</span></a>
        <a onclick="JavaScript: selname('Y')"><span class="tabtext" id="Y">Y</span></a>
        <a onclick="JavaScript: selname('Z')"><span class="tabtext" id="Z">Z</span></a>
        <a onclick="JavaScript: selname('0')"><span class="tabtext" id="0">All</span></a>
      </td>
     </tr>
  </table>

    <table width='95%' border='1' cellpadding='0' cellspacing='0'>
      <tr>
        <td align="<%=titlealign%>">
          

<script language="JavaScript">
<!--  

_d = document;
var mt = "", mt1 = "", mt2 = "";
	
//mt += "<table width='97%' border='0' cellpadding='3' cellspacing='1'>";
if (queryField("frm") == "party2") {
	mt += "  <thead><tr>";
	mt += "    <td align='center' width='150' height='20'>Name</td>";
	mt += "    <td align='center' width='200'>Email</td>";
	mt += "    <td align='center' width='40'><SPAN>Selected</SPAN></td>";
	
	mt1 += "    <td align='center' width='40'><SPAN>";
	mt1 += "      <a title='External Attendees have their own individual physical endpoints'>External Attendees</a>";
	mt1 += "    </SPAN></td>";
	mt1 += "    <td align='center' width='40'><SPAN>";
	mt1 += "      <a title='Room Attendees are room-based participants.Each room is associated with only one physical endpoint'>Room Attendees</a>";
	mt1 += "    </SPAN></td>";
	
	mt2 += "    <td align='center' width='40'><SPAN>" + (isFuCreate ? "External Attendees" : "Room Attendees") + "</SPAN></td>";

	mt += ( (isFuCreate || isRm) ?  mt2 : mt1);
	
	mt += "    <td align='center' width='15'><SPAN>CC</SPAN></td>";
	mt += "    <td align='center' width='25'><SPAN>NOTIFY</SPAN></td>";
	//mt += "    <td align='center' width='30'><SPAN>Audio</SPAN></td>";
	//mt += "    <td align='center' width='30'><SPAN>Video</SPAN></td>";
	mt += "  </tr></thead>";
} else if (queryField("frm") == "party2NET") {
    mt += "  <thead><tr>";
	mt += "    <td align='center' width='35%' height='20'><SPAN>Name</SPAN></td>";
	mt += "    <td align='center' width='55%'><SPAN>Email</SPAN></td>";
	mt += "    <td align='center' width='10%'><SPAN>Selected</SPAN></td>";
	mt += "  </tr></thead>";
} else {
	mt += "  <thead><tr>";
	mt += "    <td align='center' width='35%' height='20'><SPAN>Name</SPAN></td>";
	mt += "    <td align='center' width='55%'><SPAN>Email</SPAN></td>";
	mt += "    <td align='center' width='10%'><SPAN>Selected</SPAN></td>";
	mt += "  </tr></thead>";
}
//mt += "</table>";
document.frmOutlookemaillist2main.mt.value = mt;

//_d.write(mt)

window.resizeTo("850","500");
//-->
</script>

        </td>
      </tr>
      <tr>
        <td>
          <iframe src="outlookemaillist2.aspx?wintype=ifr&frm=<%=Request.QueryString["frm"]%>&sn=0" name="ifrmList" id="ifrmList" width="100%" height="310" align="left" valign="top" >
            <p>go to <a href="outlookemaillist2.aspx">Email List</a></p>
          </iframe>
        </td>
      </tr>
    </table>

    <table cellpadding="4" cellspacing="6">
      <tr>
        <td height="3">
        </td>
      </tr>
      <tr>
        <td>
          <input type="button" name="Outlookemaillist2Submit" value="Close" class="altShortBlueButtonFormat" onclick="Javascript: window.close()" />
        </td>
        <td>
          <input type="button" name="Outlookemaillist2Submit" value="Search" class="altShortBlueButtonFormat" onclick="javascript:window.location.href='outlookemailsearch.aspx?frm=<%=Request.QueryString["frm"] %>'" />
        </td>
        <td>
          <input type="button" name="Outlookemaillist2Submit" value="Submit" class="altShortBlueButtonFormat" onclick="JavaScript: window.close();" />
        </td>
      </tr>
    </table>
  </center>
    <input type="hidden" name="cmd" value="PreRetrieveOutlookUsers" />
  <input type="hidden" name="xmlstr" id="xmlstr" runat="server" />
</form>

	 
</body>

</html>
<script type="text/javascript">

if(document.frmOutlookemaillist2main.hdnsel.value == "")
 document.frmOutlookemaillist2main.hdnsel.value = 0;
 
nameimg ("", document.frmOutlookemaillist2main.hdnsel.value)
</script>

