<%--ZD 100147 start--%>
<%--/* Copyright (C) 2013 myVRM - All Rights Reserved	
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.		
*
* You should have received a copy of the myVRM license with	
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/--%><%--ZD 100147 End--%>				
<%@ Page Language="C#" AutoEventWireup="true" Inherits="en_recurNET" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ Register Assembly="MetaBuilders.WebControls.ComboBox" Namespace="MetaBuilders.WebControls"
    TagPrefix="mbcbb" %>

<script type="text/javascript" src="script/wincheck.js"></script> <%--FB 3055-URL--%>
<script type="text/javascript" language="JavaScript" src="inc/functions.js"></script>
<script type="text/javascript" language="JavaScript" src="script/errorList.js"></script>
<script type="text/javascript" language="javascript1.1" src="extract.js"></script>

<script type="text/javascript">
  var servertoday = new Date();
  var dFormat;
    dFormat = "<%=format %>";
    
var maxRecurrence = ' 24 instances for Monthly and  104 for weekly or equivalent';
  
var servertoday = new Date(parseInt("<%=DateTime.Today.Year%>", 10), parseInt("<%=DateTime.Today.Month%>", 10)-1, parseInt("<%=DateTime.Today.Day%>", 10),
        parseInt("<%=DateTime.Today.Hour%>", 10), parseInt("<%=DateTime.Today.Minute%>", 10), parseInt("<%=DateTime.Today.Second%>", 10)); 
 
 var  maxDuration = 24;
 if('<%=Application["MaxConferenceDurationInHours"] %>' != '')
    maxDuration = parseInt('<%=Application["MaxConferenceDurationInHours"] %>',10);  

</script>

<script type="text/javascript" src="script/cal-flat.js"></script>
<script type="text/javascript" src="script/calview.js"></script>
<script type="text/javascript" src="lang/calendar-en.js"></script>
<script type="text/javascript" src="script/calendar-setup.js"></script>
<script type="text/javascript" src="script/settings2.js"></script>
<script type="text/javascript" src="script/calendar-flat-setup.js"></script>

<script type="text/javascript">

var openerfrm = eval('opener.document.' + queryField("frm") );
function CheckDate(obj)
{    
    var endDate;//FB 2246
    if ('<%=format%>'=='dd/MM/yyyy')
        endDate = GetDefaultDate(obj.value,'dd/MM/yyyy');
    else
        endDate = obj.value;
        
    if (Date.parse(endDate) < Date.parse(new Date()))
        alert("Invalid Date");
}

function fnShow()
{
    if(rpstr != "")
    {    
        var rpstrSplit = rpstr.split("&");
        
        if(document.getElementById("daySel" + rpstrSplit[0]))
            document.getElementById("daySel" + rpstrSplit[0]).checked = true;  
        
        if(document.getElementById("selRecur" + rpstrSplit[1]))
            document.getElementById("selRecur" + rpstrSplit[1]).checked = true;            
        
        if(document.getElementById("selDay" + rpstrSplit[1]))
        {
            if(rpstrSplit[2] != "-1")
            document.getElementById("selDay" + rpstrSplit[1]).value = rpstrSplit[2];
        }
    }
}

function fnDefault()
{
    var recPattern = 5;
    for(var r =1; r<= recPattern;r++)
    {
        if(document.getElementById("selDay"+ r))
        {
             var opt = document.getElementById("selDay" + r);
	            if(opt != null)
	                opt.value = 1;
        }
    }
}

function initial()
{
//FB 2634
    if('<%=Session["EnableBufferZone"] %>' == "1")
    {
        document.getElementById("SetupRow").style.display = '';
        document.getElementById("TearDownRow").style.display = '';
    }
    else
    {
        document.getElementById("SetupRow").style.display = 'None';
        document.getElementById("TearDownRow").style.display = 'None';
    }

	if (openerfrm.RecurSpec.value=="") 
	{
		timeMin = openerfrm.hdnStartTime.value.split(":")[1].split(" ")[0] ; 
		
		sdt = GetDefaultDate(openerfrm.confStartDate.value,dFormat);
		sdt = new Date(sdt + " " + openerfrm.hdnStartTime.value);
		
	    edt = GetDefaultDate(openerfrm.confEndDate.value,dFormat);
	    edt = new Date(edt + " " + openerfrm.hdnEndTime.value) ;
	
		var oDiff = get_time_difference(sdt,edt);
		var timeDur = oDiff.duration;
		
        if (oDiff.duration > (maxDuration * 60))
            alert(EN_314);
         
         /* *** code changed for buffer zone *** --Start */
		if('<%=Session["timeFormat"].ToString()%>' == '0' || '<%=Session["timeFormat"].ToString()%>' == '2')
		    tmpstr = openerfrm.lstConferenceTZ.options[openerfrm.lstConferenceTZ.selectedIndex].value + "&" + openerfrm.hdnStartTime.value.split(":")[0] + "&" + timeMin + "&-1&" + timeDur + "#"
        else
		    tmpstr = openerfrm.lstConferenceTZ.options[openerfrm.lstConferenceTZ.selectedIndex].value + "&" + openerfrm.confStartTime_Text.value.split(":")[0] + "&" + timeMin + "&" + openerfrm.confStartTime_Text.value.split(" ")[1] + "&" + timeDur + "#"

		tmpstr += "1&1&-1&-1&-1&-1&-1&-1&-1&-1&-1&-1&-1&-1&-1&-1&-1#"
		tmpstr += openerfrm.confStartDate.value + "&1&-1&-1";

 		document.frmRecur.RecurValue.value = tmpstr;
		document.frmRecur.RecurPattern.value = "1&1&-1&-1&-1&-1&-1&-1&-1&-1&-1&-1&-1&-1&-1&-1&-1";
		
		document.frmRecur.RecurSubmit1.disabled = true;
		document.frmRecur.RecurSubmit1.className = "btndisable"; // FB 2664
	    fnDefault();	   
	}
     
	rpstr = AnalyseRecurStr(document.frmRecur.RecurValue.value);	
	
	
	
	if(atint[1] < 10 && (atint[1] != "0" || atint[1] != "00"))
	    atint[1] = "0" + atint[1];
	
	if(atint[2] < 10 && (atint[2] != "0" || atint[2] != "00"))
	    atint[2] = "0" + atint[2];
	    
	if('<%=Session["timeFormat"].ToString()%>' == '0' || '<%=Session["timeFormat"].ToString()%>' == '2')
	    startstr = (atint[1] == "12" && atint[3] == "AM"? "00" : atint[1]) + ":" + (atint[2]=="0" ? "00" : atint[2]);
	else
	    startstr = atint[1] + ":" + (atint[2]=="0" ? "00" : atint[2]) + " " + atint[3];
	
    if(document.getElementById("confStartTime_Text"))
	    document.getElementById("confStartTime_Text").value = startstr;
	    
	if(document.getElementById("confEndTime_Text"))
	    document.getElementById("confEndTime_Text").value = openerfrm.confEndTime_Text.value;
    //FB 2558
    ChangeTimeFormat("O");
    if(document.getElementById("confStartTime_Text"))
	    document.getElementById("confStartTime_Text").value = document.getElementById("hdnStartTime").value;
    
    //FB 2634
    var actualDur = atint[4];
	var setup = "<%=setup%>";
    var teardown = "<%=teardown%>";
    document.getElementById("SetupDuration").value = setup;
    document.getElementById("TearDownDuration").value = teardown;
    
    if (openerfrm.RecurSpec.value != "") 
    {
        //FB 2558
        ChangeTimeFormat("D");
        actualDur = parseInt(actualDur,10) - (parseInt(setup,10) + parseInt(teardown,10));
        
        var cstartdate = new Date(rrint[0] + " " + document.getElementById("hdnStartTime").value);
	    var sttime = setCDuration(cstartdate, setup)
	    var sttime1 = getCTime(sttime);        
     
	    document.getElementById("confStartTime_Text").value = sttime1;
	    
	    //FB 2558
        ChangeTimeFormat("O");
	    document.getElementById("confStartTime_Text").value = document.getElementById("hdnStartTime").value;
    }
    
	if (document.frmRecur.RecurDurationhr) 
	{
		set_select_field (document.frmRecur.RecurDurationhr, parseInt(actualDur/60, 10) , true);
		set_select_field (document.frmRecur.RecurDurationmi, actualDur%60, true);
	}
	
	if (document.frmRecur.RecurDurationhr) 
	{
		rdhr = parseInt(document.frmRecur.RecurDurationhr.value, 10);
		rdmi = parseInt(document.frmRecur.RecurDurationmi.value, 10);
		
		recurduration = rdhr * 60 + rdmi;
        if (recurduration > (maxDuration * 60))
            alert(EN_314);		
	}

	document.frmRecur.RecurPattern.value = rpstr;	
    document.frmRecur.Occurrence.value ="";
    document.frmRecur.EndDate.value ="";
   
	if (rpint[0] != 5) 
	{
		document.frmRecur.StartDate.value = rrint[0];
	
		switch (rrint[1]) 
		{
			case 1:
		        document.frmRecur.EndType.checked = true;
				break;
			case 2:
		        document.frmRecur.REndAfter.checked = true;
				document.frmRecur.Occurrence.value = rrint[2];
				break;
			case 3:
		        document.frmRecur.REndBy.checked = true;
				document.frmRecur.EndDate.value = rrint[3];
				break;
			default:
				alert(EN_36);
				break;
		}
    }

    if (rpint[0] != null)
    {
        var colorDay = document.frmRecur.rdlstDayColor;   
        if (colorDay)
        {
            for (var c = 0; c < colorDay.length; c++)
            {
                if (colorDay[c].defaultValue == rpint[0])
                   colorDay[c].checked = true;
           }
           
           if (colorDay.length == null)
               if (colorDay.defaultValue == rpint[0])
               colorDay.checked = true;
        }
    }
}
//FB 2558
function ChangeTimeFormat() {
    var args = ChangeTimeFormat.arguments;
    var stime = document.getElementById("confStartTime_Text").value;
    var etime = document.getElementById("confEndTime_Text").value;
    var hdnsTime = document.getElementById("hdnStartTime");
    var hdneTime = document.getElementById("hdnEndTime");

    if ('<%=Session["timeFormat"]%>' == "2") {
        if (args[0] == "D") {
            stime = stime.replace('Z', '')
            stime = stime.substring(0, 2) + ":" + stime.substring(2, 4);
            etime = etime.replace('Z', '')
            etime = etime.substring(0, 2) + ":" + etime.substring(2, 4);
        }
        else {
            if (stime.indexOf("Z") < 0)
                stime = stime.replace(':', '').replace(" AM","").replace(" PM","") + "Z";
            if (etime.indexOf("Z") < 0)
                etime = etime.replace(':', '').replace(" AM","").replace(" PM","") + "Z";
        }
    }

    hdnsTime.value = stime
    hdneTime.value = etime
}

</script>

<%--Submit Recurence --%>

<script type="text/javascript">

function SubmitRecurrence()
{
    if (Page_ClientValidate() == false) 
    { 
        return false; 
    }

    if (validateConfDuration())
    {
        if(summary())
            return true;
    }
    return false;        
}

function validateDurationHr()
{
    var obj = document.getElementById("RecurDurationhr");
    
    if (obj.value == "") 
         obj.value = "0";
         
    if (isNaN(obj.value)) 
    {
        alert(EN_314);
        return false;
    }

    var maxdur = '<%= Application["MaxConferenceDurationInHours"] %>';
    
    if(maxdur == "")
        maxdur = "24"; 
    
    if (obj.value < 0 || eval(obj.value) > eval(maxdur))
    {
        alert(EN_314);
        return false;
    }
    return true;
}

function validateDurationMi()
{
    var obj = document.getElementById("RecurDurationmi");
    if (obj.value == "") 
        obj.value = "0";
    if (isNaN(obj.value))
    {
        alert(EN_314);
        obj.focus();
        return false;
    }
    return true;
}

function validateConfDuration()
{
    var obj = document.getElementById("RecurDurationhr");
    var obj1 = document.getElementById("RecurDurationmi");
    
    if (obj.value == "")
         obj.value = "0";
         
    if (obj1.value == "") 
        obj1.value = "0";
         
    if (isNaN(obj.value)) 
    {
        alert(EN_314);
        return false;
    }
    if (isNaN(obj1.value)) 
    {
        alert(EN_314);
        return false;
    }
    
    var maxdur = '<%= Application["MaxConferenceDurationInHours"] %>';
    if(maxdur == "")
        maxdur = "24";

    var totDur = parseInt(obj.value) * 60;
    totDur = totDur + parseInt(obj1.value);
    
    if (totDur < 0 || totDur > eval(maxdur*60))
    {
        alert(EN_314);
        return false;
    }
  
    return true;
}

function setCDuration(setupTime, dura)
{
    var chTime = new Date(setupTime);
    var d = 0;
    var min = 0;
    var hh = 0, dur = 0;
    if(dura > 60)
    {   
        hh = dura / 60;
        min = dura % 60;                        
        if(min > 0)
            hh = Math.floor(hh) + 1;                        
     
        for(d = 1; d <= hh ; d++)
        {
            if(min > 0 && d == hh)
                dur = dura % 60;
            else
                dur = 60;
                
             chTime.setMinutes(chTime.getMinutes() + dur);   
        }
    }
    else
        chTime.setTime(chTime.getTime() + (dura * 60 * 1000));
        
   return chTime
}

function getCTime(changeTime)
{
    var hh = changeTime.getHours(); 
    var ap ="AM";
    if('<%=Session["timeFormat"]%>' == "1") 
    {
        if(hh >= 12)
        {
            hh = hh - 12;
            ap = "PM";
        }
            
        if(hh == 0)
            hh = 12
    }
             
    if (hh < 10)
        hh = "0" + hh;
    
    var mm = changeTime.getMinutes();
    if (mm < 10)
        mm = "0" + mm;

    //var ap = changeTime.format("tt"); //FB 2108
    var evntime = parseInt(hh,10);
    
    if(evntime < 12 && ap == "PM")
       evntime = evntime + 12;
       
    var tiFormat =  '<%=Session["timeFormat"]%>' ;

    if(tiFormat == '0')
    {
        if(ap == "AM")                        
            strTime = (hh == "12") ? "00:" + mm : hh + ":" + mm ;
        else
        {
            if(evntime == "24")
                strTime = (hh == "12") ? "00:" + mm : evntime + ":" + mm;
            else
                strTime =  evntime + ":" + mm;
        }   
    }
    else
        strTime = hh + ":" + mm + " " + ap;
        
   return strTime;
}

function summary()
{
    var rp = "";
    var recPattern = 8;
    
    var colorDay = document.frmRecur.rdlstDayColor;
    if (!colorDay)
    {
        alert("Special recurrence must contain at least one color day.");
        document.frmRecur.Occurrence.focus();
        return false;
    }
    
    for (var c = 0; c < colorDay.length; c++)
    {
        if (colorDay[c].checked)
           document.frmRecur.hdnColor.value = colorDay[c].defaultValue;
   }
   
   if (colorDay.length == null)
       if (colorDay.checked)
           document.frmRecur.hdnColor.value = colorDay.defaultValue;
       
    for(var r =1; r<= recPattern;r++)
    {
        if(document.getElementById("selRecur" + r))
            if(document.getElementById("selRecur" + r).checked)
                document.frmRecur.RecPattern.value = r;
    }
    
	rp  = document.frmRecur.hdnColor.value + "&" +  document.frmRecur.RecPattern.value + "&";
	
    if(document.getElementById("selDay"+ document.frmRecur.RecPattern.value))
    {
         var opt = document.getElementById("selDay" + document.frmRecur.RecPattern.value);
		    if(opt != null)
		        rp += opt.value + "&";
    }
    else
        rp += "-1&";

    rp += "-1&-1&-1&-1&-1&-1&-1&-1&-1&-1&-1&-1&-1&-1";
	document.frmRecur.RecurPattern.value = rp;
	
     if(document.frmRecur.REndAfter.checked)
    {
       if(document.frmRecur.Occurrence.value <= 1)
       {
           alert("A recurring conference must contain at least (2) instances. Please modify recurrence pattern before submitting conference.");
           document.frmRecur.Occurrence.focus();
           return false;   
       }
    }
    //FB 2634
	//var aryStart = document.getElementById("confStartTime_Text").value.split(" ");
	//FB 2558
	ChangeTimeFormat("D");
	var cstartdate = new Date(GetDefaultDate(document.getElementById("StartDate").value,'<%=format%>') + " "
            + document.getElementById("hdnStartTime").value);
                
    var dura = parseInt(document.getElementById("SetupDuration").value,10)    
	var sttime = setCDuration(cstartdate, -dura)
	var sttime1 = getCTime(sttime);
	var aryStart = sttime1.split(" ");
	
	sh = aryStart[0].split(":")[0];
	sm = aryStart[0].split(":")[1];
	ss = aryStart[1];
	tz = openerfrm.lstConferenceTZ.options[openerfrm.lstConferenceTZ.selectedIndex].value;
	
	if('<%=Session["timeFormat"].ToString()%>' == '0' || '<%=Session["timeFormat"].ToString()%>' == '2') //FB 2558
	{
	    if(sh != "")
	    { 
	        if(eval(sh) >= 12)
	            ss = "PM";
	        else
	            ss = "AM";
	    }
	}
	//FB 2634
	/*
	var aryStup = document.getElementById("confEndTime_Text").value.split(" ");
	endhr = aryStup[0].split(":")[0];
	endmi = aryStup[0].split(":")[1];
	endap = aryStup[1];

    if('<%=Session["timeFormat"].ToString()%>' == '0')
	{ 
	    startTime = sh + ":" + (sm =="0" ? "00" : sm);
	    endTime = endhr + ":" + (endmi =="0" ? "00" : endmi);
    }
	else
	{
	    startTime = sh + ":" + (sm =="0" ? "00" : sm) + " " + ss;
	    endTime = endhr + ":" + (endmi =="0" ? "00" : endmi) + " " + endap;
    }
   
    var insStDate = '';
    insStDate = document.frmRecur.StartDate.value;   
    startdate = insStDate + " " + startTime;
   
    var obj = document.getElementById("RecurDurationhr");
    var obj1 = document.getElementById("RecurDurationmi");
    
    var totDur = parseInt(obj.value) * 60;
    totDur = totDur + parseInt(obj1.value);
    
    if(totDur > 1440)
    {
        alert(EN_314);
        return false;
    }
    
    var confEndDt = '';
    var sysdate = dateAddition(startdate,"m",totDur);
    var dateP = sysdate.getDate();
	
	var monthN = sysdate.getMonth() + 1;
	var yearN = sysdate.getFullYear();
	var hourN = sysdate.getHours();
	var minN = sysdate.getMinutes();
	var secN = sysdate.getSeconds();
	var timset = 'AM';
	
	if('<%=Session["timeFormat"].ToString()%>' == '0')
	    timset = '';
	else
	{
	    if(hourN == 12)
	        timset = 'PM';
	    else if( hourN > 12)
	    {
	         timset = 'PM';
	         hourN = hourN - 12;
	    }
	    else if(hourN == 0)
	    {
	        timset = "AM";
	        hourN = 12;
	    }
	 }
	
	var confDtAlone = monthN + "/" + dateP + "/" + yearN;	
	confEndDt = monthN + "/" + dateP + "/" + yearN + " "+ hourN + ":" + minN + ":" + secN +" "+ timset;
	//FB 2634
    endDate = insStDate + " " + endTime;
    if(Date.parse(endDate) < Date.parse(startdate))
        endDate = confDtAlone + " " + endTime;
    
    if('<%=Application["Client"].ToString().ToUpper()%>' !="MOJ")
    {
        if(Date.parse(endDate) < Date.parse(startdate))
        {
            alert(EN_316);
            return false;
        }
    }
*/
	if (document.frmRecur.RecurDurationhr)
		dr = parseInt(document.frmRecur.RecurDurationhr.value, 10) * 60 + parseInt(document.frmRecur.RecurDurationmi.value, 10);
	
	//FB 2634
	if (dr < 15) 
    {
	    alert(EN_31);
	    return (false);		
	}
	
    dr = dr + parseInt(document.getElementById("SetupDuration").value,10) + parseInt(document.getElementById("TearDownDuration").value,10);
	
	if (document.frmRecur.RecurEndhr) 
	{
		rehr = document.frmRecur.RecurEndhr.options[document.frmRecur.RecurEndhr.selectedIndex].value;
		remi = document.frmRecur.RecurEndmi.options[document.frmRecur.RecurEndmi.selectedIndex].value;
		reap = document.frmRecur.RecurEndap.options[document.frmRecur.RecurEndap.selectedIndex].value;
		
		dr = calDur(sh, sm, ss, rehr, remi, reap, 0);
	}
	
	if ( dr < 15 ) 
	{
		alert(EN_31);
		
		if (document.frmRecur.RecurDurationhr)
			document.frmRecur.RecurDurationhr.focus();
		if (document.frmRecur.RecurEndhr)
			document.frmRecur.RecurEndhr.focus();
			
		return (false);		
	}
	
	rv = tz + "&" + sh + "&" + sm + "&" + ss + "&" + dr + "#";
	
	recurrange = document.frmRecur.StartDate.value + "&" + (document.frmRecur.EndType.checked ? "1&-1&-1" : ( document.frmRecur.REndAfter.checked ? ("2&" + document.frmRecur.Occurrence.value + "&-1") : (document.frmRecur.REndBy.checked ? ("3&-1&" + document.frmRecur.EndDate.value) : "-1&-1&-1") ));
	rv += document.frmRecur.RecurPattern.value + "#" + recurrange;
	if (frmRecur_Validator(document.frmRecur.RecurPattern.value, recurrange)) 
	{
	    openerfrm.RecurSpec.value = rv;
	    var recurstr;//FB 2558
		recurstr = specialRecur_discription(rv, 0, openerfrm.lstConferenceTZ.options[openerfrm.lstConferenceTZ.selectedIndex].text,		
		document.frmRecur.StartDate.value, '<%=Session["timeFormat"].ToString()%>', '<%=Session["timezoneDisplay"].ToString()%>', maxRecurrence);
		if ('<%=Session["timeFormat"].ToString()%>' == '2')
		    openerfrm.RecurText.value = recurstr.replace(" AM", "Z").replace(" PM", "Z").replace(":", "");
		else
		    openerfrm.RecurText.value = recurstr
		
		openerfrm.hdnOldTimezone.value = openerfrm.lstConferenceTZ.options[openerfrm.lstConferenceTZ.selectedIndex].text;//FB 2699
		openerfrm.RecurringText.value = openerfrm.RecurText.value	//FB 2699	
		openerfrm.hdnSpecRec.value = "1";
		//BtnCheckAvailDisplay ();
		openerfrm.SetupDuration.value = document.getElementById("SetupDuration").value;
		openerfrm.TearDownDuration.value = document.getElementById("TearDownDuration").value;
		
		window.close();
	}
}

function frmRecur_Validator(rp, rr)
{
	if (parseInt(document.frmRecur.Occurrence.value) > parseInt('<%=Application["confRecurrence"]%>'))
	{
		alert("Maximum limit of " + '<%=Application["confRecurrence"]%>' + " instances/occurrences in the recurring series.");
		return false;
	}
	
	if (chkRange(rr))
		return true;
	else
		return false;
}

var british = false;

function chkRange(rr)
{
	rrary = rr.split("&");
    
    if('<%=Session["FormatDateType"]%>' == 'dd/MM/yyyy')
        british = true;
    
    var dDate = GetDefaultDate(rrary[0],dFormat);
     
	if ( (!isValidDate(rrary[0])) || ( caldatecompare(dDate, servertoday) == -1 ) ) {		// check start time is reasonable future time
		alert(EN_74);
		document.frmRecur.StartDate.focus();
		return (false);
	}

	switch (rrary[1]) {
		case "1":
			if ( (rrary[2]!= "-1") && (rrary[3]!= "-1") ) {
				alert(EN_38);
				return false;
			} else 
				return true;
			break;
		case "2":
			if (rrary[3]!= "-1") {
				alert(EN_38);
				return false;
			} else {
				if ( isPositiveInt(rrary[2], "times of occurrences")==1 )
					return true;
				else {
					document.frmRecur.Occurrence.focus();
					return false;
				}
			}
			break;

case "3":

    if (rrary[2] != "-1") {

        alert(EN_38);

        return false;

    } else {
        if ((!isValidDate(rrary[3])) || (caldatecompare(GetDefaultDate(rrary[3], dFormat), servertoday) == -1)) {

            alert(EN_108);

            document.frmRecur.EndDate.focus();

            return false;

        } else {

            var fstdate = rrary[0];  //FB 2366
            var snddate = rrary[3];
            if (!british) {
                fstdate = GetDefaultDate(rrary[0], "dd/MM/yyyy");
                snddate = GetDefaultDate(rrary[3], "dd/MM/yyyy");
            }

            if (!dateIsBefore(fstdate, snddate)) {

                alert(EN_109);

                document.frmRecur.EndDate.focus();

                return false;

            } else

                return true;

        }

    }

    return false;

    break;
		default:
			alert(EN_38);
			return false;
			break;
	}	
}

function BtnCheckAvailDisplay ()
{
	if (opener.ifrmLocation != null) {
		
		if ( (e = opener.document.getElementById("btnCheckAvailDIV")) != null ) {
			e.style.display = (openerfrm.RecurSpec.value=="") ? "" : "none";
		}
	}
}

function removerecur()
{
	openerfrm.RecurSpec.value=""; 
	openerfrm.RecurringText.value = "";
	openerfrm.hdnSpecRec.value = "";
	window.close();
}
</script>

<%--Time Calculations--%>
<script type="text/javascript">

function formatTime(timeText, regText)
{
   if('<%=Session["timeFormat"]%>' == '1')
   {    
        if (document.getElementById(timeText).value.search(/^[0|1][0-9]:[0-5][0-9] ?[a|A|p|P][M|m]/) != -1)
        {
            var a_p = "";
            //FB 2614
            //var t = new Date();
            //var d = new Date(t.getDay() + "/" + t.getMonth() + "/" + t.getYear() + " " + document.getElementById(timeText).value);
            var tText = document.getElementById(timeText);
            var d = new Date(GetDefaultDate(document.getElementById("StartDate").value,'<%=format%>') + " " + tText.value);
           
            var curr_hour = d.getHours();
            if (curr_hour < 12) a_p = "AM"; else a_p = "PM";

            if (curr_hour == 0) curr_hour = 12;
            if (curr_hour > 12) curr_hour = curr_hour - 12;
            
            curr_hour = curr_hour + "";             
            if (curr_hour.length == 1)
               curr_hour = "0" + curr_hour;
                     
            var curr_min = d.getMinutes();
            curr_min = curr_min + "";
            if (curr_min.length == 1)
               curr_min = "0" + curr_min;

            document.getElementById(timeText).value = curr_hour + ":" + curr_min + " " + a_p;            
            document.getElementById(regText).style.display = "None"; 
            return true;            
       }
       else
       {    
            document.getElementById(regText).style.display = ""; 
            return false;
       }
   }
    return true;
}
//FB 2634
function EndDateValidation() {

    var sDate = Date.parse(GetDefaultDate(document.getElementById("StartDate").value,'<%=format%>') + " " + document.getElementById("confStartTime_Text").value);
    var eDate = Date.parse(GetDefaultDate(document.getElementById("StartDate").value,'<%=format%>') + " " + document.getElementById("confEndTime_Text").value);
    
    if ( (sDate >= eDate) && (document.getElementById("confStartTime_Text").value.search(/^[0|1][0-9]:[0-5][0-9] ?[a|A|p|P][M|m]/) != -1)
    && (document.getElementById("confEndTime_Text").value.search(/^[0|1][0-9]:[0-5][0-9] ?[a|A|p|P][M|m]/) != -1))
    {
        if (sDate == eDate)
        {
             if (sDate > eDate)
                alert("End Time will be changed because it should be greater than Start Time.");
            else if (sDate == eDate)
                alert("End Time will be changed because it should be greater than Start Time.");            
        }
            
         ChangeEndDate();
      }
}

function ChangeEndDate() {
    ChangeTimeFormat("D"); //FB 2588
    var args = ChangeEndDate.arguments;
    var startDateTime = new Date(GetDefaultDate(document.getElementById("StartDate").value, '<%=format%>') + " " + document.getElementById("hdnStartTime").value);
    var endDateTime = Date.parse(GetDefaultDate(document.getElementById("StartDate").value, '<%=format%>') + " " + document.getElementById("hdnEndTime").value);
    var skipcheck = 0;
    
    if(args[0] == "ET")
    {
        if(startDateTime >= endDateTime)
            skipcheck = 0;
        else
            skipcheck = 1;
    }
    
    if('<%=Session["DefaultConfDuration"]%>' != null && skipcheck == 0)
    {
        var ConfDuration = '<%=Session["DefaultConfDuration"]%>';
        var ConfHours = parseInt(ConfDuration) / 60; 
        var ConfMinutes = parseInt(ConfDuration) % 60;    
        ConfMinutes = ConfMinutes + startDateTime.getMinutes();
        if(ConfMinutes > 59)
        {
           ConfMinutes = ConfMinutes - 60;
           ConfHours = ConfHours + 1;
        }
        startDateTime.setHours(startDateTime.getHours() + ConfHours);
        startDateTime.setMinutes(ConfMinutes);
    }
    else
        startDateTime = new Date(endDateTime);
        
    var hh = startDateTime.getHours();
    var ap = "AM";
    
    if('<%=Session["timeFormat"]%>' == "1")
    {
        if(hh >= 12)
        {
            hh = hh - 12;
            ap = "PM";
        }
           
        if(hh == 0)
            hh = 12
    }
            
    if (hh < 10)
        hh = "0" + hh;
    var mm = startDateTime.getMinutes()
    if (mm < 10)
        mm = "0" + mm;
     
    var evntime = parseInt(hh,10);
    
     if(evntime < 12 && ap == "PM")
        evntime = evntime + 12;

    if ('<%=Session["timeFormat"]%>' == "0" || '<%=Session["timeFormat"]%>' == "2") //FB 2588
    {
        if(ap == "AM")
        {
            if(hh == "12")
                document.getElementById("confEndTime_Text").value = "00:" + mm ;
            else
                document.getElementById("confEndTime_Text").value = hh + ":" + mm ;
        }
        else
        {
            if(evntime == "24")
                document.getElementById("confEndTime_Text").value = "12:" + mm ;
            else
                document.getElementById("confEndTime_Text").value = evntime + ":" + mm ;
        }
    }
    else
        document.getElementById("confEndTime_Text").value = hh + ":" + mm + " " + ap;

    //FB 2588
    ChangeTimeFormat("O");
    document.getElementById("confEndTime_Text").value = document.getElementById("hdnEndTime").value;
    
    CalculateDurationFromTime();
}

function changeTime(endTime, ctrlName, numDur, type)
{
    var apBfr = endTime.toLocaleTimeString().toString().split(" ")[1];          
    if(type == "M")
        endTime.setMinutes(endTime.getMinutes() + numDur);        
    else
        endTime.setHours(endTime.getHours() + numDur);

    AssignTimeFromDate(endTime, ctrlName)
}  

function AssignTimeFromDate(endTime, ctrlName)
{
    var hh = parseInt(endTime.getHours());
    var ap = "AM";
    if (hh == 0) hh = 12;
    if (hh > 12) 
    {    
        hh = hh - 12;
        ap = "PM";
    }
    
    if (hh < 10)
        hh = "0" + hh;
    
    var mm = parseInt(endTime.getMinutes()); 
    if (mm < 10)
        mm = "0" + mm;
        
    var evntime = parseInt(hh,10);
    
     if(evntime < 12 && ap == "PM")
        evntime = evntime + 12;
               
    if('<%=Session["timeFormat"]%>' == '0')
    {
        if(ap == "AM")
        {
            if(hh == "12")
                document.getElementById(ctrlName).value = "00:" + mm ;
            else
                document.getElementById(ctrlName).value = hh + ":" + mm ;
        }
        else
        {
            if(evntime == "24")
                document.getElementById(ctrlName).value = "12:" + mm ;
            else
                document.getElementById(ctrlName).value = evntime + ":" + mm ;
        }
    }
    else
        document.getElementById(ctrlName).value = hh + ":" + mm + " " + ap;
}

function CalculateDurationFromTime() {
    ChangeTimeFormat("D"); //FB 2588
    startTime = new Date("01/01/1999 " + document.getElementById("hdnStartTime").value);
    endTime = new Date("01/01/1999 " + document.getElementById("hdnEndTime").value); 
    
    var oDiff = get_time_difference(startTime, endTime);
    
    document.getElementById("RecurDurationhr").value = oDiff.hours;
    document.getElementById("RecurDurationmi").value = oDiff.minutes;
}

function CalculateTimeFromDuration() {
    ChangeTimeFormat("D"); //FB 2588
    var startDate = GetDefaultDate(document.getElementById("StartDate").value,'<%=format%>');
    startTime = new Date(startDate + " " + document.getElementById("hdnStartTime").value); 
    
    if (document.frmRecur.RecurDurationhr) {
		rdhr = parseInt(document.frmRecur.RecurDurationhr.value, 10);
		rdmi = parseInt(document.frmRecur.RecurDurationmi.value, 10);
		
		recurduration = rdhr * 60 + rdmi;
        if (recurduration > (maxDuration * 60))
            alert(EN_314);		
	}
	
	changeTime(startTime, "confEndTime_Text", recurduration, "M");
}
//FB 2634
function fnClear()
{
    var args = fnClear.arguments;
    
    var txtCtrl = document.getElementById(args[0]);
    
    if(txtCtrl)
    {
        if(txtCtrl.value == "")
            txtCtrl.value = "0";
    }
}

</script>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>myVRM</title>
    <link rel="StyleSheet" href="css/divtable.css" type="text/css" />
    <link rel="stylesheet" title="Expedite base styles" type="text/css" href="css/border-table.css" />
    <link rel="stylesheet" title="Expedite base styles" type="text/css" href="css/main-table.css" />
    <%--<link rel="stylesheet" title="Expedite base styles" type="text/css" href="css/main.css" />--%>
    <script type="text/javascript">    // FB 2790
    var path = '<%=Session["OrgCSSPath"]%>';
    path = path.split('.')[0] + '<%=Session["ThemeType"]%>' + ".css"; // FB 2815
    document.write('<link rel="stylesheet" title="Expedite base styles" type="text/css" href="' + path + '?' + new Date().getTime() + '">');
    </script>
    <link rel="stylesheet" type="text/css" media="all" href="css/calendar-blue.css" /> <%--FB 1982--%>
    <style type="text/css">
        .blackxxxsboldtext label
        {
            font-size: 11px;
            vertical-align: text-top;
            font-weight: normal;
        }
        .blackxxxstext label
        {
            vertical-align: text-top;
        }
        .style1
        {
            font-size: xx-small;
        }
         .rowbreak td
         {
     	    word-wrap: break-word;
         }
    </style>
</head>
<body>
    <form id="frmRecur" runat="server">
    <input type="hidden" id="TimeZoneText" name="TimeZoneText" value="" />
    <input type="hidden" id="TimeZoneValues" name="TimeZoneValues" value="" />
    <input type="hidden" id="EndDateText" name="EndDateText" value="" />
    <input type="hidden" id="RecurValue" name="RecurValue" value="" />
    <input type="hidden" id="RecurPattern" name="RecurPattern" value="" />
    <input type="hidden" id="CustomDates" name="CutomDates" value="" />
    <input type="hidden" id="RecPattern" name="RecPattern" value="" />
    <input type="hidden" id="HdnDateFormat" name="HdnDateFormat" value="<%=format %>" />
    <input type="hidden" id="hdnColor"  name="hdnColor" />
    <input type="hidden" runat="server" id="hdnStartTime" name="hdnStartTime" /><%--FB 2588--%>
	<input type="hidden" runat="server" id="hdnEndTime" name="hdnEndTime"  /><%--FB 2588--%>
    <div>
        <table border="0" align="center">
            <tr>
                <td colspan="3" align="center">
                    <span class="subtitleblueblodtext">Special Recurring Pattern</span>
                </td>
            </tr>
            <%--FB 2634--%>
             <tr id="SetupRow">
                <td class="blackxxxsboldtext" align="left" id="SDateText" nowrap="nowrap" width="21%">
                    Set-up (Minutes)
                </td>
                <td valign="top" style="text-align: left; height: 27px; color: black; font-family: arial;
                    width: 403px;" align="center" colspan="2">
                     <asp:TextBox ID="SetupDuration" runat="server" CssClass="altText" Width="19%" AutoPostBack="false" onblur="javascript:fnClear('SetupDuration')"></asp:TextBox>
                    <asp:RegularExpressionValidator ID="regSetupDuration" runat="server" ControlToValidate="SetupDuration" ValidationGroup="Submit"
                        Display="Dynamic" ErrorMessage="Invalid Duration" ValidationExpression="\d+"></asp:RegularExpressionValidator>
                </td>
            </tr>
            <tr id="ConfStartRow">
                <td class="blackxxxsboldtext" align="left">
                    <span class="reqfldstarText">*</span> Conference Start
                </td>
                <td valign="top" style="text-align: left; height: 27px; color: black; font-family: arial;
                    width: 403px;" align="center" colspan="2">
                    <mbcbb:ComboBox ID="confStartTime" runat="server" CssClass="altText" Rows="10" CausesValidation="True"
                        onblur="javascript:formatTime('confStartTime_Text');return ChangeEndDate();" Style="width: auto" AutoPostBack="false">
                    </mbcbb:ComboBox>
                    <asp:RequiredFieldValidator ID="reqConfStartTime" runat="server" ControlToValidate="confStartTime"
                        Display="Dynamic" ErrorMessage="Time is Required"></asp:RequiredFieldValidator>
                    <asp:RegularExpressionValidator ID="regConfStartTime" runat="server" ControlToValidate="confStartTime"
                        Display="Dynamic" ErrorMessage="Invalid Time (HH:MM AM/PM)" ValidationExpression="[0-1][0-9]:[0-5][0-9] ?[A|a|P|p][M|m]"></asp:RegularExpressionValidator><%--FB 1715--%>
                </td>
            </tr>
            <tr id="ConfEndRow" style="display:none;">
                <td class="blackxxxsboldtext" align="left" id="EDateText">
                    <span class="reqfldstarText">*</span>Conference End
                </td>
                <td valign="top" style="text-align: left; height: 27px; color: black; font-family: arial;
                    width: 403px;" align="center" colspan="2">
                    <mbcbb:ComboBox ID="confEndTime" runat="server" CssClass="altSelectFormat" Rows="10"
                        onblur="javascript:formatTime('confEndTime_Text');" Style="width: auto" CausesValidation="True" AutoPostBack="false">
                    </mbcbb:ComboBox>
                    <asp:RequiredFieldValidator ID="reqEndTime" runat="server" ControlToValidate="confEndTime"
                        Display="Dynamic" ErrorMessage="Time is Required"></asp:RequiredFieldValidator>
                    <asp:RegularExpressionValidator ID="regEndTime" runat="server" ControlToValidate="confEndTime"
                        Display="Dynamic" ErrorMessage="Invalid Time (HH:MM AM/PM)" ValidationExpression="[0-1][0-9]:[0-5][0-9] ?[A|a|P|p][M|m]"></asp:RegularExpressionValidator><%--FB 1715--%>
                </td>
            </tr>
            <tr id="TearDownRow">
                <td class="blackxxxsboldtext" align="left" id="Td1" nowrap="nowrap">
                     Tear Down (Minutes)
                </td>
                <td>
                    <span id="TearDownArea">
                        <asp:TextBox ID="TearDownDuration"  runat="server" CssClass="altText" Width="19%" AutoPostBack="false" onblur="javascript:fnClear('TearDownDuration')"></asp:TextBox>
                        <asp:RegularExpressionValidator ID="regTearDown" runat="server" ControlToValidate="TearDownDuration"
                        Display="Dynamic" ErrorMessage="Invalid Duration" ValidationExpression="\d+"></asp:RegularExpressionValidator>
                    </span>
                </td>
            </tr>
            <tr>
                <td align="left" class="blackxxxsboldtext" valign="top">
                    Duration
                </td>
                <td>
                    <asp:TextBox ID="RecurDurationhr" runat="server" CssClass="altText" Width="10%" onblur="Javascript:  return validateDurationHr();" 
                    onchange="javascript: CalculateTimeFromDuration();" ></asp:TextBox>
                    hrs
                    <asp:TextBox ID="RecurDurationmi" runat="server" CssClass="altText" Width="10%" onblur="Javascript: return validateDurationMi();" 
                    onchange="javascript: CalculateTimeFromDuration();"></asp:TextBox>
                    mins &nbsp; <span class="blackxxxsboldtext">
                    <%--FB 2634--%>
                    <%if (EnableBufferZone == 0)
                      { %> 
                        ( Maximum Limit is <%=Application["MaxConferenceDurationInHours"]%> hours) 
                     <%}
                      else
                      { %>
                        ( Maximum Limit is <%=Application["MaxConferenceDurationInHours"]%> hours including buffer period) 
                    <%} %> </span>
                    <asp:TextBox ID="EndText" runat="server" Style="display: none"></asp:TextBox>
                </td>
            </tr>
            <tr> <%--FB 2052--%>
                <td align="left" class="blackxxxsboldtext" valign="top">
                    Day Colors
                </td>
                <td align="left" colspan="2">
                    <asp:Label ID="lblNoneDayColor" runat="server" Text="None"></asp:Label>                
                    <asp:RadioButtonList ID="rdlstDayColor" runat="server" GroupName="daySelection" OnClick="javascript:document.getElementById('hdnColor').value='1'" RepeatDirection="Vertical" CssClass="rowbreak">
                    </asp:RadioButtonList>
                </td>
            </tr>
            <tr>
                <td colspan="3">
                    <span class="subtitleblueblodtext">Recurring Pattern</span>
                </td>
            </tr>
            <tr>
                <td colspan="3" align="left" valign="top" class="blackxxxstext">
                    <table width="100%" cellpadding="2" cellspacing="0">
                        <tr>
                            <td>
                                &nbsp;
                            </td>
                            <td class="blackxxxstext">
                                &nbsp;<asp:RadioButton ID="selRecur1" runat="server" GroupName="scenario" OnClick="javascript:RecPattern.value='1'; fnDefault();"
                                    value="1" Style="vertical-align: middle;" />
                                <span class="blackxxxsboldtext">Recurrence 1: </span>&nbsp; Every
                                <asp:DropDownList ID="selDay1" runat="server" CssClass="altText">
                                    <asp:ListItem Value="1">Monday</asp:ListItem>
                                    <asp:ListItem Value="2">Tuesday</asp:ListItem>
                                    <asp:ListItem Value="3">Wednesday</asp:ListItem>
                                    <asp:ListItem Value="4">Thursday</asp:ListItem>
                                    <asp:ListItem Value="5">Friday</asp:ListItem>
                                </asp:DropDownList>
                                 for chosen color day
                            </td>
                        </tr>
                        <tr>
                            <td>
                                &nbsp;
                            </td>
                            <td class="blackxxxstext">
                                &nbsp;<asp:RadioButton ID="selRecur2" runat="server" GroupName="scenario" OnClick="javascript:RecPattern.value='2'; fnDefault();"
                                    value="2" Style="vertical-align: middle;" />
                                <span class="blackxxxsboldtext">Recurrence 2: </span>&nbsp; Every other
                                <asp:DropDownList ID="selDay2" runat="server" CssClass="altText">
                                    <asp:ListItem Value="1">Monday</asp:ListItem>
                                    <asp:ListItem Value="2">Tuesday</asp:ListItem>
                                    <asp:ListItem Value="3">Wednesday</asp:ListItem>
                                    <asp:ListItem Value="4">Thursday</asp:ListItem>
                                    <asp:ListItem Value="5">Friday</asp:ListItem>
                                </asp:DropDownList>
                                 for chosen color day
                            </td>
                        </tr>
                        <tr>
                            <td>
                                &nbsp;
                            </td>
                            <td class="blackxxxstext">
                                &nbsp;<asp:RadioButton ID="selRecur3" runat="server" GroupName="scenario" OnClick="javascript:RecPattern.value='3'; fnDefault(); "
                                    value="3" Style="vertical-align: middle;" />
                                <span class="blackxxxsboldtext">Recurrence 3: </span>&nbsp; Every first
                                <asp:DropDownList ID="selDay3" runat="server" CssClass="altText">
                                    <asp:ListItem Value="1">Monday</asp:ListItem>
                                    <asp:ListItem Value="2">Tuesday</asp:ListItem>
                                    <asp:ListItem Value="3">Wednesday</asp:ListItem>
                                    <asp:ListItem Value="4">Thursday</asp:ListItem>
                                    <asp:ListItem Value="5">Friday</asp:ListItem>
                                </asp:DropDownList>
                                 of the month for chosen color day
                            </td>
                        </tr>
                        <%-- FB 1911 --%>
                        <tr>
                            <td>
                                &nbsp;
                            </td>
                            <td class="blackxxxstext">
                                &nbsp;<asp:RadioButton ID="selRecur4" runat="server" GroupName="scenario" OnClick="javascript:RecPattern.value='4'; fnDefault(); "
                                    value="7" Style="vertical-align: middle;" />
                                <span class="blackxxxsboldtext">Recurrence 4: </span>&nbsp; Every second
                                <asp:DropDownList ID="selDay4" runat="server" CssClass="altText">
                                    <asp:ListItem Value="1">Monday</asp:ListItem>
                                    <asp:ListItem Value="2">Tuesday</asp:ListItem>
                                    <asp:ListItem Value="3">Wednesday</asp:ListItem>
                                    <asp:ListItem Value="4">Thursday</asp:ListItem>
                                    <asp:ListItem Value="5">Friday</asp:ListItem>
                                </asp:DropDownList>
                                 of the month for chosen color day
                            </td>
                        </tr>
                        
                        <tr>
                            <td>
                                &nbsp;
                            </td>
                            <td class="blackxxxstext">
                                &nbsp;<asp:RadioButton ID="selRecur5" GroupName="scenario" runat="server" OnClick="javascript:RecPattern.value='5'; fnDefault();"
                                    value="4" Style="vertical-align: middle;" />
                                    <%-- FB 1911 --%>
                                <span class="blackxxxsboldtext">Recurrence 5: </span>&nbsp; Every last
                                <asp:DropDownList ID="selDay5" runat="server" CssClass="altText">
                                    <asp:ListItem Value="1">Monday</asp:ListItem>
                                    <asp:ListItem Value="2">Tuesday</asp:ListItem>
                                    <asp:ListItem Value="3">Wednesday</asp:ListItem>
                                    <asp:ListItem Value="4">Thursday</asp:ListItem>
                                    <asp:ListItem Value="5">Friday</asp:ListItem>
                                </asp:DropDownList>
                                 of the month for chosen color day
                            </td>
                        </tr>
                        <tr>
                            <td>
                                &nbsp;
                            </td>
                            <td class="blackxxxstext">
                                &nbsp;<asp:RadioButton ID="selRecur6" runat="server" GroupName="scenario" OnClick="javascript:RecPattern.value='6'; fnDefault();"
                                    value="5" Style="vertical-align: middle;" />
                                  <%-- FB 1911 --%>  
                                <span class="blackxxxsboldtext">Recurrence 6: </span>&nbsp; Every first color day of
                                the month for chosen color day (regardless of day)
                            </td>
                        </tr>
                        
                        <tr>
                            <td>
                                &nbsp;
                            </td>
                            <td class="blackxxxstext">
                                &nbsp;<asp:RadioButton ID="selRecur7" runat="server" GroupName="scenario" OnClick="javascript:RecPattern.value='7'; fnDefault();"
                                    value="8" Style="vertical-align: middle;" />
                                <span class="blackxxxsboldtext">Recurrence 7: </span>&nbsp; Every second color day of
                                the month for chosen color day (regardless of day)
                            </td>
                        </tr>
                        <tr>
                            <td>
                                &nbsp;
                            </td>
                            <td class="blackxxxstext">
                                &nbsp;<asp:RadioButton ID="selRecur8" runat="server" GroupName="scenario" OnClick="javascript:RecPattern.value='8'; fnDefault();"
                                    value="6" Style="vertical-align: middle;" />
                                    <%-- FB 1911 --%>
                                <span class="blackxxxsboldtext">Recurrence 8: </span>&nbsp; Every last color day
                                of the month for chosen color day (regardless of day)
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr id="RangeRow" runat="server">
                <td colspan="3">
                    <table width="100%" cellspacing="0">
                        <tr>
                            <td colspan="3">
                                <span class="subtitleblueblodtext">Range of Recurrence </span>
                            </td>
                        </tr>
                        <tr valign="top">
                            <td class="blackblodtext" nowrap>
                                Start
                                <asp:TextBox ID="StartDate" Width="100px" Font-Size="9" CssClass="altText" runat="server"></asp:TextBox>
                                <img alt="" src="image/calendar.gif" border="0" id="cal_triggerd" style="cursor: pointer;
                                    vertical-align: top;" title="Date selector" onclick="return showCalendar('<%=StartDate.ClientID%>', 'cal_triggerd', 1, '<%=format %>');" />
                            </td>
                            <td style="width: 3%;">
                            </td>
                            <td>
                                <table>
                                    <tr>
                                        <td class="blackxxxsboldtext" colspan="2">
                                            <asp:RadioButton ID="EndType" runat="server" Style="vertical-align: middle;" GroupName="RangeGroup"
                                                onClick="javascript: document.frmRecur.Occurrence.value=''; document.frmRecur.EndDate.value='';" />
                                            No end date (<span class="style1">Maximum 24 instances for Monthly and 104 for
                                                weekly or equivalent) </span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="blackxxxsboldtext" nowrap>
                                            <asp:RadioButton ID="REndAfter" runat="server" Style="vertical-align: middle;" GroupName="RangeGroup"
                                                onClick="javascript: document.frmRecur.EndDate.value='';" />
                                            End after
                                        </td>
                                        <td class="blackxxxsboldtext">
                                            <asp:TextBox ID="Occurrence" CssClass="altText" Width="35%" runat="server" onClick="javascript: document.frmRecur.REndAfter.checked=true; document.frmRecur.EndDate.value='';"></asp:TextBox>
                                            occurrences
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="blackxxxsboldtext">
                                            <asp:RadioButton ID="REndBy" runat="server" Style="vertical-align: middle;" GroupName="RangeGroup"
                                                onClick="javascript: document.frmRecur.Occurrence.value='';" />
                                            End by
                                        </td>
                                        <td nowrap>
                                            <asp:TextBox ID="EndDate" Width="100px" onblur="javascript:CheckDate(this)" onchange="javascript:CheckDate(this)"
                                                CssClass="altText" runat="server" onClick="javascript: document.frmRecur.REndBy.checked=true; document.frmRecur.Occurrence.value='';"></asp:TextBox>
                                            <img alt="" src="image/calendar.gif" border="0" id="cal_trigger2" style="cursor: pointer;
                                                vertical-align: top;" title="Date selector" onclick="return showCalendar('<%=EndDate.ClientID%>', 'cal_trigger2', 1, '<%=format %>');" />
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td colspan="3" >
                    Note: Maximum limit of
                    <%=Application["confRecurrence"]%>
                    instances/occurrences in the recurring series.
                </td>
            </tr>
            <tr>
                <td colspan="3" align="center">
                    <asp:Button ID="Cancel" Text="Cancel" runat="server" CssClass="altMedium0BlueButtonFormat"
                        OnClientClick="javaScript: opener.visControls();window.close();" />
                    <asp:Button ID="Reset" Text="Reset" runat="server" CssClass="altMedium0BlueButtonFormat"
                        OnClientClick="javaScript: frmRecur.RecurValue.value = openerfrm.RecurSpec.value; fnDefault(); initial(); " />
                    <asp:Button ID="RecurSubmit" Text="Submit" runat="server" OnClientClick="javascript:return SubmitRecurrence();"
                        CssClass="altMedium0BlueButtonFormat" />
                    <asp:Button ID="RecurSubmit1" Text="Remove Recurrence" runat="server" CssClass="altMedium0BlueButtonFormat" style="width:180px"
                        OnClientClick="javaScript: removerecur();opener.visControls();" />
                </td>
            </tr>
        </table>
    </div>
    </form>

    <script type="text/javascript"> 

    if (document.frmRecur.EndText)
	    document.frmRecur.EndText.disabled = true;
    if (document.frmRecur.DurText)
	    document.frmRecur.DurText.disabled = true;

    document.frmRecur.RecurValue.value = openerfrm.RecurSpec.value;
	
    initial();
    
    fnShow();
    //FB 2634
    if (document.getElementById("confStartTime_Text")) 
    {
        var confstarttime_text = document.getElementById("confStartTime_Text");
        confstarttime_text.onblur = function() {
            if(formatTime('confStartTime_Text','regConfStartTime'))
                return ChangeEndDate()
        };
    }
    if (document.getElementById("confEndTime_Text")) 
    {
        var confendtime_text = document.getElementById("confEndTime_Text");
        confendtime_text.onblur = function() {
            if(formatTime('confEndTime_Text','regEndTime'))
                return ChangeEndDate("ET")
        };
    }
        
    function onclosesp()
    {
       window.opener.visControls();
    }

    window.onbeforeunload = onclosesp;
        
    </script>

</body>
</html>
<%--code added for Soft Edge button--%>
<script type="text/javascript" src="inc/softedge.js"></script>
<!--  #INCLUDE FILE="../en/inc/Holiday.aspx"  -->
