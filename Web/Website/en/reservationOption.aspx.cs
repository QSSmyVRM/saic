﻿//ZD 100147 Start
/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
//ZD 100147 End
using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

public partial class en_reservationOption : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (obj == null)
            obj = new myVRMNet.NETFunctions();
        obj.AccessandURLConformityCheck("reservationOption.aspx", Request.Url.AbsoluteUri.ToLower()); // ZD 100263

    }
}
