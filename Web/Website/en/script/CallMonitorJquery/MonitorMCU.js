﻿/*ZD 100147 Start*/
/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
/*ZD 100147 End*/
$(document).ready(function() {

    //--------------------------------------- FB 2652 Starts ---------------------------------------

    //  ****************** Mute Functions Starts ***************************
    $('.MuteAll').click(function() {
        $('#popupdiv').fadeIn();
        $('#PopupMuteAll').fadeIn();
        $('#communStatus', window.parent.document).val("1");
        var tempID = $(this).attr('id');
        $('#CurrentConfSupportingRowID').val(tempID);
        var tempStr = "";
        tempStr = '<table cellpadding="0" cellspacing="0" border="0" style="border-color:gray" width="90%">';
        tempStr += '<tr height="30px" style=" background-color:#A7958B"><td align="center" style="color:White"><b>S.No</b></td><td align="center" style="color:White"><b>Select</b></td><td align="left" style="padding-left:20px;color:White"><b>Room Name</b></td></tr>';
        for (var i = 0; i < $('#PartyListCount' + $(this).attr('id')).val(); i++) {
            tempStr += '<tr><td align="center">' + parseInt(i + 1) + '</td><td align="center"><input type="checkbox" name="' + $(this).attr('id') + '_' + i + '" title="' + $('#partTerminalType' + tempID + '__' + i).val() + '" class="chkMuteAll" id="' + $('#partEndpointID' + tempID + '__' + i).val() + '" /></td>';
            tempStr += '<td align="left" style="padding-left:20px">' + $('#participantName' + tempID + '__' + i).val() + '</td>';
        }
        tempStr += '</tr></table>';
        $('#divMuteAll').html(tempStr);
        $("#PopupMuteAll").bPopup({
            fadeSpeed: 'slow',
            followSpeed: 1500,
            modalColor: 'gray'
        });
    });

    $('.chkMuteAll').live('click', function() {
        if ($('#MuteAllIdArray').val().indexOf($(this).attr('id')) >= 0) { } // Check duplicate
        else {
            $('#MuteAllIdArray').val($('#MuteAllIdArray').val() + "," + $(this).attr('id'));
            $('#MuteAllterminalTypeArray').val($('#MuteAllterminalTypeArray').val() + "," + $(this).attr('title'));
        }
        //        if ($('#MuteAllterminalTypeArray').val().indexOf($(this).attr('title')) >= 0) { } // Check duplicate
        //        else
        //            $('#MuteAllterminalTypeArray').val($('#MuteAllterminalTypeArray').val() + "," + $(this).attr('title'));
    });


    $('#btnMuteAllSubmit').click(function() {
        var IDList = $('#MuteAllIdArray').val();
        IDList = IDList.substring(1, IDList.length);
        var tType = $('#MuteAllterminalTypeArray').val();
        tType = tType.substring(1, tType.length);

        //alert($('#CurrentConfSupportingRowID').val() + '_');        
        //        for (var i = 0; i < $('#PartyListCount' + $('#CurrentConfSupportingRowID').val()).val(); i++) {            
        //            alert($("input[name='"+"1-2-3-56_8_"+"'"+i+"'] :checked]"));            
        //            if ($('#' + $('#CurrentConfSupportingRowID').val() + '_' + i).attr('name').selected(true))
        //        }

        $.ajax({
            type: "POST",
            contentType: "application/json; charset=utf-8",
            url: "MonitorMCU.aspx/MuteAllExceptSelectedParticipants",
            data: JSON.stringify({ userID: $('#userID').val(), confID: $('#conID' + $('#CurrentConfSupportingRowID').val()).val(), ParticipantsIdList: IDList, ParticipantTerminalTypeList: tType }),
            dataType: "json",
            success: function(data) {
                if (data != null) {
                    $('#popupdiv').fadeOut();
                    $('#PopupMuteAll').fadeOut();
                    $('#communStatus', window.parent.document).val("0");
                    operationSuccess();
                    $('#btnRefreshPage', window.parent.document).trigger('click');
                }
                else
                    $('#divMuteAll').val($('#errormsgbox').html("Operation UnSuccessful"));
            }
        });
    });

    $('#btnMuteAllClose').click(function() {
        $('#popupdiv').fadeOut();
        $('#PopupMuteAll').fadeOut();
        $('#communStatus', window.parent.document).val("0");
    });
    //  ****************** Mute Functions Ends *****************************

    //  ****************** Un-Mute Functions Starts ************************
    $('.UnMuteAll').click(function() {
        $.ajax({
            type: "POST",
            contentType: "application/json; charset=utf-8",
            url: "MonitorMCU.aspx/UnmuteAllParticipants",
            data: JSON.stringify({ userID: $('#userID').val(), confID: $('#conID' + $(this).attr('id')).val() }),
            dataType: "json",
            success: function(data) {
                debugger
                if (data != null)
                    operationSuccess();
                else
                    operationError();
            }
        });
    });
    //  ****************** Un-Mute Functions Ends ************************
    //--------------------------- FB 2652 Ends ------------------------------------------------


    //--------------------------------------- Timer Control -----------------------------------
    var Duarion;
    for (var i = 1; i <= $('#TotalMcuCount').val(); i++) {
        for (var j = 1; j <= $('#confTotalCount' + i).val(); j++) {
            Duarion = $('#hdntime' + i + '' + j).val();
            settimer(i, j, Duarion);
        }
    }

    function settimer(i, j, Duarion) {
        var countdown = {
            startInterval: function() {
                var TotalTime = Duarion;
                Duarion = Duarion.substr(Duarion.length - 2).replace('0', '');
                var count = Duarion;
                var tHr = TotalTime.substr(0, 2);
                var tMin = TotalTime.substr(3, 2);
                //FB 2877 - Start
                var currentId = setInterval(function() {
                    //FB 2501 Dec5 Start
                    if (count >= 0) {
                        if (tMin <= 0) {
                            if (tHr != 0 && count == 0) {

                                tHr = tHr - 1;
                                tMin = 59;
                                count = 60;
                            }
                        }
                        $("#coundowntime" + i + '' + j).html(tHr + ':' + tMin + ':' + count);
                    }
                    if (count <= 0) {
                        if (count == 0 && tHr == 0 && tMin == 0) {
                            tMin = tMin;
                            count = count;
                        }
                        else {
                            tMin = tMin - 1;
                            count = 60;
                        }

                        if (tMin < 0) {
                            if (tHr != 0 && count == 0) {
                                tHr = tHr - 1;
                                tMin = 59;
                                count = 60;
                            }
                        }
                        $("#coundowntime" + i + '' + j).html(tHr + ':' + tMin + ':' + count);
                    }
                    //FB 2501 Dec5 End
                    //FB 2877 - End
                    else
                        --count;
                }, 1000);
                countdown.intervalId = currentId;
            }
        };
        countdown.startInterval();
    }

    // --------------------------------------------------------------------------------------------------   


    function operationSuccess() {
        $('#successbox').html("Operation Successful");
        $('#successbox').fadeIn(5000, function() {
            $('#successbox').fadeOut(5000);
        });
    }

    function operationError() {
        $('#errormsgbox').html("Operation UnSuccessful");
        $('#errormsgbox').fadeIn(5000, function() {
            $('#errormsgbox').fadeOut(5000);
        });
    }

    //--------------------------------------- Tooltip Contents ------------------------------------------
    $(".GridEvents").mouseover(function() {
        var imgpath = $(this).attr('src');
        var imgID = $(this).attr('id');
        imgpath = imgpath.replace('../image/MonitorMCU/', '');
        imgpath = imgpath.replace('.gif', '');
        // lock
        if (imgpath == "lock_1")
            $(this).attr('title', 'Un lock')
        else if (imgpath == "lock_0")
            $(this).attr('title', 'Lock')
        // AudioTX
        else if (imgpath == "audioTx_1")
            $(this).attr('title', 'Unmute AudioTx')
        else if (imgpath == "audioTx_0")
            $(this).attr('title', 'Mute AudioTx')
        // AudioRX
        else if (imgpath == "audioRx_1")
            $(this).attr('title', 'Unmute AudioRx')
        else if (imgpath == "audioRx_0")
            $(this).attr('title', 'Mute AudioRx')
        // VideoRX
        else if (imgpath == "videoTx_1")
            $(this).attr('title', 'Unmute VideoTx')
        else if (imgpath == "videoTx_0")
            $(this).attr('title', 'Mute VideoTx')
        // VideoRX
        else if (imgpath == "videoRx_1")
            $(this).attr('title', 'Unmute VideoRx')
        else if (imgpath == "videoRx_0")
            $(this).attr('title', 'Mute VideoRx')
        // Info image status
        else if (imgpath == "TerminalStaus_0")
            $(this).attr('title', 'DisConnected')
        else if (imgpath == "TerminalStaus_1")
            $(this).attr('title', 'Connecting')
        else if (imgpath == "TerminalStaus_2")
            $(this).attr('title', 'Connected')
        else if (imgpath == "TerminalStaus_3")
            $(this).attr('title', 'Online')
        // Call
        else if (imgpath == "call_1") {
            var childgridID = imgID.match("__", 'g');
            if (childgridID == "__")
                $(this).attr('title', 'Connect')
        }
        else if (imgpath == "call_0") {
            var childgridID = imgID.match("__", 'g');
            if (childgridID == "__")
                $(this).attr('title', 'DisConnect')
        }
    });
    // --------------------------------------------------------------------------------------------------


    //--------------------------------------- Set layout Popup window -----------------------------------

    $('.layout').click(function() {
        $('#AddUserWindowRedirect').val($(this).attr("id").replace('layout', ''));
        $('#communStatus', window.parent.document).val("1");
        var id = $(this).attr("id");
        var lVal = $('#hdnSetlayoutVal' + id).val();
        if (lVal < 10)
            lVal = "0" + lVal;
        $('#CurrentLayout').attr("src", "image/displaylayout/" + lVal + ".gif");
        $('#PopupSetLayout').bPopup({
            fadeSpeed: 'slow',
            followSpeed: 1500,
            modalColor: 'gray'
        });
    });

    // ---------------------------------------------------------------------------------------------------              

    //--------------------------------------- Select Set layout Popup window ---------------------------- 
    $('.SelectLayout').live('click', function() {
        $('#CurSelectedLayout').attr("src", "image/displaylayout/" + $(this).attr("id") + ".gif");
    });
    // -------------------------------------------------------------------------------------------------              

    // --------------------------------------- Set Layout Submit ---------------------------------------
    $('#SetLayoutSubmit').click(function() {

        var ImgVal = $('#AddUserWindowRedirect').val();
        var childgridID = ImgVal.match("__", 'g');
        var id = $('#AddUserWindowRedirect').val();
        var lblConfID = $('#conID' + id).val();
        var ImgVal = $(this).attr("id");
        var current = $('#CurSelectedLayout').attr("src");
        current = current.replace('image/displaylayout/', '');
        current = current.replace('.gif', '');
        var dtParam;
        if (childgridID == "__") {
            fnmethod = "particpant";
            var EndpointType = $('#partEndpointID' + id).val();
            var terminalType = $('#partTerminalType' + id).val();
            dtParam = { fnmethod: fnmethod, userID: $('#userID').val(), terminal: terminalType, Etype: EndpointType, confID: $('#confId' + id).val(), CurrentImgVal: current };

        }
        else {
            fnmethod = "conference";
            dtParam = { fnmethod: fnmethod, userID: $('#userID').val(), terminal: "", Etype: "", confID: $('#conID' + id).val(), CurrentImgVal: current };
        }
        var dataParameter = JSON.stringify(dtParam);
        $.ajax({
            type: "POST",
            contentType: "application/json; charset=utf-8",
            data: dataParameter,
            dataType: "json",
            url: "MonitorMCU.aspx/fnPopupSetLayout",
            success: function(data) {
            if (data.d != '' && data.d == "Operation Successful!") { //FB 3009
                    $('#popupdiv', window.parent.document).fadeOut();
                    $('#PopupSetLayout', window.parent.document).fadeOut();
                    $('#communStatus', window.parent.document).val("0");
                    $('#successbox', window.parent.document).html("Operation Successful");
                    $('#successbox', window.parent.document).fadeIn(5000, function() {
                        $('#successbox', window.parent.document).fadeOut(5000);
                    });
                    $('#btnRefreshPage', window.parent.document).trigger('click');//FB 3009
                }
                else {
                    $('#popupdiv', window.parent.document).fadeOut();
                    $('#PopupSetLayout', window.parent.document).fadeOut();
                    $('#communStatus', window.parent.document).val("0");
                    $('#errormsgbox', window.parent.document).html("Operation UnSuccessful");
                    $('#errormsgbox', window.parent.document).fadeIn(5000, function() {
                        $('#errormsgbox', window.parent.document).fadeOut(5000);
                    });
                    $('#btnRefreshPage', window.parent.document).trigger('click'); //FB 3009
                }
            },
            error: function(result) {
                $('#errormsgbox', window.parent.document).html("Operation UnSuccessful");
                $('#errormsgbox', window.parent.document).fadeIn(5000, function() {
                    $('#errormsgbox', window.parent.document).fadeOut(5000);
                });
                $('#btnRefreshPage', window.parent.document).trigger('click'); //FB 3009
            }
        });
        $('#AddUserWindowRedirect').val("");
        
        return false;
    });
    // ------------------------------------Set Layout Submit End-------------------------------------        

    // --------------------------------------- Set Layout Cancel -----------------------------------------       
    $('#SetLayoutCancel').click(function() {
        $('#btnRefreshPage', window.parent.document).trigger('click'); //Tamil
        var id = window.parent.document.getElementById('AddUserWindowRedirect').value;
        if (id != '' || id != null) {
            $('#PopupSetLayout', window.parent.document).fadeOut();
            $('#popupdiv', window.parent.document).fadeOut();
            $('#communStatus', window.parent.document).val("0");
            return false;
        }
        else
            return true;
        $('#AddUserWindowRedirect').val("");
    });
    // ---------------------------------------------------------------------------------------------------         


    // --------------------------------------- Add User Popup window -------------------------------------
    $('.addUser').click(function() {
        $('#popupdiv').fadeIn();
        var confIndex = $(this).attr('id').replace('addUser', '');
        var confID = $('#conforgID' + confIndex).val();
        $("#addListUser").attr("src", "AddNewEndpoint.aspx?conforgID=" + confID + "&ifrm=true"); //FB 2646
        $('#AddUserWindowRedirect').val($(this).attr("id").replace('addUser', ''));
        $('#PopupAddUser').show();
        $('#communStatus', window.parent.document).val("1");
        $('#PopupAddUser').bPopup({
            fadeSpeed: 'slow',
            followSpeed: 1500,
            modalColor: 'gray'
        });
    });
    // -------------------------------------------------------------------------------------------------  

    // --------------------------------------- Add User Cancel ------------------------------------------       
    $('.altShortBlueButtonFormat').click(function() {
        var id = window.parent.document.getElementById('AddUserWindowRedirect').value;
        if (id != '' || id != null) {
            $('#popupdiv', window.parent.document).fadeOut();
            $('#PopupAddUser', window.parent.document).fadeOut();
            $('#communStatus', window.parent.document).val("0");
            return false;
        }
        else
            return true;
    });
    // --------------------------------------------------------------------------------------------------      

    // --------------------------------------- Add User Submit -----------------------------------------
    $('.altLongBlueButtonFormat').click(function() {        
        var ip = $('#txtAddress').val();
        var type = $('#lstProtocol').val();        
        // FB 2501  12 Dec 07
        if (ip == "" || ip == null)             
            return false;        
        //if(isValidIPAddress(ip,type))
        //     if(isValidIPAddress(ip))
        //     {        
        //$('#communStatus', window.parent.document).val("0");
        var id = window.parent.document.getElementById('AddUserWindowRedirect').value;
        if (id != '' || id != null) {
            if (($('#txtAddress').val() != null || $('#txtAddress').val() != '')) {
                var chkEncryption = $('#chkEncryption:checked').val() ? 1 : 0;
                var chkIsOutside = $('#chkIsOutside:checked').val() ? 1 : 0;
                var chkMute = $('#chkMute:checked').val() ? 1 : 0; //FB 2680
                var mcuIP = ($('#hdnMcuIP' + id, window.parent.document).val()).replace('Address: ', '');
                var dtParam = { userID: $('#conUserId' + id, window.parent.document).val(), confID: $('#conID' + id, window.parent.document).val(), MCUBridgeID: $('#McuBridgeID' + id, window.parent.document).val(), hdnEndpointID: ($('#hdnEndpointID') != null) ? $('#hdnEndpointID').val() : "", txtEndpointName: $('#txtEndpointName').val(), hdnProfileID: $('#hdnProfileID', window.parent.document).val(), hdnApiPortNo: $('#hdnApiPortNo').val(), lstAddressType: $('select#lstAddressType').val(), txtAddress: $('#txtAddress').val(), hdnEndpointURL: ($('#hdnEndpointURL') != null) ? $('#hdnEndpointURL').val() : "", lstConnectionType: $('select#lstConnectionType').val(), hdnVideoEquipment: $('#hdnVideoEquipment').val(), hdnExchangeID: $('#hdnExchangeID').val(), hdnLineRate: $('#hdnLineRate').val(), lstConnection: $('select#lstConnection').val(), lstProtocol: $('select#lstProtocol').val(), hdnMCUServiceAdd: mcuIP, chkEncryption: chkEncryption, chkIsOutside: chkIsOutside, ConforgID: $('#conforgID' + id, window.parent.document).val(), chkMute: chkMute }; //FB 2646 //FB 2680
                var dataParameter = JSON.stringify(dtParam);
                $.ajax({
                    type: "POST",
                    contentType: "application/json; charset=utf-8",
                    url: "MonitorMCU.aspx/fnPopupAddUser",
                    data: dataParameter,
                    dataType: "json",
                    success: function(data) {
                        if (data.d != '') {
                            $('#successbox').html("Operation Successful");
                            $('#successbox').fadeIn(1500, function() {
                                $('#successbox').fadeOut(1500);
                            });
                            $('#popupdiv', window.parent.document).fadeOut();
                            $('#PopupAddUser', window.parent.document).fadeOut();
                            $('#communStatus', window.parent.document).val("0");
                            $('#btnRefreshPage', window.parent.document).trigger('click');
                        }
                    },
                    error: function(result) {
                        $('#errormsgbox').html("Operation UnSuccessful");
                        $('#errormsgbox').fadeIn(1500, function() {
                            $('#errormsgbox').fadeOut(1500);
                        });
                    }
                });
                $('#AddUserWindowRedirect').val("");
                return false;
                //            }
                //            return false;
            }
            else {
                return true;
            }
        }
        else {
            $('#regAddress').show();
            return false;
        }

    });
    // -------------------------------------------------------------------------------------------------  

    //--------------------------------------- Ip Validation function -----------------------------------  
    //function isValidIPAddress(ipaddr, type) {
    function isValidIPAddress(ipaddr) {
        var str = ipaddr;
        //var str = ipaddr = type;
        var pattern;
        //if(type=="1")
        pattern = /^(([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\.){3}([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])$/g;
        //    else if(type=="2")
        //        pattern = /(^[A-Za-z0-9])$/;            
        var bValidIP = pattern.test(str);
        return bValidIP;
    }
    // -------------------------------------------------------------------------------------------------  

    //--------------------------------------- Selected layout color change ------------------------------  
    $('.LayoutColour').live('mouseover mouseout', function(e) {
        if (e.type == 'mouseover') {
            $(this).css("background", "#FC9126");
        }
        else if (e.type == 'mouseout') {
            $(this).css("background", "#ffffff");
        }
    });
    // ------------------------------------------------------------------------------------------------              

    //--------------------------------------- set Favorite ------------------------------            
    $(".setfavorite").click(function() {
        var mcuid = $(this).attr('id');
        var imgStatus = $(this).attr('src');
        imgStatus = imgStatus.replace('.gif', '');
        imgStatus = imgStatus.substr(imgStatus.length - 1);
        if (imgStatus == '1')
            imgStatus = '0';
        else
            imgStatus = '1';
        var userID;
        if ($('#userID').length > 0)
            userID = $('#userID').val();
        else
            userID = "11";
        var dataParameter = JSON.stringify({ userID: userID, mcuid: mcuid, imgStatus: imgStatus });

        $.ajax({
            type: "POST",
            contentType: "application/json; charset=utf-8",
            url: "MonitorMCU.aspx/setFavorite",
            data: dataParameter,
            dataType: "json",
            success: function(data) {
                var dataCount = data
                if (data.d != '' && (data.d == '1' || data.d == '0')) {
                    $("#" + mcuid).attr("src", "../image/MonitorMCU/favourite_" + data.d + ".gif");
                    $('#successbox').html("Operation Successful");
                    $('#successbox').fadeIn(1500, function() {
                        $('#successbox').fadeOut(1500);
                    });
                }
                else {
                    $('#successbox').html(data.d);
                    $('#successbox').fadeIn(1500, function() {
                        $('#successbox').fadeOut(1500);
                    });
                }
            },
            error: function(result) {
                $('#errormsgbox').html("Operation UnSuccessful");
                $('#errormsgbox').fadeIn(1500, function() {
                    $('#errormsgbox').fadeOut(1500);
                });
            }
        });
    });
    // ------------------------------------------------------------------------------------------------

    // -------------- All Grid - Image Events are call this Function ----------------------------------
    $(".GridEvents").live('click', function() {
        $('#communStatus', window.parent.document).val("1");
        var thisImg = $(this).attr("id");
        var imgpath = $(this).attr("src");
        var imgpath1 = $(this).attr("src");
        var fnaccessbackend;
        var dataParameter;
        imgpath = imgpath.replace('../image/MonitorMCU/', '');
        imgpath = imgpath.split('_', 1)[0];
        if (imgpath == "TerminalStaus")
            return false;
        var childgridID = thisImg.match("__", 'g');
        var thisImgid = thisImg.replace(imgpath, '');
        var fnmethod = "";
        if (childgridID == "__") {
            if (imgpath == "camera" || imgpath == "message") {
                $('#popupdiv').fadeIn();
                OpenPopupWindow(imgpath, thisImgid);
                return false;
            }
            else {
                fnaccessbackend = "MonitorMCU.aspx/fn" + imgpath;
                if (imgpath == "videoTx" || imgpath == "videoRx" || imgpath == "audioRx" || imgpath == "audioTx" || imgpath == "call") {
                    fnaccessbackend = 'MonitorMCU.aspx/fnAudioVideoStatus';
                    var Istatus = $(this).attr("src");
                    Istatus = Istatus.replace('.gif', '');
                    Istatus = Istatus.substr(Istatus.length - 1);
                    fnmethod = "particpant";
                    dataParameter = JSON.stringify({ userID: $('#partUserId' + thisImgid).val(), confID: $('#confId' + thisImgid).val(), EndpointID: $('#partEndpointID' + thisImgid).val(), terminalType: $('#partTerminalType' + thisImgid).val(), ImgStatus: Istatus, fnidentification: imgpath, fnmethod: fnmethod });
                }
                if (imgpath == "delete" || imgpath == "LectureMode" || imgpath == "LeaderParty") {//FB 2553

                    $('#popupdiv').fadeIn();
                    $("#progressdivwindow").bPopup({
                        fadeSpeed: 'fast',
                        followSpeed: 50,
                        modalColor: 'gray'
                    });
                    fnmethod = "particpant";
                    //fnaccessbackend = 'MonitorMCU.aspx/fndelete';
                    //dataParameter = JSON.stringify({ userID: $('#partUserId' + thisImgid).val(), confID: $('#confId' + thisImgid).val(), EndpointID: $('#partEndpointID' + thisImgid).val(), terminalType: $('#partTerminalType' + thisImgid).val(), ImgStatus: "", fnmethod: fnmethod });
			
	
                    if (imgpath == "delete") //FB 2553 Starts
                        fnaccessbackend = 'MonitorMCU.aspx/fndelete';
                    else if (imgpath == "LectureMode")
                        fnaccessbackend = 'MonitorMCU.aspx/fnLectureMode';
                    else if (imgpath == "LeaderParty")
                        fnaccessbackend = 'MonitorMCU.aspx/fnPartyLeader';
                    var Istatus = $(this).attr("src");
                    Istatus = Istatus.replace('.gif', '');
                    Istatus = Istatus.substr(Istatus.length - 1);
                    dataParameter = JSON.stringify({ userID: $('#partUserId' + thisImgid).val(), confID: $('#confId' + thisImgid).val(), EndpointID: $('#partEndpointID' + thisImgid).val(), terminalType: $('#partTerminalType' + thisImgid).val(), fnmethod: fnmethod, ImgStatus: Istatus });  //FB 2553 Ends
                }
            }
        }
        else {
            if (imgpath == "camera" || imgpath == "message" || imgpath == "time") {
                $('#popupdiv').fadeIn();
                OpenPopupWindow(imgpath, thisImgid);
                return false;
            }
            else {

                fnaccessbackend = "MonitorMCU.aspx/fns" + imgpath;
                if (imgpath == "videoTx" || imgpath == "videoRx" || imgpath == "audioRx" || imgpath == "audioTx" || imgpath == "call") {
                    fnaccessbackend = 'MonitorMCU.aspx/fnAudioVideoStatus';
                    var Istatus = $(this).attr("src");
                    Istatus = Istatus.replace('.gif', '');
                    Istatus = Istatus.substr(Istatus.length - 1);
                    fnmethod = "conference";
                    dataParameter = JSON.stringify({ userID: $('#conUserId' + thisImgid).val(), confID: $('#conID' + thisImgid).val(), EndpointID: "", terminalType: "", ImgStatus: Istatus, fnidentification: imgpath, fnmethod: fnmethod });
                }//FB 2441
                if (imgpath == "lock" || imgpath == "record") {
                    var imgStatus = $(this).attr("src");
                    imgStatus = imgStatus.replace('.gif', '');
                    imgStatus = imgStatus.substr(imgStatus.length - 1);
                    var MCUBridgeID = $('#McuBridgeID' + thisImgid).val();
                    dataParameter = JSON.stringify({ userID: $('#conUserId' + thisImgid).val(), confID: $('#conID' + thisImgid).val(), imgStatus: imgStatus, MCUBridgeID: MCUBridgeID });
                }
                if (imgpath == "delete") {
                    $('#popupdiv').fadeIn();
                    $("#progressdivwindow").bPopup({
                        fadeSpeed: 'fast',
                        followSpeed: 50,
                        modalColor: 'gray'
                    });
                    fnmethod = "conference";
                    fnaccessbackend = 'MonitorMCU.aspx/fndelete';
                    dataParameter = JSON.stringify({ userID: $('#conUserId' + thisImgid).val(), confID: $('#conID' + thisImgid).val(), EndpointID: "", terminalType: "", ImgStatus: "", fnmethod: fnmethod });
                }
            }
        }
        $.ajax({
            type: "POST",
            contentType: "application/json; charset=utf-8",
            url: fnaccessbackend,
            data: dataParameter,
            dataType: "json",
            success: function(data) {
                if (data.d != '') {
                    $('#msgData').val(data.d);
                    if (childgridID == "__") { //FB 2989 Starts
                        if ((imgpath == "call" || imgpath == "delete") && (data.d == "Operation Successful" || data.d == "1" || data.d == "0")) {
                            operationSuccess();
                            $('#btnRefreshPage', window.parent.document).trigger('click');
                        } //FB 2989 End
                        else if (data.d == "1" || data.d == "0") {
                            $("#" + thisImg).attr("src", "../image/MonitorMCU/" + imgpath + "_" + data.d + ".gif");
                            operationSuccess();
                        }
                        else if (data.d == "Operation UnSuccessful")
                            operationError();
                        else
                            operationSuccess();
                    }
                    else if (childgridID != "__") { //FB 2989 Starts
                        if ((imgpath == "call" || imgpath == "delete") && (data.d == "Operation Successful" || data.d == "1" || data.d == "0")) {
                            operationSuccess();
                            $('#btnRefreshPage', window.parent.document).trigger('click');
                        } //FB 2989 End
                        else if (data.d == "1" || data.d == "0" && imgpath != "call") {
                            $("#" + thisImg).attr("src", "../image/MonitorMCU/" + imgpath + "_" + data.d + ".gif");
                            operationSuccess();
                        }
                        else if (data.d == "Operation UnSuccessful")
                            operationError();
                        else
                            operationSuccess();
                    }
                }
            },
            error: function(result) {
                $('#errormsgbox').html("Operation UnSuccessful");
                $('#errormsgbox').fadeIn(5000, function() {
                    $('#errormsgbox').fadeOut(5000);
                });
            }
        });
        $('#communStatus', window.parent.document).val("0");
        if (imgpath == 'delete' && $('#msgData').val() == 'Operation Successful')
            $('#btnRefreshPage', window.parent.document).trigger('click');
        $('#popupdiv').fadeOut();
    });
    //------------------------------------------------------------------------------------------                                            

    // ------------------------ popup cancel ---------------------------------------------------    
    $('#btnpopupcancel').click(function() {
        $('#popmsg').fadeOut();
        $('#popupdiv').fadeOut();
        $('#communStatus', window.parent.document).val("0");
    });
    // ------------------------------------------------------------------------------------------   

    // ------------------ Popup window MessageBox Validation -----------------------------------        
    $("#popuptxt").keypress(function() {
        var username_length;
        username_length = $("#popuptxt").val().length;
        $("#username_warning").empty();
        var textbal = parseInt(username_length) - parseInt(250);
        if (username_length < 250) {
            $("#username_warning").append("<b>Characters Left:</b><b style='color:green;'>" + parseInt(textbal) + "</b>");
            return true;
        }
        else if (username_length == 250) {
            $("#username_warning").append("<b>Characters Left:</b><b style='color:red;'>" + parseInt(textbal) + "</b>");
            //            if(keycode == 8 || keycode == 46) // backspace || Delete                                      
            //                return true;            
            return false;
        }
    });

    //    $('#msgMinutes').keypress(function(e) {
    //    var keyCode = e.keyCode;
    //    alert(keyCode);
    //    if (e.keyCode >= 49 && e.keyCode <= 57) {}
    //            else {
    //                    if (e.keyCode >= 97 && e.keyCode <= 122) 
    //                    {
    //                        alert('Error');
    //                        // return false;
    //                    }
    //                     else return false;
    //                }
    //    });

    //------------------------------------------------------------------------------------------     

    // ---------------------------------- Popup submit -----------------------------------------    
    $("#btnpopupSubmit").click(function() {
        var id = $("#popupstatus").val();
        $("#username_warning").val('');
        var username_length = $("#popuptxt").val().length;
        var camUrl;
        var dataParameter;
        var confID = $('#msgPopupIdentificationID').val();
        var msgIdentificationID = confID.match("__", 'g');
        if (id == "MsgOn") {
            var fnIdentification = "";
            if (msgIdentificationID == "__") {
                fnIdentification = "particpant";
                var MCUBridgeID = $('#mID' + confID).val();
                dataParameter = JSON.stringify({ userID: $('#partUserId' + confID).val(), confID: $('#confId' + confID).val(), endpointID: $('#partEndpointID' + confID).val(), terminalType: $('#partTerminalType' + confID).val(), Message: $('#popuptxt').val(), Direction: $('select#direction').val(), Duration: $('#msgSec').val(), fnIdentification: fnIdentification, MCUBridgeID: MCUBridgeID });//FB 2981
            }
            else {
                fnIdentification = "conference";
                var MCUBridgeID = $('#McuBridgeID' + confID).val();
                dataParameter = JSON.stringify({ userID: $('#conUserId' + confID).val(), confID: $('#conID' + confID).val(), endpointID: "", terminalType: "", Message: $('#popuptxt').val(), Direction: $('select#direction').val(), Duration: $('#msgSec').val(), fnIdentification: fnIdentification, MCUBridgeID: MCUBridgeID }); //FB 2981
            }
            camUrl = "MonitorMCU.aspx/fnSaveComments";
            if (username_length == 0) {
                $("#username_warning").append("<b style='color:Red;'>Please Enter the Comments</b>");
                $('#popmsg').show();
                return false;
            }
        }
        else if (id == "CamreraOn") {
            if (msgIdentificationID == "__") {
                fnIdentification = "particpant";
                dataParameter = JSON.stringify({ userID: $('#partUserId' + confID).val(), confID: $('#confId' + confID).val(), endpointID: $('#partEndpointID' + confID).val(), terminalType: $('#partTerminalType' + confID).val(), direction: $('select#camDirection').val(), fnIdentification: fnIdentification });
            }
            else {
                fnIdentification = "conference";
                dataParameter = JSON.stringify({ userID: $('#conUserId' + confID).val(), confID: $('#conID' + confID).val(), endpointID: "", terminalType: "", direction: $('select#camDirection').val(), fnIdentification: fnIdentification });
            }
            camUrl = "MonitorMCU.aspx/fnCameraDirection";
        }
        else if (id == "timeOn") {
            dataParameter = JSON.stringify({ userID: $('#conUserId' + confID).val(), confID: $('#conID' + confID).val(), Duration: $('#msgMinutes').val() });
            camUrl = "MonitorMCU.aspx/fnTimeExpand";
        }
        $('#popmsg').fadeOut();
        $.ajax({
            type: "POST",
            contentType: "application/json; charset=utf-8",
            url: camUrl,
            data: dataParameter,
            dataType: "json",
            success: function(data) {
                if (data.d != '') {
                    $('#popmsg').fadeOut();
                    $('#popupdiv').fadeOut();
                    if (data.d == 'Operation Successful') {
                        $('#successbox').html(data.d);
                        $('#successbox').fadeIn(5500, function() {
                            $('#successbox').fadeOut(5500);
                        });
                    }
                    else {
                        $('#errormsgbox').html(data.d);
                        $('#errormsgbox').fadeIn(5500, function() {
                            $('#errormsgbox').fadeOut(5500);
                        });
                    }
                }
            },
            error: function(result) {
                $('#popupdiv').fadeOut();
                $('#errormsgbox').html("Operation UnSuccessful");
                $('#errormsgbox').fadeIn(1500, function() {
                    $('#errormsgbox').fadeOut(1500);
                });
            }
        });
        $('#communStatus', window.parent.document).val("0");
        $('#msgPopupIdentificationID').val("");
    });
    // -------------------------------------------------------------------------------------------						             



    // -----------------------Bandwith Popup -----------------------------------------------------
    $('.bandwidthmeter').click(function() {
        $('#popupdiv').fadeIn();
        $('#communStatus', window.parent.document).val("1");
        var id = $(this).attr("id").replace('meter', '');
        var params = { userID: $('#partUserId' + id).val(), confID: $('#confId' + id).val(), EndpointID: $('#partEndpointID' + id).val(), terminalType: $('#partTerminalType' + id).val() };
        dataParameter = JSON.stringify(params);
        $.ajax({
            type: "POST",
            contentType: "application/json; charset=utf-8",
            url: "MonitorMCU.aspx/PacketDetails",
            data: dataParameter,
            dataType: "json",
            success: function(data) {
                if (data.d != '') {
                    var PacketDetailsContent = data.d;

                    var A1 = PacketDetailsContent.indexOf('AudioPacketReceived_');
                    var B1 = PacketDetailsContent.indexOf('_AudioPacketError_');
                    var AudioPacketReceived = (PacketDetailsContent.slice(A1, B1)).replace('AudioPacketReceived_', '');

                    var A2 = PacketDetailsContent.indexOf('_AudioPacketError_');
                    var B2 = PacketDetailsContent.indexOf('_AudioPacketMissing_');
                    var AudioPacketError = (PacketDetailsContent.slice(A2, B2)).replace('_AudioPacketError_', '');

                    var A3 = PacketDetailsContent.indexOf('_AudioPacketMissing_');
                    var B3 = PacketDetailsContent.indexOf('_VideoPacketError_');
                    var AudioPacketMissing = (PacketDetailsContent.slice(A3, B3)).replace('_AudioPacketMissing_', '');

                    var A4 = PacketDetailsContent.indexOf('_VideoPacketError_');
                    var B4 = PacketDetailsContent.indexOf('_VideoPacketReceived_');
                    var VideoPacketError = (PacketDetailsContent.slice(A4, B4)).replace('_VideoPacketError_', '');

                    var A5 = PacketDetailsContent.indexOf('_VideoPacketReceived_');
                    var B5 = PacketDetailsContent.indexOf('_VideoPacketMissing_');
                    var VideoPacketReceived = (PacketDetailsContent.slice(A5, B5)).replace('_VideoPacketReceived_', '');

                    var VideoPacketMissing = (PacketDetailsContent.substr(PacketDetailsContent.indexOf("_VideoPacketMissing_") + 1)).replace('VideoPacketMissing_', '');

                    $('#AudioPacketReceived').html(AudioPacketReceived);
                    $('#AudioPacketError').html(AudioPacketError);
                    $('#AudioPacketMissing').html(AudioPacketMissing);
                    $('#VideoPacketError').html(VideoPacketError);
                    $('#VideoPacketReceived').html(VideoPacketReceived);
                    $('#VideoPacketMissing').html(VideoPacketMissing);

                    $('#BandWidth').bPopup({
                        fadeSpeed: 'slow',
                        followSpeed: 1500,
                        modalColor: 'gray'
                    });
                }
            },
            error: function(result) {
                $('#popupdiv').fadeOut();
                $('#errormsgbox').html("Operation UnSuccessful");
                $('#errormsgbox').fadeIn(1500, function() {
                    $('#errormsgbox').fadeOut(1500);
                });
            }
        });
    });

    $('#Cancelbandwidth').click(function() {
        $('#BandWidth').fadeOut();
        $('#popupdiv').fadeOut();
        $('#communStatus', window.parent.document).val("0");
    });

    // -------------------------------------------------------------------------------------------

    // ------------------------ Event Log Submit FB 2501 Dec7 Start ------------------------------------------------
    $('.EventLog').click(function() {
        $('#communStatus', window.parent.document).val("1");
        var imgId = $(this).attr('id');
        imgId = imgId.replace('eventLog', '');
        var dataParam = { userID: $('#userID').val(), confID: $('#conID' + imgId).val() };

        var dataParameter = JSON.stringify(dataParam);
        var camUrl = "MonitorMCU.aspx/fnGetEventLogs";
        $.ajax({
            type: "POST",
            contentType: "application/json; charset=utf-8",
            url: camUrl,
            data: dataParameter,
            dataType: "json",
            success: function(data) {
                if (data.d != '') {
                    $('#popupdiv').fadeIn();
                    $('#EventLogHtmlContent').html(data.d);
                    $('#diveventlog').show();
                    $("#diveventlog").bPopup({
                        fadeSpeed: 'slow',
                        followSpeed: 1500,
                        modalColor: 'gray'
                    });
                }
                else {
                    $('#popupdiv').fadeIn();
                    $('#diveventlog').show();
                    $("#diveventlog").bPopup({
                        fadeSpeed: 'slow',
                        followSpeed: 1500,
                        modalColor: 'gray'
                    });
                }
            },
            error: function(result) {
                $('#popupdiv').fadeOut();
                operationError();
            }
        });
        $('#msgPopupIdentificationID').val("");
    });
    // -----------------------------------------------------------------------------------------
    // ------------------------ btnCancelEventLog ------------------------------------------------    
    $('#btnCancelEventLog').click(function() {
        $('#popupdiv').fadeOut();
        $('#diveventlog').fadeOut();
        $('#communStatus', window.parent.document).val("0");
    });
    // -----------------------------------------------------------------------------------------

    // ------------------------ Event Log Submit FB 2501 Dec7 End ------------------------------------------------
});
// Document Ready


// ---------------------------------- Popup Window -----------------------------------------     
function OpenPopupWindow(imgpath, thisImg) {
    if (imgpath == "message") {
        $('#eTime').hide();
        $('#eCamera').hide();
        $('#smsg').show();
        $('#communStatus', window.parent.document).val("1");
        $('#msgPopupIdentificationID').val(thisImg);
        $('#popuptxt').val("");
        $('.pMsg').show();
        $('.pMsgDuration').hide(); //FB 2981
        $('.pSendMsgDuration').show();
        $('.pCamera').hide();
        $('#tblpopup').css('height', "320px");
        $('#popmsg').css('height', "320px");
        $('#btnpopupSubmit').html('Send');
        $("#popupstatus").val("MsgOn");
        $('#popmsg').show();
        $("#popmsg").bPopup({
            fadeSpeed: 'slow',
            followSpeed: 1500,
            modalColor: 'gray'
        });
        return false;
    }
    else if (imgpath == "camera") {
        $('#smsg').hide();
        $('#eTime').hide();
        $('#eCamera').show();
        $('#communStatus', window.parent.document).val("1");
        $('#msgPopupIdentificationID').val(thisImg);
        $('.pMsg').hide();
        $('.pMsgDuration').hide();
        $('.pSendMsgDuration').hide(); //FB 2981
        $('.pCamera').show();
        $('#tblpopup').css('height', "170px");
        $('#popmsg').css('height', "170px");
        $("#popupstatus").val("CamreraOn");
        $('#btnpopupSubmit').html('Submit');
        $('#popmsg').show();
        $("#popmsg").bPopup({
            fadeSpeed: 'slow',
            followSpeed: 1500,
            modalColor: 'gray'
        });
        return false;
    }
    else if (imgpath == "time") {
        $('#smsg').hide();
        $('#eCamera').hide();
        $('#eTime').show();
        $('#communStatus', window.parent.document).val("1");
        $('#msgPopupIdentificationID').val(thisImg);
        $('.pMsg').hide();
        $('.pCamera').hide();
        $('.pMsgDuration').show();
        $('.pSendMsgDuration').hide(); //FB 2981
        $('#tblpopup').css('height', "170px");
        $('#popmsg').css('height', "170px");
        $("#popupstatus").val("timeOn");
        $('#btnpopupSubmit').html('Submit');
        $('#popmsg').show();
        $("#popmsg").bPopup({
            fadeSpeed: 'slow',
            followSpeed: 1500,
            modalColor: 'gray'
        });
        return false;
    }
}
