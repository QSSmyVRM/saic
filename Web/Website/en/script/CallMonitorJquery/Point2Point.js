﻿/*ZD 100147 Start*/
/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
/*ZD 100147 End*/

$(document).ready(function() {

        // FB 3016 Starts
        function settimer(j, Duarion) {
            var countdown = {
                startInterval: function() {
                    var TotalTime = Duarion;
                    Duarion = Duarion.substr(Duarion.length - 2).replace('0', '');
                    var count = Duarion;
                    var tHr = TotalTime.substr(0, 2);
                    var tMin = TotalTime.substr(3, 2);
                    //FB 2877 - Start
                    var currentId = setInterval(function() {
                        //FB 2501 Dec5 Start
                        if (count >= 0) {
                            if (tMin <= 0) {
                                if (tHr != 0 && count == 0) {

                                    tHr = tHr - 1;
                                    tMin = 59;
                                    count = 60;
                                }
                            }
                            $("#coundowntime" + i + '' + j).html(tHr + ':' + tMin + ':' + count);
                        }
                        if (count <= 0) {
                            if (count == 0 && tHr == 0 && tMin == 0) {
                                tMin = tMin;
                                count = count;
                            }
                            else {
                                tMin = tMin - 1;
                                count = 60;
                            }

                            if (tMin < 0) {
                                if (tHr != 0 && count == 0) {
                                    tHr = tHr - 1;
                                    tMin = 59;
                                    count = 60;
                                }
                            }
                            $("#coundowntime" + i + '' + j).html(tHr + ':' + tMin + ':' + count);
                        }
                        //FB 2501 Dec5 End
                        //FB 2877 - End
                        else
                            --count;
                    }, 1000);
                    countdown.intervalId = currentId;
                }
            };
            countdown.startInterval();
        }

        var pageid = window.top.location.href; //FB 2588
        var pathname = window.location.pathname;
        if (pathname.match("point2point") != null || pageid.match("P2P") != null) {
            //--------------------------------------- Timer Control Tamil1 -----------------------------------
            var Duarion;
            // for (var i = 1; i <= $('#TotalMcuCount').val(); i++) {    
            for (var j = 1; j <= $('#confTotalCount').val(); j++) {
                Duarion = $('#hdntime' + j).val();
                settimer(j, Duarion);
                //   }
            }
        // FB 3016 Ends
        
        // --------------------------------------------------------------------------------------------------


        $('.changeHost').click(function() {
            var confIndex = $(this).attr('id').replace('addUser', ''); //FB 2646 Starts
            $('#hdnconforgID').val($('#conforgID' + confIndex).val());
            var url = "RoomSearch.aspx?rmsframe=''&confID=" + $(this).attr('name') + "&stDate=''&enDate=''&tzone=26&serType=-1&hf=1" + "&isVMR=0&immediate=0&pageID=P2P&conforgID=" + $('#conforgID' + confIndex).val();  //FB 2646
            window.open(url, "RoomSearch", "width=" + screen.availWidth + ",height=650px,resizable=no,scrollbars=yes,status=no,top=0,left=0"); //FB 2646 Ends
            $('#CallmonitorPageID').html("CallMonitor"); //FB 2596
            $("#communStatus", window.parent.document).val("1");
            return true;
        });


        $('.altShort2BlueButtonFormat').click(function() {     //FB 2596
            if ($('#CallmonitorPageID', window.opener.document) != null)//FB 2645 Feb 21
            {
                var pageID = $('#CallmonitorPageID', window.opener.document).val();
                if (pageID == "CallMonitor") {
                    var conferenceID = getQueryString('confID');
                    var cType = $('#callerOrCalle' + conferenceID + "__00", window.opener.document).val();
                    if (cType == "Caller")
                        id = "0";
                    else
                        id = "1";
                    var NewePoint = $('#selectedlocframe', window.parent.document).val();
                    NewePoint = NewePoint.split(",")[1];
                    var dataParameter = JSON.stringify({ userID: $('#userID', window.opener.document).val(), confID: $('#conID' + conferenceID, window.opener.document).val(), OldEndpointID: $('#partEndpointID' + conferenceID + "__0" + id, window.opener.document).val(), NewEndpointID: NewePoint, terminalType: $('#partTerminalType' + conferenceID + '__0' + id, window.opener.document).val(), callStatus: $('#partTerminalTypeinfo' + conferenceID + '__0' + id, window.opener.document).val() });
                    $.ajax({
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        url: 'Point2Point.aspx/fnChangeHostDetails',
                        data: dataParameter,
                        dataType: "json",
                        success: function(data) {
                            if (data.d != '')
                                operationSuccess();
                            else
                                operationError();
                        },
                        error: function(result) {
                            operationError();
                        }
                    });
                    $('#popupdiv', window.parent.document).fadeOut(); //FB 3013
                }
                else
                    return true;
                $('#communStatus', window.opener.document).val('0');
            } //FB 2645 Feb 21
        });

        function getQueryString(par) {
            par = par + '=';
            var url = window.location.href;
            var splited = url.split(par);
            var extracted = splited[1].split('&');
            return extracted[0];
        }


        function operationSuccess() {
            $('#successbox', window.parent.document).html("Operation Successful");
            $('#successbox', window.parent.document).fadeIn(1000, function() {
                $('#successbox', window.parent.document).fadeOut(3000);
            });
        }

        function operationError() {
            $('#errormsgbox', window.parent.document).html("Operation UnSuccessful");
            $('#errormsgbox', window.parent.document).fadeIn(5000, function() {
                $('#errormsgbox', window.parent.document).fadeOut(5000);
            });
        }

        // -------------- All Grid - Image Events are call this Function ----------------------------------
        $(".classPoint2Point").live('click', function() {

            $('#communStatus', window.parent.document).val("1");
            var thisImg = $(this).attr("id");
            var imgpath = $(this).attr("src");
            var fnaccessbackend;
            var dataParameter;
            imgpath = imgpath.replace('../image/MonitorMCU/', '');
            imgpath = imgpath.split('_', 1)[0];
            if (imgpath == "TerminalStaus")
                return false;
            var childgridID = thisImg.match("__", 'g');
            var thisImgid = thisImg.replace(imgpath, '');
            var fnmethod = "";
            if (childgridID == "__" && imgpath == "call") {
                fnaccessbackend = 'Point2Point.aspx/fnconnectOrDisconnect';
                var Istatus = $(this).attr("src");
                Istatus = Istatus.replace('.gif', '');
                Istatus = Istatus.substr(Istatus.length - 1);
                fnmethod = "particpant";
                dataParameter = JSON.stringify({ userID: $('#userID').val(), confID: $('#confID' + thisImgid).val(), EndpointID: $('#partEndpointID' + thisImgid).val(), terminalType: $('#partTerminalType' + thisImgid).val(), ImgStatus: Istatus, fnidentification: imgpath, fnmethod: fnmethod });
            }
            else {
                if (imgpath == "message" || imgpath == "time" || imgpath == "graphView" || imgpath == "bandWidth" || imgpath == "eventLog") {
                    $('#popupdiv').fadeIn();
                    OpenPopupWindow(imgpath, thisImgid);
                    
                    return false;
                }
                else {
                    if (imgpath == "call") {
                        fnaccessbackend = 'Point2Point.aspx/fnconnectOrDisconnect';
                        var Istatus = $(this).attr("src");
                        Istatus = Istatus.replace('.gif', '');
                        Istatus = Istatus.substr(Istatus.length - 1);
                        fnmethod = "conference";
                        var id;
                        var cType = $('#callerOrCalle' + thisImgid + "__0").val();
                        if (cType == "Caller")
                            id = "0";
                        else
                            id = "1";
                        dataParameter = JSON.stringify({ userID: $('#userID').val(), confID: $('#conID' + thisImgid).val(), EndpointID: $('#partEndpointID' + thisImgid + "__" + id).val(), terminalType: $('#partTerminalType' + thisImgid + "__" + id).val(), ImgStatus: Istatus, fnidentification: imgpath, fnmethod: fnmethod });

                    }
                    if (imgpath == "delete") {
                        $('#popupdiv').fadeIn();
                        $("#progressdivwindow").bPopup({
                            fadeSpeed: 'fast',
                            followSpeed: 50,
                            modalColor: 'gray'
                        });
                        fnmethod = "conference";
                        fnaccessbackend = 'Point2Point.aspx/fndelete';
                        dataParameter = JSON.stringify({ userID: $('#userID').val(), confID: $('#conID' + thisImgid).val(), EndpointID: "", terminalType: "", ImgStatus: "", fnmethod: fnmethod });

                    }
                }
            }
            $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: fnaccessbackend,
                data: dataParameter,
                dataType: "json",
                success: function(data) {
                    if (data.d != '') {
                        if (childgridID == "__") {
                            if (data.d == "1" || data.d == "0") {
                                $("#" + thisImg).attr("src", "../image/MonitorMCU/" + imgpath + "_" + data.d + ".gif");
                                operationSuccess();
                            }
                            else if (data.d == "Operation UnSuccessful") {
                                $('#hdnmsg').val(data.d);
                                operationError();
                            }
                            else {
                                operationSuccess();
                                $('#btnRefreshPage', window.parent.document).trigger('click');
                            }
                        }
                        else if (childgridID != "__") {
                            if (data.d == "Operation UnSuccessful")
                                operationError();
                            else {
                                operationSuccess();
                                $('#btnRefreshPage', window.parent.document).trigger('click');
                            }
                        }
                    }
                },
                error: function(result) {
                    operationError();
                }
            });
            $('#communStatus', window.parent.document).val("0");
            if (imgpath == 'delete' && ($('#hdnmsg').val() == "Operation Successful"))
                $('#btnRefreshPage', window.parent.document).trigger('click');
            $('#popupdiv',window.parent.document).fadeOut();//FB 3013
        });
        //------------------------------------------------------------------------------------------                                            

        //--------------------------------------- Tooltip Contents ---------------------------------
        $(".classPoint2Point").mouseover(function() {
            var imgpath = $(this).attr('src');
            var imgID = $(this).attr('id');
            imgpath = imgpath.replace('../image/MonitorMCU/', '');
            imgpath = imgpath.replace('.gif', '');
            // Info image status
            if (imgpath == "TerminalStaus_0")
                $(this).attr('title', 'DisConnected')
            else if (imgpath == "TerminalStaus_1")
                $(this).attr('title', 'Connecting')
            else if (imgpath == "TerminalStaus_2")
                $(this).attr('title', 'Connected')
            else if (imgpath == "TerminalStaus_3")
                $(this).attr('title', 'Online')
            // Call
            else if (imgpath == "call_1") {
                var childgridID = imgID.match("__", 'g');
                if (childgridID == "__")
                    $(this).attr('title', 'Connect')
            }
            else if (imgpath == "call_0") {
                var childgridID = imgID.match("__", 'g');
                if (childgridID == "__")
                    $(this).attr('title', 'DisConnect')
            }
        });
        // ------------------------------------------------------------------------------------------


        // ---------------------------------- Popup submit -----------------------------------------    
        $("#btnpopupSubmit").click(function() {
            var id = $("#popupstatus").val();
            $("#username_warning").val('');
            var username_length = $("#popuptxt").val().length;
            var camUrl;
            var dataParameter;
            var confID = $('#msgPopupIdentificationID').val();
            var msgIdentificationID = confID.match("__", 'g');
            if (id == "MsgOn") {
                dataParameter = JSON.stringify({ userID: $('#userID').val(), confID: $('#conID' + confID).val(), Message: $('#popuptxt').val() });
                camUrl = "Point2Point.aspx/fnSaveComments";
                if (username_length == 0) {
                    $("#username_warning").append("<b style='color:Red;'>Please Enter the Comments</b>");
                    $('#popmsg').show();
                    return false;
                }
            }
            else if (id == "timeOn") {
                dataParameter = JSON.stringify({ userID: $('#userID').val(), confID: $('#conID' + confID).val(), Duration: $('#msgMinutes').val() });
                camUrl = "Point2Point.aspx/fnTimeExpand";
            }

            $('#popmsg', window.parent.document).fadeOut(); //FB 3013
            $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: camUrl,
                data: dataParameter,
                dataType: "json",
                success: function(data) {
                    if (data.d != '') {
                        $('#popmsg', window.parent.document).fadeOut(); //FB 3013
                        $('#popupdiv', window.parent.document).fadeOut(); //FB 3013
                        if (data.d == 'Operation Successful')
                            operationSuccess();
                        else
                            operationError();
                    }
                },
                error: function(result) {
                $('#popupdiv', window.parent.document).fadeOut(); //FB 3013
                    operationError();
                }
            });
            $('#communStatus', window.parent.document).val("0");
            $('#msgPopupIdentificationID').val("");
            $('#popupdiv', window.parent.document).fadeOut(); //FB 3013
        });
        // -------------------------------------------------------------------------------------------						             


        // ------------------------ Event Log Submit ------------------------------------------------       
        $('.EventLog').click(function() {
            $('#communStatus', window.parent.document).val("1");
            var imgId = $(this).attr('id');
            imgId = imgId.replace('eventLog', '');
            var dataParam = { userID: $('#userID').val(), confID: $('#conID' + imgId).val() };
            var dataParameter = JSON.stringify(dataParam);
            var camUrl = "Point2Point.aspx/fnGetEventLogs";
            $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: camUrl,
                data: dataParameter,
                dataType: "json",
                success: function(data) {
                    if (data.d != '') {

                        $('#EventLogHtmlContent').html(data.d);
                        $('#diveventlog').show();
                        $("#diveventlog").bPopup({
                            fadeSpeed: 'slow',
                            followSpeed: 1500,
                            modalColor: 'gray'
                        });
                    }
                    else {
                        $('#diveventlog').show();
                        $("#diveventlog").bPopup({
                            fadeSpeed: 'slow',
                            followSpeed: 1500,
                            modalColor: 'gray'
                        });
                    }
                },
                error: function(result) {
                $('#popupdiv', window.parent.document).fadeOut(); //FB 3013
                    operationError();
                }
            });
            $('#msgPopupIdentificationID').val("");
        });
        // -----------------------------------------------------------------------------------------   


        // ------------------------ btnCancelEventLog ------------------------------------------------    
        $('#btnCancelEventLog').click(function() {
        $('#popupdiv', window.parent.document).fadeOut(); //FB 3013
        $('#diveventlog', window.parent.document).fadeOut(); //FB 3013
            $('#communStatus', window.parent.document).val("0");
        });
        // -----------------------------------------------------------------------------------------   


        // ------------------------ Bandwidth Submit ------------------------------------------------       
        $('#Submitbandwidth').click(function() {
            alert("Please wait, Terminateing your existing endpoint and connect with new endpoint");

            var tType = "";
            var EID = "";
            var confID = $('#msgPopupIdentificationID').val();
            if ($('#callerOrCalle' + confID + "__0" + "0").val() == 'Caller') {
                tType = $('#partTerminalType' + confID + "__0" + "0").val();
                EID = $('#partEndpointID' + confID + "__0" + "0").val();
            }
            else {
                tType = $('#partTerminalType' + confID + "__0" + "1").val();
                EID = $('#partEndpointID' + confID + "__0" + "1").val();
            }
            var lineRate = $("#lstLineRate option:selected").val();
            if (lineRate == "-1") {
                $('#msglinerate').val('Required');
                return false;
            }
            var dataParameter = JSON.stringify({ userID: $('#userID').val(), confID: $('#conID' + confID).val(), EndpointID: EID, terminalType: tType, Linerate: lineRate });
            var camUrl = "Point2Point.aspx/fnChangeLineRate";

            $('#BandWidth', window.parent.document).fadeOut(); //FB 3013
            $('#popupdiv', window.parent.document).fadeOut(); //FB 3013
            $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: camUrl,
                data: dataParameter,
                dataType: "json",
                success: function(data) {
                    if (data.d != '') {
                        $('#BandWidth', window.parent.document).fadeOut(); //FB 3013
                        $('#popupdiv', window.parent.document).fadeOut(); //FB 3013
                        if (data.d == 'Operation Successful')
                            operationSuccess();
                        else
                            operationError();
                    }
                },
                error: function(result) {
                    //$('#popupdiv').fadeOut();
                    operationError();
                }
            });
            $('#communStatus', window.parent.document).val("0");
            $('#msgPopupIdentificationID').val("");
        });
        // -----------------------------------------------------------------------------------------   


        // ------------------------ Cancelbandwidth ------------------------------------------------    
        $('#Cancelbandwidth').click(function() {
        $('#popupdiv', window.parent.document).fadeOut(); //FB 3013
        $('#BandWidth', window.parent.document).fadeOut(); //FB 3013
            $('#communStatus', window.parent.document).val("0");
        });
        // -----------------------------------------------------------------------------------------   


        // ------------------------ popup cancel ---------------------------------------------------    
        $('#btnpopupcancel').click(function() {
        $('#popupdiv', window.parent.document).fadeOut(); //FB 3013
        $('#popmsg', window.parent.document).fadeOut(); //FB 3013
            $('#communStatus', window.parent.document).val("0");
        });
        // -----------------------------------------------------------------------------------------   

        // ------------------------ Graphical View Cancel ---------------------------------------------    
        $('#btnGrpview').click(function() {
        $('#popupdiv', window.parent.document).fadeOut(); //FB 3013
        $('#GraphicalView', window.parent.document).fadeOut(); //FB 3013
            $('#communStatus', window.parent.document).val("0");
        });
        // -----------------------------------------------------------------------------------------
    } //FB 2588

});



// ---------------------------------- Popup Window -----------------------------------------     
function OpenPopupWindow(imgpath, thisImg) {
    if (imgpath == "message") {
        $('#eTime').hide();
        $('#smsg').show();
        $('#communStatus', window.parent.document).val("1");
        $('#msgPopupIdentificationID').val(thisImg);
        $('#popuptxt').val("");
        $('.pMsg').show();
        $('.pMsgDuration').hide();
        $('#tblpopup').css('height', "200px");
        $('#popmsg').css('height', "200px");
        $('#btnpopupSubmit').html('Send');
        $("#popupstatus").val("MsgOn");
        $('#popmsg').show();
        $("#popmsg").bPopup({
            fadeSpeed: 'slow',
            followSpeed: 1500,
            modalColor: 'gray'
        });
        return false;
    }
    else if (imgpath == "time") {
        $('#smsg').hide();
        $('#eTime').show();
        $('#communStatus', window.parent.document).val("1");
        $('#msgPopupIdentificationID').val(thisImg);
        $('.pMsg').hide();
        $('.pMsgDuration').show();
        $('#btnpopupSubmit').html('Submit');
        $('#tblpopup').css('height', "170px");
        $('#popmsg').css('height', "170px");
        $("#popupstatus").val("timeOn");
        $('#popmsg').show();
        $("#popmsg").bPopup({
            fadeSpeed: 'slow',
            followSpeed: 1500,
            modalColor: 'gray'
        });
        return false;
    }
    else if (imgpath == "graphView") {
        $('#communStatus', window.parent.document).val("1");
        $('#msgPopupIdentificationID').val(thisImg);
        $('#gView').css('height', "450px");
        $('#popmsg').css('height', "680px");
        $("#popupstatus").val("graphView");
        var callerid = $('#partTerminalTypeinfo' + thisImg + "__" + "0").val();
        if (callerid == "1")
            $('#GraphicalImgID').attr('src', "../image/MonitorMCU/p2pconnect.jpg");
        else
            $('#GraphicalImgID').attr('src', "../image/MonitorMCU/p2pdisconnect.jpg");

        $('#GraphicalView').show();
        $("#GraphicalView").bPopup({
            fadeSpeed: 'slow',
            followSpeed: 1500,
            modalColor: 'gray'
        });
        return false;
    }
    else if (imgpath == "bandWidth") {
        $('#communStatus', window.parent.document).val("1");
        $('#msgPopupIdentificationID').val(thisImg);
        $("#popupstatus").val("bandWidth");
        $('#BandWidth').show();
        $("#BandWidth").bPopup({
            fadeSpeed: 'slow',
            followSpeed: 1500,
            modalColor: 'gray'
        });
       
        return false;
    }


}
// ------------------------------------------------------------------------------------------		

				


