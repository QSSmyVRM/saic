/*ZD 100147 Start*/
/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
/*ZD 100147 End*/
function movePosition(to) {
	var list = document.getElementById("BridgeList");
	var index = list.selectedIndex;
	var total = list.options.length-1;

	if (index == -1) return false;
	if (to == +1 && index == total) return false;
	if (to == -1 && index == 0) return false;

	var items = new Array;
	var values = new Array;

	for (i = total; i >= 0; i--) {
		items[i] = list.options[i].text;
		values[i] = list.options[i].value;
	}

	for (i = total; i >= 0; i--) {
		if (index == i) {
			list.options[i + to] = new Option(items[i],values[i], 0, 1);
			list.options[i] = new Option(items[i + to], values[i+to]);
			i--;
		}
		else {
			list.options[i] = new Option(items[i], values[i]);
		   }
	}
	list.focus();
}


function display_prompt(promptpicture, prompttitle, htmlText) 
{
	promptbox = document.createElement('div'); 
	promptbox.setAttribute ('id' , 'prompt');
	document.getElementsByTagName('body')[0].appendChild(promptbox);
	promptbox = eval("document.getElementById('prompt').style");

	getMouseXY();

	promptbox.position = 'absolute'
	//alert(window.screen.height + " : " + mousedownY);
	//if ((mousedownY + 200)>= window.screen.height)
	//    mousedownY -= 100;
	promptbox.top = mousedownY; 
	promptbox.left = mousedownX;
	promptbox.zIndex = 5;
	promptbox.width = 700
	promptbox.border = 'outset 0 #bbbbbb' 
	htmlText = htmlText.replace(/DISPLAY: none/gi, "display: ");
	//alert(htmlText);
	document.getElementById('prompt').innerHTML = htmlText;
	document.getElementById('prompt').style.display = "";
} 


function saveOrder(cb) 
{ 
	var list = document.getElementById("BridgeList");
	var neworder = "";
	
	for (i = 0; i < list.options.length; i++) {
		neworder += list.options[i].value + ";"
	}
	document.frmManagebridge.Bridges.value = neworder;
	frmsubmit('SAVE', '');
} 


function cancelthis()
{
	document.getElementsByTagName("body")[0].removeChild(document.getElementById("prompt"));
}