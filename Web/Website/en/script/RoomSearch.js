﻿/*ZD 100147 Start*/
/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
/*ZD 100147 End*/
function OpenRoomSearch()
{
        if(OpenRoomSearch.arguments != null)
        {
            var rmargs = OpenRoomSearch.arguments;
            var prnt = rmargs[0];
            var url = "";
            var locs = document.getElementById("selectedloc");
            var isTel = ""; //FB 2400 start
            //FB 2694
            var chkHotdeskin = document.getElementById("chkHotdesk");
            var type ="";
            
            if (document.getElementById("ddlConType"))
            {
                if (document.getElementById("ddlConType").value == "4")
                    isTel = "&isTel=";
            }
            //FB 2694
            if (document.getElementById("chkHotdesk") != null) {
                if (chkHotdeskin) {
                    if (chkHotdeskin.checked)
                        type = "h";
                }
            }

            url = "RoomSearch.aspx?rmsframe=" + locs.value + "&hf=1&type="+ type +"&frm=" + prnt + isTel; //frmCalendarRoom"; //FB 2400 end //FB 2694
            window.open(url, "RoomSearch", "width="+ screen. availWidth +",height=666px,resizable=no,scrollbars=yes,status=no,top=0,left=0");
        }
    
}

function Delroms()
{

var args = Delroms.arguments;
var locs =  document.getElementById("selectedloc");
roomIdsStr = locs.value.split(',');

    if(Loccontains(roomIdsStr,args[0]))
    {
        var i = roomIdsStr.length;
        
          while (i--) 
          {
            if (roomIdsStr[i] == args[0]) 
            {
               roomIdsStr[i] = ""; 
            }
            
         }
         
        i = roomIdsStr.length;
        locs.value = ""; 
         while (i--) {
            if (roomIdsStr[i] != "") 
            {
             if(locs.value == "")
                locs.value =roomIdsStr[i];
            else
                locs.value += ","+roomIdsStr[i];
            }
            }
            
             var lb  =  document.getElementById("RoomList");
            
            for (var i=lb.options.length-1; i>=0; i--){
                 if(lb.options[i].value == args[0])
                 {
                    lb.options[i] = null;
                 }
    
    
  }
      
    }
}
  
  function Loccontains(a, obj) {
  var i = a.length;  
  while (i--) {
    if (a[i] === obj) {
      return true;
    }
  }
  return false;
}

function AddRooms()
{

        var url = "";
        var locs = document.getElementById("locstrname");
        
        if(locs)
        {
            var rooms = locs.value.split('++'); //FB 1640
            var i = rooms.length;
            
            var lst  =  document.getElementById("RoomList");
            
            if(lst)
                clearlistbox(lst);
            
            while (i--) 
              {
                  if(rooms[i].split('|')[0] != "")
                  {
                    AddItem(rooms[i].split('|')[1],rooms[i].split('|')[0]);
                  }
                
             }
         }
    
}

function clearlistbox(lb){
  for (var i=lb.options.length-1; i>=0; i--){
    lb.options[i] = null;
  }
  lb.selectedIndex = -1;
}

function AddItem(Text,Value)
    {
        // Create an Option object        
        var opt = document.createElement("option");
        // Add an Option object to Drop Down/List Box
        var lst  =  document.getElementById("RoomList");
        
        if(lst)
        {
            lst.options.add(opt);
            // Assign text and value to Option object
            opt.text = Text;
            opt.value = Value;
        }

    }