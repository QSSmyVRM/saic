/*ZD 100147 Start*/
/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
/*ZD 100147 End*/
function ViewWorkorderDetails(woID, confID)
{
  	if (woID != "") {
		url = "ViewWorkorderDetails.aspx?woID=" + woID + "&confID=" + confID;	
		document.location.href = url;
		//myPopup = window.open(url,'wordorder','status=no,width=850,height=490,resizable=yes,scrollbars=yes');
		//myPopup.focus();
		//if (!myPopup.opener) {
		//	myPopup.opener = self;
		//}
	}
}

function ViewWorkorderDetails(woID, confID, ttype)
{
  	if (woID != "") {
		url = "ViewWorkorderDetails.aspx?woID=" + woID + "&confID=" + confID + "&t=" + ttype+ "&hf="	//COde added for WO bug
		document.location.href = url;
		//myPopup = window.open(url,'wordorder','status=no,width=850,height=490,resizable=yes,scrollbars=yes');
		//myPopup.focus();
		//if (!myPopup.opener) {
		//	myPopup.opener = self;
		//}
	}
}

//Added for WO bug
function ViewWorkorderDetailspopup(woID, confID, ttype, ispopup)
{
  	if (woID != "") {
		url = "ViewWorkorderDetails.aspx?woID=" + woID + "&confID=" + confID + "&t=" + ttype+ "&hf=1" ;	//COde added for WO bug
		document.location.href = url;
		
	}
}