/*ZD 100147 Start*/
/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
/*ZD 100147 End*/
var xmlHttp0 = "", xmlHttp1, xmlHttp2, xmlHttp3, xmlHttp4, xmlHttp5, xmlHttp6, xmlHttp7, xmlHttp8, xmlHttp9



function ajaxGetXML()
{
	var argv = ajaxGetXML.arguments;
	var argc = argv.length;
	var url = "";
	  
	if (argc < 3)
		return 0;
	else
		url = argv[1] + "?cmd=" + argv[2] + "&pno=" + (argc-3);
		
	  
	for (var i = 3; i < argc; i++)
		url += "&p" + (i-2) + "=" + argv[i];		// param id from 1
		
	switch (argv[0]) {
		case 0:
			xmlHttp0 = GetXmlHttpObject(stateChanged0);
			xmlHttp0.open("GET", url , true);
			xmlHttp0.send(null);
			break;
			
		case 1:
			xmlHttp1 = GetXmlHttpObject(stateChanged1);
			xmlHttp1.open("GET", url , true);
			xmlHttp1.send(null);
			break;
			
		case 2:
			xmlHttp2 = GetXmlHttpObject(stateChanged2);
			xmlHttp2.open("GET", url , true);
			xmlHttp2.send(null);
			break;
			
		case 3:
			xmlHttp3 = GetXmlHttpObject(stateChanged3);
			xmlHttp3.open("GET", url , true);
			xmlHttp3.send(null);
			break;
			
		case 4:
			xmlHttp4 = GetXmlHttpObject(stateChanged4);
			xmlHttp4.open("GET", url , true);
			xmlHttp4.send(null);
			break;
			
		case 5:
			xmlHttp5 = GetXmlHttpObject(stateChanged5);
			xmlHttp5.open("GET", url , true);
			xmlHttp5.send(null);
			break;
			
		case 6:
			xmlHttp6 = GetXmlHttpObject(stateChanged6);
			xmlHttp6.open("GET", url , true);
			xmlHttp6.send(null);
			break;
			
		case 7:
			xmlHttp7 = GetXmlHttpObject(stateChanged7);
			xmlHttp7.open("GET", url , true);
			xmlHttp7.send(null);
			break;
			
		case 8:
			xmlHttp8 = GetXmlHttpObject(stateChanged8);
			xmlHttp8.open("GET", url , true);
			xmlHttp8.send(null);
			break;
			
		case 9:
			xmlHttp9 = GetXmlHttpObject(stateChanged9);
			xmlHttp9.open("GET", url , true);
			xmlHttp9.send(null);
			break;
	}
} 


function stateChanged0() 
{
	if (xmlHttp0.readyState==4 || xmlHttp0.readyState=="complete") {
		if (ajaxSetXML) {
			ajaxSetXML (xmlHttp0.responseText)
		}
	} 
} 


function stateChanged1() 
{
	if (xmlHttp1.readyState==4 || xmlHttp1.readyState=="complete") {
		if (ajaxSetXML)
			ajaxSetXML (xmlHttp1.responseText)
	} 
} 

function stateChanged2() 
{
	if (xmlHttp2.readyState==4 || xmlHttp2.readyState=="complete") {
		if (ajaxSetXML)
			ajaxSetXML (xmlHttp2.responseText)
	} 
} 


function stateChanged3() 
{
	if (xmlHttp3.readyState==4 || xmlHttp3.readyState=="complete") {
		if (ajaxSetXML)
			ajaxSetXML (xmlHttp3.responseText)
	} 
} 


function stateChanged4() 
{
	if (xmlHttp4.readyState==4 || xmlHttp4.readyState=="complete") {
		if (ajaxSetXML)
			ajaxSetXML (xmlHttp4.responseText)
	} 
} 


function stateChanged5() 
{
	if (xmlHttp5.readyState==4 || xmlHttp5.readyState=="complete") {
		if (ajaxSetXML)
			ajaxSetXML (xmlHttp5.responseText)
	} 
} 

function stateChanged6() 
{
	if (xmlHttp6.readyState==4 || xmlHttp6.readyState=="complete") {
		if (ajaxSetXML)
			ajaxSetXML (xmlHttp6.responseText)
	} 
} 

function stateChanged7() 
{
	if (xmlHttp7.readyState==4 || xmlHttp7.readyState=="complete") {
		if (ajaxSetXML)
			ajaxSetXML (xmlHttp7.responseText)
	} 
} 

function stateChanged8() 
{
	if (xmlHttp8.readyState==4 || xmlHttp8.readyState=="complete") {
		if (ajaxSetXML)
			ajaxSetXML (xmlHttp8.responseText)
	} 
} 

function stateChanged9() 
{
	if (xmlHttp9.readyState==4 || xmlHttp9.readyState=="complete") {
		if (ajaxSetXML)
			ajaxSetXML (xmlHttp9.responseText)
	} 
} 


function GetXmlHttpObject(handler)
{ 
	var objXmlHttp=null

	if (navigator.userAgent.indexOf("Opera") >= 0) {
		alert("This example doesn't work in Opera") 
		return; 
	}
	
	if (navigator.userAgent.indexOf("MSIE")>=0) {
		var strName="Msxml2.XMLHTTP"
		if (navigator.appVersion.indexOf("MSIE 5.5")>=0) {
			strName="Microsoft.XMLHTTP"
		} 
		try { 
			objXmlHttp=new ActiveXObject(strName)
			objXmlHttp.onreadystatechange=handler 
			return objXmlHttp
		} catch(e) { 
			alert("Error. Scripting for ActiveX might be disabled") 
			return 
		} 
	} 
	
	if (navigator.userAgent.indexOf("Mozilla") >= 0) {
		objXmlHttp=new XMLHttpRequest()
		objXmlHttp.onload=handler
		objXmlHttp.onerror=handler 
		return objXmlHttp
	}
} 