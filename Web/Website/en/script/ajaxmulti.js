/*ZD 100147 Start*/
/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
/*ZD 100147 End*/
var xmlHttp = new Array();
var cbfuncname = ""


// 0: handle id; 1: call COM file; 2: post-function; 3..: call COM params
function ajaxGetXML()
{
	var argv = ajaxGetXML.arguments;
	var argc = argv.length;
	var url = "";
	  
	if (argc < 3)
		return 0;
	else
		url = argv[1] + ( (argv[1].indexOf("?") == -1) ? "?" : "&" ) + "ajax=1&pno=" + (argc-3);
		
	  
	for (var i = 3; i < argc; i++)
		url += "&p" + (i-2) + "=" + argv[i];

	cbfuncname = argv[2];

//itest.location.href=url

	xmlHttp[argv[0]] = GetXmlHttpObject(argv[0]);
	xmlHttp[argv[0]].open("GET", url , true);
	xmlHttp[argv[0]].send(null);
} 



function GetXmlHttpObject(handlerid)
{ 
	var objXmlHttp=null

	if (navigator.userAgent.indexOf("Opera") >= 0) {
		alert("This example doesn't work in Opera") 
		return; 
	}
	
	if (navigator.userAgent.indexOf("MSIE")>=0) {
		var strName="Msxml2.XMLHTTP"
		if (navigator.appVersion.indexOf("MSIE 5.5")>=0) {
			strName="Microsoft.XMLHTTP"
		} 
		try { 
			objXmlHttp=new ActiveXObject(strName)
			objXmlHttp.onreadystatechange = function() {
				if (xmlHttp[handlerid].readyState==4 || xmlHttp[handlerid].readyState=="complete") {
					if (typeof(eval(cbfuncname))!="undefined")
						eval(cbfuncname) (xmlHttp[handlerid].responseText)

				} 
			} 

			return objXmlHttp
		} catch(e) { 
			alert("Error. Scripting for ActiveX might be disabled") 
			return 
		} 
	} 
	
	
	if (navigator.userAgent.indexOf("Mozilla") >= 0) {
		objXmlHttp=new XMLHttpRequest()
		
		objXmlHttp.onload = function() {
			if (xmlHttp[handlerid].readyState==4 || xmlHttp[handlerid].readyState=="complete") {
				if (typeof(eval(cbfuncname))!="undefined")
					eval(cbfuncname) (xmlHttp[handlerid].responseText)
			}
		} 
		
		objXmlHttp.onerror = function() {
			if (xmlHttp[handlerid].readyState==4 || xmlHttp[handlerid].readyState=="complete") {
				if (typeof(eval(cbfuncname))!="undefined")
					eval(cbfuncname) (xmlHttp[handlerid].responseText)
			} 
		} 
			
		return objXmlHttp
	}
} 