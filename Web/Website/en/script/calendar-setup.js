/*ZD 100147 Start*/
/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
/*ZD 100147 End*/
function selected(cal, date) {
  cal.sel.value = date;
  if (cal.dateClicked) {
    cal.callCloseHandler();
	// b add
	if ((location.href).indexOf("calendar") != -1)
		datechg();
  }
}


function closeHandler(cal) {
  cal.hide();
  cal.destroy();
  calendar = null;
}

function showCalendar(id, id2, limit, format, pos, showsTime) {
  var el = document.getElementById(id);

  if ((document.getElementById (id2).src).indexOf("_dark") != -1)
	return false;
	//Code added by offshore for FB Issue 1073 -- start
   if(format == "dd/MM/yyyy")
        format = '%d/%m/%Y';
   else if(format == "MM/dd/yyyy")
        format = '%m/%d/%Y';
   else
        format = '%m/%d/%Y';
   //Code added by offshore for FB Issue 1073 -- end

  if (calendar != null) {
    calendar.hide();
  } else {
    var cal = new Calendar(false, null, selected, closeHandler);

    switch (limit) {
		case -1:
			cal.setDisabledHandler(isAfterDisabled); break;
		case 0:
			break;
		case 1:
			cal.setDisabledHandler(isBeforeDisabled); break;
	}
    if (typeof showsTime == "string") {
      cal.showsTime = true;
      cal.time24 = (showsTime == "24");
    }
    if (typeof pos != "string") {
      pos = "Br";
    }

    calendar = cal;
    cal.setRange(1900, 2070);
    cal.create();
  }
  
  calendar.setDateFormat(format);
  calendar.parseDate(el.value);
  calendar.sel = el;

  calendar.showAtElement(document.getElementById(id2), pos);
  //Code Modified For FB 1425
  if(eval(el))
    if(!el.disabled)
        el.focus();
  return false;
}

var MINUTE = 60 * 1000;
var HOUR = 60 * MINUTE;
var DAY = 24 * HOUR;
var WEEK = 7 * DAY;

function isBeforeDisabled(date) {
	return ( caldatecompare(date, servertoday) == -1  );
}

function isAfterDisabled(date) {
	return ( caldatecompare(date, servertoday) == 1  );
}


