/*ZD 100147 Start*/
/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
/*ZD 100147 End*/
function getselrmids()
{
	var roominfo, ids = ", ";
	
	if (typeof(temparyConfRoom) != "undefined")
    {
	    for (var i = 0; i < temparyConfRoom.length; i++) {
		    roominfo = aryRooms.split("^^")[temparyConfRoom[i]];
		    ids += roominfo.split("``")[1] + ", ";
	    }
	
    	return ids;
    }
    else 
        return "";
}

function initpreselection ()
{
	if ((s = queryField('m')) != ",") {
	    var t;
	    if (s.charAt("0") == ",")
    		t = s.substring (2, s.length);
        else
            t = s;
//		fogbugz case 385
		rms = t.split(",");
		for (var i = 0; i < rms.length; i++) {
    		    rms[i] = rms[i].replace(/^\s*|\s(?=\s)|\s*$/g, "");
			issuc = chgRm (s, rms[i], true);
		}
	}
}

function getcomselrmids()
{	
	if (typeof(ifrmLocation) != "undefined")
		return ifrmLocation.document.frmSettings2loc.comparedsellocs.value
	else
		return "";
}


function viewrmdy(ispersonal)
{

	if (ispersonal)
		//window.location.href = "calendarpersonaldaily.aspx?v=1&r=1&hf=" + queryField("hf") + "&d=" + queryField("d");Code changed for Tab Calendar
		window.location.href = "PersonalCalendar.aspx?Type=D&v=1&r=1&hf=" + queryField("hf") + "&d=" + queryField("d");//Code changed for Tab Calendar
	else {
		//window.location.href = "calendarroomdaily.aspx?v=1&r=1&hf=" + queryField("hf") + "&d=" + queryField("d") + "&pub=" + queryField("pub") + "&m=" + getselrmids() + "&comp=" + getcomselrmids();//Code changed for Tab Calendar
		window.location.href = "roomcalendar.aspx?v=1&r=1&hf=" + queryField("hf") + "&d=" + queryField("d") + "&pub=" + queryField("pub") + "&m=" + getselrmids() + "&comp=" + getcomselrmids();//Code changed for Tab Calendar
	}
}


function viewrmwk(ispersonal)
{
	if (ispersonal)
		window.location.href = "calendarpersonalweekly.aspx?v=1&r=1&hf=" + queryField("hf") + "&d=" + queryField("d");
	else {
		window.location.href = "calendarroomweekly.aspx?v=1&r=1&hf=" + queryField("hf") + "&d=" + queryField("d") + "&pub=" + queryField("pub") + "&m=" + getselrmids() + "&comp=" + getcomselrmids();
	}
}


function viewrmmo(ispersonal)
{
	if (ispersonal)
		window.location.href = "calendarpersonalmonthly.aspx?v=1&r=1&hf=" + queryField("hf") + "&d=" + queryField("d");
	else
		window.location.href = "calendarroommonthly.aspx?v=1&r=1&hf=" + queryField("hf") + "&d=" + queryField("d") + "&pub=" + queryField("pub") + "&m=" + getselrmids() + "&comp=" + getcomselrmids();
}


function viewrmyr(ispersonal)
{
	if (ispersonal)
		window.location.href = "calendaryearly.aspx?v=1&r=1&p=1&hf=" + queryField("hf");
	else
		window.location.href = "calendaryearly.asp?v=1&r=1&hf=" + queryField("hf") + "&pub=" + queryField("pub");
}


function jsGetXML (rmid, rmno, rmpos, roomdate, frmmod)
{
//code changed
    var currentTime = new Date();
    var minutes = currentTime.getSeconds();
	ajaxGetXML (rmpos, "inc/calendarviewinc.aspx", "ajaxSetXML", roomdate, rmid, rmno, rmpos, frmmod,minutes);
	//ajaxGetXML (rmpos, "aspil_searchconference.aspx", "ajaxSetXML", roomdate, rmid, rmno, rmpos, frmmod);
    //setTimeout("ajaxGetXML (rmpos, 'inc/calendarviewinc.asp', 'ajaxSetXML', roomdate, rmid, rmno, rmpos, frmmod);", 1000);
}


function selviewmod(viewmod, viewdate)
{
	if (queryField("p") == "1") {
		switch (viewmod) {
			case 1:
				window.location.href = "calendarpersonaldaily.aspx?v=1&r=1&d=" + viewdate;
				break;
			case 2:
				window.location.href = "calendarpersonalweekly.aspx?v=1&r=1&d=" + viewdate;
				break;
			case 3:
				window.location.href = "calendarpersonalmonthly.aspx?v=1&r=1&d=" + viewdate;
				break;
		}
	} else {
		switch (viewmod) {
			case 1:
				window.location.href = "calendarroomdaily.aspx?v=1&r=1&d=" + viewdate;
				break;
			case 2:
				window.location.href = "calendarroomweekly.aspx?v=1&r=1&d=" + viewdate;
				break;
			case 3:
				window.location.href = "calendarroommonthly.aspx?v=1&r=1&d=" + viewdate;
				break;
		}
	}
}

//method executes after getting data from server
function ajaxSetXML(outXML)
{
	var dayrmary = outXML.split("%%");
	var ary = dayrmary[0].split("!!");
//alert(outXML);
	if (outXML.indexOf("<head>")!= -1) {
		alert("Error: there is some unexpected error.")
		return false;
	}

	if (ary[1] < 0)	{
		strConfs = outXML;
		drawConf (strConfs);
	} else { 
		if (dayrmary.length == 2)
			aryRoomConf[ary[1]] = ary[0] + "!!" + ary[1] + "!!" + ary[2] + "!!" + ary[3];
		else
			aryRoomConf[ary[1]] = outXML;
	
		if (ary[1] != aryConfRoom[ary[4]])
			return -1;
		else
			drawConf (aryRoomConf[ary[1]], ary[4]);
	}	
}


function viewconf(cid)
{
	url = "dispatcher/conferencedispatcher.asp?cmd=ViewConference&cid=" + cid;
	if (myPopup) {
		if (!myPopup.closed) {
			myPopup.close();
		}
	}
	myPopup = window.open(url, "viewconference", "width=1,height=1,resizable=yes,scrollbars=yes,status=no");
	myPopup.focus();
}


function cursor_wait() 
{
	var cursor = 
	   document.layers ? document.cursor :
	   document.all ? document.all.cursor :
	   document.getElementById ? document.getElementById('cursor') : null;
	cursor = 'wait';
}

function cursor_clear() 
{
 	var cursor = 
	   document.layers ? document.cursor :
	   document.all ? document.all.cursor :
	   document.getElementById ? document.getElementById('cursor') : null;
	cursor = 'default';
}
