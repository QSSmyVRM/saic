/*ZD 100147 Start*/
/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
/*ZD 100147 End*/
// JavaScript 

////////////////////////////////////////////////////////////////////////
// Collection Class
////////////////////////////////////////////////////////////////////////

function Collection(name, sortCompFunc)
{
	this.name = name;	// decription of this collection object
	this.compFunc = sortCompFunc;	// determine the order of elements
	this.elements = new Array();
	this.toString = collectionToString;
	this.item = item;
	this.add = add;
	this.remove = remove;
	this.size = size;
}

function collectionToString()
{
	return this.name + ": " + this.elements.length;
}

function size()
{
	return this.elements.length;
}

function item(index)
{
	if (index >= this.elements.length)
		return null;
	else
		return this.elements[index];
}

function add(newElmt)
{
	var newArray = new Array(this.elements.length + 1);
	for (var i=0; i<this.elements.length; i++) 
		newArray[i] = this.elements[i];
	newArray[this.elements.length] = newElmt;
	this.elements = newArray;
	if (this.compFunc != null)
		this.elements.sort(this.compFunc);
}

function remove(index)
{
	if (this.elements.length == 0 || index >= this.elements.length)
		return;
	var newArray = new Array(this.elements.length - 1);
	for (var i=0; i<index; i++)
		newArray[i] = this.elements[i];
	for (i = index; i<this.elements.length-1; i++)
		newArray[i] = this.elements[i+1];
	this.elements = newArray;
}

