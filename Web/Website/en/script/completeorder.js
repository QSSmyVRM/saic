/*ZD 100147 Start*/
/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
/*ZD 100147 End*/
function complete_order_prompt(promptpicture, prompttitle, orderinfo, cno, showonly, sendto, isfood) 
{
	promptbox = document.createElement('div'); 
	promptbox.setAttribute ('id' , 'prompt');
	document.getElementsByTagName('body')[0].appendChild(promptbox);
	promptbox = eval("document.getElementById('prompt').style");

	w = 300;
	promptbox.position = 'absolute';
	document.getElementById('prompt').style.top = mousedownY - 36; // eval("document.getElementById('prompt'+mod).style").top - promptbox.top;//(mod == 3) ? -350 : 0;
	document.getElementById('prompt').style.left = mousedownX - w - 28; //(mod == 3) ? 565 : 0;
	promptbox.width = w;
	promptbox.border = 'outset 1 #bbbbbb'; 

	m = "<table cellspacing='0' cellpadding='0' border='0' width='100%'><tr valign='middle'><td width='22' height='22' style='text-indent:2;' class='titlebar'><img src='" + promptpicture + "' height='18' width='18'></td><td class='titlebar'>" + prompttitle + "</td></tr></table>" 
	m += "<table cellspacing='0' cellpadding='0' border='1' width='100%' class='promptbox'>";
	m += "  <tr><td align=center><SPAN class=tableblackblodtext>Order Name</SPAN></td><td align=center><SPAN class=tableblackblodtext>Order Time</SPAN></td><td align=center><SPAN class=tableblackblodtext>Complete</SPAN></td></tr>";
	
	trgcbname = (isfood) ? ("document.frmManagefoodorder.completedfoodorder" + cno) : ("document.frmManageresourceorder.completedresourceorder" + cno);
	chkcbname = (isfood) ? ("document.frmManagefoodorder.CompleteFoodOrder" + cno) : ("document.frmManageresourceorder.CompleteResourceOrder" + cno);

	chkbx = "";		hascompleted = false;
	ordersary = orderinfo.split("||");
	
	for (var i = 0; i < ordersary.length - 1; i++) {
		orderary = ordersary[i].split("@@");
		m += "  <tr>";
		m += "    <td align=center>" + orderary[1] + "</td>";
		m += "    <td align=center>" + orderary[2] + " " + orderary[3] + "</td>";
		m += "    <td align=center>" + ( (orderary[4] == "1") ? "<img src='image/tick.gif' width=18 height=18 title='completed'>" : ( (showonly) ? "<img src='image/btn_delete.gif' width=18 height=18 title='not completed'>" : "<input type=checkbox name='SetComplete" + i + "' id='SetComplete" + i + "' value=1" + ( ( (";" + eval(trgcbname).value).indexOf(";" + orderary[0] + ";") == -1 ) ? "" : " checked" ) + ">" ) ) + "</td>";
		m += "  </tr>";
		m += "<input type=hidden name='OrderID" + i + "' value='" + orderary[0] + "'>"
		
		chkbx += (orderary[4] == "1") ? "" : (i + ";");
		hascompleted = (orderary[4] == "1") ? true : hascompleted;
	}
	m += "</table>" 

	m += "<center><table cellspacing='0' cellpadding='0' border='0' width='100%' class='promptbox'>" 
	m += "  <tr><td align='right'>"
	if (showonly) {
		m += "    <input type='button' class='prompt' value='Close' onMouseOver='this.style.border=\"1 outset transparent\"' onMouseOut='this.style.border=\"1 solid transparent\"' onClick='document.getElementsByTagName(\"body\")[0].removeChild(document.getElementById(\"prompt\"))'>"
	} else {
		m += "    <input type='button' class='prompt' value='Submit' onMouseOver='this.style.border=\"1 outset #dddddd\"' onMouseOut='this.style.border=\"1 solid transparent\"' onClick='" + sendto + "(\"" + chkbx + "\", \"" + trgcbname + "\", \"" + chkcbname + "\", " + hascompleted + ");'>"
		m += "    <input type='button' class='prompt' value='Cancel' onMouseOver='this.style.border=\"1 outset transparent\"' onMouseOut='this.style.border=\"1 solid transparent\"' onClick='canceldecision(\"" + chkcbname + "\");'>"
	}
	m += "  </td></tr>"
	m += "</table></center>"
	
	document.getElementById('prompt').innerHTML = m;
} 


function savedecision(selNos, trgcbname, chkcbname, hascompleted) 
{
	selNoary = selNos.split(";");
	trgcb = eval(trgcbname);	chkcb = eval(chkcbname);
	trgcb.value = "";			allcompleted = true;

	for (var i = 0; i < selNoary.length - 1; i++) {
		if ( document.getElementById("SetComplete" + selNoary[i]).checked ) {
			trgcb.value += document.getElementById("OrderID" + selNoary[i]).value  + ";"
		} else {
			allcompleted = false;
		}
	}


	if ( (trgcb.value == "") && (!hascompleted) && (selNoary.length > 1) ) {
		chkcb.checked = false;
		chkcb.style.backgroundColor = ""
		chkcb.title = "all orders are still not completed";
	} else {
		if (allcompleted) {
			chkcb.checked = true;
			chkcb.style.backgroundColor = ""
			chkcb.title = "all orders completed";
		} else {
			chkcb.checked = true;
			chkcb.style.backgroundColor = "#aaaaaa"
			chkcb.title = "some orders completed, some have not";
		}
	}
	
	document.getElementsByTagName("body")[0].removeChild(document.getElementById("prompt"));
}


function canceldecision(chkcbname) 
{
	
	switch (eval(chkcbname).title) {
		case "all orders are still not completed" :
			eval(chkcbname).checked = false;
			eval(chkcbname).style.backgroundColor = ""
			break;
		case "some orders completed, some have not" :
			eval(chkcbname).checked = true;
			eval(chkcbname).style.backgroundColor = "#aaaaaa"
			break;
		case "all orders completed" :
			eval(chkcbname).checked = true;
			eval(chkcbname).style.backgroundColor = ""
			break;
	};


	document.getElementsByTagName("body")[0].removeChild(document.getElementById("prompt"));
} 

