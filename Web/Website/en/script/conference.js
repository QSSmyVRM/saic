/*ZD 100147 Start*/
/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
/*ZD 100147 End*/
// *** view conference details ***
function view_conference(cid)
{
	url = "conferencedetails.aspx?cid=" + cid;
	confdetail = window.open(url, "viewconference", "width=750,height=420,resizable=yes,scrollbars=yes,status=no");
	confdetail.focus();
}


// pgmode: parent page. 1-edit; 2-delete; 3-confirm
// allreccids: all recurring conference ids
// p0: bool.   in pgmode=1, deleteconf, it is isdeleted 1/0 that indict arent recuring conf is selected for delete;
//             in pgmode=3, confirmconf, it is isall 1/0 that indict parent conf can be all confirm as same one.
// p1: string. in pgmode=1, deleteconf, it is delreasonstr that used in parent page;
//             in pgmode=3, confirmconf, it is conference total num
// p2: string. in pgmode=2, confselect, it is ordermode that indictor food, resource, food+resource;
//             in pgmode=3, confresponse, it is conference current no
function click_recur(cno, cid, allreccids, pgmode, p0, p1, p2)
{
	var fld = document.getElementById("recurimg" + cno);

	if ((fld.src).indexOf("down") != -1) {
		cls_all_instances (allreccids);
		document.getElementById("instanceDIV" + cno).style.display="";

		if (document.getElementById("instanceDIV" + cno).innerHTML == "") {
			document.getElementById("instanceDIV" + cno).innerHTML="<b><font color='#FF00FF' size='2'>Data loading ...</font></b>&nbsp;&nbsp;&nbsp;&nbsp;<img border='0' src='image/wait2.gif' width='30' height='30'>";
			fld.src = "image/space.gif";
	        if ( (cno != "") && (cid != "-1") ) {
		        ajaxGetXML ( 0, "ajGetinstances.aspx", "ajSetXML", cno, cid, pgmode, ( p0 ? "1" : "0" ), p1, p2);
		    }
		} else {
			ridsary = allreccids.split(",");
			for (var i = 0; i < ridsary.length-1; i++)
				document.getElementById("recurimg" + ridsary[i]).src = (ridsary[i] == cno) ? "image/sort_up.gif" : "image/sort_down.gif" ;
		}
			
	} else {
		if ((fld.src).indexOf("up") != -1) {
			document.getElementById("instanceDIV" + cno).style.display="none";
			fld.src = "image/sort_down.gif";
		}
	}
}


// allreccids: all recurring conference ids
function show_recur(str, allreccids)
{
    var strary = str.split("`@@`");
    // not use strary[1] for more accurate string.

    document.getElementById ("instanceDIV" + strary[0]).innerHTML = strary[1];
    
    var ridsary = allreccids.split(",");
    for (var i = 0; i < ridsary.length-1; i++) {
	    document.getElementById("recurimg" + ridsary[i]).src = (ridsary[i] == strary[0]) ? "image/sort_up.gif" : "image/sort_down.gif" 
    }
}


// allreccids: all recurring conference ids
function cls_all_instances (allreccids)
{
	var ridsary = allreccids.split(",");

	for (var i = 0; i < ridsary.length-1; i++) {
		document.getElementById("recurimg" + ridsary[i]).src = "image/space.gif";
		document.getElementById("instanceDIV" + ridsary[i]).style.display="none";
	}
}
