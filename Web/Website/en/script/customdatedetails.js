/*ZD 100147 Start*/
/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
/*ZD 100147 End*/
function replace_user_para_prompt(promptpicture, prompttitle, pgname, str) 
{
	promptbox = document.createElement('div'); 
	promptbox.setAttribute ('id' , 'prompt');
	document.getElementsByTagName('body')[0].appendChild(promptbox);
	promptbox = eval("document.getElementById('prompt').style");
	
	w= 100
	
		mousedownX = event.clientX + document.body.scrollLeft;
		mousedownY = event.clientY + document.body.scrollTop;
	
	promptbox.position = 'absolute'
	document.getElementById('prompt').style.top = mousedownY + 10; // eval("document.getElementById('prompt'+mod).style").top - promptbox.top;//(mod == 3) ? -350 : 0;
	document.getElementById('prompt').style.left = mousedownX; //(mod == 3) ? 565 : 0;
//	promptbox.top = mousedownY + 20;
//	promptbox.left = mousedownX + 20;
	promptbox.width = w 
	promptbox.border = 'outset 1 #bbbbbb' 
	
	m = "<table cellspacing='0' cellpadding='0' border='0' width='100%'><tr valign='middle'><td width='28' height='25' style='text-indent:2;' class='titlebar'><img src='" + promptpicture + "' height='25' width='25'></td><td></td><td class='titlebar'>" + prompttitle + "</td></tr></table>" 
	m += "<table cellspacing='0' cellpadding='0' border='0' width='100%' class='promptbox'>";
	m += "  <tr>";
	m += "    <td>" + str + "</td>";
	m += "  </tr>"
	m += "</table>" 
	
	document.getElementById('prompt').innerHTML = m;
} 

function cancelthis()
{
	isreplaced = false;
	if (document.getElementById("prompt") && document.getElementsByTagName("body")[0])
		document.getElementsByTagName("body")[0].removeChild(document.getElementById("prompt"));
}