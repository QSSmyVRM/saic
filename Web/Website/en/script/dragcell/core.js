/*ZD 100147 Start*/
/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
/*ZD 100147 End*/
/*
Copyright (c) 2005 Tim Taylor Consulting <http://tool-man.org/>

Permission is hereby granted, free of charge, to any person obtaining a
copy of this software and associated documentation files (the "Software"),
to deal in the Software without restriction, including without limitation
the rights to use, copy, modify, merge, publish, distribute, sublicense,
and/or sell copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
IN THE SOFTWARE.
*/

var ToolMan = {
	events : function() {
		if (!ToolMan._eventsFactory) throw "ToolMan Events module isn't loaded";
		return ToolMan._eventsFactory
	},

	css : function() {
		if (!ToolMan._cssFactory) throw "ToolMan CSS module isn't loaded";
		return ToolMan._cssFactory
	},

	coordinates : function() {
		if (!ToolMan._coordinatesFactory) throw "ToolMan Coordinates module isn't loaded";
		return ToolMan._coordinatesFactory
	},

	drag : function() {
		if (!ToolMan._dragFactory) throw "ToolMan Drag module isn't loaded";
		return ToolMan._dragFactory
	},

	dragsort : function() {
		if (!ToolMan._dragsortFactory) throw "ToolMan DragSort module isn't loaded";
		return ToolMan._dragsortFactory
	},

	helpers : function() {
		return ToolMan._helpers
	},

	cookies : function() {
		if (!ToolMan._cookieOven) throw "ToolMan Cookie module isn't loaded";
		return ToolMan._cookieOven
	},

	junkdrawer : function() {
		return ToolMan._junkdrawer
	}

}

ToolMan._helpers = {
	map : function(array, func) {
		for (var i = 0, n = array.length; i < n; i++) func(array[i])
	},

	nextItem : function(item, nodeName) {
		if (item == null) return
		var next = item.nextSibling
		while (next != null) {
			if (next.nodeName == nodeName) return next
			next = next.nextSibling
		}
		return null
	},

	previousItem : function(item, nodeName) {
		var previous = item.previousSibling
		while (previous != null) {
			if (previous.nodeName == nodeName) return previous
			previous = previous.previousSibling
		}
		return null
	},

	moveBefore : function(item1, item2) {
//alert("here")
showmystatus(item1, item2);
		var parent = item1.parentNode
		parent.removeChild(item1)
		parent.insertBefore(item1, item2)
	},

	moveAfter : function(item1, item2) {
//alert("not here")
if (item1 !=null && item2 !=null)
document.getElementById("mystatus").innerHTML = item1.id + "," + item2.id;
if (item1 !=null && item2 ==null)
document.getElementById("mystatus").innerHTML = item1.id + ",null";
if (item1 ==null && item2 !=null)
document.getElementById("mystatus").innerHTML = "null," + item2.id;
if (item1 ==null && item2 ==null)
document.getElementById("mystatus").innerHTML = "null,null";
		var parent = item1.parentNode
		parent.removeChild(item1)
		parent.insertBefore(item1, item2 ? item2.nextSibling : null)
	}
}

/** 
 * scripts without a proper home
 *
 * stuff here is subject to change unapologetically and without warning
 */
ToolMan._junkdrawer = {
	serializeList : function(list) {
		var items = list.getElementsByTagName("li")
		var array = new Array()
		for (var i = 0, n = items.length; i < n; i++) {
			var item = items[i]

			array.push(ToolMan.junkdrawer()._identifier(item))
		}
		return array.join('|')
	},

	inspectListOrder : function(id) {
		alert(ToolMan.junkdrawer().serializeList(document.getElementById(id)))
	},

	restoreListOrder : function(listID) {
		var list = document.getElementById(listID)
		if (list == null) return

		var cookie = ToolMan.cookies().get("list-" + listID)
		if (!cookie) return;

		var IDs = cookie.split('|')
		var items = ToolMan.junkdrawer()._itemsByID(list)

		for (var i = 0, n = IDs.length; i < n; i++) {
			var itemID = IDs[i]
			if (itemID in items) {
				var item = items[itemID]
				list.removeChild(item)
				list.insertBefore(item, null)
			}
		}
	},

	_identifier : function(item) {
		var trim = ToolMan.junkdrawer().trim
		var identifier

		identifier = trim(item.getAttribute("id"))
		if (identifier != null && identifier.length > 0) return identifier;
		
		identifier = trim(item.getAttribute("itemID"))
		if (identifier != null && identifier.length > 0) return identifier;
		
		// FIXME: strip out special chars or make this an MD5 hash or something
		return trim(item.innerHTML)
	},

	_itemsByID : function(list) {
		var array = new Array()
		var items = list.getElementsByTagName('li')
		for (var i = 0, n = items.length; i < n; i++) {
			var item = items[i]
			array[ToolMan.junkdrawer()._identifier(item)] = item
		}
		return array
	},

	trim : function(text) {
		if (text == null) return null
		return text.replace(/^(\s+)?(.*\S)(\s+)?$/, '$2')
	}
}


function getitemno (item)
{
	if (item == null)
		return "0";
	else
		return (item.id).substr(itemnostart)
}

function getitemindex (itemno)
{
	for (i=0; i<itemorder.length; i++) {
		if (itemno == "" + itemorder[i])
			return i;
	}
	return -1;
}

function getmystatus(item1, item2)
{
	var n=itemorder.length;
	var newitemorder = new Array (n);
	var msg = "";
	
	item1no = getitemno (item1);
	item2no = getitemno (item2);
	msg += item1no + "," + item2no + ";   ";
	
	item1index = getitemindex (item1no);
	item2index = getitemindex (item2no);
	
//	if (item1index == -1 || item2index == -1 || item1index == item2index) 
//		return "item1index=" + item1index + ", item2index=" + item2index + ". error!";
	
	if (item2no == "0") {
		if (item1index == -1) 
			return "item1index=" + item1index + ", item2index=" + item2index + ". error!";

		for (i=0; i<item1index; i++) {
			newitemorder[i] = itemorder[i];
		}
		for (i=item1index+1; i<n; i++) {
			newitemorder[i-1] = itemorder[i];
		}
		newitemorder[n-1] = itemorder[item1index]
	} else {
		if (item1index == -1 || item2index == -1 || item1index == item2index) 
			return "item1index=" + item1index + ", item2index=" + item2index + ". error!";
		if (item1index < item2index) {
			for (i=0; i<item1index; i++) {
				newitemorder[i] = itemorder[i];
			}
			for (i=item1index+1; i<item2index; i++) {
				newitemorder[i-1] = itemorder[i];
			}
			newitemorder[item2index-1] = itemorder[item1index]
			newitemorder[item2index] = itemorder[item2index]
			for (i=item2index+1; i<n; i++) {
				newitemorder[i] = itemorder[i];
			}
		}

		if (item1index > item2index) {
			for (i=0; i<item2index; i++) {
				newitemorder[i] = itemorder[i];
			}
			newitemorder[item2index] = itemorder[item1index]
			newitemorder[item2index+1] = itemorder[item2index]
			for (i=item2index+1; i<item1index; i++) {
				newitemorder[i+1] = itemorder[i];
			}
			for (i=item1index+1; i<n; i++) {
				newitemorder[i] = itemorder[i];
			}
		}

	}

	itemorder = newitemorder;
	
	return("" + itemorder);
}


function showmystatus(item1, item2)
{
	rstmsg = getmystatus(item1, item2);
	document.getElementById("BridgeNewOrder").value = rstmsg;
	
	for (i=0; i<itemorder.length; i++) {
		document.getElementById ("title" + itemorder[i]).innerHTML = i+1;
	}
}