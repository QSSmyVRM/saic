/*ZD 100147 Start*/
/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
/*ZD 100147 End*/
// max 220

//user info
var EN_1 = "Please enter a User Login Name.";
var EN_2 = "Please enter a Password.";
var EN_3 = "Your entries for Password do not match.";
var EN_4 = "Please enter a First Name.";
var EN_5 = "Please enter a Last Name.";
var EN_6 = "Please enter a valid e-mail address.";
var EN_51 = "Please select a User from the User List.";
var EN_81 = "Please enter a valid Company Email";
var EN_89 = "Please enter your Full Name."
var EN_90 = "Please enter a Subject";
var EN_91 = "Please enter a Comment."
var EN_129 = "Please enter Contact Name.";
var EN_130 = "Please enter Contact Phone Number.";
var EN_172 = "Please enter User Login Email.";
var EN_200 = "Two email addresses do not match.";


//date and time info
var EN_7 = "Please check the From Date and reenter.";
var EN_8 = "From Date cannot be a future date.";
var EN_9 = "Please enter a From Date that is before the To Date.";
var EN_17 = "Please enter a number in TimeBefore.";
var EN_18 = "Please enter a number in DeltaTime.";
var EN_69 = "Please enter a To Date.";
var EN_70 = "Please enter a From Date.";
var EN_92 = "To Date cannot be a future date. ";
var EN_93 = "Please enter a To Date that is after the From Date."
var EN_117 = "Please enter an Up To Date.";
var EN_118 = "From Date cannot be a past date.";
var EN_119 = "Up To Date cannot be a past date.";
var EN_120 = "This date has been selected. Please select another one.";
var EN_177 = "Please enter an End Time that is at least 15 minutes later the Start Time.";
var EN_202 = "Date change for instance customization is not allowed. Please drag instance to another time slot. Time will not change.";
var EN_215 = "Please input the correct time format (hh:mm AM/PM).";
var EN_216 = "Please check the room start time with the conference end time.";
var EN_217 = "Please check the room end time with the conference end time.";
var EN_218 = "Please check the room start time with the conference start time.";
var EN_220 = "Please check the room end time with the conference start time.";

// bridge and port info
var EN_11 = "Please enter an MCU Name.";
var EN_12 = "Please enter an MCU Address.";
var EN_13 = "Please enter an MCU Login.";
var EN_19 = "Please enter a number in Survey Interval.";
var EN_20 = "Please enter a number (between 1 and 12) in T1 Channels per Card.";
var EN_21 = "Please enter a number (between 1 and 12) in Number of IP Ports per Card";
var EN_22 = "Please enter a number (between 1 and 12) in Number of Reserved T1 Channels.";
var EN_23 = "Please enter a number (between 1 and 12) in Number of Reserved IP Ports.";
var EN_24 = "Please enter a number (between 1 and 12) in Number of Audio Ports per card.";
var EN_25 = "Please enter a number (between 1 and 12) in Reserved Number of Audio Ports.";
var EN_28 = "Please enter a valid IP Address.";
var EN_82 = "Please enter a Remote SMTP IP or Name.";
var EN_83 = "Please enter a Port No.";
var EN_94 = "Please enter an ISDN Phone Number";
var EN_95 = "Please check the ISDN Phone Number and reenter";
var EN_96 = "Please check the IP Address and reenter.";
var EN_98 = "Please enter a valid ISDN Number.";
var EN_122 = "Please enter a Service Name.";
var EN_123 = "Please enter an Address.";
var EN_124 = "Please enter a valid Start Range.";
var EN_125 = "Please enter a valid End Range.";
var EN_126 = "Please enter an End Range bigger than Start Range.";
var EN_133 = "Please enter a Cascade Name.";
var EN_134 = "Please select a Protocol type.";
var EN_135 = "Please select a Connection type.";
var EN_136 = "Please select a Bridge.";
var EN_137 = "Please enter a valid IP or ISDN Address for bridge.";
var EN_138 = "Please select a default IP Service.";
var EN_139 = "Please select a default ISDN Service.";
var EN_141 = "Please select a private service.";
var EN_142 = "Please select a public service.";
var EN_143 = "Please select a Media.";
var EN_171 = "Please select an Interface type.";
var EN_181 = "Please enter a Prefix.";
var EN_182 = "Please enter an Endpoint Name.";
var EN_185 = "Please enter an Endpoint Address.";
var EN_189 = "Please fill a number between 0 and 100 in Reserved Port %.";
var EN_213 = "Please enter a valid IP Address for  for Control Port.";
var EN_214 = "Please enter a valid IP Address for Port A.";
var EN_314 = "Please enter a valid Duration."; 
var EN_315 = "Conference duration should be minimum of 15 mins.(Difference between the duration and buffer period)";
var EN_316 = "Please enter a valid Conference Start Duration.";
var EN_317 = "Please enter a valid Conference End Duration.";

//Group info
var EN_10 = "Please enter a Group Name.";
var EN_39 = "Please select different group(s) for Available Groups and CC Groups."
var EN_53 = "Please double-click the Group Name.";
var EN_99 = "Group must have at least one member.";
var EN_105 = "Please select different group(s) for Default Group and Default CC Group.";
var EN_128 = "Please add atleast one user in the group."; //FB 1914

//recurring
var EN_32 = "Please check Recurring Start Time and reenter.";
var EN_33 = "Please check Recurring Duration and reenter.";
var EN_34 = "Please check Recurring Timezone and reenter.";
var EN_35 = "Please check the Type of Recurring Pattern and reenter.";
var EN_36 = "Please check the End Type of Range of Recurring and reenter.";
var EN_37 = "Please check the Pattern of Recurring Range and reenter.";
var EN_38 = "Please check the Type of Recurring Range and reenter.";
var EN_74 = "Please enter a valid future Recurring Start Date.\n(date format: mm/dd/yyyy)";
var EN_79 = "Number of Participant for Recurring Conference can't over ";
var EN_107 = "Please select a weekday.";
var EN_108 = "Please enter a valid future End by Date.\n(date format: mm/dd/yyyy)";
var EN_109 = "Please enter a future End Date.\n(date format: mm/dd/yyyy)";
var EN_193 = "Please select at least one date.";
var EN_211 = "You have reached the maximum limit of custom selected date.";

//conference and conference room
var EN_26 = "Please enter a Conference Room Name.";
var EN_27 = "Please enter a number in Capacity.";
var EN_30 = "Please enter a Conference Name.";
//var EN_31 = "Please select a Conference Duration of at least 15 minutes.";
var EN_31 = "Conference has to be at least 15 minutes.";
var EN_40 = "Please enter a number in Number of Extra Participants.";
var EN_41 = "Please enter a positive number in Number of Extra Participants.";
var EN_43 = "Please select a Conference Room.";
var EN_44 = "Please enter a Conference Password.";
var EN_45 = "Please check the format of Conference Date and reenter.";
var EN_49 = "Please enter a future Conference Date.";
var EN_52 = "Please enter all information for the New User \nbefore you Replace the previous User.";
var EN_54 = "Please double-click the Room Name.";
var EN_61 = "The following room(s) have no video facility. Continue to set up the video conference?";
var EN_62 = "During this time period, the following conference(s) are also scheduled:"
var EN_71 = "Please select room for the External Attendees.";
var EN_72 = "Please select External Attendees before you select any room.";
var EN_75 = "Please select room for the External Attendees by clicking Schedule Room(s).";
var EN_76 = "You must select External Attendees and room(s) \nbefore scheduling a Room-based conference.";
var EN_87 = "Please select one of the options above.";
var EN_97 = "Please enter a Conference Room Number.";
var EN_102 = "Please enter a Conference Date.";
var EN_103 = "Please check Number of Extra Participants and reenter.";
var EN_104 = "Please select room for External Attendees.";
var EN_112 = "Please select your Room before submitting the request.";
var EN_113 = "Please select a Conference.";
var EN_114 = "Please select a room from the Selected Room(s) list.";
var EN_131 = "Please enter a number in Maximum Number of Concurrent Phone Calls.";
var EN_140 = "The maximum allowance of 512 characters in Conference Description has been reached.";
var EN_144 = "You have chosen one or more rooms without any participants in it. Would you still like to continue?"
var EN_145 = "Please select at least one Protocol.";
var EN_146 = "Please select at least one Audio Protocol.";
var EN_147 = "Please select at least one Video Protocol.";
var EN_148 = "Please select at least one Line Rate.";
var EN_149 = "Please select a default Equipment.";
var EN_150 = "Please input Room Phone Number.";
var EN_186 = "No room participants selected. Are you sure you want to continue?";
var EN_187 = "Conference Password should be numeric only.";//FB 2017
var EN_190 = "Please select a endpoint to view details.";
var EN_192 = "Please input up to 10 emails for Multiple assistant emails.";
var EN_194 = "Please select Assistant in Charge from myVRM address book.\nTo show myVRM address book, click edit icon on the right of the text field.";
var EN_195 = "Please select a room from the list or provide your own connection information.";
var EN_203 = "Please select at least one department to associate with room.";
var EN_204 = "When selecting endpoint, please select Media for it.";
var EN_205 = "Please select a endpoint, because you select a type of media.";
var EN_208 = "You have reached the maximum limit of Characters allowed for this field.";
var EN_212 = "Advanced room allocation can only be assigned with non-video conference.\n Please either remove advanced room allocation or create non-video conference.";
var EN_219 = "Room has already been added.";

//Template
var EN_50 = "Please enter a Template Name.";
var EN_63 = "A maximum of five templates may be selected.";

var EN_55 = "Are you sure you want to remove this group?";
var EN_56 = "Are you sure you want to remove the selected groups?";
var EN_57 = "Are you sure you want to remove this conference? \nAll the previously invited participants will be notified of the cancellation.";
var EN_58 = "Are you sure you want to remove the selected conference(s)?";
var EN_59 = "Are you sure you want to remove this template?";
var EN_60 = "Are you sure you want to remove the selected templates?";
var EN_77 = "Please double-click the Template or Conference.";
var EN_191 = "Please enter a positive integer in Initial Time.";

// Food/Resource
var EN_156 = "Please input Order Name.";
var EN_157 = "An order must contain at least one food item. If you choose to continue, this order will be removed.\nAre you sure you want continue?";
var EN_158 = "Please select an image.";
var EN_159 = "Please select one resource image.";
var EN_160 = "Please select one resource item.";
var EN_161 = "Please input Item Name.";
var EN_162 = "Please input a correct Item Quantity. It should be positive integer.";
var EN_163 = "If item quantity is 0, this item will be deleted.\nAre you sure you want continue?";
var EN_164 = "This resource item is already in the list. Please input a different name or select it to edit.";
var EN_165 = "Please input a correct price. It should be positive number and with up to two decimals.";
var EN_166 = "All field are empty.\nThis order will not be saved, if it is new order;\n or it will be deleted, if it is old.\n\nAre you sure you want continue?";
var EN_167 = "You order for this room will be saved.\nWarning: An order must contain at least one resource item. If you choose to continue, This order will be removed.\nAre you sure you want continue?";
var EN_168 = "Please input Item Name, Or remove this new item.";
var EN_169 = "Please select a Category.";
var EN_170 = "Please input Item Unit Price.";
var EN_178 = "System only support JPG and JPEG format image. Please convert it into JPG or JPEG format by some graphic tool, e.g. Microsoft Photo Editor.";
var EN_179 = "Please do not upload executable file to avoid any virus.";
var EN_188 = "Please fill a positive integer in Item Quantity.";

// terminal control
var EN_183 = "Please enter an Endpoint Name.";
var EN_184 = "Please enter a valid Guest Email.";
var EN_206 = "Are you sure you want to terminate the selected endpoint?";


//misc
var EN_64 = "You must enter a new User to replace the previous User.";
var EN_65 = "Conference successfully saved to your Lotus Notes Calendar.";
var EN_66 = "Conference successfully saved to your Outlook Calendar.";
var EN_67 = "You permissioned this user as a General User but have given the user access to the Super Administrator Preference menu. \nSuggestions: 1) Permission this user as a Super Administrator or \n2) Disable his/her Super Administrator Preference menu.";
var EN_68 = "Sorry, this page has expired. Please click here to login again.";
var EN_14 = "Please enter a positive or negative number in Position in Chain.";
var EN_15 = "Card Number is not a positive or negative number, \nor is over the maximum number.";
var EN_16 = "Please enter a User Home Page Link.";
var EN_73 = "The URL tag in Conference Description should be in pair (<url></url>). \nPlease correct it and try again.";
var EN_78 = "Please use the SELECT button to select a Main Conference Room";
var EN_80 = "Please double-click the Location.";
var EN_84 = "Please enter a Connection Timeout.";
var EN_85 = "Please check the To Date and reenter.";
var EN_86 = "Sorry, no Resource has a Resource ID of ";
var EN_88 = "Original Window is missing. This window will be closed.\nPlease go back to original page and try again.";
var EN_100 = "Comment cannot be longer than 500 characters.";
var EN_106 = "Please wait while we process your request.";
var EN_110 = "Please enter New User's Information.";
var EN_111 = "New User and the Previous User have the same email. Please reenter.";
var EN_115 = "Please select a value in Video Session.";
var EN_116 = "Your browser doesn't support printing. Please use menu item to print."
var EN_127 = "Warning: you have changed the template preference.\nSave these new changes?";
var EN_151 = "Please enter a Category Name.";
var EN_152 = "Please enter a File Name.";
var EN_154 = "Sorry, cannot select Sort By Room if you have chosen None in Rooms.";
var EN_155 = "Sorry, cannot select Sort By Room if you have chosen None or Any in Rooms.";
var EN_174 = "Are you sure you want to remove the file?";
var EN_175 = "Error: failed to upload the file [reason - folder]. Please provide this to administrator.";
var EN_176 = "Error: there are some duplicate files in upload file list.";
var EN_180 = "Please select at least one report to delete.";
var EN_196 = "Please input a correct MCU ISDN Port Charge. It should be positive number with up to two decimals.";
var EN_197 = "Please input a correct ISDN Line Cost. It should be positive number with up to two decimals.";
var EN_198 = "Please input a correct IP Port Charge. It should be positive number with up to two decimals.";
var EN_199 = "Please input a correct IP Line Cost. It should be positive number with up to two decimals.";
var EN_201 = "This page has expired due to inactivity.\nPlease login and try again.";
var EN_207 = "Sorry, some error happen in SIM. It may be caused by Internet or system.\nPlease try again. If it still happens, please notify administrator.\nSIM will be shut down now."
var EN_209 = "Only Alphanumeric characters are allowed in filenames.";
var EN_210 = "Please enter a unique email ID."

// browser
var EN_132 = "Your browser has a popup blocker enabled. Please disable it for the myVRM website, as myVRM uses popups that are necessary for your information input.";


