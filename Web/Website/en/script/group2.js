/*ZD 100147 Start*/
/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
/*ZD 100147 End*/
function deleteAllParty()
{
	if (document.frmManagegroup2.PartysInfo.value != "") {
		var isRemove = confirm("Are you sure you want to remove all attendees?")
		if (isRemove == false) {
			return (false);
		}
	}
	
	document.frmManagegroup2.PartysInfo.value = "";
	var url = ifrmMemberlist.location + "";
	//Code changed for FB 412 -Start
	//if(url.indexOf('group2member.asp') != -1){	
	if(url.indexOf('group2member.aspx') != -1){
	//Code changed for FB 412 -End
	    ifrmMemberlist.location.reload(); 
	}
	
	cb = document.frmManagegroup2.Group;
	for (i = 0; i < cb.length; i++) {
		cb[i].selected = false;
	}
}


function addNewParty()
{
	var url = ifrmMemberlist.location + "";
	//Code Changed for FB412 start
    //	if(url.indexOf('group2member.asp') != -1) {
	if(url.indexOf('group2member.aspx') != -1) {
    //Code Changed for FB412 End
		willContinue = ifrmMemberlist.bfrRefresh();
		if (willContinue) {
			partysinfo = document.frmManagegroup2.PartysInfo.value;
			partysinfo = "!!!!!!!!0||" + partysinfo; // FB 1888
			document.frmManagegroup2.PartysInfo.value = partysinfo;
			ifrmMemberlist.location.reload(); 
		}
	}	
}


function getYourOwnEmailList()
{
	
	var url = ifrmMemberlist.location + "";
	//Code Changed for FB412 start
	//if(url.indexOf('group2member.asp') != -1){
	if(url.indexOf('group2member.aspx') != -1){
	//Code Changed for FB412 End
	    willContinue = ifrmMemberlist.bfrRefresh(); 
		if (willContinue) {
			ifrmMemberlist.location.reload(); 
//			url = "dispatcher/conferencedispatcher.asp?frm=group&cmd=GetEmailList&emailListPage=1&wintype=pop"; //Login Management
                if(queryField("sb") > 0)//Login Management
                {
                     url = "emaillist2.aspx?t=e&frm=" + queryField("frm") + "&wintype=ifr" + "&fn=" + queryField("frmname") + "&n=" + queryField("n")
                }
                else
                {
                    url = "emaillist2main.aspx?t=e&frm=" + queryField("frm") + "&fn=" + queryField("frmname") + "&n=" + queryField("no")
                }
			if (!window.winrtc) {	// has not yet been defined
			    winrtc = window.open(url, "", "width=920,height=470,top=0,left=0,resizable=yes,scrollbars=yes,status=no"); //FB 2735
				winrtc.focus();
			} else // has been defined
			    if (!winrtc.closed) {     // still open
			    	winrtc.close();
			    	winrtc = window.open(url, "", "width=920,height=470,top=0,left=0,resizable=yes,scrollbars=yes,status=no"); //FB 2735
					winrtc.focus();
				} else {
				winrtc = window.open(url, "", "width=920,height=470,top=0,left=0,resizable=yes,scrollbars=yes,status=no"); //FB 2735
			        winrtc.focus();
				}
		}
	}
}

function chkdelBlankLine(str)
{
	newstr = ""
	strary = str.split("||"); // FB 1888
	for (var i=0; i < strary.length-1; i++) {
		sary = strary[i].split("!!"); // FB 1888 Starts
		if (sary[3]!="") {   
			newstr += strary[i] + "||"; // FB 1888 Ends
		}
	}
	return newstr;
}


function getGuest()
{	

   
    
	var url = ifrmMemberlist.location + "";
	//Code Changed for FB412 start
	//if(url.indexOf('group2member.asp') != -1){
	if(url.indexOf('group2member.aspx') != -1){
	//Code Changed for FB412 End
		willContinue = ifrmMemberlist.bfrRefresh(); 
		if (willContinue) {
			ifrmMemberlist.location.reload(); 
//			url = "dispatcher/conferencedispatcher.asp?frm=group&cmd=GetGuestList&emailListPage=1&wintype=pop"; //Login Management
            url = "emaillist2main.aspx?t=g&frm=group&wintype=ifr&fn=&n=";//Login Management
			if (!window.winrtc) {	// has not yet been defined
			    winrtc = window.open(url, "", "width=920,height=470,top=0,left=0,resizable=yes,scrollbars=yes,status=no"); //FB 2735
				winrtc.focus();
			} else // has been defined
			    if (!winrtc.closed) {     // still open
			    	winrtc.close();
			    	winrtc = window.open(url, "", "width=920,height=470,top=0,left=0,resizable=yes,scrollbars=yes,status=no"); //FB 2735
					winrtc.focus();
				} else {
				winrtc = window.open(url, "", "width=920,height=470,top=0,left=0,resizable=yes,scrollbars=yes,status=no"); //FB 2735
			        winrtc.focus();
				}
		}
	}
}


function delMember(pi_str)
{
	new_pi_str = "";

	pi_ary1 = pi_str.split("||"); // FB 1888
	for (var i=0; i < pi_ary1.length - 1; i++) {
		pi_ary2 = pi_ary1[i].split("!!"); // FB 1888
		if (pi_ary2[4]!="3") {
			new_pi_str += pi_ary1[i] + "||"; // FB 1888
		}
	}
	return (new_pi_str);
}


function getAGroupDetail(frm, cb, gid)
{
	if (cb == null) {
		if (gid != null) {
			id = gid;
		} else {
			alert("Sorry, system meets some error. Please notofy yoru administrator.")
			return false;
		}
	} else {

		if (cb.selectedIndex != -1) {
			if (gid != null) id = gid; else id = cb.options[cb.selectedIndex].value;
		} else {
			alert(EN_53);
			return false;
		}
	}
	
	//code added for Managegroup2.asp to aspx convertion 
	if(frm == 'g')
	    gm = window.open("memberallstatus.aspx?GroupID=" + id , "groupmember", "status=no,width=420,height=300,scrollbars=yes,resizable=no");
	else
	    //gm = window.open("memberallstatus.asp?f=" + frm + "&n=" + id + "&wintype=pop", "groupmember", "status=no,width=420,height=300,scrollbars=yes,resizable=yes");
	    gm = window.open("memberallstatus.aspx?GroupID=" + id , "groupmember", "status=no,width=420,height=300,scrollbars=yes,resizable=yes");
	if (gm) gm.focus(); else alert(EN_132);
}

//code added for Managegroup2.asp to aspx convertion 


function groupChg()
{
//alert(document.frmManagegroup2.PartysInfo.value)
	if (document.frmManagegroup2.UsersStr.value != "") {
		new_pi = "";
		var url = ifrmMemberlist.location + "";
		//Code changed for FB412 Start
		//if(url.indexOf('group2member.asp') != -1){
		if(url.indexOf('group2member.aspx') != -1){
		//Code changed for FB412 End
//alert(document.frmManagegroup2.PartysInfo.value)
			willContinue = ifrmMemberlist.bfrRefresh(); 
//alert(document.frmManagegroup2.PartysInfo.value)
			if (willContinue) {
				partysinfo = document.frmManagegroup2.PartysInfo.value
//alert("1:" + partysinfo);
				tmp_pi = delMember (partysinfo);
//alert("2:" + tmp_pi);

				usersinfo = document.frmManagegroup2.UsersStr.value;
				usersary = usersinfo.split("``"); // FB 1888 Starts
				pi_ary1 = tmp_pi.split("||"); 
				k = 0; pi_ary2 = pi_ary1[k].split("!!"); // FB 1888 Ends
//alert("pi_ary2:" + pi_ary2 + " pi_ary2[4]=" + pi_ary2[4]);

				while ( (pi_ary2[4]=="0") && (k < pi_ary1.length-1) ) {
					new_pi += pi_ary1[k] + "||"; // FB 1888
					k++;
					pi_ary2 = pi_ary1[k].split("!!"); // FB 1888
				}
//alert("pi_ary2:" + pi_ary2);

				for (var i=0; i < usersary.length; i++) {
					if (document.frmManagegroup2.Group.options[i].selected) {
						partysary = usersary[i].split("||"); // FB 1888
						for (var j=0; j < partysary.length-1; j++) {
							partyary = partysary[j].split("!!"); // FB 1888  Starts
							if ( (tmp_pi.indexOf("!!" + partyary[3] + "!!") == -1) && (new_pi.indexOf("!!" + partyary[3] + "!!") == -1) ) {
								new_pi += partyary[0] + "!!" + partyary[1] + "!!" + partyary[2] + "!!" + partyary[3] + "!!3||"; // FB 1888 Ends
							}
						}
					}
				}

//alert("2:" + new_pi);
				while (k < pi_ary1.length-1) {
					new_pi += pi_ary1[k] + "||"; // FB 1888
					k++;
					pi_ary2 = pi_ary1[k].split("!!"); // FB 1888
				}
	
//alert("2:" + new_pi);
				document.frmManagegroup2.PartysInfo.value = new_pi;
				ifrmMemberlist.location.reload(); 
			}
		}
	}
}


