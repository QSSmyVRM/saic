'******************************************************************************
' getOutLookEmailList
'
'	Pull all the email address out of the local Outlook address book and 
'	show in a pop up window
'******************************************************************************
Function getOutLookEmailList()
	msgbox "Please make sure Outlook is correctly installed and opened." & Chr(10) & "Click OK to continue and click Yes if asking ActiveX interaction." & Chr(10) & "If nothing happens, please check IE setting or contact administrator.", vbExclamation, "Outlook Warning"

	if instr(1,ifrmMemberlist.location,"group2member.asp") <> 0 then
		willContinue = ifrmMemberlist.bfrRefresh()
		if (willContinue) then
			ifrmMemberlist.location.reload()
			document.frmManagegroup2.lotusEmails.value = ""
			document.frmManagegroup2.outlookEmails.value = ContactsAsXML()
			'Code Modified for FB 412- Start -Outlook look up program
	        'url = "preoutlookemaillist2.asp?frm=group&page=1&wintype=pop"
	        url = "preoutlookemaillist2.aspx?frm=group&page=1&wintype=pop"			
	        'Code Modified for FB 412- End -Outlook look up program
            window.open url, "EmailList", "width=700, height=450, resizable=yes, toolbar=no, menubar=no, location=no, directories=no, scrollbars=yes"

		end if
	end if
End Function



'******************************************************************************
' getLotusEmailList
'
'	Pull all the email address out of the local Outlook address book and 
'	show in a pop up window
'******************************************************************************
Function getLotusEmailList(loginName, loginPwd, dbPath)
    'Code changed for FB412 -Start
	'if instr(1,ifrmMemberlist.location,"group2member.asp") <> 0 then
	if instr(1,ifrmMemberlist.location,"group2member.aspx") <> 0 then
	'Code changed for FB412 -End
		willContinue = ifrmMemberlist.bfrRefresh()
		if (willContinue) then
			ifrmMemberlist.location.reload()

			document.frmManagegroup2.outlookEmails.value = ""
			document.frmManagegroup2.lotusEmails.value = lnContactsAsXML(loginName, loginPwd, dbPath)
			url = "prelotusemaillist2.asp?frm=group&page=1&wintype=pop"
			window.open url, "EmailList", "width=700, height=450, resizable=yes, toolbar=no, menubar=no, location=no, directories=no, scrollbars=yes"

		end if
	end if
End Function