/*ZD 100147 Start*/
/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
/*ZD 100147 End*/
// max 143

//user info
var HLP_T_S = "Viewing_Scheduled_Conferences.htm";
var HLP_2 = "Please enter a Password.";
var HLP_3 = "Your entries for Password do not match.";
var HLP_4 = "Please enter a First Name.";
var HLP_5 = "Please enter a Last Name.";
var HLP_6 = "Please enter a valid e-mail address.";
var HLP_51 = "Please select a User from the User List.";
var HLP_81 = "Please enter a valid Company Email";
var HLP_89 = "Please enter your Full Name."
var HLP_90 = "Please enter a Subject";
var HLP_91 = "Please enter a Comment."
