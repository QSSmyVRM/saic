/*ZD 100147 Start*/
/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
/*ZD 100147 End*/
var loc_chkno_rmid = ""; 	// for find correct checkbox(chkno) by roomid (rmid)

function generateLocTree (str, showl2only, displaymod,rmType)//Edited For FB 1428
{
	var chkno = 0;

	if ((location.href).indexOf("user2loc.asp") != -1)
		cs = "," + queryField("cursel") + ","
	else
		cs = queryField("cursel")

	csary = cs.split(",")

	str = str.replace(/'/g, "&#39;")
	str = str.replace(/"/g, "&#39;&#39;")

	selfid = 0;		fathid = -1;	l3fathid = selfid;

	if (displaymod == "2") {
		l3img = "image/loc/room2.gif";	l2img = "";	l1img = "";
	} else {
		l3img = "image/loc/city2.gif";	l2img = "image/loc/building2.gif";	l1img = "image/loc/room2.gif";
	}
	
	nonvideo_locnames = "";
	
	locstr = (str.split("!"))[1];
	
	loc = new locTree('loc', displaymod);
	//loc.add(selfid,fathid,'Conference rooms',"-2","","","","","",4);
	loc.add(selfid,fathid,rmType,"-2","","","","","",4); //changed //Edited For FB 1428
	locary = locstr.split("|");
	for (i=0; i<locary.length; i++) {
		locary[i] = locary[i].split("^");

		locary[i][0] = locary[i][0].split("?");

		if (displaymod == "2") {
		
			// switch the display mode not affect already selected.
			seled = locary[i][0][2]
			for (var v=1; v<csary.length-1; v++) {
				if (parseInt(locary[i][0][0]) == parseInt(csary[v])) {
					seled = "1"
					break;
				} else {
					seled = "0"
				}
			}
//			if (queryField("f").indexOf("frmCalendar") != 0) {
//				tmpary = locary[i][0][3].split("`")
//				tmpstr = tmpary[0] + "`" + tmpary[1] + "`" + tmpary[2] + "`" + tmpary[3] + "`0`0";
				// remove locked and deleted icon of room
//				selfid++;	l2fathid = selfid;
//				loc.add(selfid,l3fathid,locary[i][0][1],locary[i][0][0], seled, tmpstr + '`' + locary[i][0][1],l3img,l3img,0,3);

//				chkno++;
//				loc_chkno_rmid += chkno + "," + locary[i][0][0] + ";"
//			} else {
				selfid++;	l2fathid = selfid;
				loc.add(selfid,l3fathid,locary[i][0][1],locary[i][0][0], seled,locary[i][0][3] + '`' + locary[i][0][1],l3img,l3img,0,3);
				
				chkno++;
				loc_chkno_rmid += chkno + "," + locary[i][0][0] + ";"
//			}
		} else {
			selfid++;	l2fathid = selfid;
			loc.add(selfid,l3fathid,locary[i][0][1],locary[i][0][0],'','',l3img,l3img,0,3);
		}
		
		if (locary[i].length == 2) {
			locary[i][1] = locary[i][1].split("%");
			for (j=0; j<locary[i][1].length; j++) { 
				locary[i][1][j] = locary[i][1][j].split("+");
				
				locary[i][1][j][0] = locary[i][1][j][0].split("?");
				selfid++;	l1fathid = selfid;

				loc.add(selfid,l2fathid,locary[i][1][j][0][1],locary[i][1][j][0][0],( (showl2only) ? '2' : '' ),'',l2img,l2img,0,2);

				if (locary[i][1][j].length == 2) {
					locary[i][1][j][1] = locary[i][1][j][1].split("=");
					for (k=0; k<locary[i][1][j][1].length; k++) {
						locary[i][1][j][1][k] = locary[i][1][j][1][k].split("?");


						// switch the display mode not affect already selected.
						seled = locary[i][1][j][1][k][2]
						for (var v=1; v<csary.length-1; v++) {
							if (parseInt(locary[i][1][j][1][k][0], 10) == parseInt(csary[v], 10)) {
								seled = "1"
								break;
							} else {
								seled = "0"
							}
						}
		

//						if (queryField("f").indexOf("frmCalendar") != 0) {
//							tmpary = locary[i][1][j][1][k][3].split("`")
//							tmpstr = tmpary[0] + "`" + tmpary[1] + "`" + tmpary[2] + "`" + tmpary[3] + "`0`0";
//							// remove locked and deleted icon of room
//							selfid++;
//							loc.add(selfid,l1fathid,locary[i][1][j][1][k][1],locary[i][1][j][1][k][0],seled,tmpstr + '`' + locary[i][1][j][1][k][1],l1img,l1img,0,1);
//							chkno++;
//							loc_chkno_rmid += chkno + "," + locary[i][1][j][1][k][0] + ";"
//						} else {
							if (locary[i][1][j][1][k] != "") {
								selfid++;
								loc.add(selfid,l1fathid,locary[i][1][j][1][k][1],locary[i][1][j][1][k][0],seled,locary[i][1][j][1][k][3] + '`' + locary[i][1][j][1][k][1],l1img,l1img,0,1);
								chkno++;
								loc_chkno_rmid += chkno + "," + locary[i][1][j][1][k][0] + ";"
							}
//						}
						nonvideo_locnames += (locary[i][1][j][1][k][2] == "1") ? ( (locary[i][1][j][1][k][3].split("`")[3] == "0") ? (locary[i][1][j][1][k][0] + "`" + locary[i][1][j][1][k][1] + "%") : "") : "";
					}
				}
	
			}
		}
	}
	
	document.frmSettings2loc.nonvideolocname.value = "%" + nonvideo_locnames;

	document.write(loc);
	
	if (displaymod == "2") {
		loc.chkrootstatus();
	} else
		chkPnodes(loc);
};


function chkPnodes(objloc)
{
	for (var i=0; i<objloc.aNodes.length; i++) {
		if (objloc.aNodes[i].level == 2) {
			objloc.chkpnodestatus(objloc.aNodes[i])
		}
	}
}


function delStringRev(str, substr)
{
	if ( (str != "") && (substr != "") ) {
		if ( str.lastIndexOf(substr) == (str.length - substr.length) )
			return( str.substr(0, str.length - substr.length) )
		else
			return (str)
	} else 
		return str
}


function getRoomDisplayList (roomtreestr)
{

	roomliststr = ""
	
	selrm = (roomtreestr.split("!"))[0]


	selrmary = selrm.split(";")

	for (i = 0; i < selrmary.length; i++) {
		selrmary[i] = selrmary[i].split(",")
		roomliststr += selrmary[i][0] + ";"
	}
	roomliststr += "!"

	sortrmlist = ""
	rmlist = ""
	locstr = (roomtreestr.split("!"))[1];
	locary = locstr.split("|");
	for (i=0; i<locary.length; i++) {
		locary[i] = locary[i].split("^");
		if (typeof(locary[i][1])!= "undefined") {
		locary[i][1] = locary[i][1].split("%");
		for (j=0; j<locary[i][1].length; j++) { 
		  if (locary[i][1][j].indexOf("+") >=0) {
			locary[i][1][j] = locary[i][1][j].split("+");


			locary[i][1][j][1] = locary[i][1][j][1].split("=");

			for (k=0; k<locary[i][1][j][1].length; k++) {
				rmlist += locary[i][1][j][1][k] + "|";
				
				locary[i][1][j][1][k] = locary[i][1][j][1][k].split("?");
				sortrmlist += locary[i][1][j][1][k][1] + "??" + locary[i][1][j][1][k][0] + "||";
			}
		   } 
		} }
	}
	

	rmlist = delStringRev(rmlist, "|")
	sortrmlist = delStringRev(sortrmlist, "||")
	sortrmlistary = sortrmlist.split("||")
	sortrmlistary.sort();

	for (i = 0; i < sortrmlistary.length; i++) {
		rmid = sortrmlistary[i].split("??")[1];

		rmary = rmlist.split("|")
		for (j = 0; j < rmary.length; j++) {
			rmstr = rmary[j];
			rmary[j] = rmary[j].split("?")
			if (rmary[j][0] == rmid) {
				roomliststr += rmstr + "|";
				break;
			}
		}
	}
	
	roomliststr = delStringRev(roomliststr, "|")

	return (roomliststr)
}
