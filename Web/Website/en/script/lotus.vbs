'/*ZD 100147 Start*/
'/* Copyright (C) 2013 myVRM - All Rights Reserved
'* You may not use, distribute and/or modify this code under the
'* terms of the myVRM license.
'*
'* You should have received a copy of the myVRM license with
'* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
'*/
'/*ZD 100147 End*/

'++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
'Function Name: lnContactsAsXML()
'Description: retrieve all contacts from local Lotus Notes address book
'Input: notes username, notes password, notes database file name
'Output: xml string
'Developed by: A
'Last Modified Date: 3/23/04
'++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

Function lnContactsAsXML(username, password, dbPath)
	Dim objNotesSession
	set objNotesSession = CreateObject("Lotus.NotesSession")
	Dim objNotesDB, objNotesView, objNotesDoc, objContact
	Dim fname, lname, email
    msgbox (err.Description)
	On Error Resume Next
	
'===========================================================================
'validate if any critical input info. are missing in the following section
'===========================================================================
	if (isNull(username)) or username = ""  then
		xml = "<error> Username not supplied </error>"
		lnContactsAsXML = xml
		Exit Function
	end if

	if (isNull(password)) or password = ""  then
		xml = "<error> Password not supplied </error>"
		lnContactsAsXML = xml
		Exit Function
	end if

	if (isNull(dbPath)) or dbPath = "" then
		dbPath = "names.nsf"
	end if
'===========================================================================
'===========================================================================

	'initilize the notes database
	call objNotesSession.initialize("")
	
	if (isNull(objNotesSession)) then
		xml = "<error> Failed session initialization </error>"
		lnContactsAsXML = xml
		Exit Function
	end if

	'open notes db
	Set objNotesDB = objNotesSession.GetDatabase("", dbPath)
	
	if (isNull(objNotesDB)) then
		xml = "<error> User address database not found </error>"
		lnContactsAsXML = xml
		Exit Function
	end if
	
	If Not (objNotesDB.IsOpen) then
		Call objNotesDB.Open
	end if

	'get contact view
	Set objNotesView = objNotesDB.GetView("Contacts")
	if (isNull(objNotesView)) then
		xml = "<error> Error obtaining contacts view </error>"
		lnContactsAsXML = xml
		Exit Function
	end if
	
	'get the name list
	Set objNotesDoc = objNotesView.GetFirstDocument()

	xml = "<users>"
	
'===========================================================================
'generate the name list in XML in the following section
'===========================================================================
	Do While Not (objNotesDoc Is Nothing)
	    Set objContact = objNotesDoc
		
		xml = xml + "<user>"
		xml = xml + "<userName>"
		xml = xml + "<userFirstName>"

		If objContact.HasItem("FirstName") Then
			fname = objContact.GetItemValue("FirstName")(0)
		Else
			fname = ""
		end if

		If objContact.HasItem("LastName") Then
			lname = objContact.GetItemValue("LastName")(0)
		Else
			lname = ""
		end if
				
		If objContact.HasItem("MailAddress") Then
			email = objContact.GetItemValue("MailAddress")(0)
		Else
			email = ""
		end if

		xml = xml + fname
		xml = xml + "</userFirstName>"
		xml = xml + "<userLastName>"
		xml = xml + lname
		xml = xml + "</userLastName>"
		xml = xml + "</userName>"
		xml = xml + "<userEmail>"
		xml = xml + email
		xml = xml + "</userEmail>"
		xml = xml + "</user>"
		
		Set objNotesDoc = objNotesView.GetNextDocument(objNotesDoc)
	Loop

	xml = xml + "</users>"
'===========================================================================
'===========================================================================
	'clean up	
	set objNotesSession = nothing
	set objNotesDB = nothing
	set objNotesView = nothing
	set objNotesDoc = nothing
	set objContact = nothing

	lnContactsAsXML = xml

End Function



'++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
'Function Name: lnFindContacts()
'Description: search contact in local Lotus Notes address book using first and/or last name
'Input: strFirstName, strLastName, doCaseSensitive, similarTo, username, password, dbPath
'Output: xml string
'Developed by: A
'Last Modified Date: 3/23/04
'++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

Function lnFindContacts(strFirstName, strLastName, doCaseSensitive, similarTo, username, password, dbPath)

	Dim objNotesSession
	set objNotesSession = CreateObject("Lotus.NotesSession")
	Dim objNotesDB, objNotesView, objNotesDoc, objContact
	Dim fname, lname, email
	
	On Error Resume Next

	if (isNull(username)) or username = "" then
		xml = "<error> Username not supplied </error>"
		lnFindContacts = xml
		Exit Function
	end if

	if (isNull(password)) or password = "" then
		xml = "<error> Password not supplied </error>"
		lnFindContacts = xml
		Exit Function
	end if

	if (isNull(dbPath)) or dbPath = ""  then
		dbPath = "names.nsf"
	end if

	call objNotesSession.initialize("")
	
	if (isNull(objNotesSession)) then
		xml = "<error> Failed session initialization </error>"
		lnFindContacts = xml
		Exit Function
	end if

	Set objNotesDB = objNotesSession.GetDatabase("", dbPath)
	if (isNull(db)) then
		xml = "<error> User address database not found </error>"
		lnFindContacts = xml
		Exit Function
	end if
	
	If Not (objNotesDB.IsOpen) then
		Call objNotesDB.Open
	end if

	Set objNotesView = objNotesDB.GetView("Contacts")
	if (isNull(objNotesView)) then
		xml = "<error> Error obtaining contacts view </error>"
		lnFindContacts = xml
		Exit Function
	end if


	'check to see whether user supplies first name 
	'and/or last name as serach criteria
	if( not (IsNull( strFirstName) or (Len(strFirstName) = 0))) then	
		gotFirstName = true
	else
		gotFirstName = false
	end if
	
	if( not (IsNull( strLastName) or (Len(strLastName) = 0)))	then
		gotLastName = true
	else
		gotLastName = false
	end if

	Set objNotesDoc = objNotesView.GetFirstDocument()

	xml = "<users>"

'===========================================================================	
'check if result matches the search criteria, if found, add to the xml.
'===========================================================================
	do While Not (objNotesDoc Is Nothing)

	    Set objContact = objNotesDoc
				
		If objContact.HasItem("FirstName") Then
			fname = objContact.GetItemValue("FirstName")(0)
		Else
			fname = ""
		end if

		If objContact.HasItem("LastName") Then
			lname = objContact.GetItemValue("LastName")(0)
		Else
			lname = ""
		end if
		
		If objContact.HasItem("MailAddress") Then
			email = objContact.GetItemValue("MailAddress")(0)
		Else
			email = ""
		end if

		if(gotFirstName) then
			if( Compare(strFirstName,fname,doCaseSensitive,similarTo))then
				add = true
			else
				add = false
			end if
		end if
		
		if(gotLastName) then
			if( Compare(strLastName,lname,doCaseSensitive,similarTo)) then
				add = true
			else
				add = false
			end if
		end if

		if(add) then
			xml = xml + "<user>"
			xml = xml + "<userName>"
			xml = xml + "<userFirstName>"
			xml = xml + fname
			xml = xml + "</userFirstName>"
			xml = xml + "<userLastName>"
			xml = xml + lname
			xml = xml + "</userLastName>"
			xml = xml + "</userName>"
			xml = xml + "<userEmail>"
			xml = xml + email
			xml = xml + "</userEmail>"
			xml = xml + "</user>"
		end if
'===========================================================================
'===========================================================================

		Set objNotesDoc = objNotesView.GetNextDocument(objNotesDoc)
	Loop

	'Close the users tag.
	xml = xml + "</users>"

	'clean up	
	set objNotesSession = nothing
	set objNotesDB = nothing
	set objNotesView = nothing
	set objNotesDoc = nothing
	set objContact = nothing

	'Return search results in XML
	lnFindContacts = xml
End Function



'******************************************************************************
' lnDelAppointment
' 
'	Delete Apppointment
'
'
'	On Success:
'
'	<success>success message</success>
'
'
'
'	On Error:
'
'	<error> suitable error message </error>
'
'
'	username				Lotus username
'	password				Lotus password
'	dbfile					Mail database file of the user on Domino Server.
'							The name of this file is user dependant. It is usually
'							first character from first name followed by last name ans with a .nsf extension.
'							For example: for username = "Bill Gates", dbfile = bgates.nsf	
'	confID					The conference ID for which appointments needs to be deleted
'******************************************************************************
Function lnDelAppointment(username, password, dbfile, confID)

Dim s
set s = CreateObject("Lotus.NotesSession")
Dim db, vMeet, doc3, docCol

On Error Resume Next

if (isNull(username)) then
	xml = "<error> Username not supplied </error>"
	lnDelAppointment = xml
	Exit Function
end if

if (isNull(password)) then
	xml = "<error> Password not supplied </error>"
	lnDelAppointment = xml
	Exit Function
end if

if (isNull(dbfile)) then
	xml = "<error> Mail DB file not supplied </error>"
	lnDelAppointment = xml
	Exit Function
end if

if (isNull(confID)) then
	xml = "<error> Conference ID not supplied </error>"
	lnDelAppointment = xml
	Exit Function
end if

Call s.InitializeUsingNotesUserName(username, password)

if (isNull(s)) then
	xml = "<error> Failed session initialization </error>"
	lnDelAppointment = xml
	Exit Function
end if

Set db = s.GetDatabase("", dbfile)
if (isNull(db)) then
	xml = "<error> User mail database not found </error>"
	lnDelAppointment = xml
	Exit Function
end if

If Not (db.IsOpen) then
	Call db.Open
end if

searchFormula = "Form = ""Appointment"" & ConfID = ""125"" "
Set docCol = db.Search(searchFormula, Nothing, 0)

For i = 1 To docCol.Count

    Set doc3 = docCol.GetNthDocument(i)

    If Not doc3 Is Nothing Then
        Call doc3.Remove(True)
    End If

Next
lnDelAppointment = "<success>Appointment deleted successfully</success>"

End Function



