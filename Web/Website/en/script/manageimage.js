/*ZD 100147 Start*/
/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
/*ZD 100147 End*/
var userfilearray = new Array();
var oparray = new Array();

function imagemanage_prompt(promptpicture, prompttitle, imgpath) 
{ 
	promptbox = document.createElement('div'); 
	promptbox.setAttribute ('id' , 'prompt');
	document.getElementsByTagName('body')[0].appendChild(promptbox);
	promptbox = eval("document.getElementById('prompt').style");

	w = 500;
	promptbox.position = 'absolute';
	document.getElementById('prompt').style.top = mousedownY - 400;
	document.getElementById('prompt').style.left = mousedownX - w - 50;
	promptbox.width = w;
	promptbox.border = 'outset 1 #bbbbbb'; 


	m  = "<table cellspacing='0' cellpadding='0' border='0' width='100%'><tr valign='middle'><td width='22' height='22' style='text-indent:2;' class='titlebar'><img src='" + promptpicture + "' height='18' width='18'></td><td class='titlebar'>" + prompttitle + "</td></tr></table>" 
	
	m += "<table border=0>";
	m += "  <tr>";
	m += "    <td width=150 height=400 align=left valign=top>";
	m += "      <iframe src='http://www.google.com' name='ifrmRoomimages' width='150' height='100%'>";
	m += "        <p>Loading page</p>";
	m += "      </iframe>";
	m += "    </td>";
	m += "    <td width=5></td>";
	m += "    <td width=400 align=width valign=top>";
	
	m += "      <table border=0 width=100%>";
	m += "        <tr>";
	m += "          <td align=center><img width='400' height='300'></td>";
	m += "        </tr>"
	m += "        <tr>";
	m += "          <td align=left><input type='button' class='prompt' value='Delete' onMouseOver='this.style.border=\"1 outset #dddddd\"' onMouseOut='this.style.border=\"1 solid transparent\"' onClick='deleteimg(" + imgpath + ");'></td>";
	m += "        </tr>";
	m += "        <tr><td>";
	m += "          Room Image <input type='text' name='fimgname' value='' style='width: 200pt'>";
	m += "          <img src='image/browse.gif' onClick='JavaScript: openbrowse();'>";
	m += "        </td></tr>"
	m += "        <tr>";
	m += "          <td align=right>";
	m += "            <input type='button' class='longprompt' value='Confirm & Upload' onMouseOver='this.style.border=\"1 outset #dddddd\"' onMouseOut='this.style.border=\"1 solid transparent\"' onClick='uploadfiles(1);'>"
	m += "            <input type='button' class='prompt' value='Cancel' onMouseOver='this.style.border=\"1 outset transparent\"' onMouseOut='this.style.border=\"1 solid transparent\"' onClick='document.getElementsByTagName(\"body\")[0].removeChild(document.getElementById(\"prompt\"))'>"
	m += "          </td>";
	m += "        </tr>"
	m += "      </table>" 

	m += "    </td>";
	m += "  </tr>"
	m += "</table>" 

	document.getElementById('prompt').innerHTML = m;
} 



function uploadfiles(fnum) 
{
	var i, j;
	var hasupload = false;
	var processstr = "";

	for (i=1; i<=fnum; i++) {
		cb = document.getElementById("userFile" + i);
		
		if (!cb) {
			oparray[i] = false;
			userfilearray[i] = "";
			continue;
		}
		
		oparray[i] = true;
		userfilearray[i] = Trim(cb.value);
		
		for (j = 1; j < i; j++) {
			if ( (userfilearray[i] == userfilearray[j]) && (userfilearray[i] != "") ) {
				alert(EN_176);
				return -1;
			}
		}
	}
		
	for (i=1; i<userfilearray.length; i++) {
		if (!oparray[i])
			continue;
			
		if (userfilearray[i] == "") {
			
			if ( (v = document.getElementById("conffile" + i).value) != "" ) {
				processstr += "</td><td align=center valign=top><image src='image/arrow.gif' border=0></td><td>file " + i + " (" + getUploadFileName(v) + ") : remove ... <u><i>done</u></i></td></tr><tr><td>";
			}
			
			document.getElementById("conffile" + i).value = "";
			continue;
		}
		
		processstr += "</td><td align=center valign=top><image src='image/arrow.gif' border=0></td><td>file " + i + " (" + getUploadFileName(userfilearray[i]) + ") : upload ... processing</td></tr><tr><td>";
		hasupload = true;
	}
	
	if (processstr == "") {
		document.getElementsByTagName("body")[0].removeChild(document.getElementById("prompt"));
		return -1;
	}
	
	processstr += (hasupload) ? "</td></tr><tr><td colspan=3>&nbsp;&nbsp;&nbsp;&nbsp;<img border='0' src='image/wait1.gif'>" : "</td></tr><tr><td align=right colspan=3><input type='button' class='prompt' value='Close' onMouseOver='this.style.border=\"1 outset transparent\"' onMouseOut='this.style.border=\"1 solid transparent\"' onClick='document.getElementsByTagName(\"body\")[0].removeChild(document.getElementById(\"prompt\"))'>";
	
	document.getElementById('dlgcontent').innerHTML = "<table><tr><td>" + processstr + "</td></tr></table>";
	
	if (hasupload) {
		if (ifrmPreloading == null) {
			alert("Error: can not support file upload now. Please notify administrator about this.");
			document.getElementsByTagName("body")[0].removeChild(document.getElementById("prompt"));
			return -1;
		}

		ifrmPreloading.frmsubmit();
	} else {
		genuploadfilestr ();
	}
} 



function chgfile (fno)
{
	document.getElementById("userfile" + fno + "DIV").innerHTML = "<input type='text' name='userFile" + fno + "' value='' style='width: 220px' onClick='JavaScript: openbrowse(" + fno + ");' readOnly> <img src='image/browse.gif' onClick='JavaScript: openbrowse(" + fno + ");'>";
}



function rmvfile(fno)
{
	var isRemove = confirm(EN_174);
	if (isRemove) {
		chgfile (fno);
	}
}


function openbrowse(fno)
{
	if (ifrmPreloading == null) {
		alert("Error: can not support file upload now. Please notify administrator about this.");
		document.getElementsByTagName("body")[0].removeChild(document.getElementById("prompt"));
		return -1;
	}

	ifrmPreloading.document.getElementById("File" + fno).click();
}


function finishupload(f, infrmlmt, inf1lmt, inf2lmt, inf3lmt, strfrmlmt, strf1lmt, strf2lmt, strf3lmt)
{
	var processstr = "";
	
//alert("infrmlmt=" + infrmlmt +"; inf1lmt=" + inf1lmt +"; inf2lmt=" + inf2lmt +"; inf3lmt=" + inf3lmt +"; ");
	if (!infrmlmt) {
		processstr = "</td><td align=center valign=top><image src='image/arrow.gif' border=0></td><td>file(s) uploads <u><i>failed</u></i>: total size over limit[" + strfrmlmt + "], no files uploaded.</td></tr><tr><td>";
	}
	
	for (i=1; i<userfilearray.length; i++) {
		if (!oparray[i])
			continue;
			
		if (userfilearray[i] == "") {
			
			if ( (v = document.getElementById("conffile" + i).value) != "" ) {
				processstr += "</td><td align=center valign=top><image src='image/arrow.gif' border=0></td><td>file " + i + " (" + getUploadFileName(v) + ") : remove ... <u><i>done</u></i></td></tr><tr><td>";
			}
			
			continue;
		}
//alert("inf" + i + "lmt=" + eval("inf" + i + "lmt"))
		if (infrmlmt) {
			processstr += "</td><td align=center valign=top><image src='image/arrow.gif' border=0></td><td>file " + i + " (" + getUploadFileName(userfilearray[i]) + ") : upload ... "
			processstr += (eval("inf" + i + "lmt")) ? "<u><i>done</u>" : ( "<u><i>failed</u></i>: file size over limit[" + eval("strf" + i + "lmt") + "]" );
			processstr += "</i></td></tr><tr><td>";
		}

	}
//alert(processstr)
	if (processstr == "") {
		document.getElementsByTagName("body")[0].removeChild(document.getElementById("prompt"));
		return -1;
	}
	
	processstr += "</td></tr><tr><td align=right colspan=3><input type='button' class='prompt' value='Close' onMouseOver='this.style.border=\"1 outset transparent\"' onMouseOut='this.style.border=\"1 solid transparent\"' onClick='document.getElementsByTagName(\"body\")[0].removeChild(document.getElementById(\"prompt\"))'>";
	
	document.getElementById('dlgcontent').innerHTML = "<table><tr><td>" + processstr + "</td></tr></table>";
}


function failupload(f)
{
	var processstr = "";

	processstr += "Error: file upload failed.</td></tr><tr><td align=right><input type='button' class='prompt' value='Close' onMouseOver='this.style.border=\"1 outset transparent\"' onMouseOut='this.style.border=\"1 solid transparent\"' onClick='document.getElementsByTagName(\"body\")[0].removeChild(document.getElementById(\"prompt\"))'>";
	
	document.getElementById('dlgcontent').innerHTML = "<table><tr><td>" + processstr + "</td></tr></table>";
	
	switch (f) {
		case "1" :
			genuploadfilestr ();
			break;
		case "2" :
		case "3" :
			break;
	}
}