/*ZD 100147 Start*/
/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
/*ZD 100147 End*/
var mousedownX = 0;
var mousedownY = 0;

// If it is not IE, we assume that the browser is NS.
var IE = document.all ? true : false

// NS
if (!IE) document.captureEvents(Event.MOUSEMOVE);

// Set-up to use getMouseXY function mouse click
document.onmousedown  = getMouseXY;
document.onmouseover = getMouseXY;
function getMouseXY(e) 
{
	// Set-up to use getMouseXY function onMouseMove
	var IE = document.all ? true : false;
	if (IE) {		// IE
	    //alert(document.documentElement.scrollTop);
	    if (event)
	    {
		    mousedownX = event.clientX + document.documentElement.scrollLeft; //event.clientX;
		    mousedownY = event.clientY + document.documentElement.scrollTop; //event.clientY;
		}
	} else {		// NS
		mousedownX = e.pageX
		mousedownY = e.pageY
	}  
	// catch possible negative values in NS4
	mousedownX = ( (mousedownX < 0) ? 0 : mousedownX );
	mousedownY = ( (mousedownY < 0) ? 0 : mousedownY );

	return true
}
