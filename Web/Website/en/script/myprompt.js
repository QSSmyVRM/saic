/*ZD 100147 Start*/
/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
/*ZD 100147 End*/
function my_prompt(promptpicture, prompttitle, message, sendto) 
{ 
	promptbox = document.createElement('div'); 
	promptbox.setAttribute ('id' , 'prompt') 
	document.getElementsByTagName('body')[0].appendChild(promptbox) 
	promptbox = eval("document.getElementById('prompt').style") 

	promptbox.position = 'relative'
	promptbox.top = -120 
	promptbox.left = 500 
	promptbox.width = 300 
	promptbox.border = 'outset 1 #bbbbbb' 

	m = "<table cellspacing='0' cellpadding='0' border='0' width='100%'><tr valign='middle'><td width='22' height='22' style='text-indent:2;' class='titlebar'><img src='" + promptpicture + "' height='18' width='18'></td><td class='titlebar'>" + prompttitle + "</td></tr></table>" 
	m += "<table cellspacing='0' cellpadding='0' border='0' width='100%' class='promptbox'>"
	m += "  <tr><td>" + message + "</td></tr>"
	m += "  <tr><td align='right'>"
	m += "    <input type='button' class='prompt' value='Yes' onMouseOver='this.style.border=\"1 outset #dddddd\"' onMouseOut='this.style.border=\"1 solid transparent\"' onClick='" + sendto + "(1);'>"
	m += "    <input type='button' class='prompt' value='No' onMouseOver='this.style.border=\"1 outset transparent\"' onMouseOut='this.style.border=\"1 solid transparent\"' onClick='" + sendto + "(0);'>"
	m += "  </td></tr>"
	m += "</table>" 
	
	document.getElementById('prompt').innerHTML = m;
} 

function myfunction(v) 
{ 
	document.frmMainadminiatrator.needsave.value = v;
	if (v == "1") {
		if (frmMainadminiatrator_Validator())
			document.frmMainadminiatrator.submit();
		else
			document.getElementById('prompt').style.display = "none";
	} else
		document.frmMainadminiatrator.submit();
} 
