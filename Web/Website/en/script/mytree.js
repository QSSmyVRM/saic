/*ZD 100147 Start*/
/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
/*ZD 100147 End*/
/*--------------------------------------------------|
| dTree 2.05 | www.destroydrop.com/javascript/tree/ |
|---------------------------------------------------|
| Copyright (c) 2002-2003 Geir Landr�               |
|                                                   |
| This script can be used freely as long as all     |
| copyright messages are intact.                    |
|                                                   |
| Updated: 17.04.2003                               |
|--------------------------------------------------*/

// Node object
function Node(id, pid, name, url, title, target, icon, iconOpen, open, level) {
	this.id = id;
	this.pid = pid;
	this.name = name;
	this.url = url;
	this.title = title;
	this.target = target;
	this.icon = icon;
	this.iconOpen = iconOpen;
	this.level = level;
	this._io = open || false;
	this._is = false;
	this._ls = false;
	this._hc = false;
	this._ai = 0;
	this._p;
};

// Tree object
function locTree(objName, mod) {
	this.config = {
		target				: null,
		folderLinks			: true,
		useSelection		: true,
		useCookies			: true,
		useLines			: true,
		useIcons			: false, ///true,
		useStatusText		: false,
		closeSameLevel		: false,
		inOrder				: false
	}
	this.icon = {
		root				: 'image/loc/base.gif',
		folder				: 'image/loc/building2.gif',
		folderOpen			: 'image/loc/folderopen.gif',
		node				: 'image/loc/room2.gif',
		empty				: 'image/loc/empty2.gif',
		line				: 'image/loc/line2.gif',
		join				: 'image/loc/join2.gif',
		joinBottom			: 'image/loc/joinbottom2.gif', 
		plus				: 'image/loc/plus2.gif', 
		plusBottom			: 'image/loc/plusbottom2.gif',
		minus				: 'image/loc/minus2.gif',
		minusBottom			: 'image/loc/minusbottom2.gif',
		nlPlus				: 'image/loc/nolines_plus2.gif',
		nlMinus				: 'image/loc/nolines_minus2.gif'
	};

	this.obj = objName;
	this.aNodes = [];
	this.aIndent = [];
	this.root = new Node(-1);
	this.selectedNode = null;
	this.selectedFound = false;
	this.completed = false;
	this.displaymode = mod;
};

// Adds a new node to the node array
locTree.prototype.add = function(id, pid, name, url, title, target, icon, iconOpen, open, level) {
	this.aNodes[this.aNodes.length] = new Node(id, pid, name, url, title, target, icon, iconOpen, open, level);
};

// Open/close all nodes
locTree.prototype.openAll = function() {
	this.oAll(true);
};

locTree.prototype.closeAll = function() {
	this.oAll(false);
};

locTree.prototype.selectAll = function() {
	this.sAll(true);
};

locTree.prototype.deselectAll = function() {
	this.sAll(false);
};
// Outputs the tree to the page
locTree.prototype.toString = function() {
	var str = '<div class="loctree">\n';
	if (document.getElementById) {
		if (this.config.useCookies) this.selectedNode = this.getSelected();
		str += this.addNode(this.root);
	} else str += 'Browser not supported.';
	str += '</div>';
	if (!this.selectedFound) this.selectedNode = null;
	this.completed = true;
	return str;
};

// Creates the tree structure
locTree.prototype.addNode = function(pNode) {
	var str = '';
	var n=0;
	if (this.config.inOrder) n = pNode._ai;
	for (n; n<this.aNodes.length; n++) {
		if (this.aNodes[n].pid == pNode.id) {
			var cn = this.aNodes[n];
			cn._p = pNode;
			cn._ai = n;
			this.setCS(cn);
			if (!cn.target && this.config.target) cn.target = this.config.target;
			if (cn._hc && !cn._io && this.config.useCookies) cn._io = this.isOpen(cn.id);
			if (!this.config.folderLinks && cn._hc) cn.url = null;
			if (this.config.useSelection && cn.id == this.selectedNode && !this.selectedFound) {
					cn._is = true;
					this.selectedNode = n;
					this.selectedFound = true;
			}
			str += this.node(cn, n);
			if (cn._ls) break;
		}
	}
	return str;
};

// Creates the node icon, url and text
locTree.prototype.node = function(node, nodeId) {
	var str = '<div class="loctreeNode">' + this.indent(node, nodeId);
	var title = "click to show room resources.";
	var newnodename = "";
	
	if ((parent.location.href).indexOf("calendar") != -1)
		title = node.name + "\nClick to show room resources.";
	
	if (this.config.useIcons) {
		if (!node.icon) node.icon = (this.root.id == node.pid) ? this.icon.root : ((node._hc) ? this.icon.folder : this.icon.node);
		if (!node.iconOpen) node.iconOpen = (node._hc) ? this.icon.folderOpen : this.icon.node;
		if (this.root.id == node.pid) {
			node.icon = this.icon.root;
			node.iconOpen = this.icon.root;
		}
		str += '<img id="i' + this.obj + nodeId + '" src="' + ((node._io) ? node.iconOpen : node.icon) + '" alt="" />';
	}

	if (node.title) {  // 1 level
		if (node.title != "") {
			dlt = ( ((node.target).split("`"))[4]=="1" ) ? "<img border='0' src='image/deleted.gif' width='16' height='15' alt='this room is deleted' onClick='Javascript: reactiverm(\"" + node.url + "\")'>" : ""; 
			lck = ( ((node.target).split("`"))[5]=="1" ) ? "<img border='0' src='image/locked.gif' width='16' height='16' alt='this room is locked'>" : ""; 

			chk = ""
			if (node.title=="1") {

				if (queryField("f") == "frmSettings2") {
					if (parent.document.frmSettings2.AdvancedRoomAllocaiton)
						if (parseInt(parent.document.frmSettings2.AdvancedRoomAllocaiton.value))
							newnodename = getAdvRmName(node.url, node.name);
				}
				
///				chk += "<input id='s" + this.obj + nodeId + "' type=checkbox name=C1 value=1 checked onclick='Javascript: mkrst(\"" + (node.url) + "\",\"" + (node.target) + "\",this,1);'"
				chk += "<input id='s" + this.obj + nodeId + "' type=checkbox name='s" + this.obj + nodeId + "' value=1 checked onclick='Javascript: " + this.obj + ".os(" + nodeId + ");'";
			} else {
				chk += "<input id='s" + this.obj + nodeId + "' type=checkbox name='s" + this.obj + nodeId + "' value=1 onclick='Javascript: " + this.obj + ".os(" + nodeId + ");'";
//				chk += "<input id='s" + this.obj + nodeId + "' type=checkbox name='s" + this.obj + nodeId + "' value=1 onclick='Javascript: mkrst(\"" + (node.url) + "\",\"" + (node.target) + "\",this,1);'"

///				chk += "<input id='s" + this.obj + nodeId + "' type=checkbox name=C1 value=1 onclick='Javascript: mkrst(\"" + (node.url) + "\",\"" + (node.target) + "\",this,1);'"
			}
		} else {
			chk += "<a id='s" + this.obj + nodeId + "' class='" + ((this.config.useSelection) ? ((node._is ? "nodeSel" : "node")) : "node") + "' href='" + node.url + "' onClick='Javscript:eval(this.id).style.display=\"block\";'";
		}

		if (node.target) chk += " target='" + node.target + "'";
		if (this.config.useStatusText) chk += " onmouseover='window.status=\"" + node.name + "\";return true;' onmouseout='window.status=\"\";return true;' ";
		if (this.config.useSelection && ((node._hc && this.config.folderLinks) || !node._hc))
			chk += " onclick='javascript: " + this.obj + ".s(" + nodeId + ");'";
		chk += ">";

		str += ( (dlt == "") && (lck == "") ) ? chk : ((dlt != "") ? dlt : lck); 
		if (node.title != "") // && queryField("pub") != "1")
			str += '<a href="javascript:chkresource(\'' + (node.url) + '\');" title=\'' + title + '\' onmouseover="window.status=\'\';return true;">';
	}
	else {
		if ((!this.config.folderLinks || !node.url) && node._hc && node.pid != this.root.id) {
			str += '<a href="javascript: ' + this.obj + '.o(' + nodeId + ');" class="node">';
		} else {// 2+ level
			str += '<input type=checkbox name="n' + this.obj + nodeId + '" id="n' + this.obj + nodeId + '" value=1 onclick="javascript: ' + this.obj + '.so(' + nodeId + ');">';//"<input type=checkbox name='n" + this.obj + nodeId + "' id='n" + this.obj + nodeId + "' value=1 onclick='javascript:mkrstNode(\"" + (node.url) + "\",\"" + (node.target) + "\",this);'>";
		}
	}
	
	str += (newnodename == "") ? node.name : newnodename;

	if (node.title)
		if (node.title != "")
			str += '</a>';

	if (node.url || ((!this.config.folderLinks || !node.url) && node._hc)) str += '</a>';
	str += '</div>';
	if (node._hc) {
		str += '<div id="d' + this.obj + nodeId + '" class="clip" style="display:' + ((this.root.id == node.pid || node._io) ? 'block' : 'none') + ';">';
		str += this.addNode(node);
		str += '</div>';
	}
	this.aIndent.pop();
	return str;
};

// Adds the empty and line icons
locTree.prototype.indent = function(node, nodeId) {
	var str = '';
	if (this.root.id != node.pid) {
		for (var n=0; n<this.aIndent.length; n++)
			str += '<img src="' + ( (this.aIndent[n] == 1 && this.config.useLines) ? this.icon.line : this.icon.empty ) + '" alt="" />';
		(node._ls) ? this.aIndent.push(0) : this.aIndent.push(1);
		if (node._hc) {
			str += '<a href="javascript: ' + this.obj + '.o(' + nodeId + ');"><img id="j' + this.obj + nodeId + '" src="';
			if (!this.config.useLines) str += (node._io) ? this.icon.nlMinus : this.icon.nlPlus;
			else str += ( (node._io) ? ((node._ls && this.config.useLines) ? this.icon.minusBottom : this.icon.minus) : ((node._ls && this.config.useLines) ? this.icon.plusBottom : this.icon.plus ) );
			str += '" alt="" /></a>';
		} else str += '<img src="' + ( (this.config.useLines) ? ((node._ls) ? this.icon.joinBottom : this.icon.join ) : this.icon.empty) + '" alt="" />';
	}
	return str;
};

// Checks if a node has any children and if it is the last sibling
locTree.prototype.setCS = function(node) {
	var lastId;
	for (var n=0; n<this.aNodes.length; n++) {
		if (this.aNodes[n].pid == node.id) node._hc = true;
		if (this.aNodes[n].pid == node.pid) lastId = this.aNodes[n].id;
	}
	if (lastId==node.id) node._ls = true;
};

// Returns the selected node
locTree.prototype.getSelected = function() {
	var sn = this.getCookie('cs' + this.obj);
	return (sn) ? sn : null;
};

// Highlights the selected node
locTree.prototype.s = function(id) {
	if (!this.config.useSelection) return;
	var cn = this.aNodes[id];
	if (cn._hc && !this.config.folderLinks) return;
	if (this.selectedNode != id) {
		if (this.selectedNode || this.selectedNode==0) {
			eOld = document.getElementById("s" + this.obj + this.selectedNode);
			eOld.className = "node";
		}
		eNew = document.getElementById("s" + this.obj + id);
		eNew.className = "nodeSel";
		this.selectedNode = id;
		if (this.config.useCookies) this.setCookie('cs' + this.obj, cn.id);
	}
};

// Toggle Open or close
locTree.prototype.o = function(id) {
	var cn = this.aNodes[id];
	this.nodeStatus(!cn._io, id, cn._ls);
	cn._io = !cn._io;
	if (this.config.closeSameLevel) this.closeLevel(cn);
	if (this.config.useCookies) this.updateCookie();
};

locTree.prototype.so = function(id) {
	var status = document.getElementById("nloc" + id).checked;
	var cn= this.aNodes[id];

	if (this.displaymode == 2) {
		this.selallchild(status);
	} else {
		this.selectAllChildren(cn, status);
		if (cn.id != 0)
			this.chkpnodestatus(this.aNodes[cn.pid]);
	}
};

locTree.prototype.os = function(id) {
	var elem = document.getElementById("sloc" + id);
	var cn= this.aNodes[id];

	mkrst(cn.url, cn.target, elem);
	this.chkpnodestatus(this.aNodes[cn.pid]);
};

// Open or close all nodes
locTree.prototype.oAll = function(status) {
	for (var n=0; n<this.aNodes.length; n++) {
		if (this.aNodes[n]._hc && this.aNodes[n].pid != this.root.id) {
			this.nodeStatus(status, n, this.aNodes[n]._ls)
			this.aNodes[n]._io = status;
		}
	}
	if (this.config.useCookies) this.updateCookie();
};

locTree.prototype.sAll = function(status) {
	for (var n=0; n<this.aNodes.length; n++) {
		if (this.aNodes[n]._hc && this.aNodes[n].pid != this.root.id)  {}
		else {
		  if ( (typeof(this.aNodes[n]) != "undefined") && (typeof(this.aNodes[n].target) != "undefined") )
			if (status == true) {
				mkrst(this.aNodes[n].url, this.aNodes[n].target, this);
			}
			else 
			{	
				decheck(this.aNodes[n].url, this.aNodes[n].target, this);
			}
		}
	}
	if (this.config.useCookies) this.updateCookie();
};

// Opens the tree to a specific node
locTree.prototype.openTo = function(nId, bSelect, bFirst) {
	if (!bFirst) {
		for (var n=0; n<this.aNodes.length; n++) {
			if (this.aNodes[n].id == nId) {
				nId=n;
				break;
			}
		}
	}
	var cn=this.aNodes[nId];
	if (cn.pid==this.root.id || !cn._p) return;
	cn._io = true;
	cn._is = bSelect;
	if (this.completed && cn._h) this.nodeStatus(true, cn._ai, cn._ls);
	if (this.completed && bSelect) this.s(cn._ai);
	else if (bSelect) this._sn=cn._ai;
	this.openTo(cn._p._ai, false, true);
};

// Closes all nodes on the same level as certain node
locTree.prototype.closeLevel = function(node) {
	for (var n=0; n<this.aNodes.length; n++) {
		if (this.aNodes[n].pid == node.pid && this.aNodes[n].id != node.id && this.aNodes[n]._hc) {
			this.nodeStatus(false, n, this.aNodes[n]._ls);
			this.aNodes[n]._io = false;
			this.closeAllChildren(this.aNodes[n]);
		}
	}
}

// Closes all children of a node
locTree.prototype.closeAllChildren = function(node) {
	for (var n=0; n<this.aNodes.length; n++) {
		if (this.aNodes[n].pid == node.id && this.aNodes[n]._hc) {
			if (this.aNodes[n]._io) this.nodeStatus(false, n, this.aNodes[n]._ls);
			this.aNodes[n]._io = false;
			this.closeAllChildren(this.aNodes[n]);		
		}
	}
}

locTree.prototype.selectAllChildren = function(node, status) 
{
	for (var n=0; n<this.aNodes.length; n++) 
		if (node.id == this.aNodes[n].pid){
			if (this.aNodes[n].level>1) {
				document.getElementById("nloc" + this.aNodes[n].id).checked = status;
				this.selectAllChildren(this.aNodes[n], status);
			} else
				mkrstNode(this.aNodes[n].id, this.aNodes[n].url, this.aNodes[n].target,document.getElementById("nloc" + node.id), status);
		}
}

function mkrstNode(nodeId, id, more, cb, status)
{
	s0 = document.frmSettings2loc.selectedloc.value;

	if (document.getElementById("sloc" + nodeId)) {
		document.getElementById("sloc" + nodeId).checked = status;
		mkrst(id, more, cb);
	}
}

// Change the status of a node(open or closed)
locTree.prototype.nodeStatus = function(status, id, bottom) {
	eDiv	= document.getElementById('d' + this.obj + id);
	eJoin	= document.getElementById('j' + this.obj + id);
	if (this.config.useIcons) {
		eIcon	= document.getElementById('i' + this.obj + id);
		eIcon.src = (status) ? this.aNodes[id].iconOpen : this.aNodes[id].icon;
	}
	eJoin.src = (this.config.useLines)?
	((status)?((bottom)?this.icon.minusBottom:this.icon.minus):((bottom)?this.icon.plusBottom:this.icon.plus)):
	((status)?this.icon.nlMinus:this.icon.nlPlus);
	eDiv.style.display = (status) ? 'block': 'none';
};


// [Cookie] Clears a cookie
locTree.prototype.clearCookie = function() {
	var now = new Date();
	var yesterday = new Date(now.getTime() - 1000 * 60 * 60 * 24);
	this.setCookie('co'+this.obj, 'cookieValue', yesterday);
	this.setCookie('cs'+this.obj, 'cookieValue', yesterday);
};

// [Cookie] Sets value in a cookie
locTree.prototype.setCookie = function(cookieName, cookieValue, expires, path, domain, secure) {
	document.cookie =
		escape(cookieName) + '=' + escape(cookieValue)
		+ (expires ? '; expires=' + expires.toGMTString() : '')
		+ (path ? '; path=' + path : '')
		+ (domain ? '; domain=' + domain : '')
		+ (secure ? '; secure' : '');
};

// [Cookie] Gets a value from a cookie
locTree.prototype.getCookie = function(cookieName) {
	var cookieValue = '';
	var posName = document.cookie.indexOf(escape(cookieName) + '=');
	if (posName != -1) {
		var posValue = posName + (escape(cookieName) + '=').length;
		var endPos = document.cookie.indexOf(';', posValue);
		if (endPos != -1) cookieValue = unescape(document.cookie.substring(posValue, endPos));
		else cookieValue = unescape(document.cookie.substring(posValue));
	}
	return (cookieValue);
};

// [Cookie] Returns ids of open nodes as a string
locTree.prototype.updateCookie = function() {
	var str = '';
	for (var n=0; n<this.aNodes.length; n++) {
		if (this.aNodes[n]._io && this.aNodes[n].pid != this.root.id) {
			if (str) str += '.';
			str += this.aNodes[n].id;
		}
	}
	this.setCookie('co' + this.obj, str);
};

// [Cookie] Checks if a node id is in a cookie
locTree.prototype.isOpen = function(id) {
	var aOpen = this.getCookie('co' + this.obj).split('.');
	for (var n=0; n<aOpen.length; n++)
		if (aOpen[n] == id) return true;
	return false;
};

// If Push and pop is not implemented by the browser
if (!Array.prototype.push) {
	Array.prototype.push = function array_push() {
		for(var i=0;i<arguments.length;i++)
			this[this.length]=arguments[i];
		return this.length;
	}
};
if (!Array.prototype.pop) {
	Array.prototype.pop = function array_pop() {
		lastElement = this[this.length-1];
		this.length = Math.max(this.length-1,0);
		return lastElement;
	}
};

locTree.prototype.chkpnodestatus = function(pnode) {
	var fsta = true;
	var haschild = false;

	if (this.displaymode == "2") {
		this.chkrootstatus();
	} else {
		for (var i=0; i<this.aNodes.length; i++) {
			if (pnode.id == this.aNodes[i].pid)
				if (document.getElementById( ( (this.aNodes[i].level > 1) ? "nloc" : "sloc") + this.aNodes[i].id)) {
					fsta = document.getElementById( ( (this.aNodes[i].level > 1) ? "nloc" : "sloc") + this.aNodes[i].id).checked ? fsta : false;
					haschild = true;
				}
		}
		if (!haschild)
			fsta = false;
		
		document.getElementById("nloc" + pnode.id).checked = fsta;
	
		if (pnode.id != 0) { 
			this.chkpnodestatus(this.aNodes[pnode.pid]);
		}
	} // if
}

locTree.prototype.chkrootstatus = function() {
	var fsta = true;

	for (var i=0; i<loc.aNodes.length; i++) {
		if (loc.aNodes[i].level == 3)
			if (document.getElementById("sloc" + loc.aNodes[i].id))
				fsta = document.getElementById("sloc" + loc.aNodes[i].id).checked ? fsta : false;
	}
	document.getElementById("nloc0").checked = fsta;
}

locTree.prototype.selallchild = function(status) {
	for (var i=0; i<loc.aNodes.length; i++) {
		if (loc.aNodes[i].level == 3)
			if (document.getElementById("sloc" + loc.aNodes[i].id)) {
				document.getElementById("sloc" + loc.aNodes[i].id).checked = status;
				mkrst(loc.aNodes[i].url, loc.aNodes[i].target, document.getElementById("sloc" + loc.aNodes[i].id));
			}
	}
	
}


function addstr(str, addstr, substr, replacestr, isadd)
{
	if (isadd) {
		if (str.indexOf(substr) == -1)
			return (str + addstr);
		else
			return -99;		// already done
	} else {
		if (str.indexOf(substr) != -1)
			return (str.replace(substr, replacestr));
		else
			return -99;		// already done
	}
	return (str);
}


function mkrst(id, more, cb)
{
	var s0 = document.frmSettings2loc.selectedloc.value, 
		s1 = document.frmSettings2loc.nonvideolocname.value,
		s2 = id + "`" + more.split("`")[6] + "%",
		s3 = document.frmSettings2loc.comparedsellocs.value,
		s4 = "``" + more + "``";

	var isadd = cb.checked;
	// save
		
	if ( (v=addstr(s1, s2, "%" + s2, "%", isadd)) !=-99 )
		document.frmSettings2loc.nonvideolocname.value = (more.split("`")[3] != "0") ? s1 : v;
	if ( (v=addstr(s3, ( (s3=="") ? s4 : (more + "``") ), s4, ( (s3==s4) ? "" : "``"), isadd)) !=-99 )
		document.frmSettings2loc.comparedsellocs.value = v;
	if ( (v=addstr(s0, (id + ", "), (", " + id + ", "), ", ", isadd)) !=-99 )
		document.frmSettings2loc.selectedloc.value =  v;
	else
		return;

//	var isadd = (s0.indexOf(", " + id + ", ") == -1);
//	document.frmSettings2loc.selectedloc.value = isadd ? (s0 + id + ", ") : s0.replace((", " + id + ", "), ", ");
//	document.frmSettings2loc.nonvideolocname.value = (more.split("`")[3] != "0") ? document.frmSettings2loc.nonvideolocname.value : ( (( (s0 = document.frmSettings2loc.nonvideolocname.value).indexOf( "%" + (s = id + "`" + more.split("`")[6] + "%") )) == -1) ? (s0 + s) : s0.replace(("%" + s), "%") );
//	document.frmSettings2loc.comparedsellocs.value = (((s0 = document.frmSettings2loc.comparedsellocs.value).indexOf(s1="``" + more + "``")) == -1) ? ((s0=="") ? (s0 + s1) : (s0 + more + "``")) : ((s0==s1) ? "" : s0.replace(s1, "``"));
	
	if ((parent.location.href).indexOf("calendar") != -1) {
		if (parent.chgRm) {
			issuc = parent.chgRm (document.frmSettings2loc.selectedloc.value, id, isadd);
			//issuc = parent.chgRm (document.frmSettings2loc.selectedloc.value, id, true);
//			if (!issuc)
//				decheck(id, more, cb);
		}
	}

	if (parent.document.location.href.indexOf("Settings2Event.asp") > 0)
		parent.loadSpecialRequest(id, more.split("`")[6], cb.checked);	
	else
//		if (parent.document.location.href.indexOf("settings2") > 0) || () 
		if (parent.addRooms) 
			parent.addRooms(id, more.split("`")[6], cb.checked);
};


function decheck(id, more, cb)
{
	cb.checked = false;
	document.frmSettings2loc.selectedloc.value = (((s0 = document.frmSettings2loc.selectedloc.value).indexOf(", " + id + ", ")) == -1) ? (s0 + id + ", ") : s0.replace((", " + id + ", "), ", ");
	document.frmSettings2loc.nonvideolocname.value = (more.split("`")[3] != "0") ? document.frmSettings2loc.nonvideolocname.value : ( (( (s0 = document.frmSettings2loc.nonvideolocname.value).indexOf( "%" + (s = id + "`" + more.split("`")[6] + "%") )) == -1) ? (s0 + s) : s0.replace(("%" + s), "%") );
	document.frmSettings2loc.comparedsellocs.value = (((s0 = document.frmSettings2loc.comparedsellocs.value).indexOf(s1="``" + more + "``")) == -1) ? ((s0=="") ? (s0 + s1) : (s0 + more + "``")) : ((s0==s1) ? "" : s0.replace(s1, "``"));
}



function autocheck(rmid, ischeck)
{
	if ((parent.location.href).indexOf("calendar") == -1) {
		return 0;	// do not support other pages, because now only support "selectedloc" field, not "nonvideolocname" and "comparedsellocs", later will support through rmid
	}
/*
	// find cb
	var ary = loc_chkno_rmid.split(";"), subary, cbno=0;
	
	for (var i = 0; i < ary.length-1; i++) {
		subary = ary[i].split(",");
		if (subary[1] == rmid)
			cbno = subary[0];
	}
	
	if ( (cbno < 0) || (cbno > document.frmSettings2loctree.elements.length) ) {
//		alert("error: not found");
		return 0;
	}
	
	cb = document.frmSettings2loctree.elements[cbno-1];
	if (ischeck)
		cb.checked = ischeck;
*/

	for (var i=0; i<loc.aNodes.length; i++) {
		if ( (loc.aNodes[i].url == rmid) && document.getElementById("sloc" + loc.aNodes[i].id) ) {
			document.getElementById("sloc" + loc.aNodes[i].id).checked = ischeck;
		}
	}
	
	if (ischeck)
		document.frmSettings2loc.selectedloc.value = (((s0 = document.frmSettings2loc.selectedloc.value).indexOf(", " + rmid + ", ")) == -1) ? (s0 + rmid + ", ") : s0;
	else
		document.frmSettings2loc.selectedloc.value = (((s0 = document.frmSettings2loc.selectedloc.value).indexOf(", " + rmid + ", ")) == -1) ? s0 : s0.replace((", " + rmid + ", "), ", ");
		
	//alert("selectedloc" + document.frmSettings2loc.selectedloc.value);
}


var rmresPopup = null;

function chkresource1(info)
{
	if (info != "") {
		url = "roomresource.asp?info=" + info + "&wintype=pop";	
		rmresPopup = window.open(url,'roomresource','status=no,width=400,height=280,resizable=yes,scrollbars=yes');

		rmresPopup.focus();
		if (!rmresPopup.opener) {
			rmresPopup.opener = self;
		}
	}
}


function chkresource(id)
{
    if (id != "") {
        if (id.indexOf(",") <0)
            id += ",";
		url = "roomresourcecomparesel.aspx?wintype=pop&f=pop&rms=" + id;
		rmresPopup = window.open(url,'roomresource','status=no,width=450,height=480,resizable=yes,scrollbars=yes');
		rmresPopup.focus();
		if (!rmresPopup.opener) {
			rmresPopup.opener = self;
		}
	}
}


function reactiverm(rid)
{
	if (rmresPopup)
		rmresPopup.close();
		
	if (rid != "")
		parent.document.location.href = "dispatcher/admindispatcher.asp?cmd=ActiveRoom&r=" + rid;
}


function compareselected()
{
	rms = document.frmSettings2loc.selectedloc.value
	rms = rms.replace(", ,",",")
	if (rms.split(",").length <=3)
		alert("Please select at least 2 locations to compare.");
	else
	    if (rms.split(",").length > 7)//fogbugz case 179 kapil said it is 5
	        alert("Please select 2 to 5 rooms");
    else {
		url = "roomresourcecomparesel.aspx?wintype=pop&f=pop&rms=" + rms;
		rmresPopup = window.open(url,'roomresource','status=yes,width=500,height=280,resizable=yes,scrollbars=yes');
			
		rmresPopup.focus();
		if (!rmresPopup.opener) {
			rmresPopup.opener = self;
		}
	}
}
function compareselectedOLD()
{
//	alert(document.frmSettings2loc.comparedsellocs.value);
	if (document.frmSettings2loc.comparedsellocs.value == "") 
		alert("There is no selected location.")
	else if (document.frmSettings2loc.comparedsellocs.value.split("``").length <=3)
		alert("Please select at least 2 locations to compare.");
	else {
		url = "roomresourcecomparesel.asp?wintype=pop";	
		rmresPopup = window.open(url,'roomresource','status=yes,width=500,height=280,resizable=yes,scrollbars=yes');
			
		rmresPopup.focus();
		if (!rmresPopup.opener) {
			rmresPopup.opener = self;
		}
	}
}


function childwinclose()
{
	if ( rmresPopup != null) {
		rmresPopup.close();
	}
}


var rmavailPopup = null;


function viewselectedavailability()
{
	if (document.frmSettings2loc.comparedsellocs.value == "") 
		alert("There is no selected location.")
	else {
		if (parent.getConfRoom)
			parent.getConfRoom();
		else
			alert("Error: Javascript error. Please notify administrator about this error.");
	}
}


function getAdvRmName(rid, orgrmname)
{
	rms = (parent.document.frmSettings2.advAlocRoom.value).split(";;");
	for (i = 0; i < rms.length - 1; i++) {
		rm = rms[i].split(",,");
		if (rm[0] == rid) {
			return ("<b>" + orgrmname + "</b> <span style='color: green; font-size: 9px;'>" + rm[2] + "~" + rm[3] + "</span>")
		}
	}
	
	return orgrmname;
}