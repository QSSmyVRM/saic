/*ZD 100147 Start*/
/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
/*ZD 100147 End*/
// Node object
function Node(id, pid, name, url, title, target, icon, iconOpen, open) {
	this.id = id;
	this.pid = pid;
	this.name = name;
	this.url = url;
	this.title = title;
	this.target = target;
	this.icon = icon;
	this.iconOpen = iconOpen;
	this._io = open || false;
	this._is = false;
	this._ls = false;
	this._hc = false;
	this._ai = 0;
	this._p;
};

// Tree object
function locTree(objName) {
	this.config = {
		target				: null,
		folderLinks			: true,
		useSelection		: true,
		useCookies			: true,
		useLines			: true,
		useIcons			: true,
		useStatusText		: false,
		closeSameLevel		: false,
		inOrder				: false
	}
	this.icon = {
		root				: 'image/loc/base.gif',
		folder				: 'image/loc/building2.gif',
		folderOpen			: 'image/loc/folderopen.gif',
		node				: 'image/loc/room2.gif',
		empty				: 'image/loc/empty2.gif',
		line				: 'image/loc/line2.gif',
		join				: 'image/loc/join2.gif',
		joinBottom			: 'image/loc/joinbottom2.gif',
		plus				: 'image/loc/plus2.gif',
		plusBottom			: 'image/loc/plusbottom2.gif',
		minus				: 'image/loc/minus2.gif',
		minusBottom			: 'image/loc/minusbottom2.gif',
		nlPlus				: 'image/loc/nolines_plus2.gif',
		nlMinus				: 'image/loc/nolines_minus2.gif'
	};

	this.obj = objName;
	this.aNodes = [];
	this.aIndent = [];
	this.root = new Node(-1);
	this.selectedNode = null;
	this.selectedFound = false;
	this.completed = false;
};

// Adds a new node to the node array
locTree.prototype.add = function(id, pid, name, url, title, target, icon, iconOpen, open) {
	this.aNodes[this.aNodes.length] = new Node(id, pid, name, url, title, target, icon, iconOpen, open);
};

// Open/close all nodes
locTree.prototype.openAll = function() {
	this.oAll(true);
};
locTree.prototype.closeAll = function() {
	this.oAll(false);
};

// Outputs the tree to the page
locTree.prototype.toString = function() {
	var str = '<div class="loctree">\n';
	if (document.getElementById) {
		if (this.config.useCookies) this.selectedNode = this.getSelected();
		str += this.addNode(this.root);
	} else str += 'Browser not supported.';
	str += '</div>';
	if (!this.selectedFound) this.selectedNode = null;
	this.completed = true;
	return str;
};

// Creates the tree structure
locTree.prototype.addNode = function(pNode) {
	var str = '';
	var n=0;
	if (this.config.inOrder) n = pNode._ai;
	for (n; n<this.aNodes.length; n++) {
		if (this.aNodes[n].pid == pNode.id) {
			var cn = this.aNodes[n];
			cn._p = pNode;
			cn._ai = n;
			this.setCS(cn);
			if (!cn.target && this.config.target) cn.target = this.config.target;
			if (cn._hc && !cn._io && this.config.useCookies) cn._io = this.isOpen(cn.id);
			if (!this.config.folderLinks && cn._hc) cn.url = null;
			if (this.config.useSelection && cn.id == this.selectedNode && !this.selectedFound) {
					cn._is = true;
					this.selectedNode = n;
					this.selectedFound = true;
			}
			str += this.node(cn, n);
			if (cn._ls) break;
		}
	}
	return str;
};

// Creates the node icon, url and text
locTree.prototype.node = function(node, nodeId) {
	var str = '<div class="loctreeNode">' + this.indent(node, nodeId);
	if (this.config.useIcons) {
		if (!node.icon) node.icon = (this.root.id == node.pid) ? this.icon.root : ((node._hc) ? this.icon.folder : this.icon.node);
		if (!node.iconOpen) node.iconOpen = (node._hc) ? this.icon.folderOpen : this.icon.node;
		if (this.root.id == node.pid) {
			node.icon = this.icon.root;
			node.iconOpen = this.icon.root;
		}
		str += '<img id="i' + this.obj + nodeId + '" src="' + ((node._io) ? node.iconOpen : node.icon) + '" alt="" />';
	}

	if (node.title) {
//alert(node.name);
		if (node.title != "")
			if (node.title=="1")
				if ( (queryField("f") == "frmAdvanceroomallocation") || ( (queryField("f") == "frmManagefoodorder2") && (queryField("rid") == "") ) || ( (queryField("f") == "frmManagefoodorder2") && (queryField("rid") != node.url) ) )
					str += '<input id="s' + this.obj + nodeId + '" type="radio" name="C1" value="1" onclick="Javascript: mkrst(\'' + (node.url) + '\',\'' + (node.target) + '\',\'' + (node.name) + '\');"'
				else {
//					alert("node.title:" + node.title + ";" + "node.url:" + node.url + ";" + "node.target:" + node.target)
					str += '<input id="s' + this.obj + nodeId + '" type="radio" name="C1" value="1" checked onclick="Javascript: mkrst(\'' + (node.url) + '\',\'' + (node.target) + '\',\'' + (node.name) + '\');"'
				}
//				str += '<input id="s' + this.obj + nodeId + '" type="radio" name="C1" value="1" checked onclick="Javascript: mkrst(\'' + (node.url) + '\',\'' + (node.target) + '\',\'' + this.checked + '\');"'
			else
				str += '<input id="s' + this.obj + nodeId + '" type="radio" name="C1" value="1" onclick="Javascript: mkrst(\'' + (node.url) + '\',\'' + (node.target) + '\',\'' + (node.name) + '\');"'
//				str += '<input id="s' + this.obj + nodeId + '" type="radio" name="C1" value="1" onclick="Javascript: mkrst(\'' + (node.url) + '\',\'' + (node.target) + '\',\'' + this.checked + '\');"'
		else {
			alert(node.name)
			str += '<a id="s' + this.obj + nodeId + '" class="' + ((this.config.useSelection) ? ((node._is ? 'nodeSel' : 'node')) : 'node') + '" href="' + node.url + '" onClick="Javscript:alert(' + "wrong string" + ');alert(this.href); eval(this.id).style.display='+"block"+';"';
		}

//		if (node.title) str += ' title="' + node.title + '"';
		if (node.target) str += ' target="' + node.target + '"';
		if (this.config.useStatusText) str += ' onmouseover="window.status=\'' + node.name + '\';return true;" onmouseout="window.status=\'\';return true;" ';
		if (this.config.useSelection && ((node._hc && this.config.folderLinks) || !node._hc))
			str += ' onclick="javascript: ' + this.obj + '.s(' + nodeId + ');"';
		str += '>';

		if (node.title != "")
//			str += '<a href="javascript: chkresource1(\'' + (node.target) + '\');" title="click to show room resources" onmouseover="window.status=\'\';return true;">';
			str += '<a href="javascript: chkresource(\'' + (node.url) + '\');" title="click to show room resources" onmouseover="window.status=\'\';return true;">';
	}
	else if ((!this.config.folderLinks || !node.url) && node._hc && node.pid != this.root.id)
		str += '<a href="javascript: ' + this.obj + '.o(' + nodeId + ');" class="node">';

	str += node.name;
//	alert(node.name)
//	if (node.name == "Expedite Westbury Building")
//		alert("1:" + node.name + ":" + node.title + ":")
	
	
	if (node.title)
		if (node.title != "")
			str += '</a>';

	if (node.url || ((!this.config.folderLinks || !node.url) && node._hc)) str += '</a>';
	str += '</div>';
	if (node._hc) {
		str += '<div id="d' + this.obj + nodeId + '" class="clip" style="display:' + ((this.root.id == node.pid || node._io) ? 'block' : 'none') + ';">';
		str += this.addNode(node);
		str += '</div>';
	}
	this.aIndent.pop();
	return str;
};

// Adds the empty and line icons
locTree.prototype.indent = function(node, nodeId) {
	var str = '';
	if (this.root.id != node.pid) {
		for (var n=0; n<this.aIndent.length; n++)
			str += '<img src="' + ( (this.aIndent[n] == 1 && this.config.useLines) ? this.icon.line : this.icon.empty ) + '" alt="" />';
		(node._ls) ? this.aIndent.push(0) : this.aIndent.push(1);
		if (node._hc) {
			str += '<a href="javascript: ' + this.obj + '.o(' + nodeId + ');"><img id="j' + this.obj + nodeId + '" src="';
			if (!this.config.useLines) str += (node._io) ? this.icon.nlMinus : this.icon.nlPlus;
			else str += ( (node._io) ? ((node._ls && this.config.useLines) ? this.icon.minusBottom : this.icon.minus) : ((node._ls && this.config.useLines) ? this.icon.plusBottom : this.icon.plus ) );
			str += '" alt="" /></a>';
		} else str += '<img src="' + ( (this.config.useLines) ? ((node._ls) ? this.icon.joinBottom : this.icon.join ) : this.icon.empty) + '" alt="" />';
	}
	return str;
};

// Checks if a node has any children and if it is the last sibling
locTree.prototype.setCS = function(node) {
	var lastId;
	for (var n=0; n<this.aNodes.length; n++) {
		if (this.aNodes[n].pid == node.id) node._hc = true;
		if (this.aNodes[n].pid == node.pid) lastId = this.aNodes[n].id;
	}
	if (lastId==node.id) node._ls = true;
};

// Returns the selected node
locTree.prototype.getSelected = function() {
	var sn = this.getCookie('cs' + this.obj);
	return (sn) ? sn : null;
};

// Highlights the selected node
locTree.prototype.s = function(id) {
	if (!this.config.useSelection) return;
	var cn = this.aNodes[id];
	if (cn._hc && !this.config.folderLinks) return;
	if (this.selectedNode != id) {
		if (this.selectedNode || this.selectedNode==0) {
			eOld = document.getElementById("s" + this.obj + this.selectedNode);
			eOld.className = "node";
		}
		eNew = document.getElementById("s" + this.obj + id);
		eNew.className = "nodeSel";
		this.selectedNode = id;
		if (this.config.useCookies) this.setCookie('cs' + this.obj, cn.id);
	}
};

// Toggle Open or close
locTree.prototype.o = function(id) {
	var cn = this.aNodes[id];
	this.nodeStatus(!cn._io, id, cn._ls);
	cn._io = !cn._io;
	if (this.config.closeSameLevel) this.closeLevel(cn);
	if (this.config.useCookies) this.updateCookie();
};

// Open or close all nodes
locTree.prototype.oAll = function(status) {
	for (var n=0; n<this.aNodes.length; n++) {
		if (this.aNodes[n]._hc && this.aNodes[n].pid != this.root.id) {
			this.nodeStatus(status, n, this.aNodes[n]._ls)
			this.aNodes[n]._io = status;
		}
	}
	if (this.config.useCookies) this.updateCookie();
};

// Opens the tree to a specific node
locTree.prototype.openTo = function(nId, bSelect, bFirst) {
	if (!bFirst) {
		for (var n=0; n<this.aNodes.length; n++) {
			if (this.aNodes[n].id == nId) {
				nId=n;
				break;
			}
		}
	}
	var cn=this.aNodes[nId];
	if (cn.pid==this.root.id || !cn._p) return;
	cn._io = true;
	cn._is = bSelect;
	if (this.completed && cn._hc) this.nodeStatus(true, cn._ai, cn._ls);
	if (this.completed && bSelect) this.s(cn._ai);
	else if (bSelect) this._sn=cn._ai;
	this.openTo(cn._p._ai, false, true);
};

// Closes all nodes on the same level as certain node
locTree.prototype.closeLevel = function(node) {
	for (var n=0; n<this.aNodes.length; n++) {
		if (this.aNodes[n].pid == node.pid && this.aNodes[n].id != node.id && this.aNodes[n]._hc) {
			this.nodeStatus(false, n, this.aNodes[n]._ls);
			this.aNodes[n]._io = false;
			this.closeAllChildren(this.aNodes[n]);
		}
	}
}

// Closes all children of a node
locTree.prototype.closeAllChildren = function(node) {
	for (var n=0; n<this.aNodes.length; n++) {
		if (this.aNodes[n].pid == node.id && this.aNodes[n]._hc) {
			if (this.aNodes[n]._io) this.nodeStatus(false, n, this.aNodes[n]._ls);
			this.aNodes[n]._io = false;
			this.closeAllChildren(this.aNodes[n]);		
		}
	}
}

// Change the status of a node(open or closed)
locTree.prototype.nodeStatus = function(status, id, bottom) {
	eDiv	= document.getElementById('d' + this.obj + id);
	eJoin	= document.getElementById('j' + this.obj + id);
	if (this.config.useIcons) {
		eIcon	= document.getElementById('i' + this.obj + id);
		eIcon.src = (status) ? this.aNodes[id].iconOpen : this.aNodes[id].icon;
	}
	eJoin.src = (this.config.useLines)?
	((status)?((bottom)?this.icon.minusBottom:this.icon.minus):((bottom)?this.icon.plusBottom:this.icon.plus)):
	((status)?this.icon.nlMinus:this.icon.nlPlus);
	eDiv.style.display = (status) ? 'block': 'none';
};


// [Cookie] Clears a cookie
locTree.prototype.clearCookie = function() {
	var now = new Date();
	var yesterday = new Date(now.getTime() - 1000 * 60 * 60 * 24);
	this.setCookie('co'+this.obj, 'cookieValue', yesterday);
	this.setCookie('cs'+this.obj, 'cookieValue', yesterday);
};

// [Cookie] Sets value in a cookie
locTree.prototype.setCookie = function(cookieName, cookieValue, expires, path, domain, secure) {
	document.cookie =
		escape(cookieName) + '=' + escape(cookieValue)
		+ (expires ? '; expires=' + expires.toGMTString() : '')
		+ (path ? '; path=' + path : '')
		+ (domain ? '; domain=' + domain : '')
		+ (secure ? '; secure' : '');
};

// [Cookie] Gets a value from a cookie
locTree.prototype.getCookie = function(cookieName) {
	var cookieValue = '';
	var posName = document.cookie.indexOf(escape(cookieName) + '=');
	if (posName != -1) {
		var posValue = posName + (escape(cookieName) + '=').length;
		var endPos = document.cookie.indexOf(';', posValue);
		if (endPos != -1) cookieValue = unescape(document.cookie.substring(posValue, endPos));
		else cookieValue = unescape(document.cookie.substring(posValue));
	}
	return (cookieValue);
};

// [Cookie] Returns ids of open nodes as a string
locTree.prototype.updateCookie = function() {
	var str = '';
	for (var n=0; n<this.aNodes.length; n++) {
		if (this.aNodes[n]._io && this.aNodes[n].pid != this.root.id) {
			if (str) str += '.';
			str += this.aNodes[n].id;
		}
	}
	this.setCookie('co' + this.obj, str);
};

// [Cookie] Checks if a node id is in a cookie
locTree.prototype.isOpen = function(id) {
	var aOpen = this.getCookie('co' + this.obj).split('.');
	for (var n=0; n<aOpen.length; n++)
		if (aOpen[n] == id) return true;
	return false;
};

// If Push and pop is not implemented by the browser
if (!Array.prototype.push) {
	Array.prototype.push = function array_push() {
		for(var i=0;i<arguments.length;i++)
			this[this.length]=arguments[i];
		return this.length;
	}
};
if (!Array.prototype.pop) {
	Array.prototype.pop = function array_pop() {
		lastElement = this[this.length-1];
		this.length = Math.max(this.length-1,0);
		return lastElement;
	}
};


function mkrst(id, more, name)
{
	document.frmSettings2loc.selectedloc.value = id;
	
	if (queryField("f") == "frmAdvanceroomallocation") {
		parent.document.frmAdvanceroomallocation.RoomName.value = name;
		parent.document.frmAdvanceroomallocation.roomid.value = id;
		parent.document.frmAdvanceroomallocation.AddSubmit.disabled = false;
	}
	
	if (queryField("f") == "frmManagefoodorder2") {
		curselrmno = -2;
		for (i = 0; i < document.frmUser2loctree.elements.length; i++)
			if (document.frmUser2loctree.elements[i].type == "radio")
				curselrmno = ( document.frmUser2loctree.elements[i].checked ) ? i : curselrmno;

		parent.selectroom (id, curselrmno);
	}
};



var rmresPopup = null;

function chkresource1(info)
{
	if (info != "") {
		url = "roomresource1.asp?info=" + info;	
		rmresPopup = window.open(url,'roomresource','status=no,width=400,height=280,resizable=yes,scrollbars=yes');
		rmresPopup.focus();
		if (!rmresPopup.opener) {
			rmresPopup.opener = self;
		}
	}
}

function chkresource(id)
{
//	alert(id);
	if (id != "") {
		url = "dispatcher/admindispatcher.asp?cmd=GetOldRoom&rid=" + id;	
		rmresPopup = window.open(url,'roomresource','status=no,width=450,height=480,resizable=yes,scrollbars=yes');
		rmresPopup.focus();
		if (!rmresPopup.opener) {
			rmresPopup.opener = self;
		}
	}
}

function compareselected()
{
	if (document.frmSettings2loc.comparedsellocs.value == "") 
		alert("There is no selected location.")
	else {
		url = "roomresourcecomparesel.asp?wintype=pop";	
		rmresPopup = window.open(url,'roomresource','status=yes,width=500,height=280,resizable=yes,scrollbars=yes');
			
		rmresPopup.focus();
		if (!rmresPopup.opener) {
			rmresPopup.opener = self;
		}
	}
}


function childwinclose()
{
	if ( rmresPopup != null) {
		rmresPopup.close();
	}
}