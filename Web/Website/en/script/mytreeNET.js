/*ZD 100147 Start*/
/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
/*ZD 100147 End*/
function getRooms1(evt)
   {
        var src = window.event != window.undefined ? window.event.srcElement : evt.target;
        var isChkBoxClick = (src.tagName.toLowerCase() == "input" && src.type == "checkbox");
        if(isChkBoxClick)
        {
            var parentTable = GetParentByTagName("table", src);
            var nxtSibling = parentTable.nextSibling;
            if(nxtSibling && nxtSibling.nodeType == 1)//check if nxt sibling is not null & is an element node
            {
                if(nxtSibling.tagName.toLowerCase() == "div") //if node has children
                {
                    //check or uncheck children at all levels
                    CheckUncheckChildren(parentTable.nextSibling, src.checked);
                }
            }
            else
            {
 //                  alert("clicked a leaf node " + src.checked);
                    
                    if (src.checked)
                    {
                        var tnName = src.id;
                        var nodeNum = tnName.substring(26, tnName.indexOf("CheckBox"));
                        if (nodeNum != "")
                        {
                            var obbj = document.getElementById("Wizard1_treeRoomSelectiont" + nodeNum);
                            if (obbj.title != "")
                              if (document.getElementById("selectedloc").value.indexOf(obbj.title + ",") < 0)
                                document.getElementById("selectedloc").value += obbj.title + ",";
                        }   
                    }
                    else
                    {
                        var tnName = src.id;
                        var nodeNum = tnName.substring(26, tnName.indexOf("CheckBox"));
                        if (nodeNum != "")
                        {
                            var obbj = document.getElementById("Wizard1_treeRoomSelectiont" + nodeNum);
                            var obbbj = document.getElementById("selectedloc");
                            if ((obbbj.value.indexOf(obbj.title + ",") >=0) && (obbj.title != ""))
                                 obbbj.value = obbbj.value.replace(obbj.title + ",", "");
                        }
                    }
            }
            //check or uncheck parents at all levels
            CheckUncheckParents(src, src.checked);
        }
   } 
   function CheckUncheckChildren(childContainer, check)
   {
      var childChkBoxes = childContainer.getElementsByTagName("input");
      var childChkBoxCount = childChkBoxes.length;
      for(var i = 0; i<childChkBoxCount; i++)
      {
        childChkBoxes[i].checked = check;
        alert(childChkBoxes[i].id);
      }
   }
   function CheckUncheckParents(srcChild, check)
   {
       var parentDiv = GetParentByTagName("div", srcChild);
       var parentNodeTable = parentDiv.previousSibling;
       
       if(parentNodeTable)
        {
            var checkUncheckSwitch;
            
            if(check) //checkbox checked
            {
                var isAllSiblingsChecked = AreAllSiblingsChecked(srcChild);
                if(isAllSiblingsChecked)
                    checkUncheckSwitch = true;
                else    
                    return; //do not need to check parent if any(one or more) child not checked
            }
            else //checkbox unchecked
            {
                checkUncheckSwitch = false;
            }
            
            var inpElemsInParentTable = parentNodeTable.getElementsByTagName("input");
            if(inpElemsInParentTable.length > 0)
            {
                var parentNodeChkBox = inpElemsInParentTable[0]; 
                parentNodeChkBox.checked = checkUncheckSwitch; 
                //do the same recursively
                CheckUncheckParents(parentNodeChkBox, checkUncheckSwitch);
            }
        }
   }
   function AreAllSiblingsChecked(chkBox)
   {
     var parentDiv = GetParentByTagName("div", chkBox);
     var childCount = parentDiv.childNodes.length;
     for(var i=0; i<childCount; i++)
     {
        if(parentDiv.childNodes[i].nodeType == 1) //check if the child node is an element node
        {
            if(parentDiv.childNodes[i].tagName.toLowerCase() == "table")
            {
               var prevChkBox = parentDiv.childNodes[i].getElementsByTagName("input")[0];
              //if any of sibling nodes are not checked, return false
              if(!prevChkBox.checked) 
              {
                return false;
              } 
            }
        }
     }
     return true;
   }
   //utility function to get the container of an element by tagname
   function GetParentByTagName(parentTagName, childElementObj)
   {
      var parent = childElementObj.parentNode;
      while(parent.tagName.toLowerCase() != parentTagName.toLowerCase())
      {
         parent = parent.parentNode;
      }
    return parent;    
   }

function AddResource_CallBack(response)
{
    alert(response.value);
}

function getRooms(evt)
{
// obj gives us the node on which check or uncheck operation has performed 
    var obj;
    if (window.event != window.undefined)
       obj =  window.event.srcElement;
    else
       obj = evt.target; 
       
    var treeNodeFound = false; 
    var checkedState; 
    
    if (window.location.href.indexOf("Calendar.aspx") >= 0 && window.location.href.indexOf("Personal") <= 0)
    {
        if (obj.tagName == "INPUT" && obj.type == "checkbox") 
        {    //AddResource(obj.name, obj.id, AddResource_CallBack);
            //alert(obj.name);
      	    document.getElementById("__EVENTTARGET").value= obj.name;
      	    //alert("1");
//      	    WebForm_DoPostBackWithOptions(new WebForm_PostBackOptions("<%=treeRoomSelection.ClientID%>", "", true, "", "", false, false));
            __doPostBack(obj.name, true);
      	    //alert(document.getElementById("__EVENTTARGET").value);
      	    if (obj.checked)
      	    {
      	        //alert(obj.name + " : " + obj.value);
      	        //document.getElementById("__EVENTTARGET").value= obj.name;
      	        //Calendar.NodeCheckedAjax(obj.parentNode.innerHTML, obj.title, AddResource_CallBack); // for AJAX
      	    }
        }
    }        
    else
    {
    if (obj.tagName == "INPUT" && obj.type == "checkbox") 
    {     //1 checking whether obj consists of checkbox or not 
        var treeNode = obj; 
        //record the checked state of the TreeNode 
        checkedState = treeNode.checked; 
        //work our way back to the parent <table> element 
        do {
            obj = obj.parentNode; 
        } while (obj.tagName != "TABLE") 
        //keep track of the padding level for comparison with any children 
        var parentTreeLevel = obj.rows[0].cells.length; 
        var parentTreeNode = obj.rows[0].cells[0]; 
        //get all the TreeNodes inside the TreeView (the parent <div>) 
        var tables = obj.parentNode.getElementsByTagName("TABLE"); 
        //checking for any node is checked or unchecked during operation 
        if(obj.tagName == "TABLE") 
        { //2
            // if any node is unchecked then their parent node are unchecked 
            if (!treeNode.checked) 
            { //3
                //head1 gets the parent node of the unchecked node 
                var head1 = obj.parentNode.previousSibling; 
                if(head1.tagName == "TABLE") {
                //checks for the input tag which consists of checkbox 
                var matchElement1 = head1.getElementsByTagName("INPUT"); 
                //matchElement1[0] gives us the checkbox and it is unchecked 
                matchElement1[0].checked = false; 
            } //3
            else 
                head1 = obj.parentNode.previousSibling;
            if(head1.tagName == "TABLE") 
            { //4
            //head2 gets the parent node of the unchecked node 
                var head2 = obj.parentNode.parentNode.previousSibling; 
                if(head2.tagName == "TABLE") 
                { //5
                    //checks for the input tag which consists of checkbox 
                    var matchElement2 = head2.getElementsByTagName("INPUT"); 
                    matchElement2[0].checked = false; 
                } //5
            }//4
            else 
                head2 = obj.parentNode.previousSibling;
            if(head2.tagName == "TABLE") 
            {//6
                //head3 gets the parent node of the unchecked node 
                var head3 = obj.parentNode.parentNode.parentNode.previousSibling; 
                if(head3.tagName == "TABLE") 
                {//7
                    //checks for the input tag which consists of checkbox 
                    var matchElement3 = head3.getElementsByTagName("INPUT"); 
                    matchElement3[0].checked = false; 
                }//7
            }//6
            else 
                head3 = obj.parentNode.previousSibling;
            if(head3.tagName == "TABLE") 
            { //7
               //head4 gets the parent node of the unchecked node 
                var head4 = obj.parentNode.parentNode.parentNode.parentNode.previousSibling; 
                if(head4 != null) 
                {//8 
                    if(head4.tagName == "TABLE") 
                    {//8
                        //checks for the input tag which consists of checkbox 
                        var matchElement4 = head4.getElementsByTagName("INPUT"); 
                        matchElement4[0].checked = false; 
                    }//8
                }//7
            } //6
        } //end if - unchecked
        //total number of TreeNodes 
        var numTables = tables.length 
        if (numTables >= 1) 
        {
            for (i=0; i < numTables; i++) 
            {
                if (tables[i] == obj) 
                {
                    treeNodeFound = true; 
                    i++;
                    if (i == numTables) 
                        break;
                }
                if (treeNodeFound == true) 
                {
                    var childTreeLevel = tables[i].rows[0].cells.length; 
                    if (childTreeLevel > parentTreeLevel) 
                    { 
                        var cell = tables[i].rows[0].cells[childTreeLevel - 1]; 
                        var inputs = cell.getElementsByTagName("INPUT"); 
                        inputs[0].checked = checkedState; 
                    } 
                    else 
                        break;
                } //end if 
                if (childTreeLevel == "5")
                {
                    if (inputs[0].checked)
                    {
                        var tnName = inputs[0].id; var nodeNum;
                        if (tnName.indexOf("Wizard1") >= 0)
                           nodeNum = tnName.substring(26, tnName.indexOf("CheckBox"));
                        else
                            nodeNum = tnName.substring(18, tnName.indexOf("CheckBox"));
                        if (nodeNum != "")
                        {
                            var obbj;
                            //alert(tnName.indexOf("Wizard1"));
                            if (tnName.indexOf("Wizard1") >= 0)
                                obbj = document.getElementById("Wizard1_treeRoomSelectiont" + nodeNum);
                            else
                                obbj = document.getElementById("treeRoomSelectiont" + nodeNum);
                            if (obbj.title != "")
                              if (document.getElementById("selectedloc").value.indexOf(" " + obbj.title + ", ") < 0)
                                document.getElementById("selectedloc").value += " " + obbj.title + ", ";
                        }   
                    }
                    else
                    {
                        var tnName = inputs[0].id;
                        var nodeNum;
                        if (tnName.indexOf("Wizard1") >= 0)
                           nodeNum = tnName.substring(26, tnName.indexOf("CheckBox"));
                        else
                            nodeNum = tnName.substring(18, tnName.indexOf("CheckBox"));
                        if (nodeNum != "")
                        {
                            var obbj;
                            
                            if (tnName.indexOf("Wizard1") >= 0)
                                obbj = document.getElementById("Wizard1_treeRoomSelectiont" + nodeNum);
                            else
                                obbj = document.getElementById("treeRoomSelectiont" + nodeNum);
                            var obbbj = document.getElementById("selectedloc");
                            if ((obbbj.value.indexOf(" " + obbj.title + ", ") >=0) && (obbj.title != ""))
                                 obbbj.value = obbbj.value.replace(" " + obbj.title + ", ", "");
                        }
                    }
                }
            }//end for 
        } //end if - numTables >= 1

    // if all child nodes are checked then their parent node is checked
    //Wizard1_treeRoomSelectiont4
    //Wizard1_treeRoomSelectionn4Checkbox
    if (treeNode.checked) 
    {
        var tnName = treeNode.id;
        var nodeNum;
        if (tnName.indexOf("Wizard1") >= 0)
           nodeNum = tnName.substring(26, tnName.indexOf("CheckBox"));
        else
            nodeNum = tnName.substring(18, tnName.indexOf("CheckBox"));
        if (nodeNum != "")
        {
            var obbj;
            if (tnName.indexOf("Wizard1") >= 0)
                obbj = document.getElementById("Wizard1_treeRoomSelectiont" + nodeNum);
            else
                obbj = document.getElementById("treeRoomSelectiont" + nodeNum);
                
            if (obbj == null)
            {
                obbj = document.getElementById(nodeNum);
                tle = obbj.parentNode.title;
            }
            else
                tle = obbj.title;
            if (obbj != null)
            if (tle != "")
              if (document.getElementById("selectedloc").value.indexOf(" " + tle + ", ") < 0)
                document.getElementById("selectedloc").value += " " + tle + ", ";
        }
        var chk1 = true;
        var head1 = obj.parentNode.previousSibling;
        var pTreeLevel1 = obj.rows[0].cells.length;
        if(head1.tagName == "TABLE") 
        {
            var tbls = obj.parentNode.getElementsByTagName("TABLE");
            var tblsCount = tbls.length;
            for (i=0; i < tblsCount; i++) 
            {
                var childTreeLevel = tbls[i].rows[0].cells.length;
                if (childTreeLevel = pTreeLevel1) 
                {
                    var chld = tbls[i].getElementsByTagName("INPUT");
                    if (chld[0].checked == false) 
                    {
                        chk1 = false;
                        break;
                    }
                }
            }
            var nd = head1.getElementsByTagName("INPUT");
            nd[0].checked = chk1;
        }
        else
            head1 = obj.parentNode.previousSibling;
        var chk2 = true;
        if(head1.tagName == "TABLE") 
        {
            var head2 = obj.parentNode.parentNode.previousSibling; 
            if(head2.tagName == "TABLE") 
            {
                var tbls = head1.parentNode.getElementsByTagName("TABLE");
                var pTreeLevel2 = head1.rows[0].cells.length;
                var tblsCount = tbls.length;
                for (i=0; i < tblsCount; i++) 
                {
                    var childTreeLevel = tbls[i].rows[0].cells.length;
                    if (childTreeLevel = pTreeLevel2) 
                    {
                        var chld = tbls[i].getElementsByTagName("INPUT");
                        if (chld[0].checked == false) 
                        {
                            chk2 = false;
                            break;
                        }
                    }
                }
                var nd = head2.getElementsByTagName("INPUT");
                nd[0].checked = (chk2 && chk1);
            }
        }
        else 
            head2 = obj.parentNode.previousSibling;
        var chk3 = true;
        if(head2.tagName == "TABLE") 
        {
            var head3 = obj.parentNode.parentNode.parentNode.previousSibling; 
            if(head3.tagName == "TABLE") 
            {
                var tbls = head2.parentNode.getElementsByTagName("TABLE");
                var pTreeLevel3 = head2.rows[0].cells.length;
                var tblsCount = tbls.length;
                for (i=0; i < tblsCount; i++) 
                {
                    var childTreeLevel = tbls[i].rows[0].cells.length;
                    if (childTreeLevel = pTreeLevel3) 
                    {
                        var chld = tbls[i].getElementsByTagName("INPUT");
                        if (chld[0].checked == false) 
                        {
                            chk3 = false;
                            break;
                        }
                    }
                }
                var nd = head3.getElementsByTagName("INPUT");
                nd[0].checked = (chk3 && chk2 && chk1);
            }
        }
        else 
            head3 = obj.parentNode.previousSibling;
        var chk4 = true;
        if(head3.tagName == "TABLE") 
        {
            var head4 = obj.parentNode.parentNode.parentNode.parentNode.previousSibling; 
            //Added for FB 1415,1416,1417,1418 Start
            if(head4 != null)
            {
            //Added for FB 1415,1416,1417,1418 End
                if(head4.tagName == "TABLE") 
                {
                    var tbls = head3.parentNode.getElementsByTagName("TABLE");
                    var pTreeLevel4 = head3.rows[0].cells.length;
                    var tblsCount = tbls.length;
                    for (i=0; i < tblsCount; i++) 
                    {
                        var childTreeLevel = tbls[i].rows[0].cells.length;
                        if (childTreeLevel = pTreeLevel4) 
                        {
                            var chld = tbls[i].getElementsByTagName("INPUT");
                            if (chld[0].checked == false) 
                            {
                                chk4 = false;
                                break;
                            }
                        }
                    }
                    var nd = head4.getElementsByTagName("INPUT");
                    nd[0].checked = (chk4 && chk3 && chk2 && chk1);
                    //alert(chk1.value + " : " + chk2.value + " : " + chk3.value + " : " + chk4.value);
                }
            }  //Added for FB 1415,1416,1417,1418 
        }
    }//end if - checked
    else
    { //remove the id from the selected location
        var tnName = treeNode.id;
        var nodeNum;
        if (tnName.indexOf("Wizard1") >= 0)
           nodeNum = tnName.substring(26, tnName.indexOf("CheckBox"));
        else
            nodeNum = tnName.substring(18, tnName.indexOf("CheckBox"));
        if (nodeNum != "")
        {
            var obbj;
            
            if (tnName.indexOf("Wizard1") >= 0)
                obbj = document.getElementById("Wizard1_treeRoomSelectiont" + nodeNum);
            else
                obbj = document.getElementById("treeRoomSelectiont" + nodeNum);
            var obbbj = document.getElementById("selectedloc");
                
            if (obbj == null)
            {
                obbj = document.getElementById(nodeNum);
                tle = obbj.parentNode.title;
            }
            else
                tle = obbj.title;

            if (obbbj)
            if ((obbbj.value.indexOf(" " + tle + ", ") >=0) && (tle != ""))
                obbbj.value = obbbj.value.replace(" " + tle + ", ", "");
        }
    }
    }
  } //end if - tagName = TABLE
  
  
 } //end if

} //end function
//FB 1138,1142 ---START
function getListViewChecked()
{
    var args = getListViewChecked.arguments;
    var rms = args[0];
    var _thisCntrl = 'lstRoomSelection';
    if(rms.length < 2)
    {
        var chkBoxList = document.getElementById(_thisCntrl);   
        var checkedValues = '';     
        if(eval(chkBoxList))
        {
            var chkBoxCount= chkBoxList.getElementsByTagName("input");
            for(var i=0;i<chkBoxCount.length;i++)
            {
                var checkBoxRef = chkBoxCount[i];
                if(checkBoxRef.checked == true)
                {
                    var lblChecked = checkBoxRef.nextSibling;
                    if(lblChecked.hasChildNodes())
                    {
                        var labelArray = lblChecked.getElementsByTagName('a');
                        if(labelArray.length > 0 )
                        {
                            for(var j=0;j<labelArray.length;j++)
                            {
                                if(checkedValues.length > 0 )
                                    checkedValues += ', ';
            
                                checkedValues += labelArray[j].title;
                            }
                       }
                    }
                }
            }
            rms = checkedValues + ', ';
        }
    }
    return rms;
}

function compareselected()
{
	rms = document.getElementById("selectedloc").value;
	rms = getListViewChecked(rms);
	rms = rms.replace(", ,",",");
    //rms += ",";
	if (rms.split(",").length <=2)
		alert("Please select at least 2 locations to compare.");
	else
	    if (rms.split(",").length > 6) //fogbugz case 179 kapil said it is 5
	        alert("Please select 2 to 5 rooms");
	else {
		url = "roomresourcecomparesel.aspx?wintype=pop&f=pop&rms=" + rms;
		rmresPopup = window.open(url,'roomresource','status=yes,width=500,height=280,resizable=yes,scrollbars=yes');
			
		rmresPopup.focus();
		if (!rmresPopup.opener) {
			rmresPopup.opener = self;
		}
	}
}
//FB 1138,1142 ---END
function chkresource(id)
{
	if (id != "") {
        if (id.indexOf(",") <0)
            id += ",";
		url = "roomresourcecomparesel.aspx?wintype=pop&f=pop&rms=" + id;
		rmresPopup = window.open(url,'roomresource','status=no,width=450,height=480,resizable=yes,scrollbars=yes');
		rmresPopup.focus();
		if (!rmresPopup.opener) {
			rmresPopup.opener = self;
		}
	}
}

function getOneRoom(evt)
{
    var obj;
    if (window.event != window.undefined)
       obj =  window.event.srcElement;
    else
       obj = evt.target; 
       
    var treeNodeFound = false; 
    var checkedState; 

    if (obj.tagName == "INPUT" && obj.type == "checkbox" && obj.checked) 
    {
        var elements = document.getElementsByTagName('input'); 
        for (i=0;i<elements.length;i++)
        if ( (elements.item(i).type == "checkbox") && (elements.item(i).id.indexOf("treeRoomSelection") == 0) ) 
        {
            if(elements.item(i).id!=obj.id) 
            {
                elements.item(i).checked= false; 
            }
            else
                if (opener)
                opener.document.getElementById("hdnLocation").value = obj.value;
        } 
    }
}

// code changed for FB 1319 -- start

function CheckBoxListSelect(cbControl, elementRef)
{   
   var chkBoxList = document.getElementById(cbControl);
   var chkBoxCount= chkBoxList.getElementsByTagName("input");

   
   var selloc = document.getElementById("selectedloc");
   var selectAll = document.getElementById("selectAllCheckBox");
    
    if(elementRef.checked == false)
        document.getElementById("selectedloc").value = '';
        
    for(var i=0;i<chkBoxCount.length;i++)
    {
        chkBoxCount[i].checked = elementRef.checked;
        
        if(elementRef.checked == true)
        {
            var lblChecked =  chkBoxCount[i].nextSibling;
            if(lblChecked.hasChildNodes())
            {
                var labelArray = lblChecked.getElementsByTagName('a');
                if(labelArray.length > 0 )
                {
                    for(var j=0;j<labelArray.length;j++)
                    {                    
                        if ((selloc.value.indexOf(" " + labelArray[j].title + ", ") >=0))
                            selloc.value = selloc.value;
                        else                        
                            selloc.value += " " + labelArray[j].title + ", ";                        
                    }
               }
            }
        }
    }
    
    selectAll.checked = elementRef.checked;
    
        return false; 
}


function getValues(evt)
{

    var obj;
    if (window.event != window.undefined)
       obj =  window.event.srcElement;
    else
       obj = evt.target; 

    if (obj.tagName == "INPUT")
    {
        var selloc = document.getElementById("selectedloc");
        var selectAll = document.getElementById("selectAllCheckBox");
        var lstrmSelection = document.getElementById("lstRoomSelection");
        var chkBoxCount= lstrmSelection.getElementsByTagName("input");
        var c = 0;
        
        if(obj.checked == true)
        {
            var lblChecked = obj.nextSibling;
            if(lblChecked.hasChildNodes())
            {
                var labelArray = lblChecked.getElementsByTagName('a');
                if(labelArray.length > 0 )
                {
                    for(var j=0;j<labelArray.length;j++)
                    {                    
                        if ((selloc.value.indexOf(" " + labelArray[j].title + ", ") >=0))
                            selloc.value = selloc.value;
                        else                        
                            selloc.value += " " + labelArray[j].title + ", ";                        
                    }
               }
            }
            
            if(c == 0)
            {
                for (var i=0; i < chkBoxCount.length; i++)
                {
                    if(chkBoxCount[i].checked == false)
                        c = 1;                   
                }
            }
            
            if(selectAll)
            {
                if(c == 1)
                   selectAll.checked = false;
                else
                   selectAll.checked = true;                
            }                          
        }        
        else
        {
            var lblChecked = obj.nextSibling;
            if(lblChecked.hasChildNodes())
            {
                var labelArray = lblChecked.getElementsByTagName('a');
                if(labelArray.length > 0 )
                {
                    for(var j=0;j<labelArray.length;j++)
                    {                    
                        if ((selloc.value.indexOf(" " + labelArray[j].title + ", ") >=0))
                            selloc.value = selloc.value.replace(" " + labelArray[j].title + ", ","") ;                                            
                    }
               }
            }
            
            if(selectAll)
                selectAll.checked = false;
        }
    }
}
//code changed for FB 1319 -- end

//Code added for Tab Calendar pages in daypilot
function getTabCalendar(evt)
{
// obj gives us the node on which check or uncheck operation has performed 
    var obj;
    if (window.event != window.undefined)
       obj =  window.event.srcElement;
    else
       obj = evt.target; 
       
    var treeNodeFound = false; 
    var checkedState;
    
    if (obj.tagName == "INPUT" && obj.type == "checkbox") 
    {     //1 checking whether obj consists of checkbox or not 
        var treeNode = obj; 
        //record the checked state of the TreeNode 
        checkedState = treeNode.checked; 
        //work our way back to the parent <table> element 
        do {
            obj = obj.parentNode; 
        } while (obj.tagName != "TABLE") 
        //keep track of the padding level for comparison with any children 
        var parentTreeLevel = obj.rows[0].cells.length; 
        var parentTreeNode = obj.rows[0].cells[0]; 
        //get all the TreeNodes inside the TreeView (the parent <div>) 
        var tables = obj.parentNode.getElementsByTagName("TABLE"); 
        //checking for any node is checked or unchecked during operation 
        if(obj.tagName == "TABLE") 
        { //2
            // if any node is unchecked then their parent node are unchecked 
            if (!treeNode.checked) 
            { //3
                //head1 gets the parent node of the unchecked node 
                var head1 = obj.parentNode.previousSibling; 
                if(head1.tagName == "TABLE") {
                //checks for the input tag which consists of checkbox 
                var matchElement1 = head1.getElementsByTagName("INPUT"); 
                //matchElement1[0] gives us the checkbox and it is unchecked 
                matchElement1[0].checked = false; 
            } //3
            else 
                head1 = obj.parentNode.previousSibling;
            if(head1.tagName == "TABLE") 
            { //4
            //head2 gets the parent node of the unchecked node 
                var head2 = obj.parentNode.parentNode.previousSibling; 
                if(head2.tagName == "TABLE") 
                { //5
                    //checks for the input tag which consists of checkbox 
                    var matchElement2 = head2.getElementsByTagName("INPUT"); 
                    matchElement2[0].checked = false; 
                } //5
            }//4
            else 
                head2 = obj.parentNode.previousSibling;
            if(head2.tagName == "TABLE") 
            {//6
                //head3 gets the parent node of the unchecked node 
                var head3 = obj.parentNode.parentNode.parentNode.previousSibling; 
                if(head3.tagName == "TABLE") 
                {//7
                    //checks for the input tag which consists of checkbox 
                    var matchElement3 = head3.getElementsByTagName("INPUT"); 
                    matchElement3[0].checked = false; 
                }//7
            }//6
            else 
                head3 = obj.parentNode.previousSibling;
            if(head3.tagName == "TABLE") 
            { //7
               //head4 gets the parent node of the unchecked node 
                var head4 = obj.parentNode.parentNode.parentNode.parentNode.previousSibling; 
                if(head4 != null) 
                {//8 
                    if(head4.tagName == "TABLE") 
                    {//8
                        //checks for the input tag which consists of checkbox 
                        var matchElement4 = head4.getElementsByTagName("INPUT"); 
                        matchElement4[0].checked = false; 
                    }//8
                }//7
            } //6
        } //end if - unchecked
        //total number of TreeNodes 
        var numTables = tables.length 
        if (numTables >= 1) 
        {
            for (i=0; i < numTables; i++) 
            {
                if (tables[i] == obj) 
                {
                    treeNodeFound = true; 
                    i++;
                    if (i == numTables) 
                        break;
                }
                if (treeNodeFound == true) 
                {
                    var childTreeLevel = tables[i].rows[0].cells.length; 
                    if (childTreeLevel > parentTreeLevel) 
                    { 
                        var cell = tables[i].rows[0].cells[childTreeLevel - 1]; 
                        var inputs = cell.getElementsByTagName("INPUT"); 
                        inputs[0].checked = checkedState; 
                    } 
                    else 
                        break;
                } //end if 
                if (childTreeLevel == "5")
                {
                    if (inputs[0].checked)
                    {
                        var tnName = inputs[0].id; var nodeNum;
                        if (tnName.indexOf("Wizard1") >= 0)
                           nodeNum = tnName.substring(26, tnName.indexOf("CheckBox"));
                        else
                            nodeNum = tnName.substring(18, tnName.indexOf("CheckBox"));
                        if (nodeNum != "")
                        {
                            var obbj;
                            //alert(tnName.indexOf("Wizard1"));
                            if (tnName.indexOf("Wizard1") >= 0)
                                obbj = document.getElementById("Wizard1_treeRoomSelectiont" + nodeNum);
                            else
                                obbj = document.getElementById("treeRoomSelectiont" + nodeNum);
                            if (obbj.title != "")
                              if (document.getElementById("selectedloc").value.indexOf(" " + obbj.title + ", ") < 0)
                                document.getElementById("selectedloc").value += " " + obbj.title + ", ";
                        }   
                    }
                    else
                    {
                        var tnName = inputs[0].id;
                        var nodeNum;
                        if (tnName.indexOf("Wizard1") >= 0)
                           nodeNum = tnName.substring(26, tnName.indexOf("CheckBox"));
                        else
                            nodeNum = tnName.substring(18, tnName.indexOf("CheckBox"));
                        if (nodeNum != "")
                        {
                            var obbj;
                            
                            if (tnName.indexOf("Wizard1") >= 0)
                                obbj = document.getElementById("Wizard1_treeRoomSelectiont" + nodeNum);
                            else
                                obbj = document.getElementById("treeRoomSelectiont" + nodeNum);
                            var obbbj = document.getElementById("selectedloc");
                            if ((obbbj.value.indexOf(" " + obbj.title + ", ") >=0) && (obbj.title != ""))
                                 obbbj.value = obbbj.value.replace(" " + obbj.title + ", ", "");
                        }
                    }
                }
            }//end for 
        } //end if - numTables >= 1

    // if all child nodes are checked then their parent node is checked
    //Wizard1_treeRoomSelectiont4
    //Wizard1_treeRoomSelectionn4Checkbox
    if (treeNode.checked) 
    {
        var tnName = treeNode.id;
        var nodeNum;
        if (tnName.indexOf("Wizard1") >= 0)
           nodeNum = tnName.substring(26, tnName.indexOf("CheckBox"));
        else
            nodeNum = tnName.substring(18, tnName.indexOf("CheckBox"));
        if (nodeNum != "")
        {
            var obbj;
            if (tnName.indexOf("Wizard1") >= 0)
                obbj = document.getElementById("Wizard1_treeRoomSelectiont" + nodeNum);
            else
                obbj = document.getElementById("treeRoomSelectiont" + nodeNum);
                
            if (obbj == null)
            {
                obbj = document.getElementById(nodeNum);
                tle = obbj.parentNode.title;
            }
            else
                tle = obbj.title;
            if (obbj != null)
            if (tle != "")
              if (document.getElementById("selectedloc").value.indexOf(" " + tle + ", ") < 0)
                document.getElementById("selectedloc").value += " " + tle + ", ";
        }
        var chk1 = true;
        var head1 = obj.parentNode.previousSibling;
        var pTreeLevel1 = obj.rows[0].cells.length;
        if(head1.tagName == "TABLE") 
        {
            var tbls = obj.parentNode.getElementsByTagName("TABLE");
            var tblsCount = tbls.length;
            for (i=0; i < tblsCount; i++) 
            {
                var childTreeLevel = tbls[i].rows[0].cells.length;
                if (childTreeLevel = pTreeLevel1) 
                {
                    var chld = tbls[i].getElementsByTagName("INPUT");
                    if (chld[0].checked == false) 
                    {
                        chk1 = false;
                        break;
                    }
                }
            }
            var nd = head1.getElementsByTagName("INPUT");
            nd[0].checked = chk1;
        }
        else
            head1 = obj.parentNode.previousSibling;
        var chk2 = true;
        if(head1.tagName == "TABLE") 
        {
            var head2 = obj.parentNode.parentNode.previousSibling; 
            if(head2.tagName == "TABLE") 
            {
                var tbls = head1.parentNode.getElementsByTagName("TABLE");
                var pTreeLevel2 = head1.rows[0].cells.length;
                var tblsCount = tbls.length;
                for (i=0; i < tblsCount; i++) 
                {
                    var childTreeLevel = tbls[i].rows[0].cells.length;
                    if (childTreeLevel = pTreeLevel2) 
                    {
                        var chld = tbls[i].getElementsByTagName("INPUT");
                        if (chld[0].checked == false) 
                        {
                            chk2 = false;
                            break;
                        }
                    }
                }
                var nd = head2.getElementsByTagName("INPUT");
                nd[0].checked = (chk2 && chk1);
            }
        }
        else 
            head2 = obj.parentNode.previousSibling;
        var chk3 = true;
        if(head2.tagName == "TABLE") 
        {
            var head3 = obj.parentNode.parentNode.parentNode.previousSibling; 
            if(head3.tagName == "TABLE") 
            {
                var tbls = head2.parentNode.getElementsByTagName("TABLE");
                var pTreeLevel3 = head2.rows[0].cells.length;
                var tblsCount = tbls.length;
                for (i=0; i < tblsCount; i++) 
                {
                    var childTreeLevel = tbls[i].rows[0].cells.length;
                    if (childTreeLevel = pTreeLevel3) 
                    {
                        var chld = tbls[i].getElementsByTagName("INPUT");
                        if (chld[0].checked == false) 
                        {
                            chk3 = false;
                            break;
                        }
                    }
                }
                var nd = head3.getElementsByTagName("INPUT");
                nd[0].checked = (chk3 && chk2 && chk1);
            }
        }
        else 
            head3 = obj.parentNode.previousSibling;
        var chk4 = true;
        if(head3.tagName == "TABLE") 
        {
            var head4 = obj.parentNode.parentNode.parentNode.parentNode.previousSibling; 
            //Added for FB 1415,1416,1417,1418 Start
            if(head4 != null)
            {
            //Added for FB 1415,1416,1417,1418 End
                if(head4.tagName == "TABLE") 
                {
                    var tbls = head3.parentNode.getElementsByTagName("TABLE");
                    var pTreeLevel4 = head3.rows[0].cells.length;
                    var tblsCount = tbls.length;
                    for (i=0; i < tblsCount; i++) 
                    {
                        var childTreeLevel = tbls[i].rows[0].cells.length;
                        if (childTreeLevel = pTreeLevel4) 
                        {
                            var chld = tbls[i].getElementsByTagName("INPUT");
                            if (chld[0].checked == false) 
                            {
                                chk4 = false;
                                break;
                            }
                        }
                    }
                    var nd = head4.getElementsByTagName("INPUT");
                    nd[0].checked = (chk4 && chk3 && chk2 && chk1);
                    //alert(chk1.value + " : " + chk2.value + " : " + chk3.value + " : " + chk4.value);
                }
            }  //Added for FB 1415,1416,1417,1418 
        }
    } //end if - checked
    document.getElementById("showDeletedConf").checked = false; //FB 1800 
    document.getElementById ("IsSettingsChange").value = "Y";     
    document.getElementById ("btnDate").click();
    
  }
  
 } //end if
 
 

} //end function

function getTabValues(evt)
{

    var obj;
    if (window.event != window.undefined)
       obj =  window.event.srcElement;
    else
       obj = evt.target; 

    if (obj.tagName == "INPUT")
    {
        var selloc = document.getElementById("selectedloc");
        var selectAll = document.getElementById("selectAllCheckBox");
        var lstrmSelection = document.getElementById("lstRoomSelection");
        var chkBoxCount= lstrmSelection.getElementsByTagName("input");
        var c = 0;
        
        if(obj.checked == true)
        {
            var lblChecked = obj.nextSibling;
            if(lblChecked.hasChildNodes())
            {
                var labelArray = lblChecked.getElementsByTagName('a');
                if(labelArray.length > 0 )
                {
                    for(var j=0;j<labelArray.length;j++)
                    {                    
                        if ((selloc.value.indexOf(" " + labelArray[j].title + ", ") >=0))
                            selloc.value = selloc.value;
                        else                        
                            selloc.value += " " + labelArray[j].title + ", ";                        
                    }
               }
            }
            
            if(c == 0)
            {
                for (var i=0; i < chkBoxCount.length; i++)
                {
                    if(chkBoxCount[i].checked == false)
                        c = 1;                   
                }
            }
            
            if(selectAll)
            {
                if(c == 1)
                   selectAll.checked = false;
                else
                   selectAll.checked = true;                
            }                          
        }        
        else
        {
            var lblChecked = obj.nextSibling;
            if(lblChecked.hasChildNodes())
            {
                var labelArray = lblChecked.getElementsByTagName('a');
                if(labelArray.length > 0 )
                {
                    for(var j=0;j<labelArray.length;j++)
                    {                    
                        if ((selloc.value.indexOf(" " + labelArray[j].title + ", ") >=0))
                            selloc.value = selloc.value.replace(" " + labelArray[j].title + ", ","") ;                                            
                    }
               }
            }
            
            if(selectAll)
                selectAll.checked = false;
        }
    }
    
    document.getElementById ("IsSettingsChange").value = "Y";     
    document.getElementById ("btnDate").click();
}

function CheckBoxListSelectTab(cbControl, elementRef)
{   
   var chkBoxList = document.getElementById(cbControl);
   var chkBoxCount= chkBoxList.getElementsByTagName("input");

   
   var selloc = document.getElementById("selectedloc");
   var selectAll = document.getElementById("selectAllCheckBox");
    
    if(elementRef.checked == false)
        document.getElementById("selectedloc").value = '';
        
    for(var i=0;i<chkBoxCount.length;i++)
    {
        chkBoxCount[i].checked = elementRef.checked;
        
        if(elementRef.checked == true)
        {
            var lblChecked =  chkBoxCount[i].nextSibling;
            if(lblChecked.hasChildNodes())
            {
                var labelArray = lblChecked.getElementsByTagName('a');
                if(labelArray.length > 0 )
                {
                    for(var j=0;j<labelArray.length;j++)
                    {                    
                        if ((selloc.value.indexOf(" " + labelArray[j].title + ", ") >=0))
                            selloc.value = selloc.value;
                        else                        
                            selloc.value += " " + labelArray[j].title + ", ";                        
                    }
               }
            }
        }
    }
    
    selectAll.checked = elementRef.checked;
    
    document.getElementById ("IsSettingsChange").value = "Y";     
    document.getElementById ("btnDate").click();
    
        return false; 
}