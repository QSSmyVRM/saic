/*ZD 100147 Start*/
/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
/*ZD 100147 End*/
function new_category_prompt(promptpicture, prompttitle, sendto) 
{ 
	promptbox = document.createElement('div'); 
	promptbox.setAttribute ('id' , 'prompt');
	document.getElementsByTagName('body')[0].appendChild(promptbox);
	promptbox = eval("document.getElementById('prompt').style");

	w = 200;
	promptbox.position = 'absolute';
	document.getElementById('prompt').style.top = mousedownY - 36; 
	document.getElementById('prompt').style.left = mousedownX - w - 28;
	promptbox.width = w;
	promptbox.border = 'outset 1 #bbbbbb'; 

	m = "<table cellspacing='0' cellpadding='0' border='0' width='100%'><tr valign='middle'><td width='22' height='22' style='text-indent:2;' class='titlebar'><img src='" + promptpicture + "' height='18' width='18'></td><td class='titlebar'>" + prompttitle + "</td></tr></table>" 
	m += "<table cellspacing='0' cellpadding='0' border='0' width='100%' class='promptbox'>";
	m += "  <tr><td>";
	m += "    Category Name <input type='text' name='ncname' value='' style='width: 80pt'>";
	m += "  </td></tr>"
	m += "  <tr><td align='right'>"
	m += "    <input type='button' class='prompt' value='Submit' onMouseOver='this.style.border=\"1 outset #dddddd\"' onMouseOut='this.style.border=\"1 solid transparent\"' onClick='" + sendto + "(" + "document.getElementById(\"ncname\").value" + ");'>"
	m += "    <input type='button' class='prompt' value='Cancel' onMouseOver='this.style.border=\"1 outset transparent\"' onMouseOut='this.style.border=\"1 solid transparent\"' onClick='document.getElementsByTagName(\"body\")[0].removeChild(document.getElementById(\"prompt\"))'>"
	m += "  </td></tr>"
	m += "</table>" 
	
	document.getElementById('prompt').innerHTML = m;
} 

function saveNewCategory(ncn) 
{
	if (Trim(ncn) == "") {
		alert(EN_151);
		document.getElementById("ncname").focus();
		return false;
	}

	ifrmCategory.document.frmIfrmcategory.newcategory.value = ncn;
	ifrmCategory.document.frmIfrmcategory.cmd.value = "AddCategory";
	ifrmCategory.document.frmIfrmcategory.submit();

	document.getElementsByTagName("body")[0].removeChild(document.getElementById("prompt"));
} 
