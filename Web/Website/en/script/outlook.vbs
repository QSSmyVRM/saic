'/*ZD 100147 Start*/
'/* Copyright (C) 2013 myVRM - All Rights Reserved
'* You may not use, distribute and/or modify this code under the
'* terms of the myVRM license.
'*
'* You should have received a copy of the myVRM license with
'* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
'*/
'/*ZD 100147 End*/
'******************************************************************************
' ContactsAsXML
'
'	Pull all the contacts out of the local instance of Outlook and build an
'	XML string. Format:
'	<users>
'		<user>
'			<userName>
'				<userFirstName>Derek</userFirstName>
'				<userLastName>Jeter</userLastName>
'			</userName>
'			<userEmail>djeter@nyyankees.com</userEmail>
'		<user>
'	</users>
'******************************************************************************
Function ContactsAsXML()
	'Get all the necessary objects; application, namespace, contactsFolder,
	'and items list. The constant identifing the contacts folder is 10.
	On Error Resume Next
		Set outlookApp = CreateObject("Outlook.Application")
		Set	mapiNamespace = outlookApp.GetNamespace("MAPI")
		Set	contactFolder = mapiNamespace.GetDefaultFolder(10)
		Set contacts = contactFolder.Items

		If Err.Number <> 0 Then
			MsgBox "Fail to connect to your Outlook, please make sure it is correctly installed and activated." & VBCRLF & "Under Tools > Advanced Settings, make sure that download ActiveX Objects is set to PROMPT or ENABLE." & VBCRLF & "This feature is only available for IE browser." & VBCRLF & "Or, please contact your administrator for solution." & VBCRLF & VBCRLF & "Click OK to close window.", VBExclamation, "Error"
			ContactsAsXML = "-1"
			Exit Function
		End If

	
	contacts.Sort "[FirstName]"
	numContacts = contacts.Count
	
	i = 0

		'Open users tag.
	xml = "<users>"
		'Loop through the contact list. For each contact create a user tag.
	if (numContacts <> 0) then
		Set contact = contacts.GetFirst()
		while (i < numContacts) And (NOT(isNull(contact)))
			xml = xml + ContactAsXML( contact)
			Set contact = contacts.GetNext()
			i = i+1
		wend
	end if
		'Close the users tag.
	xml = xml + "</users>"
	
		'Return the list of contacts as XML
	ContactsAsXML = xml
'	alert(xml)
End Function


'******************************************************************************
' ContactAsXML
'
'	Given a contact as input build an XML string
'	Format:
'		<user>
'			<userName>
'				<userFirstName>Derek</userFirstName>
'				<userLastName>Jeter</userLastName>
'			</userName>
'			<userEmail>djeter@nyyankees.com</userEmail>
'		<user>
'******************************************************************************
Function ContactAsXML( contact)
	On Error Resume Next
	if NOT(isNull(contact)) then
		xml = "<user><userName><userFirstName>"
		
		if NOT(isNull(contact.FirstName)) then
			xml = xml + contact.FirstName
		end if
		xml = xml + "</userFirstName><userLastName>"

		if NOT(isNull(contact.LastName)) then
			xml = xml + contact.LastName
		end if
		xml = xml + "</userLastName></userName><userEmail>"

		if NOT(isNull(contact.Email1Address)) then
			xml = xml + contact.Email1Address
		end if
		xml = xml + "</userEmail></user>"
		ContactAsXML = xml
	end if
End Function


'******************************************************************************
' FindContacts
'	
'	Given a first and/or last name search the contacts local instance of Outlook
'	for a match. If any are found return the contacts as an XML string.
'	(See ContactsAsXML for format)
'******************************************************************************
Function FindContacts( targetFirstName, targetLastName, doCaseSensitive, similarTo)
	msgbox "Please make sure Outlook is correctly installed and opened." & Chr(10) & "Click OK to continue and click Yes if asking ActiveX interaction." & Chr(10) & "If nothing happens, please check IE setting or contact administrator.", vbExclamation, "Outlook Warning"

		'Get all the necessary objects; application, namespace, contactsFolder,
		'and items list. The constant identifing the contacts folder is 10.
	On Error Resume Next
		Set outlookApp = CreateObject("Outlook.Application")
		Set	mapiNamespace = outlookApp.GetNamespace("MAPI")
		Set	contactFolder = mapiNamespace.GetDefaultFolder(10)
		Set contacts = contactFolder.Items

		If Err.Number <> 0 Then
			MsgBox "Fail to connect to your Outlook, please make sure it is correctly installed and activated." & VBCRLF & "Or, please contact your administrator for solution." & VBCRLF & VBCRLF & "Click OK to close window.", VBExclamation, "Error"
			FindContacts = "-1"
			Exit Function
		End If

	numContacts = contacts.Count
	
		'Open the users tag.
	xml = "<users>"
	
		'Establish the flags gotFirstName and gotLastName. These only need to 
		'be determined once. These are invariant with respect to the loop.
	if( not (IsNull( targetFirstName) or (Len(targetFirstName) = 0))) then	
		gotFirstName = true
	else
		gotFirstName = false
	end if
	
	if( not (IsNull( targetLastName) or (Len(targetLastName) = 0)))	then
		gotLastName = true
	else
		gotLastName = false
	end if
	
		'Loop through the contacts list. If we find one that matches the search
		'criteria, ie. matches/similar to first and or last name, add them to the
		'xml.
	while i < numContacts
			'Get next contact.
		if i = 0 then
			Set contact = contacts.GetFirst()
		else
			Set contact = contacts.GetNext()
		end if
		
			'See if we add the contact.
		add = false
		if( gotFirstName) then
			if( Compare(targetFirstName, contact.FirstName,doCaseSensitive, similarTo))then
				add = true
			else
				add = false
			end if
		end if
		
		if( gotLastName) then
			if( Compare(targetLastName,contact.LastName ,doCaseSensitive, similarTo)) then
				add = true
			else
				add = false
			end if
		end if
		
			'If add is true add contact to xml.
		if( add) then
			xml = xml + ContactAsXML( contact)
		end if
		i=i+1
	wend
	
		'Close the users tag.
	xml = xml + "</users>"
	
		'Return xml list of hits.
	FindContacts = xml
End Function

'******************************************************************************
' Compare
'	
'	Compares str to orig. Function returns true if str is identical to orig or
'   if str is shorter the function returnd true if str matches its characters
'   against orig. Ex:
'   str     orig    return
'   abcd    abcd    true
'   ab      abcd    true
'   bc      abcd    false
'   abcde   abcd    false
'******************************************************************************
Function Compare( str, orig, caseSensitive, similarTo)
	
    strLen = Len( str)
    origLen = Len( orig)
    compareFlag = 1
    
    Compare = false
    
		'Set the compare flag based on the input parameter.
		'0 does a case sensitive comparison. (binary)
		'1 does a case insensitive comparison. (character)
    if( caseSensitive) then
        compareFlag = 0
    end if
    
		'If the strings are the same length we just compare them straight up.
		'Nothing fancy here.
    if( strLen = origLen) then
        if( StrComp( str, orig, compareFlag) = 0) then
            Compare = true
        else
            Compare = false
        end if
     elseif( similarTo) then
			'If the guess is shorter then the original string we compare only
			'those characters that overlap the guess.
		if( strLen < origLen) then
			temp = Left( orig, strLen)
			
			if( StrComp( str, temp, compareFlag) = 0) then
				Compare = true
			else
				Compare = false
			end if
		end if
	else
			'If we end up here we definitely didn't match anything.
		Compare = false
    end if
    
End Function



'******************************************************************************
' AddAppointment
' 
'	startDt		Date and time appointment starts	6/10/2002 1:00PM
'	endDT		Date and time appointment ends
'	subject		Subject of the meeting
'	isRecurring	Flag identifying this is a recurring meeting
'	recStart	Date recurrance starts
'	recEnd		Date recurrence ends
'	freq		Frequency of the recurring meeting			
'											0=daily
'											1=weekly
'											2=monthly
'											5=yearly
'											11=biweekly
'											12=quartly
'******************************************************************************
Sub AddAppointment(apptstr)
	msgbox "Please make sure Outlook is correctly installed and opened." & Chr(10) & "Click OK to continue and click Yes if asking ActiveX interaction." & Chr(10) & "If nothing happens, please check IE setting or contact administrator.", vbExclamation, "Outlook Warning"
	if (Instr(apptstr, "#5#") <> 0) then
		MsgBox ("This is a Custom Select Dates recurring conference. Because Outlook do not support custom recurring, each instance has to been saved as individual conference.")
	end if
		
	Dim aatint(5)
	Dim arpint(17)
	Dim arrint(4)
	Dim i

	apptary = Split (apptstr, "|", -1, 1)

	On Error Resume Next
		Set outlookApp = CreateObject("Outlook.Application")
		If Err.Number <> 0 Then
			MsgBox "Fail to connect to your Outlook, please make sure it is correctly installed and activated." & VBCRLF & "Or, please contact your administrator for solution." & VBCRLF & VBCRLF & "Click OK to close window.", VBExclamation, "Error"
			Exit Sub
		End If
	
	
	if apptary(2) = "0" then	' non-recur
		Set	apptItem = outlookApp.CreateItem(1)
		
		apptItem.Subject = apptary(0)
		apptItem.Location = apptary(1)

		apptseary = Split (apptary(3), "&", -1, 1)
		apptItem.Start = apptseary(0)
		apptItem.End = apptseary(1)
'MsgBox("apptItem.Start = " & apptItem.Start & "; apptItem.End = " & apptItem.End)	
		
		apptItem.Save
		apptItem.Display

		Set	apptItem = Nothing
	else						' recur

		rary = Split(apptary(3), "#", -1, 1)
		atstr = rary(0)
		rpstr = rary(1)
		rrstr = rary(2)

		atary = Split(atstr, "&", -1, 1)
		aatint(0) = cInt(atary(0))	:	aatint(1) = cInt(atary(1))	:	aatint(2) = cInt(atary(2))
		aatint(3) = atary(3)		:	aatint(4) = cInt(atary(4))
		
		if rpstr = "5" then
			arpint(0) = 5
			
			rrstrary = Split(rrstr, "&")
			for i=0 to Ubound(rrstrary)
				Set	apptItem = outlookApp.CreateItem(1)
				apptItem.Subject = apptary(0)
				apptItem.Location = apptary(1)

				apptItem.Start = rrstrary(i) & " " & aatint(1) & ":" & aatint(2) & " " & aatint(3)
				apptItem.End = DateAdd("n", aatint(4),apptItem.Start)
'MsgBox("apptItem.Start = " & apptItem.Start & "; apptItem.End = " & apptItem.End)	
				apptItem.Save
'				apptItem.Display

				Set	apptItem = Nothing
			next
			
		else
			Set	apptItem = outlookApp.CreateItem(1)
			apptItem.Subject = apptary(0)
			apptItem.Location = apptary(1)

			rpary = Split(rpstr, "&", -1, 1)
			for i = 0 to 3
				arpint(i) = cInt(rpary(i))	
			next
			arpint(4) = rpary(4)
			for i = 5 to 16		
				arpint(i) = cInt(rpary(i))	
			next

			rrary = Split(rrstr, "&", -1, 1)
			arrint(0) = rrary(0)	:	arrint(1) = cInt(rrary(1))	:	arrint(2) = cInt(rrary(2))	:	arrint(3) = rrary(3)
		
			apptItem.Start = rrary(0) & " " & atary(1) & ":" & atary(2) & " " & atary(3) 
			apptItem.Duration = atary(4)
			Set recPattern = apptItem.GetRecurrencePattern()
			select case arpint(0)
				case 1
					select case arpint(1)
						case 1
							recPattern.RecurrenceType = 0
							recPattern.Interval = arpint(2)
						case 2
							recPattern.RecurrenceType = 1
							recPattern.DayOfWeekMask = (2 Or 4 Or 8 Or 16 Or 32)
							recPattern.Interval = 1
					end select
				case 2
					index1 = 1	:	index2 = 1	:	dowm = 0
					if arpint(4)<>"" then
						if InStr(index1, arpint(4), ",") <> 0 then
							while (InStr(index1, arpint(4), ",") <> 0)
								index2 = InStr(index1, arpint(4), ",")

								dowm = ( dowm Or (2 ^ (cInt(Mid(arpint(4), index1, index2 - index1)) - 1) ) )
								index1 = index2 + 1
							wend
						end if
						dowm = ( dowm Or (2 ^ (cInt(Mid(arpint(4), index1, len(arpint(4)) - index1 + 1)) - 1) ) )
					end if

					recPattern.RecurrenceType = 1
					recPattern.DayOfWeekMask = dowm
					recPattern.Interval = arpint(3)
				case 3
					select case arpint(5)
						case 1
							recPattern.RecurrenceType = 2
							recPattern.DayOfMonth = arpint(6)
							recPattern.Interval = arpint(7)
						case 2
							select case arpint(9)
								case 1
									dowm = (1 Or 2 Or 4 Or 8 Or 16 Or 32 Or 64)
								case 2
									dowm = (2 Or 4 Or 8 Or 16 Or 32)
								case 3
									dowm = (1 Or 64) 
								case 4
									dowm = 1
								case 5
									dowm = 2
								case 6
									dowm = 4
								case 7
									dowm = 8
								case 8
									dowm = 16
								case 9
									dowm = 32
								case 10
									dowm = 64
							end select

							recPattern.RecurrenceType = 3
							recPattern.Instance = arpint(8)
							recPattern.DayOfWeekMask = dowm
							recPattern.Interval = arpint(10)
					end select
				case 4
					select case arpint(11)
						case 1
							recPattern.RecurrenceType = 5
							recPattern.MonthOfYear = arpint(12)
							recPattern.DayOfMonth = arpint(13)
						case 2
							select case arpint(15)
								case 1
									dowm = (1 Or 2 Or 4 Or 8 Or 16 Or 32 Or 64)
								case 2
									dowm = (2 Or 4 Or 8 Or 16 Or 32)
								case 3
									dowm = (2 Or 32) 
								case 4
									dowm = 1
								case 5
									dowm = 2
								case 6
									dowm = 4
								case 7
									dowm = 8
								case 8
									dowm = 16
								case 9
									dowm = 32
								case 10
									dowm = 64
							end select
							recPattern.RecurrenceType = 6
							recPattern.Instance = arpint(14)
							recPattern.DayOfWeekMask = dowm
							recPattern.MonthOfYear = arpint(16)
					end select
			end select
			recPattern.PatternStartDate = rrary(0)

			select case rrary(1)
				case 1
					recPattern.NoEndDate = false
				case 2
					recPattern.Occurrences  = rrary(2)
				case 3
					recPattern.PatternEndDate = rrary(3)
			end select
		
		
			apptItem.Save
			apptItem.Display
			
	
			Set	apptItem = Nothing
		end if

	end if
	
	
	Set outlookApp = Nothing
End Sub


'******************************************************************************
' GetAppointmentsFor
' 
'   Return all the appointments for a given date in xml format.
'	Format:
'		<appointments>
'			<appointment>
'				<subject></subject>
'				<start></start>
'				<end></end>
'			</appointment>
'		</appointments>
'
'******************************************************************************
Function GetAppointmentsFor (cfDate)
	On Error Resume Next
		Set myOlApp = CreateObject("Outlook.Application")
		Set	myAppt = myOlApp.CreateItem(1)

		Set myNS = myOlApp.GetNamespace("MAPI")
		Set myAppts = myNS.GetDefaultFolder(9).Items

		If Err.Number <> 0 Then
			MsgBox "Fail to connect to your Outlook, please make sure it is correctly installed and activated." & VBCRLF & VBCRLF & "Click Tools | Internet Options | Security TAB | Custom Level | Select 'Prompt ON' for the Initialize and script ActiveX Controls setting." & VBCRLF & VBCRLF & "Or, please contact your administrator for solution." & VBCRLF & VBCRLF & "Click OK to close window.", VBExclamation, "Error"
			GetAppointmentsFor = "-1"
			Exit Function
		End If
    

	strToday = "[Start] >= '" & cfDate & " 12:00 am' and [Start] < '" & cfDate & " 11:59 pm'" _
				& " or [End] >= '" & cfDate & " 12:00 am' and [End] < '" & cfDate & " 11:59 pm'"

	myAppts.IncludeRecurrences = True
    myAppts.Sort "[Start]"
 	Set myAppts = myAppts.Restrict(strToday)


	myApptsCount = 0
	xml = "<appointments>"
	
    Set myAppt = myAppts.GetFirst
    Do While TypeName(myAppt) <> "Nothing"
'		strMsg = strMsg & vbLf & myAppt.Subject
'		strMsg = strMsg & " at " & FormatDateTime(myAppt.Start, 2) & vbCRLF
	
		xml = xml + AppointmentToXML(myAppt)

		myApptsCount = myApptsCount + 1
        Set myAppt = myAppts.GetNext
    Loop
	xml = xml & "</appointments>"

'	MsgBox(xml  & vbCRLF & vbCRLF & myApptsCount)

	GetAppointmentsFor = xml
End Function



Function GetAppointmentsFor1( aDate)
	On Error Resume Next
		Set outlookApp = CreateObject("Outlook.Application")
		Set	mapiNamespace = outlookApp.GetNamespace("MAPI")
		Set	calendarFolder = mapiNamespace.GetDefaultFolder(9)
		Set items = calendarFolder.Items

		If Err.Number <> 0 Then
			MsgBox "Fail to connect to your Outlook, please make sure it is correctly installed and activated." & VBCRLF & "Or, please contact your administrator for solution." & VBCRLF & VBCRLF & "Click OK to close window.", VBExclamation, "Error"
			GetAppointmentsFor = "-1"
			Exit Function
		End If
	
	'dateOnly = DateOnly( aDate)
	
	numAppointments = items.Count
	xml = "<appointments>"
	
	i = 0
	while i < numAppointments
			'Get next appointment.
		if i = 0 then
			Set appt = items.GetFirst()
		else
			Set appt = items.GetNext()
		end if
		
		'temp = DateOnly( appt.Start)
		
		if( DateDiff( "d", CStr(aDate), CStr(appt.Start)) = 0) then
			xml = xml + AppointmentToXML( appt)
		end if
		
		i = i + 1
	wend
	
	GetAppointmentsFor1 = xml + "</appointments>"
	
End Function

'******************************************************************************
' FindConflicts
' 
'   Return all the appointments that conflict with the proposed appointment
'
'	appStart	Start time for the appointment "08/15/2002 1:00 PM"
'	appEnd	End time for the appointment  "08/15/2002 1:00 PM"
'
'	Format:
'		<appointments>
'			<appointment>
'				<subject></subject>
'				<start></start>
'				<end></end>
'			</appointment>
'		</appointments>
'
'******************************************************************************
Function FindConflicts( apptStart, apptEnd)
	On Error Resume Next
		Set outlookApp = CreateObject("Outlook.Application")
		Set mapiNamespace = outlookApp.GetNamespace("MAPI")
		Set calendarFolder = mapiNamespace.GetDefaultFolder(9)
		Set items = calendarFolder.Items

		If Err.Number <> 0 Then
			MsgBox "Fail to connect to your Outlook, please make sure it is correctly installed and activated." & VBCRLF & "Or, please contact your administrator for solution." & VBCRLF & VBCRLF & "Click OK to close window.", VBExclamation, "Error"
			FindConflicts = "-1"
			Exit Function
		End If
		
	numAppointments = items.Count
	xml = "<appointments>"
	
	i = 0
	while i < numAppointments
			'Get next appointment.
		if i = 0 then
			Set appt = items.GetFirst()
		else
			Set appt = items.GetNext()
		end if
		
		if( HasScheduleConflict( apptStart, apptEnd, appt.Start, appt.End)) then
			xml = xml + AppointmentToXML( appt)
		end if
		
		i = i + 1
	wend
	
	FindConflicts = xml + "</appointments>"
	
End Function


'******************************************************************************
' AppointmentToXML
' 
'   Build an XML string for the given appointment
'	Format:
'			<appointment>
'				<subject></subject>
'				<start></start>
'				<end></end>
'			</appointment>
'
'******************************************************************************
Function AppointmentToXML( appt)
	xml = "<appointment>"
	
	xml = xml + "<subject>"
	xml = xml + appt.Subject
	xml = xml + "</subject>"
	
	xml = xml + "<start>"
	xml = xml + CStr( appt.Start)
	xml = xml + "</start>"
	
	xml = xml + "<end>"
	xml = xml + CStr( appt.End)
	xml = xml + "</end>"
	
	AppointmentToXML = xml + "</appointment>"
End Function


'******************************************************************************
' DateOnly
' 
'   Truncates the time off a give date (timestamp)
'
'******************************************************************************
Function DateOnly( timestamp)
	DateOnly = DateSerial( DatePart( "YYYY", timestamp),DatePart("M",timestamp),DatePart("D",timestamp))
End Function


'******************************************************************************
'HasScheduleConflict
' 
' Function returns true if the proposed item time conficts with an existing
' block of time false otherwise.
'
'  itemStart,itemEnd	date/time string for the proposed appointment.
'  blockStart,blockEnd	date/time string for a block of time already scheduled.
'
'******************************************************************************
Function HasScheduleConflict( itemStart, itemEnd, blockStart, blockEnd)
	deltaItem = DateDiff( "n", itemStart, itemEnd)
	deltaItemStart = DateDiff( "n", itemEnd, blockStart)
	deltaItemEnd = DateDiff( "n", blockEnd, itemStart)
	
	HasScheduleConflict = not((deltaItem > 0) and ((deltaItemStart >= 0) or (deltaItemEnd >=0)))
End Function


'******************************************************************************
'DelAppointment
' 
'	Delete Apppointment
'
'******************************************************************************
Sub DelAppointment(hn, cn, starttime, durtime, recur, delrec)
'alert("hostname=" & hn & ", confname=" & cn & ", starttime=" & starttime & ", durtime=" & durtime & ", recur=" & recur & ", delrec=" & delrec)

	On Error Resume Next
		Set myOlApp = CreateObject("Outlook.Application")
		Set	myAppt = myOlApp.CreateItem(1)

		If Err.Number <> 0 Then
			MsgBox "Fail to connect to your Outlook, please make sure it is correctly installed and activated." & VBCRLF & "Or, please contact your administrator for solution." & VBCRLF & VBCRLF & "Click OK to close window.", VBExclamation, "Error"
			Exit Sub
		End If

	endtimestr = DateAdd("n", durtime, starttime)
	endtime = Left(endtimestr, InStrRev(endtimestr, ":")-1) & Right(endtimestr, Len(endtimestr)-InStrRev(endtimestr, " ")+1)

	strTheDay = "[Start]='" & starttime & "' And [End]='" & endtime & "'"
'	strTheDay = "[PatternStartDate]='12/17/2002 6:15 AM' And [PatternEndDate]='12/27/2002 8:30 AM'"

'	alert(Left(starttime, InStr(starttime, " ")-1))


'alert("strTheDay=" & strTheDay)

    Set myNS = myOlApp.GetNamespace("MAPI")
    Set myAppts = myNS.GetDefaultFolder(9).Items

    myAppts.Sort "[Start]"
	If recur = "99" Then
		myAppts.IncludeRecurrences = Flase
	Else
		myAppts.IncludeRecurrences = True
	End If
	
'	If (delrec="-12") Or (delrec="02") Then
'		myAppts.IncludeRecurrences = Flase
'	End If
'alert(myAppts.IncludeRecurrences)

    Set myAppts = myAppts.Restrict(strTheDay)

    myAppts.Sort "[Start]"

	myApptsCount = 0
    Set myAppt = myAppts.GetFirst
    Do While TypeName(myAppt) <> "Nothing"

		strMsg = strMsg & vbLf & myAppt.Subject
		strMsg = strMsg & " at " & FormatDateTime(myAppt.Start, 3)
		myApptsCount = myApptsCount + 1
        Set myAppt = myAppts.GetNext
    Loop

'alert(myApptsCount)

    ' Display the information
	If myApptsCount <> 1 Then

		If myApptsCount = 0 Then
			MsgBox "Your old conference information is not exist in Outlook." & vbLf & vbLf & "The reason maybe:" & vbLf & "* You did not save the old conference in Outlook." & vbLf & "* You change this conference." & vbLf & "* other..." & vbLf & vbLf & "Please contact Expedite for assistance."
		Else
			MsgBox "Your have multiple existed appointments have the same time." & vbLf & "You need to delete the correct one manually." & vbLf & vbLf & "The appointments are:" & strMsg
		End If

	Else

		Set myAppt = myAppts.GetFirst
'alert(myAppt.Start)

		If (delrec="-11") Or (delrec="01") Then
'alert("GetRecurrencePattern")
'			Set myApptRecPatt = myAppt.GetRecurrencePattern
'			alert(myApptRecPatt.PatternEndDate)
'			alert(myApptRecPatt.IsRecurring)
'alert("ClearRecurrencePattern")
			Set myApptException = myApptRecPatt.Exceptions.Item(1)
'alert("1 end")
			MsgBox "Deleted = " & myApptException.Deleted
'alert("2 end")
'			myAppt.ClearRecurrencePattern()
'alert("ClearRecurrencePattern end")
'			myAppt.Start = "12/19/2002 10:15 AM"
'			myAppt.End = "12/19/2002 12:30 AM"
'			myAppt.Subject = "123"
'			myAppt.Update()
		End If

'alert("delete myAppt:" + myAppt.Subject)
		myAppt.Delete
'alert("delete done")

	End If

    Set myOlApp = Nothing
    Set myAppt = Nothing
    Set myNS = Nothing
    Set myAppts = Nothing
End Sub
