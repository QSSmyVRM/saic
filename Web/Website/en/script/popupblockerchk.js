/*ZD 100147 Start*/
/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
/*ZD 100147 End*/
if (navigator.appName == "Microsoft Internet Explorer") {
	popwintester = window.open("expired_page.htm",'','status=no,width=0,height=0,left=5000,top=5000,resizable=no,scrollbars=no');
	if (popwintester) {
		popwintester.close();
	} else {
		alert("Your browser has a popup blocker enabled. Please disable it for the myVRM website, as myVRM uses popups that are necessary for your information input.");
		popwintester = null;
	}
}
