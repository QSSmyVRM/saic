/*ZD 100147 Start*/
/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
/*ZD 100147 End*/
function replace_user_para_prompt(promptpicture, prompttitle, id, ofn, oln, olgn, opwd, oe) 
{
	promptbox = document.createElement('div'); 
	promptbox.setAttribute ('id' , 'prompt');
	document.getElementsByTagName('body')[0].appendChild(promptbox);
	promptbox = eval("document.getElementById('prompt').style");

	promptbox.position = 'absolute'
	promptbox.top = mousedownY;
	promptbox.left = mousedownX - 400;
	promptbox.width = 400 
	promptbox.border = 'outset 1 #bbbbbb' 
	
	m = "<table cellspacing='0' cellpadding='0' border='0' width='100%'><tr valign='middle'><td width='22' height='22' style='text-indent:2;' class='titlebar'><img src='" + promptpicture + "' height='18' width='18'></td><td class='titlebar'>" + prompttitle + "</td></tr></table>" 
	m += "<table cellspacing='0' cellpadding='0' border='0' width='100%' class='promptbox'>";
/*
	m += "  <tr>";

	switch (emailclt) {
		case "1" :
			if (enableout.toLowerCase() == "true") {
				if (navigator.appName.indexOf('Microsoft') !=-1)
					m += "    <td align='center'><input type='button' name='btnoutlookup' id='btnoutlookup' value='MS Outlook Look Up' class='prompt1' onClick='getOutLookEmailList();'></td>";
				else
					m += "    <td align='center'><input type='button' name='btnoutlookup' id='btnoutlookup' value='MS Outlook Look Up' title='Netscape/Firefox does not support Outlook function' class='prompt1'></td>";
			}
			break;
			
		case "2" :
			if (navigator.appName.indexOf('Microsoft') !=-1)
				m += "    <td align='center'><input type='button' name='btnlnlookup' id='btnlnlookup' value='Lotus Notes Look Up' class='prompt1' onClick='getLotusEmailList(\"" + lnLoginName + "\",\"" + lnLoginName + "\",\"" + lnLoginName + "\");'></td>";
			else
				m += "    <td align='center'><input type='button' name='btnlnlookup' id='btnlnlookup' value='Lotus Notes Look Up' title='Netscape/Firefox does not support Lotus Notes function' class='prompt1'></td>";
			break;
			
		default :
			m += "    <td align='center'></td>";
			break;
	}

	m += "    <td align='center' height='30'><input type='button' name='btnvrmlookup' id='btnvrmlookup' value='myVRM Look Up' class='prompt1'></td>";
//	m += "    <td align='center'><input type='button' name='btnvrmlookup' id='btnvrmlookup' value='myVRM Look Up' style='width: 160pt'></td>";
	m += "  </tr>"
*/
	m += "  <tr>";
	m += "    <td>First Name <input type='text' name='nfn' id='nfn' maxlength='14' value='" + ofn + "' style='width: 75pt'></td>";
	m += "    <td>Last Name <input type='text' name='nln' id='nln' maxlength='14' value='" + oln + "' style='width: 75pt'></td>";
	m += "  </tr>"
	m += "  <tr>";
	m += "    <td>Login Name <input type='text' name='nlgn' id='nlgn' maxlength='14' value='" + olgn + "' style='width: 75pt'></td>";
	m += "    <td>Password <input type='password' name='npwd' id='npwd' maxlength='14' value='" + opwd + "' style='width: 75pt'></td>";
	m += "  </tr>"
	m += "  <tr>"
	m += "    <td colspan=2>Email <input type='text' name='ne' id='ne' maxlength='28' value='" + oe + "' style='width: 150pt'></td>";
	m += "  </tr>"
	m += "  <tr><td align='right' colspan=2>"
	m += "    <input type='button' name='btnvrmlookup' id='btnvrmlookup' value='myVRM Look Up' class='prompt1' onclick='javascript: getYourOwnEmailList(2);'>";
	m += "    <input type='button' class='prompt' value='Submit' onMouseOver='this.style.border=\"1 outset #dddddd\"' onMouseOut='this.style.border=\"1 solid transparent\"' onClick='saveInput(" + id + ");'>"
	m += "    <input type='button' class='prompt' value='Cancel' onMouseOver='this.style.border=\"1 outset transparent\"' onMouseOut='this.style.border=\"1 solid transparent\"' onClick='cancelthis(" + id +");'>"
	m += "  </td></tr>"
	m += "</table>" 
	
	document.getElementById('prompt').innerHTML = m;
} 


function saveInput(id) 
{
	nfncb = document.getElementById("nfn");
	nlncb = document.getElementById("nln");
	nlgncb = document.getElementById("nlgn");
	npwdcb = document.getElementById("npwd");
	necb = document.getElementById("ne");
	
	iscorrect1 = checkNewParty (nfncb, nlncb, necb);
	iscorrect2 = checkLgnPwd (nlgncb, npwdcb);
	
	if (iscorrect1 && iscorrect2) {
		document.getElementsByTagName("body")[0].removeChild(document.getElementById("prompt"));
		post_rpluser(id, nfncb.value + ";;" + nlncb.value + ";;" + nlgncb.value + ";;" + npwdcb.value + ";;" + necb.value, true);
	}
} 


function cancelthis(id)
{
	isreplaced = false;
	document.getElementsByTagName("body")[0].removeChild(document.getElementById("prompt"));
	post_rpluser(id, "", false);
}