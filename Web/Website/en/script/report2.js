/*ZD 100147 Start*/
/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
/*ZD 100147 End*/
function deleteAllParty()
{
	if (document.frmSettings2Report.PartysInfo.value != "") {
		var isRemove = confirm("Are you sure you want to remove all attendees?")
		if (isRemove == false) {
			return (false);
		}
	}
	
	document.frmSettings2Report.PartysInfo.value = "";
	var url = ifrmMemberlist.location + "";
	if(url.indexOf('report2member.asp') != -1){
		ifrmMemberlist.location.reload(); 
	}
}


function addNewParty()
{
	var url = ifrmMemberlist.location + "";
	if(url.indexOf('report2member.asp') != -1) {
		willContinue = ifrmMemberlist.bfrRefresh();
		if (willContinue) {
			partysinfo = document.frmSettings2Report.PartysInfo.value;
			partysinfo = ",,,,0;" + partysinfo;
			document.frmSettings2Report.PartysInfo.value = partysinfo;
			ifrmMemberlist.location.reload(); 
		}
	}	
}


function getYourOwnEmailList()
{
	var url = ifrmMemberlist.location + "";
	if(url.indexOf('report2member.asp') != -1){
		willContinue = ifrmMemberlist.bfrRefresh(); 
		if (willContinue) {
			ifrmMemberlist.location.reload(); 
			url = "dispatcher/conferencedispatcher.asp?frm=report&cmd=GetEmailList&emailListPage=1&wintype=pop";
			if (!window.winrtc) {	// has not yet been defined
				winrtc = window.open(url, "", "width=700,height=300,top=0,left=0,resizable=yes,scrollbars=yes,status=no");
				winrtc.focus();
			} else // has been defined
			    if (!winrtc.closed) {     // still open
			    	winrtc.close();
			        winrtc = window.open(url, "", "width=700,height=300,top=0,left=0,resizable=yes,scrollbars=yes,status=no");
					winrtc.focus();
				} else {
			        winrtc = window.open(url, "", "width=700,height=300,top=0,left=0,resizable=yes,scrollbars=yes,status=no");
			        winrtc.focus();
				}
		}
	}
}

function chkdelBlankLine(str)
{
	newstr = ""
	strary = str.split(";");
	for (var i=0; i < strary.length-1; i++) {
		sary = strary[i].split(",");
		if (sary[3]!="") {
			newstr += strary[i] + ";";
		}
	}
	return newstr;
}


function getGuest()
{	
	var url = ifrmMemberlist.location + "";
	if(url.indexOf('report2member.asp') != -1){
		willContinue = ifrmMemberlist.bfrRefresh(); 
		if (willContinue) {
			ifrmMemberlist.location.reload(); 
			url = "dispatcher/conferencedispatcher.asp?frm=group&cmd=GetGuestList&emailListPage=1&wintype=pop";
			if (!window.winrtc) {	// has not yet been defined
				winrtc = window.open(url, "", "width=700,height=300,top=0,left=0,resizable=yes,scrollbars=yes,status=no");
				winrtc.focus();
			} else // has been defined
			    if (!winrtc.closed) {     // still open
			    	winrtc.close();
			        winrtc = window.open(url, "", "width=700,height=300,top=0,left=0,resizable=yes,scrollbars=yes,status=no");
					winrtc.focus();
				} else {
			        winrtc = window.open(url, "", "width=700,height=300,top=0,left=0,resizable=yes,scrollbars=yes,status=no");
			        winrtc.focus();
				}
		}
	}
}


function delMember(pi_str)
{
	new_pi_str = "";

	pi_ary1 = pi_str.split(";");
	for (var i=0; i < pi_ary1.length - 1; i++) {
		pi_ary2 = pi_ary1[i].split(",");
		if (pi_ary2[4]!="3") {
			new_pi_str += pi_ary1[i] + ";";
		}
	}
	return (new_pi_str);
}


function getAGroupDetail(frm, cb, gid)
{
	if (cb == null) {
		if (gid != null) {
			id = gid;
		} else {
			alert("Sorry, system meets some error. Please notofy yoru administrator.")
			return false;
		}
	} else {

		if (cb.selectedIndex != -1) {
			if (gid != null) id = gid; else id = cb.options[cb.selectedIndex].value;
		} else {
			alert(EN_53);
			return false;
		}
	}
	
	gm = window.open("memberallstatus.asp?f=" + frm + "&n=" + id + "&wintype=pop", "groupmember", "status=no,width=420,height=300,scrollbars=yes,resizable=yes");
	if (gm) gm.focus(); else alert(EN_132);
}


function groupChg()
{
//alert(document.frmSettings2Report.PartysInfo.value)
	if (document.frmSettings2Report.UsersStr.value != "") {
		new_pi = "";
		var url = ifrmMemberlist.location + "";
		if(url.indexOf('report2member.asp') != -1){
//alert(document.frmSettings2Report.PartysInfo.value)
			willContinue = ifrmMemberlist.bfrRefresh(); 
//alert(document.frmSettings2Report.PartysInfo.value)
			if (willContinue) {
				partysinfo = document.frmSettings2Report.PartysInfo.value
//alert("1:" + partysinfo);
				tmp_pi = delMember (partysinfo);
//alert("2:" + tmp_pi);

				usersinfo = document.frmSettings2Report.UsersStr.value;
				usersary = usersinfo.split("|");
				pi_ary1 = tmp_pi.split(";");
				k = 0; pi_ary2 = pi_ary1[k].split(",");
//alert("pi_ary2:" + pi_ary2 + " pi_ary2[4]=" + pi_ary2[4]);

				while ( (pi_ary2[4]=="0") && (k < pi_ary1.length-1) ) {
					new_pi += pi_ary1[k] + ";";
					k++;
					pi_ary2 = pi_ary1[k].split(",");
				}
//alert("pi_ary2:" + pi_ary2);

				for (var i=0; i < usersary.length; i++) {
					if (document.frmSettings2Report.Group.options[i].selected) {
						partysary = usersary[i].split(";");
						for (var j=0; j < partysary.length-1; j++) {
							partyary = partysary[j].split(",");
							if ( (tmp_pi.indexOf("," + partyary[3] + ",") == -1) && (new_pi.indexOf("," + partyary[3] + ",") == -1) ) {
								new_pi += partyary[0] + "," + partyary[1] + "," + partyary[2] + "," + partyary[3] + ",3;";
							}
						}
					}
				}

//alert("2:" + new_pi);
				while (k < pi_ary1.length-1) {
					new_pi += pi_ary1[k] + ";";
					k++;
					pi_ary2 = pi_ary1[k].split(",");
				}
	
//alert("2:" + new_pi);
				document.frmSettings2Report.PartysInfo.value = new_pi;
				ifrmMemberlist.location.reload(); 
			}
		}
	}
}


function deleteAllDepartment()
{
	if (document.frmSettings2Report.DeptsInfo.value != "") {
		var isRemove = confirm("Are you sure you want to remove all departments?")
		if (isRemove == false) {
			return (false);
		}
	}
	
	document.frmSettings2Report.DeptsInfo.value = "";
	var url = ifrmDeptlist.location + "";
	if(url.indexOf('report2dept.asp') != -1){
		ifrmDeptlist.location.reload(); 
	}
}

function getYourOwnDepartmentList()
{
	var url = ifrmDeptlist.location + "";
	if(url.indexOf('report2dept.asp') != -1){
		willContinue = ifrmDeptlist.bfrRefresh(); 
		if (willContinue) {
			ifrmDeptlist.location.reload(); 
			url = "dispatcher/admindispatcher.asp?frm=report&cmd=GetManageDepartment&wintype=pop";
			if (!window.winrtc) {	// has not yet been defined
				winrtc = window.open(url, "", "width=700,height=300,top=0,left=0,resizable=yes,scrollbars=yes,status=no");
				winrtc.focus();
			} else // has been defined
			    if (!winrtc.closed) {     // still open
			    	winrtc.close();
			        winrtc = window.open(url, "", "width=700,height=300,top=0,left=0,resizable=yes,scrollbars=yes,status=no");
					winrtc.focus();
				} else {
			        winrtc = window.open(url, "", "width=700,height=300,top=0,left=0,resizable=yes,scrollbars=yes,status=no");
			        winrtc.focus();
				}
		}
	}
}

//end point for reports
function deleteAllEndpoint()
{
	if (document.frmSettings2Report.EndpointsInfo.value != "") {
		var isRemove = confirm("Are you sure you want to remove all endpoints?")
		if (isRemove == false) {
			return (false);
		}
	}
	
	document.frmSettings2Report.EndpointsInfo.value = "";
	var url = ifrmEndpointlist.location + "";
	if(url.indexOf('report2endpoint.asp') != -1){
		ifrmEndpointlist.location.reload(); 
	}
}

function getYourOwnEndpointList()
{
	var url = ifrmEndpointlist.location + "";
	if(url.indexOf('report2endpoint.asp') != -1){
		willContinue = ifrmEndpointlist.bfrRefresh(); 
		if (willContinue) {
			ifrmEndpointlist.location.reload(); 
			url = "dispatcher/admindispatcher.asp?frm=report&cmd=GetEndpointList&wintype=pop";
			if (!window.winrtc) {	// has not yet been defined
				winrtc = window.open(url, "", "width=700,height=300,top=0,left=0,resizable=yes,scrollbars=yes,status=no");
				winrtc.focus();
			} else // has been defined
			    if (!winrtc.closed) {     // still open
			    	winrtc.close();
			        winrtc = window.open(url, "", "width=700,height=300,top=0,left=0,resizable=yes,scrollbars=yes,status=no");
					winrtc.focus();
				} else {
			        winrtc = window.open(url, "", "width=700,height=300,top=0,left=0,resizable=yes,scrollbars=yes,status=no");
			        winrtc.focus();
				}
		}
	}
}

//MCUs for reports
function deleteAllMCU()
{
	if (document.frmSettings2Report.MCUsInfo.value != "") {
		var isRemove = confirm("Are you sure you want to remove all MCUs?")
		if (isRemove == false) {
			return (false);
		}
	}
	
	document.frmSettings2Report.MCUsInfo.value = "";
	var url = ifrmMCUlist.location + "";
	if(url.indexOf('report2mcu.asp') != -1){
		ifrmMCUlist.location.reload(); 
	}
}


function getYourOwnMCUList()
{
	var url = ifrmMCUlist.location + "";
	if(url.indexOf('report2mcu.asp') != -1){
		willContinue = ifrmMCUlist.bfrRefresh(); 
		if (willContinue) {
			ifrmMCUlist.location.reload(); 
			url = "dispatcher/admindispatcher.asp?frm=report&cmd=GetBridgeList&wintype=pop";
			if (!window.winrtc) {	// has not yet been defined
				winrtc = window.open(url, "", "width=700,height=300,top=0,left=0,resizable=yes,scrollbars=yes,status=no");
				winrtc.focus();
			} else // has been defined
			    if (!winrtc.closed) {     // still open
			    	winrtc.close();
			        winrtc = window.open(url, "", "width=700,height=300,top=0,left=0,resizable=yes,scrollbars=yes,status=no");
					winrtc.focus();
				} else {
			        winrtc = window.open(url, "", "width=700,height=300,top=0,left=0,resizable=yes,scrollbars=yes,status=no");
			        winrtc.focus();
				}
		}
	}
}

//Rooms for reports
function deleteAllRoom()
{
	if (document.frmSettings2Report.RoomsInfo.value != "") {
		var isRemove = confirm("Are you sure you want to remove all Rooms?")
		if (isRemove == false) {
			return (false);
		}
	}
	
	document.frmSettings2Report.RoomsInfo.value = "";
	var url = ifrmRoomlist.location + "";
	if(url.indexOf('report2room.asp') != -1){
		ifrmRoomlist.location.reload(); 
	}
}

function getYourOwnRoomList()
{
	var url = ifrmRoomlist.location + "";
	if(url.indexOf('report2room.asp') != -1){
		willContinue = ifrmRoomlist.bfrRefresh(); 
		if (willContinue) {
			ifrmRoomlist.location.reload(); 
			url = "dispatcher/admindispatcher.asp?frm=report&to=1&cmd=ManageConfRoom&wintype=pop";
			if (!window.winrtc) {	// has not yet been defined
				winrtc = window.open(url, "", "width=700,height=300,top=0,left=0,resizable=yes,scrollbars=yes,status=no");
				winrtc.focus();
			} else // has been defined
			    if (!winrtc.closed) {     // still open
			    	winrtc.close();
			        winrtc = window.open(url, "", "width=700,height=300,top=0,left=0,resizable=yes,scrollbars=yes,status=no");
					winrtc.focus();
				} else {
			        winrtc = window.open(url, "", "width=700,height=300,top=0,left=0,resizable=yes,scrollbars=yes,status=no");
			        winrtc.focus();
				}
		}
	}
}

function deleteAllTier2()
{
	if (document.frmSettings2Report.Tier2sInfo.value != "") {
		var isRemove = confirm("Are you sure you want to remove all Tier2s?")
		if (isRemove == false) {
			return (false);
		}
	}
	
	document.frmSettings2Report.Tier2sInfo.value = "";
	var url = ifrmTier2list.location + "";
	if(url.indexOf('report2tier2.asp') != -1){
		ifrmTier2list.location.reload(); 
	}
}


function getYourOwnTier2List()
{
	var url = ifrmTier2list.location + "";
	if(url.indexOf('report2tier2.asp') != -1){
		willContinue = ifrmTier2list.bfrRefresh(); 
		if (willContinue) {
			ifrmTier2list.location.reload(); 
			url = "dispatcher/admindispatcher.asp?frm=report&to=2&cmd=ManageConfRoom&wintype=pop";
			if (!window.winrtc) {	// has not yet been defined
				winrtc = window.open(url, "", "width=700,height=300,top=0,left=0,resizable=yes,scrollbars=yes,status=no");
				winrtc.focus();
			} else // has been defined
			    if (!winrtc.closed) {     // still open
			    	winrtc.close();
			        winrtc = window.open(url, "", "width=700,height=300,top=0,left=0,resizable=yes,scrollbars=yes,status=no");
					winrtc.focus();
				} else {
			        winrtc = window.open(url, "", "width=700,height=300,top=0,left=0,resizable=yes,scrollbars=yes,status=no");
			        winrtc.focus();
				}
		}
	}
}

function deleteAllTier1()
{
	if (document.frmSettings2Report.Tier1sInfo.value != "") {
		var isRemove = confirm("Are you sure you want to remove all Tier1s?")
		if (isRemove == false) {
			return (false);
		}
	}
	
	document.frmSettings2Report.Tier1sInfo.value = "";
	var url = ifrmTier1list.location + "";
	if(url.indexOf('report2tier1.asp') != -1){
		ifrmTier1list.location.reload(); 
	}
}


function getYourOwnTier1List()
{
	var url = ifrmTier1list.location + "";
	if(url.indexOf('report2tier1.asp') != -1){
		willContinue = ifrmTier1list.bfrRefresh(); 
		if (willContinue) {
			ifrmTier1list.location.reload(); 
			url = "dispatcher/admindispatcher.asp?frm=report&cmd=GetLocations&wintype=pop";
			if (!window.winrtc) {	// has not yet been defined
				winrtc = window.open(url, "", "width=700,height=300,top=0,left=0,resizable=yes,scrollbars=yes,status=no");
				winrtc.focus();
			} else // has been defined
			    if (!winrtc.closed) {     // still open
			    	winrtc.close();
			        winrtc = window.open(url, "", "width=700,height=300,top=0,left=0,resizable=yes,scrollbars=yes,status=no");
					winrtc.focus();
				} else {
			        winrtc = window.open(url, "", "width=700,height=300,top=0,left=0,resizable=yes,scrollbars=yes,status=no");
			        winrtc.focus();
				}
		}
	}
}
