/*ZD 100147 Start*/
/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
/*ZD 100147 End*/
function show_custom_recur_prompt(promptpicture, prompttitle) 
{
	promptbox = document.createElement('div'); 
	promptbox.setAttribute ('id' , 'prompt');
	document.getElementsByTagName('body')[0].appendChild(promptbox);
	promptbox = eval("document.getElementById('prompt').style");

	promptbox.position = 'absolute';
	w = 500;
	document.getElementById('prompt').style.top = mousedownY - 25; 
	document.getElementById('prompt').style.left = mousedownX - w;
	promptbox.width = w; 
	promptbox.border = 'outset 1 #bbbbbb' 
	
	style1 = "border-bottom: 1px solid #AAA;border-right: 1px solid #AAA;";
	style2 = "border-bottom: 1px solid #AAA;";
	
	m = "<table cellspacing='0' cellpadding='0' border='0' width='100%'><tr valign='middle'><td width='22' height='22' style='text-indent:2;' class='titlebar'><img src='" + promptpicture + "' height='18' width='18'></td><td class='titlebar'>" + prompttitle + "</td></tr></table>" 
	m += "<table cellspacing='0' cellpadding='0' border='0' width='100%' class='promptbox'>";
	m += "  <tr>";
	m += "    <td colspan='5'><span style='font-size: 8pt; font-weight: bold; color: #990033'>Because outlook does not support custom date for recurring conference, you have to save each instance in your outlook. Please notice that each instance in outlook is a independent conference.</style></td>";
	m += "  </tr>"
	m += "  <tr>";
	m += "    <td colspan='5' height='8'></td>";
	m += "  </tr>"
	m += "  <tr>";
	m += "    <td style='" + style1 + "'><span class='tableblackblodtext'>Instance Name</span></td>";
	m += "    <td style='" + style1 + "'><span class='tableblackblodtext'>Date</span></td>";
	m += "    <td style='" + style1 + "'><span class='tableblackblodtext'>Time</span></td>";
	m += "    <td style='" + style1 + "'><span class='tableblackblodtext'>Duration</span></td>";
	m += "    <td style='" + style2 + " text-align:center'><span class='tableblackblodtext'>Save</span></td>";
	m += "  </tr>"
	
	instanceary = instanceInfo.split("^^")
	for (i = 0; i < instanceary.length - 1; i++) {
		instinfo = instanceary[i];
		instanceary[i] = instanceary[i].split("``")

		m += "  <tr>";
		m += "    <td style='" + style1 + "'>" + instanceary[i][1] + "</td>";
		m += "    <td style='" + style1 + "'>" + instanceary[i][2] + "</td>";
		m += "    <td style='" + style1 + "'>" + instanceary[i][3] + ":" + instanceary[i][4] + " " + instanceary[i][5] + "</td>";
		m += "    <td style='" + style1 + "'>" + instanceary[i][7] + " min(s)</td>";
		m += "    <td style='" + style2 + " text-align:center'><img border='0' src='image/outlookcalendar.gif' width='14' height='14' alt='Create instance in your personal MS Outlook' onclick='JavaScriptScript: SaveInstance(\"" + instinfo + "\");'></td>";
		m += "  </tr>"
	}
	
	
	m += "  <tr>";
	m += "    <td colspan='5' height='5'></td>";
	m += "  </tr>"
	m += "  <tr>";
	m += "   <td colspan='5' align='right'>"
	m += "    <input type='button' class='prompt' value='Close' onMouseOver='this.style.border=\"1 outset transparent\"' onMouseOut='this.style.border=\"1 solid transparent\"' onClick='closethis();'>"
	m += "  </td></tr>"
	m += "</table>" 
	
	document.getElementById('prompt').innerHTML = m;
} 


function SaveInstance(instInfo) 
{
	saveToOutlookCalendar (0, 0, 3, instInfo);
} 


function closethis()
{
	document.getElementsByTagName("body")[0].removeChild(document.getElementById("prompt"));
}
