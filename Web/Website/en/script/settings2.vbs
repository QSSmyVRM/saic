'/*ZD 100147 Start*/
'/* Copyright (C) 2013 myVRM - All Rights Reserved
'* You may not use, distribute and/or modify this code under the
'* terms of the myVRM license.
'*
'* You should have received a copy of the myVRM license with
'* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
'*/
'/*ZD 100147 End*/
'******************************************************************************
' getOutlookDailyAppointment
'
'	Pull all the appointments out of the local Outlook calendar and 
'	show in a pop up window using chart
'******************************************************************************
Function getOutlookDailyAppointment(immediate, confDuration)
	Dim winocc
	
	msgbox "Please make sure Outlook is correctly installed and opened." & Chr(10) & "Click OK to continue and click Yes if asking ActiveX interaction." & Chr(10) & "If nothing happens, please check IE setting or contact administrator.", vbExclamation, "Outlook Warning"

	if (confDuration <= 0) then
		msgbox "Please enter a Conference Duration.", vbOK, "Error"
		exit function
	end if

	if (immediate) then
		confDate = CDate(Date())
'		t = ""
'		e = ""
'		msgBox(Now())
		t = Now()
		e = DateAdd("n", cInt(confDuration), t)
	else
		confDate = document.frmSettings2.ConferenceDate.value
		if Not(IsDate(confDate)) then
			msgBox "Please check the Conference Date and reenter.", vbOK, "Error"
			exit function
		end if
		
		if DateDiff("d", Now, confDate) < 0 then
			msgBox "Please enter a future Conference Date.", vbOK, "Error"
			exit function
		end if

		if (document.frmSettings2.ConferenceDate.value <> "") then
			confDate = CDate(document.frmSettings2.ConferenceDate.value)
	'		msgbox("confDate=" & confDate)
		end if

		confStartHr = cInt(document.frmSettings2.ConferenceTimehr.value)
		confStartMi = cInt(document.frmSettings2.ConferenceTimemi.value)

		if (document.frmSettings2.ConferenceTimeap.selectedIndex = 0) then
			confStartAP = "AM"
		else
			confStartAP = "PM"
		end if
	
		confTime = confStartHr & ":" & confStartMi & " " & confStartAP
'		msgbox("confTime=" & confTime)

		t = confDate  & " " & confTime
		e = DateAdd("n", cInt(confDuration), t)
	end if


	appoints = GetAppointmentsFor(confDate)
	if appoints <> "-1" then
		url = "outlookconferencechart.asp?wintype=pop&t=" & t & "&e=" & e & "&n=" & document.frmSettings2.ConferenceName.value & "&s=" & appoints
'		msgbox("url=" & url)
		Set winocc = window.open (url, "OutlookConferenceChart", "width=850, height=310, top=40, left=20, resizable=yes, toolbar=no, menubar=no, location=no, directories=no, scrollbars=yes, status=no")
		If winocc Is Nothing Then
			msgbox(EN_132)
		Else
			winocc.focus()
		End If
	end if
End Function



'******************************************************************************
' getOutLookEmailList
'
'	Pull all the email address out of the local Outlook address book and 
'	show in a pop up window
'******************************************************************************
Function getOutLookEmailList()
	msgbox "Please make sure Outlook is correctly installed and opened." & Chr(10) & "Click OK to continue and click Yes if asking ActiveX interaction." & Chr(10) & "If nothing happens, please check IE setting or contact administrator.", vbExclamation, "Outlook Warning"
    'FB 412
	'if ( (instr(1,ifrmPartylist.location,"settings2party.asp") <> 0) or (instr(1,ifrmPartylist.location,"settings2partyNET.asp") <> 0) ) then
	'Code Changed for FB 412 -Start
	if ( (instr(1,ifrmPartylist.location,"settings2party.aspx") <> 0) or (instr(1,ifrmPartylist.location,"settings2partyNET.aspx") <> 0) ) then	'FB412 ends
	'Code Changed for FB 412 -End
		willContinue = ifrmPartylist.bfrRefresh()
		if (willContinue) then
			ifrmPartylist.location.reload()
			
			outlookcontacts = ContactsAsXML()
			if outlookcontacts <> "-1" then
				document.frmSettings2.outlookEmails.value = outlookcontacts
				document.frmSettings2.lotusEmails.value = ""
				'Code Modified for FB 412- Start -Outlook look up program
	            'url = "preoutlookemaillist2.asp?frm=party2NET&page=1&wintype=pop"				
	            url = "preoutlookemaillist2.aspx?frm=party2NET&page=1&wintype=pop"
	            'Code Modified for FB 412- End - Outlook look up program
                window.open url, "EmailList", "width=700, height=450, resizable=yes, toolbar=no, menubar=no, location=no, directories=no, scrollbars=yes"
			end if
		end if
	end if
End Function



'******************************************************************************
' getLotusEmailList
'
'	Pull all the email address out of the local Outlook address book and 
'	show in a pop up window
'******************************************************************************
Function getLotusEmailList(loginName, loginPwd, dbPath)
    'Code Changed for FB 412 start
    '	if instr(1,ifrmPartylist.location,"settings2partyNET.asp") <> 0 then
        if instr(1,ifrmPartylist.location,"settings2partyNET.aspx") <> 0 then
    'Code Changed for FB 412 End	
		willContinue = ifrmPartylist.bfrRefresh()
		if (willContinue) then
			ifrmPartylist.location.reload()
			document.frmSettings2.outlookEmails.value = ""
			document.frmSettings2.lotusEmails.value = "<lotus><user><userName><userFirstName>saima</userFirstName><userLastName>ahmad</userLastName></userName><userEmail>saima@lotusemail.com</userEmail></user></lotus>" 
			'lnContactsAsXML(loginName, loginPwd, dbPath)
			url = "prelotusemaillist2.asp?frm=party2&page=1&wintype=pop"
			window.open url, "EmailList", "width=700, height=450, resizable=yes, toolbar=no, menubar=no, location=no, directories=no, scrollbars=yes"
		end if
	end if
End Function