/*ZD 100147 Start*/
/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
/*ZD 100147 End*/
//var msgTimerRunning = false;
//var msgTimer = null;


function overuser (cb, wid)
{
	cb.className = "seltxthov";
}


function outuser (cb, wid)
{
	cb.className = "seltxt";
}

function inviteusr(uid, un)
{
	createtalk (uid, un, "");
//	alert("1. open window for input text 2. in that window send msg and for submit")
//	alert("2. after 1 submit, change timer from 5 sec to .5 sec to get info.")
}


function show_imuser(promptpicture, prompttitle, userstr) 
{
	if (document.getElementById("imuserprompt") == null) {
		promptbox = document.createElement('div'); 
		promptbox.setAttribute ('id' , 'imuserprompt');
		document.getElementsByTagName('body')[0].appendChild(promptbox);
		promptbox = eval("document.getElementById('imuserprompt').style");
	
		promptbox.position = 'absolute';
		w = 150;
		document.getElementById('imuserprompt').style.top = MouseY + 10; 
		document.getElementById('imuserprompt').style.left = MouseX - w;
		document.getElementById('imuserprompt').style.zIndex = 999;
	
		promptbox.width = w; 
		promptbox.border = 'outset 1 #bbbbbb' 
	}

	users = userstr.split("%%")
		
	m = "<table cellspacing='0' cellpadding='0' border='0' width='100%' class='promptbox'>";
	m += "  <tr valign='middle'>"
	m += "    <td height='22' style='text-indent:2;' class='titlebar' align='left'>"
	m += "      <img src='" + promptpicture + "' height='18' width='18'>&nbsp;&nbsp;" + prompttitle
	m += "    </td>"
	m += "    <td class='titlebar' align='right' valign='bottom'>"
	m += "      <img src='image/closeprompt.gif' height='18' width='17' onclick='closethis();'></img>"
	m += "    </td>"
	m += "  </tr>"
	m += "  <tr>";
	m += "    <td colspan='2'><span style='font-size: 8pt; font-weight: bold; color: #990033'>Click the person to invite in SIM</style></td>";
	m += "  </tr>"
	m += "  <tr>";
	m += "    <td colspan='2'>"
	if (userstr == "") {
		m += "      <div class='scroll' style='height: 80'>"
		m += "<br>Currently there is<br> no user login SIM."	
	} else {
		m += "      <div class='scroll'>"
		for (i = 0; i < users.length-1; i++) {
			user = users[i].split("!!");// style='width: 100%; height: 20px; overflow: hidden' 
			m += "<div id='imusr" + i + "' class='seltxt' style='width: " + (w-42) + "px; overflow: hidden;' onmouseover='overuser(this);' onmouseout='outuser(this);'><a onclick='Javascript: inviteusr(\"" + user[0] + "\", \"" + user[1] + "\");'>" + user[1] + "</a></div>"
		}
	}
	m += "      </div>"
	m += "    </td>";
	m += "  </tr>"
	m += "</table>" 

	document.getElementById('imuserprompt').innerHTML = m;
} 


function closethis()
{
	if (document.getElementById("imuserprompt"))
		document.getElementsByTagName("body")[0].removeChild(document.getElementById("imuserprompt"));
}

