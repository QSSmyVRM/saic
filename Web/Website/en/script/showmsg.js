/*ZD 100147 Start*/
/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
/*ZD 100147 End*/
//var msgTimerRunning = false;
//var msgTimer = null;

function show_message_prompt(promptpicture, prompttitle, msg1, msg2, msg3, msg4) 
{
//	if (msgTimerRunning) {
//		closethis();
//		clearTimeout(msgTimer);
//		msgTimerRunning = false;
//	}
	
	
	promptbox = document.createElement('div'); 
	promptbox.setAttribute ('id' , 'prompt');
	document.getElementsByTagName('body')[0].appendChild(promptbox);
	promptbox = eval("document.getElementById('prompt').style");
	
	getMouseXY();
	
	promptbox.position = 'absolute';
	w = 255;
	document.getElementById('prompt').style.top = mousedownY; 
	document.getElementById('prompt').style.left = mousedownX;
	document.getElementById('prompt').style.zIndex = 5;
	
	promptbox.width = w; 
	promptbox.border = 'outset 1 #bbbbbb' 
	
	style1 = "border-bottom: 1px solid #AAA;border-right: 1px solid #AAA;";
	style2 = "border-bottom: 1px solid #AAA;";
	
	dhour = parseInt(parseInt(msg3, 10) / 60, 10)
	dmin = parseInt(msg3, 10) - dhour * 60
		
	m = "<table cellspacing='0' cellpadding='0' border='0' width='100%' class='promptbox' bgColor = '#ccccff'>";//FB 1048
	m += "  <tr valign='middle'>"
	m += "    <td width='100%' height='22' style='text-indent:2;' class='titlebar' align='left' colspan='2' bgColor = '#9999ff'>" //FB 1048
	m += "      <img src='" + promptpicture + "' height='18' width='18'>&nbsp;&nbsp;" + prompttitle
	m += "    </td>"
	m += "  </tr>"
	m += "  <tr>";
	m += "    <td valign='top'><span style='font-size: 8pt; font-weight: bold; color: #990033'>Name: </style></td>";
	m += "    <td><span style='font-size: 8pt; font-weight: bold; color: #990033'>" + msg1 + "</style></td>";
	m += "  </tr>"
//	m += "  <tr>";
//	m += "    <td><span style='font-size: 8pt; font-weight: bold; color: #990033'>Start: </style></td>";
//	m += "    <td><span style='font-size: 8pt; font-weight: bold; color: #990033'>" + msg2 + "</style></td>";
//	m += "  </tr>"
	m += "  <tr>";
	m += "    <td><span style='font-size: 8pt; font-weight: bold; color: #990033'>Duration: </style></td>";
	m += "    <td><span style='font-size: 8pt; font-weight: bold; color: #990033'>" + dhour + " hr(s) " + dmin + " min(s)</style></td>";
	m += "  </tr>"
	m += "  <tr>";
	m += "    <td valign='top'><span style='font-size: 8pt; font-weight: bold; color: #990033'>Location: </style></td>";
	m += "    <td><span style='font-size: 8pt; font-weight: bold; color: #990033'>" + ((msg4 == "") ? "N/A" : msg4) + "</style></td>";
	m += "  </tr>"
	m += "</table>" 

	document.getElementById('prompt').innerHTML = m;
	
//	msgTimer = setTimeout('closethis()', 2500);
//	msgTimerRunning = true;
	
//alert(document.getElementById('prompt').style.top)
//alert(document.getElementById('prompt').style.left)
} 


/* FB Issue - 1170 Buffer Zone Start */

function show_BufferZone_prompt(promptpicture, prompttitle, msg1, msg2, msg3, msg4, msg5, msg6, showBuffer) 
{
//	if (msgTimerRunning) {
//		closethis();
//		clearTimeout(msgTimer);
//		msgTimerRunning = false;
//	}
	
	promptbox = document.createElement('div'); 
	promptbox.setAttribute ('id' , 'prompt');
	document.getElementsByTagName('body')[0].appendChild(promptbox);
	promptbox = eval("document.getElementById('prompt').style");
	
	getMouseXY();
	
	promptbox.position = 'absolute';
	w = 255;
	document.getElementById('prompt').style.top = mousedownY; 
	document.getElementById('prompt').style.left = mousedownX;
	document.getElementById('prompt').style.zIndex = 5;
	
	promptbox.width = w; 
	promptbox.border = 'outset 1 #bbbbbb' 
	
	style1 = "border-bottom: 1px solid #AAA;border-right: 1px solid #AAA;";
	style2 = "border-bottom: 1px solid #AAA;";
	
	dhour = parseInt(parseInt(msg3, 10) / 60, 10)
	dmin = parseInt(msg3, 10) - dhour * 60
	
	m = "<table cellspacing='0' cellpadding='0' border='0' width='100%' class='promptbox' bgColor = '#FFBBFF'>";//FB 1048
	m += "  <tr valign='middle'>"
	m += "    <td width='100%' height='22' style='text-indent:2;' class='titlebar' align='left' colspan='2' bgColor = '#EE00EE'>" //FB 1048
	m += "      <img src='" + promptpicture + "' height='18' width='18'>&nbsp;&nbsp;" + prompttitle
	m += "    </td>"
	m += "  </tr>"
	m += "  <tr>";
	m += "    <td valign='top'><span style='font-size: 8pt; font-weight: bold; color: #990033'>Name: </style></td>";
	m += "    <td><span style='font-size: 8pt; font-weight: bold; color: #990033'>" + msg1 + "</style></td>";
	m += "  </tr>"
//	m += "  <tr>";
//	m += "    <td><span style='font-size: 8pt; font-weight: bold; color: #990033'>Start: </style></td>";
//	m += "    <td><span style='font-size: 8pt; font-weight: bold; color: #990033'>" + msg2 + "</style></td>";
//	m += "  </tr>"
	m += "  <tr>";
	m += "    <td><span style='font-size: 8pt; font-weight: bold; color: #990033'>Duration: </style></td>";
	m += "    <td><span style='font-size: 8pt; font-weight: bold; color: #990033'>" + dhour + " hr(s) " + dmin + " min(s)</style></td>";
	m += "  </tr>"
	m += "  <tr>";
	m += "    <td valign='top'><span style='font-size: 8pt; font-weight: bold; color: #990033'>Location: </style></td>";
	m += "    <td><span style='font-size: 8pt; font-weight: bold; color: #990033'>" + ((msg4 == "") ? "N/A" : msg4) + "</style></td>";
	m += "  </tr>"
	if(showBuffer == "Y")  //buffer zone is hided for participants
	{
	    m += "  <tr>";
	    m += "    <td valign='top'><span style='font-size: 8pt; font-weight: bold; color: #008000'>Setup Time: </style></td>";
	    m += "    <td><span style='font-size: 8pt; font-weight: bold; color: #008000'>" + ((msg5 == "") ? "N/A" : msg5) + " min(s)</style></td>";
	    m += "  </tr>"
	    m += "  <tr>";
	    m += "    <td valign='top' nowrap ><span style='font-size: 8pt; font-weight: bold; color: #008000'>Teardown Time: </style></td>";
	    m += "    <td><span style='font-size: 8pt; font-weight: bold; color: #008000'>" + ((msg6 == "") ? "N/A" : msg6) + " min(s)</style></td>";
	    m += "  </tr>"
	 }
	
	m += "</table>" 

	document.getElementById('prompt').innerHTML = m;
	
//	msgTimer = setTimeout('closethis()', 2500);
//	msgTimerRunning = true;
	
//alert(document.getElementById('prompt').style.top)
//alert(document.getElementById('prompt').style.left)
}

function closethis()
{
	if (document.getElementById("prompt"))
		document.getElementsByTagName("body")[0].removeChild(document.getElementById("prompt"));
}


function move_message_prompt()
{
	if (document.getElementById("prompt")) {
		getMouseXY();
		document.getElementById('prompt').style.top = mousedownY; 
		document.getElementById('prompt').style.left = mousedownX;
	}
}