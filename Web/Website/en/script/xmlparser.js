/*ZD 100147 Start*/
/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
/*ZD 100147 End*/
function initJSXMLParser (strXML)
{
	var xmlDocument
	
	var isIE = navigator.userAgent.toLowerCase().indexOf("msie") > -1;
	var isMoz = document.implementation && document.implementation.createDocument;

	if (isIE) {
		var ARR_ACTIVEX = ["MSXML4.DOMDocument", "MSXML3.DOMDocument", "MSXML2.DOMDocument", "MSXML.DOMDocument", "Microsoft.XmlDom"];
		var bFound = false;
		for (var i=0; i < ARR_ACTIVEX.length && !bFound; i++) {
		    try {
		        xmlDocument = new ActiveXObject(ARR_ACTIVEX[i]);
		        bFound = true
		    } catch (objException) { 
		    }
		}
		
		if (!bFound)
			throw "No DOM DOcument found on your computer."
		else {
			var loaded = xmlDocument.loadXML(strXML);
			if (!loaded) {
				alert(xmlDocument.parseError.reason + xmlDocument.parseError.srcText);
			}
		}     
	}

	if (isMoz) {
		var objDOMParser = new DOMParser();
alert("strXML=" + strXML)
		var xmlDocument = objDOMParser.parseFromString(strXML, "text/xml");
		        
		var parseError = checkForParseError(xmlDocument);
alert("parseError.errorCode=" + parseError.errorCode)
		if (parseError.errorCode != 0) {
			alert(parseError.reason + '\r\n' + parseError.srcText);
		}
	}
	
	return xmlDocument;
}

function checkForParseError (xmlDocument) {
	var errorNamespace = 'http://www.mozilla.org/newlayout/xml/parsererror.xml';
	var documentElement = xmlDocument.documentElement;
	var parseError = { errorCode : 0 };
	
	if (documentElement.nodeName == 'parsererror' && documentElement.namespaceURI == errorNamespace) {
		parseError.errorCode = 1;
		var sourceText = documentElement.getElementsByTagNameNS(errorNamespace, 'sourcetext')[0];
		if (sourceText != null) {
			parseError.srcText = sourceText.firstChild.data
		}
		parseError.reason = documentElement.firstChild.data;
	}
	return parseError;
}

