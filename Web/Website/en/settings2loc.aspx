<%--ZD 100147 start--%>
<%--/* Copyright (C) 2013 myVRM - All Rights Reserved	
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.		
*
* You should have received a copy of the myVRM license with	
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/--%><%--ZD 100147 End--%>
<%@ Page Language="C#" AutoEventWireup="true" Inherits="en_settings2loc" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
  <head>
    <link rel="StyleSheet" href="css/mytree.css" type="text/css" />
    <script type="text/javascript" src="script/wincheck.js"></script> <%--FB 3055-URL--%>
    <script type="text/javascript">        // FB 2790
        var path = '<%=Session["OrgCSSPath"]%>';
        path = path.split('.')[0] + '<%=Session["ThemeType"]%>' + ".css"; // FB 2815
        document.write('<link rel="stylesheet" title="Expedite base styles" type="text/css" href="' + path + '?' + new Date().getTime() + '">');
    </script>
    <script type="text/javascript" src="inc/functions.js"></script>
    <script type="text/javascript" src="script/errorList.js"></script>
    <script type="text/javascript" src="script/mytree.js"></script>
    <script type="text/javascript" src="script/loctree.js"></script>
    <script language="javascript1.1" src="extract.js"></script>

    <script type="text/javascript" language="javascript">

	if ( !eval("parent.document." + queryField("f")) ) {	
		setTimeout('window.location.reload();',500);
	} else {

		if ( queryField("f") == "frmManageroom" )
		{
			eval("parent.document." + queryField("f") + ".settings2locstr").value = "<%=strsettings2loc%>";
		}
		else
        {
            //alert(eval("parent.document." + queryField("f") + ".settings2locstr"));
			eval("parent.document." + queryField("f") + ".settings2locstr").value = "<%=strsettings2loc%>";}
	    }
	
    </script>
  </head>

  <body bottommargin="0" leftmargin="0" rightmargin="0" topmargin="0" marginheight="0" marginwidth="0" onUnload="childwinclose();" bgcolor="white">

    
    <form id="frmSettings2loc"  runat="server" method="post" >
        <table border=0 height=0>
      <input type="hidden" name="selectedloc" id="selectedloc" runat="server" />
      <input type="hidden" id="locidname" name="locidname" runat="server" />
      <input type="hidden" id="special" name="special" runat="server" />
      <input type="hidden" id="nonvideolocname" name="nonvideolocname" runat="server" />
      <input type="hidden" id="comparedlocs" name="comparedlocs" runat="server" />
      <input type="hidden" id="comparedsellocs" name="comparedsellocs" runat="server" />
      <input type="hidden" id="locstr" name="locstr" runat="server" />

      <!-- nonvideolocname used in scheduleroom.asp for future video conf (ASPX)-->
    </form>
    </table>
    
    <div class="loctree">
      <table width="100%" height=1 border="0" cellpadding="0" cellspacing="0">
        <form id="frmSettings2loc_compareloc">
          <tr>
            <td align="left">
<%
                if (paramMod != "2")
                {
%>
                    <font size=1><a href="javascript: loc.openAll();" onMouseOver="window.status=''; return true" onMouseOut="window.status=''; return true">expand</a> | <a href="javascript: loc.closeAll();" onMouseOver="window.status=''; return true" onMouseOut="window.status=''; return true">collapse</a></font>
<%
                }
                else
                {
%>
                    &nbsp;
<%
                }
%>
            </td>
            <td align="right" width="20" id="btnCheckAvailabilityDIV">

<%
            /* This block is used for earlier ASP-conference setup page - currently this is not used
             * this block is deffered
             */
            if((paramFVal == "frmSettings2") && (paramSp != "1"))
            {
%>
			    &nbsp;<input type="Button" name="Settings2locSubmit" value="Check Availability" class="altSmallButtonFormat" onClick="JavaScript: childwinclose(); viewselectedavailability();">
<%
            }
            else
            {
%>
               &nbsp;
<%
            }
%>

            </td>
            <td width="2"></td>
             <!--Code changed for calendar-->
              <!--<td align="justify">-->
              <td align="right" width="20">
			  <input type="Button" name="Settings2locSubmit" value="Compare" style="width:68px" class="altShortBlueButtonFormat" onClick="JavaScript: childwinclose(); compareselected();">
            </td>
          </tr>

        </form>
      </table>

      <form id='frmSettings2loctree' name="frmSettings2loctree">
      <script type="text/javascript">
        <!--
          
	  	  var str;

	  	  str = eval("parent.document." + queryField("f") + ".settings2locstr").value;
	
	  	  if (queryField("mod") == "2") {
	  		str = getRoomDisplayList (str)
	  	  }

			
	  	  if (str.indexOf("!") == -1 ) {
	  		setTimeout('window.location.reload();',500);
	  	  } else {
	  		if (str == "!")
	  			window.location.href = "settings2locfail.aspx?special=" + queryField("special") + "&f=" + queryField("f");
	  		else
	  			//added for FB 1428 Start
	  		{
	  			if('<%=Application["Client"].ToString().ToUpper()%>' == "MOJ")
	  			generateLocTree( str, false, queryField("mod") ,"Hearing Rooms");
	  			else
	  		//added for FB 1428 End
	  			generateLocTree( str, false, queryField("mod"),"Conference Rooms");//Edited for FB 1428
	  	    }//added for FB 1428
	  	  }
 	  	  
 	  	  cb = eval("parent.document." + queryField("f") + ".GetAvailableRoom") ;
 	  	  if (cb) 
 	  		if (cb.disabled) {
 	  			cb.disabled = false;
 	  			parent.window.status = "";
 	  		}

		  if (eval("parent.document." + queryField("f") + ".CreateBy")) {
		    if ( (eval("parent.document." + queryField("f") + ".CreateBy").value == "2") || (eval("parent.document." + queryField("f") + ".CreateBy").value == "7") ) {
				if (parent.document.getElementById("locstrid"))
					parent.document.getElementById("locstrid").innerHTML="";
					
				if (parent.document.getElementById("Settings2Submit1"))
					parent.document.getElementById("Settings2Submit1").disabled = false;
				if (parent.document.getElementById("Settingsusingconfroom2Submit2"))
					parent.document.getElementById("Settingsusingconfroom2Submit2").disabled = false;
				if (parent.document.getElementById("Settingsusingconfroom2Submit3"))
					parent.document.getElementById("Settingsusingconfroom2Submit3").disabled = false;
			}
		  }
		  
		   if (queryField("special") == "1")
		     loc.openAll();
		   else
	  	     loc.openAll();
	  	     // loc.closeAll();
	  	     
        //-->
      </script>
      
      </form>
    </div>

    <script type="text/javascript" language="javascript">
	<!--

	document.getElementById("btnCheckAvailabilityDIV").style.display = "none";
    
	if (queryField("comp") != "")
		document.frmSettings2loc.comparedsellocs.value= queryField("comp");

	if (queryField("cursel").indexOf(", ") != -1) 
		document.frmSettings2loc.selectedloc.value = queryField("cursel");

	if ( (parent.location.href).indexOf("calendar") != -1 ) {
		if (parent.chgRmCtr)
			parent.chgRmCtr();
		
		if (parent.updateRoomCheckable)
			parent.updateRoomCheckable();
	}
	  	     if ("<%= roomExpandLevel %>" == "1") //this should actually be 1
	  	        loc.closeAll();
	  	     else
	  	         if ("<%= roomExpandLevel %>" == "3")
	  	            loc.openAll();
	  	     else
	  	         if ("<%= roomExpandLevel %>" == "2") // this is actually 2
	  	         {
    	  			for (var n=0; n<loc.aNodes.length; n++)
    	  			{
    	  			    var str = loc.aNodes[n].level;
    	  			    if (str == "3")
    	  			        loc.closeAllChildren(loc.aNodes[n]);
	  	            }
	  	        }

	//-->
    </script>

  </body>
</html>
<%--code added for Soft Edge button--%>
<script type="text/javascript" src="inc/softedge.js"></script>