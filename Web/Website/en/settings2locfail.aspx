<%--ZD 100147 start--%>
<%--/* Copyright (C) 2013 myVRM - All Rights Reserved	
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.		
*
* You should have received a copy of the myVRM license with	
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/--%><%--ZD 100147 End--%>
<%@ Page Language="C#" AutoEventWireup="true"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>MyVRM</title>
</head>
<body bottommargin="10" leftmargin="15" rightmargin="3" topmargin="10" marginheight="0" marginwidth="0" bgcolor="white">
    <form id="frmSettings2loc">
      <input type="hidden" name="selectedloc" value="" runat ="server" />
      <input type="hidden" name="nonvideolocname" value=""  runat ="server" />
      <input type="hidden" name="locstr" value=""  runat ="server" />
      <input type="hidden" name="comparedsellocs" value=""  runat ="server" />
      <!-- nonvideolocname used in scheduleroom.asp for future video conf -->
    </form>

	 You have no Room(s) available.<br>
 </body>
  <script type="text/javascript" src="script/wincheck.js"></script> <%--FB 3055-URL--%>
  <script type="text/javascript" src="script/errorList.js"></script>
  <script language="javascript1.1" src="extract.js" type="text/javascript"></script>

	<script type="text/javascript">

		function uncheckall ()	// do not remove, need for food page.
		{
			return true;
		}


		if (queryField("f") != ""){
		  if (eval("parent.document." + queryField("f"))) {
				if (cb = eval("parent.document." + queryField("f") + ".GetAvailableRoom"))
					if (cb.disabled) {
						cb.disabled = false;
						parent.window.status = "";
					}
			
				if (eval("parent.document." + queryField("f") + ".CreateBy"))
				  if ( (eval("parent.document." + queryField("f") + ".CreateBy").value == "2") || (eval("parent.document." + queryField("f") + ".CreateBy").value == "7") ) {
						if (parent.document.getElementById("locstrid"))
							parent.document.getElementById("locstrid").innerHTML="<font size='2' color='red'><b>No conference room(s) available. Please contact your A/V Specialist or myVRM Administrator for further help.</b></font>";
						
						if (parent.document.getElementById("Settings2Submit1"))
							parent.document.getElementById("Settings2Submit1").disabled = true;
						
						if (parent.document.getElementById("Settingsusingconfroom2Submit2"))
							parent.document.getElementById("Settingsusingconfroom2Submit2").disabled = true;
						
						if (parent.document.getElementById("Settingsusingconfroom2Submit3"))
							parent.document.getElementById("Settingsusingconfroom2Submit3").disabled = true;	
					}
			}
		}

	</script>

</html>
