<%--ZD 100147 start--%>
<%--/* Copyright (C) 2013 myVRM - All Rights Reserved	
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.		
*
* You should have received a copy of the myVRM license with	
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/--%><%--ZD 100147 End--%>
<script type="text/javascript" src="script/wincheck.js"></script> <%--FB 3055-URL--%>
<script language="JavaScript" src="inc/functions.js"></script>
<link rel="stylesheet" type="text/css" href="css/fixedHeader.css"/>
<script type="text/javascript">        // FB 2790
    var path = '<%=Session["OrgCSSPath"]%>';
    path = path.split('.')[0] + '<%=Session["ThemeType"]%>' + ".css"; // FB 2815
    document.write('<link rel="stylesheet" title="Expedite base styles" type="text/css" href="' + path + '?' + new Date().getTime() + '">');
</script>

  <script type="text/javascript" src="script/errorList.js"></script>

<!-- JavaScript begin -->

<script language="JavaScript">
<!--

var	oldpartyno = 0;
var	newpartyno = 0;

var isRm = ( (parent.document.getElementById("CreateBy").value == "7") ? true : false ) ;
var isVD = ( (parent.document.getElementById("CreateBy").value == "2") || (parent.document.getElementById("CreateBy").value == "4") ? true : false ) ;
var isAD = ((parent.document.getElementById("CreateBy").value == "6") ? true : false);
//FB 2376 start
var isVMR = false;
if (parent.document.getElementById("isVMR"))
    if ((parent.document.getElementById("isVMR").value == "1"))
        isVMR = true; 
//FB 2376 end
    
var isFuCreate = (isAD || isVD); //true;
var isImFu = (isAD || isVD || isRm); // && isCreate);;
//alert(isRm + " : " + isVD + " : " + isAD + " : " + isFuCreate + " : " + isImFu);
/*
var isImFu = ( ( (parent.document.frmSettings2.CreateBy.value == "1") || (parent.document.frmSettings2.CreateBy.value == "3") || (parent.document.frmSettings2.CreateBy.value == "-1") ) ? true : false ) ;

if (parent.document.frmSettings2.ConfID)
	var isCreate = ( (parent.document.frmSettings2.ConfID.value == "new") ? true : false ) ;
else	// template
	var isCreate = true ;

var isFu = ( (parent.document.frmSettings2.CreateBy.value == "1") ? true : false ) ;
var isRm = ( ((parent.document.frmSettings2.CreateBy.value == "2") || (parent.document.frmSettings2.CreateBy.value == "7")) ? true : false ) ;

var isFuCreate = (isFu && isCreate)
var isRmCreate = (isRm && isCreate)
*/
function deleteBlankLine(linenum, str)
{	
	var newstr = "";
	
	strary = str.split("||");//FB 1888
	//alert(strary.length);
	for (var i=linenum; i < strary.length-1; i++) {
		newstr += strary[i] + "||";//FB 1888
	}
	return newstr;
}


function addBlankLine(linenum, str)
{
	var newstr = str;

	for (var i=0; i < linenum; i++) {
		//newstr = ( isRm ? ",,,,0,1,0,0,0,1,,,0,,,;" : ",,,,1,0,0,0,0,1,,,0,,,;") + newstr;
		if (isRm)
			newstr = "!!!!!!!!0!!1!!0!!1!!0!!1!!!!!!0!!!!!!0!!0!!0!!0||" + newstr;//FB 1888 //FB 2348 
			//newstr = ",,,,0,1,0,1,0,1,,,0,,,;" + newstr;
		if (isVD)
		    newstr = "!!!!!!!!1!!0!!0!!1!!0!!0!!!!!!0!!!!!!0!!0!!0!!0||" + newstr;//FB 1888 //FB 2348 
		    //newstr = ",,,,1,0,0,1,0,0,,,0,,,;" + newstr;
	    if (isVD)
		    newstr = "!!!!!!!!1!!0!!0!!1!!0!!1!!!!!!0!!!!!!0!!0!!0!!0||" + newstr;//FB 1888 //FB 2348 
		    //newstr = ",,,,1,0,0,1,0,1,,,0,,,;" + newstr;
	}
	return newstr;
}


function assemble(ary, endstr)
{
	var newstr = "";
	for (var i=0; i < ary.length-1; i++) {
		newstr += ary[i] + endstr;
	}
	newstr += ary[ary.length-1];
	return newstr;
}


function checkNewParty(fncb, lncb, emlcb)
{
	if ((Trim(fncb.value) == "[First Name]") && (Trim(lncb.value) == "[Last Name]") && (Trim(emlcb.value) == ""))
		return true;
	if (Trim(fncb.value) == "[First Name]") {
		alert(EN_4)
		fncb.focus();
		return false;
    }
	/*else{ //FB 1888
		if(checkInvalidChar(fncb.value) == false){
			return false;
		}
	}*/
	
	
	if (Trim(lncb.value) == "[Last Name]") {
		alert(EN_5)
		lncb.focus();
		return false;
	}
	/*else{ //FB 1888
		if(checkInvalidChar(lncb.value) == false){
			return false;
		}
	}*/
	
	if (Trim(emlcb.value) == "") {
		alert(EN_6)
		emlcb.focus();
		return false;
	}
	else{
		if(checkInvalidChar(emlcb.value) == false){
			return false;
		}
	}
	//alert(checkEmail(Trim(emlcb.value)));
	if (checkemail(Trim(emlcb.value)))
		return true;
	else {
		alert(EN_6);
		emlcb.focus();
		return false;
	}
}


function bfrRefresh()
{
    
	partysinfo = parent.document.getElementById("txtPartysInfo").value;	
	eno = -1;
	cornew = 0;
	partysinfo = deleteBlankLine(newpartyno, partysinfo);
    var pcParty = "0";
    var vmrParty = "0";
    var infoParty = "0";
//=== need add: check input valid.
//=== if above check is wrong, then return false cause two calling function break.
	for (i=0; i<newpartyno; i++) {//FB 1888 - Starts
		//alert(document.frmSettings2party.elements[eno+1].value + "," + document.frmSettings2party.elements[eno+2].value + "," + document.frmSettings2party.elements[eno+3].value);
		willContinue = checkNewParty(document.frmSettings2party.elements[eno+1], document.frmSettings2party.elements[eno+2], document.frmSettings2party.elements[eno+3]);
		if (!willContinue)
			return false;
		infoParty = "";
		pcParty = "0";
		vmrParty = "0";
		if ( (document.frmSettings2party.elements[eno+3].value != "") && (partysinfo.indexOf("!!" + document.frmSettings2party.elements[eno+3].value + "!!") == -1) ) {
			infoParty = "new!!" + document.frmSettings2party.elements[++eno].value + "!!" + document.frmSettings2party.elements[++eno].value + "!!" +  
							document.frmSettings2party.elements[++eno].value + "!!" +

							( document.frmSettings2party.elements[++eno].checked ? "1" : "0") + "!!" +  // : (isRm ? "0" : (document.frmSettings2party.elements[++eno].checked ? "1" : "0") ) ) + "," +
							( document.frmSettings2party.elements[++eno].checked ? "1" : "0") + "!!" ; //: (document.frmSettings2party.elements[++eno].checked ? "1" : "0") ) ) + "," + 
							
							if(document.frmSettings2party.elements[++eno].checked)
							    pcParty = "0"; //FB 2348
							if(document.frmSettings2party.elements[++eno].checked)
							    vmrParty = "0"; //FB 2348
							
			 infoParty +=   ( (document.frmSettings2party.elements[++eno].checked) ? "1" : "0" ) + "!!" + 
			                ( (document.frmSettings2party.elements[++eno].checked) ? "1" : "0" ) + "!!" + 
							( (document.frmSettings2party.elements[++eno].checked) ? "1" : "0" ) + "!!" + 
							( (document.frmSettings2party.elements[++eno].checked) ? "1" : "0" ) + "!!" + 
							( (document.frmSettings2party.elements[++eno].checked) ? "1" : "0" ) + "!!" + 
							( (document.frmSettings2party.elements[++eno].checked) ? "1" : "0" ) + "!!" + 
							( (document.frmSettings2party.elements[++eno].checked) ? "1" : "0" ) + "!!" +
							//FB 2348 Start
							( (document.frmSettings2party.elements[++eno].checked) ? "1" : "0" ) + "!!" + 
							( (document.frmSettings2party.elements[++eno].checked) ? "1" : "0" ) + "!!" + 
							( (document.frmSettings2party.elements[++eno].checked) ? "1" : "0" ) + "!!" + 
							( (document.frmSettings2party.elements[++eno].checked) ? "1" : "0" ) + "!!" + 
							pcParty +"!!"+ vmrParty +"!!0||" ;//FB 1888  //FB 2550 - Public Party
							//FB 2348 End
							//"-1,,,;" + partysinfo;
			
			partysinfo = infoParty + partysinfo;
							
			cornew ++;
			eno += 0; //FB 2348
			
		} else {
			eno += ( ( isFuCreate || isRm) ? 14 : 15);
			alert(EN_210);
			return false;
		}
	}
//alert("after New Party=" + partysinfo);

	partysinfo = addBlankLine(newpartyno-cornew, partysinfo);
//alert("after addBlankLine=" + partysinfo);

	partysary = partysinfo.split("||");//FB 1888
	//alert(oldpartyno);
	
	for (i=0; i<oldpartyno; i++) {
		pemail = (document.frmSettings2party.elements[eno+1].name).substr(1, (document.frmSettings2party.elements[eno+1].name).length-1)
		for (j=0; j < partysary.length-1; j++) {
			partyary = partysary[j].split("!!");//FB 1888
			if (partyary[3]==pemail) {
 				partyary[4] = (document.frmSettings2party.elements[eno+1].checked) ? "1" : "0";
				partyary[5] = (document.frmSettings2party.elements[eno+2].checked) ? "1" : "0";
				partyary[17] = (document.frmSettings2party.elements[eno+3].checked) ? "1" : "0";//FB 2348 
				partyary[18] = (document.frmSettings2party.elements[eno+4].checked) ? "1" : "0";//FB 2348
				partyary[6] = (document.frmSettings2party.elements[eno+5].checked) ? "1" : "0";//FB 2348
				partyary[7] = (document.frmSettings2party.elements[eno+6].checked) ? "1" : "0";//FB 2348
				
//				partyary[4] = ( isFuCreate ? (document.frmSettings2party.elements[eno+1].checked ? "1" : "0") : (isRm ? "0" : (document.frmSettings2party.elements[eno+1].checked ? "1" : "0") ) );
//				partyary[5] = ( isFuCreate ? "0" : (isRm ? (document.frmSettings2party.elements[eno+1].checked ? "1" : "0") : (document.frmSettings2party.elements[eno+2].checked ? "1" : "0") ) );
				
//				partyary[6] = (document.frmSettings2party.elements[(isFuCreate || isRm) ? (eno+2) : (eno+3)].checked) ? "1" : "0";
//				partyary[7] = (document.frmSettings2party.elements[(isFuCreate || isRm) ? (eno+3) : (eno+4)].checked) ? "1" : "0";
				partyary[8] = (document.frmSettings2party.elements[(!isRm) ? (eno+7) : (eno+6)].checked) ? "1" : "0";//FB 2348
				partyary[9] = (document.frmSettings2party.elements[(isVD) ? (eno+8) : (eno+5)].checked) ? "1" : "0";//FB 2348
				partyary[10] = "0"; //document.frmSettings2party.elements[(isVD) ? (eno+7) : (eno+6)].value;
				partyary[11] = "0"; //document.frmSettings2party.elements[(isVD) ? (eno+8) : (eno+7)].value;
				//alert(partyary[12]);
				partyary[12] = partyary[12]; //document.frmSettings2party.elements[(isVD) ? (eno+9) : (eno+8)].value;
				partyary[13] = "0"; //document.frmSettings2party.elements[(isVD) ? (eno+10) : (eno+9)].value;
				partyary[14] = "0"; //document.frmSettings2party.elements[(isVD) ? (eno+10) : (eno+9)].value;
				partyary[15] = "0"; //document.frmSettings2party.elements[(isFuCreate || isRm) ? (eno+11) : (eno+12)].value;
				//FB 2348 Start
				partyary[16] = (document.frmSettings2party.elements[eno+15].checked) ? "1" : "0";
				partyary[18] = "0";
				//FB 2376 End
				partysary[j] = assemble(partyary, "!!");//FB 1888
				//alert(partysary[j]);
			}
		}
/*		if (isRm)
		    eno += 12;
		if (isAD)
		    eno += 12;
		if (isVD)
		    eno += 12;
*/
		eno += 15; //FB 2348
	}

	partysinfo = assemble(partysary, "||");//FB 1888
//alert("after Old Party, partysary=" + partysary);
	parent.document.getElementById("txtPartysInfo").value = partysinfo;	
	
	return true;
}

function nFocus(cb)
{
	if ( (cb.value=="[First Name]") || (cb.value=="[Last Name]") || (cb.value=="[Enter Email]") ) {
		cb.value = "";
	}
}


function nBlur(cb)
{
	if ( ((cb.name).indexOf("NewParticipantFirstName") != -1) && (cb.value=="") ) {
		cb.value = "[First Name]";
	}
	
	if ( ((cb.name).indexOf("NewParticipantLastName") != -1) && (cb.value=="") ) {
		cb.value = "[Last Name]";
	}
	if ( ((cb.name).indexOf("NewParticipantEmail") != -1) && (cb.value=="") ) {
		cb.value = "[Enter Email]";
	}
}


function deleteThisParty(thisparty)
{
	var needRemove = confirm("Are you sure you want to remove this participant?")
	if (needRemove == true) {
		if (thisparty != "") {
			willContinue = true;//bfrRefresh();  FB Case 727
            if (willContinue) {
				partysinfo = parent.document.getElementById("txtPartysInfo").value;	
				
				//FB 1966 - Starts
				var pInfoSplit = partysinfo.split("||")
				var strpartyInfo = ""
				for(var p=0;p< pInfoSplit.length; p++)
				{
				    var party = pInfoSplit[p].indexOf(thisparty);
				    if(party < 0)
				    {
				        if(strpartyInfo == "")
				            strpartyInfo = pInfoSplit[p];
				        else
				            strpartyInfo = strpartyInfo + "||" + pInfoSplit[p];
				    }
				}
			    parent.document.getElementById("txtPartysInfo").value = strpartyInfo;
				
				/*if ( (tmploc = partysinfo.indexOf("!!" + thisparty + "!!")) != -1 )//FB 1888
					parent.document.getElementById("txtPartysInfo").value = partysinfo.substring(0, partysinfo.lastIndexOf("||", tmploc)+1) + partysinfo.substring(partysinfo.indexOf("||", tmploc)+1, partysinfo.length);//FB 1888
			    */
				//FB 1966 - End
			    
				//history.go(0); // Commented for FB 2050
				parent.refreshIframe(); // FB 2050
			}
		} else {
				partysinfo = parent.document.getElementById("txtPartysInfo").value;	
				
				if ( (tmploc = partysinfo.indexOf("!!" + thisparty + "!!")) != -1 )//FB 1888
					parent.document.getElementById("txtPartysInfo").value = partysinfo.substring(0, partysinfo.lastIndexOf("||", tmploc)+1) + partysinfo.substring(partysinfo.indexOf("||", tmploc)+2, partysinfo.length);
			
				//history.go(0); // Commented for FB 2050
				parent.refreshIframe(); // FB 2050
		}
	}
}


function frmSettings2party_Validator()
{
	return true;
}

//-->
</script>
<!-- JavaScript finish -->


<!-- JavaScript begin -->
<script language="JavaScript">
<!--
	partysinfo = parent.document.getElementById("txtPartysInfo").value;

	_d = document;
	var mt = "";
	
	
	mt += "<div align='center'>";
	mt += "  <form name='frmSettings2party' method='POST' action=''>"
	mt += "  <table border='0' cellpadding='0' cellspacing='0' width='100%'>";
//	alert(partysinfo)

    if (isRm || isAD || isVD)
    {
        mt += "<thead><tr class='tableHeader'>";
        mt += " <td align='center' class='tableHeader'>Delete</td>";    
        mt += " <td align='center' class='tableHeader' width='20%'>Name</td>";    
        mt += " <td align='center' class='tableHeader'>Email</td>";
        if (!isRm && !isVMR)//FB 2448)     
            mt += " <td align='center' class='tableHeader'>External<br>Attendee</td>";    
        mt += " <td align='center' class='tableHeader'>Room<br>Attendee</td>";    
        mt += " <td align='center' class='tableHeader'>CC</td>";    
        mt += " <td align='center' class='tableHeader'>Notify</td>";
        if (isVD)
            mt += " <td align='center' class='tableHeader'>Video</td>";             
        else //FB 1760
            mt += " <td width='0%' style='display: none' class='tableHeader'>Video</td>";  
        if (isAD || isVD)
            mt += " <td align='center' class='tableHeader'>Audio</td>";
        //FB 2348 Start
        if("<%=Session["EnableSurvey"]%>" == "1")
        {
            mt += " <td align='center' class='tableHeader'>Survey</td>";
        }
        //FB 2348 End
        mt += "</tr></thead>";
    }

	partysary = partysinfo.split("||");//FB 1888
	
	for (var i=0; i < partysary.length-1; i++) {
		tdbgcolor = (i % 2 == 0) ? "#e1e1e1" : "#e1e1e1";


		partyary = partysary[i].split("!!");//FB 1888
		//alert(partyary);
		partyemail = partyary[3];
//alert(partyary);
		if (partyemail=="") {
			newpartyno++;
			mt += "    <tr class='tableBody'>"
            mt += "      <td align='center' width='3%'>"
			mt += "        <a href='#' onCLick='Javascript:deleteThisParty(\"" + partyary[3] + "\")'><img border='0' src='image/btn_delete.gif' alt='Delete' WIDTH='16' HEIGHT='16' style='cursor:pointer;' title='Delete' ></a>"
            mt += "      </td>"
            mt += "      <td align='center' width='28%'>"
            mt += "        <input id='addPartyFname' type='text' name='NewParticipantFirstName" + newpartyno + "' size='10' value='[First Name]' onFocus='JavaScript: nFocus(this);' onBlur='JavaScript: nBlur(this);' onchange='javascript:chkLimit(this, 254);'  class='altText'>" //FB 1888
            mt += "        <input id='addPartyLname' type='text' name='NewParticipantLastName" + newpartyno + "' size='10' value='[Last Name]' onFocus='JavaScript: nFocus(this);' onBlur='JavaScript: nBlur(this);' onchange='javascript:chkLimit(this, 254);' class='altText'>" //FB 1888 
            mt += "      </td>"
            mt += "      <td align='center' width='27%'>"
            mt += "        <input type='text' name='NewParticipantEmail" + newpartyno + "' value='[Enter Email]'  onFocus='JavaScript: nFocus(this);' onBlur='JavaScript: nBlur(this);' size='15' class='altText'>" //fogbugz case 482
            mt += "      </td>"

            //mt += ( (isFuCreate || isRm) ? "      <td align='center' width='7%' bgcolor='" + tdbgcolor + "'><input type='radio' name='NewInvitedCCParticipant" + newpartyno + "' value='1'" + ( ( (partyary[4]=="1") || (partyary[5]=="1") ) ? " checked" : "") + "></td>" : "" );
            //mt += ( (isFuCreate || isRm) ? "" : "      <td align='center' width='7%' bgcolor='" + tdbgcolor + "'><input type='radio' name='NewInvitedCCParticipant" + newpartyno + "' value='1'" + ((partyary[4]=="1") ? " checked" : "") + "></td>" );
            //mt += ( (isFuCreate || isRm) ? "" : "      <td align='center' width='7%' bgcolor='" + tdbgcolor + "'><input type='radio' name='NewInvitedCCParticipant" + newpartyno + "' value='2'" + ((partyary[5]=="1") ? " checked" : "") + "></td>" );
            if (isRm || isVMR)//FB 2448
			    disp = "none";
			else
			    disp = "";
            mt += "      <td align='center' width='3%' style='display:" + disp + "'><input type='radio' name='NewInvitedCCParticipant" + newpartyno + "' value='0'" + ((partyary[4]=="1") ? " checked" : "") + "></td>"
            /* Code Modified For FB 1456 - Start */
			if (isRm)
                mt += "      <td align='center' width='3%'><input type='radio' name='NewInvitedCCParticipant" + newpartyno + "' value='0'" + ((partyary[5]=="1") ? " checked" : " checked") + "></td>"
            else
                mt += "      <td align='center' width='3%'><input type='radio' name='NewInvitedCCParticipant" + newpartyno + "' value='0'" + ((partyary[5]=="1") ? " checked" : "") + "></td>"
            /* Code Modified For FB 1456 - End */  
            //FB 2348 Start
             mt += "      <td align='center' style='display:none' width='5%'><input type='radio' name='NewsurveyParticipant" + newpartyno + "' value='1'" + ((partyary[16]=="1") ? " checked" : "") + "></td>"
             mt += "      <td align='center' style='display:none' width='5%'><input type='radio' name='NewsurveyParticipant" + newpartyno + "' value='1'" + ((partyary[16]=="1") ? " checked" : "") + "></td>"
            //FB 2348 End   
            mt += "      <td align='center' width='3%'><input type='radio' name='NewInvitedCCParticipant" + newpartyno + "' value='0'" + ((partyary[6]=="1") ? " checked" : "") + "></td>"
            mt += "      <td align='center' width='5%'><input type='checkbox' name='NewNotifyParticipant" + newpartyno + "' value='1'" + ((partyary[7]=="1") ? " checked" : "") + "></td>"
			//FB 1760 - Start
            if(!isRm)
              {
                if(!isVD) 
                  {
                    mt += "<td align='centre' style='display: none'><input type='radio' name='3" + partyary[3] + "' value='0'" + ((partyary[8]=="1")? "":"") + ">";
                    mt += "<td align='centre' width='10%'><input type='radio' name='3" + partyary[3] + "' value='0'" + ((partyary[9]=="1")? "checked": "checked") + "></td>";
                  }
                if(isVD)
                  {
                    mt += "<td align='centre' width='5%'><input type='radio' name='3" + partyary[3] + "' value='0'" + ((partyary[8]=="1")? "checked": "") + "></td>";
                    mt += "<td align='centre' width='5%'><input type='radio' name='3" + partyary[3] + "' value='0'" + ((partyary[9]=="1")? "checked": "") + "></td>";
                  }
              }
            else
              {
                mt += "<td align='centre' style='display: none'><input type='radio' name='3" + partyary[3] + "' value='0'" + ((partyary[8]=="1")? "":"") + "></td>";
			    mt += "<td align='centre' style='display: none'><input type='radio' name='3" + partyary[3] + "' value='0'" + ((partyary[9]=="1")? "checked":"checked") + "></td>";
              }
            //FB 1760 - End
			mt += "      <input type='hidden' name='NewInterfaceTypeParticipant" + newpartyno + "' value='" + partyary[10] + "'>";
			mt += "      <input type='hidden' name='NewConnectionTypeParticipant" + newpartyno + "' value='" + partyary[11] + "'>";
			mt += "      <input type='hidden' name='NewSourceParticipant" + newpartyno + "' value='" + partyary[12] + "'>";
			mt += "      <input type='hidden' name='NewIPISDNAddressParticipant" + newpartyno + "' value='" + partyary[13] + "'>";
			mt += "      <input type='hidden' name='NewVideoEquipmentParticipant" + newpartyno + "' value='" + partyary[14] + "'>";
			mt += "      <input type='hidden' name='NewLineRateParticipant" + newpartyno + "' value='" + partyary[15] + "'>";
			//FB 2348 Start
            if("<%=Session["EnableSurvey"]%>" == "1")
            {
                mt += "      <td align='center' width='5%'><input type='checkbox' name='NewsurveyParticipant" + newpartyno + "' value='1'" + ((partyary[16]=="1") ? " checked" : "") + "></td>"
			}
			else
			{
			    mt += "      <td align='center' style='display:none' width='5%'><input type='checkbox' name='NewsurveyParticipant" + newpartyno + "' value='1'" + ((partyary[16]=="1") ? " checked" : "") + "></td>"
			}
			//FB 2348 End
			mt += "    </tr>"
		
		} else {
			if(partyary[3].length > 18){
				strLongEmail = partyary[3].substr(0,17) + "..."
			}
			else{
				strLongEmail = partyary[3]
								
			}
			if (partyary[2].replace(/\s/g, "") == "&nbsp;") //FB 2023
			{
			    partyary[2] = "";
			    if(partyary[9] == "1")//FB 2348
			        strLongEmail = "";
			}
			    
			oldpartyno++;
			mt += "    <tr class='tableBody' height='10px'>"
			mt += "      <td align='center' width='3%'>" + " <a href='#' onCLick='Javascript:deleteThisParty(\"" + partyary[3] + "\")'><img border='0' src='image/btn_delete.gif' alt='Delete' WIDTH='16' HEIGHT='16' style='cursor:pointer;' title='Delete' ></a></td>"
			mt += "      <td align='center' class ='tableBody' width='28%'>" + partyary[1] + " " + partyary[2] + "</td>"
			mt += "      <td align='center' class ='tableBody' width='27%' title='" + partyary[3] + "'>" + strLongEmail + "</td>"
			//mt += "      <td align='center' width='27%' bgcolor='" + tdbgcolor + "'>" + partyary[3] + "</td>"
			//mt += ( (isFuCreate || isRm) ? "      <td align='center' width='7%' bgcolor='" + tdbgcolor + "'><input type='radio' name='1" + partyary[3] + "' value='1'" + ( ( (partyary[4]=="1") || (partyary[5]=="1") ) ? " checked" : "") + "></td>" : "");
			//mt += ( (isFuCreate || isRm) ? "" : "      <td align='center' width='7%' bgcolor='" + tdbgcolor + "'><input type='radio' name='1" + partyary[3] + "' value='1'" + ((partyary[4]=="1") ? " checked" : "") + "></td>" );
			//mt += ( (isFuCreate || isRm) ? "" : "      <td align='center' width='7%' bgcolor='" + tdbgcolor + "'><input type='radio' name='1" + partyary[3] + "' value='2'" + ((partyary[5]=="1") ? " checked" : "") + "></td>" );
			if (isRm || isVMR)//FB 2448
			    disp = "none";
			else
			    disp = "";
			mt += "<td align='center' width='7%' style='display:" + disp + "'><input type='radio' name='1" + partyary[3] + "' value='1'" + ((partyary[4]=="1") ? " checked" : "") + "></td>"; 
			/* Code Modified For FB 1456 - Start */
			if (isRm)
                mt += "<td align='center' width='7%'><input type='radio' name='1" + partyary[3] + "' value='2'" + ((partyary[5]=="1") ? " checked" : " checked") + "></td>";
            else			
                mt += "<td align='center' width='7%'><input type='radio' name='1" + partyary[3] + "' value='2'" + ((partyary[5]=="1") ? " checked" : "") + "></td>";
			/* Code Modified For FB 1456 - End */    
			//FB 2348 Start
			mt += "      <td align='center' style='display:none' width='5%'><input type='radio' name='NewsurveyParticipant" + newpartyno + "' value='1'" + ((partyary[16]=="1") ? " checked" : "") + "></td>"
            mt += "      <td align='center' style='display:none' width='5%'><input type='radio' name='NewsurveyParticipant" + newpartyno + "' value='1'" + ((partyary[16]=="1") ? " checked" : "") + "></td>"
            //FB 2348 End              
            mt += "<td align='center' width='7%'><input type='radio' name='1" + partyary[3] + "' value='3'" + ((partyary[6]=="1") ? " checked" : "") + "></td>";
			mt += "<td align='center' width='5%'><input type='checkbox' name='2" + partyary[3] + "' value='1'" + ((partyary[7]=="1") ? " checked" : "") + "></td>"
			//FB 1760 - Start
            if(!isRm)
              {
                if(!isVD) 
                  {
                    mt += "<td align='centre' style='display: none'><input type='radio' name='3" + partyary[3] + "' value='0'" + ((partyary[8]=="1")? "":"") + ">";
                    mt += "<td align='centre' width='10%'><input type='radio' name='3" + partyary[3] + "' value='0'" + ((partyary[9]=="1")? "checked": "checked") + "></td>";
                  }
                if(isVD)
                  {
                    mt += "<td align='centre' width='5%'><input type='radio' name='3" + partyary[3] + "' value='0'" + ((partyary[8]=="1")? "checked": "") + "></td>";
                    mt += "<td align='centre' width='5%'><input type='radio' name='3" + partyary[3] + "' value='0'" + ((partyary[9]=="1")? "checked": "") + "></td>";
                  }
              }
            else
              {
                mt += "<td align='centre' style='display: none'><input type='radio' name='3" + partyary[3] + "' value='0'" + ((partyary[8]=="1")? "":"") + "></td>";
			    mt += "<td align='centre' style='display: none'><input type='radio' name='3" + partyary[3] + "' value='0'" + ((partyary[9]=="1")? "checked":"checked") + "></td>";
              }
            //FB 1760 - End
            mt += "      <input type='hidden' name='NewInterfaceTypeParticipant" + newpartyno + "' value='" + partyary[10] + "'>";
            mt += "      <input type='hidden' name='NewConnectionTypeParticipant" + newpartyno + "' value='" + partyary[11] + "'>";
			mt += "      <input type='hidden' name='NewSourceParticipant" + newpartyno + "' value='" + partyary[12] + "'>";
			mt += "      <input type='hidden' name='NewIPISDNAddressParticipant" + newpartyno + "' value='" + partyary[13] + "'>";
			mt += "      <input type='hidden' name='NewVideoEquipmentParticipant" + newpartyno + "' value='" + partyary[14] + "'>";
			mt += "      <input type='hidden' name='NewLineRateParticipant" + newpartyno + "' value='" + partyary[15] + "'>";
			//FB 2348 Start
            if("<%=Session["EnableSurvey"]%>" == "1")
            {
                mt += "<td align='center' width='5%'><input type='checkbox' name='2" + partyary[3] + "' value='1'" + ((partyary[16]=="1") ? " checked" : "") + "></td>"
			}
			else
			{
			    mt += "<td align='center' style='display: none' width='5%'><input type='checkbox' name='2" + partyary[3] + "' value='1'" + ((partyary[16]=="1") ? " checked" : "") + "></td>"
			}
			//FB 2348 End
			mt += "    </tr>"
		}
	}

	mt += "  </table>"
	mt += "  </form>"
    mt += "  </div>" //Edited for FF
    _d.write(mt)

	if (parent.document.frmSettings2.NeedInitial) {
		if (parent.document.frmSettings2.NeedInitial.value == "1") {

			parent.initialpartlist();

			parent.document.frmSettings2.NeedInitial.value = "0";
		}
	} else {
		setTimeout('window.location.reload();',500);
	}

//-->
</script>
<!-- JavaScript finish -->
<body bottommargin="0" leftmargin="0" rightmargin="0" topmargin="0" marginheight="0" marginwidth="0">

	 
</body>

</html>
