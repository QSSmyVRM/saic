//ZD 100147 Start
/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
//ZD 100147 End
using System;

namespace myVRM.ExcelXmlWriter
{
	public enum DataType
	{
		Boolean = 4,
		DateTime = 3,
		Error = 0x3e7,
		Integer = 5,
		NotSet = 0,
		Number = 2,
		String = 1
	}

	public enum ItemType
	{
		NotSet,
		Grand
	}

	public enum LineStyleOption
	{
		NotSet,
		Continuous,
		Dot,
		Dash,
		DashDot,
		DashDotDot,
		SlantDashDot,
		Double
	}
 
	public enum Orientation
	{
		NotSet,
		Portrait,
		Landscape
	}
	public enum PivotFieldOrientation
	{
		NotSet,
		Row,
		Data
	}
	public enum PrintCommentsLayout
	{
		None,
		SheetEnd,
		InPlace
	}

	public enum PrintErrorsOption
	{
		Displayed,
		Blank,
		Dash,
		NA
	}

	public enum PTFunction
	{
		NotSet,
		Count
	}

	public enum StyleHorizontalAlignment
	{
		Automatic,
		Left,
		Center,
		Right,
		Fill,
		Justify,
		CenterAcrossSelection,
		Distributed,
		JustifyDistributed
	}

	public enum StyleInteriorPattern
	{
		NotSet,
		None,
		Solid,
		Gray75,
		Gray50,
		Gray25,
		Gray125,
		Gray0625,
		HorzStripe,
		VertStripe,
		ReverseDiagStripe,
		DiagStripe,
		DiagCross,
		ThickDiagCross,
		ThinHorzStripe,
		ThinVertStripe,
		ThinReverseDiagStripe,
		ThinDiagStripe,
		ThinHorzCross,
		ThinDiagCross
	}

	public enum StylePosition
	{
		NotSet,
		Left,
		Top,
		Right,
		Bottom
	}

	public enum StyleReadingOrder
	{
		NotSet,
		Context,
		RightToLeft,
		LeftToRight
	}

	public enum StyleVerticalAlignment
	{
		Automatic,
		Top,
		Bottom,
		Center,
		Justify,
		Distributed,
		JustifyDistributed
	}

	public enum UnderlineStyle
	{
		None,
		Single,
		Double,
		SingleAccounting,
		DoubleAccounting
	}

}
