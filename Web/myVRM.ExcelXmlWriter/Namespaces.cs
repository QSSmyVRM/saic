//ZD 100147 Start
/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
//ZD 100147 End
using System;

namespace myVRM.ExcelXmlWriter
{
	internal sealed class Namespaces
	{
		// Fields
		public const string ComponentNamespace = "urn:schemas-microsoft-com:office:component:spreadsheet";
		public const string ComponentPrefix = "c";
		public const string DataType = "uuid:C2F41010-65B3-11d1-A29F-00AA00C14882";
		public const string DataTypePrefix = "dt";
		public const string Excel = "urn:schemas-microsoft-com:office:excel";
		public const string ExcelPrefix = "x";
		public const string Office = "urn:schemas-microsoft-com:office:office";
		public const string OfficePrefix = "o";
		public const string Rowset = "urn:schemas-microsoft-com:rowset";
		public const string RowsetPrefix = "rs";
		public const string RowsetSchema = "#RowsetSchema";
		public const string RowsetSchemaPrefix = "z";
		public const string Schema = "uuid:BDC6E3F0-6DA3-11d1-A2A3-00AA00C14882";
		public const string SchemaPrefix = "s";
		public const string SpreadSheet = "urn:schemas-microsoft-com:office:spreadsheet";
		public const string SpreadSheetPrefix = "s";

		// Methods
		private Namespaces()
		{
		}
	}
}
