//ZD 100147 Start
/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
//ZD 100147 End
using System;
using System.Globalization;
using System.Collections;
using System.Collections.Specialized;
using System.Xml;
using System.CodeDom;
using myVRM.ExcelXmlWriter.Schemas;

namespace myVRM.ExcelXmlWriter
{
	public sealed class PTConsolidationReference : IWriter
	{
		// Fields
		private string _fileName;
		private string _reference;

		// Methods
		internal PTConsolidationReference()
		{
		}

		void IWriter.WriteXml(XmlWriter writer)
		{
			writer.WriteStartElement("x", "ConsolidationReference", "urn:schemas-microsoft-com:office:excel");
			if (this._fileName != null)
			{
				writer.WriteElementString("FileName", "urn:schemas-microsoft-com:office:excel", this._fileName);
			}
			if (this._reference != null)
			{
				writer.WriteElementString("Reference", "urn:schemas-microsoft-com:office:excel", this._reference);
			}
			writer.WriteEndElement();
		}

		// Properties
		public string FileName
		{
			get
			{
				return this._fileName;
			}
			set
			{
				this._fileName = value;
			}
		}

		public string Reference
		{
			get
			{
				return this._reference;
			}
			set
			{
				this._reference = value;
			}
		}
	}

	public sealed class PTLineItem : IWriter
	{
		// Fields
		private string _item;
		private ItemType _itemType;

		// Methods
		public PTLineItem()
		{
		}

		public PTLineItem(string item)
		{
			this._item = item;
		}

		public PTLineItem(string item, ItemType itemType)
		{
			this._item = item;
			this._itemType = itemType;
		}

		void IWriter.WriteXml(XmlWriter writer)
		{
			writer.WriteStartElement("x", "PTLineItem", "urn:schemas-microsoft-com:office:excel");
			writer.WriteElementString("Item", "urn:schemas-microsoft-com:office:excel", this._item);
			if (this._itemType != ItemType.NotSet)
			{
				writer.WriteElementString("ItemType", "urn:schemas-microsoft-com:office:excel", this._itemType.ToString(CultureInfo.InvariantCulture));
			}
			writer.WriteEndElement();
		}

		// Properties
		public string Item
		{
			get
			{
				return this._item;
			}
			set
			{
				this._item = value;
			}
		}

		public ItemType ItemType
		{
			get
			{
				return this._itemType;
			}
			set
			{
				this._itemType = value;
			}
		}
	}

	public sealed class PTLineItemCollection : CollectionBase, IWriter
	{
		// Methods
		internal PTLineItemCollection()
		{
		}

		public int Add(PTLineItem pTLineItem)
		{
			return base.InnerList.Add(pTLineItem);
		}

		void IWriter.WriteXml(XmlWriter writer)
		{
			writer.WriteStartElement("x", "PTLineItems", "urn:schemas-microsoft-com:office:excel");
			for (int i = 0; i < base.InnerList.Count; i++)
			{
				((IWriter) base.InnerList[i]).WriteXml(writer);
			}
			writer.WriteEndElement();
		}

		public bool Contains(PTLineItem item)
		{
			return base.InnerList.Contains(item);
		}

		public void CopyTo(PTLineItem[] array, int index)
		{
			base.InnerList.CopyTo(array, index);
		}

		public int IndexOf(PTLineItem item)
		{
			return base.InnerList.IndexOf(item);
		}

		public void Insert(int index, PTLineItem item)
		{
			base.InnerList.Insert(index, item);
		}

		public void Remove(PTLineItem item)
		{
			base.InnerList.Remove(item);
		}

		// Properties
		public PTLineItem this[int index]
		{
			get
			{
				return (PTLineItem) base.InnerList[index];
			}
		}
	}

	public sealed class PTSource : IWriter
	{
		// Fields
		private int _cacheIndex = Int32.MinValue;
		private PTConsolidationReference _consolidationReference;
		private DateTime _refreshDate = DateTime.MinValue;
		private DateTime _refreshDateCopy = DateTime.MinValue;
		private string _refreshName;
		private int _versionLastRefresh = Int32.MinValue;

		// Methods
		internal PTSource()
		{
		}

		void IWriter.WriteXml(XmlWriter writer)
		{
			writer.WriteStartElement("x", "PTSource", "urn:schemas-microsoft-com:office:excel");
			if (this._cacheIndex != Int32.MinValue)
			{
				writer.WriteElementString("CacheIndex", "urn:schemas-microsoft-com:office:excel", this._cacheIndex.ToString(CultureInfo.InvariantCulture));
			}
			if (this._versionLastRefresh != Int32.MinValue)
			{
				writer.WriteElementString("VersionLastRefresh", "urn:schemas-microsoft-com:office:excel", this._versionLastRefresh.ToString(CultureInfo.InvariantCulture));
			}
			if (this._refreshName != null)
			{
				writer.WriteElementString("RefreshName", "urn:schemas-microsoft-com:office:excel", this._refreshName);
			}
			if (this._refreshDate != DateTime.MinValue)
			{
				writer.WriteElementString("RefreshDate", "urn:schemas-microsoft-com:office:excel", this._refreshDate.ToString("s", CultureInfo.InvariantCulture));
			}
			if (this._refreshDateCopy != DateTime.MinValue)
			{
				writer.WriteElementString("RefreshDateCopy", "urn:schemas-microsoft-com:office:excel", this._refreshDateCopy.ToString("s", CultureInfo.InvariantCulture));
			}
			if (this._consolidationReference != null)
			{
				((IWriter) this._consolidationReference).WriteXml(writer);
			}
			writer.WriteEndElement();
		}

		// Properties
		public int CacheIndex
		{
			get
			{
				return this._cacheIndex;
			}
			set
			{
				this._cacheIndex = value;
			}
		}

		public PTConsolidationReference ConsolidationReference
		{
			get
			{
				if (this._consolidationReference == null)
				{
					this._consolidationReference = new PTConsolidationReference();
				}
				return this._consolidationReference;
			}
			set
			{
				this._consolidationReference = value;
			}
		}

		public DateTime RefreshDate
		{
			get
			{
				return this._refreshDate;
			}
			set
			{
				this._refreshDate = value;
			}
		}

		public DateTime RefreshDateCopy
		{
			get
			{
				return this._refreshDateCopy;
			}
			set
			{
				this._refreshDateCopy = value;
			}
		}

		public string RefreshName
		{
			get
			{
				return this._refreshName;
			}
			set
			{
				this._refreshName = value;
			}
		}

		public int VersionLastRefresh
		{
			get
			{
				return this._versionLastRefresh;
			}
			set
			{
				this._versionLastRefresh = value;
			}
		}
	}
	public sealed class SpreadSheetComponentOptions : IWriter, IReader, ICodeWriter
	{
		// Fields
		private bool _doNotEnableResize;
		private string _maxHeight;
		private string _maxWidth;
		private int _nextSheetNumber = Int32.MinValue;
		private bool _preventPropBrowser;
		private bool _spreadsheetAutoFit;
		private SpreadSheetToolbar _toolbar;

		// Methods
		internal SpreadSheetComponentOptions()
		{
		}

		void ICodeWriter.WriteTo(CodeTypeDeclaration type, CodeMemberMethod method, CodeExpression targetObject)
		{
			if (this._toolbar != null)
			{
				((ICodeWriter) this._toolbar).WriteTo(type, method, new CodePropertyReferenceExpression(targetObject, "Toolbar"));
			}
			if (this._nextSheetNumber != Int32.MinValue)
			{
				Util.AddAssignment(method, targetObject, "NextSheetNumber", this._nextSheetNumber);
			}
			if (this._spreadsheetAutoFit)
			{
				Util.AddAssignment(method, targetObject, "SpreadsheetAutoFit", this._spreadsheetAutoFit);
			}
			if (this._doNotEnableResize)
			{
				Util.AddAssignment(method, targetObject, "DoNotEnableResize", this._doNotEnableResize);
			}
			if (this._preventPropBrowser)
			{
				Util.AddAssignment(method, targetObject, "PreventPropBrowser", this._preventPropBrowser);
			}
			if (this._maxHeight != null)
			{
				Util.AddAssignment(method, targetObject, "MaxHeight", this._maxHeight);
			}
			if (this._maxWidth != null)
			{
				Util.AddAssignment(method, targetObject, "MaxWidth", this._maxWidth);
			}
		}

		void IReader.ReadXml(XmlElement element)
		{
			if (!IsElement(element))
			{
				throw new ArgumentException("Invalid element", "element");
			}
			foreach (XmlNode node in element.ChildNodes)
			{
				XmlElement element2 = node as XmlElement;
				if (element2 != null)
				{
					if (SpreadSheetToolbar.IsElement(element2))
					{
						((IReader) this.Toolbar).ReadXml(element2);
					}
					else
					{
						if (Util.IsElement(element2, "NextSheetNumber", "urn:schemas-microsoft-com:office:component:spreadsheet"))
						{
							this._nextSheetNumber = int.Parse(element2.InnerText, CultureInfo.InvariantCulture);
							continue;
						}
						if (Util.IsElement(element2, "SpreadsheetAutoFit", "urn:schemas-microsoft-com:office:component:spreadsheet"))
						{
							this._spreadsheetAutoFit = true;
							continue;
						}
						if (Util.IsElement(element2, "DoNotEnableResize", "urn:schemas-microsoft-com:office:component:spreadsheet"))
						{
							this._doNotEnableResize = true;
							continue;
						}
						if (Util.IsElement(element2, "PreventPropBrowser", "urn:schemas-microsoft-com:office:component:spreadsheet"))
						{
							this._preventPropBrowser = true;
							continue;
						}
						if (Util.IsElement(element2, "MaxHeight", "urn:schemas-microsoft-com:office:component:spreadsheet"))
						{
							this._maxHeight = element2.InnerText;
							continue;
						}
						if (Util.IsElement(element2, "MaxWidth", "urn:schemas-microsoft-com:office:component:spreadsheet"))
						{
							this._maxWidth = element2.InnerText;
						}
					}
				}
			}
		}

		void IWriter.WriteXml(XmlWriter writer)
		{
			writer.WriteStartElement("c", "ComponentOptions", "urn:schemas-microsoft-com:office:component:spreadsheet");
			if (this._toolbar != null)
			{
				((IWriter) this._toolbar).WriteXml(writer);
			}
			if (this._nextSheetNumber != Int32.MinValue)
			{
				writer.WriteElementString("NextSheetNumber", "urn:schemas-microsoft-com:office:component:spreadsheet", this._nextSheetNumber.ToString(CultureInfo.InvariantCulture));
			}
			if (this._spreadsheetAutoFit)
			{
				writer.WriteElementString("SpreadsheetAutoFit", "urn:schemas-microsoft-com:office:component:spreadsheet", "");
			}
			if (this._doNotEnableResize)
			{
				writer.WriteElementString("DoNotEnableResize", "urn:schemas-microsoft-com:office:component:spreadsheet", "");
			}
			if (this._preventPropBrowser)
			{
				writer.WriteElementString("PreventPropBrowser", "urn:schemas-microsoft-com:office:component:spreadsheet", "");
			}
			if (this._maxHeight != null)
			{
				writer.WriteElementString("MaxHeight", "urn:schemas-microsoft-com:office:component:spreadsheet", this._maxHeight);
			}
			if (this._maxWidth != null)
			{
				writer.WriteElementString("MaxWidth", "urn:schemas-microsoft-com:office:component:spreadsheet", this._maxWidth);
			}
			writer.WriteEndElement();
		}

		internal static bool IsElement(XmlElement element)
		{
			return Util.IsElement(element, "ComponentOptions", "urn:schemas-microsoft-com:office:component:spreadsheet");
		}

		// Properties
		public bool DoNotEnableResize
		{
			get
			{
				return this._doNotEnableResize;
			}
			set
			{
				this._doNotEnableResize = value;
			}
		}

		public string MaxHeight
		{
			get
			{
				return this._maxHeight;
			}
			set
			{
				this._maxHeight = value;
			}
		}

		public string MaxWidth
		{
			get
			{
				return this._maxWidth;
			}
			set
			{
				this._maxWidth = value;
			}
		}

		public int NextSheetNumber
		{
			get
			{
				return this._nextSheetNumber;
			}
			set
			{
				this._nextSheetNumber = value;
			}
		}

		public bool PreventPropBrowser
		{
			get
			{
				return this._preventPropBrowser;
			}
			set
			{
				this._preventPropBrowser = value;
			}
		}

		public bool SpreadsheetAutoFit
		{
			get
			{
				return this._spreadsheetAutoFit;
			}
			set
			{
				this._spreadsheetAutoFit = value;
			}
		}

		public SpreadSheetToolbar Toolbar
		{
			get
			{
				if (this._toolbar == null)
				{
					this._toolbar = new SpreadSheetToolbar();
				}
				return this._toolbar;
			}
		}
	}
	public sealed class SpreadSheetToolbar : IWriter, IReader, ICodeWriter
	{
		// Fields
		private bool _hidden;

		// Methods
		internal SpreadSheetToolbar()
		{
		}

		void ICodeWriter.WriteTo(CodeTypeDeclaration type, CodeMemberMethod method, CodeExpression targetObject)
		{
			if (this._hidden)
			{
				Util.AddAssignment(method, targetObject, "Hidden", this._hidden);
			}
		}

		void IReader.ReadXml(XmlElement element)
		{
			if (!IsElement(element))
			{
				throw new ArgumentException("Invalid element", "element");
			}
			this._hidden = element.GetAttribute("Hidden", "urn:schemas-microsoft-com:office:spreadsheet") == "1";
		}

		void IWriter.WriteXml(XmlWriter writer)
		{
			writer.WriteStartElement("c", "Toolbar", "urn:schemas-microsoft-com:office:component:spreadsheet");
			if (this._hidden)
			{
				writer.WriteAttributeString("s", "Hidden", "urn:schemas-microsoft-com:office:spreadsheet", "1");
			}
			writer.WriteEndElement();
		}

		internal static bool IsElement(XmlElement element)
		{
			return Util.IsElement(element, "Toolbar", "urn:schemas-microsoft-com:office:component:spreadsheet");
		}

		// Properties
		public bool Hidden
		{
			get
			{
				return this._hidden;
			}
			set
			{
				this._hidden = value;
			}
		}
	}


}
