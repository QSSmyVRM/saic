//ZD 100147 Start
/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
//ZD 100147 End
using System;
using System.Globalization;
using System.Collections;
using System.Collections.Specialized;
using System.Xml;
using System.CodeDom;
using myVRM.ExcelXmlWriter.Schemas;

namespace myVRM.ExcelXmlWriter
{
	public sealed class PivotCache : IWriter
	{
		// Fields
		private int _cacheIndex = Int32.MinValue;
		private RowsetData _rowsetData;
		private Schema _schema;

		// Methods
		internal PivotCache()
		{
		}

		void IWriter.WriteXml(XmlWriter writer)
		{
			writer.WriteStartElement("x", "PivotCache", "urn:schemas-microsoft-com:office:excel");
			if (this._cacheIndex != Int32.MinValue)
			{
				writer.WriteElementString("CacheIndex", "urn:schemas-microsoft-com:office:excel", this._cacheIndex.ToString(CultureInfo.InvariantCulture));
			}
			if (this._schema != null)
			{
				((IWriter) this._schema).WriteXml(writer);
			}
			if (this._rowsetData != null)
			{
				((IWriter) this._rowsetData).WriteXml(writer);
			}
			writer.WriteEndElement();
		}

		// Properties
		public int CacheIndex
		{
			get
			{
				return this._cacheIndex;
			}
			set
			{
				this._cacheIndex = value;
			}
		}

		public RowsetData Data
		{
			get
			{
				if (this._rowsetData == null)
				{
					this._rowsetData = new RowsetData();
				}
				return this._rowsetData;
			}
		}

		public Schema Schema
		{
			get
			{
				if (this._schema == null)
				{
					this._schema = new Schema();
				}
				return this._schema;
			}
		}
	}

	public sealed class PivotField : IWriter
	{
		// Fields
		private string _dataField;
		private DataType _dataType;
		private PTFunction _function;
		private string _name;
		private PivotFieldOrientation _orientation = PivotFieldOrientation.NotSet;
		private string _parentField;
		private PivotItemCollection _pivotItems;
		private int _position = Int32.MinValue;

		// Methods
		void IWriter.WriteXml(XmlWriter writer)
		{
			writer.WriteStartElement("x", "PivotField", "urn:schemas-microsoft-com:office:excel");
			if (this._dataField != null)
			{
				writer.WriteElementString("DataField", "urn:schemas-microsoft-com:office:excel", this._dataField);
			}
			if (this._name != null)
			{
				writer.WriteElementString("Name", "urn:schemas-microsoft-com:office:excel", this._name);
			}
			if (this._parentField != null)
			{
				writer.WriteElementString("ParentField", "urn:schemas-microsoft-com:office:excel", this._parentField);
			}
			if (this._dataType != DataType.NotSet)
			{
				writer.WriteElementString("DataType", "urn:schemas-microsoft-com:office:excel", this._dataType.ToString(CultureInfo.InvariantCulture));
			}
			if (this._function != PTFunction.NotSet)
			{
				writer.WriteElementString("Function", "urn:schemas-microsoft-com:office:excel", this._function.ToString(CultureInfo.InvariantCulture));
			}
			if (this._position != Int32.MinValue)
			{
				writer.WriteElementString("Position", "urn:schemas-microsoft-com:office:excel", this._position.ToString(CultureInfo.InvariantCulture));
			}
			if (this._orientation != PivotFieldOrientation.NotSet)
			{
				writer.WriteElementString("Orientation", "urn:schemas-microsoft-com:office:excel", this._orientation.ToString(CultureInfo.InvariantCulture));
			}
			if (this._pivotItems != null)
			{
				((IWriter) this._pivotItems).WriteXml(writer);
			}
			writer.WriteEndElement();
		}

		// Properties
		public string DataField
		{
			get
			{
				return this._dataField;
			}
			set
			{
				this._dataField = value;
			}
		}

		public DataType DataType
		{
			get
			{
				return this._dataType;
			}
			set
			{
				this._dataType = value;
			}
		}

		public PTFunction Function
		{
			get
			{
				return this._function;
			}
			set
			{
				this._function = value;
			}
		}

		public string Name
		{
			get
			{
				return this._name;
			}
			set
			{
				this._name = value;
			}
		}

		public PivotFieldOrientation Orientation
		{
			get
			{
				return this._orientation;
			}
			set
			{
				this._orientation = value;
			}
		}

		public string ParentField
		{
			get
			{
				return this._parentField;
			}
			set
			{
				this._parentField = value;
			}
		}

		public PivotItemCollection PivotItems
		{
			get
			{
				if (this._pivotItems == null)
				{
					this._pivotItems = new PivotItemCollection();
				}
				return this._pivotItems;
			}
		}

		public int Position
		{
			get
			{
				return this._position;
			}
			set
			{
				this._position = value;
			}
		}
	}

	public sealed class PivotFieldCollection : CollectionBase, IWriter
	{
		// Methods
		internal PivotFieldCollection()
		{
		}

		public PivotField Add()
		{
			PivotField pivotField = new PivotField();
			this.Add(pivotField);
			return pivotField;
		}

		public int Add(PivotField pivotField)
		{
			return base.InnerList.Add(pivotField);
		}

		void IWriter.WriteXml(XmlWriter writer)
		{
			for (int i = 0; i < base.InnerList.Count; i++)
			{
				((IWriter) base.InnerList[i]).WriteXml(writer);
			}
		}

		public bool Contains(PivotField item)
		{
			return base.InnerList.Contains(item);
		}

		public void CopyTo(PivotField[] array, int index)
		{
			base.InnerList.CopyTo(array, index);
		}

		public int IndexOf(PivotField item)
		{
			return base.InnerList.IndexOf(item);
		}

		public void Insert(int index, PivotField field)
		{
			base.InnerList.Insert(index, field);
		}

		public void Remove(PivotField field)
		{
			base.InnerList.Remove(field);
		}

		// Properties
		public PivotField this[int index]
		{
			get
			{
				return (PivotField) base.InnerList[index];
			}
		}
	}

	public sealed class PivotItem : IWriter
	{
		// Fields
		private bool _hideDetail;
		private string _name;

		// Methods
		public PivotItem()
		{
		}

		public PivotItem(string name)
		{
			this._name = name;
		}

		void IWriter.WriteXml(XmlWriter writer)
		{
			writer.WriteStartElement("x", "PivotItem", "urn:schemas-microsoft-com:office:excel");
			if (this._name != null)
			{
				writer.WriteElementString("Name", "urn:schemas-microsoft-com:office:excel", this._name);
			}
			if (this._hideDetail)
			{
				writer.WriteElementString("HideDetail", "urn:schemas-microsoft-com:office:excel", "");
			}
			writer.WriteEndElement();
		}

		// Properties
		public bool HideDetail
		{
			get
			{
				return this._hideDetail;
			}
			set
			{
				this._hideDetail = value;
			}
		}

		public string Name
		{
			get
			{
				return this._name;
			}
			set
			{
				this._name = value;
			}
		}
	}

	public sealed class PivotItemCollection : CollectionBase, IWriter
	{
		// Methods
		internal PivotItemCollection()
		{
		}

		public int Add(PivotItem pivotItem)
		{
			return base.InnerList.Add(pivotItem);
		}

		void IWriter.WriteXml(XmlWriter writer)
		{
			for (int i = 0; i < base.InnerList.Count; i++)
			{
				((IWriter) base.InnerList[i]).WriteXml(writer);
			}
		}

		public bool Contains(PivotItem item)
		{
			return base.InnerList.Contains(item);
		}

		public void CopyTo(PivotItem[] array, int index)
		{
			base.InnerList.CopyTo(array, index);
		}

		public int IndexOf(PivotItem item)
		{
			return base.InnerList.IndexOf(item);
		}

		public void Insert(int index, PivotItem item)
		{
			base.InnerList.Insert(index, item);
		}

		public void Remove(PivotItem item)
		{
			base.InnerList.Remove(item);
		}

		// Properties
		public PivotItem this[int index]
		{
			get
			{
				return (PivotItem) base.InnerList[index];
			}
		}
	}
	public sealed class PivotTable : IWriter
	{
		// Fields
		private string _defaultVersion;
		private string _location;
		private string _name;
		private PivotFieldCollection _pivotFields;
		private PTLineItemCollection _pTLinesItems;
		private PTSource _pTSource;
		private string _versionLastUpdate;

		// Methods
		internal PivotTable()
		{
		}

		void IWriter.WriteXml(XmlWriter writer)
		{
			writer.WriteStartElement("x", "PivotTable", "urn:schemas-microsoft-com:office:excel");
			if (this._name != null)
			{
				writer.WriteElementString("Name", "urn:schemas-microsoft-com:office:excel", this._name);
			}
			writer.WriteStartElement("x", "ImmediateItemsOnDrop", "urn:schemas-microsoft-com:office:excel");
			writer.WriteEndElement();
			writer.WriteStartElement("x", "ShowPageMultipleItemLabel", "urn:schemas-microsoft-com:office:excel");
			writer.WriteEndElement();
			if (this._location != null)
			{
				writer.WriteElementString("Location", "urn:schemas-microsoft-com:office:excel", this._location);
			}
			if (this._defaultVersion != null)
			{
				writer.WriteElementString("DefaultVersion", "urn:schemas-microsoft-com:office:excel", this._defaultVersion);
			}
			if (this._versionLastUpdate != null)
			{
				writer.WriteElementString("VersionLastUpdate", "urn:schemas-microsoft-com:office:excel", this._versionLastUpdate);
			}
			if (this._pivotFields != null)
			{
				((IWriter) this._pivotFields).WriteXml(writer);
			}
			if (this._pTLinesItems != null)
			{
				((IWriter) this._pTLinesItems).WriteXml(writer);
			}
			if (this._pTSource != null)
			{
				((IWriter) this._pTSource).WriteXml(writer);
			}
			writer.WriteEndElement();
		}

		// Properties
		public string DefaultVersion
		{
			get
			{
				return this._defaultVersion;
			}
			set
			{
				this._defaultVersion = value;
			}
		}

		public PTLineItemCollection LineItems
		{
			get
			{
				if (this._pTLinesItems == null)
				{
					this._pTLinesItems = new PTLineItemCollection();
				}
				return this._pTLinesItems;
			}
		}

		public string Location
		{
			get
			{
				return this._location;
			}
			set
			{
				this._location = value;
			}
		}

		public string Name
		{
			get
			{
				return this._name;
			}
			set
			{
				this._name = value;
			}
		}

		public PivotFieldCollection PivotFields
		{
			get
			{
				if (this._pivotFields == null)
				{
					this._pivotFields = new PivotFieldCollection();
				}
				return this._pivotFields;
			}
		}

		public PTSource Source
		{
			get
			{
				if (this._pTSource == null)
				{
					this._pTSource = new PTSource();
				}
				return this._pTSource;
			}
		}

		public string VersionLastUpdate
		{
			get
			{
				return this._versionLastUpdate;
			}
			set
			{
				this._versionLastUpdate = value;
			}
		}
	}



}
