//ZD 100147 Start
/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
//ZD 100147 End
using System;
using System.Globalization;
using System.Collections;
using System.Collections.Specialized;
using System.Xml;
using System.CodeDom;

namespace myVRM.ExcelXmlWriter.Schemas
{
	public sealed class Attribute : IWriter
	{
		// Fields
		private string _type;

		// Methods
		public Attribute()
		{
		}

		public Attribute(string type)
		{
			this._type = type;
		}

		void IWriter.WriteXml(XmlWriter writer)
		{
			writer.WriteStartElement("s", "attribute", "uuid:BDC6E3F0-6DA3-11d1-A2A3-00AA00C14882");
			if (this._type != null)
			{
				writer.WriteAttributeString("s", "type", "uuid:BDC6E3F0-6DA3-11d1-A2A3-00AA00C14882", this._type);
			}
			writer.WriteEndElement();
		}

		// Properties
		public string Type
		{
			get
			{
				return this._type;
			}
			set
			{
				this._type = value;
			}
		}
	}
	public sealed class AttributeCollection : CollectionBase, IWriter
	{
		// Methods
		internal AttributeCollection()
		{
		}

		public int Add(Attribute attribute)
		{
			return base.InnerList.Add(attribute);
		}

		public Attribute Add(string type)
		{
			Attribute attribute = new Attribute(type);
			this.Add(attribute);
			return attribute;
		}

		void IWriter.WriteXml(XmlWriter writer)
		{
			for (int i = 0; i < base.InnerList.Count; i++)
			{
				((IWriter) base.InnerList[i]).WriteXml(writer);
			}
		}

		public bool Contains(Attribute item)
		{
			return base.InnerList.Contains(item);
		}

		public void CopyTo(Attribute[] array, int index)
		{
			base.InnerList.CopyTo(array, index);
		}

		public int IndexOf(Attribute item)
		{
			return base.InnerList.IndexOf(item);
		}

		public void Insert(int index, Attribute item)
		{
			base.InnerList.Insert(index, item);
		}

		public void Remove(Attribute item)
		{
			base.InnerList.Remove(item);
		}

		// Properties
		public Attribute this[int index]
		{
			get
			{
				return (Attribute) base.InnerList[index];
			}
		}
	}

	public sealed class AttributeType : SchemaType, IWriter
	{
		// Fields
		private string _dataType;
		private string _name;
		private string _rsname;

		// Methods
		public AttributeType()
		{
		}

		public AttributeType(string name, string rowsetName, string dataType)
		{
			this._name = name;
			this._rsname = rowsetName;
			this._dataType = dataType;
		}

		void IWriter.WriteXml(XmlWriter writer)
		{
			writer.WriteStartElement("s", "AttributeType", "uuid:BDC6E3F0-6DA3-11d1-A2A3-00AA00C14882");
			if (this._name != null)
			{
				writer.WriteAttributeString("s", "name", "uuid:BDC6E3F0-6DA3-11d1-A2A3-00AA00C14882", this._name);
			}
			if (this._rsname != null)
			{
				writer.WriteAttributeString("rs", "name", "urn:schemas-microsoft-com:rowset", this._rsname);
			}
			if (this._dataType != null)
			{
				writer.WriteStartElement("dt", "datatype", "uuid:C2F41010-65B3-11d1-A29F-00AA00C14882");
				writer.WriteAttributeString("dt", "type", "uuid:C2F41010-65B3-11d1-A29F-00AA00C14882", this._dataType);
				writer.WriteEndElement();
			}
			writer.WriteEndElement();
		}

		// Properties
		public string DataType
		{
			get
			{
				return this._dataType;
			}
			set
			{
				this._dataType = value;
			}
		}

		public string Name
		{
			get
			{
				return this._name;
			}
			set
			{
				this._name = value;
			}
		}

		public string RowsetName
		{
			get
			{
				return this._rsname;
			}
			set
			{
				this._rsname = value;
			}
		}
	}



}
