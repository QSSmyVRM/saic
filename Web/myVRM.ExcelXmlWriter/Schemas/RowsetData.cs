//ZD 100147 Start
/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
//ZD 100147 End
using System;
using System.Xml;

namespace myVRM.ExcelXmlWriter.Schemas
{
	public sealed class RowsetData : IWriter
	{
		// Fields
		private RowsetRowCollection _rows;

		// Methods
		void IWriter.WriteXml(XmlWriter writer)
		{
			writer.WriteStartElement("rs", "data", "urn:schemas-microsoft-com:rowset");
			if (this._rows != null)
			{
				((IWriter) this._rows).WriteXml(writer);
			}
			writer.WriteEndElement();
		}

		// Properties
		public RowsetRowCollection Rows
		{
			get
			{
				if (this._rows == null)
				{
					this._rows = new RowsetRowCollection();
				}
				return this._rows;
			}
		}
	}


}
