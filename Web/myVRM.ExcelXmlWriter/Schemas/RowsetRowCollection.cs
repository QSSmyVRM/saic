//ZD 100147 Start
/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
//ZD 100147 End
using System;
using System.Xml;
using System.Collections;

namespace myVRM.ExcelXmlWriter.Schemas
{
	public sealed class RowsetRowCollection : CollectionBase, IWriter
	{
		// Methods
		internal RowsetRowCollection()
		{
		}

		public RowsetRow Add()
		{
			RowsetRow row = new RowsetRow();
			this.Add(row);
			return row;
		}

		public int Add(RowsetRow row)
		{
			return base.InnerList.Add(row);
		}

		void IWriter.WriteXml(XmlWriter writer)
		{
			for (int i = 0; i < base.InnerList.Count; i++)
			{
				((IWriter) base.InnerList[i]).WriteXml(writer);
			}
		}

		public bool Contains(RowsetRow item)
		{
			return base.InnerList.Contains(item);
		}

		public void CopyTo(RowsetRow[] array, int index)
		{
			base.InnerList.CopyTo(array, index);
		}

		public int IndexOf(RowsetRow item)
		{
			return base.InnerList.IndexOf(item);
		}

		public void Insert(int index, RowsetRow item)
		{
			base.InnerList.Insert(index, item);
		}

		public void Remove(RowsetRow item)
		{
			base.InnerList.Remove(item);
		}

		// Properties
		public RowsetRow this[int index]
		{
			get
			{
				return (RowsetRow) base.InnerList[index];
			}
		}
	}

	public enum SchemaContent
	{
		NotSet,
		eltOnly
	}
	
	public abstract class SchemaType
	{
		// Methods
		protected SchemaType()
		{
		}
	}

	public sealed class SchemaTypeCollection : CollectionBase, IWriter
	{
		// Methods
		internal SchemaTypeCollection()
		{
		}

		public int Add(SchemaType schemaType)
		{
			return base.InnerList.Add(schemaType);
		}

		void IWriter.WriteXml(XmlWriter writer)
		{
			for (int i = 0; i < base.InnerList.Count; i++)
			{
				((IWriter) base.InnerList[i]).WriteXml(writer);
			}
		}

		public bool Contains(SchemaType item)
		{
			return base.InnerList.Contains(item);
		}

		public void CopyTo(SchemaType[] array, int index)
		{
			base.InnerList.CopyTo(array, index);
		}

		public int IndexOf(SchemaType item)
		{
			return base.InnerList.IndexOf(item);
		}

		public void Insert(int index, SchemaType item)
		{
			base.InnerList.Insert(index, item);
		}

		public void Remove(SchemaType item)
		{
			base.InnerList.Remove(item);
		}

		// Properties
		public SchemaType this[int index]
		{
			get
			{
				return (SchemaType) base.InnerList[index];
			}
		}
	}

	public sealed class Schema : IWriter
	{
		// Fields
		private SchemaTypeCollection _types;

		// Methods
		void IWriter.WriteXml(XmlWriter writer)
		{
			writer.WriteStartElement("s", "Schema", "uuid:BDC6E3F0-6DA3-11d1-A2A3-00AA00C14882");
			if (this._types != null)
			{
				((IWriter) this._types).WriteXml(writer);
			}
			writer.WriteEndElement();
		}

		// Properties
		public SchemaTypeCollection Types
		{
			get
			{
				if (this._types == null)
				{
					this._types = new SchemaTypeCollection();
				}
				return this._types;
			}
		}
	}

	public sealed class RowsetRow : IWriter
	{
		// Fields
		private RowsetColumnCollection _columns;

		// Methods
		void IWriter.WriteXml(XmlWriter writer)
		{
			writer.WriteStartElement("z", "row", "#RowsetSchema");
			if (this._columns != null)
			{
				((IWriter) this._columns).WriteXml(writer);
			}
			writer.WriteEndElement();
		}

		// Properties
		public RowsetColumnCollection Columns
		{
			get
			{
				if (this._columns == null)
				{
					this._columns = new RowsetColumnCollection();
				}
				return this._columns;
			}
		}
	}

	public sealed class RowsetColumn : IWriter
	{
		// Fields
		private string _name;
		private string _value;

		// Methods
		public RowsetColumn(string name, string value)
		{
			this._name = name;
			this._value = value;
		}

		void IWriter.WriteXml(XmlWriter writer)
		{
			writer.WriteAttributeString("z", this._name, "#RowsetSchema", this._value);
		}

		// Properties
		public string Name
		{
			get
			{
				return this._name;
			}
			set
			{
				this._name = value;
			}
		}

		public string Value
		{
			get
			{
				return this._value;
			}
			set
			{
				this._value = value;
			}
		}
	}

	public sealed class RowsetColumnCollection : CollectionBase, IWriter
	{
		// Methods
		internal RowsetColumnCollection()
		{
		}

		public int Add(RowsetColumn column)
		{
			return base.InnerList.Add(column);
		}

		void IWriter.WriteXml(XmlWriter writer)
		{
			for (int i = 0; i < base.InnerList.Count; i++)
			{
				((IWriter) base.InnerList[i]).WriteXml(writer);
			}
		}

		public bool Contains(RowsetColumn item)
		{
			return base.InnerList.Contains(item);
		}

		public void CopyTo(RowsetColumn[] array, int index)
		{
			base.InnerList.CopyTo(array, index);
		}

		public int IndexOf(RowsetColumn item)
		{
			return base.InnerList.IndexOf(item);
		}

		public void Insert(int index, RowsetColumn item)
		{
			base.InnerList.Insert(index, item);
		}

		public void Remove(RowsetColumn item)
		{
			base.InnerList.Remove(item);
		}

		// Properties
		public RowsetColumn this[int index]
		{
			get
			{
				return (RowsetColumn) base.InnerList[index];
			}
		}
	}

}
