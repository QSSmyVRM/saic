//ZD 100147 Start
/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
//ZD 100147 End
using System;
using System.Collections;
using System.Collections.Specialized;
using System.Xml;
using System.CodeDom;

namespace myVRM.ExcelXmlWriter
{
	public sealed class SupBook : IWriter, IReader, ICodeWriter
	{
		// Fields
		private StringCollection _externNames;
		private string _path;
		private XctCollection _references;
		private StringCollection _sheetNames;

		// Methods
		void ICodeWriter.WriteTo(CodeTypeDeclaration type, CodeMemberMethod method, CodeExpression targetObject)
		{
			if (this._path != null)
			{
				Util.AddAssignment(method, targetObject, "Path", this._path);
			}
			if (this._sheetNames != null)
			{
				foreach (string str in this._sheetNames)
				{
					method.Statements.Add(new CodeMethodInvokeExpression(new CodePropertyReferenceExpression(targetObject, "SheetNames"), "Add", new CodeExpression[] { new CodePrimitiveExpression(str) }));
				}
			}
			if (this._externNames != null)
			{
				foreach (string str2 in this._externNames)
				{
					method.Statements.Add(new CodeMethodInvokeExpression(new CodePropertyReferenceExpression(targetObject, "ExternNames"), "Add", new CodeExpression[] { new CodePrimitiveExpression(str2) }));
				}
			}
			if (this._references != null)
			{
				((ICodeWriter) this._references).WriteTo(type, method, targetObject);
			}
		}

		void IReader.ReadXml(XmlElement element)
		{
			if (!IsElement(element))
			{
				throw new ArgumentException("Invalid element", "element");
			}
			foreach (XmlNode node in element.ChildNodes)
			{
				XmlElement element2 = node as XmlElement;
				if (element2 != null)
				{
					if (Util.IsElement(element2, "Path", "urn:schemas-microsoft-com:office:excel"))
					{
						this._path = element2.InnerText;
					}
					else
					{
						if (Util.IsElement(element2, "SheetName", "urn:schemas-microsoft-com:office:excel"))
						{
							this.SheetNames.Add(element2.InnerText);
							continue;
						}
						if (Util.IsElement(element2, "ExternName", "urn:schemas-microsoft-com:office:excel"))
						{
							this.ExternNames.Add(element2.InnerText);
							continue;
						}
						if (Xct.IsElement(element2))
						{
							Xct item = new Xct();
							((IReader) item).ReadXml(element2);
							this.References.Add(item);
						}
					}
				}
			}
		}

		void IWriter.WriteXml(XmlWriter writer)
		{
			writer.WriteStartElement("x", "SupBook", "urn:schemas-microsoft-com:office:excel");
			if (this._path != null)
			{
				writer.WriteElementString("Path", "urn:schemas-microsoft-com:office:excel", this._path);
			}
			if (this._sheetNames != null)
			{
				foreach (string str in this._sheetNames)
				{
					writer.WriteElementString("SheetName", "urn:schemas-microsoft-com:office:excel", str);
				}
			}
			if (this._externNames != null)
			{
				foreach (string str2 in this._externNames)
				{
					writer.WriteStartElement("ExternName", "urn:schemas-microsoft-com:office:excel");
					writer.WriteElementString("Name", "urn:schemas-microsoft-com:office:excel", str2);
					writer.WriteEndElement();
				}
			}
			if (this._references != null)
			{
				((IWriter) this._references).WriteXml(writer);
			}
			writer.WriteEndElement();
		}

		internal static bool IsElement(XmlElement element)
		{
			return Util.IsElement(element, "SupBook", "urn:schemas-microsoft-com:office:excel");
		}

		// Properties
		public StringCollection ExternNames
		{
			get
			{
				if (this._externNames == null)
				{
					this._externNames = new StringCollection();
				}
				return this._externNames;
			}
		}

		public string Path
		{
			get
			{
				return this._path;
			}
			set
			{
				this._path = value;
			}
		}

		public XctCollection References
		{
			get
			{
				if (this._references == null)
				{
					this._references = new XctCollection();
				}
				return this._references;
			}
		}

		public StringCollection SheetNames
		{
			get
			{
				if (this._sheetNames == null)
				{
					this._sheetNames = new StringCollection();
				}
				return this._sheetNames;
			}
		}
	}
}
