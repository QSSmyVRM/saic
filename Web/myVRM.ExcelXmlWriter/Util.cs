//ZD 100147 Start
/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
//ZD 100147 End
using System;
using System.Globalization;
using System.Text;
using System.Reflection;
using System.ComponentModel;
using System.ComponentModel.Design.Serialization;
using System.Collections;
using System.Xml;
using System.CodeDom;


namespace myVRM.ExcelXmlWriter
{
	internal sealed class Util
	{
		// Methods
		private Util()
		{
		}

		public static void AddAssignment(CodeMemberMethod method, CodeExpression targetObject, string property, bool value)
		{
			method.Statements.Add(GetAssignment(targetObject, property, value));
		}

		public static void AddAssignment(CodeMemberMethod method, CodeExpression targetObject, string property, DateTime value)
		{
			method.Statements.Add(GetAssignment(targetObject, property, value));
		}

		public static void AddAssignment(CodeMemberMethod method, CodeExpression targetObject, string property, Enum enumValue)
		{
			method.Statements.Add(new CodeAssignStatement(new CodePropertyReferenceExpression(targetObject, property), GetRightExpressionForValue(enumValue, enumValue.GetType())));
		}

		public static void AddAssignment(CodeMemberMethod method, CodeExpression targetObject, string property, int value)
		{
			method.Statements.Add(GetAssignment(targetObject, property, value));
		}

		public static void AddAssignment(CodeMemberMethod method, CodeExpression targetObject, string property, float value)
		{
			method.Statements.Add(new CodeAssignStatement(new CodePropertyReferenceExpression(targetObject, property), GetRightExpressionForValue(value, typeof(float))));
		}

		public static void AddAssignment(CodeMemberMethod method, CodeExpression targetObject, string property, string value)
		{
			method.Statements.Add(GetAssignment(targetObject, property, value));
		}

		public static void AddComment(CodeMemberMethod method, string text)
		{
			AddCommentSeparator(method);
			method.Statements.Add(new CodeCommentStatement(" " + text));
			AddCommentSeparator(method);
		}

		public static void AddCommentSeparator(CodeMemberMethod method)
		{
			method.Statements.Add(new CodeCommentStatement("-----------------------------------------------"));
		}

		internal static string CreateSafeName(string strName, string prefix)
		{
			StringBuilder builder = new StringBuilder();
			for (int i = 0; i < strName.Length; i++)
			{
				char ch = strName[i];
				if ((((ch >= 'A') && (ch <= 'Z')) || ((ch >= 'a') && (ch <= 'z'))) || ((ch == '_') || ((ch >= '0') && (ch <= '9'))))
				{
					builder.Append(ch);
				}
			}
			if (builder.Length == 0)
			{
				return prefix;
			}
			if ((builder[0] >= '0') && (builder[0] <= '9'))
			{
				return (prefix + builder.ToString());
			}
			return builder.ToString();
		}

		private static CodeAssignStatement GetAssignment(CodeExpression targetObject, string property, bool value)
		{
			return new CodeAssignStatement(new CodePropertyReferenceExpression(targetObject, property), GetRightExpressionForValue(value, typeof(bool)));
		}

		private static CodeAssignStatement GetAssignment(CodeExpression targetObject, string property, DateTime value)
		{
			return new CodeAssignStatement(new CodePropertyReferenceExpression(targetObject, property), GetRightExpressionForValue(value, typeof(DateTime)));
		}

		private static CodeAssignStatement GetAssignment(CodeExpression targetObject, string property, int value)
		{
			return new CodeAssignStatement(new CodePropertyReferenceExpression(targetObject, property), GetRightExpressionForValue(value, typeof(int)));
		}

		private static CodeAssignStatement GetAssignment(CodeExpression targetObject, string property, string value)
		{
			return new CodeAssignStatement(new CodePropertyReferenceExpression(targetObject, property), GetRightExpressionForValue(value, typeof(string)));
		}

		public static string GetAttribute(XmlElement element, string name, string ns)
		{
			XmlAttribute attributeNode = element.GetAttributeNode(name, ns);
			if (attributeNode == null)
			{
				return null;
			}
			return attributeNode.Value;
		}

		public static bool GetAttribute(XmlElement element, string name, string ns, bool defaultValue)
		{
			string attribute = element.GetAttribute(name, ns);
			if ((attribute == null) || (attribute.Length == 0))
			{
				return defaultValue;
			}
			return (attribute == "1");
		}

		public static int GetAttribute(XmlElement element, string name, string ns, int defaultValue)
		{
			string attribute = element.GetAttribute(name, ns);
			if ((attribute == null) || (attribute.Length == 0))
			{
				return defaultValue;
			}
			int num = defaultValue;
			try
			{
				int index = attribute.IndexOf('.');
				if (index != -1)
				{
					attribute = attribute.Substring(0, index);
				}
				num = int.Parse(attribute, CultureInfo.InvariantCulture);
			}
			catch
			{
			}
			return num;
		}

		public static float GetAttribute(XmlElement element, string name, string ns, float defaultValue)
		{
			string attribute = element.GetAttribute(name, ns);
			if ((attribute == null) || (attribute.Length == 0))
			{
				return defaultValue;
			}
			float num = defaultValue;
			try
			{
				num = float.Parse(attribute, CultureInfo.InvariantCulture);
			}
			catch
			{
			}
			return num;
		}

		public static CodeExpression GetRightExpressionForValue(object value, Type valueType)
		{
			if (value is DBNull)
			{
				if (valueType == typeof(decimal))
				{
					return new CodePrimitiveExpression(null);
				}
				return new CodePrimitiveExpression(null);
			}
			if ((valueType == typeof(string)) && (value is string))
			{
				return new CodePrimitiveExpression((string) value);
			}
			if (valueType.IsPrimitive)
			{
				if (valueType != value.GetType())
				{
					TypeConverter converter = TypeDescriptor.GetConverter(valueType);
					if (converter.CanConvertFrom(value.GetType()))
					{
						return new CodePrimitiveExpression(converter.ConvertFrom(value));
					}
				}
				return new CodePrimitiveExpression(value);
			}
			if (valueType.IsArray)
			{
				Array array = (Array) value;
				CodeArrayCreateExpression expression = new CodeArrayCreateExpression();
				expression.CreateType = new CodeTypeReference(valueType.GetElementType());
				if (array != null)
				{
					foreach (object obj2 in array)
					{
						expression.Initializers.Add(GetRightExpressionForValue(obj2, valueType.GetElementType()));
					}
				}
				return expression;
			}
			TypeConverter converter2 = null;
			converter2 = TypeDescriptor.GetConverter(valueType);
			if (valueType.IsEnum && (value is string))
			{
				value = converter2.ConvertFromString(value.ToString());
			}
			if (converter2 != null)
			{
				InstanceDescriptor descriptor = null;
				if (converter2.CanConvertTo(typeof(InstanceDescriptor)))
				{
					descriptor = (InstanceDescriptor) converter2.ConvertTo(value, typeof(InstanceDescriptor));
				}
				if (descriptor != null)
				{
					if (descriptor.MemberInfo is FieldInfo)
					{
						return new CodeFieldReferenceExpression(new CodeTypeReferenceExpression(descriptor.MemberInfo.DeclaringType.FullName), descriptor.MemberInfo.Name);
					}
					if (descriptor.MemberInfo is PropertyInfo)
					{
						return new CodePropertyReferenceExpression(new CodeTypeReferenceExpression(descriptor.MemberInfo.DeclaringType.FullName), descriptor.MemberInfo.Name);
					}
					object[] objArray = new object[descriptor.Arguments.Count];
					descriptor.Arguments.CopyTo(objArray, 0);
					CodeExpression[] expressionArray = new CodeExpression[objArray.Length];
					if (descriptor.MemberInfo is MethodInfo)
					{
						ParameterInfo[] parameters = ((MethodInfo) descriptor.MemberInfo).GetParameters();
						for (int i = 0; i < objArray.Length; i++)
						{
							expressionArray[i] = GetRightExpressionForValue(objArray[i], parameters[i].ParameterType);
						}
						CodeMethodInvokeExpression expression4 = new CodeMethodInvokeExpression(new CodeTypeReferenceExpression(descriptor.MemberInfo.DeclaringType.FullName), descriptor.MemberInfo.Name, new CodeExpression[0]);
						foreach (CodeExpression expression5 in expressionArray)
						{
							expression4.Parameters.Add(expression5);
						}
						return expression4;
					}
					if (descriptor.MemberInfo is ConstructorInfo)
					{
						ParameterInfo[] infoArray2 = ((ConstructorInfo) descriptor.MemberInfo).GetParameters();
						for (int j = 0; j < objArray.Length; j++)
						{
							expressionArray[j] = GetRightExpressionForValue(objArray[j], infoArray2[j].ParameterType);
						}
						CodeObjectCreateExpression expression6 = new CodeObjectCreateExpression(descriptor.MemberInfo.DeclaringType.FullName, new CodeExpression[0]);
						foreach (CodeExpression expression7 in expressionArray)
						{
							expression6.Parameters.Add(expression7);
						}
						return expression6;
					}
				}
			}
			return null;
		}

		public static bool IsElement(XmlElement element, string name, string ns)
		{
			return (((element != null) && (element.LocalName == name)) && (element.NamespaceURI == ns));
		}

		public static void Traverse(CodeTypeDeclaration type, ICodeWriter o, CodeMemberMethod method, CodeExpression targetObject, string propertyName)
		{
			o.WriteTo(type, method, new CodePropertyReferenceExpression(targetObject, propertyName));
		}

		public static void WriteElementString(XmlWriter writer, string elementName, string prefix, bool value)
		{
			if (value)
			{
				writer.WriteElementString(elementName, prefix, "True");
			}
			else
			{
				writer.WriteElementString(elementName, prefix, "False");
			}
		}
	}


}
