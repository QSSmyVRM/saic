//ZD 100147 Start
/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
//ZD 100147 End
using System;
using System.ComponentModel;
using System.Globalization;
using System.Collections;
using System.Collections.Specialized;
using System.Xml;
using System.CodeDom;
using myVRM.ExcelXmlWriter.Schemas;


namespace myVRM.ExcelXmlWriter
{
	public sealed class WorksheetStyle : IWriter, IReader, ICodeWriter
	{
		// Fields
		private WorksheetStyleAlignment _alignment;
		internal Workbook _book;
		private WorksheetStyleBorderCollection _borders;
		private WorksheetStyleFont _font;
		private string _id;
		private WorksheetStyleInterior _interior;
		private string _name;
		private string _numberFormat = "General";
		private string _parent;

		// Methods
		public WorksheetStyle(string id)
		{
			this._id = id;
		}

		void ICodeWriter.WriteTo(CodeTypeDeclaration type, CodeMemberMethod method, CodeExpression targetObject)
		{
			if (this._name != null)
			{
				Util.AddAssignment(method, targetObject, "Name", this._name);
			}
			if (this._parent != null)
			{
				Util.AddAssignment(method, targetObject, "Parent", this._parent);
			}
			if (this._font != null)
			{
				Util.Traverse(type, this._font, method, targetObject, "Font");
			}
			if (this._interior != null)
			{
				Util.Traverse(type, this._interior, method, targetObject, "Interior");
			}
			if (this._alignment != null)
			{
				Util.Traverse(type, this._alignment, method, targetObject, "Alignment");
			}
			if (this._borders != null)
			{
				Util.Traverse(type, this._borders, method, targetObject, "Borders");
			}
			if (this._numberFormat != "General")
			{
				Util.AddAssignment(method, targetObject, "NumberFormat", this._numberFormat);
			}
		}

		void IReader.ReadXml(XmlElement element)
		{
			if (!IsElement(element))
			{
				throw new ArgumentException("Invalid element", "element");
			}
			this._id = Util.GetAttribute(element, "ID", "urn:schemas-microsoft-com:office:spreadsheet");
			this._name = Util.GetAttribute(element, "Name", "urn:schemas-microsoft-com:office:spreadsheet");
			this._parent = Util.GetAttribute(element, "Parent", "urn:schemas-microsoft-com:office:spreadsheet");
			foreach (XmlNode node in element.ChildNodes)
			{
				XmlElement element2 = node as XmlElement;
				if (element2 != null)
				{
					if (WorksheetStyleFont.IsElement(element2))
					{
						((IReader) this.Font).ReadXml(element2);
					}
					else
					{
						if (WorksheetStyleInterior.IsElement(element2))
						{
							((IReader) this.Interior).ReadXml(element2);
							continue;
						}
						if (WorksheetStyleAlignment.IsElement(element2))
						{
							((IReader) this.Alignment).ReadXml(element2);
							continue;
						}
						if (WorksheetStyleBorderCollection.IsElement(element2))
						{
							((IReader) this.Borders).ReadXml(element2);
							continue;
						}
						if (Util.IsElement(element2, "NumberFormat", "urn:schemas-microsoft-com:office:spreadsheet"))
						{
							string attribute = element2.GetAttribute("Format", "urn:schemas-microsoft-com:office:spreadsheet");
							if ((attribute != null) && (attribute.Length > 0))
							{
								this._numberFormat = attribute;
							}
						}
					}
				}
			}
		}

		void IWriter.WriteXml(XmlWriter writer)
		{
			writer.WriteStartElement("Style", "urn:schemas-microsoft-com:office:spreadsheet");
			if (this._id != null)
			{
				writer.WriteAttributeString("ID", "urn:schemas-microsoft-com:office:spreadsheet", this._id);
			}
			if (this._name != null)
			{
				writer.WriteAttributeString("Name", "urn:schemas-microsoft-com:office:spreadsheet", this._name);
			}
			if (this._parent != null)
			{
				writer.WriteAttributeString("Parent", "urn:schemas-microsoft-com:office:spreadsheet", this._parent);
			}
			if (this._alignment != null)
			{
				((IWriter) this._alignment).WriteXml(writer);
			}
			if (this._borders != null)
			{
				((IWriter) this._borders).WriteXml(writer);
			}
			if (this._font != null)
			{
				((IWriter) this._font).WriteXml(writer);
			}
			if (this._interior != null)
			{
				((IWriter) this._interior).WriteXml(writer);
			}
			if (this._numberFormat != "General")
			{
				writer.WriteStartElement("NumberFormat", "urn:schemas-microsoft-com:office:spreadsheet");
				writer.WriteAttributeString("s", "Format", "urn:schemas-microsoft-com:office:spreadsheet", this._numberFormat);
				writer.WriteEndElement();
			}
			writer.WriteEndElement();
		}

		internal static bool IsElement(XmlElement element)
		{
			return Util.IsElement(element, "Style", "urn:schemas-microsoft-com:office:spreadsheet");
		}

		// Properties
		public WorksheetStyleAlignment Alignment
		{
			get
			{
				if (this._alignment == null)
				{
					this._alignment = new WorksheetStyleAlignment();
				}
				return this._alignment;
			}
            set
            {
                this._alignment = value;
            }
		}

		public WorksheetStyleBorderCollection Borders
		{
			get
			{
				if (this._borders == null)
				{
					this._borders = new WorksheetStyleBorderCollection(this);
				}
				return this._borders;
			}
		}

		public WorksheetStyleFont Font
		{
			get
			{
				if (this._font == null)
				{
					this._font = new WorksheetStyleFont();
				}
				return this._font;
			}
		}

		public string ID
		{
			get
			{
				return this._id;
			}
		}

		public WorksheetStyleInterior Interior
		{
			get
			{
				if (this._interior == null)
				{
					this._interior = new WorksheetStyleInterior();
				}
				return this._interior;
			}
		}

		public string Name
		{
			get
			{
				return this._name;
			}
			set
			{
				this._name = value;
			}
		}

		public string NumberFormat
		{
			get
			{
				return this._numberFormat;
			}
			set
			{
				this._numberFormat = value;
			}
		}

		public string Parent
		{
			get
			{
				return this._parent;
			}
			set
			{
				this._parent = value;
			}
		}

		public Workbook Workbook
		{
			get
			{
				return this._book;
			}
		}
	}

	public sealed class WorksheetStyleAlignment : IWriter, IReader, ICodeWriter
	{
		// Fields
		private StyleHorizontalAlignment _horizontal;
		private int _indent;
		private StyleReadingOrder _readingOrder;
		private int _rotate;
		private bool _shrinkToFit;
		private StyleVerticalAlignment _vertical;
		private bool _verticalText;
		private bool _wrapText;

		// Methods
		internal WorksheetStyleAlignment()
		{
		}

		void ICodeWriter.WriteTo(CodeTypeDeclaration type, CodeMemberMethod method, CodeExpression targetObject)
		{
			if (this._horizontal != StyleHorizontalAlignment.Automatic)
			{
				Util.AddAssignment(method, targetObject, "Horizontal", this._horizontal);
			}
			if (this._indent != 0)
			{
				Util.AddAssignment(method, targetObject, "Indent", this._indent);
			}
			if (this._rotate != 0)
			{
				Util.AddAssignment(method, targetObject, "Rotate", this._rotate);
			}
			if (this._shrinkToFit)
			{
				Util.AddAssignment(method, targetObject, "ShrinkToFit", this._shrinkToFit);
			}
			if (this._vertical != StyleVerticalAlignment.Automatic)
			{
				Util.AddAssignment(method, targetObject, "Vertical", this._vertical);
			}
			if (this._verticalText)
			{
				Util.AddAssignment(method, targetObject, "VerticalText", this._verticalText);
			}
			if (this._wrapText)
			{
				Util.AddAssignment(method, targetObject, "WrapText", this._wrapText);
			}
			if (this._readingOrder != StyleReadingOrder.NotSet)
			{
				Util.AddAssignment(method, targetObject, "ReadingOrder", this._readingOrder);
			}
		}

		void IReader.ReadXml(XmlElement element)
		{
			if (!IsElement(element))
			{
				throw new ArgumentException("Invalid element", "element");
			}
			string attribute = element.GetAttribute("Horizontal", "urn:schemas-microsoft-com:office:spreadsheet");
			if ((attribute != null) && (attribute.Length != 0))
			{
				this._horizontal = (StyleHorizontalAlignment) Enum.Parse(typeof(StyleHorizontalAlignment), attribute, true);
			}
			this._indent = Util.GetAttribute(element, "Indent", "urn:schemas-microsoft-com:office:spreadsheet", 0);
			this._rotate = Util.GetAttribute(element, "Rotate", "urn:schemas-microsoft-com:office:spreadsheet", 0);
			this._shrinkToFit = Util.GetAttribute(element, "ShrinkToFit", "urn:schemas-microsoft-com:office:spreadsheet", false);
			attribute = element.GetAttribute("Vertical", "urn:schemas-microsoft-com:office:spreadsheet");
			if ((attribute != null) && (attribute.Length != 0))
			{
				this._vertical = (StyleVerticalAlignment) Enum.Parse(typeof(StyleVerticalAlignment), attribute, true);
			}
			attribute = element.GetAttribute("ReadingOrder", "urn:schemas-microsoft-com:office:spreadsheet");
			if ((attribute != null) && (attribute.Length != 0))
			{
				this._readingOrder = (StyleReadingOrder) Enum.Parse(typeof(StyleReadingOrder), attribute, true);
			}
			this._verticalText = element.GetAttribute("VerticalText", "urn:schemas-microsoft-com:office:spreadsheet") == "1";
			this._wrapText = element.GetAttribute("WrapText", "urn:schemas-microsoft-com:office:spreadsheet") == "1";
		}

		void IWriter.WriteXml(XmlWriter writer)
		{
			writer.WriteStartElement("s", "Alignment", "urn:schemas-microsoft-com:office:spreadsheet");
			if (this._horizontal != StyleHorizontalAlignment.Automatic)
			{
				writer.WriteAttributeString("Horizontal", "urn:schemas-microsoft-com:office:spreadsheet", this._horizontal.ToString(CultureInfo.InvariantCulture));
			}
			if (this._indent != 0)
			{
				writer.WriteAttributeString("Indent", "urn:schemas-microsoft-com:office:spreadsheet", this._indent.ToString(CultureInfo.InvariantCulture));
			}
			if (this._rotate != 0)
			{
				writer.WriteAttributeString("Rotate", "urn:schemas-microsoft-com:office:spreadsheet", this._rotate.ToString(CultureInfo.InvariantCulture));
			}
			if (this._shrinkToFit)
			{
				writer.WriteAttributeString("ShrinkToFit", "urn:schemas-microsoft-com:office:spreadsheet", "1");
			}
			if (this._vertical != StyleVerticalAlignment.Automatic)
			{
				writer.WriteAttributeString("Vertical", "urn:schemas-microsoft-com:office:spreadsheet", this._vertical.ToString(CultureInfo.InvariantCulture));
			}
			if (this._verticalText)
			{
				writer.WriteAttributeString("VerticalText", "urn:schemas-microsoft-com:office:spreadsheet", "1");
			}
			if (this._wrapText)
			{
				writer.WriteAttributeString("WrapText", "urn:schemas-microsoft-com:office:spreadsheet", "1");
			}
			if (this._readingOrder != StyleReadingOrder.NotSet)
			{
				writer.WriteAttributeString("ReadingOrder", "urn:schemas-microsoft-com:office:spreadsheet", this._readingOrder.ToString(CultureInfo.InvariantCulture));
			}
			writer.WriteEndElement();
		}

		internal static bool IsElement(XmlElement element)
		{
			return Util.IsElement(element, "Alignment", "urn:schemas-microsoft-com:office:spreadsheet");
		}

		// Properties
		public StyleHorizontalAlignment Horizontal
		{
			get
			{
				return this._horizontal;
			}
			set
			{
				this._horizontal = value;
			}
		}

		public int Indent
		{
			get
			{
				return this._indent;
			}
			set
			{
				this._indent = value;
			}
		}

		public StyleReadingOrder ReadingOrder
		{
			get
			{
				return this._readingOrder;
			}
			set
			{
				this._readingOrder = value;
			}
		}

		public int Rotate
		{
			get
			{
				return this._rotate;
			}
			set
			{
				this._rotate = value;
			}
		}

		public bool ShrinkToFit
		{
			get
			{
				return this._shrinkToFit;
			}
			set
			{
				this._shrinkToFit = value;
			}
		}

		public StyleVerticalAlignment Vertical
		{
			get
			{
				return this._vertical;
			}
			set
			{
				this._vertical = value;
			}
		}

		public bool VerticalText
		{
			get
			{
				return this._verticalText;
			}
			set
			{
				this._verticalText = value;
			}
		}

		public bool WrapText
		{
			get
			{
				return this._wrapText;
			}
			set
			{
				this._wrapText = value;
			}
		}
	}

	public sealed class WorksheetStyleBorder : IWriter, IReader, ICodeWriter
	{
		// Fields
		private string _color;
		private LineStyleOption _lineStyle;
		private StylePosition _position;
		private int _weight = -1;

		// Methods
		internal WorksheetStyleBorder()
		{
		}

		void ICodeWriter.WriteTo(CodeTypeDeclaration type, CodeMemberMethod method, CodeExpression targetObject)
		{
			if (this._position != StylePosition.NotSet)
			{
				Util.AddAssignment(method, targetObject, "Position", this._position);
			}
			if (this._weight != -1)
			{
				Util.AddAssignment(method, targetObject, "Weight", this._weight);
			}
			if (this._color != null)
			{
				Util.AddAssignment(method, targetObject, "Color", this._color);
			}
			if (this._lineStyle != LineStyleOption.NotSet)
			{
				Util.AddAssignment(method, targetObject, "LineStyle", this._lineStyle);
			}
		}

		void IReader.ReadXml(XmlElement element)
		{
			if (!IsElement(element))
			{
				throw new ArgumentException("Invalid element", "element");
			}
			this._color = Util.GetAttribute(element, "Color", "urn:schemas-microsoft-com:office:spreadsheet");
			string attribute = element.GetAttribute("Position", "urn:schemas-microsoft-com:office:spreadsheet");
			if ((attribute != null) && (attribute.Length != 0))
			{
				this._position = (StylePosition) Enum.Parse(typeof(StylePosition), attribute, true);
			}
			this._weight = Util.GetAttribute(element, "Weight", "urn:schemas-microsoft-com:office:spreadsheet", -1);
			this._color = Util.GetAttribute(element, "Color", "urn:schemas-microsoft-com:office:spreadsheet");
			attribute = element.GetAttribute("LineStyle", "urn:schemas-microsoft-com:office:spreadsheet");
			if ((attribute != null) && (attribute.Length != 0))
			{
				this._lineStyle = (LineStyleOption) Enum.Parse(typeof(LineStyleOption), attribute, true);
			}
		}

		void IWriter.WriteXml(XmlWriter writer)
		{
			writer.WriteStartElement("s", "Border", "urn:schemas-microsoft-com:office:spreadsheet");
			if (this._color != null)
			{
				writer.WriteAttributeString("s", "Color", "urn:schemas-microsoft-com:office:spreadsheet", this._color);
			}
			if (this._position != StylePosition.NotSet)
			{
				writer.WriteAttributeString("s", "Position", "urn:schemas-microsoft-com:office:spreadsheet", this._position.ToString(CultureInfo.InvariantCulture));
			}
			if (this._lineStyle != LineStyleOption.NotSet)
			{
				writer.WriteAttributeString("s", "LineStyle", "urn:schemas-microsoft-com:office:spreadsheet", this._lineStyle.ToString(CultureInfo.InvariantCulture));
			}
			if (this._weight != -1)
			{
				writer.WriteAttributeString("s", "Weight", "urn:schemas-microsoft-com:office:spreadsheet", this._weight.ToString(CultureInfo.InvariantCulture));
			}
			writer.WriteEndElement();
		}

		internal static bool IsElement(XmlElement element)
		{
			return Util.IsElement(element, "Border", "urn:schemas-microsoft-com:office:spreadsheet");
		}

		internal int IsSpecial()
		{
			if (((this._position == StylePosition.NotSet) || (this._weight == -1)) || (this._lineStyle == LineStyleOption.NotSet))
			{
				return 2;
			}
			if (this._color == null)
			{
				return 0;
			}
			return 1;
		}

		// Properties
		public string Color
		{
			get
			{
				return this._color;
			}
			set
			{
				this._color = value;
			}
		}

		public LineStyleOption LineStyle
		{
			get
			{
				return this._lineStyle;
			}
			set
			{
				this._lineStyle = value;
			}
		}

		public StylePosition Position
		{
			get
			{
				return this._position;
			}
			set
			{
				this._position = value;
			}
		}

		public int Weight
		{
			get
			{
				return this._weight;
			}
			set
			{
				if (value >= 3)
				{
					this._weight = 3;
				}
				else if (value <= 0)
				{
					this._weight = 0;
				}
				else
				{
					this._weight = value;
				}
			}
		}
	}

	public sealed class WorksheetStyleBorderCollection : CollectionBase, IWriter, IReader, ICodeWriter
	{
		// Fields
		private WorksheetStyle _style;

		// Methods
		internal WorksheetStyleBorderCollection(WorksheetStyle style)
		{
			this._style = style;
		}

		public WorksheetStyleBorder Add()
		{
			WorksheetStyleBorder border = new WorksheetStyleBorder();
			this.Add(border);
			return border;
		}

		public int Add(WorksheetStyleBorder border)
		{
			return base.InnerList.Add(border);
		}

		public WorksheetStyleBorder Add(StylePosition position, LineStyleOption lineStyle)
		{
			return this.Add(position, lineStyle, 0, null);
		}

		public WorksheetStyleBorder Add(StylePosition position, LineStyleOption lineStyle, int weight)
		{
			return this.Add(position, lineStyle, weight, null);
		}

		public WorksheetStyleBorder Add(StylePosition position, LineStyleOption lineStyle, int weight, string color)
		{
			WorksheetStyleBorder border = new WorksheetStyleBorder();
			border.Position = position;
			border.LineStyle = lineStyle;
			border.Weight = weight;
			border.Color = color;
			this.Add(border);
			return border;
		}

		void ICodeWriter.WriteTo(CodeTypeDeclaration type, CodeMemberMethod method, CodeExpression targetObject)
		{
			for (int i = 0; i < base.InnerList.Count; i++)
			{
				WorksheetStyleBorder border = this[i];
				switch (border.IsSpecial())
				{
					case 0:
						method.Statements.Add(new CodeMethodInvokeExpression(targetObject, "Add", new CodeExpression[] { Util.GetRightExpressionForValue(border.Position, typeof(StylePosition)), Util.GetRightExpressionForValue(border.LineStyle, typeof(LineStyleOption)), new CodePrimitiveExpression(border.Weight) }));
						break;

					case 1:
						method.Statements.Add(new CodeMethodInvokeExpression(targetObject, "Add", new CodeExpression[] { Util.GetRightExpressionForValue(border.Position, typeof(StylePosition)), Util.GetRightExpressionForValue(border.LineStyle, typeof(LineStyleOption)), new CodePrimitiveExpression(border.Weight), new CodePrimitiveExpression(border.Color) }));
						break;

					default:
					{
						string name = this._style.ID + "Border" + i.ToString(CultureInfo.InvariantCulture);
						CodeVariableDeclarationStatement statement = new CodeVariableDeclarationStatement(typeof(WorksheetStyleBorder), name, new CodeMethodInvokeExpression(targetObject, "Add", new CodeExpression[0]));
						method.Statements.Add(statement);
						((ICodeWriter) border).WriteTo(type, method, new CodeVariableReferenceExpression(name));
						break;
					}
				}
			}
		}

		void IReader.ReadXml(XmlElement element)
		{
			if (!IsElement(element))
			{
				throw new ArgumentException("Invalid element", "element");
			}
			foreach (XmlNode node in element.ChildNodes)
			{
				XmlElement element2 = node as XmlElement;
				if ((element2 != null) && WorksheetStyleBorder.IsElement(element2))
				{
					WorksheetStyleBorder border = new WorksheetStyleBorder();
					((IReader) border).ReadXml(element2);
					this.Add(border);
				}
			}
		}

		void IWriter.WriteXml(XmlWriter writer)
		{
			writer.WriteStartElement("s", "Borders", "urn:schemas-microsoft-com:office:spreadsheet");
			for (int i = 0; i < base.InnerList.Count; i++)
			{
				((IWriter) base.InnerList[i]).WriteXml(writer);
			}
			writer.WriteEndElement();
		}

		public bool Contains(WorksheetStyleBorder item)
		{
			return base.InnerList.Contains(item);
		}

		public void CopyTo(WorksheetStyleBorder[] array, int index)
		{
			base.InnerList.CopyTo(array, index);
		}

		public int IndexOf(WorksheetStyleBorder item)
		{
			return base.InnerList.IndexOf(item);
		}

		public void Insert(int index, WorksheetStyleBorder border)
		{
			base.InnerList.Insert(index, border);
		}

		internal static bool IsElement(XmlElement element)
		{
			return Util.IsElement(element, "Borders", "urn:schemas-microsoft-com:office:spreadsheet");
		}

		public void Remove(WorksheetStyleBorder border)
		{
			base.InnerList.Remove(border);
		}

		public object[] ToArray()
		{
			return base.InnerList.ToArray();
		}

		// Properties
		public WorksheetStyleBorder this[int index]
		{
			get
			{
				return (WorksheetStyleBorder) base.InnerList[index];
			}
			set
			{
				base.InnerList[index] = value;
			}
		}
	}

	public sealed class WorksheetStyleCollection : CollectionBase, IWriter, IReader, ICodeWriter
	{
		// Fields
		internal Workbook _book;

		// Methods
		internal WorksheetStyleCollection(Workbook book)
		{
			if (book == null)
			{
				throw new ArgumentNullException("book");
			}
			this._book = book;
		}

		public int Add(WorksheetStyle style)
		{
			if (style == null)
			{
				throw new ArgumentNullException("style");
			}
			style._book = this._book;
			return base.InnerList.Add(style);
		}

		public WorksheetStyle Add(string id)
		{
			WorksheetStyle style = new WorksheetStyle(id);
			this.Add(style);
			return style;
		}

		void ICodeWriter.WriteTo(CodeTypeDeclaration type, CodeMemberMethod method, CodeExpression targetObject)
		{
			CodeMemberMethod method2 = new CodeMemberMethod();
			method2.Name = "GenerateStyles";
			method2.Parameters.Add(new CodeParameterDeclarationExpression(typeof(WorksheetStyleCollection), "styles"));
			Util.AddComment(method, "Generate Styles");
			method.Statements.Add(new CodeMethodInvokeExpression(new CodeThisReferenceExpression(), method2.Name, new CodeExpression[] { targetObject }));
			type.Members.Add(method2);
			for (int i = 0; i < base.InnerList.Count; i++)
			{
				WorksheetStyle style = this[i];
				string name = Util.CreateSafeName(style.ID, "style");
				CodeVariableDeclarationStatement statement = new CodeVariableDeclarationStatement(typeof(WorksheetStyle), name, new CodeMethodInvokeExpression(new CodeVariableReferenceExpression("styles"), "Add", new CodeExpression[] { new CodePrimitiveExpression(style.ID) }));
				Util.AddComment(method2, style.ID);
				method2.Statements.Add(statement);
				((ICodeWriter) style).WriteTo(type, method2, new CodeVariableReferenceExpression(name));
			}
		}

		void IReader.ReadXml(XmlElement element)
		{
			if (!IsElement(element))
			{
				throw new ArgumentException("Invalid element", "element");
			}
			foreach (XmlNode node in element.ChildNodes)
			{
				XmlElement element2 = node as XmlElement;
				if ((element2 != null) && WorksheetStyle.IsElement(element2))
				{
					WorksheetStyle style = new WorksheetStyle(null);
					((IReader) style).ReadXml(element2);
					this.Add(style);
				}
			}
		}

		void IWriter.WriteXml(XmlWriter writer)
		{
			writer.WriteStartElement("Styles", "urn:schemas-microsoft-com:office:spreadsheet");
			for (int i = 0; i < base.InnerList.Count; i++)
			{
				((IWriter) base.InnerList[i]).WriteXml(writer);
			}
			writer.WriteEndElement();
		}

		public bool Contains(WorksheetStyle item)
		{
			return base.InnerList.Contains(item);
		}

		public void CopyTo(WorksheetStyle[] array, int index)
		{
			base.InnerList.CopyTo(array, index);
		}

		public int IndexOf(WorksheetStyle item)
		{
			return base.InnerList.IndexOf(item);
		}

		public int IndexOf(string id)
		{
			for (int i = 0; i < base.InnerList.Count; i++)
			{
				if (string.Compare(((WorksheetStyle) base.InnerList[i]).ID, id, true, CultureInfo.InvariantCulture) == 0)
				{
					return i;
				}
			}
			return -1;
		}

		public void Insert(int index, WorksheetStyle item)
		{
			base.InnerList.Insert(index, item);
		}

		internal static bool IsElement(XmlElement element)
		{
			return Util.IsElement(element, "Styles", "urn:schemas-microsoft-com:office:spreadsheet");
		}

		public void Remove(WorksheetStyle item)
		{
			base.InnerList.Remove(item);
		}

		// Properties
		public WorksheetStyle this[string id]
		{
			get
			{
				int index = this.IndexOf(id);
				if (index == -1)
				{
					throw new ArgumentException("The specified style " + id + " does not exists in the collection");
				}
				return (WorksheetStyle) base.InnerList[index];
			}
		}

		public WorksheetStyle this[int index]
		{
			get
			{
				return (WorksheetStyle) base.InnerList[index];
			}
			set
			{
				base.InnerList[index] = value;
			}
		}
	}

	public sealed class WorksheetStyleFont : IWriter, IReader, ICodeWriter
	{
		// Fields
		private bool _bold;
		private string _color;
		private string _fontName;
		private bool _italic;
		private int _size;
		private bool _strikethrough;
		private UnderlineStyle _underline;

		// Methods
		internal WorksheetStyleFont()
		{
		}

		void ICodeWriter.WriteTo(CodeTypeDeclaration type, CodeMemberMethod method, CodeExpression targetObject)
		{
			if (this._bold)
			{
				Util.AddAssignment(method, targetObject, "Bold", this._bold);
			}
			if (this._italic)
			{
				Util.AddAssignment(method, targetObject, "Italic", this._italic);
			}
			if (this._underline != UnderlineStyle.None)
			{
				Util.AddAssignment(method, targetObject, "Underline", this._underline);
			}
			if (this._strikethrough)
			{
				Util.AddAssignment(method, targetObject, "StrikeThrough", this._strikethrough);
			}
			if (this._fontName != null)
			{
				Util.AddAssignment(method, targetObject, "FontName", this._fontName);
			}
			if (this._size != 0)
			{
				Util.AddAssignment(method, targetObject, "Size", this._size);
			}
			if (this._color != null)
			{
				Util.AddAssignment(method, targetObject, "Color", this._color);
			}
		}

		void IReader.ReadXml(XmlElement element)
		{
			if (!IsElement(element))
			{
				throw new ArgumentException("Invalid element", "element");
			}
			this._bold = element.GetAttribute("Bold", "urn:schemas-microsoft-com:office:spreadsheet") == "1";
			this._italic = element.GetAttribute("Italic", "urn:schemas-microsoft-com:office:spreadsheet") == "1";
			string attribute = element.GetAttribute("Underline", "urn:schemas-microsoft-com:office:spreadsheet");
			if ((attribute != null) && (attribute.Length != 0))
			{
				this._underline = (UnderlineStyle) Enum.Parse(typeof(UnderlineStyle), attribute, true);
			}
			this._strikethrough = element.GetAttribute("StrikeThrough", "urn:schemas-microsoft-com:office:spreadsheet") == "1";
			this._fontName = Util.GetAttribute(element, "FontName", "urn:schemas-microsoft-com:office:spreadsheet");
			this._size = Util.GetAttribute(element, "Size", "urn:schemas-microsoft-com:office:spreadsheet", 0);
			this._color = Util.GetAttribute(element, "Color", "urn:schemas-microsoft-com:office:spreadsheet");
		}

		void IWriter.WriteXml(XmlWriter writer)
		{
			writer.WriteStartElement("s", "Font", "urn:schemas-microsoft-com:office:spreadsheet");
			if (this._bold)
			{
				writer.WriteAttributeString("Bold", "urn:schemas-microsoft-com:office:spreadsheet", "1");
			}
			if (this._italic)
			{
				writer.WriteAttributeString("Italic", "urn:schemas-microsoft-com:office:spreadsheet", "1");
			}
			if (this._underline != UnderlineStyle.None)
			{
				writer.WriteAttributeString("Underline", "urn:schemas-microsoft-com:office:spreadsheet", this._underline.ToString(CultureInfo.InvariantCulture));
			}
			if (this._strikethrough)
			{
				writer.WriteAttributeString("StrikeThrough", "urn:schemas-microsoft-com:office:spreadsheet", "1");
			}
			if (this._fontName != null)
			{
				writer.WriteAttributeString("FontName", "urn:schemas-microsoft-com:office:spreadsheet", this._fontName.ToString(CultureInfo.InvariantCulture));
			}
			if (this._size != 0)
			{
				writer.WriteAttributeString("Size", "urn:schemas-microsoft-com:office:spreadsheet", this._size.ToString(CultureInfo.InvariantCulture));
			}
			if (this._color != null)
			{
				writer.WriteAttributeString("Color", "urn:schemas-microsoft-com:office:spreadsheet", this._color);
			}
			writer.WriteEndElement();
		}

		internal static bool IsElement(XmlElement element)
		{
			return Util.IsElement(element, "Font", "urn:schemas-microsoft-com:office:spreadsheet");
		}

		// Properties
		public bool Bold
		{
			get
			{
				return this._bold;
			}
			set
			{
				this._bold = value;
			}
		}

		public string Color
		{
			get
			{
				return this._color;
			}
			set
			{
				this._color = value;
			}
		}

		public string FontName
		{
			get
			{
				return this._fontName;
			}
			set
			{
				this._fontName = value;
			}
		}

		public bool Italic
		{
			get
			{
				return this._italic;
			}
			set
			{
				this._italic = value;
			}
		}

		public int Size
		{
			get
			{
				return this._size;
			}
			set
			{
				this._size = value;
			}
		}

		public bool Strikethrough
		{
			get
			{
				return this._strikethrough;
			}
			set
			{
				this._strikethrough = value;
			}
		}

		public UnderlineStyle Underline
		{
			get
			{
				return this._underline;
			}
			set
			{
				this._underline = value;
			}
		}
	}

	public sealed class WorksheetStyleInterior : IWriter, IReader, ICodeWriter
	{
		// Fields
		private string _color;
		private StyleInteriorPattern _pattern;

		// Methods
		internal WorksheetStyleInterior()
		{
		}

		void ICodeWriter.WriteTo(CodeTypeDeclaration type, CodeMemberMethod method, CodeExpression targetObject)
		{
			if (this._color != null)
			{
				Util.AddAssignment(method, targetObject, "Color", this._color);
			}
			if (this._pattern != StyleInteriorPattern.NotSet)
			{
				Util.AddAssignment(method, targetObject, "Pattern", this._pattern);
			}
		}

		void IReader.ReadXml(XmlElement element)
		{
			if (!IsElement(element))
			{
				throw new ArgumentException("Invalid element", "element");
			}
			this._color = Util.GetAttribute(element, "Color", "urn:schemas-microsoft-com:office:spreadsheet");
			string attribute = element.GetAttribute("Pattern", "urn:schemas-microsoft-com:office:spreadsheet");
			if ((attribute != null) && (attribute.Length != 0))
			{
				this._pattern = (StyleInteriorPattern) Enum.Parse(typeof(StyleInteriorPattern), attribute, true);
			}
		}

		void IWriter.WriteXml(XmlWriter writer)
		{
			writer.WriteStartElement("s", "Interior", "urn:schemas-microsoft-com:office:spreadsheet");
			if (this._color != null)
			{
				writer.WriteAttributeString("Color", "urn:schemas-microsoft-com:office:spreadsheet", this._color);
			}
			if (this._pattern != StyleInteriorPattern.NotSet)
			{
				writer.WriteAttributeString("Pattern", "urn:schemas-microsoft-com:office:spreadsheet", this._pattern.ToString(CultureInfo.InvariantCulture));
			}
			writer.WriteEndElement();
		}

		internal static bool IsElement(XmlElement element)
		{
			return Util.IsElement(element, "Interior", "urn:schemas-microsoft-com:office:spreadsheet");
		}

		// Properties
		public string Color
		{
			get
			{
				return this._color;
			}
			set
			{
				this._color = value;
			}
		}

		public StyleInteriorPattern Pattern
		{
			get
			{
				return this._pattern;
			}
			set
			{
				this._pattern = value;
			}
		}
	}
 
	public sealed class WorksheetTable : IWriter, IReader, ICodeWriter
	{
		// Fields
		private WorksheetColumnCollection _columns;
		private float _defaultColumnWidth = 48f;
		private float _defaultRowHeight = 12.75f;
		private int _expandedColumnCount = Int32.MinValue;
		private int _expandedRowCount = Int32.MinValue;
		private int _fullColumns = Int32.MinValue;
		private int _fullRows = Int32.MinValue;
		private WorksheetRowCollection _rows;
		private string _styleID;

		// Methods
		internal WorksheetTable()
		{
		}

		void ICodeWriter.WriteTo(CodeTypeDeclaration type, CodeMemberMethod method, CodeExpression targetObject)
		{
			if (this._defaultRowHeight != 12.75f)
			{
				Util.AddAssignment(method, targetObject, "DefaultRowHeight", this._defaultRowHeight);
			}
			if (this._defaultColumnWidth != 48f)
			{
				Util.AddAssignment(method, targetObject, "DefaultColumnWidth", this._defaultColumnWidth);
			}
			if (this._expandedColumnCount != Int32.MinValue)
			{
				Util.AddAssignment(method, targetObject, "ExpandedColumnCount", this._expandedColumnCount);
			}
			if (this._expandedRowCount != Int32.MinValue)
			{
				Util.AddAssignment(method, targetObject, "ExpandedRowCount", this._expandedRowCount);
			}
			if (this._fullColumns != Int32.MinValue)
			{
				Util.AddAssignment(method, targetObject, "FullColumns", this._fullColumns);
			}
			if (this._fullRows != Int32.MinValue)
			{
				Util.AddAssignment(method, targetObject, "FullRows", this._fullRows);
			}
			if (this._styleID != null)
			{
				Util.AddAssignment(method, targetObject, "StyleID", this._styleID);
			}
			if (this._columns != null)
			{
				Util.Traverse(type, this._columns, method, targetObject, "Columns");
			}
			if (this._rows != null)
			{
				Util.Traverse(type, this._rows, method, targetObject, "Rows");
			}
		}

		void IReader.ReadXml(XmlElement element)
		{
			if (!IsElement(element))
			{
				throw new ArgumentException("Invalid element", "element");
			}
			this._defaultRowHeight = Util.GetAttribute(element, "DefaultRowHeight", "urn:schemas-microsoft-com:office:spreadsheet", (float) 12.75f);
			this._defaultColumnWidth = Util.GetAttribute(element, "DefaultColumnWidth", "urn:schemas-microsoft-com:office:spreadsheet", (float) 48f);
			this._expandedColumnCount = Util.GetAttribute(element, "ExpandedColumnCount", "urn:schemas-microsoft-com:office:spreadsheet", Int32.MinValue);
			this._expandedRowCount = Util.GetAttribute(element, "ExpandedRowCount", "urn:schemas-microsoft-com:office:spreadsheet", Int32.MinValue);
			this._fullColumns = Util.GetAttribute(element, "FullColumns", "urn:schemas-microsoft-com:office:excel", Int32.MinValue);
			this._fullRows = Util.GetAttribute(element, "FullRows", "urn:schemas-microsoft-com:office:excel", Int32.MinValue);
			this._fullRows = Util.GetAttribute(element, "FullRows", "urn:schemas-microsoft-com:office:excel", Int32.MinValue);
			this._styleID = Util.GetAttribute(element, "StyleID", "urn:schemas-microsoft-com:office:spreadsheet");
			foreach (XmlNode node in element.ChildNodes)
			{
				XmlElement element2 = node as XmlElement;
				if (element2 != null)
				{
					if (WorksheetColumn.IsElement(element2))
					{
						WorksheetColumn column = new WorksheetColumn();
						((IReader) column).ReadXml(element2);
						this.Columns.Add(column);
					}
					else if (WorksheetRow.IsElement(element2))
					{
						WorksheetRow row = new WorksheetRow();
						((IReader) row).ReadXml(element2);
						this.Rows.Add(row);
					}
				}
			}
		}

		void IWriter.WriteXml(XmlWriter writer)
		{
			writer.WriteStartElement("s", "Table", "urn:schemas-microsoft-com:office:spreadsheet");
			if (this._defaultRowHeight != 12.75f)
			{
				writer.WriteAttributeString("s", "DefaultRowHeight", "urn:schemas-microsoft-com:office:spreadsheet", this._defaultRowHeight.ToString(CultureInfo.InvariantCulture));
			}
			if (this._defaultColumnWidth != 48f)
			{
				writer.WriteAttributeString("s", "DefaultColumnWidth", "urn:schemas-microsoft-com:office:spreadsheet", this._defaultColumnWidth.ToString(CultureInfo.InvariantCulture));
			}
			if (this._expandedColumnCount != Int32.MinValue)
			{
				writer.WriteAttributeString("s", "ExpandedColumnCount", "urn:schemas-microsoft-com:office:spreadsheet", this._expandedColumnCount.ToString(CultureInfo.InvariantCulture));
			}
			if (this._expandedRowCount != Int32.MinValue)
			{
				writer.WriteAttributeString("s", "ExpandedRowCount", "urn:schemas-microsoft-com:office:spreadsheet", this._expandedRowCount.ToString(CultureInfo.InvariantCulture));
			}
			if (this._fullColumns != Int32.MinValue)
			{
				writer.WriteAttributeString("x", "FullColumns", "urn:schemas-microsoft-com:office:excel", this._fullColumns.ToString(CultureInfo.InvariantCulture));
			}
			if (this._fullRows != Int32.MinValue)
			{
				writer.WriteAttributeString("x", "FullRows", "urn:schemas-microsoft-com:office:excel", this._fullRows.ToString(CultureInfo.InvariantCulture));
			}
			if (this._styleID != null)
			{
				writer.WriteAttributeString("s", "StyleID", "urn:schemas-microsoft-com:office:spreadsheet", this._styleID);
			}
			if (this._columns != null)
			{
				((IWriter) this._columns).WriteXml(writer);
			}
			if (this._rows != null)
			{
				((IWriter) this._rows).WriteXml(writer);
			}
			writer.WriteEndElement();
		}

		internal static bool IsElement(XmlElement element)
		{
			return Util.IsElement(element, "Table", "urn:schemas-microsoft-com:office:spreadsheet");
		}

		// Properties
		public WorksheetColumnCollection Columns
		{
			get
			{
				if (this._columns == null)
				{
					this._columns = new WorksheetColumnCollection(this);
				}
				return this._columns;
			}
		}

		public float DefaultColumnWidth
		{
			get
			{
				return this._defaultColumnWidth;
			}
			set
			{
				this._defaultColumnWidth = value;
			}
		}

		public float DefaultRowHeight
		{
			get
			{
				return this._defaultRowHeight;
			}
			set
			{
				this._defaultRowHeight = value;
			}
		}

		public int ExpandedColumnCount
		{
			get
			{
				return this._expandedColumnCount;
			}
			set
			{
				this._expandedColumnCount = value;
			}
		}

		public int ExpandedRowCount
		{
			get
			{
				return this._expandedRowCount;
			}
			set
			{
				this._expandedRowCount = value;
			}
		}

		[EditorBrowsable(EditorBrowsableState.Never)]
		public int FullColumns
		{
			get
			{
				return this._fullColumns;
			}
			set
			{
				this._fullColumns = value;
			}
		}

		[EditorBrowsable(EditorBrowsableState.Never)]
		public int FullRows
		{
			get
			{
				return this._fullRows;
			}
			set
			{
				this._fullRows = value;
			}
		}

		public WorksheetRowCollection Rows
		{
			get
			{
				if (this._rows == null)
				{
					this._rows = new WorksheetRowCollection(this);
				}
				return this._rows;
			}
		}

		public string StyleID
		{
			get
			{
				return this._styleID;
			}
			set
			{
				this._styleID = value;
			}
		}
	}

}
