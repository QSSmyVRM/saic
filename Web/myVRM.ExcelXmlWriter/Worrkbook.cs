//ZD 100147 Start
/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
//ZD 100147 End
using System;
using System.IO;
using System.ComponentModel;
using System.Globalization;
using System.Collections;
using System.Collections.Specialized;
using System.Xml;
using System.CodeDom;
using myVRM.ExcelXmlWriter.Schemas;


namespace myVRM.ExcelXmlWriter
{
	public sealed class Workbook : IWriter, IReader, ICodeWriter
	{
		// Fields
		private DocumentProperties _documentProperties;
		private ExcelWorkbook _excelWorkbook;
		private bool _generateExcelProcessingInstruction = true;
		private WorksheetNamedRangeCollection _names;
		private PivotCache _pivotCache;
		private SpreadSheetComponentOptions _spreadSheetComponentOptions;
		private WorksheetStyleCollection _styles;
		private WorksheetCollection _worksheets;

		// Methods
		void ICodeWriter.WriteTo(CodeTypeDeclaration type, CodeMemberMethod method, CodeExpression targetObject)
		{
			if (this._documentProperties != null)
			{
				Util.Traverse(type, this._documentProperties, method, targetObject, "Properties");
			}
			if (this._spreadSheetComponentOptions != null)
			{
				Util.Traverse(type, this._spreadSheetComponentOptions, method, targetObject, "SpreadSheetComponentOptions");
			}
			if (this._excelWorkbook != null)
			{
				Util.Traverse(type, this._excelWorkbook, method, targetObject, "ExcelWorkbook");
			}
			if (this._styles != null)
			{
				Util.Traverse(type, this._styles, method, targetObject, "Styles");
			}
			if (this._names != null)
			{
				Util.Traverse(type, this._names, method, targetObject, "Names");
			}
			if (this._worksheets != null)
			{
				Util.Traverse(type, this._worksheets, method, targetObject, "Worksheets");
			}
		}

		void IReader.ReadXml(XmlElement element)
		{
			if (!IsElement(element))
			{
				throw new InvalidOperationException("The specified Xml is not a valid Workbook.\nElement Name:" + element.Name);
			}
			foreach (XmlNode node in element.ChildNodes)
			{
				XmlElement element2 = node as XmlElement;
				if (element2 != null)
				{
					if (DocumentProperties.IsElement(element2))
					{
						((IReader) this.Properties).ReadXml(element2);
					}
					else
					{
						if (SpreadSheetComponentOptions.IsElement(element2))
						{
							((IReader) this.SpreadSheetComponentOptions).ReadXml(element2);
							continue;
						}
						if (ExcelWorkbook.IsElement(element2))
						{
							((IReader) this.ExcelWorkbook).ReadXml(element2);
							continue;
						}
						if (WorksheetStyleCollection.IsElement(element2))
						{
							((IReader) this.Styles).ReadXml(element2);
							continue;
						}
						if (WorksheetNamedRangeCollection.IsElement(element2))
						{
							((IReader) this.Names).ReadXml(element2);
							continue;
						}
						if (Worksheet.IsElement(element2))
						{
							Worksheet sheet = new Worksheet(null);
							((IReader) sheet).ReadXml(element2);
							this.Worksheets.Add(sheet);
						}
					}
				}
			}
		}

		void IWriter.WriteXml(XmlWriter writer)
		{
			writer.WriteStartElement("s", "Workbook", "urn:schemas-microsoft-com:office:spreadsheet");
			writer.WriteAttributeString("xmlns", "x", null, "urn:schemas-microsoft-com:office:excel");
			writer.WriteAttributeString("xmlns", "o", null, "urn:schemas-microsoft-com:office:office");
			if (this._documentProperties != null)
			{
				((IWriter) this._documentProperties).WriteXml(writer);
			}
			if (this._spreadSheetComponentOptions != null)
			{
				((IWriter) this._spreadSheetComponentOptions).WriteXml(writer);
			}
			if (this._excelWorkbook != null)
			{
				((IWriter) this._excelWorkbook).WriteXml(writer);
			}
			if (this._styles != null)
			{
				((IWriter) this._styles).WriteXml(writer);
			}
			if (this._names != null)
			{
				((IWriter) this._names).WriteXml(writer);
			}
			if (this._worksheets != null)
			{
				((IWriter) this._worksheets).WriteXml(writer);
			}
			if (this._pivotCache != null)
			{
				((IWriter) this._pivotCache).WriteXml(writer);
			}
			writer.WriteEndElement();
		}

		internal static bool IsElement(XmlElement element)
		{
			return Util.IsElement(element, "Workbook", "urn:schemas-microsoft-com:office:spreadsheet");
		}

		public void Load(Stream stream)
		{
			if (stream == null)
			{
				throw new ArgumentNullException("stream");
			}
			if (stream.Position >= stream.Length)
			{
				stream.Position = 0L;
			}
			XmlDocument document = new XmlDocument();
			document.Load(stream);
			((IReader) this).ReadXml(document.DocumentElement);
		}

		public void Load(string filename)
		{
			FileStream stream = null;
			try
			{
				stream = new FileStream(filename, FileMode.Open, FileAccess.Read, FileShare.ReadWrite);
				this.Load(stream);
			}
			finally
			{
				if (stream != null)
				{
					stream.Close();
				}
			}
		}

		public void Save(Stream stream)
		{
			if (this.Worksheets.Count == 0)
			{
				this.Worksheets.Add("Sheet 1");
			}
			XmlTextWriter writer = new XmlTextWriter(stream, System.Text.Encoding.UTF8);
			writer.Namespaces = true;
			writer.Formatting = Formatting.Indented;
			writer.WriteProcessingInstruction("xml", "version='1.0'");
			if (this._generateExcelProcessingInstruction)
			{
				writer.WriteProcessingInstruction("mso-application", "progid='Excel.Sheet'");
			}
			((IWriter) this).WriteXml(writer);
			writer.Flush();
		}

		public void Save(string filename)
		{
			FileStream stream = null;
			try
			{
				stream = new FileStream(filename, FileMode.Create, FileAccess.Write, FileShare.None);
				this.Save(stream);
			}
			finally
			{
				if (stream != null)
				{
					stream.Close();
				}
			}
		}

		// Properties
		public ExcelWorkbook ExcelWorkbook
		{
			get
			{
				if (this._excelWorkbook == null)
				{
					this._excelWorkbook = new ExcelWorkbook();
				}
				return this._excelWorkbook;
			}
		}

		public bool GenerateExcelProcessingInstruction
		{
			get
			{
				return this._generateExcelProcessingInstruction;
			}
			set
			{
				this._generateExcelProcessingInstruction = value;
			}
		}

		public WorksheetNamedRangeCollection Names
		{
			get
			{
				if (this._names == null)
				{
					this._names = new WorksheetNamedRangeCollection();
				}
				return this._names;
			}
		}

		[EditorBrowsable(EditorBrowsableState.Never)]
		public PivotCache PivotCache
		{
			get
			{
				if (this._pivotCache == null)
				{
					this._pivotCache = new PivotCache();
				}
				return this._pivotCache;
			}
		}

		public DocumentProperties Properties
		{
			get
			{
				if (this._documentProperties == null)
				{
					this._documentProperties = new DocumentProperties();
				}
				return this._documentProperties;
			}
		}

		public SpreadSheetComponentOptions SpreadSheetComponentOptions
		{
			get
			{
				if (this._spreadSheetComponentOptions == null)
				{
					this._spreadSheetComponentOptions = new SpreadSheetComponentOptions();
				}
				return this._spreadSheetComponentOptions;
			}
		}

		public WorksheetStyleCollection Styles
		{
			get
			{
				if (this._styles == null)
				{
					this._styles = new WorksheetStyleCollection(this);
				}
				return this._styles;
			}
		}

		public WorksheetCollection Worksheets
		{
			get
			{
				if (this._worksheets == null)
				{
					this._worksheets = new WorksheetCollection();
				}
				return this._worksheets;
			}
		}
	}
	public sealed class Worksheet : IWriter, IReader, ICodeWriter
	{
		// Fields
		private WorksheetAutoFilter _autoFilter;
		private string _name;
		private WorksheetNamedRangeCollection _names;
		private WorksheetOptions _options;
		private PivotTable _pivotTable;
		private bool _protected;
		private StringCollection _sorting;
		private WorksheetTable _table;
		internal static CodeVariableDeclarationStatement cellDeclaration;

		// Methods
		internal Worksheet(string name)
		{
			this._name = name;
		}

		void ICodeWriter.WriteTo(CodeTypeDeclaration type, CodeMemberMethod method, CodeExpression targetObject)
		{
			if (this._protected)
			{
				Util.AddAssignment(method, targetObject, "Protected", true);
			}
			if (this._names != null)
			{
				Util.Traverse(type, this._names, method, targetObject, "Names");
			}
			if (this._table != null)
			{
				Util.Traverse(type, this._table, method, targetObject, "Table");
			}
			if (this._options != null)
			{
				Util.Traverse(type, this._options, method, targetObject, "Options");
			}
			if (this._autoFilter != null)
			{
				Util.Traverse(type, this._autoFilter, method, targetObject, "AutoFilter");
			}
			if (this._sorting != null)
			{
				foreach (string str in this._sorting)
				{
					method.Statements.Add(new CodeMethodInvokeExpression(new CodePropertyReferenceExpression(targetObject, "Sorting"), "Add", new CodeExpression[] { new CodePrimitiveExpression(str) }));
				}
			}
		}

		void IReader.ReadXml(XmlElement element)
		{
			if (!IsElement(element))
			{
				throw new ArgumentException("Invalid element", "element");
			}
			this._name = Util.GetAttribute(element, "Name", "urn:schemas-microsoft-com:office:spreadsheet");
			this._protected = Util.GetAttribute(element, "Protected", "urn:schemas-microsoft-com:office:spreadsheet", false);
			foreach (XmlNode node in element.ChildNodes)
			{
				XmlElement element2 = node as XmlElement;
				if (element2 != null)
				{
					if (WorksheetTable.IsElement(element2))
					{
						((IReader) this.Table).ReadXml(element2);
					}
					else
					{
						if (WorksheetNamedRangeCollection.IsElement(element2))
						{
							((IReader) this.Names).ReadXml(element2);
							continue;
						}
						if (WorksheetAutoFilter.IsElement(element2))
						{
							((IReader) this.AutoFilter).ReadXml(element2);
							continue;
						}
						if (WorksheetOptions.IsElement(element2))
						{
							((IReader) this.Options).ReadXml(element2);
							continue;
						}
						if (Util.IsElement(element2, "Sorting", "urn:schemas-microsoft-com:office:excel"))
						{
							foreach (XmlElement element3 in element2.ChildNodes)
							{
								if (Util.IsElement(element3, "Sort", "urn:schemas-microsoft-com:office:excel"))
								{
									this.Sorting.Add(element3.InnerText);
								}
							}
							continue;
						}
					}
				}
			}
		}

		void IWriter.WriteXml(XmlWriter writer)
		{
			writer.WriteStartElement("s", "Worksheet", "urn:schemas-microsoft-com:office:spreadsheet");
			if (this._name != null)
			{
				writer.WriteAttributeString("Name", "urn:schemas-microsoft-com:office:spreadsheet", this._name);
			}
			if (this._protected)
			{
				writer.WriteAttributeString("Protected", "urn:schemas-microsoft-com:office:spreadsheet", "1");
			}
			if (this._names != null)
			{
				((IWriter) this._names).WriteXml(writer);
			}
			if (this._table != null)
			{
				((IWriter) this._table).WriteXml(writer);
			}
			if (this._options != null)
			{
				((IWriter) this._options).WriteXml(writer);
			}
			if (this._autoFilter != null)
			{
				((IWriter) this._autoFilter).WriteXml(writer);
			}
			if (this._pivotTable != null)
			{
				((IWriter) this._pivotTable).WriteXml(writer);
			}
			if (this._sorting != null)
			{
				writer.WriteStartElement("x", "Sorting", "urn:schemas-microsoft-com:office:excel");
				foreach (string str in this._sorting)
				{
					writer.WriteElementString("Sort", "urn:schemas-microsoft-com:office:excel", str);
				}
				writer.WriteEndElement();
			}
			writer.WriteEndElement();
		}

		internal static bool IsElement(XmlElement element)
		{
			return Util.IsElement(element, "Worksheet", "urn:schemas-microsoft-com:office:spreadsheet");
		}

		// Properties
		public WorksheetAutoFilter AutoFilter
		{
			get
			{
				if (this._autoFilter == null)
				{
					this._autoFilter = new WorksheetAutoFilter();
				}
				return this._autoFilter;
			}
		}

		public string Name
		{
			get
			{
				return this._name;
			}
		}

		public WorksheetNamedRangeCollection Names
		{
			get
			{
				if (this._names == null)
				{
					this._names = new WorksheetNamedRangeCollection();
				}
				return this._names;
			}
		}

		public WorksheetOptions Options
		{
			get
			{
				if (this._options == null)
				{
					this._options = new WorksheetOptions();
				}
				return this._options;
			}
		}

		public PivotTable PivotTable
		{
			get
			{
				if (this._pivotTable == null)
				{
					this._pivotTable = new PivotTable();
				}
				return this._pivotTable;
			}
		}

		public bool Protected
		{
			get
			{
				return this._protected;
			}
			set
			{
				this._protected = value;
			}
		}

		public StringCollection Sorting
		{
			get
			{
				if (this._sorting == null)
				{
					this._sorting = new StringCollection();
				}
				return this._sorting;
			}
		}

		public WorksheetTable Table
		{
			get
			{
				if (this._table == null)
				{
					this._table = new WorksheetTable();
				}
				return this._table;
			}
		}
	}

	public sealed class WorksheetAutoFilter : IReader, IWriter, ICodeWriter
	{
		// Fields
		private string _range;

		// Methods
		internal WorksheetAutoFilter()
		{
		}

		void ICodeWriter.WriteTo(CodeTypeDeclaration type, CodeMemberMethod method, CodeExpression targetObject)
		{
			if (this._range != null)
			{
				Util.AddAssignment(method, targetObject, "Range", this._range);
			}
		}

		void IReader.ReadXml(XmlElement element)
		{
			if (!IsElement(element))
			{
				throw new ArgumentException("Invalid element", "element");
			}
			this._range = Util.GetAttribute(element, "Range", "urn:schemas-microsoft-com:office:excel");
		}

		void IWriter.WriteXml(XmlWriter writer)
		{
			writer.WriteStartElement("x", "AutoFilter", "urn:schemas-microsoft-com:office:excel");
			if (this._range != null)
			{
				writer.WriteAttributeString("Range", "urn:schemas-microsoft-com:office:excel", this._range);
			}
			writer.WriteEndElement();
		}

		internal static bool IsElement(XmlElement element)
		{
			return Util.IsElement(element, "AutoFilter", "urn:schemas-microsoft-com:office:excel");
		}

		// Properties
		public string Range
		{
			get
			{
				return this._range;
			}
			set
			{
				this._range = value;
			}
		}
	}

	public sealed class WorksheetCell : IWriter, IReader, ICodeWriter
	{
		// Fields
		private WorksheetComment _comment;
		private WorksheetCellData _data;
		private string _formula;
		private string _href;
		private int _index;
		private int _mergeAcross;
		private int _mergeDown;
		private WorksheetNamedCellCollection _namedCell;
		internal WorksheetRow _row;
		private string _styleID;

		// Methods
		public WorksheetCell()
		{
			this._mergeAcross = -1;
			this._mergeDown = -1;
		}

		public WorksheetCell(string text)
		{
			this._mergeAcross = -1;
			this._mergeDown = -1;
			this.Data.Text = text;
			this.Data.Type = DataType.String;
		}

		public WorksheetCell(string text, DataType type)
		{
			this._mergeAcross = -1;
			this._mergeDown = -1;
			this.Data.Text = text;
			this.Data.Type = type;
		}

		public WorksheetCell(string text, string styleID)
		{
			this._mergeAcross = -1;
			this._mergeDown = -1;
			this.Data.Text = text;
			this._styleID = styleID;
			this.Data.Type = DataType.String;
		}

		public WorksheetCell(string text, DataType type, string styleID)
		{
			this._mergeAcross = -1;
			this._mergeDown = -1;
			this.Data.Text = text;
			this.Data.Type = type;
			this._styleID = styleID;
		}

		void ICodeWriter.WriteTo(CodeTypeDeclaration type, CodeMemberMethod method, CodeExpression targetObject)
		{
			if (this._styleID != null)
			{
				Util.AddAssignment(method, targetObject, "StyleID", this._styleID);
			}
			if (this._data != null)
			{
				Util.Traverse(type, this._data, method, targetObject, "Data");
			}
			if (this._index != 0)
			{
				Util.AddAssignment(method, targetObject, "Index", this._index);
			}
			if (this._mergeAcross > 0)
			{
				Util.AddAssignment(method, targetObject, "MergeAcross", this._mergeAcross);
			}
			if (this._mergeDown >= 0)
			{
				Util.AddAssignment(method, targetObject, "MergeDown", this._mergeDown);
			}
			if (this._formula != null)
			{
				Util.AddAssignment(method, targetObject, "Formula", this._formula);
			}
			if (this._href != null)
			{
				Util.AddAssignment(method, targetObject, "HRef", this._href);
			}
			if (this._comment != null)
			{
				Util.Traverse(type, this._comment, method, targetObject, "Comment");
			}
			if (this._namedCell != null)
			{
				foreach (string str in this._namedCell)
				{
					method.Statements.Add(new CodeMethodInvokeExpression(new CodePropertyReferenceExpression(targetObject, "NamedCell"), "Add", new CodeExpression[] { new CodePrimitiveExpression(str) }));
				}
			}
		}

		void IReader.ReadXml(XmlElement element)
		{
			if (!IsElement(element))
			{
				throw new ArgumentException("Invalid element", "element");
			}
			this._index = Util.GetAttribute(element, "Index", "urn:schemas-microsoft-com:office:spreadsheet", 0);
			this._mergeAcross = Util.GetAttribute(element, "MergeAcross", "urn:schemas-microsoft-com:office:spreadsheet", -1);
			this._mergeDown = Util.GetAttribute(element, "MergeDown", "urn:schemas-microsoft-com:office:spreadsheet", -1);
			this._styleID = Util.GetAttribute(element, "StyleID", "urn:schemas-microsoft-com:office:spreadsheet");
			this._formula = Util.GetAttribute(element, "Formula", "urn:schemas-microsoft-com:office:spreadsheet");
			this._href = Util.GetAttribute(element, "HRef", "urn:schemas-microsoft-com:office:spreadsheet");
			foreach (XmlNode node in element.ChildNodes)
			{
				XmlElement element2 = node as XmlElement;
				if (element2 != null)
				{
					if (WorksheetCellData.IsElement(element2))
					{
						((IReader) this.Data).ReadXml(element2);
					}
					else
					{
						if (WorksheetComment.IsElement(element2))
						{
							((IReader) this.Comment).ReadXml(element2);
							continue;
						}
						if (element2.LocalName == "NamedCell")
						{
							string name = Util.GetAttribute(element2, "Name", "urn:schemas-microsoft-com:office:spreadsheet");
							if ((name != null) && (name.Length > 0))
							{
								this.NamedCell.Add(name);
							}
						}
					}
				}
			}
		}

		void IWriter.WriteXml(XmlWriter writer)
		{
			writer.WriteStartElement("s", "Cell", "urn:schemas-microsoft-com:office:spreadsheet");
			if (this._index != 0)
			{
				writer.WriteAttributeString("s", "Index", "urn:schemas-microsoft-com:office:spreadsheet", this._index.ToString(CultureInfo.InvariantCulture));
			}
			if (this._mergeAcross > 0)
			{
				writer.WriteAttributeString("s", "MergeAcross", "urn:schemas-microsoft-com:office:spreadsheet", this._mergeAcross.ToString(CultureInfo.InvariantCulture));
			}
			if (this._mergeDown >= 0)
			{
				writer.WriteAttributeString("s", "MergeDown", "urn:schemas-microsoft-com:office:spreadsheet", this._mergeDown.ToString(CultureInfo.InvariantCulture));
			}
			if (this._styleID != null)
			{
				writer.WriteAttributeString("s", "StyleID", "urn:schemas-microsoft-com:office:spreadsheet", this._styleID);
			}
			if (this._formula != null)
			{
				writer.WriteAttributeString("s", "Formula", "urn:schemas-microsoft-com:office:spreadsheet", this._formula);
			}
			if (this._href != null)
			{
				writer.WriteAttributeString("s", "HRef", "urn:schemas-microsoft-com:office:spreadsheet", this._href);
			}
			if (this._comment != null)
			{
				((IWriter) this._comment).WriteXml(writer);
			}
			if (this._data != null)
			{
				((IWriter) this._data).WriteXml(writer);
			}
			if (this._namedCell != null)
			{
				foreach (string str in this._namedCell)
				{
					writer.WriteStartElement("s", "NamedCell", "urn:schemas-microsoft-com:office:spreadsheet");
					writer.WriteAttributeString("s", "Name", "urn:schemas-microsoft-com:office:spreadsheet", str);
					writer.WriteEndElement();
				}
			}
			writer.WriteEndElement();
		}

		internal static bool IsElement(XmlElement element)
		{
			return Util.IsElement(element, "Cell", "urn:schemas-microsoft-com:office:spreadsheet");
		}

		// Properties
		public WorksheetComment Comment
		{
			get
			{
				if (this._comment == null)
				{
					this._comment = new WorksheetComment();
				}
				return this._comment;
			}
		}

		public WorksheetCellData Data
		{
			get
			{
				if (this._data == null)
				{
					this._data = new WorksheetCellData(this);
				}
				return this._data;
			}
		}

		public string Formula
		{
			get
			{
				return this._formula;
			}
			set
			{
				this._formula = value;
			}
		}

		public string HRef
		{
			get
			{
				return this._href;
			}
			set
			{
				this._href = value;
			}
		}

		public int Index
		{
			get
			{
				return this._index;
			}
			set
			{
				this._index = value;
			}
		}

		internal bool IsSimple
		{
			get
			{
				return (((((this._styleID != null) && (this._data != null)) && (this._data.IsSimple && (this._index == 0))) && (((this._mergeAcross <= 0) && (this._mergeDown < 0)) && ((this._formula == null) && (this._href == null)))) && ((this._comment == null) && (this._namedCell == null)));
			}
		}

		public int MergeAcross
		{
			get
			{
				if (this._mergeAcross == -1)
				{
					return 0;
				}
				return this._mergeAcross;
			}
			set
			{
				if (value < 0)
				{
					throw new ArgumentOutOfRangeException("value");
				}
				this._mergeAcross = value;
			}
		}

		public int MergeDown
		{
			get
			{
				if (this._mergeDown == -1)
				{
					return 0;
				}
				return this._mergeDown;
			}
			set
			{
				if (value < 0)
				{
					throw new ArgumentOutOfRangeException("value");
				}
				this._mergeDown = value;
			}
		}

		public WorksheetNamedCellCollection NamedCell
		{
			get
			{
				if (this._namedCell == null)
				{
					this._namedCell = new WorksheetNamedCellCollection();
				}
				return this._namedCell;
			}
		}

		public WorksheetRow Row
		{
			get
			{
				return this._row;
			}
		}

		public string StyleID
		{
			get
			{
				return this._styleID;
			}
			set
			{
				this._styleID = value;
			}
		}
	}

	public sealed class WorksheetCellCollection : CollectionBase, IWriter, ICodeWriter
	{
		// Fields
		private WorksheetRow _row;

		// Methods
		internal WorksheetCellCollection(WorksheetRow row)
		{
			if (row == null)
			{
				throw new ArgumentNullException("row");
			}
			this._row = row;
		}

		public WorksheetCell Add()
		{
			WorksheetCell cell = new WorksheetCell();
			this.Add(cell);
			return cell;
		}

		public int Add(WorksheetCell cell)
		{
			if (cell == null)
			{
				throw new ArgumentNullException("cell");
			}
			cell._row = this._row;
			return base.InnerList.Add(cell);
		}

		public WorksheetCell Add(string text)
		{
			WorksheetCell cell = new WorksheetCell(text);
			this.Add(cell);
			return cell;
		}

		public WorksheetCell Add(string text, DataType dataType, string styleID)
		{
			WorksheetCell cell = new WorksheetCell(text, dataType, styleID);
			this.Add(cell);
			return cell;
		}

		void ICodeWriter.WriteTo(CodeTypeDeclaration type, CodeMemberMethod method, CodeExpression targetObject)
		{
			for (int i = 0; i < base.InnerList.Count; i++)
			{
				WorksheetCell cell = this[i];
				if (cell.IsSimple)
				{
					method.Statements.Add(new CodeMethodInvokeExpression(targetObject, "Add", new CodeExpression[] { new CodePrimitiveExpression(cell.Data.Text), Util.GetRightExpressionForValue(cell.Data.Type, typeof(DataType)), new CodePrimitiveExpression(cell.StyleID) }));
				}
				else
				{
					string variableName = "cell";
					if (Worksheet.cellDeclaration == null)
					{
						Worksheet.cellDeclaration = new CodeVariableDeclarationStatement(typeof(WorksheetCell), "cell");
						method.Statements.Add(Worksheet.cellDeclaration);
					}
					CodeAssignStatement statement = new CodeAssignStatement(new CodeVariableReferenceExpression("cell"), new CodeMethodInvokeExpression(targetObject, "Add", new CodeExpression[0]));
					method.Statements.Add(statement);
					((ICodeWriter) cell).WriteTo(type, method, new CodeVariableReferenceExpression(variableName));
				}
			}
		}

		void IWriter.WriteXml(XmlWriter writer)
		{
			for (int i = 0; i < base.InnerList.Count; i++)
			{
				((IWriter) base.InnerList[i]).WriteXml(writer);
			}
		}

		public bool Contains(WorksheetCell item)
		{
			return base.InnerList.Contains(item);
		}

		public void CopyTo(WorksheetCell[] array, int index)
		{
			base.InnerList.CopyTo(array, index);
		}

		public int IndexOf(WorksheetCell item)
		{
			return base.InnerList.IndexOf(item);
		}

		public void Insert(int index, WorksheetCell item)
		{
			base.InnerList.Insert(index, item);
		}

		public void Remove(WorksheetCell item)
		{
			base.InnerList.Remove(item);
		}

		public object[] ToArray()
		{
			return base.InnerList.ToArray();
		}

		// Properties
		public WorksheetCell this[int index]
		{
			get
			{
				return (WorksheetCell) base.InnerList[index];
			}
			set
			{
				if (value != null)
				{
					value._row = this._row;
				}
				base.InnerList[index] = value;
			}
		}
	}

	public sealed class WorksheetCellData : IWriter, IReader, ICodeWriter
	{
		// Fields
		private IReader _parent;
		private string _text;
		private DataType _type = DataType.String;

		// Methods
		internal WorksheetCellData(IReader parent)
		{
			this._parent = parent;
		}

		void ICodeWriter.WriteTo(CodeTypeDeclaration type, CodeMemberMethod method, CodeExpression targetObject)
		{
			if ((this._type != DataType.NotSet) && !(this._parent is WorksheetComment))
			{
				Util.AddAssignment(method, targetObject, "Type", this._type);
			}
			if (this._text != null)
			{
				Util.AddAssignment(method, targetObject, "Text", this._text);
			}
		}

		void IReader.ReadXml(XmlElement element)
		{
			if (!IsElement(element))
			{
				throw new ArgumentException("Invalid element", "element");
			}
			string attribute = element.GetAttribute("Type", "urn:schemas-microsoft-com:office:spreadsheet");
			if ((attribute != null) && (attribute.Length > 0))
			{
				this._type = (DataType) Enum.Parse(typeof(DataType), attribute);
			}
			if (!element.IsEmpty)
			{
				this._text = element.InnerText;
			}
		}

		void IWriter.WriteXml(XmlWriter writer)
		{
			writer.WriteStartElement("s", "Data", "urn:schemas-microsoft-com:office:spreadsheet");
			if ((this._type != DataType.NotSet) && !(this._parent is WorksheetComment))
			{
				writer.WriteAttributeString("s", "Type", "urn:schemas-microsoft-com:office:spreadsheet", this._type.ToString(CultureInfo.InvariantCulture));
			}
			writer.WriteString(this._text);
			writer.WriteEndElement();
		}

		internal static bool IsElement(XmlElement element)
		{
			return Util.IsElement(element, "Data", "urn:schemas-microsoft-com:office:spreadsheet");
		}

		// Properties
		internal bool IsSimple
		{
			get
			{
				return ((this._type != DataType.NotSet) && (this._text != null));
			}
		}

		public string Text
		{
			get
			{
				return this._text;
			}
			set
			{
				this._text = value;
			}
		}

		public DataType Type
		{
			get
			{
				return this._type;
			}
			set
			{
				this._type = value;
			}
		}
	}

	public sealed class WorksheetCollection : CollectionBase, IWriter, ICodeWriter
	{
		// Methods
		internal WorksheetCollection()
		{
		}

		public int Add(Worksheet sheet)
		{
			return base.InnerList.Add(sheet);
		}

		public Worksheet Add(string name)
		{
			Worksheet sheet = new Worksheet(name);
			this.Add(sheet);
			return sheet;
		}

		void ICodeWriter.WriteTo(CodeTypeDeclaration type, CodeMemberMethod method, CodeExpression targetObject)
		{
			for (int i = 0; i < base.InnerList.Count; i++)
			{
				Worksheet worksheet = this[i];
				string str = Util.CreateSafeName(worksheet.Name, "Sheet");
				CodeMemberMethod method2 = new CodeMemberMethod();
				method2.Name = "GenerateWorksheet" + str;
				method2.Parameters.Add(new CodeParameterDeclarationExpression(typeof(WorksheetCollection), "sheets"));
				Worksheet.cellDeclaration = null;
				Util.AddComment(method, "Generate " + worksheet.Name + " Worksheet");
				method.Statements.Add(new CodeMethodInvokeExpression(new CodeThisReferenceExpression(), method2.Name, new CodeExpression[] { targetObject }));
				type.Members.Add(method2);
				CodeVariableDeclarationStatement statement = new CodeVariableDeclarationStatement(typeof(Worksheet), "sheet", new CodeMethodInvokeExpression(new CodeVariableReferenceExpression("sheets"), "Add", new CodeExpression[] { new CodePrimitiveExpression(str) }));
				method2.Statements.Add(statement);
				((ICodeWriter) worksheet).WriteTo(type, method2, new CodeVariableReferenceExpression("sheet"));
			}
		}

		void IWriter.WriteXml(XmlWriter writer)
		{
			for (int i = 0; i < base.InnerList.Count; i++)
			{
				((IWriter) base.InnerList[i]).WriteXml(writer);
			}
		}

		public bool Contains(Worksheet item)
		{
			return base.InnerList.Contains(item);
		}

		public void CopyTo(Worksheet[] array, int index)
		{
			base.InnerList.CopyTo(array, index);
		}

		public int IndexOf(Worksheet item)
		{
			return base.InnerList.IndexOf(item);
		}

		public int IndexOf(string name)
		{
			for (int i = 0; i < base.InnerList.Count; i++)
			{
				if (string.Compare(((Worksheet) base.InnerList[i]).Name, name, true, CultureInfo.InvariantCulture) == 0)
				{
					return i;
				}
			}
			return -1;
		}

		public void Insert(int index, Worksheet sheet)
		{
			base.InnerList.Insert(index, sheet);
		}

		public void Remove(Worksheet item)
		{
			base.InnerList.Remove(item);
		}

		public object[] ToArray()
		{
			return base.InnerList.ToArray();
		}

		// Properties
		public Worksheet this[string name]
		{
			get
			{
				int index = this.IndexOf(name);
				if (index == -1)
				{
					throw new ArgumentException("The specified worksheet " + name + " does not exists in the collection");
				}
				return (Worksheet) base.InnerList[index];
			}
			set
			{
				int index = this.IndexOf(name);
				if (index == -1)
				{
					throw new ArgumentException("The specified worksheet " + name + " does not exists in the collection");
				}
				base.InnerList[index] = value;
			}
		}

		public Worksheet this[int index]
		{
			get
			{
				return (Worksheet) base.InnerList[index];
			}
			set
			{
				base.InnerList[index] = value;
			}
		}
	}

	public sealed class WorksheetColumn : IWriter, IReader, ICodeWriter
	{
		// Fields
		private bool _autoFitWidth;
		private bool _hidden;
		private int _index;
		private int _span;
		private string _styleID;
		internal WorksheetTable _table;
		private int _width;

		// Methods
		public WorksheetColumn()
		{
			this._width = Int32.MinValue;
			this._span = Int32.MinValue;
		}

		public WorksheetColumn(int width)
		{
			this._width = Int32.MinValue;
			this._span = Int32.MinValue;
			this._width = width;
		}

		void ICodeWriter.WriteTo(CodeTypeDeclaration type, CodeMemberMethod method, CodeExpression targetObject)
		{
			if (this._index != 0)
			{
				Util.AddAssignment(method, targetObject, "Index", this._index);
			}
			if (this._width != Int32.MinValue)
			{
				Util.AddAssignment(method, targetObject, "Width", this._width);
			}
			if (this._hidden)
			{
				Util.AddAssignment(method, targetObject, "Hidden", this._hidden);
			}
			if (this._autoFitWidth)
			{
				Util.AddAssignment(method, targetObject, "AutoFitWidth", this._autoFitWidth);
			}
			if (this._styleID != null)
			{
				Util.AddAssignment(method, targetObject, "StyleID", this._styleID);
			}
			if (this._span != Int32.MinValue)
			{
				Util.AddAssignment(method, targetObject, "Span", this._span);
			}
		}

		void IReader.ReadXml(XmlElement element)
		{
			if (!IsElement(element))
			{
				throw new ArgumentException("Invalid element", "element");
			}
			this._index = Util.GetAttribute(element, "Index", "urn:schemas-microsoft-com:office:spreadsheet", 0);
			this._span = Util.GetAttribute(element, "Span", "urn:schemas-microsoft-com:office:spreadsheet", Int32.MinValue);
			this._width = Util.GetAttribute(element, "Width", "urn:schemas-microsoft-com:office:spreadsheet", Int32.MinValue);
			this._hidden = Util.GetAttribute(element, "Hidden", "urn:schemas-microsoft-com:office:spreadsheet", false);
			this._autoFitWidth = Util.GetAttribute(element, "AutoFitWidth", "urn:schemas-microsoft-com:office:spreadsheet", false);
			this._styleID = Util.GetAttribute(element, "StyleID", "urn:schemas-microsoft-com:office:spreadsheet");
		}

		void IWriter.WriteXml(XmlWriter writer)
		{
			writer.WriteStartElement("s", "Column", "urn:schemas-microsoft-com:office:spreadsheet");
			if (this._index != 0)
			{
				writer.WriteAttributeString("s", "Index", "urn:schemas-microsoft-com:office:spreadsheet", this._index.ToString(CultureInfo.InvariantCulture));
			}
			if (this._width != Int32.MinValue)
			{
				writer.WriteAttributeString("s", "Width", "urn:schemas-microsoft-com:office:spreadsheet", this._width.ToString(CultureInfo.InvariantCulture));
			}
			if (this._hidden)
			{
				writer.WriteAttributeString("s", "Hidden", "urn:schemas-microsoft-com:office:spreadsheet", "1");
			}
			if (this._autoFitWidth)
			{
				writer.WriteAttributeString("s", "AutoFitWidth", "urn:schemas-microsoft-com:office:spreadsheet", "1");
			}
			if (this._styleID != null)
			{
				writer.WriteAttributeString("s", "StyleID", "urn:schemas-microsoft-com:office:spreadsheet", this._styleID);
			}
			if (this._span != Int32.MinValue)
			{
				writer.WriteAttributeString("s", "Span", "urn:schemas-microsoft-com:office:spreadsheet", this._span.ToString(CultureInfo.InvariantCulture));
			}
			writer.WriteEndElement();
		}

		internal static bool IsElement(XmlElement element)
		{
			return Util.IsElement(element, "Column", "urn:schemas-microsoft-com:office:spreadsheet");
		}

		// Properties
		public bool AutoFitWidth
		{
			get
			{
				return this._autoFitWidth;
			}
			set
			{
				this._autoFitWidth = value;
			}
		}

		public bool Hidden
		{
			get
			{
				return this._hidden;
			}
			set
			{
				this._hidden = value;
			}
		}

		public int Index
		{
			get
			{
				return this._index;
			}
			set
			{
				this._index = value;
			}
		}

		internal bool IsSimple
		{
			get
			{
				return ((((this._width != Int32.MinValue) && (this._index == 0)) && (!this._hidden && (this._styleID == null))) && (this._span == Int32.MinValue));
			}
		}

		public int Span
		{
			get
			{
				if (this._span == Int32.MinValue)
				{
					return 0;
				}
				return this._span;
			}
			set
			{
				this._span = value;
			}
		}

		public string StyleID
		{
			get
			{
				return this._styleID;
			}
			set
			{
				this._styleID = value;
			}
		}

		public WorksheetTable Table
		{
			get
			{
				return this._table;
			}
		}

		public int Width
		{
			get
			{
				if ((this._width == Int32.MinValue) && (this._table != null))
				{
					return (int) this._table.DefaultColumnWidth;
				}
				return this._width;
			}
			set
			{
				this._width = value;
			}
		}
	}

	public sealed class WorksheetColumnCollection : CollectionBase, IWriter, ICodeWriter
	{
		// Fields
		private WorksheetTable _table;

		// Methods
		internal WorksheetColumnCollection(WorksheetTable table)
		{
			if (table == null)
			{
				throw new ArgumentNullException("table");
			}
			this._table = table;
		}

		public WorksheetColumn Add()
		{
			WorksheetColumn column = new WorksheetColumn();
			this.Add(column);
			return column;
		}

		public int Add(WorksheetColumn column)
		{
			if (column == null)
			{
				throw new ArgumentNullException("column");
			}
			column._table = this._table;
			return base.InnerList.Add(column);
		}

		public WorksheetColumn Add(int width)
		{
			WorksheetColumn column = new WorksheetColumn();
			column.Width = width;
			this.Add(column);
			return column;
		}

		void ICodeWriter.WriteTo(CodeTypeDeclaration type, CodeMemberMethod method, CodeExpression targetObject)
		{
			for (int i = 0; i < base.InnerList.Count; i++)
			{
				WorksheetColumn column = this[i];
				if (column.IsSimple)
				{
					method.Statements.Add(new CodeMethodInvokeExpression(targetObject, "Add", new CodeExpression[] { new CodePrimitiveExpression(column.Width) }));
				}
				else
				{
					string name = "column" + i.ToString(CultureInfo.InvariantCulture);
					CodeVariableDeclarationStatement statement = new CodeVariableDeclarationStatement(typeof(WorksheetColumn), name, new CodeMethodInvokeExpression(targetObject, "Add", new CodeExpression[0]));
					method.Statements.Add(statement);
					((ICodeWriter) column).WriteTo(type, method, new CodeVariableReferenceExpression(name));
				}
			}
		}

		void IWriter.WriteXml(XmlWriter writer)
		{
			for (int i = 0; i < base.InnerList.Count; i++)
			{
				((IWriter) base.InnerList[i]).WriteXml(writer);
			}
		}

		public bool Contains(WorksheetColumn item)
		{
			return base.InnerList.Contains(item);
		}

		public void CopyTo(WorksheetColumn[] array, int index)
		{
			base.InnerList.CopyTo(array, index);
		}

		public int IndexOf(WorksheetColumn item)
		{
			return base.InnerList.IndexOf(item);
		}

		public void Insert(int index, WorksheetColumn item)
		{
			base.InnerList.Insert(index, item);
		}

		public void Remove(WorksheetColumn item)
		{
			base.InnerList.Remove(item);
		}

		// Properties
		public WorksheetColumn this[int index]
		{
			get
			{
				return (WorksheetColumn) base.InnerList[index];
			}
		}
	}

	public sealed class WorksheetComment : IWriter, IReader, ICodeWriter
	{
		// Fields
		private string _author;
		private WorksheetCellData _data;
		private bool _showAlways;

		// Methods
		internal WorksheetComment()
		{
		}

		void ICodeWriter.WriteTo(CodeTypeDeclaration type, CodeMemberMethod method, CodeExpression targetObject)
		{
			if (this._showAlways)
			{
				Util.AddAssignment(method, targetObject, "ShowAlways", this._showAlways);
			}
			if (this._author != null)
			{
				Util.AddAssignment(method, targetObject, "Author", this._author);
			}
			if (this._data != null)
			{
				Util.Traverse(type, this._data, method, targetObject, "Data");
			}
		}

		void IReader.ReadXml(XmlElement element)
		{
			if (!IsElement(element))
			{
				throw new ArgumentException("Invalid element", "element");
			}
			this._showAlways = Util.GetAttribute(element, "ShowAlways", "urn:schemas-microsoft-com:office:spreadsheet", false);
			this._author = Util.GetAttribute(element, "Author", "urn:schemas-microsoft-com:office:spreadsheet");
			foreach (XmlNode node in element.ChildNodes)
			{
				XmlElement element2 = node as XmlElement;
				if ((element2 != null) && WorksheetCellData.IsElement(element2))
				{
					((IReader) this.Data).ReadXml(element2);
				}
			}
		}

		void IWriter.WriteXml(XmlWriter writer)
		{
			writer.WriteStartElement("s", "Comment", "urn:schemas-microsoft-com:office:spreadsheet");
			if (this._showAlways)
			{
				writer.WriteAttributeString("s", "ShowAlways", "urn:schemas-microsoft-com:office:spreadsheet", this._showAlways.ToString(CultureInfo.InvariantCulture));
			}
			if (this._author != null)
			{
				writer.WriteAttributeString("s", "Author", "urn:schemas-microsoft-com:office:spreadsheet", this._author);
			}
			if (this._data != null)
			{
				((IWriter) this._data).WriteXml(writer);
			}
			writer.WriteEndElement();
		}

		internal static bool IsElement(XmlElement element)
		{
			return Util.IsElement(element, "Comment", "urn:schemas-microsoft-com:office:spreadsheet");
		}

		// Properties
		public string Author
		{
			get
			{
				return this._author;
			}
			set
			{
				this._author = value;
			}
		}

		public WorksheetCellData Data
		{
			get
			{
				if (this._data == null)
				{
					this._data = new WorksheetCellData(this);
				}
				return this._data;
			}
		}

		public bool ShowAlways
		{
			get
			{
				return this._showAlways;
			}
			set
			{
				this._showAlways = value;
			}
		}
	}

	public sealed class WorksheetNamedCellCollection : CollectionBase
	{
		// Methods
		internal WorksheetNamedCellCollection()
		{
		}

		public int Add(string name)
		{
			if (name == null)
			{
				throw new ArgumentNullException("name");
			}
			return base.InnerList.Add(name);
		}

		public bool Contains(string item)
		{
			return base.InnerList.Contains(item);
		}

		public void CopyTo(string[] array, int index)
		{
			base.InnerList.CopyTo(array, index);
		}

		public int IndexOf(string item)
		{
			return base.InnerList.IndexOf(item);
		}

		public void Insert(int index, string item)
		{
			base.InnerList.Insert(index, item);
		}

		public void Remove(string item)
		{
			base.InnerList.Remove(item);
		}

		// Properties
		public string this[int index]
		{
			get
			{
				return (string) base.InnerList[index];
			}
		}
	}
	public sealed class WorksheetNamedRange : IWriter, IReader
	{
		// Fields
		private bool _hidden;
		private string _name;
		private string _refersTo;

		// Methods
		internal WorksheetNamedRange()
		{
		}

		public WorksheetNamedRange(string name)
		{
			if (name == null)
			{
				throw new ArgumentNullException("name");
			}
			this._name = name;
		}

		public WorksheetNamedRange(string name, string refersTo, bool hidden) : this(name)
		{
			this._refersTo = refersTo;
			this._hidden = hidden;
		}

		void IReader.ReadXml(XmlElement element)
		{
			if (!IsElement(element))
			{
				throw new ArgumentException("Invalid element", "element");
			}
			this._name = Util.GetAttribute(element, "Name", "urn:schemas-microsoft-com:office:spreadsheet");
			this._refersTo = Util.GetAttribute(element, "RefersTo", "urn:schemas-microsoft-com:office:spreadsheet");
			this._hidden = Util.GetAttribute(element, "Hidden", "urn:schemas-microsoft-com:office:spreadsheet", false);
		}

		void IWriter.WriteXml(XmlWriter writer)
		{
			writer.WriteStartElement("s", "NamedRange", "urn:schemas-microsoft-com:office:spreadsheet");
			if (this._name != null)
			{
				writer.WriteAttributeString("s", "Name", "urn:schemas-microsoft-com:office:spreadsheet", this._name);
			}
			if (this._refersTo != null)
			{
				writer.WriteAttributeString("s", "RefersTo", "urn:schemas-microsoft-com:office:spreadsheet", this._refersTo);
			}
			if (this._hidden)
			{
				writer.WriteAttributeString("s", "Hidden", "urn:schemas-microsoft-com:office:spreadsheet", "1");
			}
			writer.WriteEndElement();
		}

		internal static bool IsElement(XmlElement element)
		{
			return Util.IsElement(element, "NamedRange", "urn:schemas-microsoft-com:office:spreadsheet");
		}

		// Properties
		public bool Hidden
		{
			get
			{
				return this._hidden;
			}
			set
			{
				this._hidden = value;
			}
		}

		public string Name
		{
			get
			{
				return this._name;
			}
		}

		public string RefersTo
		{
			get
			{
				return this._refersTo;
			}
			set
			{
				this._refersTo = value;
			}
		}
	}

	public sealed class WorksheetNamedRangeCollection : CollectionBase, IWriter, ICodeWriter, IReader
	{
		// Methods
		internal WorksheetNamedRangeCollection()
		{
		}

		public int Add(WorksheetNamedRange namedRange)
		{
			if (namedRange == null)
			{
				throw new ArgumentNullException("namedRange");
			}
			return base.InnerList.Add(namedRange);
		}

		void ICodeWriter.WriteTo(CodeTypeDeclaration type, CodeMemberMethod method, CodeExpression targetObject)
		{
			for (int i = 0; i < base.InnerList.Count; i++)
			{
				WorksheetNamedRange range = this[i];
				method.Statements.Add(new CodeMethodInvokeExpression(targetObject, "Add", new CodeExpression[] { new CodeObjectCreateExpression(new CodeTypeReference(typeof(WorksheetNamedRange)), new CodeExpression[] { new CodePrimitiveExpression(range.Name), new CodePrimitiveExpression(range.RefersTo), new CodePrimitiveExpression(range.Hidden) }) }));
			}
		}

		void IReader.ReadXml(XmlElement element)
		{
			if (!IsElement(element))
			{
				throw new ArgumentException("Invalid element", "element");
			}
			foreach (XmlNode node in element.ChildNodes)
			{
				XmlElement element2 = node as XmlElement;
				if ((element2 != null) && WorksheetNamedRange.IsElement(element2))
				{
					WorksheetNamedRange namedRange = new WorksheetNamedRange();
					((IReader) namedRange).ReadXml(element2);
					this.Add(namedRange);
				}
			}
		}

		void IWriter.WriteXml(XmlWriter writer)
		{
			writer.WriteStartElement("s", "Names", "urn:schemas-microsoft-com:office:spreadsheet");
			for (int i = 0; i < base.InnerList.Count; i++)
			{
				((IWriter) base.InnerList[i]).WriteXml(writer);
			}
			writer.WriteEndElement();
		}

		public bool Contains(WorksheetNamedRange item)
		{
			return base.InnerList.Contains(item);
		}

		public void CopyTo(WorksheetNamedRange[] array, int index)
		{
			base.InnerList.CopyTo(array, index);
		}

		public int IndexOf(WorksheetNamedRange item)
		{
			return base.InnerList.IndexOf(item);
		}

		public void Insert(int index, WorksheetNamedRange item)
		{
			base.InnerList.Insert(index, item);
		}

		internal static bool IsElement(XmlElement element)
		{
			return Util.IsElement(element, "Names", "urn:schemas-microsoft-com:office:spreadsheet");
		}

		public void Remove(WorksheetNamedRange item)
		{
			base.InnerList.Remove(item);
		}

		// Properties
		public WorksheetNamedRange this[int index]
		{
			get
			{
				return (WorksheetNamedRange) base.InnerList[index];
			}
		}
	}

	public sealed class WorksheetOptions : IWriter, IReader, ICodeWriter
	{
		// Fields
		private int _activePane = Int32.MinValue;
		private bool _fitToPage;
		private bool _freezePanes;
		private string _gridLineColor;
		private int _leftColumnRightPane = Int32.MinValue;
		private WorksheetPageSetup _pageSetup;
		private WorksheetPrintOptions _print;
		private bool _protectObjects;
		private bool _protectScenarios;
		private bool _selected;
		private int _splitHorizontal = Int32.MinValue;
		private int _splitVertical = Int32.MinValue;
		private int _topRowBottomPane = Int32.MinValue;
		private int _topRowVisible = Int32.MinValue;
		private string _viewableRange;

		// Methods
		internal WorksheetOptions()
		{
		}

		void ICodeWriter.WriteTo(CodeTypeDeclaration type, CodeMemberMethod method, CodeExpression targetObject)
		{
			Util.AddComment(method, "Options");
			if (this._selected)
			{
				Util.AddAssignment(method, targetObject, "Selected", this._selected);
			}
			if (this._topRowVisible != Int32.MinValue)
			{
				Util.AddAssignment(method, targetObject, "TopRowVisible", this._topRowVisible);
			}
			if (this._freezePanes)
			{
				Util.AddAssignment(method, targetObject, "FreezePanes", this._freezePanes);
			}
			if (this._fitToPage)
			{
				Util.AddAssignment(method, targetObject, "FitToPage", this._fitToPage);
			}
			if (this._splitHorizontal != Int32.MinValue)
			{
				Util.AddAssignment(method, targetObject, "SplitHorizontal", this._splitHorizontal);
			}
			if (this._topRowBottomPane != Int32.MinValue)
			{
				Util.AddAssignment(method, targetObject, "TopRowBottomPane", this._topRowBottomPane);
			}
			if (this._splitVertical != Int32.MinValue)
			{
				Util.AddAssignment(method, targetObject, "SplitVertical", this._splitVertical);
			}
			if (this._leftColumnRightPane != Int32.MinValue)
			{
				Util.AddAssignment(method, targetObject, "LeftColumnRightPane", this._leftColumnRightPane);
			}
			if (this._activePane != Int32.MinValue)
			{
				Util.AddAssignment(method, targetObject, "ActivePane", this._activePane);
			}
			if (this._viewableRange != null)
			{
				Util.AddAssignment(method, targetObject, "ViewableRange", this._viewableRange);
			}
			if (this._gridLineColor != null)
			{
				Util.AddAssignment(method, targetObject, "GridlineColor", this._gridLineColor);
			}
			Util.AddAssignment(method, targetObject, "ProtectObjects", this._protectObjects);
			Util.AddAssignment(method, targetObject, "ProtectScenarios", this._protectScenarios);
			if (this._pageSetup != null)
			{
				Util.Traverse(type, this._pageSetup, method, targetObject, "PageSetup");
			}
			if (this._print != null)
			{
				Util.Traverse(type, this._print, method, targetObject, "Print");
			}
		}

		void IReader.ReadXml(XmlElement element)
		{
			if (!IsElement(element))
			{
				throw new ArgumentException("Invalid element", "element");
			}
			foreach (XmlNode node in element.ChildNodes)
			{
				XmlElement element2 = node as XmlElement;
				if (element2 != null)
				{
					if (Util.IsElement(element2, "Selected", "urn:schemas-microsoft-com:office:excel"))
					{
						this._selected = true;
					}
					else
					{
						if (Util.IsElement(element2, "TopRowVisible", "urn:schemas-microsoft-com:office:excel"))
						{
							this._topRowVisible = Util.GetAttribute(element2, "TopRowVisible", "urn:schemas-microsoft-com:office:excel", Int32.MinValue);
							continue;
						}
						if (Util.IsElement(element2, "FreezePanes", "urn:schemas-microsoft-com:office:excel"))
						{
							this._freezePanes = true;
							continue;
						}
						if (Util.IsElement(element2, "SplitHorizontal", "urn:schemas-microsoft-com:office:excel"))
						{
							this._splitHorizontal = int.Parse(element2.InnerText, CultureInfo.InvariantCulture);
							continue;
						}
						if (Util.IsElement(element2, "TopRowBottomPane", "urn:schemas-microsoft-com:office:excel"))
						{
							this._topRowBottomPane = int.Parse(element2.InnerText, CultureInfo.InvariantCulture);
							continue;
						}
						if (Util.IsElement(element2, "SplitVertical", "urn:schemas-microsoft-com:office:excel"))
						{
							this._splitVertical = int.Parse(element2.InnerText, CultureInfo.InvariantCulture);
							continue;
						}
						if (Util.IsElement(element2, "LeftColumnRightPane", "urn:schemas-microsoft-com:office:excel"))
						{
							this._leftColumnRightPane = int.Parse(element2.InnerText, CultureInfo.InvariantCulture);
							continue;
						}
						if (Util.IsElement(element2, "ActivePane", "urn:schemas-microsoft-com:office:excel"))
						{
							this._activePane = int.Parse(element2.InnerText, CultureInfo.InvariantCulture);
							continue;
						}
						if (Util.IsElement(element2, "ViewableRange", "urn:schemas-microsoft-com:office:excel"))
						{
							this._viewableRange = element2.InnerText;
							continue;
						}
						if (Util.IsElement(element2, "GridlineColor", "urn:schemas-microsoft-com:office:excel"))
						{
							this._gridLineColor = element2.InnerText;
							continue;
						}
						if (Util.IsElement(element2, "ProtectObjects", "urn:schemas-microsoft-com:office:excel"))
						{
							this._protectObjects = bool.Parse(element2.InnerText);
							continue;
						}
						if (Util.IsElement(element2, "ProtectScenarios", "urn:schemas-microsoft-com:office:excel"))
						{
							this._protectScenarios = bool.Parse(element2.InnerText);
							continue;
						}
						if (Util.IsElement(element2, "FitToPage", "urn:schemas-microsoft-com:office:excel"))
						{
							this._fitToPage = true;
							continue;
						}
						if (WorksheetPageSetup.IsElement(element2))
						{
							((IReader) this.PageSetup).ReadXml(element2);
							continue;
						}
						if (WorksheetPrintOptions.IsElement(element2))
						{
							((IReader) this.Print).ReadXml(element2);
						}
					}
				}
			}
		}

		void IWriter.WriteXml(XmlWriter writer)
		{
			writer.WriteStartElement("x", "WorksheetOptions", "urn:schemas-microsoft-com:office:excel");
			if (this._selected)
			{
				writer.WriteElementString("Selected", "urn:schemas-microsoft-com:office:excel", "");
			}
			if (this._fitToPage)
			{
				writer.WriteElementString("FitToPage", "urn:schemas-microsoft-com:office:excel", "");
			}
			if (this._topRowVisible != Int32.MinValue)
			{
				writer.WriteElementString("TopRowVisible", "urn:schemas-microsoft-com:office:excel", this._topRowVisible.ToString(CultureInfo.InvariantCulture));
			}
			if (this._freezePanes)
			{
				writer.WriteElementString("FreezePanes", "urn:schemas-microsoft-com:office:excel", "");
			}
			if (this._splitHorizontal != Int32.MinValue)
			{
				writer.WriteElementString("SplitHorizontal", "urn:schemas-microsoft-com:office:excel", this._splitHorizontal.ToString(CultureInfo.InvariantCulture));
			}
			if (this._topRowBottomPane != Int32.MinValue)
			{
				writer.WriteElementString("TopRowBottomPane", "urn:schemas-microsoft-com:office:excel", this._topRowBottomPane.ToString(CultureInfo.InvariantCulture));
			}
			if (this._splitVertical != Int32.MinValue)
			{
				writer.WriteElementString("SplitVertical", "urn:schemas-microsoft-com:office:excel", this._splitVertical.ToString(CultureInfo.InvariantCulture));
			}
			if (this._leftColumnRightPane != Int32.MinValue)
			{
				writer.WriteElementString("LeftColumnRightPane", "urn:schemas-microsoft-com:office:excel", this._leftColumnRightPane.ToString(CultureInfo.InvariantCulture));
			}
			if (this._activePane != Int32.MinValue)
			{
				writer.WriteElementString("ActivePane", "urn:schemas-microsoft-com:office:excel", this._activePane.ToString(CultureInfo.InvariantCulture));
			}
			if (this._viewableRange != null)
			{
				writer.WriteElementString("ViewableRange", "urn:schemas-microsoft-com:office:excel", this._viewableRange);
			}
			if (this._gridLineColor != null)
			{
				writer.WriteElementString("GridlineColor", "urn:schemas-microsoft-com:office:excel", this._gridLineColor);
			}
			writer.WriteElementString("ProtectObjects", "urn:schemas-microsoft-com:office:excel", this._protectObjects.ToString(CultureInfo.InvariantCulture));
			writer.WriteElementString("ProtectScenarios", "urn:schemas-microsoft-com:office:excel", this._protectScenarios.ToString(CultureInfo.InvariantCulture));
			if (this._pageSetup != null)
			{
				((IWriter) this._pageSetup).WriteXml(writer);
			}
			if (this._print != null)
			{
				((IWriter) this._print).WriteXml(writer);
			}
			writer.WriteEndElement();
		}

		internal static bool IsElement(XmlElement element)
		{
			return Util.IsElement(element, "WorksheetOptions", "urn:schemas-microsoft-com:office:excel");
		}

		// Properties
		public int ActivePane
		{
			get
			{
				return this._activePane;
			}
			set
			{
				this._activePane = value;
			}
		}

		public bool FitToPage
		{
			get
			{
				return this._fitToPage;
			}
			set
			{
				this._fitToPage = value;
			}
		}

		public bool FreezePanes
		{
			get
			{
				return this._freezePanes;
			}
			set
			{
				this._freezePanes = value;
			}
		}

		public string GridLineColor
		{
			get
			{
				return this._gridLineColor;
			}
			set
			{
				this._gridLineColor = value;
			}
		}

		public int LeftColumnRightPane
		{
			get
			{
				return this._leftColumnRightPane;
			}
			set
			{
				this._leftColumnRightPane = value;
			}
		}

		public WorksheetPageSetup PageSetup
		{
			get
			{
				if (this._pageSetup == null)
				{
					this._pageSetup = new WorksheetPageSetup();
				}
				return this._pageSetup;
			}
		}

		public WorksheetPrintOptions Print
		{
			get
			{
				if (this._print == null)
				{
					this._print = new WorksheetPrintOptions();
				}
				return this._print;
			}
		}

		public bool ProtectObjects
		{
			get
			{
				return this._protectObjects;
			}
			set
			{
				this._protectObjects = value;
			}
		}

		public bool ProtectScenarios
		{
			get
			{
				return this._protectScenarios;
			}
			set
			{
				this._protectScenarios = value;
			}
		}

		public bool Selected
		{
			get
			{
				return this._selected;
			}
			set
			{
				this._selected = value;
			}
		}

		public int SplitHorizontal
		{
			get
			{
				return this._splitHorizontal;
			}
			set
			{
				this._splitHorizontal = value;
			}
		}

		public int SplitVertical
		{
			get
			{
				return this._splitVertical;
			}
			set
			{
				this._splitVertical = value;
			}
		}

		public int TopRowBottomPane
		{
			get
			{
				return this._topRowBottomPane;
			}
			set
			{
				this._topRowBottomPane = value;
			}
		}

		public int TopRowVisible
		{
			get
			{
				return this._topRowVisible;
			}
			set
			{
				this._topRowVisible = value;
			}
		}

		public string ViewableRange
		{
			get
			{
				return this._viewableRange;
			}
			set
			{
				this._viewableRange = value;
			}
		}
	}

	public sealed class WorksheetPageFooter : IWriter, IReader, ICodeWriter
	{
		// Fields
		private string _data;
		private float _margin = 0.5f;

		// Methods
		internal WorksheetPageFooter()
		{
		}

		void ICodeWriter.WriteTo(CodeTypeDeclaration type, CodeMemberMethod method, CodeExpression targetObject)
		{
			if (this._data != null)
			{
				Util.AddAssignment(method, targetObject, "Data", this._data);
			}
			if (this._margin != 0.5)
			{
				Util.AddAssignment(method, targetObject, "Margin", this._margin);
			}
		}

		void IReader.ReadXml(XmlElement element)
		{
			if (!IsElement(element))
			{
				throw new ArgumentException("Invalid element", "element");
			}
			this._margin = Util.GetAttribute(element, "Margin", "urn:schemas-microsoft-com:office:excel", (float) 0.5f);
			this._data = Util.GetAttribute(element, "Data", "urn:schemas-microsoft-com:office:excel");
		}

		void IWriter.WriteXml(XmlWriter writer)
		{
			writer.WriteStartElement("x", "Footer", "urn:schemas-microsoft-com:office:excel");
			if (this._data != null)
			{
				writer.WriteAttributeString("Data", "urn:schemas-microsoft-com:office:excel", this._data);
			}
			if (this._margin != 0.5)
			{
				writer.WriteAttributeString("Margin", "urn:schemas-microsoft-com:office:excel", this._margin.ToString(CultureInfo.InvariantCulture));
			}
			writer.WriteEndElement();
		}

		internal static bool IsElement(XmlElement element)
		{
			return Util.IsElement(element, "Footer", "urn:schemas-microsoft-com:office:excel");
		}

		// Properties
		public string Data
		{
			get
			{
				return this._data;
			}
			set
			{
				this._data = value;
			}
		}

		public float Margin
		{
			get
			{
				return this._margin;
			}
			set
			{
				this._margin = value;
			}
		}
	}

	public sealed class WorksheetPageHeader : IWriter, IReader, ICodeWriter
	{
		// Fields
		private string _data;
		private float _margin = 0.5f;

		// Methods
		internal WorksheetPageHeader()
		{
		}

		void ICodeWriter.WriteTo(CodeTypeDeclaration type, CodeMemberMethod method, CodeExpression targetObject)
		{
			if (this._data != null)
			{
				Util.AddAssignment(method, targetObject, "Data", this._data);
			}
			if (this._margin != 0.5)
			{
				Util.AddAssignment(method, targetObject, "Margin", this._margin);
			}
		}

		void IReader.ReadXml(XmlElement element)
		{
			if (!IsElement(element))
			{
				throw new ArgumentException("Invalid element", "element");
			}
			this._margin = Util.GetAttribute(element, "Margin", "urn:schemas-microsoft-com:office:excel", (float) 0.5f);
			this._data = Util.GetAttribute(element, "Data", "urn:schemas-microsoft-com:office:excel");
		}

		void IWriter.WriteXml(XmlWriter writer)
		{
			writer.WriteStartElement("x", "Header", "urn:schemas-microsoft-com:office:excel");
			if (this._data != null)
			{
				writer.WriteAttributeString("Data", "urn:schemas-microsoft-com:office:excel", this._data);
			}
			if (this._margin != 0.5)
			{
				writer.WriteAttributeString("Margin", "urn:schemas-microsoft-com:office:excel", this._margin.ToString(CultureInfo.InvariantCulture));
			}
			writer.WriteEndElement();
		}

		internal static bool IsElement(XmlElement element)
		{
			return Util.IsElement(element, "Header", "urn:schemas-microsoft-com:office:excel");
		}

		// Properties
		public string Data
		{
			get
			{
				return this._data;
			}
			set
			{
				this._data = value;
			}
		}

		public float Margin
		{
			get
			{
				return this._margin;
			}
			set
			{
				this._margin = value;
			}
		}
	}

	public sealed class WorksheetPageLayout : IWriter, IReader, ICodeWriter
	{
		// Fields
		private bool _centerHorizontal;
		private bool _centerVertical;
		private Orientation _orientation;

		// Methods
		internal WorksheetPageLayout()
		{
		}

		void ICodeWriter.WriteTo(CodeTypeDeclaration type, CodeMemberMethod method, CodeExpression targetObject)
		{
			if (this._orientation != Orientation.NotSet)
			{
				Util.AddAssignment(method, targetObject, "Orientation", this._orientation);
			}
			if (this._centerHorizontal)
			{
				Util.AddAssignment(method, targetObject, "CenterHorizontal", true);
			}
			if (this._centerVertical)
			{
				Util.AddAssignment(method, targetObject, "CenterVertical", true);
			}
		}

		void IReader.ReadXml(XmlElement element)
		{
			if (!IsElement(element))
			{
				throw new ArgumentException("Invalid element", "element");
			}
			string str = Util.GetAttribute(element, "Orientation", "urn:schemas-microsoft-com:office:excel");
			if ((str != null) && (str.Length > 0))
			{
				this._orientation = (Orientation) Enum.Parse(typeof(Orientation), str);
			}
			this._centerHorizontal = Util.GetAttribute(element, "CenterHorizontal", "urn:schemas-microsoft-com:office:excel", false);
			this._centerVertical = Util.GetAttribute(element, "CenterVertical", "urn:schemas-microsoft-com:office:excel", false);
		}

		void IWriter.WriteXml(XmlWriter writer)
		{
			writer.WriteStartElement("x", "Layout", "urn:schemas-microsoft-com:office:excel");
			if (this._orientation != Orientation.NotSet)
			{
				writer.WriteAttributeString("Orientation", "urn:schemas-microsoft-com:office:excel", this._orientation.ToString(CultureInfo.InvariantCulture));
			}
			if (this._centerHorizontal)
			{
				writer.WriteAttributeString("CenterHorizontal", "urn:schemas-microsoft-com:office:excel", "1");
			}
			if (this._centerVertical)
			{
				writer.WriteAttributeString("CenterVertical", "urn:schemas-microsoft-com:office:excel", "1");
			}
			writer.WriteEndElement();
		}

		internal static bool IsElement(XmlElement element)
		{
			return Util.IsElement(element, "Layout", "urn:schemas-microsoft-com:office:excel");
		}

		// Properties
		public bool CenterHorizontal
		{
			get
			{
				return this._centerHorizontal;
			}
			set
			{
				this._centerHorizontal = value;
			}
		}

		public bool CenterVertical
		{
			get
			{
				return this._centerVertical;
			}
			set
			{
				this._centerVertical = value;
			}
		}

		public Orientation Orientation
		{
			get
			{
				return this._orientation;
			}
			set
			{
				this._orientation = value;
			}
		}
	}

	public sealed class WorksheetPageMargins : IWriter, IReader, ICodeWriter
	{
		// Fields
		private float _bottom = 1f;
		private float _left = 0.75f;
		private float _right = 0.75f;
		private float _top = 1f;

		// Methods
		internal WorksheetPageMargins()
		{
		}

		void ICodeWriter.WriteTo(CodeTypeDeclaration type, CodeMemberMethod method, CodeExpression targetObject)
		{
			Util.AddAssignment(method, targetObject, "Bottom", this._bottom);
			Util.AddAssignment(method, targetObject, "Left", this._left);
			Util.AddAssignment(method, targetObject, "Right", this._right);
			Util.AddAssignment(method, targetObject, "Top", this._top);
		}

		void IReader.ReadXml(XmlElement element)
		{
			if (!IsElement(element))
			{
				throw new ArgumentException("Invalid element", "element");
			}
			this._bottom = Util.GetAttribute(element, "Bottom", "urn:schemas-microsoft-com:office:excel", (float) 1f);
			this._left = Util.GetAttribute(element, "Left", "urn:schemas-microsoft-com:office:excel", (float) 0.75f);
			this._right = Util.GetAttribute(element, "Right", "urn:schemas-microsoft-com:office:excel", (float) 0.75f);
			this._top = Util.GetAttribute(element, "Top", "urn:schemas-microsoft-com:office:excel", (float) 1f);
		}

		void IWriter.WriteXml(XmlWriter writer)
		{
			writer.WriteStartElement("x", "PageMargins", "urn:schemas-microsoft-com:office:excel");
			writer.WriteAttributeString("Bottom", "urn:schemas-microsoft-com:office:excel", this._bottom.ToString(CultureInfo.InvariantCulture));
			writer.WriteAttributeString("Left", "urn:schemas-microsoft-com:office:excel", this._left.ToString(CultureInfo.InvariantCulture));
			writer.WriteAttributeString("Right", "urn:schemas-microsoft-com:office:excel", this._right.ToString(CultureInfo.InvariantCulture));
			writer.WriteAttributeString("Top", "urn:schemas-microsoft-com:office:excel", this._top.ToString(CultureInfo.InvariantCulture));
			writer.WriteEndElement();
		}

		internal static bool IsElement(XmlElement element)
		{
			return Util.IsElement(element, "PageMargins", "urn:schemas-microsoft-com:office:excel");
		}

		// Properties
		public float Bottom
		{
			get
			{
				return this._bottom;
			}
			set
			{
				this._bottom = value;
			}
		}

		public float Left
		{
			get
			{
				return this._left;
			}
			set
			{
				this._left = value;
			}
		}

		public float Right
		{
			get
			{
				return this._right;
			}
			set
			{
				this._right = value;
			}
		}

		public float Top
		{
			get
			{
				return this._top;
			}
			set
			{
				this._top = value;
			}
		}
	}

	public sealed class WorksheetPageSetup : IWriter, IReader, ICodeWriter
	{
		// Fields
		private WorksheetPageFooter _footer;
		private WorksheetPageHeader _header;
		private WorksheetPageLayout _layout;
		private WorksheetPageMargins _pageMargins;

		// Methods
		internal WorksheetPageSetup()
		{
		}

		void ICodeWriter.WriteTo(CodeTypeDeclaration type, CodeMemberMethod method, CodeExpression targetObject)
		{
			if (this._layout != null)
			{
				Util.Traverse(type, this._layout, method, targetObject, "Layout");
			}
			if (this._header != null)
			{
				Util.Traverse(type, this._header, method, targetObject, "Header");
			}
			if (this._footer != null)
			{
				Util.Traverse(type, this._footer, method, targetObject, "Footer");
			}
			if (this._pageMargins != null)
			{
				Util.Traverse(type, this._pageMargins, method, targetObject, "PageMargins");
			}
		}

		void IReader.ReadXml(XmlElement element)
		{
			if (!IsElement(element))
			{
				throw new ArgumentException("Invalid element", "element");
			}
			foreach (XmlNode node in element.ChildNodes)
			{
				XmlElement element2 = node as XmlElement;
				if (element2 != null)
				{
					if (WorksheetPageLayout.IsElement(element2))
					{
						((IReader) this.Layout).ReadXml(element2);
					}
					else
					{
						if (WorksheetPageHeader.IsElement(element2))
						{
							((IReader) this.Header).ReadXml(element2);
							continue;
						}
						if (WorksheetPageFooter.IsElement(element2))
						{
							((IReader) this.Footer).ReadXml(element2);
							continue;
						}
						if (WorksheetPageMargins.IsElement(element2))
						{
							((IReader) this.PageMargins).ReadXml(element2);
						}
					}
				}
			}
		}

		void IWriter.WriteXml(XmlWriter writer)
		{
			writer.WriteStartElement("x", "PageSetup", "urn:schemas-microsoft-com:office:excel");
			if (this._layout != null)
			{
				((IWriter) this._layout).WriteXml(writer);
			}
			if (this._header != null)
			{
				((IWriter) this._header).WriteXml(writer);
			}
			if (this._footer != null)
			{
				((IWriter) this._footer).WriteXml(writer);
			}
			if (this._pageMargins != null)
			{
				((IWriter) this._pageMargins).WriteXml(writer);
			}
			writer.WriteEndElement();
		}

		internal static bool IsElement(XmlElement element)
		{
			return Util.IsElement(element, "PageSetup", "urn:schemas-microsoft-com:office:excel");
		}

		// Properties
		public WorksheetPageFooter Footer
		{
			get
			{
				if (this._footer == null)
				{
					this._footer = new WorksheetPageFooter();
				}
				return this._footer;
			}
		}

		public WorksheetPageHeader Header
		{
			get
			{
				if (this._header == null)
				{
					this._header = new WorksheetPageHeader();
				}
				return this._header;
			}
		}

		public WorksheetPageLayout Layout
		{
			get
			{
				if (this._layout == null)
				{
					this._layout = new WorksheetPageLayout();
				}
				return this._layout;
			}
		}

		public WorksheetPageMargins PageMargins
		{
			get
			{
				if (this._pageMargins == null)
				{
					this._pageMargins = new WorksheetPageMargins();
				}
				return this._pageMargins;
			}
		}
	}

	public sealed class WorksheetPrintOptions : IWriter, IReader, ICodeWriter
	{
		// Fields
		private bool _blackAndWhite;
		private PrintCommentsLayout _commentsLayout;
		private bool _draftQuality;
		private int _fitHeight = 1;
		private int _fitWidth = 1;
		private bool _gridLines;
		private int _horizontalResolution = 600;
		private bool _leftToRight;
		private int _paperSizeIndex = Int32.MinValue;
		private PrintErrorsOption _printErrors;
		private bool _rowColHeadings;
		private int _scale = 100;
		private bool _validPrinterInfo;
		private int _verticalResolution = 600;

		// Methods
		internal WorksheetPrintOptions()
		{
		}

		void ICodeWriter.WriteTo(CodeTypeDeclaration type, CodeMemberMethod method, CodeExpression targetObject)
		{
			if (this._paperSizeIndex != Int32.MinValue)
			{
				Util.AddAssignment(method, targetObject, "PaperSizeIndex", this._paperSizeIndex);
			}
			if (this._horizontalResolution != 600)
			{
				Util.AddAssignment(method, targetObject, "HorizontalResolution", this._horizontalResolution);
			}
			if (this._verticalResolution != 600)
			{
				Util.AddAssignment(method, targetObject, "VerticalResolution", this._verticalResolution);
			}
			if (this._blackAndWhite)
			{
				Util.AddAssignment(method, targetObject, "BlackAndWhite", true);
			}
			if (this._draftQuality)
			{
				Util.AddAssignment(method, targetObject, "DraftQuality", true);
			}
			if (this._gridLines)
			{
				Util.AddAssignment(method, targetObject, "Gridlines", true);
			}
			if (this._scale != 100)
			{
				Util.AddAssignment(method, targetObject, "Scale", this._scale);
			}
			if (this._fitWidth != 1)
			{
				Util.AddAssignment(method, targetObject, "FitWidth", this._fitWidth);
			}
			if (this._fitHeight != 1)
			{
				Util.AddAssignment(method, targetObject, "FitHeight", this._fitHeight);
			}
			if (this._leftToRight)
			{
				Util.AddAssignment(method, targetObject, "LeftToRight", true);
			}
			if (this._rowColHeadings)
			{
				Util.AddAssignment(method, targetObject, "RowColHeadings", true);
			}
			if (this._printErrors != PrintErrorsOption.Displayed)
			{
				Util.AddAssignment(method, targetObject, "PrintErrors", this._printErrors);
			}
			if (this._commentsLayout != PrintCommentsLayout.None)
			{
				Util.AddAssignment(method, targetObject, "CommentsLayout", this._commentsLayout);
			}
			if (this._validPrinterInfo)
			{
				Util.AddAssignment(method, targetObject, "ValidPrinterInfo", true);
			}
		}

		void IReader.ReadXml(XmlElement element)
		{
			if (!IsElement(element))
			{
				throw new ArgumentException("Invalid element", "element");
			}
			foreach (XmlNode node in element.ChildNodes)
			{
				XmlElement element2 = node as XmlElement;
				if (element2 != null)
				{
					if (Util.IsElement(element2, "Gridlines", "urn:schemas-microsoft-com:office:excel"))
					{
						this._gridLines = true;
					}
					else
					{
						if (Util.IsElement(element2, "BlackAndWhite", "urn:schemas-microsoft-com:office:excel"))
						{
							this._blackAndWhite = true;
							continue;
						}
						if (Util.IsElement(element2, "DraftQuality", "urn:schemas-microsoft-com:office:excel"))
						{
							this._draftQuality = true;
							continue;
						}
						if (Util.IsElement(element2, "ValidPrinterInfo", "urn:schemas-microsoft-com:office:excel"))
						{
							this._validPrinterInfo = true;
							continue;
						}
						if (Util.IsElement(element2, "PaperSizeIndex", "urn:schemas-microsoft-com:office:excel"))
						{
							this._paperSizeIndex = int.Parse(element2.InnerText, CultureInfo.InvariantCulture);
							continue;
						}
						if (Util.IsElement(element2, "HorizontalResolution", "urn:schemas-microsoft-com:office:excel"))
						{
							this._horizontalResolution = int.Parse(element2.InnerText, CultureInfo.InvariantCulture);
							continue;
						}
						if (Util.IsElement(element2, "VerticalResolution", "urn:schemas-microsoft-com:office:excel"))
						{
							this._verticalResolution = int.Parse(element2.InnerText, CultureInfo.InvariantCulture);
							continue;
						}
						if (Util.IsElement(element2, "RowColHeadings", "urn:schemas-microsoft-com:office:excel"))
						{
							this._rowColHeadings = true;
							continue;
						}
						if (Util.IsElement(element2, "LeftToRight", "urn:schemas-microsoft-com:office:excel"))
						{
							this._leftToRight = true;
							continue;
						}
						if (Util.IsElement(element2, "Scale", "urn:schemas-microsoft-com:office:excel"))
						{
							this._scale = int.Parse(element2.InnerText, CultureInfo.InvariantCulture);
							continue;
						}
						if (Util.IsElement(element2, "FitWidth", "urn:schemas-microsoft-com:office:excel"))
						{
							this._fitWidth = int.Parse(element2.InnerText, CultureInfo.InvariantCulture);
							continue;
						}
						if (Util.IsElement(element2, "FitHeight", "urn:schemas-microsoft-com:office:excel"))
						{
							this._fitHeight = int.Parse(element2.InnerText, CultureInfo.InvariantCulture);
							continue;
						}
						if (Util.IsElement(element2, "PrintErrors", "urn:schemas-microsoft-com:office:excel"))
						{
							this._printErrors = (PrintErrorsOption) Enum.Parse(typeof(PrintErrorsOption), element2.InnerText);
							continue;
						}
						if (Util.IsElement(element2, "CommentsLayout", "urn:schemas-microsoft-com:office:excel"))
						{
							this._commentsLayout = (PrintCommentsLayout) Enum.Parse(typeof(PrintCommentsLayout), element2.InnerText);
						}
					}
				}
			}
		}

		void IWriter.WriteXml(XmlWriter writer)
		{
			writer.WriteStartElement("x", "Print", "urn:schemas-microsoft-com:office:excel");
			if (this._paperSizeIndex != Int32.MinValue)
			{
				writer.WriteElementString("PaperSizeIndex", "urn:schemas-microsoft-com:office:excel", this._paperSizeIndex.ToString(CultureInfo.InvariantCulture));
			}
			writer.WriteElementString("HorizontalResolution", "urn:schemas-microsoft-com:office:excel", this._horizontalResolution.ToString(CultureInfo.InvariantCulture));
			writer.WriteElementString("VerticalResolution", "urn:schemas-microsoft-com:office:excel", this._verticalResolution.ToString(CultureInfo.InvariantCulture));
			if (this._blackAndWhite)
			{
				writer.WriteElementString("BlackAndWhite", "urn:schemas-microsoft-com:office:excel", "");
			}
			if (this._draftQuality)
			{
				writer.WriteElementString("DraftQuality", "urn:schemas-microsoft-com:office:excel", "");
			}
			if (this._gridLines)
			{
				writer.WriteElementString("Gridlines", "urn:schemas-microsoft-com:office:excel", "");
			}
			if (this._scale != 100)
			{
				writer.WriteElementString("Scale", "urn:schemas-microsoft-com:office:excel", this._scale.ToString(CultureInfo.InvariantCulture));
			}
			if (this._fitWidth != 1)
			{
				writer.WriteElementString("FitWidth", "urn:schemas-microsoft-com:office:excel", this._fitWidth.ToString(CultureInfo.InvariantCulture));
			}
			if (this._fitHeight != 1)
			{
				writer.WriteElementString("FitHeight", "urn:schemas-microsoft-com:office:excel", this._fitHeight.ToString(CultureInfo.InvariantCulture));
			}
			if (this._leftToRight)
			{
				writer.WriteElementString("LeftToRight", "urn:schemas-microsoft-com:office:excel", "");
			}
			if (this._rowColHeadings)
			{
				writer.WriteElementString("RowColHeadings", "urn:schemas-microsoft-com:office:excel", "");
			}
			if (this._printErrors != PrintErrorsOption.Displayed)
			{
				writer.WriteElementString("PrintErrors", "urn:schemas-microsoft-com:office:excel", this._printErrors.ToString(CultureInfo.InvariantCulture));
			}
			if (this._commentsLayout != PrintCommentsLayout.None)
			{
				writer.WriteElementString("CommentsLayout", "urn:schemas-microsoft-com:office:excel", this._commentsLayout.ToString(CultureInfo.InvariantCulture));
			}
			if (this._validPrinterInfo)
			{
				writer.WriteElementString("ValidPrinterInfo", "urn:schemas-microsoft-com:office:excel", "");
			}
			writer.WriteEndElement();
		}

		internal static bool IsElement(XmlElement element)
		{
			return Util.IsElement(element, "Print", "urn:schemas-microsoft-com:office:excel");
		}

		// Properties
		public bool BlackAndWhite
		{
			get
			{
				return this._blackAndWhite;
			}
			set
			{
				this._blackAndWhite = value;
			}
		}

		public PrintCommentsLayout CommentsLayout
		{
			get
			{
				return this._commentsLayout;
			}
			set
			{
				this._commentsLayout = value;
			}
		}

		public bool DraftQuality
		{
			get
			{
				return this._draftQuality;
			}
			set
			{
				this._draftQuality = value;
			}
		}

		public int FitHeight
		{
			get
			{
				return this._fitHeight;
			}
			set
			{
				this._fitHeight = value;
			}
		}

		public int FitWidth
		{
			get
			{
				return this._fitWidth;
			}
			set
			{
				this._fitWidth = value;
			}
		}

		public bool GridLines
		{
			get
			{
				return this._gridLines;
			}
			set
			{
				this._gridLines = value;
			}
		}

		public int HorizontalResolution
		{
			get
			{
				return this._horizontalResolution;
			}
			set
			{
				this._horizontalResolution = value;
			}
		}

		public bool LeftToRight
		{
			get
			{
				return this._leftToRight;
			}
			set
			{
				this._leftToRight = value;
			}
		}

		public int PaperSizeIndex
		{
			get
			{
				if (this._paperSizeIndex == Int32.MinValue)
				{
					return 0;
				}
				return this._paperSizeIndex;
			}
			set
			{
				this._paperSizeIndex = value;
			}
		}

		public PrintErrorsOption PrintErrors
		{
			get
			{
				return this._printErrors;
			}
			set
			{
				this._printErrors = value;
			}
		}

		public bool RowColHeadings
		{
			get
			{
				return this._rowColHeadings;
			}
			set
			{
				this._rowColHeadings = value;
			}
		}

		public int Scale
		{
			get
			{
				return this._scale;
			}
			set
			{
				this._scale = value;
			}
		}

		public bool ValidPrinterInfo
		{
			get
			{
				return this._validPrinterInfo;
			}
			set
			{
				this._validPrinterInfo = value;
			}
		}

		public int VerticalResolution
		{
			get
			{
				return this._verticalResolution;
			}
			set
			{
				this._verticalResolution = value;
			}
		}
	}

	public sealed class WorksheetRow : IWriter, IReader, ICodeWriter
	{
		// Fields
		private bool _autoFitHeight = true;
		private WorksheetCellCollection _cells;
		private int _height = Int32.MinValue;
		private int _index;
		internal WorksheetTable _table;

		// Methods
		void ICodeWriter.WriteTo(CodeTypeDeclaration type, CodeMemberMethod method, CodeExpression targetObject)
		{
			if (this._index > 0)
			{
				Util.AddAssignment(method, targetObject, "Index", this._index);
			}
			if (this._height != Int32.MinValue)
			{
				Util.AddAssignment(method, targetObject, "Height", this._height);
			}
			if (!this._autoFitHeight)
			{
				Util.AddAssignment(method, targetObject, "AutoFitHeight", this._autoFitHeight);
			}
			if (this._cells != null)
			{
				Util.Traverse(type, this._cells, method, targetObject, "Cells");
			}
		}

		void IReader.ReadXml(XmlElement element)
		{
			if (!IsElement(element))
			{
				throw new ArgumentException("Invalid element", "element");
			}
			this._index = Util.GetAttribute(element, "Index", "urn:schemas-microsoft-com:office:spreadsheet", 0);
			this._height = Util.GetAttribute(element, "Height", "urn:schemas-microsoft-com:office:spreadsheet", Int32.MinValue);
			this._autoFitHeight = Util.GetAttribute(element, "AutoFitHeight", "urn:schemas-microsoft-com:office:spreadsheet", true);
			foreach (XmlNode node in element.ChildNodes)
			{
				XmlElement element2 = node as XmlElement;
				if ((element2 != null) && WorksheetCell.IsElement(element2))
				{
					WorksheetCell cell = new WorksheetCell();
					((IReader) cell).ReadXml(element2);
					this.Cells.Add(cell);
				}
			}
		}

		void IWriter.WriteXml(XmlWriter writer)
		{
			writer.WriteStartElement("s", "Row", "urn:schemas-microsoft-com:office:spreadsheet");
			if (this._index > 0)
			{
				writer.WriteAttributeString("s", "Index", "urn:schemas-microsoft-com:office:spreadsheet", this._index.ToString(CultureInfo.InvariantCulture));
			}
			if (this._height != Int32.MinValue)
			{
				writer.WriteAttributeString("s", "Height", "urn:schemas-microsoft-com:office:spreadsheet", this._height.ToString(CultureInfo.InvariantCulture));
			}
			if (!this._autoFitHeight)
			{
				writer.WriteAttributeString("s", "AutoFitHeight", "urn:schemas-microsoft-com:office:spreadsheet", "0");
			}
			if (this._cells != null)
			{
				((IWriter) this._cells).WriteXml(writer);
			}
			writer.WriteEndElement();
		}

		internal static bool IsElement(XmlElement element)
		{
			return Util.IsElement(element, "Row", "urn:schemas-microsoft-com:office:spreadsheet");
		}

		// Properties
		public bool AutoFitHeight
		{
			get
			{
				return this._autoFitHeight;
			}
			set
			{
				this._autoFitHeight = value;
			}
		}

		public WorksheetCellCollection Cells
		{
			get
			{
				if (this._cells == null)
				{
					this._cells = new WorksheetCellCollection(this);
				}
				return this._cells;
			}
		}

		public int Height
		{
			get
			{
				if ((this._height == Int32.MinValue) && (this._table != null))
				{
					return (int) this._table.DefaultRowHeight;
				}
				return this._height;
			}
			set
			{
				this._height = value;
			}
		}

		public int Index
		{
			get
			{
				return this._index;
			}
			set
			{
				this._index = value;
			}
		}

		public WorksheetTable Table
		{
			get
			{
				return this._table;
			}
		}
	}

	public sealed class WorksheetRowCollection : CollectionBase, IWriter, ICodeWriter
	{
		// Fields
		private WorksheetTable _table;

		// Methods
		internal WorksheetRowCollection()
		{
		}

		internal WorksheetRowCollection(WorksheetTable table)
		{
			if (table == null)
			{
				throw new ArgumentNullException("table");
			}
			this._table = table;
		}

		public WorksheetRow Add()
		{
			WorksheetRow row = new WorksheetRow();
			this.Add(row);
			return row;
		}

		public int Add(WorksheetRow row)
		{
			if (row == null)
			{
				throw new ArgumentNullException("row");
			}
			row._table = this._table;
			return base.InnerList.Add(row);
		}

		void ICodeWriter.WriteTo(CodeTypeDeclaration type, CodeMemberMethod method, CodeExpression targetObject)
		{
			for (int i = 0; i < base.InnerList.Count; i++)
			{
				Util.AddCommentSeparator(method);
				WorksheetRow row = this[i];
				string name = "Row" + i.ToString(CultureInfo.InvariantCulture);
				CodeVariableDeclarationStatement statement = new CodeVariableDeclarationStatement(typeof(WorksheetRow), name, new CodeMethodInvokeExpression(targetObject, "Add", new CodeExpression[0]));
				method.Statements.Add(statement);
				((ICodeWriter) row).WriteTo(type, method, new CodeVariableReferenceExpression(name));
			}
		}

		void IWriter.WriteXml(XmlWriter writer)
		{
			for (int i = 0; i < base.InnerList.Count; i++)
			{
				((IWriter) base.InnerList[i]).WriteXml(writer);
			}
		}

		public bool Contains(WorksheetRow item)
		{
			return base.InnerList.Contains(item);
		}

		public void CopyTo(WorksheetRow[] array, int index)
		{
			base.InnerList.CopyTo(array, index);
		}

		public int IndexOf(WorksheetRow item)
		{
			return base.InnerList.IndexOf(item);
		}

		public void Insert(int index, WorksheetRow row)
		{
			if (row == null)
			{
				throw new ArgumentNullException("row");
			}
			row._table = this._table;
			base.InnerList.Insert(index, row);
		}

		public void Remove(WorksheetRow row)
		{
			row._table = null;
			base.InnerList.Remove(row);
		}

		// Properties
		public WorksheetRow this[int index]
		{
			get
			{
				return (WorksheetRow) base.InnerList[index];
			}
		}
	}


}
