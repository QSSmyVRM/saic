//ZD 100147 Start
/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
//ZD 100147 End
using System;
using System.Globalization;
using System.Collections;
using System.Collections.Specialized;
using System.Xml;
using System.CodeDom;

namespace myVRM.ExcelXmlWriter
{

	public sealed class Xct : IWriter, IReader, ICodeWriter
	{
		// Fields
		private CrnCollection _operands;
		private int _sheetIndex = int.MinValue;

		// Methods
		void ICodeWriter.WriteTo(CodeTypeDeclaration type, CodeMemberMethod method, CodeExpression targetObject)
		{
			if (this._sheetIndex != int.MinValue)
			{
				Util.AddAssignment(method, targetObject, "SheetIndex", this._sheetIndex);
			}
			if (this._operands != null)
			{
				((ICodeWriter) this._operands).WriteTo(type, method, targetObject);
			}
		}

		void IReader.ReadXml(XmlElement element)
		{
			if (!IsElement(element))
			{
				throw new ArgumentException("Invalid element", "element");
			}
			foreach (XmlNode node in element.ChildNodes)
			{
				XmlElement element2 = node as XmlElement;
				if (element2 != null)
				{
					if (Util.IsElement(element2, "SheetIndex", "urn:schemas-microsoft-com:office:excel"))
					{
						this._sheetIndex = int.Parse(element2.InnerText);
					}
					else if (Crn.IsElement(element2))
					{
						Crn item = new Crn();
						((IReader) item).ReadXml(element2);
						this.Operands.Add(item);
					}
				}
			}
		}

		void IWriter.WriteXml(XmlWriter writer)
		{
			writer.WriteStartElement("x", "Xct", "urn:schemas-microsoft-com:office:excel");
			if (this._operands != null)
			{
				writer.WriteElementString("Count", "urn:schemas-microsoft-com:office:excel", this._operands.Count.ToString(CultureInfo.InvariantCulture));
			}
			else
			{
				writer.WriteElementString("Count", "urn:schemas-microsoft-com:office:excel", "0");
			}
			if (this._sheetIndex != Int32.MinValue)
			{
				writer.WriteElementString("SheetIndex", "urn:schemas-microsoft-com:office:excel", this._sheetIndex.ToString(CultureInfo.InvariantCulture));
			}
			if (this._operands != null)
			{
				((IWriter) this._operands).WriteXml(writer);
			}
			writer.WriteEndElement();
		}

		internal static bool IsElement(XmlElement element)
		{
			return Util.IsElement(element, "Xct", "urn:schemas-microsoft-com:office:excel");
		}

		// Properties
		public CrnCollection Operands
		{
			get
			{
				if (this._operands == null)
				{
					this._operands = new CrnCollection();
				}
				return this._operands;
			}
		}

		public int SheetIndex
		{
			get
			{
				if (this._sheetIndex == int.MinValue)
				{
					return 0;
				}
				return this._sheetIndex;
			}
			set
			{
				if (value < 0)
				{
					throw new ArgumentException("Invalid range, > 0");
				}
				this._sheetIndex = value;
			}
		}
	}


}
