//ZD 100147 Start
/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
//ZD 100147 End
using System;
using System.Globalization;
using System.Collections;
using System.Collections.Specialized;
using System.Xml;
using System.CodeDom;

namespace myVRM.ExcelXmlWriter
{
	public sealed class XctCollection : CollectionBase, IWriter, ICodeWriter
	{
		// Methods
		internal XctCollection()
		{
		}

		public Xct Add()
		{
			Xct item = new Xct();
			this.Add(item);
			return item;
		}

		public int Add(Xct item)
		{
			return base.InnerList.Add(item);
		}

		void ICodeWriter.WriteTo(CodeTypeDeclaration type, CodeMemberMethod method, CodeExpression targetObject)
		{
			for (int i = 0; i < base.InnerList.Count; i++)
			{
				Xct xct = this[i];
				string name = "Xct" + i.ToString(CultureInfo.InvariantCulture);
				CodeVariableDeclarationStatement statement = new CodeVariableDeclarationStatement(typeof(Xct), name, new CodeMethodInvokeExpression(new CodePropertyReferenceExpression(targetObject, "References"), "Add", new CodeExpression[0]));
				method.Statements.Add(statement);
				((ICodeWriter) xct).WriteTo(type, method, new CodeVariableReferenceExpression(name));
			}
		}

		void IWriter.WriteXml(XmlWriter writer)
		{
			for (int i = 0; i < base.InnerList.Count; i++)
			{
				((IWriter) base.InnerList[i]).WriteXml(writer);
			}
		}

		public bool Contains(Xct link)
		{
			return base.InnerList.Contains(link);
		}

		public void CopyTo(Xct[] array, int index)
		{
			base.InnerList.CopyTo(array, index);
		}

		public int IndexOf(Xct item)
		{
			return base.InnerList.IndexOf(item);
		}

		public void Insert(int index, Xct item)
		{
			base.InnerList.Insert(index, item);
		}

		public void Remove(string item)
		{
			base.InnerList.Remove(item);
		}

		// Properties
		public Xct this[int index]
		{
			get
			{
				return (Xct) base.InnerList[index];
			}
		}
	}


}
