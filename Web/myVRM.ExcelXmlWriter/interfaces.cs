//ZD 100147 Start
/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
//ZD 100147 End
using System;
using System.CodeDom;
using System.Xml;

namespace myVRM.ExcelXmlWriter
{
	public interface IWriter
	{
		// Methods
		void WriteXml(XmlWriter writer);
	}

	internal interface IReader
	{
		// Methods
		void ReadXml(XmlElement element);
	}

	public interface ICodeWriter
	{
		// Methods
		void WriteTo(CodeTypeDeclaration type, CodeMemberMethod method, CodeExpression targetObject);
	}
}
