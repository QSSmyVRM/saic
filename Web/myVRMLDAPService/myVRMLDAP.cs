﻿//ZD 100147 Start
/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*///ZD 100147 End
#region References
using System;
using System.Runtime.InteropServices;
using System.Threading;
using System.Net;
using System.IO;
using System.Text;
using System.Security.Cryptography.X509Certificates;
using System.Data;
using System.Xml;
using System.Collections;
using System.ComponentModel;
using System.Web;
using myVRM.DataLayer;
using System.Collections.Generic;
using System.ServiceProcess;

#endregion

namespace myVRMLDAPService
{
    public partial class myVRMLDAP : ServiceBase
    {
       
        String dirPth = Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location);
        ASPIL.VRMServer myvrmCom = new ASPIL.VRMServer();
        System.Timers.Timer timerSync= new System.Timers.Timer();
        String MyVRMServer_ConfigPath = "";
        String COM_ConfigPath = "";
        String RTC_ConfigPath = "";
        NS_CONFIG.Config config = null;
        NS_MESSENGER.ConfigParams configParams = null;
        string errMsg = null;
        NS_LOGGER.Log log = null;
        bool ret = false;


        public myVRMLDAP()
        {
            InitializeComponent();
        }

        protected override void OnStart(string[] args)
        {
            
            try
            {

                String MyVRMServer_ConfigPath = dirPth + "\\VRMSchemas\\";
                String COM_ConfigPath = dirPth + "\\VRMSchemas\\COMConfig.xml";
                String RTC_ConfigPath = dirPth + "\\VRMSchemas\\VRMRTCConfig.xml";
                config = new NS_CONFIG.Config();
                configParams = new NS_MESSENGER.ConfigParams();
                ret = config.Initialize(ref configParams, ref errMsg, MyVRMServer_ConfigPath, RTC_ConfigPath);
                log = new NS_LOGGER.Log(configParams);

                log.Trace("Into The service started");
                log.Trace("Various Configs COM:" + COM_ConfigPath + " RTC:" + RTC_ConfigPath + " ASPIL:" + MyVRMServer_ConfigPath);
                log.Trace("Site URL: " + configParams.siteUrl);
                log.Trace("ActivationTimer:" + configParams.activationTimer);

                timerSync.Elapsed += new System.Timers.ElapsedEventHandler(timerSync_Elapsed);
                timerSync.Interval = 30*1000;
                timerSync.Enabled = true;
                timerSync.AutoReset = true;
                timerSync.Start();

               
            }
            catch (Exception ex)
            {
                log.Trace(ex.StackTrace + ex.ToString());

            }

        }

        void timerSync_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            String searchConfInXML = "";
            String searchConfOutXML = "";
            try
            {
                timerSync.Enabled = false;
                timerSync.AutoReset = false;
                timerSync.Stop();

                myvrmCom = new ASPIL.VRMServer();
                searchConfInXML = "<LDAP><UserID>11</UserID></LDAP>";
                searchConfOutXML = myvrmCom.Operations(MyVRMServer_ConfigPath, "SyncWithLdap", searchConfInXML);

                timerSync.Enabled = true;
                timerSync.AutoReset = true;
                timerSync.Start();
                

            }
            catch (Exception ex)
            {
                log.Trace(ex.StackTrace + ex.ToString());

            }
        }

        protected override void OnStop()
        {
            timerSync.Enabled = false;
            timerSync.AutoReset = false;
            timerSync.Stop();
        }
    }
}
