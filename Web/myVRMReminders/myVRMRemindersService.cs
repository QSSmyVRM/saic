﻿#region References
using System;
using System.Runtime.InteropServices;
using System.Net;
using System.IO;
using System.Text;
using System.Security.Cryptography.X509Certificates;
using msxml4_Net;
using System.Data;
using System.Xml;
using System.Collections;
using System.ComponentModel;
using System.Web;
using System.Collections.Generic;
using System.ServiceProcess;

#endregion

namespace myVRMReminders
{
    public partial class myVRMRemindersService : ServiceBase
    {
        
        static String dirPth = Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location);
        static String MyVRMServer_ConfigPath = dirPth + "\\VRMSchemas\\";
        static String COM_ConfigPath = dirPth + "\\VRMSchemas\\COMConfig.xml";
        static String RTC_ConfigPath = dirPth + "\\VRMSchemas\\VRMRTCConfig.xml";
        static System.Timers.Timer timerLaunchReminders = new System.Timers.Timer();
		static System.Timers.Timer timerBatchRpt = new System.Timers.Timer(); //FB 2410
        static System.Timers.Timer timerPollCDR = new System.Timers.Timer();
        static System.Timers.Timer timerFetchMCUInfo = new System.Timers.Timer();//FB 2501 Call Monitoring Dec6
        static System.Timers.Timer tmrUpdateRooms = new System.Timers.Timer();//FB 2392

        static NS_CONFIG.Config config = null;
        static NS_MESSENGER.ConfigParams configParams = null;
        static string configPath = dirPth + "\\VRMMaintServiceConfig.xml";
        static string errMsg = null;
        static NS_LOGGER.Log log = null;
        static bool ret = false;
        static ASPIL.VRMServer myvrmCom = null;
        static VRMRTC.VRMRTC objRTC = null;
        static System.Globalization.CultureInfo globalCultureInfo = new System.Globalization.CultureInfo("en-US", true);//FB 2501

        public myVRMRemindersService()
        {
            InitializeComponent();
        }

        protected override void OnStart(string[] args)
        {

            double reminderLaunch = 90000; //1.5 Minutes
            double CDRLaunch = 30000; //FB 2593 15Minutes // FB 2944
            double rptSentInterval = 12 * 60 * 60 * 1000; //FB 2410 //12 Hours

            try
            {
                config = new NS_CONFIG.Config();
                configParams = new NS_MESSENGER.ConfigParams();
                ret = config.Initialize(configPath, ref configParams, ref errMsg, MyVRMServer_ConfigPath,RTC_ConfigPath); //FB 2944
                log = new NS_LOGGER.Log(configParams);

                log.Trace("Into The service started");
                log.Trace("Various Configs ASPIL:" + MyVRMServer_ConfigPath);
                                

                timerLaunchReminders.Elapsed += new System.Timers.ElapsedEventHandler(timerLaunchReminders_Elapsed);
                timerLaunchReminders.Interval = reminderLaunch;
                timerLaunchReminders.Start();

                //FB 2501 - CDR - Starts
                timerPollCDR.Elapsed += new System.Timers.ElapsedEventHandler(timerPollCDR_Elapsed);
                timerPollCDR.Interval = CDRLaunch; //FB 2593
                timerPollCDR.Start();
                //FB 2501 - CDR - End

                //FB 2501 - Call Monitoring - Starts Dec6
                timerFetchMCUInfo.Elapsed += new System.Timers.ElapsedEventHandler(timerFetchMCUInfo_Elapsed);
                timerFetchMCUInfo.Interval = reminderLaunch;
                timerFetchMCUInfo.Start();
                //FB 2501 - Call Monitoring - End

                //FB 2392 Starts
                DateTime startDate = DateTime.Parse(DateTime.Now.AddDays(1).ToShortDateString() + " " + "12:00:00 AM");
                DateTime curDate = DateTime.Now;
                Double timeInterval = startDate.Subtract(curDate).TotalMilliseconds;

                tmrUpdateRooms.Elapsed += new System.Timers.ElapsedEventHandler(timerLaunchRoomUpdate_Elapsed);
                tmrUpdateRooms.Interval = timeInterval;
                tmrUpdateRooms.Start();
                //FB 2392 Ends
				
				timerBatchRpt.Elapsed += new System.Timers.ElapsedEventHandler(timerBatchRpt_Elapsed);
                timerBatchRpt.Interval = rptSentInterval;
                timerBatchRpt.Enabled = true;                
                timerBatchRpt.Start();
            }
            catch (Exception ex)
            {
                log.Trace(ex.StackTrace + ex.ToString());

            }
        }

        #region timerLaunchReminders_Elapsed
        /// <summary>
        /// timerLaunchReminders_Elapsed
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        static void timerLaunchReminders_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            try
            {
                System.Threading.Thread.CurrentThread.CurrentCulture = globalCultureInfo;//FB 2501                   
                log.Trace("Into Reminders Lauch:" + DateTime.Now.ToString("F"));
                timerLaunchReminders.AutoReset = false;
                RemindersThread();
                System.Threading.Thread.Sleep(5000);
                timerLaunchReminders.AutoReset = true; ; 

            }
            catch (Exception ex)
            {
                log.Trace(ex.StackTrace + ex.ToString());

            }
        }
        #endregion

        #region RemindersThread

        private static void RemindersThread()
        {
            try
            {
                
                myvrmCom = new ASPIL.VRMServer();
                String remindersConfInXML = "<Login><UserID>11</UserID></Login>";
                String remindersConfOutXML = myvrmCom.Operations(MyVRMServer_ConfigPath, "SendAutomatedReminders", remindersConfInXML);
                if (remindersConfOutXML.IndexOf("<success>") >= 0)
                    log.Trace("Reminders successfully Lauched:" + DateTime.Now.ToString("F"));
                myvrmCom = null;
            }
            catch (Exception ex)
            {
                log.Trace(ex.StackTrace + ex.ToString());

            }
        }

        #endregion

        //FB 2501 - CDR - Starts
        #region timerPollCDR_Elapsed
        /// <summary>
        /// timerPollCDR_Elapsed
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        static void timerPollCDR_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            double CDRLaunch = 900000; //FB 2944
            try
            {
                System.Threading.Thread.CurrentThread.CurrentCulture = globalCultureInfo;//FB 2501
                log.Trace("Into CDR Method:" + DateTime.Now.ToString("F"));
                timerPollCDR.AutoReset = false;
                PollCDRThread();
                System.Threading.Thread.Sleep(5000);
                timerPollCDR.Interval = CDRLaunch; // FB 2944
                timerPollCDR.AutoReset = true;                
            }
            catch (Exception ex)
            {
                log.Trace(ex.StackTrace + ex.ToString());

            }
        }
        #endregion

        #region PollCDRThread

        private static void PollCDRThread()
        {
            try
            {
                objRTC = new VRMRTC.VRMRTC();
                String dirPth = Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location); //FB 2683
                String pollCDRInXML = "<Login><UserID>11</UserID><SeriveFilePath>" + dirPth + "</SeriveFilePath></Login>";//FB 2683
                String pollCDROutXML = objRTC.Operations(RTC_ConfigPath, "CallDetailRecords", pollCDRInXML);
                if (pollCDROutXML.IndexOf("<success>") >= 0)
                    log.Trace("Poll CDR command Lauched:" + DateTime.Now.ToString("F"));

                log.Trace("After PollCDRThread command:" + DateTime.Now.ToString("F"));

                objRTC = null;
            }
            catch (Exception ex)
            {
                log.Trace(ex.StackTrace + ex.ToString());
            }
        }

        #endregion
        //FB 2501 - CDR - End

        //FB 2501 - Call Monitoring - Starts Dec6

        #region timerFetchMCUInfo_Elapsed
        /// <summary>
        /// timerFetchMCUInfo_Elapsed
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        static void timerFetchMCUInfo_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            try
            {
                log.Trace("Into Fetch MCU Method:" + DateTime.Now.ToString("F"));
                timerFetchMCUInfo.AutoReset = false;
                timerFetchMCUInfo.Stop();
                FetchMCUThread();
                System.Threading.Thread.Sleep(5000);
                timerFetchMCUInfo.AutoReset = true; 
                timerFetchMCUInfo.Start();
            }
            catch (Exception ex)
            {
                log.Trace(ex.StackTrace + ex.ToString());

            }
        }
        #endregion

        #region FetchMCUThread
        /// <summary>
        /// FetchMCUThread
        /// </summary>
        private static void FetchMCUThread()
        {
            try
            {
                objRTC = new VRMRTC.VRMRTC();
                String FetchMCUInfoInXML = "<Login><UserID>11</UserID></Login>";
                String FetchMCUInfoOutXML = objRTC.Operations(RTC_ConfigPath, "FetchMCUInfo", FetchMCUInfoInXML);
                if (FetchMCUInfoOutXML.IndexOf("<success>") >= 0)
                    log.Trace("FetchMCUInfo command Lauched:" + DateTime.Now.ToString("F"));

                log.Trace("After FetchMCUInfo command:" + DateTime.Now.ToString("F"));

                objRTC = null;
            }
            catch (Exception ex)
            {
                log.Trace(ex.StackTrace + ex.ToString());
            }
        }

        #endregion

        //FB 2501 - Call Monitoring - End

        
        protected override void OnStop()
        {
            timerLaunchReminders.Enabled = false;
            timerLaunchReminders.AutoReset = false;
            timerLaunchReminders.Stop();

            timerPollCDR.Enabled = false; //FB 2501 - CDR
            timerPollCDR.AutoReset = false;
            timerPollCDR.Stop();

            timerFetchMCUInfo.Enabled = false; //FB 2501 - Call Monitoring Dec6
            timerFetchMCUInfo.AutoReset = false;
            timerFetchMCUInfo.Stop();
        }


        //FB 2392 Starts
        #region timerLaunchRoomUpdate_Elapsed

        static void timerLaunchRoomUpdate_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            try
            {
                log.Trace("Into Room Update Lauch:" + DateTime.Now.ToString("F"));
                tmrUpdateRooms.AutoReset = false;
                if (objRTC == null)
                    objRTC = new VRMRTC.VRMRTC();

                GetLocationUpdate();

                tmrUpdateRooms.Interval = 24 * 60 * 60 * 1000;
                tmrUpdateRooms.AutoReset = true; ;
            }
            catch (Exception ex)
            {
                log.Trace(ex.StackTrace + ex.ToString());
            }
        }
        #endregion

        #region GetLocationUpdate

        private static void GetLocationUpdate()
        {
            try
            {
                String inXML = "";
                log.Trace("COnfig path" + RTC_ConfigPath);
                String outXML = objRTC.Operations(RTC_ConfigPath, "GetLocationUpdate", inXML);
                if (outXML.IndexOf("<success>") >= 0)
                    log.Trace("Room Update successfully Lauched:" + DateTime.Now.ToString("F"));
                else
                    log.Trace(outXML + DateTime.Now.ToString("F"));
                myvrmCom = null;
            }
            catch (Exception ex)
            {
                log.Trace("GetLocationUpdate:" + ex.StackTrace + ex.ToString());
            }
        }
        #endregion

        //FB 2392 Ends
		//FB 2410
        #region timerActivation_Elapsed
        void timerBatchRpt_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            try
            {
                System.Threading.Thread.CurrentThread.CurrentCulture = globalCultureInfo;//FB 2501
                log.Trace("********** Entering into the GenerateBatchReport**************");
                timerFetchMCUInfo.AutoReset = false;
                timerFetchMCUInfo.Stop();                
                GenerateBatchReport();
                System.Threading.Thread.Sleep(5000);
                timerBatchRpt.AutoReset = true;
                timerBatchRpt.Start();
                log.Trace("********** Finished GenerateBatchReport***********************");
            }
            catch (Exception ex)
            {
                log.Trace(ex.StackTrace + ex.ToString());

            }
        }
        #endregion
        //FB 2410
        #region GenerateBatchReport
        /// <summary>
        /// GenerateBatchReport
        /// </summary>
        /// <returns></returns>
        private static bool GenerateBatchReport()
        {
            try

            {

                String inXML = "", OutXML = "";

                inXML = "<report>";
                inXML += "<configpath>" + MyVRMServer_ConfigPath + "</configpath>";
                inXML += "<Destination>" + configParams.reportFilePath + "</Destination>";
                inXML += "</report>";

                if (myvrmCom == null)
                    myvrmCom = new ASPIL.VRMServer();
                log.Trace(inXML);
                OutXML = myvrmCom.Operations(MyVRMServer_ConfigPath, "GenerateBatchReport", inXML);
                log.Trace(OutXML);
                
                if (OutXML.IndexOf("<error>") >= 0)
                    log.Trace("Error in Generate Batch Report: " + OutXML);

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return false;
            }
            return true;
        }
        #endregion
    }
}
