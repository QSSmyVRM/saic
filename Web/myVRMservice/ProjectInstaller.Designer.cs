﻿//ZD 100147 Start
/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*///ZD 100147 End
namespace myVRMservice
{
    partial class ProjectInstaller
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.myVRMServiceProcessInstaller = new System.ServiceProcess.ServiceProcessInstaller();
            this.myVRMServiceInstaller = new System.ServiceProcess.ServiceInstaller();
            // 
            // myVRMServiceProcessInstaller
            // 
            this.myVRMServiceProcessInstaller.Account = System.ServiceProcess.ServiceAccount.LocalSystem;
            this.myVRMServiceProcessInstaller.Password = null;
            this.myVRMServiceProcessInstaller.Username = null;
            // 
            // myVRMServiceInstaller
            // 
            this.myVRMServiceInstaller.Description = "Excetues and maintains essentail myVRM services";
            this.myVRMServiceInstaller.ServiceName = "Service1";
            this.myVRMServiceInstaller.StartType = System.ServiceProcess.ServiceStartMode.Automatic;
            // 
            // ProjectInstaller
            // 
            this.Installers.AddRange(new System.Configuration.Install.Installer[] {
            this.myVRMServiceProcessInstaller,
            this.myVRMServiceInstaller});

        }

        #endregion

        private System.ServiceProcess.ServiceProcessInstaller myVRMServiceProcessInstaller;
        private System.ServiceProcess.ServiceInstaller myVRMServiceInstaller;
    }
}