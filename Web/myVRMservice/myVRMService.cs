﻿//ZD 100147 Start
/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
//ZD 100147 End
/*** FB 1838 Service manager changes ***/

#region References
using System;
using System.Runtime.InteropServices;
using System.Threading;
using System.Net;
using System.IO;
using System.Text;
using System.Security.Cryptography.X509Certificates;
using msxml4_Net;
using System.Data;
using System.Xml;
using System.Collections;
using System.ComponentModel;
using System.Web;
using myVRM.DataLayer;
using System.Collections.Generic;
using System.ServiceProcess;

#endregion

namespace myVRMservice
{
    public partial class myVRMService : ServiceBase
    {
        private static DataTable _dtConfEPTs = null;
        static String dirPth = Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location);
        static String MyVRMServer_ConfigPath = dirPth + "\\VRMSchemas\\";
        static String COM_ConfigPath = dirPth + "\\VRMSchemas\\COMConfig.xml";
        static String RTC_ConfigPath = dirPth + "\\VRMSchemas\\VRMRTCConfig.xml";
        static ASPIL.VRMServer myvrmCom = new ASPIL.VRMServer();
        //static web_com_v18_Net.Com com = null; //FB 2027
        static VRMRTC.VRMRTC obj = null;
        static Thread launchThrd = null;
        static Thread updateThrd = null;
        static Thread updateMessageThrd = null;//FB 2486
        static Thread FetchSwitchingCallsThrd = null;//FB 2595
        static System.Timers.Timer timerActivation = new System.Timers.Timer();
        static System.Timers.Timer timerLaunchandUpdt = new System.Timers.Timer();
        static System.Timers.Timer timerP2PLaunchUpdt = new System.Timers.Timer();
        static System.Timers.Timer timerP2PTerminate = new System.Timers.Timer();
        static System.Timers.Timer timerFetchSwitchingCalls = new System.Timers.Timer();//FB 2595
        static NS_CONFIG.Config config = null;
        static NS_MESSENGER.ConfigParams configParams = null;
        static string configPath = dirPth + "\\VRMMaintServiceConfig.xml";
        static string errMsg = null;
        static NS_LOGGER.Log log = null;
        static bool ret = false;
        static System.Globalization.CultureInfo globalCultureInfo = new System.Globalization.CultureInfo("en-US",true);//FB 2545
        
        

        public myVRMService()
        {
            InitializeComponent();
        }

        #region OnStart
        protected override void OnStart(string[] args)
        {

            double conflauch = 30000;
            double P2Pterminate = 5000;
            try
            {

                config = new NS_CONFIG.Config();
                configParams = new NS_MESSENGER.ConfigParams();
                ret = config.Initialize(configPath, ref configParams, ref errMsg, MyVRMServer_ConfigPath, RTC_ConfigPath);
                log = new NS_LOGGER.Log(configParams);

                log.Trace("**** Starting Myvrm Service ****" + DateTime.Now.ToLocalTime());
                log.Trace("Various Configs COM:" + COM_ConfigPath + " RTC:" + RTC_ConfigPath + " ASPIL:" + MyVRMServer_ConfigPath);
                log.Trace("Site URL: " + configParams.siteUrl);
                log.Trace("ActivationTimer:" + configParams.activationTimer);

                timerActivation.Elapsed += new System.Timers.ElapsedEventHandler(timerActivation_Elapsed);
                timerActivation.Interval = configParams.activationTimer;
                timerActivation.Enabled = true;
                timerActivation.AutoReset = true;
                timerActivation.Start();

                timerLaunchandUpdt.Elapsed += new System.Timers.ElapsedEventHandler(timerLaunchandUpdt_Elapsed);
                timerLaunchandUpdt.Interval = conflauch;
                timerLaunchandUpdt.Enabled = true;
                timerLaunchandUpdt.AutoReset = true;
                timerLaunchandUpdt.Start();
                log.Trace("Timer for AudioVideo calls started...." + DateTime.Now.ToLocalTime());

                timerP2PLaunchUpdt.Elapsed += new System.Timers.ElapsedEventHandler(timerP2PLaunchUpdt_Elapsed);
                timerP2PLaunchUpdt.Interval = conflauch;
                timerP2PLaunchUpdt.Enabled = true;
                timerP2PLaunchUpdt.AutoReset = true;
                timerP2PLaunchUpdt.Start();
                log.Trace("Timer for P2P calls started...." + DateTime.Now.ToLocalTime());

                timerP2PTerminate.Elapsed += new System.Timers.ElapsedEventHandler(timerP2PTerminate_Elapsed);
                timerP2PTerminate.Interval = P2Pterminate;
                timerP2PTerminate.Enabled = true;
                timerP2PTerminate.AutoReset = true;
                timerP2PTerminate.Start();
                log.Trace("P2P timer event started...." + DateTime.Now.ToLocalTime());

                
                //FB 2595 Start

                timerFetchSwitchingCalls.Elapsed += new System.Timers.ElapsedEventHandler(timerFetchSwitchingCalls_Elapsed);
                timerFetchSwitchingCalls.Interval = conflauch;
                timerFetchSwitchingCalls.Enabled = true;
                timerFetchSwitchingCalls.AutoReset = true;
                timerFetchSwitchingCalls.Start();
                log.Trace("Timer for Updating calls for switching started...." + DateTime.Now.ToLocalTime());

                //FB 2595 End

                log.Trace("Exits Myvrm service cycle...");
            }
            catch (Exception ex)
            {
                log.Trace(ex.StackTrace + ex.ToString());

            }
        }
        #endregion

        #region timerLaunchandUpdt_Elapsed
        static void timerLaunchandUpdt_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            double conflauch = 30000;
            try
            {
                //timerLaunchandUpdt.Stop();
                log.Trace("Entering timerLaunchandUpdt_Elapsed block...." + DateTime.Now.ToLocalTime());
                
                GetConferences();

                if (launchThrd != null)
                    while (launchThrd.IsAlive)
                        launchThrd.Join();//To Terminate current thread

                if (updateThrd != null)
                    while (updateThrd.IsAlive)
                        updateThrd.Join();//To Terminate current thread

                timerLaunchandUpdt.Interval = conflauch;
                timerLaunchandUpdt.Enabled = true;
                timerLaunchandUpdt.AutoReset = true;
                timerLaunchandUpdt.Start();
                log.Trace("Timer event timerLaunchandUpdt started...." + DateTime.Now.ToLocalTime());

            }
            catch (Exception ex)
            {
                log.Trace(ex.StackTrace + ex.ToString());

            }
        }
        #endregion

        #region timerP2PLaunchUpdt_Elapsed
        static void timerP2PLaunchUpdt_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            double conflauch = 30000;
            try
            {
                //timerP2PCalls.Stop();
                log.Trace("Entering timerP2PLaunchUpdt_Elapsed block...." + DateTime.Now.ToLocalTime());
                GetP2PConferences();

                if (launchThrd != null)
                    while (launchThrd.IsAlive)
                        launchThrd.Join();//To Terminate current thread

                if (updateThrd != null)
                    while (updateThrd.IsAlive)
                        updateThrd.Join();//To Terminate current thread

                timerP2PLaunchUpdt.Interval = conflauch;
                timerP2PLaunchUpdt.Enabled = true;
                timerP2PLaunchUpdt.AutoReset = true;
                timerP2PLaunchUpdt.Start();
                log.Trace("Timer event timerP2PLaunchUpdt started...." + DateTime.Now.ToLocalTime());

            }
            catch (Exception ex)
            {
                log.Trace(ex.StackTrace + ex.ToString());

            }
        }
        #endregion

        #region timerP2PTerminate_Elapsed
        static void timerP2PTerminate_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            double P2Pterminate = 5000;
            try
            {
                timerP2PTerminate.Stop();

                if (obj == null)//FB 2363 start
                    obj = new VRMRTC.VRMRTC();


                obj.Operations(RTC_ConfigPath, "TerminateCompletedP2PConfs", "<Admin>11</Admin>");

                timerP2PTerminate.Interval = P2Pterminate;
                timerP2PTerminate.Enabled = true;
                timerP2PTerminate.AutoReset = true;
                timerP2PTerminate.Start();

            }
            catch (Exception ex)
            {
                log.Trace(ex.Message);

            }
        }
        #endregion

        #region timerActivation_Elapsed
        static void timerActivation_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            try
            {
                CheckActivation();

            }
            catch (Exception ex)
            {
                log.Trace(ex.StackTrace + ex.ToString());

            }
        }
        #endregion

        //FB 2595 Start

        #region timerFetchSwitchingCalls_Elapsed
        static void timerFetchSwitchingCalls_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            double Switchopt = 30000;
            try
            {
                log.Trace("Entering timerFetchSecuredCalls_Elapsed block...." + DateTime.Now.ToLocalTime());

                GetSwitchingConferences();

                if (FetchSwitchingCallsThrd != null)
                    while (FetchSwitchingCallsThrd.IsAlive)
                        FetchSwitchingCallsThrd.Join();//To Terminate current thread

                timerFetchSwitchingCalls.Interval = Switchopt;
                timerFetchSwitchingCalls.Enabled = true;
                timerFetchSwitchingCalls.AutoReset = true;
                timerFetchSwitchingCalls.Start();
                log.Trace("Timer event timerFetchSecuredCalls started...." + DateTime.Now.ToLocalTime());

            }
            catch (Exception ex)
            {
                log.Trace(ex.StackTrace + ex.ToString());

            }
        }
        #endregion

        //FB 2595 End

        #region GetSSLPage
        public static bool GetSSLPage(string url)
        {
            try
            {
                ServicePointManager.ServerCertificateValidationCallback = new System.Net.Security.RemoteCertificateValidationCallback(ValidateServerCertificate);

                WebRequest req = WebRequest.Create(url);
                req.Timeout = 100000;//GP Timeout Issue
                WebResponse result = req.GetResponse();
                Stream ReceiveStream = result.GetResponseStream();
                Encoding encode = System.Text.Encoding.GetEncoding("utf-8");
                StreamReader sr = new StreamReader(ReceiveStream, encode);
                Console.WriteLine("\r\nResponse stream received");
                Char[] read = new Char[10];//GP
                //int count = sr.Read(read, 0, 256);//GP
                int count = sr.Read(read, 0, 10);//GP

                Console.WriteLine("HTML...\r\n");
                //GP START
                //while (count > 0)
                //{
                //    String str = new String(read, 0, count);
                //    Console.Write(str);
                //    count = sr.Read(read, 0, 256);
                //}

                if (count > 0)
                {
                    String str = new String(read, 0, count);
                    Console.Write(str);
                }
                //GP END
                Console.WriteLine("");

                return true;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                Console.WriteLine("\r\nThe request URI could not be found or was malformed");
                return false;
            }
        }
        #endregion

        #region CheckActivation
        public static void CheckActivation()
        {

            String receive = "";
            try
            {
                if (ret)
                {
                    //GetSSLPage(configParams.siteUrl); //GP Issue
                    try
                    {
                        string siteURL2 = "";
                        //siteURL2 = configParams.siteUrl.Replace("NewRepeatOps.aspx", "ServiceResponse.aspx");

                        XMLHTTP xmlHttp = new XMLHTTP();
                        xmlHttp.open("POST", configParams.siteUrl, false, null, null);
                        xmlHttp.send("hello");

                        if (xmlHttp.status == 200)
                        {
                            receive = xmlHttp.responseText;

                            // Local config file loaded successfully.
                            log.Trace("**** Starting Check Activation Cycle****");//GP
                            log.Trace("Date/Time: " + DateTime.Now.ToString("F"));
                            log.Trace("SiteURL : " + configParams.siteUrl);
                            log.Trace("Response : " + receive.Remove(10));
                            log.Trace("-----");
                            ServicePointManager.ServerCertificateValidationCallback = new System.Net.Security.RemoteCertificateValidationCallback(ValidateServerCertificate);

                            WebRequest req = WebRequest.Create(configParams.siteUrl);
                            req.Timeout = 60000;
                            WebResponse result = req.GetResponse();
                            Stream ReceiveStream = result.GetResponseStream();
                            Encoding encode = System.Text.Encoding.GetEncoding("utf-8");
                            StreamReader sr = new StreamReader(ReceiveStream, encode);
                            Char[] read = new Char[10];
                            int count = sr.Read(read, 0, 10);
                        }


                    }
                    catch (Exception e)
                    {
                        receive = e.Message;
                    }
                }
                else
                {
                    Console.WriteLine("Failure in reading from config file.");
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }

        }
        #endregion

        //FB 2363
        #region GenerateESUserReport
        /// <summary>
        /// GenerateESUserReport
        /// </summary>
        /// <returns></returns>
        private static bool GenerateESUserReport()
        {
            try
            {
                String inXML = "", OutXML = "";

                inXML = "<report>";
                inXML += "<configpath>" + MyVRMServer_ConfigPath + "</configpath>";
                inXML += "<reportType>GU</reportType>";
                inXML += "<export>1</export>";
                inXML += "<Destination>" + configParams.reportFilePath + "</Destination>";
                inXML += "<fileName>UserReport.xls</fileName>";
                inXML += "</report>";


                if (myvrmCom == null)
                    myvrmCom = new ASPIL.VRMServer();
                OutXML = myvrmCom.Operations(MyVRMServer_ConfigPath, "GenerateESUserReport", inXML);
                if (OutXML.IndexOf("<error>") >= 0)
                    log.Trace("Error in Generate User Report: " + OutXML);

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return false;
            }
            return true;
        }
        #endregion

        #region GetP2PConferences
        /// <summary>
        /// GetP2PConferences
        /// </summary>
        /// <param name="conftype"></param>
        /// conftype = 4 for p2p calls
        private static void GetP2PConferences()
        {
            try
            {
                log.Trace("Into GetP2PConferences... " + DateTime.Now.ToLocalTime());
                if(myvrmCom == null)
                    myvrmCom = new ASPIL.VRMServer();

                CreateEPTDataTable();

                String searchConfInXML = "<SearchConference><UserID>11</UserID><ConferenceID/><ConferenceUniqueID/><StatusFilter><ConferenceStatus>0</ConferenceStatus><ConferenceStatus>6</ConferenceStatus><ConferenceStatus>5</ConferenceStatus></StatusFilter><TypeFilter><ConfType>4</ConfType></TypeFilter></SearchConference>"; //FB 2434
                String searchConfOutXML = myvrmCom.Operations(MyVRMServer_ConfigPath, "GetOngoing", searchConfInXML);
                
                GetP2PConfs(searchConfOutXML);
            }
            catch (Exception ex)
            {
                log.Trace(ex.StackTrace + ex.ToString());

            }
        }
        #endregion

        #region GetConferences
        /// <summary>
        /// GetConferences
        /// </summary>
        /// <param name="conftype"></param>
        /// if conftype = 2 video; 4 p2p; 6 audio; 7 room; 1 all
        /// need to code for launching audio calls
        private static void GetConferences()
        {
            try
            {
                log.Trace("Into GetConferences... " + DateTime.Now.ToLocalTime());
                if (myvrmCom == null)
                    myvrmCom = new ASPIL.VRMServer();

                CreateEPTDataTable();
                String searchConfInXML = "<SearchConference><UserID>11</UserID><ConferenceID/><ConferenceUniqueID/><StatusFilter><ConferenceStatus>0</ConferenceStatus><ConferenceStatus>6</ConferenceStatus><ConferenceStatus>5</ConferenceStatus></StatusFilter><TypeFilter><ConfType>2</ConfType></TypeFilter></SearchConference>"; //FB 2434
                String searchConfOutXML = myvrmCom.Operations(MyVRMServer_ConfigPath, "GetOngoing", searchConfInXML);
                GetConfs(searchConfOutXML);
            }
            catch (Exception ex)
            {
                log.Trace(ex.StackTrace + ex.ToString());
            }
        }
        #endregion
  
        #region CreateEPTDataTable
        private static void CreateEPTDataTable()
        {
            try
            {
                if (_dtConfEPTs == null)
                {
                    _dtConfEPTs = new DataTable();
                    _dtConfEPTs.Columns.Add("confid");
                    _dtConfEPTs.Columns.Add("ConfType");
                    _dtConfEPTs.Columns.Add("rowType");
                    _dtConfEPTs.Columns.Add("EndpointID");
                    _dtConfEPTs.Columns.Add("EPTTerminaltype");
                    _dtConfEPTs.Columns.Add("confUniqueID");
                    _dtConfEPTs.Columns.Add("confName");
                    _dtConfEPTs.Columns.Add("endpointname");
                    _dtConfEPTs.Columns.Add("ConfTxtMessage"); //FB 2486
                    _dtConfEPTs.Columns.Add("NetworkState"); //FB 2595
                    _dtConfEPTs.Columns.Add("OrgRequestTime"); //FB 2993
                }
                else
                {
                    if (!_dtConfEPTs.Columns.Contains("confid"))
                        _dtConfEPTs.Columns.Add("confid");
                    if (!_dtConfEPTs.Columns.Contains("rowType"))
                        _dtConfEPTs.Columns.Add("rowType");
                    if (!_dtConfEPTs.Columns.Contains("ConfType"))
                        _dtConfEPTs.Columns.Add("ConfType");
                    if (!_dtConfEPTs.Columns.Contains("EndpointID"))
                        _dtConfEPTs.Columns.Add("EndpointID");
                    if (!_dtConfEPTs.Columns.Contains("EPTTerminaltype"))
                        _dtConfEPTs.Columns.Add("EPTTerminaltype");
                    if (!_dtConfEPTs.Columns.Contains("confUniqueID"))
                        _dtConfEPTs.Columns.Add("confUniqueID");
                    if (!_dtConfEPTs.Columns.Contains("confName"))
                        _dtConfEPTs.Columns.Add("confName");
                    if (!_dtConfEPTs.Columns.Contains("endpointname"))
                        _dtConfEPTs.Columns.Add("endpointname");
                    if (!_dtConfEPTs.Columns.Contains("ConfTxtMessage")) //FB 2486
                        _dtConfEPTs.Columns.Add("ConfTxtMessage");
                    if (!_dtConfEPTs.Columns.Contains("NetworkState")) //FB 2595
                        _dtConfEPTs.Columns.Add("NetworkState");
                }
            }
            catch (Exception ex)
            {
                log.Trace(ex.StackTrace + ex.ToString());

            }
        }
        #endregion

        //FB 2595 Start

        #region GetSwitchingConferences
        /// <summary>
        /// GetSwitchingConferences
        /// </summary>
        /// <param name="conftype"></param>
        /// if conftype = 2 video; 4 p2p; 6 audio; 7 room; 1 all
        /// need to code for launching audio calls
        private static void GetSwitchingConferences()
        {
            try
            {
                log.Trace("Into GetSwitchingConferences... " + DateTime.Now.ToLocalTime());
                if (myvrmCom == null)
                    myvrmCom = new ASPIL.VRMServer();

                CreateEPTDataTable();
                String searchConfInXML = "<SearchConference><UserID>11</UserID><ConferenceID/><ConferenceUniqueID/><StatusFilter><ConferenceStatus>0</ConferenceStatus></StatusFilter><TypeFilter><ConfType>2</ConfType></TypeFilter></SearchConference>"; 
                String searchConfOutXML = myvrmCom.Operations(MyVRMServer_ConfigPath, "GetCallsForSwitching", searchConfInXML);
                GetSwitchingConfs(searchConfOutXML);
            }
            catch (Exception ex)
            {
                log.Trace(ex.StackTrace + ex.ToString());
            }
        }
        #endregion

        //FB 2595 End


        //FB 2560 - This method get modified for this case

        #region GetConfs
        /// <summary>
        /// GetConfs
        /// </summary>
        /// <param name="pinXML"></param>
        private static void GetConfs(String pinXML)
        {
            int StartMode = 0; //FB 2501
            try
            {
                log.Trace("Into GetAllConfs:" + DateTime.Now.ToLocalTime());
                obj = new VRMRTC.VRMRTC();

                //FB 2027 start
                //com = new web_com_v18_Net.ComClass();
                if (myvrmCom == null)
                    myvrmCom = new ASPIL.VRMServer();
                //FB 2027 end

                if (pinXML.IndexOf("<error>") > 0)
                    return;
                if (_dtConfEPTs == null)
                    CreateEPTDataTable();
                XmlDocument xmldoc = new XmlDocument();
                xmldoc.LoadXml(pinXML);
                XmlNodeList nodes = xmldoc.SelectNodes("//SearchConference/Conferences/Conference");
                if (nodes != null)
                {
                    log.Trace("Number of AudioVideo Conferences:" + nodes.Count.ToString() + "... Time: " + DateTime.Now.ToLocalTime());

                    List<DataRow> arrRows = new List<DataRow>();
                    List<DataRow> arrUpdtRows = new List<DataRow>();
                    List<DataRow> arrConfMsgRows = new List<DataRow>();
                    XmlNode nodeConf = null;
                    String ConfInstID = "";
                    string confUID = "";
                    string confname = "";
                    Int32 ConferenceType = 0;
                    XmlNode nodeConftype = null;
                    XmlNode nodeStatus = null;
                    String ConferenceStatus = "";
                    XmlNode nodeLastRun = null;
                    String lstrun = "";
                    string isTextMsg = "";
                    XmlNodeList iCALNodes = null;
                    DataRow drLaunch = null;
                    DataRow drStatus = null;
                    string ConfTxtMessage = "";

                    for (int cnfcnt = 0; cnfcnt < nodes.Count; cnfcnt++)
                    {
                        nodeConf = null;
                        ConfInstID = "";
                        confUID = "";
                        confname = "";
                        nodeConftype = null;
                        ConferenceType = 0;
                        nodeStatus = null;
                        ConferenceStatus = "";
                        nodeLastRun = null;
                        isTextMsg = "";
                        iCALNodes = null;
                        ConfTxtMessage = "";

                        nodeConf = nodes[cnfcnt].SelectSingleNode("ConferenceID");
                        ConfInstID = nodeConf.InnerXml.Trim();
                        confUID = nodes[cnfcnt].SelectSingleNode("ConferenceUniqueID").InnerText.Trim();
                        confname = nodes[cnfcnt].SelectSingleNode("ConferenceName").InnerText.Trim();
                        nodeConftype = nodes[cnfcnt].SelectSingleNode("ConferenceType");
                        Int32.TryParse(nodeConftype.InnerXml.Trim(), out ConferenceType);

                        nodeStatus = nodes[cnfcnt].SelectSingleNode("ConferenceActualStatus");
                        ConferenceStatus = nodeStatus.InnerXml.Trim();
                        nodeLastRun = nodes[cnfcnt].SelectSingleNode("LastRunDate");
                        lstrun = nodeLastRun.InnerXml.Trim();

                        //FB 2501 Starts
                        StartMode = 0;
                        nodeLastRun = nodes[cnfcnt].SelectSingleNode("CallStartMode");
                        if (nodeLastRun != null)
                            int.TryParse(nodeLastRun.InnerXml.Trim(), out StartMode);
                        //FB 2501 Ends

                        iCALNodes = nodes[cnfcnt].SelectNodes("descendant::terminal[isCalendarInvite='1']");
                        if (iCALNodes != null)
                            if (iCALNodes.Count > 0)
                                continue;

                        isTextMsg = nodes[cnfcnt].SelectSingleNode("isTextMsg").InnerText.Trim();

                        drLaunch = null;
                        drLaunch = _dtConfEPTs.NewRow();
                        drLaunch["confid"] = ConfInstID;
                        drLaunch["ConfType"] = ConferenceType.ToString();
                        drLaunch["confUniqueID"] = confUID;
                        drLaunch["confName"] = confname;
                        drLaunch["rowType"] = "M";
                        if (ConferenceStatus == "0") //FB 2501  0 - Automatic  1 - Manual
                        {
                            drLaunch["rowType"] = "L"; //Call needs to be launched into MCU
                            if (StartMode == 0)
                                arrRows.Add(drLaunch);
                        }

                        if (arrRows.Count > 0) // create seperate threads for launching calls
                        {
                            log.Trace("New thread started for launching call: " + ConfInstID + " on " + DateTime.Now.ToLocalTime());
                            launchThrd = new Thread(LaunchOngoing);
                            launchThrd.CurrentCulture = globalCultureInfo; //FB 2545
                            launchThrd.SetApartmentState(ApartmentState.MTA);
                            launchThrd.Start(arrRows);
                            arrRows = new List<DataRow>();
                        }

                        //To fetch endpoint status
                        XmlNodeList nodesEP = nodes[cnfcnt].SelectNodes("//SearchConference/Conferences/Conference/terminals/terminal");

                        for (int eptcnt = 0; eptcnt < nodesEP.Count; eptcnt++)
                        {
                            drStatus = null;
                            drStatus = _dtConfEPTs.NewRow();
                            drStatus["confid"] = ConfInstID;
                            drStatus["confUniqueID"] = confUID;
                            drStatus["ConfType"] = ConferenceType.ToString();
                            drStatus["endpointID"] = nodesEP[eptcnt].SelectSingleNode("endpointID").InnerText.Trim();
                            drStatus["EPTTerminaltype"] = nodesEP[eptcnt].SelectSingleNode("type").InnerText.Trim();

                            if (drLaunch["rowType"].ToString() == "M")
                            {
                                arrUpdtRows.Add(drStatus);
                            }
                        }
                        if (arrUpdtRows.Count > 0)
                        {
                            log.Trace(" Endpoint Cnt: " + arrUpdtRows.Count);
                            log.Trace("New thread started for fetching endpoint status: " + ConfInstID + " on " + DateTime.Now.ToLocalTime());
                            updateThrd = new Thread(UpdateConfStatus);
                            updateThrd.SetApartmentState(ApartmentState.MTA);
                            updateThrd.CurrentCulture = globalCultureInfo; //FB 2545 
                            updateThrd.Start(arrUpdtRows);
                            arrUpdtRows = new List<DataRow>();
                        }
                        //To send conference end message - Text overlay
                        if (isTextMsg == "1")
                        {
                            if (drLaunch["rowType"].ToString() == "M")
                            {
                                ConfTxtMessage = nodes[cnfcnt].SelectSingleNode("ConferenceTextMessage").InnerText.Trim();
                                drLaunch["ConfTxtMessage"] = ConfTxtMessage;
                                arrConfMsgRows.Add(drLaunch);
                            }
                        }
                        if (arrConfMsgRows.Count > 0)
                        {
                            log.Trace("New thread started for fetching Conference Msessage: " + ConfInstID + " on " + DateTime.Now.ToLocalTime());
                            updateMessageThrd = new Thread(UpdateConfMesseageStatus);
                            updateMessageThrd.CurrentCulture = globalCultureInfo; //FB 2545
                            updateMessageThrd.SetApartmentState(ApartmentState.MTA);
                            updateMessageThrd.Start(arrConfMsgRows);
                            arrConfMsgRows = new List<DataRow>();
                        }
                        //FB 2560 Code Changes ... End
                    }
                }
                else
                {
                    log.Trace("Number of ongoing calls: 0");
                    log.Trace("Exit GetConfs Method....");
                }
            }
            catch (Exception ex)
            {
                log.Trace(ex.StackTrace + ex.ToString());
            }
            finally
            {
                //com = null;
            }
        }
        #endregion

        #region GetP2PConfs
        private static void GetP2PConfs(String pinXML)
        {
            String inXML = "";
            String outXML = "";
            try
            {
                log.Trace("Into GetP2PConfs:" + DateTime.Now.ToLocalTime());
                obj = new VRMRTC.VRMRTC();

                //FB 2027 start
                //com = new web_com_v18_Net.ComClass();
                if (myvrmCom == null)
                    myvrmCom = new ASPIL.VRMServer();
                //FB 2027 end

                if (pinXML.IndexOf("<error>") > 0)
                    return;
                if (_dtConfEPTs == null)
                    CreateEPTDataTable();
                XmlDocument xmldoc = new XmlDocument();
                xmldoc.LoadXml(pinXML);
                XmlNodeList nodes = xmldoc.SelectNodes("//SearchConference/Conferences/Conference");
                if (nodes != null)
                {

                    log.Trace("Number of P2P Conferences:" + nodes.Count.ToString() + "... Time: " + DateTime.Now.ToLocalTime());

                    List<DataRow> arrRows = new List<DataRow>();
                    List<DataRow> arrUpdtRows = new List<DataRow>();
                    for (int cnfcnt = 0; cnfcnt < nodes.Count; cnfcnt++)
                    {
                        XmlNode nodeConf = nodes[cnfcnt].SelectSingleNode("ConferenceID");
                        String ConfInstID = nodeConf.InnerXml.Trim();

                        string confUID = nodes[cnfcnt].SelectSingleNode("ConferenceUniqueID").InnerText.Trim();
                        string confname = nodes[cnfcnt].SelectSingleNode("ConferenceName").InnerText.Trim();

                        XmlNode nodeConftype = nodes[cnfcnt].SelectSingleNode("ConferenceType");
                        Int32 ConferenceType = Int32.Parse(nodeConftype.InnerXml.Trim());
                        XmlNode nodeStatus = nodes[cnfcnt].SelectSingleNode("ConferenceActualStatus");
                        String ConferenceStatus = nodeStatus.InnerXml.Trim();
                        XmlNode nodeLastRun = nodes[cnfcnt].SelectSingleNode("LastRunDate");
                        String lstrun = nodeLastRun.InnerXml.Trim();
                        if (ConferenceType.ToString().Equals(vrmConfType.P2P.ToString().Trim()))
                        {

                            inXML = "<login><userID>11</userID><confID>" + ConfInstID + "</confID></login>";

                            if (myvrmCom == null)
                                myvrmCom = new ASPIL.VRMServer();

                            outXML = myvrmCom.Operations(MyVRMServer_ConfigPath, "GetTerminalControl", inXML);//com.comCentral("GetTerminalControl", inXML, COM_ConfigPath);
                            XmlDocument xmldocEP = new XmlDocument();
                            xmldocEP.LoadXml(outXML);
                            XmlNodeList iCALNodes = xmldocEP.SelectNodes("descendant::terminal[isCalendarInvite='1']");
                            if (iCALNodes != null)
                                if (iCALNodes.Count > 0)
                                    continue;

                            if (ConferenceStatus == "0")
                            {
                                DataRow drLaunch = _dtConfEPTs.NewRow();
                                drLaunch["confid"] = ConfInstID;
                                drLaunch["ConfType"] = ConferenceType.ToString();
                                drLaunch["confUniqueID"] = confUID;
                                drLaunch["confName"] = confname;
                                arrRows.Add(drLaunch);
                            }

                            if (arrRows.Count > 0) // create seperate threads for launching calls
                            {
                                log.Trace("New thread started for launching call: " + ConfInstID + " on " + DateTime.Now.ToLocalTime());
                                launchThrd = new Thread(LaunchOngoing);
                                launchThrd.CurrentCulture = globalCultureInfo; //FB 2545
                                launchThrd.SetApartmentState(ApartmentState.MTA);
                                launchThrd.Start(arrRows);
                                arrRows = new List<DataRow>();
                            }
                            XmlNodeList nodesEP = xmldocEP.SelectNodes("//terminalControl/confInfo/terminals/terminal");
                            for (int eptcnt = 0; eptcnt < nodesEP.Count; eptcnt++)
                            {
                                DataRow dr = _dtConfEPTs.NewRow();
                                dr["confid"] = ConfInstID;
                                dr["ConfType"] = ConferenceType.ToString();
                                dr["confUniqueID"] = confUID;
                                dr["confName"] = confname;
                                dr["rowType"] = "M";
                                if (ConferenceStatus == "0")
                                {
                                    //Launch the call for Ongoing conferences
                                    dr["rowType"] = "L";
                                }
                                dr["endpointname"] = nodesEP[eptcnt].SelectSingleNode("name").InnerText.Trim();
                                dr["endpointID"] = nodesEP[eptcnt].SelectSingleNode("endpointID").InnerText.Trim();
                                dr["EPTTerminaltype"] = nodesEP[eptcnt].SelectSingleNode("type").InnerText.Trim();

                                if (dr["rowType"].ToString() == "M")
                                {
                                    arrUpdtRows.Add(dr);
                                    //_dtConfEPTs.Rows.Add(dr);
                                }
                                /* *** Commented fetching busy status (GP requirement) *** */
                                //else
                                //{
                                //    if (ConferenceType.ToString().Equals(vrmConfType.P2P.ToString().Trim()))
                                //    {
                                //        arrUpdtRows.Add(dr);
                                //        _dtConfEPTs.Rows.Add(dr);
                                //    }
                                //}
                            }
                        }
                        if (arrUpdtRows.Count > 0)
                        {
                            log.Trace("New thread started for fetching endpoint status: " + ConfInstID + " on " + DateTime.Now.ToLocalTime());
                            updateThrd = new Thread(UpdateConfStatus);
                            updateThrd.CurrentCulture = globalCultureInfo; //FB 2545
                            updateThrd.SetApartmentState(ApartmentState.MTA);
                            updateThrd.Start(arrUpdtRows);
                            arrUpdtRows = new List<DataRow>();
                        }
                    }
                }
                else
                {
                    log.Trace("Number of ongoing calls: 0");
                    log.Trace("Exit GetConfs Method....");
                }
            }
            catch (Exception ex)
            {
                log.Trace(ex.StackTrace + ex.ToString());
            }
            finally
            {
                //com = null;
            }
        }

        #endregion

        //FB 2595 Start

        #region GetSwitchingConfs
        /// <summary>
        /// GetSwitchingConfs
        /// </summary>
        /// <param name="pinXML"></param>
        private static void GetSwitchingConfs(String pinXML)
        {
            int CallNetworkState = 0;
            try
            {
                log.Trace("Into GetAllConfs:" + DateTime.Now.ToLocalTime());
                obj = new VRMRTC.VRMRTC();

                if (myvrmCom == null)
                    myvrmCom = new ASPIL.VRMServer();

                if (pinXML.IndexOf("<error>") > 0)
                    return;
                if (_dtConfEPTs == null)
                    CreateEPTDataTable();
                XmlDocument xmldoc = new XmlDocument();
                xmldoc.LoadXml(pinXML);
                XmlNodeList nodes = xmldoc.SelectNodes("//SearchConference/Conferences/Conference");
                if (nodes != null)
                {
                    log.Trace("Number of SwitchingConfs Conferences:" + nodes.Count.ToString() + "... Time: " + DateTime.Now.ToLocalTime());

                    List<DataRow> SecureRows = new List<DataRow>();
                    List<DataRow> UnSecureRows = new List<DataRow>();
                    XmlNode nodeConf = null;
                    String ConfInstID = "";
                    string confUID = "";
                    Int32 ConferenceType = 0;
                    int CallRequestTimeout = 0;//FB 2993
                    XmlNode nodeConftype = null;
                    XmlNode nodeConfswitch = null;
                    DataRow drLaunch = null;
                    XmlNode nodeRequestTime = null;

                    for (int cnfcnt = 0; cnfcnt < nodes.Count; cnfcnt++)
                    {
                        nodeConf = null;
                        ConfInstID = "";
                        confUID = "";
                        nodeConftype = null;
                        ConferenceType = 0;
                        nodeConfswitch = null;
                        CallNetworkState = 0;
                        CallRequestTimeout = 0;//FB 2993

                        nodeConf = nodes[cnfcnt].SelectSingleNode("ConferenceID");
                        ConfInstID = nodeConf.InnerXml.Trim();
                        confUID = nodes[cnfcnt].SelectSingleNode("ConferenceUniqueID").InnerText.Trim();
                        nodeConftype = nodes[cnfcnt].SelectSingleNode("ConferenceType");
                        Int32.TryParse(nodeConftype.InnerXml.Trim(), out ConferenceType);

                        CallNetworkState = 0;
                        nodeConfswitch = nodes[cnfcnt].SelectSingleNode("CallNetworkState");
                        if (nodeConfswitch != null)
                            int.TryParse(nodeConfswitch.InnerXml.Trim(), out CallNetworkState);

                        nodeRequestTime = nodes[cnfcnt].SelectSingleNode("CallRequestTimeout");//FB 2993
                        if (nodeConfswitch != null)
                            int.TryParse(nodeRequestTime.InnerXml.Trim(), out CallRequestTimeout);

                        drLaunch = null;
                        drLaunch = _dtConfEPTs.NewRow();
                        drLaunch["confid"] = ConfInstID;
                        drLaunch["ConfType"] = ConferenceType.ToString();
                        drLaunch["confUniqueID"] = confUID;
                        drLaunch["NetworkState"] = CallNetworkState;
                        drLaunch["OrgRequestTime"] = CallRequestTimeout;//FB 2993

                        if (CallNetworkState == 1)
                            SecureRows.Add(drLaunch);
                        else
                            UnSecureRows.Add(drLaunch);

                    }

                    if (SecureRows.Count > 0) // create seperate threads for Checking Secured calls
                    {
                        log.Trace("New thread for Secured call: " + ConfInstID + " on " + DateTime.Now.ToLocalTime());
                        FetchSwitchingCallsThrd = new Thread(SwitchOngoingCall);
                        FetchSwitchingCallsThrd.CurrentCulture = globalCultureInfo;
                        FetchSwitchingCallsThrd.SetApartmentState(ApartmentState.MTA);
                        FetchSwitchingCallsThrd.Start(SecureRows);
                        SecureRows = new List<DataRow>();
                    }
                    Thread.Sleep(8000);
                    if (UnSecureRows.Count > 0) // create seperate threads for Checking Unsecured calls
                    {
                        log.Trace("New thread for Unsecured call: " + ConfInstID + " on " + DateTime.Now.ToLocalTime());
                        FetchSwitchingCallsThrd = new Thread(SwitchOngoingCall);
                        FetchSwitchingCallsThrd.CurrentCulture = globalCultureInfo;
                        FetchSwitchingCallsThrd.SetApartmentState(ApartmentState.MTA);
                        FetchSwitchingCallsThrd.Start(UnSecureRows);
                        UnSecureRows = new List<DataRow>();
                    }
                }
                else
                {
                    log.Trace("Number of ongoing switching calls: 0");
                    log.Trace("Exit GetswitchingConfs Method....");
                }
            }
            catch (Exception ex)
            {
                log.Trace(ex.StackTrace + ex.ToString());
            }
            finally
            {
                //com = null;
            }
        }
        #endregion

        //FB 2595 End
        
        #region LaunchOngoing Threads
        private static void LaunchOngoing(Object objlaunchrows)
        {
            List<DataRow> launchrows = null;
            try
            {
                if (objlaunchrows != null)
                {
                    obj = new VRMRTC.VRMRTC();
                    launchrows = (List<DataRow>)objlaunchrows;
                    foreach (DataRow dr in launchrows)
                    {
                        if (dr["confUniqueID"].ToString() != "")
                        {
                            if (dr["ConfType"].ToString().Equals(vrmConfType.P2P.ToString().Trim()) || dr["ConfType"].ToString().Equals(vrmConfType.AudioVideo.ToString().Trim()))
                            {
                                log.Trace("Invoking SetConferenceOnMcu" + " Call Type: " + dr["ConfType"].ToString() + " ConfID: " + dr["confid"].ToString() + " Time: " + DateTime.Now.ToLocalTime());

                                string inXML = "";
                                inXML = "<Conference><confID>" + dr["confid"].ToString() + "</confID><FromService>1</FromService></Conference>";//FB 2441
                                string outXML = "";
                                outXML = obj.Operations(RTC_ConfigPath, "SetConferenceOnMcu", inXML);

                                log.Trace("Exits SetConferenceOnMcu" + " Call Type: " + dr["ConfType"].ToString() + " ConfID: " + dr["confid"].ToString() + " Time: " + DateTime.Now.ToLocalTime());
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                log.Trace(ex.StackTrace + ex.ToString());
            }
        }
        #endregion

        #region UpdateConfStatus Thread

        private static void UpdateConfStatus(Object objupdtrows)
        {
            String inXML = "";
            String outXML = "";
            List<DataRow> Updaterows = null;
            try
            {
                if (objupdtrows != null)
                {
                    Updaterows = (List<DataRow>)objupdtrows;
                    obj = new VRMRTC.VRMRTC();

                    foreach (DataRow dr in Updaterows)
                    {
                        if (dr["confUniqueID"].ToString() != "")
                        {
                            if (dr["ConfType"].ToString().Equals(vrmConfType.P2P.ToString().Trim()) || dr["ConfType"].ToString().Equals(vrmConfType.AudioVideo.ToString().Trim()))
                            {
                                inXML = "<GetTerminalStatus>";  //FB 1552
                                inXML += "  <login>";
                                inXML += "      <userID>11</userID>";
                                inXML += "      <confID>" + dr["confid"].ToString() + "</confID>";
                                inXML += "      <endpointID>" + dr["EndpointID"].ToString() + "</endpointID>";
                                inXML += "      <terminalType>" + dr["EPTTerminaltype"].ToString() + "</terminalType>";
                                inXML += "  </login>";
                                inXML += "</GetTerminalStatus>";

                                log.Trace("Invoking GetTerminalStatus" + " Call Type: " + dr["ConfType"].ToString() + " ConfId: " + dr["confid"].ToString() + " Time: " + DateTime.Now.ToLocalTime());
                                outXML = obj.Operations(RTC_ConfigPath, "GetTerminalStatus", inXML);
                                log.Trace("Exits GetTerminalStatus" + " Call Type: " + dr["ConfType"].ToString() + " ConfId: " + dr["confid"].ToString() + " Time: " + DateTime.Now.ToLocalTime());
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                log.Trace(ex.StackTrace + ex.ToString());
            }
        }
        #endregion

        #region ValidateServerCertificate
        public static bool ValidateServerCertificate(
      object sender,
      X509Certificate certificate,
      X509Chain chain,
      System.Net.Security.SslPolicyErrors sslPolicyErrors)
        {
            //if (sslPolicyErrors == SslPolicyErrors.None) allow  client to communicate with authenticated servers.
            //    return true;
            // To allow client to communicate with unauthenticated servers.
            return true;
        }
        #endregion

        #region OnStop
        protected override void OnStop()
        {
            timerActivation.Enabled = false;
            timerActivation.AutoReset = false;
            timerActivation.Stop();

            timerLaunchandUpdt.Enabled = false;
            timerLaunchandUpdt.AutoReset = false;
            timerLaunchandUpdt.Stop();

            timerP2PLaunchUpdt.Enabled = false; //FB 2434
            timerP2PLaunchUpdt.AutoReset = false;
            timerP2PLaunchUpdt.Stop();

            timerP2PTerminate.Enabled = false;
            timerP2PTerminate.AutoReset = false;
            timerP2PTerminate.Stop();

            myvrmCom = null;
        }
        #endregion

        //FB 2560 Start
        #region UpdateConfMesseageStatus Thread
        /// <summary>
        /// UpdateConfMesseageStatus
        /// </summary>
        /// <param name="objupdtrows"></param>
        private static void UpdateConfMesseageStatus(Object objupdtrows)
        {
            String inXML = "";
            String outXML = "";
            List<DataRow> Updaterows = null;
            try
            {
                if (objupdtrows != null)
                {
                    Updaterows = (List<DataRow>)objupdtrows;
                    obj = new VRMRTC.VRMRTC();

                    foreach (DataRow dr in Updaterows)
                    {
                        if (dr["confUniqueID"].ToString() != "")
                        {
                            if (dr["ConfType"].ToString().Equals(vrmConfType.AudioOnly.ToString().Trim()) || dr["ConfType"].ToString().Equals(vrmConfType.AudioVideo.ToString().Trim()))
                            {
                                inXML = "  <login>";
                                inXML += "      <userID>11</userID>";
                                inXML += "      <confID>" + dr["confid"].ToString() + "</confID>";
                                inXML += "      <messageText>" + dr["ConfTxtMessage"].ToString() + "</messageText>";
                                inXML += "  </login>";

                                log.Trace("Invoking SendMessageToConference" + " ConfId: " + dr["confid"].ToString() + " Time: " + DateTime.Now.ToLocalTime() + "ConfMessage:" + dr["ConfTxtMessage"].ToString());
                                outXML = obj.Operations(RTC_ConfigPath, "SendMessageToConference", inXML);
                                log.Trace("Exits SendMessageToConference" + " ConfId: " + dr["confid"].ToString() + " Time: " + DateTime.Now.ToLocalTime() + "ConfMessage:" + dr["ConfTxtMessage"].ToString());
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                log.Trace(ex.StackTrace + ex.ToString());
            }
        }

        #endregion
        //FB 2560 End

        //FB 2595 Start

        #region SwitchOngoingCall Threads
        private static void SwitchOngoingCall(Object objlaunchrows)
        {
            List<DataRow> launchrows = null;
            try
            {
                if (objlaunchrows != null)
                {
                    obj = new VRMRTC.VRMRTC();
                    launchrows = (List<DataRow>)objlaunchrows;
                    foreach (DataRow dr in launchrows)
                    {
                        if (dr["confUniqueID"].ToString() != "")
                        {
                            if (dr["ConfType"].ToString().Equals(vrmConfType.P2P.ToString().Trim()) || dr["ConfType"].ToString().Equals(vrmConfType.AudioVideo.ToString().Trim()))
                            {
                                string inXML = "";
                                inXML = "<Conference><confID>" + dr["confid"].ToString() + "</confID><NetworkState>" + dr["NetworkState"].ToString() + "</NetworkState><CallRequestTimeout>" + dr["OrgRequestTime"].ToString() + "</CallRequestTimeout></Conference>";//FB 2993
                                string outXML = "";
                                outXML = obj.Operations(RTC_ConfigPath, "SetSwitchingtoConf", inXML);

                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                log.Trace(ex.StackTrace + ex.ToString());
            }
        }
        #endregion

        //FB 2595 End
    }
}
